PATH="/bin:/cygdrive/c/Program Files/Java/jdk1.8.0_11/bin:$PATH"

echo $PATH

packageSource="`cygpath 'E:\Lawrence\QUT\Bio\Dev\ScriptSharp\bin\Zip\Packages'`"

if [ "${COMPUTERNAME}" == "CIEA-PC-046628" ]; then packageSource="`cygpath 'C:\Lawrence\QUT\Bio\Dev\ScriptSharp\bin\Zip\Packages'`"; fi
if [ "${COMPUTERNAME}" == "SEF-PC-071475" ]; then packageSource="`cygpath 'C:\Lawrence\QUT\Bio\Dev\ScriptSharp\bin\Zip\Packages'`"; fi

echo Checking source packages at ${packageSource}...

if [ ! -d "${packageSource}" ]; then
	echo "Package source folder does not exist!"
	echo "Press Enter to close."
	read ignored	
	exit 1
fi;

root="`pwd`"

if [ ! -d Scripts ]; then mkdir Scripts; fi

allPackages="ScriptSharp.0.8
	ScriptSharp.FxCop.0.8
	ScriptSharp.Lib.BingMaps.0.8
	ScriptSharp.Lib.HTML.0.8
	ScriptSharp.Lib.jQuery.0.8
	ScriptSharp.Lib.jQuery.History.0.8
	ScriptSharp.Lib.jQuery.Templating.0.8
	ScriptSharp.Lib.jQuery.UI.0.8
	ScriptSharp.Lib.jQuery.Validation.0.8
	ScriptSharp.Lib.Knockout.0.8
	ScriptSharp.Lib.Node.0.8
	ScriptSharp.Lib.Node.Azure.0.8
	ScriptSharp.Lib.Node.Express.0.8
	ScriptSharp.Lib.Node.Mongo.0.8
	ScriptSharp.Lib.Node.Neo4j.0.8
	ScriptSharp.Lib.Node.Restify.0.8
	ScriptSharp.Runtime.0.8
	ScriptSharp.Testing.0.8"

function copyStuff () {
		cp "${sourceName}" .
		which jar
		jar xf "${packageName}"
		ls -al
		for scriptFile in `find -type f | grep '\.js$'`; do 
			echo copying script ${scriptFile}
			cp "${scriptFile}" "${root}/Scripts"
		done
}

for folderName in ${allPackages}; do
	cd "${root}"

	if [ ! -d ${folderName} ]; then 
		mkdir ${folderName}
	fi

	packageName=${folderName}.nupkg

	echo
	echo ${folderName} - ${packageName}

	cd "${folderName}"

	sourceName="${packageSource}/${packageName}"

	ls -l "${sourceName}"
	ls -l "${packageName}"
	
	echo
	# pwd

	# if [ "${sourceName}" -nt "${packageName}" ]; then
	# 	echo package "'${packageName}'" is out of date

		copyStuff
	
	# elif [ ! -f "${packageName}" ]; then
	# 	echo package "'${packageName}'" does not exist
	# 	copyStuff
	# else
	# 	echo "${sourceName}" appears to be older than "${folderName}/${packageName}"
	# fi

done

echo Done. Press Enter to close.
read ignored