﻿using System;
using System.IO;

public class RefreshLibs {
	static bool Try( string actionName, Action action ) {
		try {
			action();
			return true;
		}
		catch (Exception ex) {
			Console.Error.WriteLine( "Error while trying to {0}", actionName );
			Console.Error.WriteLine( ex.Message );
			return false;
		}
	}

	public static void Main() {
		const string ssLibSource = "A:/ScriptSharp/bin/Debug";
		const string dest = "ScriptSharp";

		if (Directory.Exists( dest )
		|| Try( "creating destination directory", () => Directory.CreateDirectory( dest ) )
		) {
			DirectoryInfo src = new DirectoryInfo(ssLibSource);

			foreach (var f in src.EnumerateFiles()) {
				Console.WriteLine("Copying {0}...", f.Name);
				Try(
					String.Format( "copy {0} to {1}", f.Name, dest ),
					() => f.CopyTo( string.Format( "{0}/{1}", dest, f.Name ), true ) );
			}

			Console.WriteLine("Done");
		}
	}
}
