﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	/// <summary> Literal values for use in SvgSvg PreserveAspectRatio property.
	/// </summary>

	public class PreserveAspectRatioType {
		private string value;

		private PreserveAspectRatioType ( string s ) { this.value = s; }

		public static PreserveAspectRatioType Concat( PreserveAspectRatioType v1, PreserveAspectRatioType v2, PreserveAspectRatioType v3 ) {
			string value = v1.value;
			if ( v2 != null ) value += v2.value;
			if ( v3 != null ) value += v3.value;

			return new PreserveAspectRatioType( value );
		}

		/// <summary> - (optional, must come first if used. 
		/// </summary>

		public static PreserveAspectRatioType Defer { get { return new PreserveAspectRatioType( "defer " ); } }

		/// <summary> - Do not force uniform scaling. Scale the graphic content of the given 
		///		element non-uniformly if necessary such that the element's bounding box exactly 
		///		matches the viewport rectangle. (Note: if &lt;align&gt; is none, then the optional &lt;meetOrSlice&gt; value is ignored.) 
		/// </summary>

		public static PreserveAspectRatioType None { get { return new PreserveAspectRatioType( "none " ); } }

		/// <summary> - Force uniform scaling. Align the &lt;min-x&gt; of the element's ‘viewBox’ with the smallest X value of the viewport. Align the &lt;min-y&gt; of the element's ‘viewBox’ with the smallest Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMinYMin { get { return new PreserveAspectRatioType( "xMinYMin " ); } }

		/// <summary> - Force uniform scaling. Align the midpoint X value of the element's ‘viewBox’ with the midpoint X value of the viewport. Align the &lt;min-y&gt; of the element's ‘viewBox’ with the smallest Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMidYMin { get { return new PreserveAspectRatioType( "xMidYMin " ); } }

		/// <summary> - Force uniform scaling. Align the &lt;min-x&gt;+&lt;width&gt; of the element's ‘viewBox’ with the maximum X value of the viewport. Align the &lt;min-y&gt; of the element's ‘viewBox’ with the smallest Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMaxYMin { get { return new PreserveAspectRatioType( "xMaxYMin " ); } }

		/// <summary> - Force uniform scaling. Align the &lt;min-x&gt; of the element's ‘viewBox’ with the smallest X value of the viewport. Align the midpoint Y value of the element's ‘viewBox’ with the midpoint Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMinYMid { get { return new PreserveAspectRatioType( "xMinYMid " ); } }

		/// <summary> (the default) - Force uniform scaling. Align the midpoint X value of the element's ‘viewBox’ with the midpoint X value of the viewport. Align the midpoint Y value of the element's ‘viewBox’ with the midpoint Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMidYMid { get { return new PreserveAspectRatioType( "xMidYMid " ); } }
		/// <summary> - Force uniform scaling. Align the &lt;min-x&gt;+&lt;width&gt; of the element's ‘viewBox’ with the maximum X value of the viewport. Align the midpoint Y value of the element's ‘viewBox’ with the midpoint Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMaxYMid { get { return new PreserveAspectRatioType( "xMaxYMid " ); } }

		/// <summary> - Force uniform scaling. Align the &lt;min-x&gt; of the element's ‘viewBox’ with the smallest X value of the viewport. Align the &lt;min-y&gt;+&lt;height&gt; of the element's ‘viewBox’ with the maximum Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMinYMax { get { return new PreserveAspectRatioType( "xMinYMax " ); } }

		/// <summary> - Force uniform scaling. Align the midpoint X value of the element's ‘viewBox’ with the midpoint X value of the viewport. Align the &lt;min-y&gt;+&lt;height&gt; of the element's ‘viewBox’ with the maximum Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMidYMax { get { return new PreserveAspectRatioType( "xMidYMax " ); } }

		/// <summary> - Force uniform scaling. Align the &lt;min-x&gt;+&lt;width&gt; of the element's ‘viewBox’ with the maximum X value of the viewport. Align the &lt;min-y&gt;+&lt;height&gt; of the element's ‘viewBox’ with the maximum Y value of the viewport. 
		/// </summary>

		public static PreserveAspectRatioType XMaxYMax { get { return new PreserveAspectRatioType( "xMaxYMax " ); } }

		/// <summary> (the default) - (optional, follows the alignment) Scale the graphic such that: Scale the graphic such that:
		///		◦ aspect ratio is preserved
		///		◦ the entire ‘viewBox’ is visible within the viewport
		///		◦ the ‘viewBox’ is scaled up as much as possible, while still meeting the other criteria
		///		In this case, if the aspect ratio of the graphic does not match the viewport, some of the viewport will extend
		///		beyond the bounds of the ‘viewBox’ (i.e., the area into which the ‘viewBox’ will draw will be smaller than the
		///		viewport).
		/// </summary>

		public static PreserveAspectRatioType Meet { get { return new PreserveAspectRatioType( "meet " ); } }

		/// <summary> - (optional, follows the alignment) Scale the graphic such that: ◦ aspect ratio is preserved ◦ the entire viewport is covered by the ‘viewBox’ ◦ the ‘viewBox’ is scaled down as much as possible, while still meeting the other criteria In this case, if the aspect ratio of the ‘viewBox’ does not match the viewport, some of the ‘viewBox’ will extend beyond the bounds of the viewport (i.e., the area into which the ‘viewBox’ will draw is larger than the  viewport). 
		/// </summary>

		public static PreserveAspectRatioType Slice { get { return new PreserveAspectRatioType( "slice " ); } }

		public override string ToString () {
			return value;
		}

		internal static PreserveAspectRatioType Parse ( string p ) {
			PreserveAspectRatioType[] all = {
				PreserveAspectRatioType.Defer,
				PreserveAspectRatioType.Meet,
				PreserveAspectRatioType.None,
				PreserveAspectRatioType.Slice,
				PreserveAspectRatioType.XMaxYMax,
				PreserveAspectRatioType.XMaxYMid,
				PreserveAspectRatioType.XMaxYMin,
				PreserveAspectRatioType.XMidYMax,
				PreserveAspectRatioType.XMidYMid,
				PreserveAspectRatioType.XMidYMin,
				PreserveAspectRatioType.XMinYMax,
				PreserveAspectRatioType.XMinYMid,
				PreserveAspectRatioType.XMinYMin
			};

			PreserveAspectRatioType[] found = { null, null, null };
			int nFound = 0;

			string[] pieces = p.Split( new RegExp( "\\s+" ) );

			if ( pieces.Length > 3 ) {
				throw new Exception ( "invalid value" );
			}

			for ( int i = 0; i < pieces.Length; i++ ) {
				for ( int j = 0; j < all.Length; j++ ) {
					PreserveAspectRatioType item = all[j];

					if ( pieces[i] == item.value ) found[nFound++] = item;
				}
			}

			if ( nFound != pieces.Length ) {
				throw new Exception( "invalid value" );
			}

			if ( nFound == 1 ) return found[0];

			return PreserveAspectRatioType.Concat( found[0], found[1], found[2] );
		}
	}
}
