// SvgAnimatedLength.cs
//

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SystemQut.Svg {

	/// <summary>
	///		Dummy ISvgAnimatedLength used to set values.
	/// </summary>

	public class SvgAnimatedLength: ISvgAnimatedLength {

		public ISvgLength BaseVal {
			get { return null; }
			set {}
		}

		public ISvgLength AnimVal {
			get { return null; }
			set {}
		}
	}
}
