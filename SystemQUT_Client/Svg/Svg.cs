﻿using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Svg {
	/// <summary> Static utility methods for SVG elements.
	/// </summary>

	public static class Svg {

		/// <summary> Element factory, creats a new element in the SVG namespace, optionally 
		///		adding it as a child of the parent element and setting the initial values of a 
		///		collection of attributes.
		/// </summary>
		/// <param name="tag"> The element name, eg. "circle", "line", "svg", "g". </param>
		/// <param name="parent"> The parent element, or null if the element is not to be added to a parent. </param>
		/// <param name="attributes"> A list of name-value pairs used to set attributes. </param>
		/// <returns></returns>

		public static Element CreateElement ( string tag, Element parent, Dictionary<string, string> attributes ) {
			Element element = HtmlUtil.SetAttributes( Document.CreateElementNS( "http://www.w3.org/2000/svg", tag ), attributes );

			if ( parent != null ) parent.AppendChild( element );

			return element;
		}

		/// <summary> Factory method to create an outer SVG element. Child elements can be added by 
		///		invoking the SvgElement constructor directly.
		/// </summary>
		/// <param name="parent"> The Html element to which (if not null) the created element should be appended as a child. </param>
		/// <param name="attributes"> A collection of attribute values to be set upon creation, if not null. </param>
		/// <returns></returns>

		public static SvgSvg CreateOuterSvgElement (
			Element parent,
			Dictionary<string, string> attributes
		) {
			SvgSvg element = new SvgSvg( null, attributes );

			if ( parent != null ) parent.AppendChild( element.domElement );

			return element;
		}

		/// <summary> Converts a length measured in absolute units to the scale
		///		used within a view-box.
		/// </summary>
		/// <param name="desiredAbsoluteLength">
		///		The desired length in absolute units, for example 0.5 (cm).
		/// </param>
		/// <param name="viewboxAbsoluteSize">
		///		The absolute size of the view box, for example 5.0 (cm).
		/// </param>
		/// <param name="viewBoxRelativeSize">
		///		The view-box length to which the absolute size corresponds, for example 1000 units.
		/// </param>
		/// <returns>
		///		Returns the dimension that corresponds to the desired absolute length, scaled to the
		///		view-box frame of reference.
		/// </returns>
		
		public static double ConvertLength( 
			double desiredAbsoluteLength, 
			double viewboxAbsoluteSize, 
			double viewBoxRelativeSize 
		) {
			return viewBoxRelativeSize / viewboxAbsoluteSize * desiredAbsoluteLength;
		}
	}
}
