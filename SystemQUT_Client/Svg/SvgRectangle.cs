﻿using System;
using System.Collections.Generic;


namespace SystemQut.Svg {
	public class SvgRectangle : SvgDrawingElement {
		public SvgRectangle ( SvgContainer parent, double x, double y, double width, double height, Dictionary<string, string> attributes )
			: base( "rect", parent, attributes ) { 
			X = x; 
			Y = y; 
			Width = width; 
			Height = height;
		}

		public double X {
			get { return GetAttribute<double>( "x" ); }
			set { SetAttribute( "x", value ); }
		}

		public double Y {
			get { return GetAttribute<double>( "y" ); }
			set { SetAttribute( "y", value ); }
		}

		public override double Width {
			get { return GetAttribute<double>( "width" ); }
			set { SetAttribute( "width", value ); }
		}

		public override double Height {
			get { return GetAttribute<double>( "height" ); }
			set { SetAttribute( "height", value ); }
		}
	}
}
