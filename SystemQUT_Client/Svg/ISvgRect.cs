// ISvgRect.cs
//

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SystemQut.Svg {

	/// <summary> Represents rectangular geometry. Rectangles are defined as consisting of 
	///		a (x,y) coordinate pair identifying a minimum X value, a minimum Y value, and 
	///		a width and height, which are usually constrained to be non-negative. An SVGRect 
	///		object can be designated as read only, which means that attempts to modify the 
	///		object will result in an exception being thrown
	/// <example>
	///		interface SVGRect {
	///			attribute float x setraises(DOMException);
	///			attribute float y setraises(DOMException);
	///			attribute float width setraises(DOMException);
	///			attribute float height setraises(DOMException);
	///		};
	/// </example>
	/// </summary>

	// [ScriptIgnoreNamespace, ScriptImport]
	public interface ISvgRect {
		[ScriptField]
		double X { get; }

		[ScriptField]
		double Y { get; }

		[ScriptField]
		double Width { get; }

		[ScriptField]
		double Height { get; }
	}
}
