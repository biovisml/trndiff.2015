// ILocatable.cs
//

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Html;

namespace SystemQut.Svg {
	// TODO: Apply interfaces to correct element types.

	/// <summary> The SVGLocatable interface: all elements which either have a �transform� 
	///		attribute or don't have a �transform� attribute but whose content can have a 
	///		bounding box in current user space.
	///	<para>
	///		This interface is used internally by the SystemQut.Svg wrappers to access properties 
	///		on SVG DOM Elements.
	/// </para>
	/// <example>
	///		interface SVGLocatable {
	///			readonly attribute SVGElement nearestViewportElement;
	///			readonly attribute SVGElement farthestViewportElement;
	///			SVGRect getBBox();
	///			SVGMatrix getCTM();
	///			SVGMatrix getScreenCTM();
	///			SVGMatrix getTransformToElement(in SVGElement element) raises(SVGException);
	///		};	
	/// </example>
	/// </summary>

	[ScriptImport, ScriptIgnoreNamespace]
	interface ISvgLocatable {
		ISvgElement NearestViewportElement ();

		ISvgElement FarthestViewportElement ();

		ISvgRect GetBBox ();

		/* SVGMatrix */
		ISvgMatrix GetCTM ();
		
		/* SVGMatrix */
		ISvgMatrix GetScreenCTM ();
		
		/* SVGMatrix */
		ISvgMatrix GetTransformToElement ( Element element );
	}
}
