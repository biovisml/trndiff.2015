// GraphicalEventType.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	[ScriptConstants(UseNames=true)]
	public enum GraphicalEventType {
		onactivate = 0, 
		onclick = 1, 
		onfocusin = 2, 
		onfocusout = 3, 
		onload = 4, 
		onmousedown = 5, 
		onmousemove = 6, 
		onmouseout = 7,
		onmouseover = 8,
		onmouseup = 9
	}
}
