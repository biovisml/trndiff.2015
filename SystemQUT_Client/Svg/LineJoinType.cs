﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {
	
	[ScriptConstants(UseNames=true)]
	public enum LineJoinType {
		miter = 0, 
		round = 1, 
		bevel = 2
	}
}
