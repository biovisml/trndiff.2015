// SvgLine.cs
//

using System;
using System.Collections.Generic;
using SystemQut.Controls;

namespace SystemQut.Svg {

	/// <summary> Codebehind for a svg polyline object. 
	/// </summary>

	public class SvgPolygon : SvgDrawingElement {
		private Point[] points;
		private double xMin;
		private double xMax;
		private double yMin;
		private double yMax;

		/// <summary> Constructs a new polyline DOM element with initial settings for points and attributes.
		/// </summary>
		/// <param name="parent">
		///		The codebehind of the container to which the polyline is to be added.
		/// </param>
		/// <param name="points">
		///		Initial (possibly null or empty) sequence of vertices.
		/// </param>
		/// <param name="attributes">
		///		Initial (possibly null) collection of initial attribute name-value pairs.
		/// </param>

		public SvgPolygon ( 
			SvgContainer parent, 
			Point [] points, 
			Dictionary<string, string> attributes 
		)
			: base( "polygon", parent, attributes ) {
			Points = points;
		}

		/// <summary> Get or set the array of points.
		/// <para>
		///		This results in the 
		/// </para>
		/// </summary>

		public Point [] Points {
			get { return this.points; }
			set {
				points = value;
				Refresh();
				NotifyPropertyChanged("Points");
			}
		}

		/// <summary> Replaces the 
		/// </summary>

		private void Refresh () {
			if ( ! Script.IsValue ( points ) || points.Length == 0 ) {
				xMin = xMax = yMin = yMax = (double) Script.Undefined;
				SetAttribute<string>( "points", string.Empty );
				return;
			}

			xMin = xMax = points[0].X;
			yMin = yMax = points[0].Y;

			StringBuilder sb = new StringBuilder( points[0].ToString() );

			for ( int i = 1; i < points.Length; i++ ) {
				Point p = points[i];

				if ( p.X < xMin ) xMin = p.X; 
				if ( p.Y < yMin ) yMin = p.Y; 
				if ( p.X > xMax ) xMax = p.X; 
				if ( p.Y > yMax ) yMax = p.Y; 

				sb.Append( " " );
				sb.Append( p );
			}

			SetAttribute<string>( "points", sb.ToString() );
		}

		public override double Width {
			get {
				return xMax - xMin;
			}
			set {
				throw new Exception( "Not supported." );
			}
		}

		public override double Height {
			get {
				return yMax - yMin;
			}
			set {
				throw new Exception( "Not supported." );
			}
		}
	}
}
