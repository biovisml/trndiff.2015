// Class1.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	/// <summary> Attempt to make specification of Transforms more type-safe.
	/// <para>
	///		To do this propery a small, flat, class hierarchy should be implemented
	///		along with a TransformParser to convert between literal and object values.
	///		that will have to wait.
	/// </para>
	/// </summary>

	public class Transform {

		/// <summary> Multiplies the coordinate system by matrix
		/// <example>
		/// | a c e |
		/// | b d f |
		/// | 0 0 1 |
		/// </example>
		/// Returns a well-formed transform attribute.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		/// <param name="d"></param>
		/// <param name="e"></param>
		/// <param name="f"></param>
		/// <returns></returns>

		public static string Matrix ( double a, double b, double c, double d, double e, double f ) {
			return string.Format( "matrix({0},{1},{2},{3},{4},{5})", a, b, c, d, e, f );
		}

		/// <summary> Gets a well-formed transform attribute for translation by (dx, dy).
		/// </summary>
		/// <param name="dx"></param>
		/// <param name="dy"></param>
		/// <returns></returns>

		public static string TranslateXY ( double dx, double dy ) {
			return String.Format( "translate({0},{1})", dx, dy );
		}

		/// <summary> Gets a well-formed transform attribute for translation by (dxy, dxy).
		/// </summary>
		/// <param name="dxy"></param>
		/// <returns></returns>

		public static string Translate ( double dxy ) {
			return String.Format( "translate({0})", dxy );
		}

		/// <summary> Gets a well-formed transform attribute for scaling by (sx, sy).
		/// </summary>
		/// <param name="sx"></param>
		/// <param name="sy"></param>
		/// <returns></returns>

		public static string ScaleXY ( double sx, double sy ) {
			return String.Format( "scale({0},{1})", sx, sy );
		}

		/// <summary> Gets a well-formed transform attribute for scaling by (sxy, sxy).
		/// </summary>
		/// <param name="sxy"></param>
		/// <returns></returns>

		public static string Scale ( double sxy ) {
			return String.Format( "scale({0})", sxy );
		}

		/// <summary> rotate(&lt;rotate-angle&gt; [&lt;cx&gt; &lt;cy&gt;]), which specifies a rotation by &lt;rotate-angle&gt; degrees about a given point.
		/// If optional parameters &lt;cx&gt; and &lt;cy&gt; are not supplied, the rotate is about the origin of the current user coordinate
		/// system. The operation corresponds to the matrix [cos(a) sin(a) -sin(a) cos(a) 0 0].
		/// If optional parameters &lt;cx&gt; and &lt;cy&gt; are supplied, the rotate is about the point (cx, cy). The operation represents
		/// the equivalent of the following specification: translate(&lt;cx&gt;, &lt;cy&gt;) rotate(&lt;rotate-angle&gt;) translate(-&lt;cx&gt;,
		/// -&lt;cy&gt;).
		/// </summary>
		/// <param name="degrees"> The angle in degrees to rotate by. </param>

		static public string Rotate ( double degrees ) {
			return String.Format( "rotate({0})", degrees );
		}

		/// <summary> rotate(&lt;rotate-angle&gt; [&lt;cx&gt; &lt;cy&gt;]), which specifies a rotation by &lt;rotate-angle&gt; degrees about a given point.
		/// If optional parameters &lt;cx&gt; and &lt;cy&gt; are not supplied, the rotate is about the origin of the current user coordinate
		/// system. The operation corresponds to the matrix [cos(a) sin(a) -sin(a) cos(a) 0 0].
		/// If optional parameters &lt;cx&gt; and &lt;cy&gt; are supplied, the rotate is about the point (cx, cy). The operation represents
		/// the equivalent of the following specification: translate(&lt;cx&gt;, &lt;cy&gt;) rotate(&lt;rotate-angle&gt;) translate(-&lt;cx&gt;,
		/// -&lt;cy&gt;).
		/// </summary>
		/// <param name="degrees"> The angle in degrees to rotate by. </param>
		/// <param name="cx"> The x-xoordinate of the centre of rotation. </param>
		/// <param name="cy"> the y-coordinate of the centre of rotation. </param>

		static public string RotateXY ( double degrees, double cx, double cy ) {
			return String.Format( "rotate({0},{1},{2})", degrees, cx, cy );
		}

		/// <summary>skewX(&lt;skew-angle&gt;), which specifies a skew transformation along the x-axis.
		/// </summary>
		/// <param name="skewDegrees"></param>
		/// <returns></returns>

		public static string SkewX ( double skewDegrees ) {
			return String.Format( "skewX({0})", skewDegrees );
		}

		/// <summary>skewY(&lt;skew-angle&gt;), which specifies a skew transformation along the y-axis.
		/// </summary>
		/// <param name="skewDegrees"></param>
		/// <returns></returns>

		public static string SkewY ( double skewDegrees ) {
			return String.Format( "skewY({0})", skewDegrees );
		}
	}
}
