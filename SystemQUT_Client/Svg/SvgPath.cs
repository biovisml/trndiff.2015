﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {
	public class SvgPath : SvgDrawingElement {

		/// <summary> Creates a new Path having the specified properties.
		/// </summary>
		/// <param name="parent">
		///		An SvgContainer (possibly null) to which the circle will be added.
		/// </param>
		/// <param name="attributes">
		///		A (possibly null) collection of attribute name-value pairs.
		/// </param>

		public SvgPath ( SvgContainer parent, Dictionary<string, string> attributes )
			: base( "path", parent, attributes ) {

            if (D == null) {
                D = "";
            }
		}

		public override double Height {
			get { return 2 * GetAttribute<double>( "r" ); }
			set { SetAttribute( "r", value / 2 ); }
		}

		public override double Width {
			get {
				return 2 * GetAttribute<double>( "r" );
			}
			set { SetAttribute( "r", value / 2 ); }
		}

        public string D {
            get {return this.GetAttribute<string>("d"); }
            set { SetAttribute("d", value); }
        }

		public SvgPath MoveTo ( double X, double Y, bool absolute ) {
            if (absolute) {
                D = D + ("M" + X + " " + Y + " ");
            } else {
                D = D + ("m" + X + " " + Y + " ");
            }
			return this;
		}
		
		public SvgPath LineTo ( double X, double Y, bool absolute ) {
            if (absolute) {
                D = D + ("L" + X + " " + Y + " ");
            } else {
                D = D + ("l" + X + " " + Y + " ");
            }
			return this;
		}

		public SvgPath HorizontalLineTo ( double X, bool absolute ) {
            if (absolute) {
                D = D + ("H" + X + " ");
            } else {
                D = D + ("h" + X + " ");                   
            }
			return this;
		}

		public SvgPath VerticalLineTo ( double Y, bool absolute ) {
            if (absolute) {
                D = D + ("V" + Y + " ");
            } else {
                D = D + ("v" + Y + " ");                   
            }
			return this;
		}

		public SvgPath CurveTo ( double X1, double Y1, double X2, double Y2, double X, double Y, bool absolute ) {
            if (absolute) {
                D = D + ("C" + X1 + " " + Y1 + " "+ X2 + " " + Y2 + " "+ X + " " + Y + " ");
            } else {
                D = D + ("c" + X1 + " " + Y1 + " "+ X2 + " " + Y2 + " "+ X + " " + Y + " ");                   
            }
			return this;
		}

		public SvgPath SmoothCurveTo ( double X2, double Y2, double X, double Y, bool absolute ) {
            if (absolute) {
                D = D + ("S" + X2 + " " + Y2 + " " + X + " " + Y + " ");
            } else {
                D = D + ("s" + X2 + " " + Y2 + " " + X + " " + Y + " ");                   
            }
			return this;
		}

		public SvgPath QuadraticBeizerCurveTo ( double X1, double Y1, double X, double Y, bool absolute ) {
            if (absolute) {
                D = D + ("Q" + X1 + " " + Y1 + " " + X + " " + Y + " ");
            } else {
                D = D + ("q" + X1 + " " + Y1 + " " + X + " " + Y + " ");                   
            }
			return this;
		}

		public SvgPath SmoothQuadraticBeizerCurveTo ( double X, double Y, bool absolute ) {
            if (absolute) {
                D = D + ("T" + X + " " + Y + " ");
            } else {
                D = D + ("t" + X + " " + Y + " ");                   
            }
			return this;
		}

		public SvgPath EllipticalArcTo ( double RX, double RY, double xAxisRotation, bool largeArcFlag, bool sweepFlag, double X, double Y, bool absolute ) {

            //Sets the boolean ints to 1 if they're anything but zero
            int largeArcFlagInt = largeArcFlag ? 1 : 0;
            int sweepFlagInt = sweepFlag ? 1 : 0;

            if (absolute) {
                D = D + ("A" + RX + " " + RY + " " + xAxisRotation + " " + largeArcFlagInt + " " + sweepFlagInt + " " + X + " " + Y + " ");
            } else {
                D = D + ("a" + RX + " " + RY + " " + xAxisRotation + " " + largeArcFlagInt + " " + sweepFlagInt + " " + X + " " + Y + " ");                   
            }
			return this;
		}

		public SvgPath ClosePath () {
            D = D + ("Z ");
			return this;
        }

		public SvgPath Clear () { 
			D = String.Empty;
			return this;
		}
	}
}
