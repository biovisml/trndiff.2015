// Text.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Svg {

	public class SvgText: SvgDrawingElement {
		public SvgText ( SvgContainer parent, Dictionary<string,string> attributes ) : base( "text", parent, attributes ) {
		}

		public override double Width {
			get {
				return BoundingBox.Width;
			}
			set {
				throw new Exception( "Not implemented");
			}
		}

		public override double Height {
			get {
				return BoundingBox.Height;
			}
			set {
				throw new Exception( "Not implemented" );
			}
		}

		public double X {
			get { return GetAttribute<double>( "x" ); }
			set { SetAttribute( "x", value ); }
		}

		public double Y {
			get { return GetAttribute<double>( "y" ); }
			set { SetAttribute( "y", value ); }
		}

		public double DX {
			get { return GetAttribute<double>( "dx" ); }
			set { SetAttribute( "dx", value ); }
		}

		public double DY {
			get { return GetAttribute<double>( "dy" ); }
			set { SetAttribute( "dy", value ); }
		}

		/// <summary> Get or set the text content of the node.
		/// </summary>

		public string Text {
			get {
				Element firstChild = domElement.FirstChild;

				return firstChild == null ? String.Empty : firstChild.TextContent;
			}
			set { 
				Element firstChild = domElement.FirstChild;

				if ( firstChild == null ) {
					domElement.AppendChild(Document.CreateTextNode(value));
				}
				else { 
					firstChild.TextContent = value;
				}
			}
		}
	}
}
