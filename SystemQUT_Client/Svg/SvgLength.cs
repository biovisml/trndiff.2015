// SvgLength.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	/// <summary> Dummy ISvgLength type used to set length properties.
	/// </summary>

	public class SvgLength: ISvgLength {
		private double value;
		private SvgLengthType lengthType;

		public SvgLength ( double value, SvgLengthType unitType ) {
			this.value = value;
			this.lengthType = unitType;
		}

		public SvgLengthType UnitType {
			get { throw new Exception( "Not implemented" ); }
		}

		public double Value {
			get { throw new Exception( "Not implemented" ); }
		}

		public double ValueInSpecifiedUnits {
			get { throw new Exception( "Not implemented" ); }
		}

		public string ValueAsString {
			get { throw new Exception( "Not implemented" ); }
		}

		public void NewValueSpecifiedInUnits ( SvgLengthType unitType, double valueInspecifiedUnits ) {
			throw new Exception( "Not implemented" );
		}

		public void ConvertToSpecifiedUnits () {
			throw new Exception( "Not implemented" );
		}

		public bool IsEqualTo( ISvgLength other ) {
			return value == other.ValueInSpecifiedUnits && lengthType == other.UnitType;
		}

		public override string ToString () {
			string [] suffices = {
				"",
				"",
				"%",
				"em",
				"ex",
				"px",
				"cm",
				"mm",
				"in",
				"pt",
				"pc",
			};
			return string.Format("{0}{1}", value, suffices[(int) lengthType] );
		}
	}
}
