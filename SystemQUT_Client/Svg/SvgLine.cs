// SvgLine.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	public class SvgLine : SvgDrawingElement {
		public SvgLine ( SvgContainer parent, double x1, double y1, double x2, double y2, Dictionary<string, string> attributes )
			: base( "line", parent, attributes ) {
			X1 = x1; 
			Y1 = y1; 
			X2 = x2; 
			Y2 = y2;
		}

		public double X1 {
			get { return double.Parse(GetAttribute<string>( "x1" ) ); }
			set { SetAttribute( "x1", value ); }
		}

		public double X2 {
			get { return double.Parse( GetAttribute<string>( "x2" ) ); }
			set { SetAttribute( "x2", value ); }
		}

		public double Y1 {
			get { return double.Parse( GetAttribute<string>( "y1" ) ); }
			set { SetAttribute( "y1", value ); }
		}

		public double Y2 {
			get { return double.Parse( GetAttribute<string>( "y2" ) ); }
			set { SetAttribute( "y2", value ); }
		}

		public override double Width {
			get {
				return Math.Abs( X2 - X1 );
			}
			set {
				throw new Exception( "Not supported." );
			}
		}

		public override double Height {
			get {
				return Math.Abs( Y2 - Y1 );
			}
			set {
				throw new Exception( "Not supported." );
			}
		}
	}
}
