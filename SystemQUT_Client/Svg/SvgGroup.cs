// ContainerElement.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Controls;

namespace SystemQut.Svg {

	/// <summary> container element � An element which can have graphics elements and other container elements as child elements.
	///	<para>
	///		Specifically: �a�, �defs�, �glyph�, �g�, �marker�, �mask�, �missing-glyph�, �pattern�, �svg�, �switch� and �symbol�.
	///	</para>
	/// </summary>

	public class SvgGroup : SvgContainer {
		public SvgGroup ( SvgContainer parent, Dictionary<string, string> attributes )
			: base( "g", parent, attributes ) { }
	}
}
