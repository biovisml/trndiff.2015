// ContainerElement.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Controls;

namespace SystemQut.Svg {

	/// <summary> container element � An element which can have graphics elements and other container elements as child elements.
	///	<para>
	///		Specifically: �a�, �defs�, �glyph�, �g�, �marker�, �mask�, �missing-glyph�, �pattern�, �svg�, �switch� and �symbol�.
	///	</para>
	/// </summary>

	public class SvgContainer : SvgElement, IEnumerable<SvgElement> {
		internal readonly List<SvgElement> content = new List<SvgElement>();

		public SvgContainer ( string tag, SvgContainer parent, Dictionary<string, string> attributes )
			: base( null, tag, parent, attributes ) { }

		/// <summary> Adds a SvgElement to the current container.
		/// </summary>
		/// <param name="child"></param>
		
		public void AppendContent ( SvgElement child ) {
			domElement.AppendChild( child.domElement );
			content.Add( child );
		}

		/// <summary> Removes a SvgElement from the current container.
		/// </summary>
		/// <param name="child"></param>

		public void RemoveContent ( SvgElement child ) {
			domElement.RemoveChild( child.domElement );
			content.Remove( child );
		}

		/// <summary> Gets the specified child control by position.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>

		public SvgElement this[int index] {
			get {
				return content[index];
			}
		}

		/// <summary> Gets the number of child controls.
		/// </summary>

		public int Count { get { return content.Length; } }

		/// <summary> Get an enumerator which traverses the content of this container.
		/// </summary>
		/// <returns></returns>

		public IEnumerator<SvgElement> GetEnumerator () {
			return content.GetEnumerator();
		}
	}

	/// <summary> A delegate type which specifies the signature of a callback function for SvgContainer.Iterate.
	/// </summary>
	/// <param name="child"> A reference to the child SvgElement. </param>
	/// <param name="index"> The position in the array. </param>

	public delegate void SvgIterationCallback ( SvgElement child, int index );
}
