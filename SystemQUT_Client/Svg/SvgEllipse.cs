﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {
	public class SvgEllipse : SvgDrawingElement {

		/// <summary> Creates a new Circle having the specified properties.
		/// </summary>
		/// <param name="parent">
		///		An SvgContainer (possibly null) to which the circle will be added.
		/// </param>
		/// <param name="cx">
		///		The x-axis coordinate of the centre of the circle.
		/// </param>
		/// <param name="cy">
		///		The y-axis coordinate of the centre of the circle.
		/// </param>
		/// <param name="radiusX">
		///		The horizontal "radius" of the ellipse.
		/// </param>
		/// <param name="radiusY">
		///		The vertical "radius" of the ellipse.
		/// </param>
		/// <param name="attributes">
		///		A (possibly null) collection of attribute name-value pairs.
		/// </param>

		public SvgEllipse ( SvgContainer parent, double cx, double cy, double radiusX, double radiusY, Dictionary<string, string> attributes )
			: base( "ellipse", parent, attributes ) {
			CX = cx;
			CY = cy;
			RadiusX = radiusX;
			RadiusY = radiusY;
		}

		public double CX {
			get { return GetAttribute<double>( "cx" ); }
			set { SetAttribute( "cx", value ); }
		}

		public double CY {
			get { return GetAttribute<double>( "cy" ); }
			set { SetAttribute( "cy", value ); }
		}

		public double RadiusX {
			get { return GetAttribute<double>( "rx" ); }
			set { SetAttribute( "rx", value ); }
		}

		public double RadiusY {
			get { return GetAttribute<double>( "ry" ); }
			set { SetAttribute( "ry", value ); }
		}

		public override double Height {
			get { return 2 * GetAttribute<double>( "ry" ); }
			set { SetAttribute( "ry", value / 2 ); }
		}

		public override double Width {
			get {
				return 2 * GetAttribute<double>( "rx" );
			}
			set { SetAttribute( "rx", value / 2 ); }
		}
	}
}
