// SvgElement.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using System.Runtime.CompilerServices;
using SystemQut.Controls;

namespace SystemQut.Svg {
	// TODO: replace strings with enumerated or class types.

	public class SvgElement : CodeBehind {

		/// <summary> Creates a new Svg DOM element and wraps it in a SvgElement wrapper.
		/// <para>
		///		The DOM element is bound to this object as a Control/Codebehind pair. 
		/// </para>
		/// </summary>
		/// <param name="domElement"> The optional pre-existing DOM element. </param>
		/// <param name="tag"> The element type. This should be null if the domElement is supplied. </param>
		/// <param name="parent"> The optional parent.  This should be null if the domElement is supplied. </param>
		/// <param name="attributes"> A collection of attribute name-value pairs that will be used to initialise atttributes of the DOM element.  This should be null if the domElement is supplied. </param>

		public SvgElement (
			Element domElement,
			string tag,
			SvgContainer parent,
			Dictionary<string, string> attributes
		)
			: base( domElement == null ? Svg.CreateElement( tag, null, attributes ) : domElement ) {

			if ( parent != null ) {
				parent.AppendContent( this );
			}
		}

		/// <summary> Adds an event listener of the specified type to the element.
		/// </summary>
		/// <param name="eventType"></param>
		/// <param name="eventListener"></param>
		/// <param name="useCapture"></param>

		public void AddGraphicalEventListener (
			GraphicalEventType eventType,
			ElementEventListener eventListener,
			bool useCapture
		) {
			domElement.AddEventListener( (string) (object) eventType, eventListener, useCapture );
		}

		/// <summary> Remove an event listener of the specified type from the element.
		/// </summary>
		/// <param name="eventType"></param>
		/// <param name="eventListener"></param>
		/// <param name="useCapture"></param>

		public void RemoveGraphicalEventListener (
			GraphicalEventType eventType,
			ElementEventListener eventListener,
			bool useCapture
		) {
			domElement.RemoveEventListener( (string) (object) eventType, eventListener, useCapture );
		}

		/// <summary> Adds an event listener of the specified type to the element.
		/// </summary>
		/// <param name="eventType"></param>
		/// <param name="eventListener"></param>
		/// <param name="useCapture"></param>

		public void AddElementEventListener (
			DocumentEventType eventType,
			ElementEventListener eventListener,
			bool useCapture
		) {
			domElement.AddEventListener( (string) (object) eventType, eventListener, useCapture );
		}

		/// <summary> Remove an event listener of the specified type from the element.
		/// </summary>
		/// <param name="eventType"></param>
		/// <param name="eventListener"></param>
		/// <param name="useCapture"></param>

		public void RemoveElementEventListener (
			DocumentEventType eventType,
			ElementEventListener eventListener,
			bool useCapture
		) {
			domElement.RemoveEventListener( (string) (object) eventType, eventListener, useCapture );
		}

		#region SVG Attributes

		public string TransformProperty {
			get { return GetAttribute<string>( "transform" ); }
			set { SetAttribute( "transform", value ); }
		}

		//	Clipping, Masking and Compositing properties:
		public string ClipPath {
			get { return GetAttribute<string>( "clip-path" ); }
			set { SetAttribute( "clip-path", value ); }
		}

		public string ClipRule {
			get { return GetAttribute<string>( "clip-rule" ); }
			set { SetAttribute( "clip-rule", value ); }
		}

		public string Mask {
			get { return GetAttribute<string>( "mask" ); }
			set { SetAttribute( "mask", value ); }
		}

		public double Opacity {
			get { return double.Parse( GetAttribute<string>( "opacity" ) ); }
			set { SetAttribute( "opacity", value ); }
		}

		//	Filter Effects properties:
		public string EnableBackground {
			get { return GetAttribute<string>( "enable-background" ); }
			set { SetAttribute( "enable-background", value ); }
		}
		public string Filter {
			get { return GetAttribute<string>( "predicate" ); }
			set { SetAttribute( "predicate", value ); }
		}
		public string FloodColor {
			get { return GetAttribute<string>( "flood-color" ); }
			set { SetAttribute( "flood-color", value ); }
		}
		public double FloodOpacity {
			get { return double.Parse( GetAttribute<string>( "flood-opacity" ) ); }
			set { SetAttribute( "flood-opacity", value.ToString() ); }
		}
		public string LightingColor {
			get { return GetAttribute<string>( "lighting-color" ); }
			set { SetAttribute( "lighting-color", value ); }
		}
		//	Gradient properties:
		public string StopColor {
			get { return GetAttribute<string>( "stop-color" ); }
			set { SetAttribute( "stop-color", value ); }
		}
		public double StopOpacity {
			get { return double.Parse( GetAttribute<string>( "stop-opacity" ) ); }
			set { SetAttribute( "stop-opacity", value.ToString() ); }
		}
		//	Interactivity properties:
		public string PointerEvents {
			get { return GetAttribute<string>( "pointer-events" ); }
			set { SetAttribute( "pointer-events", value ); }
		}
		//	Color and Painting properties:
		public string ColorInterpolation {
			get { return GetAttribute<string>( "color-interpolation" ); }
			set { SetAttribute( "color-interpolation", value ); }
		}
		public string ColorInterpolationFilters {
			get { return GetAttribute<string>( "color-interpolation-filters" ); }
			set { SetAttribute( "color-interpolation-filters", value ); }
		}
		public string ColorProfile {
			get { return GetAttribute<string>( "color-profile" ); }
			set { SetAttribute( "color-profile", value ); }
		}
		public string ColorRendering {
			get { return GetAttribute<string>( "color-rendering" ); }
			set { SetAttribute( "color-rendering", value ); }
		}
		public string Fill {
			get { return GetAttribute<string>( "fill" ); }
			set { SetAttribute( "fill", value ); }
		}
		public double FillOpacity {
			get { return double.Parse( GetAttribute<string>( "fill-opacity" ) ); }
			set { SetAttribute( "fill-opacity", value.ToString() ); }
		}
		public string FillRule {
			get { return GetAttribute<string>( "fill-rule" ); }
			set { SetAttribute( "fill-rule", value ); }
		}
		public string ImageRendering {
			get { return GetAttribute<string>( "image-rendering" ); }
			set { SetAttribute( "image-rendering", value ); }
		}
		public string Marker {
			get { return GetAttribute<string>( "marker" ); }
			set { SetAttribute( "marker", value ); }
		}
		public string MarkerEnd {
			get { return GetAttribute<string>( "marker-end" ); }
			set { SetAttribute( "marker-end", value ); }
		}
		public string MarkerMid {
			get { return GetAttribute<string>( "marker-mid" ); }
			set { SetAttribute( "marker-mid", value ); }
		}
		public string MarkerStart {
			get { return GetAttribute<string>( "marker-start" ); }
			set { SetAttribute( "marker-start", value ); }
		}

		public string ShapeRendering {
			get { return GetAttribute<string>( "shape-rendering" ); }
			set { SetAttribute( "shape-rendering", value ); }
		}

		/// <summary> The paint used to render the stroke, e.g. "red".
		/// </summary>

		public string Stroke {
			get { return GetAttribute<string>( "stroke" ); }
			set { SetAttribute( "stroke", value ); }
		}

		/// <summary> �stroke-dasharray� controls the pattern of dashes and gaps used to stroke paths. 
		///		&lt;dasharray&gt; contains a list of comma and/or white space separated &lt;length&gt;s and 
		///		&lt;percentage&gt;s that specify the lengths of alternating dashes and gaps. If an odd 
		///		number of values is provided, then the list of values is repeated to yield an even 
		///		number of values. Thus, stroke-dasharray: 5,3,2 is equivalent to stroke-dasharray: 
		///		5,3,2,5,3,2.
		///	<para>
		///		NB: This implementation supports only lengths in user coordinate space, not percentages 
		///		or absolute lengths.
		/// </para>
		/// </summary>

		public double[] StrokeDashArray {
			get {
				string dashString = GetAttribute<string>( "stroke-dasharray" );
				string[] dashArray = dashString.Split( new RegExp( "[ ,]" ) );
				return dashArray.Map( delegate( string s ) { return double.Parse( s ); } );
			}
			set {
				SetAttribute( "stroke-dasharray", value.Join( "," ) );
			}
		}

		/// <summary> �stroke-dashoffset� specifies the distance into the dash pattern to start the dash.
		/// If a &lt;percentage&gt; is used, the value represents a percentage of the current viewport (see Units).
		/// Values can be negative.
		/// <para>
		///		NB: this implementation supports only lengths in user coordinate space, not percentages 
		///		or absolute lengths.
		/// </para>
		/// </summary>

		public double StrokeDashoffset {
			get { return double.Parse( GetAttribute<string>( "stroke-dashoffset" ) ); }
			set { SetAttribute( "stroke-dashoffset", value ); }
		}

		/// <summary> �stroke-linecap� specifies the shape to be used at the end of open subpaths when they are stroked.
		/// </summary>

		public LineCapType StrokeLinecap {
			get { return GetAttribute<LineCapType>( "stroke-linecap" ); }
			set { SetAttribute( "stroke-linecap", value ); }
		}

		/// <summary> �stroke-linejoin� specifies the shape to be used at the corners of paths or basic shapes when they are stroked.
		/// </summary>

		public LineJoinType StrokeLinejoin {
			get { return GetAttribute<LineJoinType>( "stroke-linejoin" ); }
			set { SetAttribute( "stroke-linejoin", value ); }
		}

		/// <summary> When two line segments meet at a sharp angle and miter joins have been specified for �stroke-linejoin�, it is possible
		/// for the miter to extend far beyond the thickness of the line stroking the path. The �stroke-miterlimit� imposes a
		/// limit on the ratio of the miter length to the �stroke-width�. When the limit is exceeded, the join is converted from
		/// a miter to a bevel.
		/// <para>
		/// &lt;miterlimit&gt;
		/// </para>
		/// <para>
		/// The limit on the ratio of the miter length to the �stroke-width�. The value of &lt;miterlimit&gt; must be a &lt;number&gt;
		/// greater than or equal to 1. Any other value is an error (see Error processing).
		/// The ratio of miter length (distance between the outer tip and the inner corner of the miter) to �stroke-width� is
		/// directly related to the angle (theta) between the segments in user space by the formula:
		/// </para>
		/// <para>
		/// miterLength / stroke-width = 1 / sin ( theta / 2 )
		/// </para>
		/// <para>
		/// For example, a miter limit of 1.414 converts miters to bevels for theta less than 90 degrees, a limit of 4.0 converts
		/// them for theta less than approximately 29 degrees, and a limit of 10.0 converts them for theta less than approximately
		/// 11.5 degrees.
		/// </para>
		/// </summary>

		public double StrokeMiterlimit {
			get { return double.Parse( GetAttribute<string>( "stroke-miterlimit" ) ); }
			set { SetAttribute( "stroke-miterlimit", value ); }
		}

		/// <summary> get or set the opacity of the painting operation used to stroke the current object, 
		/// as a &lt;number&gt;. Any values outside
		/// the range 0.0 (fully transparent) to 1.0 (fully opaque) will be clamped to this range. (See Clamping values
		/// which are restricted to a particular range.)
		/// </summary>

		public double StrokeOpacity {
			get { return double.Parse( GetAttribute<string>( "stroke-opacity" ) ); }
			set { SetAttribute( "stroke-opacity", value ); }
		}

		/// <summary>This property specifies the width of the stroke on the current object. If a &lt;percentage&gt; is used, the value represents
		///	a percentage of the current viewport. (See Units.)
		///	A zero value causes no stroke to be painted. A negative value is an error (see Error processing).
		/// </summary>

		public double StrokeWidth {
			get { return double.Parse( GetAttribute<string>( "stroke-width" ) ); }
			set { SetAttribute( "stroke-width", value ); }
		}

		/// <summary> Provide a hint to the implementation about what tradeoffs to make as
		/// it renders text.
		/// </summary>

		public TextRenderingType TextRendering {
			get { return GetAttribute<TextRenderingType>( "text-rendering" ); }
			set { SetAttribute( "text-rendering", value ); }
		}

		// Text properties:

		/// <summary>
		///	This property specifies how an object is aligned with respect to its parent. This property specifies which baseline
		///	of this element is to be aligned with the corresponding baseline of the parent. For example, this allows alphabetic
		///	baselines in Roman text to stay aligned across font size changes. It defaults to the baseline with the same name
		///	as the computed value of the alignment-baseline property. That is, the position of "ideographic" alignment-point
		///	in the block-progression-direction is the position of the "ideographic" baseline in the baseline-table of the object
		///	being aligned.		
		/// </summary>

		public AlignmentBaselineType AlignmentBaseline {
			get { return GetAttribute<AlignmentBaselineType>( "alignment-baseline" ); }
			set { SetAttribute( "alignment-baseline", value ); }
		}

		public string BaselineShift {
			get { return GetAttribute<string>( "baseline-shift" ); }
			set { SetAttribute( "baseline-shift", value ); }
		}
		public string DominantBaseline {
			get { return GetAttribute<string>( "dominant-baseline" ); }
			set { SetAttribute( "dominant-baseline", value ); }
		}
		public string GlyphOrientationHorizontal {
			get { return GetAttribute<string>( "glyph-orientation-horizontal" ); }
			set { SetAttribute( "glyph-orientation-horizontal", value ); }
		}
		public string GlyphOrientationVertical {
			get { return GetAttribute<string>( "glyph-orientation-vertical" ); }
			set { SetAttribute( "glyph-orientation-vertical", value ); }
		}
		public string Kerning {
			get { return GetAttribute<string>( "kerning" ); }
			set { SetAttribute( "kerning", value ); }
		}
		public TextAnchorType TextAnchor {
			get { return GetAttribute<TextAnchorType>( "text-anchor" ); }
			set { SetAttribute( "text-anchor", value ); }
		}
		public string WritingMode {
			get { return GetAttribute<string>( "writing-mode" ); }
			set { SetAttribute( "writing-mode", value ); }
		}
		#endregion

		#region CSS Attributes
		// Font properties:
		public string Font { get { return GetAttribute<string>( "font" ); } set { SetAttribute( "font", value ); } }
		public string FontFamily { get { return GetAttribute<string>( "font-family" ); } set { SetAttribute( "font-family", value ); } }
		public string FontSize { get { return GetAttribute<string>( "font-size" ); } set { SetAttribute( "font-size", value ); } }
		public string FontStyle { get { return GetAttribute<string>( "font-style" ); } set { SetAttribute( "font-style", value ); } }
		public string FontVariant { get { return GetAttribute<string>( "font-variant" ); } set { SetAttribute( "font-variant", value ); } }
		public string FontWeight { get { return GetAttribute<string>( "font-weight" ); } set { SetAttribute( "font-weight", value ); } }

		// Text properties:
		public string Direction { get { return GetAttribute<string>( "direction" ); } set { SetAttribute( "direction", value ); } }
		public string LetterSpacing { get { return GetAttribute<string>( "letter-spacing" ); } set { SetAttribute( "letter-spacing", value ); } }
		public string TextDecoration { get { return GetAttribute<string>( "text-decoration" ); } set { SetAttribute( "text-decoration", value ); } }
		public string WordSpacing { get { return GetAttribute<string>( "word-spacing" ); } set { SetAttribute( "word-spacing", value ); } }

		// Other properties for visual media:
		public string Clip { get { return GetAttribute<string>( "clip" ); } set { SetAttribute( "clip", value ); } }
		public string Color { get { return GetAttribute<string>( "color" ); } set { SetAttribute( "color", value ); } }
		public string Cursor { get { return GetAttribute<string>( "cursor" ); } set { SetAttribute( "cursor", value ); } }
		public string Display { get { return GetAttribute<string>( "display" ); } set { SetAttribute( "display", value ); } }
		public string Overflow { get { return GetAttribute<string>( "overflow" ); } set { SetAttribute( "overflow", value ); } }
		public string Visibility { get { return GetAttribute<string>( "visibility" ); } set { SetAttribute( "visibility", value ); } }
		#endregion

		#region  CodeBehind translation of ISvgLocatable

		// TODO_LATER: Warning! these operations should be bound to more specific types, but right now I lack the time to do so.

		/// <summary> Gets the SvgElement codebehind for the immediately containing viewport element.
		/// </summary>

		public SvgSvg NearestViewportElement {
			get {
				return (SvgSvg) ( (Control) ( (ISvgLocatable) (object) domElement ).NearestViewportElement() ).CodeBehind;
			}
		}

		/// <summary> Gets the code-behind for the outermost svg element that contains this 
		/// </summary>

		public SvgSvg FarthestViewportElement {
			get {
				return (SvgSvg) ( (Control) ( (ISvgLocatable) (object) domElement ).FarthestViewportElement() ).CodeBehind;
			}
		}

		/// <summary> Gets the bounding box of this object.
		/// </summary>

		public ISvgRect BoundingBox {
			get {
				return ( (ISvgLocatable) (object) domElement ).GetBBox();
			}
		}

		/* SVGMatrix */
		public ISvgMatrix CTM {
			get {
				return ( (ISvgLocatable) (object) domElement ).GetCTM();
			}
		}

		/* SVGMatrix */
		public ISvgMatrix ScreenCTM {
			get {
				return ( (ISvgLocatable) (object) domElement ).GetScreenCTM();
			}
		}

		/* SVGMatrix */
		public ISvgMatrix GetTransformToElement ( Element element ) {
			return ( (ISvgLocatable) (object) element ).GetTransformToElement( element );
		}

		#endregion

		#region CodeBehind translation for ISvgElement

		/// <summary> Get a reference to the wrapper for the DOM SvgElement that owns the current element.
		/// </summary>

		public SvgSvg OwnerSvgElement {
			get {
				return (SvgSvg) ( (Control) ( (ISvgElement) (object) domElement ).OwnerSvgElement ).CodeBehind;
			}
		}

		/// <summary> Get a reference to the wrapper for the DOM element that defines the viewport for the current element.
		/// (I _think_ this will be a SvgSvg object, 
		/// </summary>

		public SvgElement ViewportElement {
			get {
				return (SvgElement) ( (Control) ( (ISvgElement) (object) domElement ).ViewportElement ).CodeBehind;
			}
		}
		#endregion
	}
}
