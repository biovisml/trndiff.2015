// GraphicalEventType.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	[ScriptConstants(UseNames=true)]
	public enum DocumentEventType {
		onabort = 0,
		onerror = 1, 
		onresize = 2, 
		onscroll = 3, 
		onunload = 4,
		onzoom = 5
	}
}
