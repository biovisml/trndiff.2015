﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	/// <summary> Enumeration of the valid text anchor values.
	/// <para>NB: ScriptSharp emits these as string literals</para>
	/// </summary>

	[ScriptConstants(UseNames=true)]
	public enum TextAnchorType {
		start = 0,
		middle = 1,
		end = 2
	}
}
