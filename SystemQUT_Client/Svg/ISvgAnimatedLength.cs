﻿using System;
using System.Collections.Generic;

using System.Runtime.CompilerServices;

namespace SystemQut.Svg {

	/// <summary>
	/// interface SVGAnimatedLength {
	///		readonly attribute SVGLength baseVal;
	///		readonly attribute SVGLength animVal;
	///	};
	/// </summary>

	public interface ISvgAnimatedLength {
		[ScriptField]
		ISvgLength BaseVal { get; }

		[ScriptField]
		ISvgLength AnimVal { get; }
	}
}
