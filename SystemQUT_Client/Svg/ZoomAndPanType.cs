﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	/// <summary> Used to specify the zoom-pan behaviour of an outermost SVG element.
	/// </summary>

	[ScriptConstants(UseNames=true)]
	public enum ZoomAndPanType {
		disable = 0,
		magnify = 1
	}
}
