﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	[ScriptConstants( UseNames = true )]
	public enum AlignmentBaselineType {

		/// <summary>
		/// The value is the dominant-baseline of the script to which the character belongs - i.e., use the dominantbaseline of the parent.
		/// </summary>

		auto = 0,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the dominant-baseline of the parent text content element.
		/// </summary>

		baseline = 1,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "before-edge" baseline of the parent text content element.
		/// </summary>

		[ScriptName( "before-edge" )]
		beforeEdge = 2,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "text-before-edge" baseline of the parent text content element.
		/// </summary>

		[ScriptName( "text-before-edge" )]
		textBeforeEdge = 3,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "middle" baseline of the parent text content element.
		/// </summary>

		middle = 4,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "central" baseline of the parent text content element.
		/// </summary>

		central = 5,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "after-edge" baseline of the parent text content element.
		/// </summary>

		[ScriptName( "after-edge" )]
		afterEdge = 6,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "text-after-edge" baseline of the parent text content element.
		/// </summary>

		[ScriptName( "text-after-edge" )]
		textAfterEdge = 7,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "ideographic" baseline of the parent text content element.
		/// </summary>

		ideographic = 8,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "alphabetic" baseline of the parent text content element.
		/// </summary>

		alphabetic = 9,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "hanging" baseline of the parent text content element.
		/// </summary>

		hanging = 10,

		/// <summary>
		/// The alignment-point of the object being aligned is aligned with the "mathematical" baseline of the parent text content element.
		/// </summary>

		mathematical = 11

	}
}
