// ContainerElement.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Svg {

	/// <summary> container element � An element which can have graphics elements and other container elements as child elements.
	///	<para>
	///		Specifically: �a�, �defs�, �glyph�, �g�, �marker�, �mask�, �missing-glyph�, �pattern�, �svg�, �switch� and �symbol�.
	///	</para>
	/// </summary>

	public class SvgSvg : SvgContainer {

		/// <summary> Creates a new svg container, creating a new DOM element which is not yet added to the DOM.
		/// </summary>
		/// <param name="parent">
		///		The (possibly null) parent container. If null, then this element will become a new top-level
		///		svg container. 
		/// </param>
		/// <param name="attributes">
		///		A (possibly null) collection of attribute name-value pairs.
		/// </param>

		public SvgSvg ( SvgContainer parent, Dictionary<string, string> attributes )
			: base( "svg", parent, attributes ) { }

		/// <summary>
		///		The outermost svg element in an SVG document fragment has attribute �zoomAndPan�, which takes the possible
		///		values of disable and magnify, with the default being magnify.
		///		If disable, the user agent shall disable any magnification and panning controls and not allow the user to magnify
		///		or pan on the given document fragment.
		///		If magnify, in environments that support user interactivity, the user agent shall provide controls to allow the
		///		user to perform a "magnify" operation on the document fragment.
		///		If a �zoomAndPan� attribute is assigned to an inner �svg� element, the �zoomAndPan� setting on the inner �svg�
		///		element will have no effect on the SVG user agent.		
		/// </summary>

		public ZoomAndPanType ZoomAndPan {
			get { return GetAttribute<ZoomAndPanType>( "zoomAndPan" ); }
			set { SetAttribute( "zoomAndPan", value ); }
		}

		/// <summary> Get or set the X coordinate of the viewport defined by this element
		///		relative to the containing viewport. This has no effect on an outer svg
		///		element.
		/// </summary>

		public double X {
			get { return (double) Script.Literal( "this._domElement.x.baseVal.value" ); }
			set { SetAttribute( "x", value ); }
		}

		/// <summary> Get or set the X coordinate of the viewport defined by this element
		///		relative to the containing viewport as a SvgLength object rather than user 
		///		coordinate. This has no effect on an outer svg
		///		element.
		/// </summary>

		public ISvgLength XLength {
			get { return (ISvgLength) Script.Literal( "this._domElement.x.baseVal" ); }
			set { SetAttribute( "x", value.ToString() ); }
		}

		/// <summary> Get the animated X coordinate of the viewport defined by this element
		///		relative to the containing viewport as a SvgLength object rather than user 
		///		coordinate. This has no effect on an outer svg
		///		element.
		/// </summary>

		public ISvgLength XAnimLength {
			get { return (ISvgLength) Script.Literal( "this._domElement.x.animVal" ); }
		}

		/// <summary> Get or set the Y coordinate of the viewport defined by this element
		///		relative to the containing viewport. This has no effect on an outer svg
		///		element.
		/// </summary>

		public double Y {
			get { return (double) Script.Literal( "this._domElement.y.baseVal.value" ); }
			set { SetAttribute( "y", value.ToString() ); }
		}

		/// <summary> Get or set the Y coordinate of the viewport defined by this element
		///		relative to the containing viewport as a SvgLength object rather than user 
		///		coordinate. This has no effect on an outer svg
		///		element.
		/// </summary>

		public ISvgLength YLength {
			get { return (ISvgLength) Script.Literal( "this._domElement.y.baseVal" ); }
			set { SetAttribute( "x", value.ToString() ); }
		}

		/// <summary> Get or set the animated Y coordinate of the viewport defined by this element
		///		relative to the containing viewport. This has no effect on an outer svg
		///		element.
		/// </summary>

		public ISvgLength YAnimLength {
			get { return (ISvgLength) Script.Literal( "this._domElement.y.animVal" ); }
		}

		/// <summary> Get or set the base value of this element's animated width property.
		/// </summary>

		public ISvgLength Width {
			get { return (ISvgLength) Script.Literal( "this._domElement.width.baseVal" ); }
			set { SetAttribute( "width", value.ToString() ); }
		}

		/// <summary> Get the animated value of this element's animated width property.
		/// </summary>

		public ISvgLength AnimWidth {
			get { return (ISvgLength) Script.Literal( "this._domElement.width.animVal" ); }
		}

		/// <summary> Get or set the base value of this element's animated height property.
		/// </summary>

		public ISvgLength Height {
			get { return (ISvgLength) Script.Literal( "this._domElement.height.baseVal" ); }
			set { SetAttribute( "height", value ); }
		}

		/// <summary> Get or set the animated value of this element's animated height property.
		/// </summary>

		public ISvgLength AnimHeight {
			get { return (ISvgLength) Script.Literal( "this._domElement.height.animVal" ); }
		}

		/// <summary> Get or set the base value of this objects ViewBox, which defined the local 
		///		coordinate system.
		/// </summary>

		public ISvgRect ViewBox {
			get { return (ISvgRect) Script.Literal( "this._domElement.viewBox.baseVal" ); }
			set { SetAttribute( "viewBox", value.ToString() ); }
		}

		/// <summary> Get or set the animated value of this objects ViewBox, which defined the local 
		///		coordinate system.
		/// </summary>

		public ISvgRect AnimViewBox {
			get { return (ISvgRect) Script.Literal( "this._domElement.viewBox.animVal" ); }
			set { SetAttribute( "viewBox", value.ToString() ); }
		}

		/// <summary> Get or set the PreserveAspectRatio attribute.
		/// </summary>

		public PreserveAspectRatioType PreserveAspectRatio {
			get { return PreserveAspectRatioType.Parse( GetAttribute<string>( "preserveAspectRatio" ) ); }
			set { SetAttribute( "preserveAspectRatio", value.ToString() ); }
		}
	}
}
