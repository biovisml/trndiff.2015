// ViewBox.cs
//

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SystemQut.Svg {

	/// <summary> A "pretend" iSvgRect implementation which can be used to set the viewPort on SvgSvg elements.
	/// </summary>

	public class SvgRect : ISvgRect {
		double x;
		double y;
		double width;
		double height;

		public SvgRect (
			double x,
			double y,
			double width,
			double height
		) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		public override string ToString () {
			return string.Format( "{0} {1} {2} {3}", x, y, width, height );
		}

		public bool IsEqualTo ( ISvgRect other ) {
			return this.x == other.X && this.y == other.Y && this.width == other.Width && this.height == other.Height;
		}

		public double X {
			get { return x; }
		}

		public double Y {
			get { return y; }
		}

		public double Width {
			get { return width; }
		}

		public double Height {
			get { return height; }
		}
	}
}
