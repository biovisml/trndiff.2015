﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	[ScriptConstants( UseNames = true )]
	public enum LineCapType {
		butt = 0, round = 1, square = 2
	}
}
