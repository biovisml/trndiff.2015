﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {
	public enum SvgLengthType {
		Unknown = 0,
		Number = 1,
		Percentage = 2,
		EM = 3,
		EX = 4,
		PX = 5,
		CM = 6,
		MM = 7,
		IN = 8,
		PT = 9,
		PC = 10,
	}
}