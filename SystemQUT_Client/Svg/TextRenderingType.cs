﻿using System;
using System.Collections.Generic;


namespace SystemQut.Svg {

	[ScriptConstants( UseNames = true )]
	public enum TextRenderingType {

		/// <summary> Indicates that the user agent shall make appropriate tradeoffs to 
		///		balance speed, legibility and geometric precision, but with legibility given 
		///		more importance than speed and geometric precision.
		/// </summary>

		auto = 0,

		/// <summary> Indicates that the user agent shall emphasize rendering speed over 
		///		legibility and geometric precision. This option will sometimes cause the 
		///		user agent to turn off text anti-aliasing.
		/// </summary>

		optimizeSpeed = 1,

		/// <summary> Indicates that the user agent shall emphasize legibility over rendering speed and geometric precision. The
		/// user agent will often choose whether to apply anti-aliasing techniques, built-in font hinting or both to produce
		/// the most legible text.
		/// </summary>

		optimizeLegibility = 2,

		/// <summary>
		/// Indicates that the user agent shall emphasize geometric precision over legibility and rendering speed. This
		/// option will usually cause the user agent to suspend the use of hinting so that glyph outlines are drawn with
		/// comparable geometric precision to the rendering of path data.
		/// </summary>

		geometricPrecision = 3
	}
}
