// SvgDrawingElement.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Svg {

	public abstract class SvgDrawingElement: SvgElement {
		public SvgDrawingElement ( string tag, SvgContainer parent, Dictionary<string,string> attributes ): base( null, tag, parent, attributes ) {}

		public abstract double Width { 
			get;
			set;
		}

		public abstract double Height {
			get;
			set;
		}
	}
}
