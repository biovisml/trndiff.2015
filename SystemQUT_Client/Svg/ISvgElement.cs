﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Html;

namespace SystemQut.Svg {


	/// <summary>
	///		All of the SVG DOM interfaces that correspond directly to elements in the SVG language (such as the SVGPathElement
	///		interface for the ‘path’ element) derive from the SVGElement interface.
	/// <example>
	///		interface SVGElement : Element {
	///			attribute DOMString id setraises(DOMException);
	///			attribute DOMString xmlbase setraises(DOMException);
	///			readonly attribute SVGSVGElement ownerSVGElement;
	///			readonly attribute SVGElement viewportElement;
	///		};
	/// </example>
	/// </summary>

	[ScriptImport, ScriptIgnoreNamespace]
	public interface ISvgElement {
		[ScriptField] string Id{ get; }

		[ScriptField] string Xmlbase { get; }

		[ScriptField] Element OwnerSvgElement { get; }

		[ScriptField] Element ViewportElement { get; }
	}
}
