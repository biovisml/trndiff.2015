﻿using System;
using System.Collections.Generic;

namespace SystemQut.Svg {
	public class SvgCircle : SvgDrawingElement {

		/// <summary> Creates a new Circle having the specified properties.
		/// </summary>
		/// <param name="parent">
		///		An SvgContainer (possibly null) to which the circle will be added.
		/// </param>
		/// <param name="cx">
		///		The x-axis coordinate of the centre of the circle.
		/// </param>
		/// <param name="cy">
		///		The y-axis coordinate of the centre of the circle.
		/// </param>
		/// <param name="radius">
		///		the radius of the circle.
		/// </param>
		/// <param name="attributes">
		///		A (possibly null) collection of attribute name-value pairs.
		/// </param>

		public SvgCircle ( SvgContainer parent, double cx, double cy, double radius, Dictionary<string, string> attributes )
			: base( "circle", parent, attributes ) {
			CX = cx;
			CY = cy;
			Radius = radius;
		}

		public double CX {
			get { return GetAttribute<double>( "cx" ); }
			set { SetAttribute( "cx", value ); }
		}

		public double CY {
			get { return GetAttribute<double>( "cy" ); }
			set { SetAttribute( "cy", value ); }
		}

		public double Radius {
			get { return GetAttribute<double>( "r" ); }
			set { SetAttribute( "r", value ); }
		}

		public override double Height {
			get { return 2 * GetAttribute<double>( "r" ); }
			set { SetAttribute( "r", value / 2 ); }
		}

		public override double Width {
			get {
				return 2 * GetAttribute<double>( "r" );
			}
			set { SetAttribute( "r", value / 2 ); }
		}
	}
}
