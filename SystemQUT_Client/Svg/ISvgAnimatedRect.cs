﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SystemQut.Svg {

	// [ScriptImport, ScriptIgnoreNamespace]
	public interface ISvgAnimatedRect {
		[ScriptField]
		ISvgRect BaseVal { get; }

		[ScriptField]
		ISvgRect AnimVal { get; }
	}
}
