﻿using System;
using System.Collections.Generic;

using System.Collections;

namespace SystemQut.ComponentModel {

	/// <summary> Implementation of IEnumerable for lists of SelectItem objects,
	///		which yields the object associated with each SelectItem rather than the 
	///		item itself.
	/// </summary>

	internal class ProjectedList: IEnumerable, IEnumerator {
		private IEnumerator enumerator;
		private Func<object,object> selector;
		
		public ProjectedList (
			IEnumerable list,
			Func<object,object> selector
		) {
			this.enumerator = list.GetEnumerator();	
			this.selector = selector;
		}

		public IEnumerator GetEnumerator () {
			return this;
		}

		public object Current {
			// Note that this has no useful effect due to bug in code generated for foreach .
			get { return enumerator.Current; }
		}

		public bool MoveNext () {
			bool canMove = enumerator.MoveNext();
			
			object currentItem = null;

			if ( canMove ) {
				// work around buggy implementation of foreach in Script Sharp.
				currentItem = selector( Script.Literal( "this._enumerator.current" ) );
			}

			// work around buggy implementation of forreach in Script Sharp.
			Script.Literal( "this.current = currentItem" );

			return canMove;
		}

		public void Reset () {
			enumerator.Reset();
		}
	}
}
