﻿using System;
using System.Collections.Generic;

namespace SystemQut.ComponentModel {
	public enum DataBindingMode {
		OneWay = 1,
		TwoWay = 2,
	}
}
