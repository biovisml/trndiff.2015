// INotifyPropertyChanged.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.ComponentModel {

	public interface INotifyPropertyChanged {
		event PropertyChangedEventHandler PropertyChanged;
	}
}
