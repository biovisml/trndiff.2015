﻿using System;
using System.Collections.Generic;

using System.Collections;

namespace SystemQut.ComponentModel {

	/// <summary> Collection type designed to be used as the items source for data 
	///		bound list controls.
	///	<para>
	///		Observers may subscribe to the CollectionChanged event to be updated on
	///		additions and removals from the collection.
	/// </para>
	/// </summary>

	public class ObservableList : IEnumerable {
		readonly ArrayList items = new ArrayList();

		/// <summary> Initialise this observable list, copying the initial contents
		///		from the supplied array, if it contains data.
		/// </summary>
		/// <param name="initialContent"></param>

		public ObservableList ( Array initialContent ) {
			if ( Script.IsValue( initialContent ) && initialContent.Length > 0 ) {
				AddRange( initialContent );
			}
		}

		public event CollectionChangedHandler CollectionChanged;

		public int Count {
			get { return items.Count; }
		}

		public int Length {
			get { return items.Length; }
		}

		public object this[int index] {
			get {
				return items[index];
			}
		}

		public void Add ( object item ) {
			items.Add( item );

			NotifyCollectionChanged( new object[] { item }, new object[0] );
		}

		public void AddRange ( Array items ) {
			foreach ( object item in items ) this.items.Add( item );

			NotifyCollectionChanged( items.Slice( 0 ), new object[0] );
		}

		public void Remove ( object item ) {
			items.Remove( item );

			NotifyCollectionChanged( new object[0], new object[] { item } );
		}

		public void RemoveAll ( Array items ) {
			foreach ( object item in items ) this.Remove( item );

			NotifyCollectionChanged( new object[0], items.Slice( 0 ) );
		}

		private void NotifyCollectionChanged ( Array itemsAdded, Array itemsRemoved ) {
			if ( CollectionChanged != null ) {
				CollectionChanged( this, new CollectionChangedEventArgs( itemsAdded, itemsRemoved ) );
			}
		}

		public IEnumerator GetEnumerator () {
			return items.GetEnumerator();
		}

		public IArrayLike<T> AsArray<T> () {
			return (IArrayLike<T>) (object) items;
		}
	}
}
