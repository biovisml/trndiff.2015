// PropertyChangedEventArgs.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.ComponentModel {

	public class PropertyChangedEventArgs: EventArgs {
		private string [] propertyNames;

		/// <summary> Gets the full list of changed property names.
		/// </summary>

		public string [] PropertyNames { get{ return this.propertyNames; } }

		/// <summary> Initialises a new PropertyChangedEventArgs object.
		/// </summary>
		/// <param name="propertyNames"> A comma-separated list of property names. 
		///		This caters for the common enough situation where multiple properties
		///		change due to side-effects of a single property set.
		/// </param>

		public PropertyChangedEventArgs (
			string propertyNames
		) {
			this.propertyNames = propertyNames.Split(',');
		}

		/// <summary> Determines if the specified property name is among those notified via the 
		///		argument object.
		/// </summary>
		/// <param name="propertyName">
		///		A property name to test.
		/// </param>
		/// <returns>
		///		True iff <c>propertyNames[0] == propertyName || propertyNames.Contains( propertyName )</c>
		/// </returns>

		public bool Includes ( string propertyName ) {
			return propertyNames[0] == propertyName || propertyNames.Contains( propertyName );
		}
	}
}
