﻿using System;
using System.Collections.Generic;

namespace SystemQut.ComponentModel {
	public class CollectionChangedEventArgs {
		private Array itemsAdded;
		private Array itemsRemoved;

		public CollectionChangedEventArgs (
			Array itemsAdded,
			Array itemsRemoved
		) {
			this.itemsAdded = Script.IsValue( itemsAdded ) ? itemsAdded : new object[0];
			this.itemsRemoved = Script.IsValue( itemsRemoved ) ? itemsRemoved : new object[0];
		}

		public Array ItemsAdded { get { return itemsAdded; } }

		public Array ItemsRemoved { get { return itemsRemoved; } }
	}
}
