﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SystemQut.ComponentModel {

	/// <summary> Presents a memory-efficient filtered, projected view on a collection.
	/// </summary>

	public class FilteredList<ItemType, ResultType> : IEnumerable<ResultType>, IEnumerator<ResultType> {
		private  IEnumerator<ItemType> enumerator;

		private  FilterCallback<ItemType> predicate = AlwaysTrue;
		
		private  SelectionCallback<ItemType,ResultType> selector = Identity;

		private static bool AlwaysTrue ( ItemType item ) { 
			return true;
		}
		
		private static ResultType Identity( ItemType item ) { 
			return (ResultType) (object) item; 
		}

		/// <summary> Initialise the filtered list.
		/// </summary>
		/// <param name="collection">
		///		the collection to be filtered.
		/// </param>
		/// <param name="filter">
		///		A (possibly null) callback which is used to select elements.
		///		If the argument for this parameter is null, all items are selected.
		/// </param>
		/// <param name="selector">
		///		A (possibly null) callback which is used to project from the element 
		///		type to some target type. If the argument supplied for this parameter 
		///		is null then the projection is treated as the identity.
		/// </param>

		public FilteredList (
			IEnumerable<ItemType> collection,
			FilterCallback<ItemType> filter,
			SelectionCallback<ItemType, ResultType> selector
		) {
			this.enumerator = collection.GetEnumerator();
			this.predicate = filter;
			
			if ( selector != null ) {
				this.selector = selector;
			}
		}

		public IEnumerator<ResultType> GetEnumerator () {
			return this;
		}

		/// <summary> NB: Due to bug in generated code for foreach, this method is never called.
		/// </summary>

		public ResultType Current {
			get {
				return selector( enumerator.Current );
			}
		}

		public bool MoveNext () {
			while ( enumerator.MoveNext() ) {
				if ( predicate == null || predicate( enumerator.Current ) ) {
					// Workaround for ssc bug in forach construct.
					Script.Literal( "this.current = this._selector == null ? this._enumerator.current : this._selector( this._enumerator.current )" );
					return true;
				}
			}

			return false;
		}

		public void Reset () {
			enumerator.Reset();
			Script.Literal( "this.current = this._enumerator.current" );

			while ( predicate != null && ( !predicate( enumerator.Current ) ) && enumerator.MoveNext() ) { }
		}
	}
}
