﻿using System;
using System.Collections.Generic;

namespace SystemQut.ComponentModel {

	/// <summary> Callback function used by (among others) DataGrid.Filter.
	/// <para>
	///		The supplied function should return true iff the specified item 
	///		is to remain visible, or false otherwise.
	/// </para>
	/// </summary>
	/// <param name="item">
	///		The item to be examned.
	/// </param>
	/// <returns>
	///		Returns true iff the item passes the matching criteria.
	/// </returns>

	public delegate bool FilterCallback<ItemType>( ItemType item );
}
