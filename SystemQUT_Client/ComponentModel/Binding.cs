// Binding.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.ComponentModel {

	/// <summary> The basis of a simple data-binding mechanism.
	/// <para>
	///		Two binding modes are available. OneWay binding updates the target property 
	///		when the source object is set and (provided that the source implements 
	///		INotifyPropertyChanged) after that whenever the source property value changes. 
	///		TwoWay binding is bidirectional, although to be fully bidirectional the target
	///		must also implement INotifyPropertyChanged. 
	/// </para>
	/// <para>
	///		Note well: binding is achieved via JavaScript's eval() function, so
	///		make sure you trust the origin and content of the source and destination
	///		property names!
	/// </para>
	/// </summary>

	public class Binding {
		private object source;
		private string sourcePropertyName;
		private object target;
		private string targetPropertyName;
		private DataBindingMode mode;
		private string sourceGetExpression;
		private string targetSetExpression;
		private string targetGetExpression;
		private string sourceSetExpression;

		/// <summary> Creates a new Binding from a source to a target.
		/// <para>
		///		After creation, the source and target object references can be changed,
		///		but the other members are immutable. This allows binding mode and properties
		///		to be specified at control creation while deferring the specification of 
		///		source and target - a requirement that occurs when creating controls that 
		///		have a UI component available before their data arrives.
		/// </para>
		/// <para>
		///		Note well: binding is achieved via JavaScript's eval() function, so
		///		make sure you trust the origin and content of the source and destination
		///		property names!
		/// </para>
		/// </summary>
		/// <param name="source">The (possibly null or undefined) </param>
		/// <param name="sourcePropertyName"></param>
		/// <param name="target"></param>
		/// <param name="targetPropertyName"></param>
		/// <param name="mode"></param>

		public Binding (
			object source,
			string sourcePropertyName,
			object target,
			string targetPropertyName,
			DataBindingMode mode
		) {
			this.source = source;
			this.target = target;
			this.mode = mode;

			this.sourcePropertyName = sourcePropertyName;
			string ssSourceProp = String.Format( "{0}{1}", sourcePropertyName.Substr( 0, 1 ).ToLowerCase(), sourcePropertyName.Substr( 1 ) );
			sourceGetExpression = String.Format( "this._source.get_{0} ? this._source.get_{0}() : this._source.{0}", ssSourceProp );
			sourceSetExpression = String.Format( "if ( this._source.set_{0} ) { this._source.set_{0}( value ); } else { this._source.{0} = value; }", ssSourceProp );

			this.targetPropertyName = targetPropertyName;
			string ssTargetProp = String.Format( "{0}{1}", targetPropertyName.Substr( 0, 1 ).ToLowerCase(), targetPropertyName.Substr( 1 ) );
			targetGetExpression = String.Format( "this._target.get_{0} ? this._target.get_{0}() : this._target.{0}", ssTargetProp );
			targetSetExpression = String.Format( "if ( this._target.set_{0} ) { this._target.set_{0}( value ); } else { this._target.{0} = value; }", ssTargetProp );

			if ( source is INotifyPropertyChanged ) {
				( (INotifyPropertyChanged) source ).PropertyChanged += SourcePropertyChanged;
			}

			if ( target is INotifyPropertyChanged && mode == DataBindingMode.TwoWay ) {
				( (INotifyPropertyChanged) target ).PropertyChanged += TargetPropertyChanged;
			}

			UpdateTarget();
		}

		/// <summary> Get or set the binding source.
		/// <para>
		///		Side effect: setting this to a defined value causes the target property to be updated if possible.
		/// </para>
		/// </summary>

		public object Source {
			get { return source; }
			set {
				if ( source is INotifyPropertyChanged ) {
					// stop listening for changes on old source.
					( (INotifyPropertyChanged) source ).PropertyChanged -= SourcePropertyChanged;
				}

				source = value;

				if ( source is INotifyPropertyChanged ) {
					// Listen for changes on new source.
					( (INotifyPropertyChanged) source ).PropertyChanged += SourcePropertyChanged;
				}

				UpdateTarget();
			}
		}

		/// <summary> Get or set the binding target.
		/// <para>
		///		Side effect: setting this to a defined value causes the target property to be updated if possible.
		/// </para>
		/// </summary>

		public object Target {
			get { return target; }
			set {
				if ( target is INotifyPropertyChanged && mode == DataBindingMode.TwoWay ) {
					// stop listening for changes on old source.
					( (INotifyPropertyChanged) target ).PropertyChanged -= TargetPropertyChanged;
				}

				target = value;

				if ( target is INotifyPropertyChanged && mode == DataBindingMode.TwoWay ) {
					// Listen for changes on new source.
					( (INotifyPropertyChanged) target ).PropertyChanged += TargetPropertyChanged;
				}

				UpdateTarget();
			}
		}

		/// <summary> Get the (immutable) target property name.
		/// </summary>

		public string TargetPropertyName {
			get { return targetPropertyName; }
		}

		/// <summary> Get the (immutable) source property name.
		/// </summary>

		public string SourcePropertyName {
			get { return sourcePropertyName; }
		}

		/// <summary> Event handler for property changed on source.
		/// </summary>
		/// <param name="args"></param>
		/// <param name="sender"></param>

		private void SourcePropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			if ( args.PropertyNames.Contains( sourcePropertyName ) ) UpdateTarget();
		}

		/// <summary> Event handler for property changed on target.
		/// </summary>
		/// <param name="args"></param>
		/// <param name="sender"></param>

		private void TargetPropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			if ( args.PropertyNames.Contains( targetPropertyName ) ) UpdateSource();
		}

		/// <summary> Update the target property on the target object, if possible.
		/// <para>
		///		In this context, "possible" means both source and target objects are not null or undefined.
		/// </para>
		/// </summary>

		public void UpdateTarget () {
			if ( !Script.IsValue( source ) ) return;
			if ( !Script.IsValue( target ) ) return;

			// Dynamic code generation to get and set property values.
			object value = Script.Eval( sourceGetExpression );
			Script.Eval( targetSetExpression );
		}

		/// <summary> Returns the source property value from an arbitrary object.
		/// </summary>

		public object GetSourceValue ( object source ) {
				return Script.Eval( sourceGetExpression.Replace("this._source", "source" ) );
		}

		/// <summary> Update the source property of a two-way binding.
		/// <para>
		///		If source or target are not useable references or mode is not TwoWay, nothing is done.
		/// </para>
		/// <para>
		///		Otherwise, the value of the target property assigned to the source property.
		/// </para>
		/// </summary>

		public void UpdateSource () {
			if ( !Script.IsValue( source ) ) return;
			if ( !Script.IsValue( target ) ) return;
			if ( mode != DataBindingMode.TwoWay ) return;

			// Dynamic code generation to get and set property values.
			object value = Script.Eval( targetGetExpression );
			Script.Eval( sourceSetExpression );
		}

		/// <summary> Remove the data binding from the source and target.
		/// </summary>

		public void Dispose () {
			if ( source is INotifyPropertyChanged ) {
				( (INotifyPropertyChanged) source ).PropertyChanged -= SourcePropertyChanged;
			}

			if ( mode == DataBindingMode.TwoWay ) {
				( (INotifyPropertyChanged) target ).PropertyChanged -= TargetPropertyChanged;
			}
		}

		public static Binding Create (
			object source,
			string sourcePropertyName,
			object target,
			string targetPropertyName,
			DataBindingMode mode
		) {
			return new Binding( source, sourcePropertyName, target, targetPropertyName, mode );
		}
	}
}
