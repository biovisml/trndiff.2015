// HtmlUtil.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Controls;
using System.Html.Data.Files;

namespace SystemQut {

	/// <summary> Utility functions for working with the Html DOM. These are for use when you do not want to
	///		depend on another 3rd party library like JQuery, or when you don't trust it yet, like in a unit test.
	/// </summary>

	public static class HtmlUtil {
		private static int uniqueId;

		/// <summary> Sets multiple attributes on an Element, which is returned to permit method chaining.
		/// </summary>
		/// <param name="element"> The element to be updated. </param>
		/// <param name="nameValuePairs"> A collection of attribute names and their associated values. If null, no action is taken. </param>
		/// <returns> Returns the supplied element. </returns>

		public static Element SetAttributes ( Element element, Dictionary<string, string> nameValuePairs ) {
			if ( nameValuePairs == null ) return element;

			foreach ( KeyValuePair<string, string> kvp in nameValuePairs ) {
				element.SetAttribute( kvp.Key, kvp.Value );
			}

			return element;
		}

		/// <summary> Creates an element in the current document, optionally adding it to a parent element, 
		///		and setting a collection of attributes.
		/// </summary>
		/// <param name="tag"> The HTML element tag. </param>
		/// <param name="parent"> An optional parent element. If not null, the new element will be added to the parent element. </param>
		/// <param name="nameValuePairs"> A collection of attribute names and their associated values. If null, no action is taken. </param>
		/// <returns> Returns the newly created element. </returns>

		public static Element CreateElement ( string tag, Element parent, Dictionary<string, string> nameValuePairs ) {
			Element element = SetAttributes( Document.CreateElement( tag ), nameValuePairs );

			if ( parent != null ) parent.AppendChild( element );

			return element;
		}

		/// <summary> Creates an element in the current document in the specified XML namespace, optionally adding it to a parent element, 
		///		and setting a collection of attributes.
		/// </summary>
		/// <param name="nameSpace"></param>
		/// <param name="tag"></param>
		/// <param name="parent"></param>
		/// <param name="nameValuePairs"></param>
		/// <returns></returns>

		public static Element CreateElementNS ( string nameSpace, string tag, Element parent, Dictionary<string, string> nameValuePairs ) {
			Element element = SetAttributes( Document.CreateElementNS( nameSpace, tag ), nameValuePairs );

			if ( parent != null ) parent.AppendChild( element );

			return element;
		}

		/// <summary> Binds a CodeBehind object to its UI element and returns the result as an IControl.
		/// <para>
		///		In addition, we generate and set a unique id for the control. This can subsequently be
		///		used to do such things as create D3 selections which are scoped to the control.
		/// </para>
		/// </summary>
		/// <param name="codeBehind">
		///		The control instance.
		/// </param>
		/// <param name="element">
		///		the UI peer of the control.
		/// </param>
		/// <returns>
		///		Returns the element cast to Control&lt;CodeBehindSubclass&gt;.
		/// </returns>

		public static Control BindControl ( ICodeBehind codeBehind, Element element ) {
			( (Control) element ).CodeBehind = codeBehind;
			return (Control) element;
		}

		/// <summary> Creates a unique id string.
		/// <para>
		///		turns out to bemuch less useful than I thought it would.
		/// </para>
		/// </summary>
		/// <returns></returns>

		public static string GetUniqueId () {
			if ( !Script.IsValue( uniqueId ) ) {
				uniqueId = 0;
			}

			return "_ctl_" + ( uniqueId++ );
		}

		/// <summary> Convenience method to create a button.
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>

		public static InputElement CreateButton ( Element parent ) {
			return (InputElement) HtmlUtil.CreateElement( "input", parent, new Dictionary<string, string>( "type", "button" ) );
		}



		/// <summary> Uses the browser's Download function to save a text document.
		/// </summary>
		/// <param name="fileName">
		///		The suggested name and extension of the downloaded document.
		/// </param>
		/// <param name="text">
		///		The document content.
		/// </param>
		/// <param name="contentType">
		///		The mime type to use, "text/csv" or "text/plain" for example.
		/// </param>

		public static void SaveText ( string fileName, string text, string contentType ) {

			Blob blob = new Blob( new string[] { text }, new Dictionary<string, string>(
				"type", contentType ?? "text/plain"
			) );

			// Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition. 
			Func<Blob, string> getBlobUrl = (Func<Blob, string>) Script.Literal( "(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL" );
			Action<string> revokeBlobUrl = (Action<string>) Script.Literal( "(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL" );

			string url = getBlobUrl( blob );
			string actualFileName = fileName ?? "Untitled.txt";

			if ( (bool) Script.Literal( "!! window.navigator.msSaveBlob" ) ) {
				// Internet Explorer... very clean.
				Script.Literal( "window.navigator.msSaveBlob( blob, actualFileName );" );
			}
			else {
				// FireFox, Chrome...
				// Save document... pretty much a hack: 
				//    code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html
				AnchorElement save = (AnchorElement) Document.CreateElement( "a" );
				save.Href = url;
				save.Target = "_blank";
				save.Download = actualFileName;
				save.Style.Display = "none";
				save.InnerHTML = "Click to save!";

				// This is a shim for Firefox.
				string alternateUrl = String.Format( "{0}:{1}:{2}", "text/csv", actualFileName, url );
				Script.Literal( "if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }" );

				// It seemsthat only buttons in the DOM can be clicked, even programmatically, in FF.
				Document.Body.AppendChild( save );

				// This appears to not work in Firefox.
				// MutableEvent evt = Document.CreateEvent( "Event" );
				// evt.InitEvent( "click", true, true );
				// save.DispatchEvent( evt );

				save.Click();

				Script.SetTimeout( delegate() {
					// Wait a while to allow things to be done. Revoking the blob url too soon prevents FF from downloading.
					Document.Body.RemoveChild( save );
					revokeBlobUrl( url );
				}, 10000 );
			}

		}

        //http://www.kirupa.com/html5/get_element_position_using_javascript.htm
        //These functions will retrieve the actual X and Y coordinates of the
        //element passed to them.

        /// <summary> Determines the absolute X coordinate for an element in an
        /// HTML document
		/// </summary>
		/// <param name="element">
		///		The element to determine the X coordinate of
		/// </param>
        /// <returns>The absolute X coordinate of the element</returns>

        public static int GetElementX(Element element) {
            int X = int.MinValue;
            while (element != null) {
                X += (element.OffsetLeft - element.ScrollLeft + element.ClientLeft);
                element = element.OffsetParent;
            }
            return X;
        }

        /// <summary> Determines the absolute Y coordinate for an element in an
        /// HTML document
		/// </summary>
		/// <param name="element">
		///		The element to determine the Y coordinate of
		/// </param>
        /// <returns>The absolute Y coordinate of the element</returns>

        public static int GetElementY(Element element) {
            int Y = int.MinValue;
            while (element != null) {
                Y += (element.OffsetTop - element.ScrollTop + element.ClientTop);
                element = element.OffsetParent;
            }
            return Y;
        }
	}
}
