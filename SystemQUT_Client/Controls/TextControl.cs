// Text.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {

	public class TextControl: CodeBehind {

		/// <summary> Binds the current code behind to a "text" element,
		///		which might be anything for which the TextContent property makes sense to be set.
		///	<para>
		///		If the element supplied is null, then a new element will be created using the non-null tag.
		/// </para>
		/// </summary>
		/// <param name="element"></param>
		/// <param name="tag"></param>

		public TextControl (
			Element element,
			string tag
		) : base( element != null ? element : Document.CreateElement( tag ) ) {

			domElement.ClassList.Add( CSS_CLASS );

		}

		public string Text {
			get { return domElement.TextContent; }
			set { domElement.TextContent = value; }
		}

		public static string CSS_CLASS {
			get {
				return "SystemQUT_Text";
			}
		}
	}
}
