// Text.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {

	public class DataGridNumericCell : DataGridCell {
		private  string format;

		/// <summary> Initialises the cell, saving the format string which will be used to 
		///		present the value.
		/// </summary>
		/// <param name="format"></param>

		public DataGridNumericCell ( string format ) {
			this.format = format;
		}

		/// <summary> Get or set the numeric content of the cell.
		/// </summary>

		public Number Value {
			get { return Number.Parse( domElement.TextContent ); }
			set { domElement.TextContent = String.Format( format, value ); }
		}

		public override string CssClass {
			get { return "SystemQUT_NumericCell"; }
		}
	}
}
