// ButtonSubclasses.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Svg;

namespace SystemQut.Controls {

	internal static class RecordButtonUtil {

		internal static Point[] Bar (
			double yMax,
			double barThickness
		) {
			return new Point[] {
				new Point( 0, 0 ), 
				new Point( 0, yMax ), 
				new Point( barThickness, yMax ), 
				new Point ( barThickness, 0 ),
				new Point( 0, 0 ), 
			 };
		}

		internal static Point[] Arrow (
			double xMax,
			double yMax,
			double arrowStartX,
			double arrowHeadThickness,
			double arrowThickness
		) {
			return new Point[] {
				new Point( arrowStartX, yMax / 2 ), 
				new Point( arrowStartX + yMax / 2, yMax ), 
				new Point( arrowStartX + yMax/2 + arrowHeadThickness, yMax ), 
				new Point( arrowStartX + arrowHeadThickness + arrowThickness / 2, ( yMax + arrowThickness ) / 2 ),
				new Point( xMax, ( yMax + arrowThickness ) / 2),
				new Point( xMax, ( yMax - arrowThickness ) / 2),
				new Point( arrowStartX + arrowHeadThickness + arrowThickness / 2, ( yMax - arrowThickness ) / 2 ),
				new Point( arrowStartX + yMax / 2 + arrowHeadThickness, 0 ), 
				new Point( arrowStartX + yMax / 2, 0 ), 
				new Point( arrowStartX, yMax / 2 ) 
			};
		}
	}

	public class GotoFirstRecordButton : SvgButton {
		public GotoFirstRecordButton ( Element domElement )
			: base( domElement, 15, 12 ) {
		}

		protected override void RenderContent () {
			double barThickness = 3;
			double arrowThickness = 3;
			double arrowHeadThickness = 4;
			double arrowStartX = barThickness + 1;

			Point[] p0 = RecordButtonUtil.Bar( yMax, barThickness );
			SvgPolyLine z0 = new SvgPolyLine( group, p0, null );

			Point[] p1 = RecordButtonUtil.Arrow( xMax, yMax, arrowStartX, arrowHeadThickness, arrowThickness );
			SvgPolyLine z1 = new SvgPolyLine( group, p1, null );
		}
	}

	/// <summary> A button subclass that represents a "go to last record" button ( "&gt;|" ).
	/// </summary>

	public class GotoLastRecordButton : SvgButton {
		public GotoLastRecordButton ( Element domElement )
			: base( domElement, 15, 12 ) {
		}

		protected override void RenderContent () {
			double barThickness = 3;
			double arrowThickness = 3;
			double arrowHeadThickness = 4;
			double arrowStartX = barThickness + 1;

			Point[] p0 = RecordButtonUtil.Bar( yMax, barThickness );
			foreach ( Point p in p0 ) p.X = xMax - p.X;
			SvgPolyLine z0 = new SvgPolyLine( group, p0, null );

			Point[] p1 = RecordButtonUtil.Arrow( xMax, yMax, arrowStartX, arrowHeadThickness, arrowThickness );
			foreach ( Point p in p1 ) p.X = xMax - p.X;
			SvgPolyLine z1 = new SvgPolyLine( group, p1, null );
		}
	}

	/// <summary> A button subclass that represents a "go to last record" button ( "&gt;" ).
	/// </summary>

	public class GotoNextRecordButton : SvgButton {
		public GotoNextRecordButton ( Element domElement )
			: base( domElement, 15, 12 ) {
		}

		protected override void RenderContent () {
			double arrowThickness = 3;
			double arrowHeadThickness = 4;
			double arrowStartX = 0;
			double barThickness = 3;

			// Draw the left-pointing arrow starting with the tip flush with the left-hand side of the drawing.
			// Then flip it horizontally and move it over by half the offset to centre it. 
			Point[] p1 = RecordButtonUtil.Arrow( xMax - barThickness - 1, yMax, arrowStartX, arrowHeadThickness, arrowThickness );
			foreach ( Point p in p1 ) p.X = xMax - p.X - (barThickness + 1) / 2;
			SvgPolyLine z1 = new SvgPolyLine( group, p1, null );
		}
	}

	/// <summary> A button subclass that represents a "go to first record" button ( "&lt;" ).
	/// </summary>

	public class GotoPreviousRecordButton : SvgButton {
		public GotoPreviousRecordButton ( Element domElement )
			: base( domElement, 15, 12 ) {
		}

		protected override void RenderContent () {
			double arrowThickness = 3;
			double arrowHeadThickness = 4;
			double arrowStartX = 0;
			double barThickness = 3;

			// Draw the left-pointing arrow starting with the tip flush with the left-hand side of the drawing.
			// Then move it over by half the offset to centre it. 
			Point[] p1 = RecordButtonUtil.Arrow( xMax - barThickness - 1, yMax, arrowStartX, arrowHeadThickness, arrowThickness );
			foreach ( Point p in p1 ) p.X = p.X + (barThickness + 1) / 2;
			SvgPolyLine z1 = new SvgPolyLine( group, p1, null );
		}
	}
}
