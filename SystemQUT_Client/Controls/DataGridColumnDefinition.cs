﻿using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;
using System.Html;

namespace SystemQut.Controls {
	/// <summary> Base class for DataGrid column definitions.
	/// </summary>

	public abstract class DataGridColumnDefinition : CodeBehind {
		private DataGrid dataGrid;
		string sourcePropertyName;
		DataBindingMode bindingMode;

		public DataGridColumnDefinition (
			Element domElement
		)
			: base( domElement != null ? domElement : domElement = Document.CreateElement( "td" ) ) {

			domElement.AddEventListener( "click", HeadingClicked, false );
			domElement.Style.Cursor = "pointer";
		}

		public string SourcePropertyName { get { return sourcePropertyName; } }

		public DataBindingMode BindingMode { get { return bindingMode; } }

		protected override void SetUpBinding (
			string sourcePropertyName,
			string targetPropertyName,
			DataBindingMode bindingMode
		) {
			if ( !string.IsNullOrWhiteSpace( targetPropertyName ) ) throw new Exception( INVALID_TARGET_PROPERTY );

			this.bindingMode = bindingMode;
			this.sourcePropertyName = sourcePropertyName;

		}


		public static string INVALID_TARGET_PROPERTY {
			get { return "DataGrid column data-binding should not specify the name of the target property."; }
		}


		public DataGrid DataGrid {
			get { return dataGrid; }
			set { dataGrid = value; }
		}

		/// <summary> Defined in a child class to create the cell.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="sourcePropertyName"></param>
		/// <param name="dataBindingMode"></param>
		/// <returns></returns>

		abstract public DataGridCell CreateCell ( object item, string sourcePropertyName, DataBindingMode dataBindingMode );
		
		/// <summary> Ranks two rows by the value displayed in the current cell.
		/// <para>
		///		This is done by using the binding to pull out the source property values, and comparing those values.
		/// </para>
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>

		abstract public int OrderBy( object x, object y );

		/// <summary> When the heading cell is clicked, this handler asks the datagrid to reorder by the
		/// associated comparison callback.
		/// </summary>
		/// <param name="args"></param>

		protected void HeadingClicked ( ElementEvent args ) {
			dataGrid.OrderBy = OrderBy;
		}
	}
}
