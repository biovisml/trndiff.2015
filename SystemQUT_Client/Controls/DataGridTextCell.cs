// Text.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {

	/// <summary> A data grid cell that holds unformatted text.
	/// </summary>

	public class DataGridTextCell: DataGridCell {

		/// <summary> Get or set the text content of the cell.
		/// </summary>

		public string Text {
			get { return domElement.TextContent; }
			set { domElement.TextContent = value; }
		}

		/// <summary> Gets the Css class used to decorate the cell.
		/// </summary>

		public override string CssClass {
			get { return "SystemQUT_TextCell"; }
		}
	}
}
