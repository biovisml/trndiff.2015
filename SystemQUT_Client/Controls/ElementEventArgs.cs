// ElementEventArgs.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {

	public class ElementEventArgs : EventArgs {
		private ElementEvent elementEvent;

		public ElementEventArgs ( ElementEvent elementEvent ) {
			this.elementEvent = elementEvent;
		}

		public ElementEvent ElementEvent { get { return this.elementEvent; } }
	}
}
