// DataGridTextColumnDefinition.cs
//

using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;
using System.Html;

namespace SystemQut.Controls {

	public class DataGridTextColumn : DataGridColumnDefinition {
		private  Binding binding;

		public DataGridTextColumn ( Element domElement )
			: base( domElement != null ? domElement : domElement = Document.CreateElement( "td" ) ) { }

		public override DataGridCell CreateCell (
			object item,
			string sourcePropertyName,
			DataBindingMode dataBindingMode ) {

			// TODO: Create a TextBox for two-way binding 

			DataGridTextCell text = new DataGridTextCell();
			binding = text.Bind( sourcePropertyName, "Text", DataBindingMode.OneWay );
			text.DataContext = item;
			return text;
		}

		public override int OrderBy ( object x, object y ) {
			string xValue = (string) binding.GetSourceValue( x );
			string yValue = (string) binding.GetSourceValue( y );
			return string.Compare( xValue, yValue, true );
		}

	}
}
