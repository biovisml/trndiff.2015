﻿using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {
	
	public abstract class DataGridCell: CodeBehind {

		/// <summary> Create a new table cell to hold the displayed content.
		/// <para>
		///		The CssClass is used to decorate the element.
		/// </para>
		/// </summary>
		public DataGridCell () : base ( Document.CreateElement( "td" ) ) {
			domElement.ClassList.Add( CssClass );
		}

		/// <summary> Get the CSS class that will be used to decorate the cell.
		/// </summary>

		public abstract string CssClass {
			get;
		}
	}
}
