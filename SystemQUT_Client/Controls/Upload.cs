// Upload.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using System.Html.Data.Files;

namespace SystemQut.Controls {

	/// <summary> Represents a file selector widget.
	/// <para>
	///		When the user changes the selected collection of files, NotifyPropertychanged("Files") is
	///		triggered.
	/// </para>
	/// </summary>

	public class Upload: CodeBehind {

		/// <summary> Attach a codebehind to a (possibly null) input element.
		/// <para>
		///		If the supplied element is null then a new intput element of type file will
		///		be created.
		/// </para>
		/// </summary>
		/// <param name="domElement">
		///		The (possibly null) pre-existing DOM element to which this CodeBehind will be bound.
		/// </param>

		public Upload ( Element domElement ): base ( 
			domElement == null ? domElement = HtmlUtil.CreateElement( "input", null, new Dictionary<string,string>( "type", "file" ) ) : domElement
		) {
			domElement.AddEventListener( "change", SelectionChanged, false );
		}

		/// <summary> Notifies any PropertyChanged listeners that the selected file 
		///		collection has changed.
		/// </summary>
		/// <param name="args"></param>

		private void SelectionChanged( ElementEvent args ) {
			NotifyPropertyChanged( "Files" ); 
		}

		/// <summary> Gets the list of currently selected files.
		/// </summary>

		public FileList Files {
			get {
				return ((FileInputElement) domElement).Files;
			}
		}
	}
}
