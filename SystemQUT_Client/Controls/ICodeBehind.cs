// ICodeBehind.cs
//

using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;
using System.Html;

namespace SystemQut.Controls {


	/// <summary> Interface which represents a code-behind object bound to a
	///		UI element.
	/// </summary>

	public interface ICodeBehind {

		/// <summary> Gets the UI peer of the control.
		/// </summary>

		Element DomElement { get; }

		/// <summary> Get or set the control's name, which should be unique within the
		///		immediate parent control. This is used primarily to match up child controls 
		///		defined in markup within composite controls.
		///	<para>
		///		In markup, this corresponds to the "data-name" attribute.
		/// </para>
		/// </summary>

		string Name { get; set; }

		/// <summary> Get or set the data binding object source.
		/// </summary>

		object DataContext { get; set; }

		/// <summary> Creates a binding between a property of the data context and 
		///		the specified property of this object.
		/// </summary>
		/// <param name="sourcePropertyName"></param>
		/// <param name="targetPropertyName"></param>
		/// <param name="mode"></param>

		Binding Bind ( string sourcePropertyName, string targetPropertyName, DataBindingMode mode );

		/// <summary> Adds the DOM element to the specified container.
		/// <para>
		///		Override this method to do transition effects.		
		/// </para>
		/// </summary>
		/// <param name="container">
		///		The container into which the element is to be added.
		/// </param>

		void AddElementTo( Element container );

		/// <summary> Removes the DOM element its current container.
		/// <para>
		///		Override this method to do transition effects.		
		/// </para>
		/// </summary>

		void RemoveElement();

		/// <summary> Searches the current visual tree to locate the first control having the specified 
		///		HTML5 "data-name" attribute.
		/// </summary>
		/// <param name="controlName">
		///		The name of the desired control.
		///	</param>
		/// <returns>
		///		Returns a referenceto the codebehind of the highest child element having the
		///		specified data-name attribute.
		/// </returns>

		ICodeBehind FindChildElement ( string controlName );
	}
}
