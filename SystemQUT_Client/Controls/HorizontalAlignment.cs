﻿using System;
using System.Collections.Generic;

namespace SystemQut.Controls {
	public enum HorizontalAlignment {
		Left = 0,
		Center = 1,
		Right = 2
	}
}
