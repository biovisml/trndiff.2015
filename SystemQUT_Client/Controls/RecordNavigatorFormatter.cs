// RecordNavigatorFormatter.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Controls {


	/// <summary> Delegate which produces a textual representation of the current state of a 
	///		RecordNavigator.
	/// </summary>
	/// <param name="format">
	///		The format string currently in use.
	/// </param>
	/// <param name="current">
	///		The (possibly undefined) current value.
	/// </param>
	/// <param name="min">
	///		The minimum legal defined value for current.
	/// </param>
	/// <param name="max">
	///		The maximum legal defined value for current. 
	/// </param>
	/// <returns>
	///		Returns a string, possibly looking like "Record 1 of 32"
	/// </returns>

	public delegate string RecordNavigatorFormatter ( string format, int current, int min, int max );
}
