// Point.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Controls {

	public class Point {
		private double x;
		private double y;

		public double X {
			get {
				return x;
			}
			set {
				if ( !Script.IsValue( x ) ) throw new Exception( "Point.x must be defined." );
				this.x = value;
			}
		}

		public double Y {
			get {
				return y;
			}
			set {
				if ( !Script.IsValue( y ) ) throw new Exception( "Point.y must be defined." );
				this.y = value;
			}
		}

		public Point ( double x, double y ) {
			X = x;
			Y = y;
		}

		public override string ToString () {
			return string.Format( "{0}, {1}", x, y );
		}
	}
}
