// Text.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {

	public class DataGridCheckBoxCell : DataGridCell {
		private CheckBox checkBox;

		/// <summary> Creates a cell containing a ceheckbox.
		/// </summary>

		public DataGridCheckBoxCell () {
			checkBox = new CheckBox( null );
			checkBox.AddElementTo( domElement );
		}

		/// <summary> Get a reference to the checkbox.
		/// </summary>

		public CheckBox CheckBox { get { return checkBox; } }

		/// <summary> Gets the CSS label used to 
		/// </summary>

		public override string CssClass {
			get { return "SystemQUT_CheckBoxCell"; }
		}
	}
}
