// DataGrid.cs
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Html;
using SystemQut.ComponentModel;
using SystemQut.Linq;

namespace SystemQut.Controls {

	public class DataGrid : CodeBehind {
		protected readonly List<DataGridColumnDefinition> columns = new List<DataGridColumnDefinition>();

		/// <summary> The list of items. provide by the user.
		/// </summary>

		protected ObservableList itemsSource;

		/// <summary> Internal list used to support sorting and filtering in the display
		/// </summary>

		protected readonly List<object> items = new List<object>();

		/// <summary> A list containing the current row collection for the data grid.
		/// </summary>

		protected readonly List<DataGridRow> rows = new List<DataGridRow>();

		/// <summary> The table row element that is used to render the data grid.
		/// </summary>

		protected Element header;

		/// <summary> The tbody element that contains the data children.
		/// </summary>

		protected Element tbody;

		/// <summary> The thead element that contains the column header row.
		/// </summary>

		protected Element thead;

		/// <summary> A customisable filter function which restricts the displayed items.
		/// </summary>

		protected FilterCallback<object> filter = null;

		/// <summary> A customisable ranking function for sorting the data.
		/// </summary>

		protected CompareCallback<object> orderBy = null;

		private const string heading_css_class_attr = "data-heading-css-class";
		private const string row_css_class_attr = "data-row-css-class";
		private const string alternate_row_css_class_attr = "data-alternate-row-css-class";
		private const string isMultiselect_attr = "data-is-multi-select";
		private DataGridRow selectedRow = null;
		private bool suspendNotify = false;
		private bool somethingChanged = false;
		private Fuzzy sortAscending = Fuzzy.Maybe;

		/// <summary> Create a new data grid.
		/// <para>
		///		If the domElement argument is null, an empty datagrid is created backed by an 
		///		empty table.
		/// </para>
		/// <para>
		///		If the domElement argument is not null then it should be a HTML table element. 
		///		The element is bound to this codebehind and datagrid settings are obtained as 
		///		far as possible by scanning the element and its children.
		/// </para>
		/// <para>
		///		Settings may be defined via HTML5 data attributes, or to a limited extent by
		///		example controls in the template. HTML5 attributes must be defined on the
		///		top-level table element. Certain (limited) other values are inferred from sample
		///		children inserted into the visual element by the control designer.
		/// </para>
		/// <para>
		///		At the current time, the only quantities that are inferred are the CSS class name
		///		to be used for the heading row, normal children, and alternating children. These are obtained by 
		/// </para>
		/// </summary>
		/// <param name="domElement">
		///		The (possibly) null template from which the datagrid settings will be inferred, and which 
		///		will be used to display the tabular data.
		/// </param>

		public DataGrid ( Element domElement )
			: base( domElement != null ? domElement : domElement = Document.CreateElement( "tr" ) ) {

			DetectColumns();
		}

		/// <summary> Gets the list of column definitions.
		/// <para>
		///		This is can be used to allow addition or removal of columns from an existing DataGrid. 
		/// </para>
		/// <para>
		///		Note well: if you change the list of columns you will need to call Refresh() to see the 
		///		results.
		/// </para>
		/// </summary>

		public List<DataGridColumnDefinition> Columns { get { return columns; } }

		/// <summary> A list of items that will be displayed by this data grid.
		/// </summary>

		public ObservableList ItemsSource {
			get {
				return this.itemsSource;
			}
			set {
				if ( Objects.Identical( itemsSource, value ) ) return;

				sortAscending = Fuzzy.Maybe;

				if ( itemsSource != null ) {
					itemsSource.CollectionChanged -= ItemsChanged;
				}

				itemsSource = value;

				if ( itemsSource != null ) {
					itemsSource.CollectionChanged += ItemsChanged;
				}

				Refresh();

				NotifyPropertyChanged( "ItemsSource" );
			}
		}

		/// <summary> Get or set the name of the CSS class used to style the header row.
		///	<para>
		///		This is backed by an attribute on the DOM element. On initialisation, if the 
		///		attribute is not present it will be inferred (if possible) from the first row 
		///		in the table.
		/// </para>
		/// </summary>

		public string HeadingRowCssClass {
			get { return GetAttribute<string>( HEADING_CSS_CLASS_ATTR ); }
			set {
				SetAttribute( HEADING_CSS_CLASS_ATTR, value );

				if ( header != null ) header.ClassName = value;
			}
		}

		/// <summary> Get or set the name of the CSS class used to style even-numbered children, 
		///		or if AlternateRowCssClass is not set, all children.
		/// </summary>

		public string RowCssClass {
			get { return GetAttribute<string>( ROW_CSS_CLASS_ATTR ); }
			set { SetAttribute( ROW_CSS_CLASS_ATTR, value ); }
		}

		/// <summary> Get or set the name of the CSS class used to style odd-numbered children.
		/// </summary>

		public string AlternateRowCssClass {
			get { return GetAttribute<string>( ALTERNATE_ROW_CSS_CLASS_ATTR ); }
			set { SetAttribute( ALTERNATE_ROW_CSS_CLASS_ATTR, value ); }
		}

		/// <summary> Get or set the delegate that is used to filter the visible rows.
		/// <para>
		///		Setting this to a new value causes the visual content of the data grid to
		///		be refreshed.
		/// </para>
		/// </summary>

		public FilterCallback<object> Filter {
			get { return filter; }
			set {
				if ( (object) filter == (object) value ) return;

				filter = value;
				RefreshBody();
			}
		}

		/// <summary> Examines the column definitions in the header row of the template (if it exists)
		///		to obtain and 
		/// </summary>

		private void DetectColumns () {
			ElementCollection children = domElement.GetElementsByTagName( "thead" );
			thead = children.Length > 0 ? children[0] : null;

			children = domElement.GetElementsByTagName( "tbody" );
			tbody = children.Length > 0 ? children[0] : null;

			children = domElement.GetElementsByTagName( "tr" );
			header = children.Length > 0 ? children[0] : null;

			Element firstRow = children.Length > 1 ? children[1] : null;
			Element secondRow = children.Length > 2 ? children[2] : null;

			if ( header == null ) return;

			for ( int i = 0; i < header.ChildNodes.Length; i++ ) {
				DataGridColumnDefinition columnDefinition = (DataGridColumnDefinition) ( (Control) header.ChildNodes[i] ).CodeBehind;

				if ( !( columnDefinition is DataGridColumnDefinition ) ) continue;

				columns.Add( columnDefinition );
				columnDefinition.DataGrid = this;
			}

			// Infer CSS classes.
			if ( string.IsNullOrWhiteSpace( HeadingRowCssClass ) ) HeadingRowCssClass = header.ClassName;

			if ( string.IsNullOrWhiteSpace( RowCssClass ) && firstRow != null ) RowCssClass = firstRow.ClassName;

			if ( string.IsNullOrWhiteSpace( AlternateRowCssClass ) && secondRow != null ) AlternateRowCssClass = secondRow.ClassName;

			if ( string.IsNullOrWhiteSpace( AlternateRowCssClass ) ) AlternateRowCssClass = RowCssClass;
		}

		/// <summary> Renders the table, replacing all previsouly existing visual content.
		/// </summary>

		public void Refresh () {
			RefreshHeader();
			RefreshBody();
		}

		/// <summary> Refreshes the content of the displayed heading row, creating it from scratch
		///		if it does not already exist.
		///	<para>
		///		Note that this removes all existing content from the header and replaces it with a 
		///		the current list of column definition DOM elements. 
		/// </para>
		/// </summary>

		private void RefreshHeader () {
			if ( thead == null ) thead = HtmlUtil.CreateElement( "thead", domElement, null );

			if ( header == null ) {
				header = HtmlUtil.CreateElement( "tr", thead, null );
				string headerClass = HeadingRowCssClass;

				if ( !string.IsNullOrWhiteSpace( headerClass ) ) header.ClassName = headerClass;
			}
			else {
				while ( header.FirstChild != null ) header.RemoveChild( header.FirstChild );
			}

			foreach ( DataGridColumnDefinition col in columns ) {
				col.AddElementTo( header );
			}
		}

		/// <summary> Refreshes the content of the table.
		/// <para>
		///		It the present iteration, table row elements are create for every non-filtered
		///		item in the collection.
		/// </para>
		/// <para>
		///		RefreshBody is made virtual to open the way for subclasses to improve on this 
		///		behaviour as necessary.
		/// </para>
		/// </summary>

		protected void RefreshBody () {
			if ( tbody == null ) {
				tbody = HtmlUtil.CreateElement( "tbody", domElement, null );
			}

			while ( tbody.FirstChild != null ) {
				tbody.RemoveChild( tbody.FirstChild );
			}

			object currentSelectedItem = selectedRow != null ? selectedRow.Item : null;

			selectedRow = null;

			items.Clear();
			rows.Clear();

			if ( itemsSource == null ) return;

			foreach ( object item in itemsSource ) {
				items.Add( item );
			}

			if ( orderBy != null && sortAscending != Fuzzy.Maybe ) {
				if ( sortAscending == Fuzzy.Yes ) {
					items.Sort( orderBy );
				}
				else {
					items.Sort( delegate( object x, object y ) { return -orderBy( x, y ); } );
				}
			}

			string alternateClassName = AlternateRowCssClass;
			string rowClassName = RowCssClass;

			FilteredList<object,object> filteredList = new FilteredList<object,object>( items, filter, null );
			int index = 0;

			foreach ( object item in filteredList ) {
				DataGridRow row = new DataGridRow( item, index++, columns, rowClassName, alternateClassName );
				rows.Add( row );
				row.AddElementTo( tbody );

				if ( currentSelectedItem != null && currentSelectedItem == item ) {
					row.IsSelected = true;
					selectedRow = row;
				}

				row.PropertyChanged += row_PropertyChanged;
			}

			if ( currentSelectedItem != null ) {
				NotifyPropertyChanged("SelectedIndex");
			}
		}

		/// <summary> Listens for IsSelected property changes on the datagrid rows.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>

		void row_PropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			DataGridRow row = sender as DataGridRow;
			bool rowSelected = row.IsSelected;

			if ( suspendNotify ) {
				somethingChanged = true;
				return;
			}

			if ( !IsMultipleSelection ) {
				// keep track of the index of the current selected item.
				// If the row is unselected and it isn't the current selected row no further action is needed.
				// If the row is selected and it is the currently selected row, no further action is needed.

				if ( rowSelected && selectedRow != row ) {
					// A new selection has been made.
					DataGridRow oldSelectedRow = selectedRow;
					selectedRow = row;

					if ( oldSelectedRow != null ) {
						oldSelectedRow.IsSelected = false;
					}

					NotifyPropertyChanged( "SelectedIndex,SelectedItem" );
				}
				else if ( ( !rowSelected ) && selectedRow == row ) {
					// The current selection has become unselected.
					selectedRow = null;
					NotifyPropertyChanged( "SelectedIndex,SelectedItem" );
				}
			}
			else {
				// SelectedItems looks in the row to see which ones are selcted.
				NotifyPropertyChanged( "SelectedItems" );
			}
		}

		/// <summary> Event handler for CollectionChanged events on the items source.
		///	<para>
		///		The present implementation does a brute force "refresh" action.
		/// </para>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>

		private void ItemsChanged ( object sender, CollectionChangedEventArgs args ) {
			RefreshBody();
		}

		/// <summary> Get the name of the attribute used to set the CSS class of the heading row.
		/// </summary>

		public static string HEADING_CSS_CLASS_ATTR {
			get {
				return heading_css_class_attr;
			}
		}

		/// <summary> Get the name of the attribute used to set the CSS class of table rows.
		/// </summary>

		public static string ROW_CSS_CLASS_ATTR {
			get {
				return row_css_class_attr;
			}
		}

		/// <summary> Get the name of the attribute used to set the CSS class of the alternating table rows.
		/// </summary>

		public static string ALTERNATE_ROW_CSS_CLASS_ATTR {
			get {
				return alternate_row_css_class_attr;
			}
		}

		/// <summary> Get the name of the attribute used to set the CSS class of the alternating table rows.
		/// </summary>

		public static string IS_MULTISELECT_ATTR {
			get {
				return isMultiselect_attr;
			}
		}

		/// <summary> Gets a list of visible items.
		/// </summary>

		public IEnumerable<object> VisibleItems {
			get {
				return new FilteredList<object,object>( items, filter, null );
			}
		}

		/// <summary> Get or set the selected items.
		/// </summary>

		public Array SelectedItems {
			get {
				ArrayList selectedItems = new ArrayList();

				foreach ( DataGridRow row in rows ) {
					if ( row.IsSelected ) selectedItems.Add( row.Item );
				}

				return (Array) selectedItems;
			}
			set {
				SuspendNotify = true;

				if ( IsMultipleSelection ) {
					foreach ( DataGridRow row in rows ) {
						row.IsSelected = items.Contains( row.Item );
					}
				}
				else {
					foreach ( DataGridRow row in rows ) {
						row.IsSelected = items[0] == row.Item;
					}
				}

				SuspendNotify = false;
				if ( somethingChanged ) NotifyPropertyChanged( "SelectedItems" );
			}
		}

		public bool IsMultipleSelection {
			get {
				return String.Compare( "true", GetAttribute<string>( IS_MULTISELECT_ATTR ), true ) == 0;
			}
			set {
				SetAttribute( IS_MULTISELECT_ATTR, Objects.ToString( value, "false" ) );
			}
		}

		public int SelectedIndex {
			get {
				return selectedRow == null ? -1 : selectedRow.Index;
			}
			set {
				if ( value < -1 || value >= rows.Count ) {
					// If invalid index, set to -1.
					value = -1;
				}

				if ( Script.IsValue( selectedRow ) ) {
					if ( selectedRow.Index == value ) return;
				}
				else {
					if ( value == -1 ) return;
				}

				DataGridRow newSelectedRow = value < 0 || !Script.IsValue( value ) ? null : rows[value];

				if ( newSelectedRow == null ) {
					if ( selectedRow != null ) {
						selectedRow.IsSelected = false;
					}
				}
				else {
					newSelectedRow.IsSelected = true;
					// PropertyChanged data flow should take care of the rest.
				}

				// Console.Log( String.Format( "Selected index changed to {0}", SelectedIndex ) );
				NotifyPropertyChanged( "SelectedIndex,SelectedItem" );
			}
		}

		public object SelectedItem {
			get {
				return selectedRow != null ? selectedRow.Item : null;
			}
			set {
				if ( SelectedItem == value ) return;

				SuspendNotify = true;

				foreach ( DataGridRow row in rows ) {
					row.IsSelected = row.Item == value;
				}

				SuspendNotify = false;

				if ( somethingChanged ) NotifyPropertyChanged( "SelectedIndex,SelectedItem" );
			}
		}

		protected bool SuspendNotify {
			get { return suspendNotify; }
			set {
				suspendNotify = value;
				somethingChanged = false;
			}
		}

		public object ItemAt ( int index ) {
			return rows[index].Item;
		}

		public CompareCallback<object> OrderBy {
			get {
				return orderBy;
			}
			set {
				if ( (object) orderBy == (object) value ) {
					sortAscending = sortAscending == Fuzzy.Yes ? Fuzzy.No : Fuzzy.Yes;
				}
				else {
					orderBy = value;
					sortAscending = Fuzzy.Yes;
				}

				RefreshBody();
			}
		}
	}

}
