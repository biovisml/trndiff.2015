// ElementEventHandler.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Controls {

	public delegate void ElementEventHandler ( object sender, ElementEventArgs eventArgs );
}
