﻿using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;
using System.Html;

namespace SystemQut.Controls {

	/// <summary> Base class for control code behind objects.
	/// </summary>

	public class CodeBehind : ICodeBehind, IDisposable, INotifyPropertyChanged {

#pragma warning disable 649
		// This is initialised via a Script literal.
		internal object dataContext;
#pragma warning restore 649

		private List<Binding> bindings;
		internal Element domElement;
		private const string DATA_BINDING = "data-binding";
		private static readonly Dictionary<string, Type> types = new Dictionary<string, Type>();

		/// <summary> Initialise a Codebehind object and bind it to the supplied DOM element.
		/// <para>
		///		If the DOM element is null or undefined, a new div will be created but not 
		///		added to the document.
		/// </para>
		/// </summary>
		/// <param name="domElement">
		///		A (possibly null) DOM element to be attached to the new codebehind.
		/// </param>

		public CodeBehind( Element domElement ) {
			if (!Script.IsValue( domElement )) {
				domElement = Document.CreateElement( "div" );
			}
			this.domElement = domElement;
			HtmlUtil.BindControl( this, domElement );
			DetectDataBindings();
		}

		#region instance members
		/// <summary> Gets a reference to the associated DOM element.
		/// </summary>

		public Element DomElement {
			get {
				return this.domElement;
			}
		}

		/// <summary> Get or set the local name of this control. This is backed by the DOM "data-name"
		///		attribute on the element.
		/// </summary>

		public string Name {
			get {
				return GetAttribute<string>( "data-name" );
			}
			set {
				SetAttribute( "data-name", value );
			}
		}

		/// <summary> Get or set the default binding source.
		///	<para>
		///		Setting a new value will recursively visit nested child controls and 
		///		replace the data context on them as well AS LONG AS THE OLD VALUE MATCHES 
		///		THE OLD VALUE BEING REPLACED IN THE CURRENT OBJECT.   
		/// </para>
		/// </summary>

		public virtual object DataContext {
			get {
				return dataContext;
			}
			set {
				SetChildDataContexts( domElement, dataContext, value );

				dataContext = value;

				if ((bool) (object) bindings) {
					for (int i = 0; i < bindings.Length; i++) {
						bindings[i].Source = value;
					}
				}
			}
		}

		/// <summary> Replaces the DataContext of the codebehind for the supplied DOM element (if
		///			it has a codebehind) plus its child elements.
		///	<para>
		///		Note that child data context is replaced only if it previously matched the old value.
		///		this prevents the data context of child controls which refer to different object being 
		///		trashed.
		/// </para>
		/// </summary>
		/// <param name="element"></param>
		/// <param name="oldValue"></param>
		/// <param name="newValue"></param>

		protected static void SetChildDataContexts( Element element, object oldValue, object newValue ) {

			for (int i = 0; i < element.ChildNodes.Length; i++) {
				Element child = element.ChildNodes[i];

				if ((bool) Script.Literal( "child.__codebehind__" )) {
					CodeBehind codeBehind = (CodeBehind) Script.Literal( "child.__codebehind__" );
					if (codeBehind.dataContext == oldValue) {
						codeBehind.DataContext = newValue;
					}
				}

				SetChildDataContexts( child, oldValue, newValue );
			}
		}

		/// <summary> Creates a binding between a property of the data context and 
		///		the specified property of this object.
		/// </summary>
		/// <param name="sourcePropertyName"></param>
		/// <param name="targetPropertyName"></param>
		/// <param name="mode"></param>
		/// <exception cref="Exception">
		///		Exception with message containing "Target property already bound" is thrown if there already exists 
		///		a binding to the target property.
		/// </exception>
		/// <returns>
		///		A reference to the newly created binding for further customisation.
		/// </returns>

		public Binding Bind( string sourcePropertyName, string targetPropertyName, DataBindingMode mode ) {
			if (bindings == null) bindings = new List<Binding>();

			for (int i = 0; i < bindings.Length; i++) {
				if (bindings[i].TargetPropertyName == targetPropertyName)
					throw new Exception( TARGET_PROPERTY_ALREADY_BOUND );
			}

			Binding binding = new Binding( dataContext, sourcePropertyName, this, targetPropertyName, mode );

			bindings.Add( binding );

			return binding;
		}

		/// <summary> Locates and parses any data binding definition from the "data-binding" attribute of the 
		///		associated DOM element. The binding obtained in this manner can be overridden later.
		///	<para>
		///		A binding declaration is a semi-colon separated list of comma-separated triples: data-binding="sourceProp,targetProp[,(OneWay|TwoWay)];..."
		/// </para>
		/// <para>
		///		Once the binding information has been parsed, each triple is processed by the
		///		SetUpBinding method. By default SetUpBinding creates a binding on the current
		///		control. However, if you need to do somethign different such as in a 
		///		column definition, you can override SetUpBinding.
		/// </para>
		/// </summary>

		private void DetectDataBindings() {
			string attr = (string) domElement.GetAttribute( DATA_BINDING );

			if (!Script.IsValue( attr ))
				return;

			string[] triples = attr.Split( ';' );

			foreach (string triple in triples) {
				string[] binding = triple.Split( ',' );

				if (binding.Length < 1 || binding.Length > 3)
					throw new Exception( INVALID_BINDING_TRIPLE );

				DataBindingMode mode = DataBindingMode.OneWay;
				string targetProperty = null;
				string sourceProperty = binding[0];

				if (binding.Length >= 2)
					targetProperty = binding[1];

				if (binding.Length == 3 && binding[2] == "TwoWay")
					mode = DataBindingMode.TwoWay;

				SetUpBinding( sourceProperty, targetProperty, mode );
			}
		}

		/// <summary> Sets up a binding based on a (sourceName, targetName, bindingMode) triple.
		/// <para>
		///		By default SetUpBinding creates a binding on the current
		///		control. However, if you need to do somethign different such as in a 
		///		column definition, you can override SetUpBinding.
		///	</para>
		/// </summary>
		/// <param name="sourceProperty"></param>
		/// <param name="targetProperty"></param>
		/// <param name="mode"></param>

		protected virtual void SetUpBinding( string sourceProperty, string targetProperty, DataBindingMode mode ) {
			Bind( sourceProperty, targetProperty, mode );
		}

		/// <summary> Default cleanup function, intended for use before an element is deleted.
		/// <para>
		///		In this you should ensure that all bidirectional connections between Codebehind and Control
		///		have been removed to prevent resource leaks.
		/// </para>
		/// <para>
		///		the default implementation, which sould be calld after custom cleanup,
		///		disposes the bindings then severs the connection between control and codebehind.
		/// </para>
		/// </summary>

		public virtual void Dispose() {
			if (Script.IsValue( bindings )) {
				for (int i = 0; i < bindings.Length; i++) {
					bindings[i].Dispose();
				}
			}

			((Control) domElement).CodeBehind = null;

			if (domElement.ParentNode != null) {
				domElement.ParentNode.RemoveChild( domElement );
			}

			domElement = null;
		}

		/// <summary> Adds the DOM element to the specified container.
		/// <para>
		///		Override this method to do transition effects.		
		/// </para>
		/// </summary>
		/// <param name="container">
		///		The container into which the element is to be added.
		/// </param>

		public virtual void AddElementTo( Element container ) {
			container.AppendChild( domElement );
		}

		/// <summary> Removes the DOM element from its current container.
		/// <para>
		///		Override this method to do transition effects.		
		/// </para>
		/// </summary>

		public virtual void RemoveElement() {
			if (domElement.ParentNode != null)
				domElement.ParentNode.RemoveChild( domElement );
		}

		/// <summary> Locates a child control having the specified name 
		/// </summary>
		/// <param name="controlName"></param>
		/// <returns></returns>

		public ICodeBehind FindChildElement( string controlName ) {
			return CodeBehind.FindChildElementRecursive( controlName, domElement );
		}

		/// <summary>
		/// Recursively searches the DOM sub-tree rooted at the designated element to locate a child eleent 
		/// </summary>
		/// <param name="controlName"></param>
		/// <param name="domElement"></param>
		/// <returns></returns>
		public static ICodeBehind FindChildElementRecursive(
			string controlName,
			Element domElement
		) {
			const string ATTRIBUTE_NAME = "data-name";

			if (domElement.Attributes != null && (string) domElement.GetAttribute( ATTRIBUTE_NAME ) == controlName) {
				return ((Control) domElement).CodeBehind;
			}

			for (int i = 0; i < domElement.ChildNodes.Length; i++) {
				ICodeBehind childResult = FindChildElementRecursive( controlName, domElement.ChildNodes[i] );

				if (childResult != null) {
					return childResult;
				}
			}

			return null;
		}

		/// <summary> Locates the uppermost child control which has a code-behind of the supplied type 
		///		or a subclass.
		/// </summary>
		/// <param name="type">
		///		The type of codebehind to locate
		/// </param>
		/// <returns></returns>

		public ICodeBehind FindChildElementByType( Type type ) {
			return FindChildElementByTypeRecursive( type, domElement );
		}

		private static ICodeBehind FindChildElementByTypeRecursive( Type type, Element domElement ) {
			if (Control.HasCodeBehind( domElement )) {
				ICodeBehind codeBehind = Control.GetCodeBehind( domElement );

				if (type.IsAssignableFrom( codeBehind.GetType() )) {
					return codeBehind;
				}
			}

			for (int i = 0; i < domElement.ChildNodes.Length; i++) {
				ICodeBehind childResult = FindChildElementByTypeRecursive( type, domElement.ChildNodes[i] );

				if (childResult != null) {
					return childResult;
				}
			}

			return null;
		}
		#endregion

		#region static members

		// TODO: these items should be moved to a static "Controls" utility class.

		/// <summary> Gets the CSS class used to indicated selected rows in item-lists 
		///		such as DataGrid.
		/// </summary>

		public static string CSS_SELECTED_CLASS {
			get { return "Selected"; }
		}

		/// <summary> Gets the exception text for a duplicate binding.
		/// </summary>

		public static string TARGET_PROPERTY_ALREADY_BOUND {
			get {
				return "Target property already bound";
			}
		}

		/// <summary> Gets the exception text used to signal a badly formatted binding triple.
		/// </summary>

		public string INVALID_BINDING_TRIPLE {
			get {
				return "Binding definition must consist of three comma-separated identifiers";
			}
		}

		/// <summary> Binds all pre-defined controls from the html element to a new instance of 
		///		their corresponding code-behind objects.
		///	<para>
		///		Note that the codebehind class name will need to be fully qualified with the _JavaScript_
		///		namespace in which the referenced class resides. That should be the name supplied in the
		///		ScriptAssembly attribute (in the project AssemblyInfo.cs file). So for example, in the 
		///		present iteration, we have SystemQut_Client.DataGrid and SystemQut_Client.CheckBox (this 
		///		is due a combination of the way Script# generates code and the way I dynamically create the
		///		codebehind; in a future iteration I would like to improve on this).
		/// </para>
		/// <para>
		///		Note well: codebehind objects are create by executing JavaScript's eval() function, so
		///		make sure you trust the origin and content of the elements!
		/// </para>
		/// <para>
		///		Child controls are bound before parents, so in principle the parent constructor should be 
		///		able to find any child controls that it expects to see as pre-existing child controls.
		///		However, that requires that the control has been set up correctly by the author.
		/// </para>
		/// <para>
		///		In the current iteration, probably best notto be too ambitious in that area.
		/// </para>
		/// </summary>
		/// <param name="element">
		///		The root element of a subtree.
		/// </param>

		public static void BindControls( Element element ) {
			const string ATTRIBUTE_NAME = "data-codebehind";

			for (int i = 0; i < element.ChildNodes.Length; i++) {
				BindControls( element.ChildNodes[i] );
			}

			if (Script.HasMethod( element, "getAttribute" )) {
				string className = (string) element.GetAttribute( ATTRIBUTE_NAME );

				if (Script.IsValue( className )) {
					if (types.ContainsKey( className )) {
						Type type = types[className];
						Script.CreateInstance( type, element );
					}
					else {
						Script.Eval( "new " + className + "(element);" );
					}
				}
			}
		}

		/// <summary> Gets the current setting of the nominated attribute on the underlying DOM element.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>

		public T GetAttribute<T>( string name ) {
			return (T) domElement.GetAttribute( name );
		}

		/// <summary> Sets an attribute  on the underlying DOM element.
		///	<para>
		///		Setting the attribute to a null value removes it from the element, if present.
		/// </para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>

		public void SetAttribute<T>( string name, T value ) {
			if (value == null) {
				domElement.RemoveAttribute( name );
			}
			else {
				domElement.SetAttribute( name, value );
			}
		}

		/// <summary> Binds all pre-defined controls from the supplied collection of html elements to  
		///		new instances of their corresponding code-behind objects.
		///	<para>
		///		Note that the codebehind class name will need to be fully qualified with the _JavaScript_
		///		namespace in which the referenced class resides. That should be the name supplied in the
		///		ScriptAssembly attribute (in the project AssemblyInfo.cs file). So for example, in the 
		///		present iteration, we have SystemQut_Client.DataGrid and SystemQut_Client.CheckBox (this 
		///		is due a combination of the way Script# generates code and the way I dynamically create the
		///		codebehind; in a future iteration I would like to improve on this).
		/// </para>
		/// <para>
		///		Note well: codebehind objects are create by executing JavaScript's eval() function, so
		///		make sure you trust the origin and content of the elements!
		/// </para>
		/// <para>
		///		Child controls are bound before parents, so in principle the parent constructor should be 
		///		able to find any child controls that it expects to see as pre-existing child controls.
		///		However, that requires that the control has been set up correctly by the author.
		/// </para>
		/// <para>
		///		In the current iteration, probably best notto be too ambitious in that area.
		/// </para>
		/// </summary>
		/// <param name="elements">
		///		A list of elements to be bound to codebehind objects, if possible.
		/// </param>

		public static void BindAllControls( ElementCollection elements ) {
			for (int i = 0; i < elements.Length; i++) {
				BindControls( elements[i] );
			}
		}

		/// <summary> Registers a type that implements ICodeBehind for use in BindControls.
		/// </summary>
		/// <param name="type">
		///		An implementation of ICodebehind which has the form 
		///		<c>class T: ICodeBehind { public T( Element element ) { ... } }</c>
		/// </param>

		public static void RegisterCodeBehindType(
			Type type
		) {
			if (types.ContainsKey( type.Name )) {
				throw new Exception( string.Format( "Type {0} is already registered", type.Name ) );
			}

			types[type.Name] = type;
		}


		/// <summary> Traverses the document object tree rooted at a particular element and 
		///		applies the specified predicate to each node which matches a specified type.
		/// </summary>
		/// <param name="element">
		///		The root element of a subtree.
		/// </param>
		/// <param name="action">
		///		The action to complete for each matching CodeBehind.
		/// </param>
		/// <param name="type">
		///		The CodeBehind subclass type to locate and process.
		/// </param>

		public static void ForallControlsOfType<T>( Type type, Element element, Action<T> action ) {
			ICodeBehind codebehind = Control.GetCodeBehind( element );

			if (Script.IsValue( codebehind ) && type.IsAssignableFrom( codebehind.GetType() )) {
				action( (T) codebehind );
			}

			for (int i = 0; i < element.ChildNodes.Length; i++) {
				ForallControlsOfType( type, element.ChildNodes[i], action );
			}
		}

		#endregion

		#region INotifyPropertyChanged
		/// <summary> PropertyChanged event handler.
		/// </summary>

		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary> Fire PropertyChanged events.
		/// </summary>
		/// <param name="propertyNames">
		///		A comma-separated list containing the names of any changed properties.
		/// </param>

		protected void NotifyPropertyChanged( string propertyNames ) {
			if (PropertyChanged != null) {
				PropertyChanged( this, new PropertyChangedEventArgs( propertyNames ) );
			}
		}
		#endregion

		/// <summary> Determines whether or not the associated DOM element is enabled.
		/// <para>
		///		This is provided to support data binding.
		/// </para>
		/// </summary>

		public bool IsEnabled {
			get {
				return !domElement.Disabled;
			}
			set {
				if (value != domElement.Disabled)
					return;

				domElement.Disabled = !value;

				NotifyPropertyChanged( "IsEnabled" );
				NotifyPropertyChanged( "IsDisabled" );
			}
		}

		/// <summary> Determines whether or not the associated DOM element is enabled.
		/// <para>
		///		This is provided to support data binding.
		/// </para>
		/// </summary>

		public bool IsDisabled {
			get {
				return domElement.Disabled;
			}
			set {
				if (value == domElement.Disabled)
					return;

				domElement.Disabled = value;

				NotifyPropertyChanged( "IsEnabled" );
				NotifyPropertyChanged( "IsDisabled" );
			}
		}

	}
}
