﻿using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {
	/// <summary> Specialise TextBox (which is my current name for InputElement, but which maybe should change) to input type=number.
	/// </summary>
	public class NumericInput : TextBox {
		/// <summary> Initialise a new NumericInput codebehind attached to the designated DOM element.
		/// <para>If the DOM element is null then a new element is created in the current HTML document.</para>
		/// </summary>
		/// <param name="domElement">The (possibly null) DOM element to which this codebehind will be connected.</param>
		public NumericInput( InputElement domElement ) :
			base( domElement == null
			? (InputElement) HtmlUtil.CreateElement( "input", null, new Dictionary<string, string>( "type", "number" ) )
			: domElement ) {
		}

		/// <summary> Get or set the numeric value of the associated input element.
		/// </summary>
		public double Value {
			get {
				return double.Parse( Text );
			}
			set {
				if (value == Value) return;

				double max = Max;
				if (Script.IsValue( max ) && value > max)
					throw new Exception( "value exceeds maximum." );

				double min = Min;
				if (Script.IsValue( min ) && value < min)
					throw new Exception( "value is below minimum." );

				Text = value.ToString();
			}
		}

		/// <summary> Get or set the minimum threshold.
		/// </summary>
		public double Min {
			get {
				return GetAttribute<double>( "min" );
			}
			set {
				SetAttribute<double>( "min", value );
			}
		}

		/// <summary> Get or set the maximum threshold.
		/// </summary>
		public double Max {
			get {
				return GetAttribute<double>( "max" );
			}
			set {
				SetAttribute<double>( "max", value );
			}
		}

		/// <summary> Get or set the step size for the numeric spinner.
		/// </summary>
		public double Step {
			get {
				return GetAttribute<double>( "step" );
			}
			set {
				SetAttribute<double>( "step", value );
			}
		}

		/// <summary> Returns true iff the current value is valid. 
		/// </summary>
		public bool IsValid {
			get {
				double value = Value;
				if (!Script.IsValue( value )) return false;

				double max = Max;
				if (Script.IsValue( max ) && value > max) return false;

				double min = Min;
				if (Script.IsValue( min ) && value < min) return false;

				return true;
			}
		}
	}
}
