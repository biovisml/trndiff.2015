// DataGridCheckBoxColumn.cs
//

using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;
using System.Html;

namespace SystemQut.Controls {

	public class DataGridCheckBoxColumn : DataGridColumnDefinition {
		private  Binding binding;

		public DataGridCheckBoxColumn ( Element domElement )
			: base( domElement != null ? domElement : domElement = Document.CreateElement( "td" ) ) { }

		public override DataGridCell CreateCell ( object item, string sourcePropertyName, DataBindingMode dataBindingMode ) {
			DataGridCheckBoxCell cell = new DataGridCheckBoxCell();
			binding = cell.CheckBox.Bind( sourcePropertyName, "IsChecked", dataBindingMode );
			cell.CheckBox.DataContext = item;
			return cell;
		}

		public override int OrderBy ( object x, object y ) {
			bool xValue = (bool) binding.GetSourceValue( x );
			bool yValue = (bool) binding.GetSourceValue( y );
			return xValue == yValue ? 0 : xValue ? 1 : -1;
		}
	}
}
