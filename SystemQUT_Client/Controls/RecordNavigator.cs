// RecordNavigator.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.ComponentModel;

namespace SystemQut.Controls {

	/// <summary> A custom control that provides button-based "first/prev/next/last" record
	///		navigation for use in conjunction with a paged collection of some sort.
	///	<para>
	///		The bulk of the visual properties of the control need to be set externally via
	///		CSS. This includes the size of the SVG-rendered imagery and the spacing around the 
	///		buttons etc.By default it will fill its container horizontally.
	/// </para>	
	/// <para>
	///		The control decorates its associated DOM element with the constant value
	///		RecordNavigator.CSS_CLASS, which is currently equal to "SystemQUT_RecordNavigator".
	///		In addition, The buttons can be addressed via RecordNavigator.(BUTTON_FIRST_CSS_CLASS|
	///		BUTTON_PREV_CSS_CLASS|BUTTON_NEXT_CSS_CLASS|BUTTON_LAST_CSS_CLASS), which have values
	///		"SystemQUT_FirstButton", "SystemQUT_PrevButton", "SystemQUT_NextButton" and 
	///		"SystemQUT_LastButton" respectively.
	/// </para>
	/// <para>
	///		The text is displayed in a div, styled explicitly to be inline-block. The child div can be selected via 
	///		CSS selection.
	/// </para>
	/// <para>
	///		For consistent layout purposes, the left margins of the first  and prev buttons and the 
	///		right margins of the next and last buttons are hard-coded.
	/// </para>
	/// </summary>

	public class RecordNavigator : CodeBehind {
		static public string CSS_CLASS {
			get {
				return "SystemQUT_RecordNavigator";
			}
		}
		static public string BUTTON_FIRST_CSS_CLASS {
			get {
				return "SystemQUT_FirstButton";
			}
		}
		static public string BUTTON_PREV_CSS_CLASS {
			get {
				return "SystemQUT_PrevButton";
			}
		}
		static public string BUTTON_NEXT_CSS_CLASS {
			get {
				return "SystemQUT_NextButton";
			}
		}
		static public string BUTTON_LAST_CSS_CLASS {
			get {
				return "SystemQUT_LastButton";
			}
		}

		Button gotoFirst;
		Button gotoLast;
		Button gotoNext;
		Button gotoPrevious;
		TextControl text;
		int min = (int) Script.Undefined;
		int current = (int) Script.Undefined;
		int max = (int) Script.Undefined;
		string format = "Record {0} of {2}";
		private RecordNavigatorFormatter formatter;

		public RecordNavigator (
			Element element
		)
			: base( element == null ? Document.CreateElement( "div" ) : element ) {

            // Hack
            bool local = false;
            if (element.TextContent == "Page selection control (local).") {
                local = true;
            }

			formatter = DefaultFormatter;

			domElement.InnerHTML = "";
			domElement.ClassList.Add( CSS_CLASS );

#if !DEBUG
			Element leftSpan = HtmlUtil.CreateElement( "span", domElement, new Dictionary<string,string> (
				"style", "float: left;"
			) );
#endif

            if (!local) {
			    gotoFirst = new GotoFirstRecordButton( null );
			    gotoFirst.domElement.ClassList.Add( BUTTON_FIRST_CSS_CLASS );
			    gotoFirst.domElement.Style.MarginLeft = "0px";
#if DEBUG
			    gotoFirst.AddElementTo( domElement );
#else
			    gotoFirst.AddElementTo( leftSpan );
#endif

			    gotoPrevious = new GotoPreviousRecordButton( null );
			    gotoPrevious.domElement.ClassList.Add( BUTTON_PREV_CSS_CLASS );
			    gotoPrevious.domElement.Style.MarginLeft = "0px";
#if DEBUG
			    gotoPrevious.AddElementTo( domElement );
#else
			    gotoPrevious.AddElementTo( leftSpan );
#endif
            }

			text = new TextControl( null, "div" );
			text.domElement.Style.Display = "inline-block";
			text.AddElementTo( domElement );

#if !DEBUG
			Element rightSpan = HtmlUtil.CreateElement( "span", domElement, new Dictionary<string,string> (
				"style", "float: right;"
			) );
#endif
            
            if (!local) {
			    gotoNext = new GotoNextRecordButton( null );
			    gotoNext.domElement.ClassList.Add( BUTTON_NEXT_CSS_CLASS );
#if !DEBUG
			    /*gotoNext.domElement.Style.MarginRight = "0px";
			    gotoNext.AddElementTo( rightSpan );*/
                gotoNext.domElement.Style.MarginLeft = "0px";
			    gotoNext.AddElementTo( leftSpan );
#else
                gotoNext.domElement.Style.MarginRight = "0px";
			    //gotoNext.AddElementTo( rightSpan );
                gotoNext.AddElementTo( domElement );
#endif

			    gotoLast = new GotoLastRecordButton( null );
			    gotoLast.domElement.ClassList.Add( BUTTON_LAST_CSS_CLASS );
#if !DEBUG
			    /*gotoLast.domElement.Style.MarginRight = "0px";
			    gotoLast.AddElementTo( rightSpan );*/
                gotoLast.domElement.Style.MarginLeft = "0px";
			    gotoLast.AddElementTo( leftSpan );
#else
                gotoLast.domElement.Style.MarginRight = "0px";
			    //gotoLast.AddElementTo( rightSpan );
                gotoLast.AddElementTo( domElement );
#endif
            }

			text.Bind( "Text", "Text", DataBindingMode.OneWay );
            if (!local) {
			    gotoFirst.Bind( "IsFirstRecord", "IsDisabled", DataBindingMode.OneWay );
			    gotoPrevious.Bind( "IsFirstRecord", "IsDisabled", DataBindingMode.OneWay );
			    gotoNext.Bind( "IsLastRecord", "IsDisabled", DataBindingMode.OneWay );
			    gotoLast.Bind( "IsLastRecord", "IsDisabled", DataBindingMode.OneWay );

			    gotoFirst.Clicked += button_Clicked;
			    gotoPrevious.Clicked += button_Clicked;
			    gotoNext.Clicked += button_Clicked;
			    gotoLast.Clicked += button_Clicked;
            }

			string format = GetAttribute<string>( "data-format" );

			if ( Script.IsValue( format ) ) this.format = format;

			DataContext = this;

            //Dummy buttons to even up the margin
#if !DEBUG
            if (!local) {
		        Button gotoFirstDummy = new GotoFirstRecordButton( null );
		        gotoFirstDummy.domElement.ClassList.Add( BUTTON_FIRST_CSS_CLASS );
		        gotoFirstDummy.domElement.Style.MarginRight = "0px";
                gotoFirstDummy.IsDisabled = true;
                gotoFirstDummy.domElement.Style.Visibility = "hidden";
                gotoFirstDummy.AddElementTo( rightSpan );
                Button gotoPreviousDummy = new GotoPreviousRecordButton( null );
		        gotoPreviousDummy.domElement.ClassList.Add( BUTTON_PREV_CSS_CLASS );
		        gotoPreviousDummy.domElement.Style.MarginRight = "0px";
                gotoPreviousDummy.IsDisabled = true;
                gotoPreviousDummy.domElement.Style.Visibility = "hidden";
                gotoPreviousDummy.AddElementTo( rightSpan );
                Button gotoNextDummy = new GotoNextRecordButton( null );
		        gotoNextDummy.domElement.ClassList.Add( BUTTON_NEXT_CSS_CLASS );
		        gotoNextDummy.domElement.Style.MarginRight = "0px";
                gotoNextDummy.IsDisabled = true;
                gotoNextDummy.domElement.Style.Visibility = "hidden";
                gotoNextDummy.AddElementTo( rightSpan );
                Button gotoLastDummy = new GotoLastRecordButton( null );
		        gotoLastDummy.domElement.ClassList.Add( BUTTON_LAST_CSS_CLASS );
		        gotoLastDummy.domElement.Style.MarginRight = "0px";
                gotoLastDummy.IsDisabled = true;
                gotoLastDummy.domElement.Style.Visibility = "hidden";
                gotoLastDummy.AddElementTo( rightSpan );
            }
#endif
        }

		void button_Clicked ( object sender, ElementEventArgs eventArgs ) {
			if ( sender == gotoFirst ) Current = min;
			else if ( sender == gotoLast ) Current = max;
			else if ( sender == gotoNext ) Current++;
			else if ( sender == gotoPrevious ) Current--;
		}

		public int Min {
			get {
				return min;
			}
			set {
				if ( value == min )
					return;

				min = value;

				bool justNotifyMin = false;

				if ( value > max ) {
					Max = (int) Script.Undefined;
					justNotifyMin = true;
				}

				if ( current < min ) {
					Current = (int) Script.Undefined;
					justNotifyMin = true;
				}

				if ( justNotifyMin ) {
					NotifyPropertyChanged( "Min" );
				}
				else {
					NotifyPropertyChanged( "Min,IsFirstRecord,IsLastRecord,Text" );
				}
			}
		}

		public int Max {
			get {
				return max;
			}
			set {
				if ( value == max )
					return;

				max = value;

				bool justNotifyMax = false;

				if ( value < min ) {
					Min = (int) Script.Undefined;
					justNotifyMax = true;
				}

				if ( current > max ) {
					Current = (int) Script.Undefined;
					justNotifyMax = true;
				}

				if ( justNotifyMax ) {
					NotifyPropertyChanged( "Min" );
				}
				else {
					NotifyPropertyChanged( "Max,IsFirstRecord,IsLastRecord,Text" );
				}
			}
		}

		/// <summary> Gets or sets the (possibly undefined) value of the current record.
		/// <para>
		///		An undefined value will be stored if the supplied value is out of bounds, or 
		///		deliberately set to be undefined.
		/// </para>
		/// </summary>

		public int Current {
			get {
				return current;
			}
			set {
				if ( value == current ) return;

				int newValue = ( !Script.IsValue( value ) || value < min || value > max ) ? (int) Script.Undefined : value;

				if ( newValue == current ) return;

				current = newValue;

				NotifyPropertyChanged( "Current,IsFirstRecord,IsLastRecord,Text" );
			}
		}

		public bool IsFirstRecord {
			get {
				return Script.IsNullOrUndefined( current )
				|| Script.IsNullOrUndefined( min )
				|| Script.IsNullOrUndefined( max )
				|| current == min;
			}
		}

		public bool IsLastRecord {
			get {
				return Script.IsNullOrUndefined( current )
				|| Script.IsNullOrUndefined( min )
				|| Script.IsNullOrUndefined( max )
				|| current == max;
			}
		}

		/// <summary> Gets a text display.
		/// </summary>

		public virtual string Text {
			get {
				return formatter( format, current, min, max );
			}
		}

		/// <summary> Get or set the format string used to create the text display.
		/// <para>
		///		Format will be applied with current as argument {0}, min as argument 
		///		{1} and max as argument {2}.
		/// </para>
		/// <para>
		///		This can also be set in markup using the "data-format" HTML5 attribute.
		/// </para>
		/// </summary>

		public string Format {
			get {
				return format;
			}
			set {
				if ( format == value ) return;

				format = value;
				NotifyPropertyChanged( "Text,Format" );
			}
		}

		/// <summary> The default formatter used to create the display text.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="current"></param>
		/// <param name="min"></param>
		/// <param name="max"></param>
		/// <returns></returns>

		private string DefaultFormatter ( string format, int current, int min, int max ) {
			return Script.IsValue( current ) && Script.IsValue( min ) && Script.IsValue( max ) 
				? string.Format( format, current, min, max ) 
				: String.Empty;
		}

		/// <summary> Get or set the formatter used to create the text displayed in the text block.
		/// <para>
		///		If the value is null or undefined, the default formatter will be used.
		/// </para>
		/// <para>
		///		Setting this notifies observers of change to the Text and Formatter properties.
		/// </para>
		/// </summary>

		public RecordNavigatorFormatter Formatter {
			get {
				return formatter;
			}
			set {
				if ( (bool) Script.Literal( "value == this.get_formatter()" ) ) return;

				formatter = Script.IsNullOrUndefined( value ) ? DefaultFormatter : value;
				NotifyPropertyChanged( "Text,Formatter" );
			}
		}
	}
}
