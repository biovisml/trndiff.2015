// IControl.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using System.Runtime.CompilerServices;
using System.Net;
using System.Xml;
using CodeBehind_ = SystemQut.Controls.CodeBehind;

namespace SystemQut.Controls {

	/// <summary> Mixin class which represents a Html Element that is bound to 
	///		a backing peer object which encapsulates the code-behind behaviour.
	///	<para>
	///		Be warned: an object of this type will never pass the "is Control&lt;CodeBehindSubclass&gt;" 
	///		test because it is not constructed via a constructor. It is attached to an 
	///		element. 
	/// </para>
	/// </summary>

	public class Control : Element {

		/// <summary> Get or set the CodeBehind object. 
		/// </summary>

		[ScriptName( "__codebehind__" )]
		public ICodeBehind CodeBehind;

		/// <summary> Returns true iff the element is attached to a codebehind.
		/// </summary>
		/// <param name="element"> A non-null element to be tested. </param>
		/// <returns> Returns true iff the element is attached to a codebehind. </returns>

		public static bool HasCodeBehind ( Element element ) {
			return (bool) Script.Literal( "element.__codebehind__" );
		}

		/// <summary> Returns true iff the element is attached to a codebehind.
		/// </summary>
		/// <param name="element"> A non-null element to be tested. </param>
		/// <returns> Returns true iff the element is attached to a codebehind. </returns>

		public static ICodeBehind GetCodeBehind ( Element element ) {
			return (ICodeBehind) Script.Literal( "element.__codebehind__" );
		}

		/// <summary> Loads a HTML file from the specified URL and converts the content of its
		///		tbody element into a document fragment. Any controls defined in the fragment are
		///		bound to their respective codebehind objects and initial data bindings are set up. 
		///	<para>
		///		On success, the resulting document fragment is processed by the onsuccess callback. 
		///	</para>	
		///	<para>
		///		On failure or it an exception occurs, the error is processed by the onError callback.
		///	</para>	
		/// </summary>
		/// <param name="url"> 
		///		The address of the HTML containing the document fragment. 
		///	</param>
		/// <param name="onSuccess"> 
		///		A delegate that will process the retrieved document. 
		///	</param>
		/// <param name="onError"> 
		///		A delegate that will process any exception that is encountered, or which is generated in the event of a HTTP error. 
		///	</param>

		static public void LoadControlTemplate ( string url, Action<DocumentFragment> onSuccess, Action<Exception> onError ) {
			XmlHttpRequest req = new XmlHttpRequest();

			req.Open( HttpVerb.Get, url );

			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status != 200 ) {
						try {
							// Must throw to generate the stack trace for client.
							throw new Exception( string.Format( "HTTP request failed, status = {0}. Response text = {1}", req.Status, req.ResponseText ) );
						}
						catch ( Exception ex ) {
							onError( ex );
						}
					}
					else {
						try {
							// Rip out the tbody content and load it into a document fragment.
							RegExp startExpr = new RegExp( "<body[^>]*>" );
							RegExp endExpr = new RegExp( "<\\/body>" );
							Match startMatch = startExpr.Exec( req.ResponseText );
							Match endMatch = endExpr.Exec( req.ResponseText );
							int startLen = startMatch[0].Length;
							string innerHTML = req.ResponseText.Substring( startMatch.Index + startLen, endMatch.Index );

							DocumentFragment fragment = ParseControl( innerHTML );

							onSuccess( fragment );
						}
						catch ( Exception e ) {
							onError( e );
						}
					}
				}
			};

			req.Send();
		}

		/// <summary> Parses a html string into a document fragment. Any controls defined in the 
		///		fragment are bound to their respective codebehind objects. 
		/// </summary>
		/// <param name="html">
		///		A string that contains valid html, possibly containing conotrol binding information
		///		signalled by the presence of "data-codebehind" attributes.
		/// </param>
		/// <returns>
		///		Returns a document fragment within which any controls have been bound to an
		///		instance of their respective code-behind class and data bindings have been set up.
		/// </returns>

		public static DocumentFragment ParseControl ( string html ) {
			Element div = Document.CreateElement( "div" );
			div.InnerHTML = html;

			DocumentFragment fragment = Document.CreateDocumentFragment();

			while ( (bool) (object) div.FirstChild ) {
				fragment.AppendChild( div.FirstChild );
			}

			CodeBehind_.BindAllControls( fragment.ChildNodes );
			return fragment;
		}
	}
}
