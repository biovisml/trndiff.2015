// ComboBox.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using System.Collections;
using SystemQut.ComponentModel;

namespace SystemQut.Controls {

	/// <summary> CodeBehind for a HTML Select object.
	/// </summary>

	public class Select : CodeBehind {
		public event ElementEventHandler SelectionChanged;

		List<SelectItem> items = new List<SelectItem>();

		/// <summary> Create a new Select codebehind object and bind it to a HTML select element.
		/// <para>
		///		Upon creation, any existing option elements will be bound to SelectItem objects
		///		which can be used to associate a data value with each option.
		/// </para>
		/// <para>
		///		A listener is added to the change event of the HTML select element. Any detected 
		///		changes are realayed to observers via the SelectionChanged event.
		/// </para>
		/// </summary>
		/// <param name="domElement">
		///		A (possibly null) HTML select element. If the supplied argument is null, a new select 
		///		element will be created.
		/// </param>

		public Select (
			SelectElement domElement
		)
			: base( domElement == null ? domElement = (SelectElement) Document.CreateElement( "select" ) : domElement ) {

			for ( int i = 0; i < domElement.Options.Length; i++ ) {
				OptionElement option = (OptionElement) domElement.Options[i];
				ICodeBehind codebehind = Control.GetCodeBehind( option );

				if ( Script.IsValue( codebehind ) ) {
					items.Add( (SelectItem) codebehind );
				}
				else {
					items.Add( new SelectItem( option ) );
				}
			}

			domElement.AddEventListener( "change", new ElementEventListener( ChangeListener ), false );
		}

		/// <summary> Event listener for change events on the dom element.
		/// </summary>
		/// <param name="elementEvent"></param>

		private void ChangeListener ( ElementEvent elementEvent ) {
			if ( SelectionChanged != null ) SelectionChanged( this, new ElementEventArgs( elementEvent ) );
		}

		/// <summary> Get or set the options displayed in the list. 
		/// </summary>

		public IEnumerable Items {
			get {
				return new ProjectedList(
					(SelectItem[]) items,
					delegate( object item ) { return ( (SelectItem) item ).Item; }
				);
			}
			set {
				SelectElement select = (SelectElement) domElement;

				while ( select.Options.Length > 0 ) {
					select.Remove( 0 );
				}

				foreach ( SelectItem item in items ) {
					item.Dispose();
				}

				items.Clear();

				foreach ( object item in value ) {
					SelectItem selectItem = new SelectItem( null );
					selectItem.Item = item;
					items.Add( selectItem );
					select.Add( (OptionElement) selectItem.domElement );
				}
			}
		}

		/// <summary> Get or set the options displayed in the list. 
		/// </summary>

		public IEnumerable<object> SelectedItems {
			get {
				return new FilteredList<SelectItem,object>(
					(SelectItem[]) items, /* hacky cast... by List is implemented by array. */
					delegate( SelectItem selectItem ) { return selectItem.IsSelected; },
					delegate( SelectItem selectItem ) { return selectItem.Item; }
				);
			}
			set {
				foreach ( SelectItem selectItem in items ) {
					selectItem.IsSelected = false;

					foreach ( object item in value ) {
						if ( selectItem.Item == item ) {
							selectItem.IsSelected = true;
							break;
						}
					}
				}
			}
		}

		/// <summary> Gets a reference to the associated DOM element as a SelectElement.
		/// </summary>

		public SelectElement SelectElement {
			get {
				return (SelectElement) domElement;
			}
		}

		/// <summary> Get or set the selected index for a single-selection list box.
		/// </summary>

		public int SelectedIndex {
			get {
				return ( (SelectElement) domElement ).SelectedIndex;
			}
			set {
				( (SelectElement) domElement ).SelectedIndex = value;
			}
		}
	}
}
