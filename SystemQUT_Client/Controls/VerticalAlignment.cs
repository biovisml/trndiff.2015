﻿using System;
using System.Collections.Generic;


namespace SystemQut.Controls {
	public enum VerticalAlignment {
		Top = 0,
		Middle = 1,
		Bottom = 2
	}
}
