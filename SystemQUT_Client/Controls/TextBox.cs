// TextBox.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {

	public class TextBox : CodeBehind {

		/*<input data-name="GenomeSearchField" data-codebehind="TextBox" type="text" placeholder="[Optional] Search text."
							style="width: 350px" /> */

		/// <summary> Code behind for a &lt;input type="text"&gt; element.
		/// </summary>
		/// <param name="domElement">
		///		Either an existing DOM element which is to be bound to the current code behind,
		///		or null, in which case an input element is dynamically created.
		/// </param>

		public TextBox( InputElement domElement )
			: base( domElement == null ?
				  HtmlUtil.CreateElement( "input", null,
					  new Dictionary<string, string>( "type", "text" ) ) :
				  domElement
		) {
			DomElement.AddEventListener( "input", InputHappened, false );
		}

		private void InputHappened (ElementEvent e) {
			NotifyPropertyChanged( "Text" );
			NotifyPropertyChanged( "Value" );
		}

		/// <summary> Get or set the place holder that will be displayed in the text box.
		/// </summary>

		public string PlaceHolder {
			get { return GetAttribute<string>( "placeholder" ); }
			set { SetAttribute( "placeholder", value ); }
		}

		/// <summary> Get or set the value of the input element.
		/// </summary>

		public string Text {
			get { return ((InputElement) domElement).Value; }
			set { ((InputElement) domElement).Value = value; }
		}
	}
}
