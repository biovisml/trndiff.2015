﻿using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.ComponentModel;

namespace SystemQut.Controls {
	public class SelectItem : CodeBehind {
		private object item;
		private static OptionElement dummyOption;

		/// <summary> Binds a code behind to an option element.
		/// </summary>
		/// <param name="option">
		///		A possibly null OptionElement. If the value supplied is null a new element is created. 
		///		If not that element is bound to the codebehind.
		/// </param>

		public SelectItem (
			OptionElement option
		)
			: base( option == null ? dummyOption = (OptionElement) Document.CreateElement( "option" ) : option ) {
			item = option == null ? String.Empty : option.Text;
		}

		/// <summary> Get or set the Item associated with this option.
		/// <para>
		///		Setting the item triggers the PropertyChanged event, and also replaces the 
		///		text content of the
		/// </para>
		/// </summary>

		public object Item {
			get {
				return item;
			}
			set {
				if ( item == value )
					return;

				item = value;
				string text = Objects.ToString( item, "undefined" );

				( (OptionElement) domElement ).Text = text;
				( (OptionElement) domElement ).Value = text;
			}
		}


		public bool IsSelected {
			get { 
				return ( (OptionElement) domElement ).Selected;
			}
			set {
				if ( value == IsSelected ) return;

				( (OptionElement) domElement ).Selected = value;
			}
		}

	}
}
