// ButtonSubclasses.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Svg;

namespace SystemQut.Controls {

	/// <summary> A button subclass that represents an arbitrary button containing scripted 
	///		(or static) svg content.
	/// </summary>

	public abstract class SvgButton : Button {
		protected SvgSvg drawing;
		protected SvgGroup group;
		protected double xMax;
		protected double yMax;

		protected abstract void RenderContent ();

		public static string CSS_CLASS { get { return "SystemQUT_SvgButton"; } }

		/// <summary> Creates a new GotoFirstRecord button and renders the content.
		/// </summary>
		/// <param name="element">
		///		A (possibly null) HTML element which will fire "onclick" events. If the value supplied is null,
		///		a new HTML Button is created.
		/// </param>
		/// <param name="xMax">
		///		the maximum x-coordinate of the drawing. 
		/// </param>
		/// <param name="yMax">
		///		the maximum y-coordinate of the drawing. 
		/// </param>

		public SvgButton (
			Element element,
			double xMax,
			double yMax
		)
			: base( element ) {
			domElement.ClassList.Add( CSS_CLASS );

			this.xMax = xMax;
			this.yMax = yMax;

			drawing = new SvgSvg( null, null );
			drawing.domElement.Style.Display = "inline-block";
			drawing.PreserveAspectRatio = PreserveAspectRatioType.XMidYMid;
			drawing.ViewBox = new SvgRect( 0, 0, xMax, yMax );
			drawing.AddElementTo( domElement );

			double mid = yMax / 2;

			group = new SvgGroup( drawing, null );
			group.SetAttribute( "style", "stroke-linecap: round; stroke-linejoin: round" );

			RenderContent();
		}
	}
}
