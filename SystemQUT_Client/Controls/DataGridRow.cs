﻿using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {

	/// <summary> Represents a row in a datagrid.
	/// </summary>

	public class DataGridRow : CodeBehind, IEnumerable<DataGridCell> {
		// TODO: override Dispose to clean up event handlers etc.
		// TODO: reuse instances where possible.

		private object item;
		private int index;
		private readonly List<DataGridCell> cells = new List<DataGridCell>();
		private bool isSelected;

		public DataGridRow (
			object item,
			int index,
			List<DataGridColumnDefinition> columns,
			string rowCssClass,
			string alternateRowCssClass 
		)
			: base( Document.CreateElement( "tr" ) ) {
			this.item = item;
			this.index = index;

			foreach ( DataGridColumnDefinition column in columns ) {
				DataGridCell cell = column.CreateCell( item, column.SourcePropertyName, column.BindingMode );
				cell.AddElementTo( domElement );

				if ( ! (cell is DataGridCheckBoxCell) ) {
					cell.domElement.AddEventListener( "click", cell_Clicked, false );
				}
			}

			domElement.ClassList.Add( CSS_CLASS );
			string className = index % 2 == 0 ? rowCssClass : alternateRowCssClass;
			
			if ( Script.IsValue(className ) && className.Trim().Length > 0 ) {
				domElement.ClassList.Add( className.Trim() );
			}
		}

		/// <summary> When any cell is clicked (unless it is a checkbox cell, later to be generalised to
		///		other kinds of input-enabled cells...) we update the isSelected status as required and notify 
		///		listeners via PropertyChanged.
		///	<para>
		///		If the control key is held down, the selection state is toggled. Otherwise, it becomes true
		///		on any click. 
		/// </para>
		/// </summary>
		/// <param name="args"></param>

		private void cell_Clicked ( ElementEvent args ) {
			this.IsSelected = args.CtrlKey ? !isSelected : true;
		}

		/// <summary> Gets an enumerator that iterates over the Cell collection.
		/// </summary>
		/// <returns></returns>

		public IEnumerator<DataGridCell> GetEnumerator () {
			return cells.GetEnumerator();
		}

		/// <summary> Get or set the selected status of this row.
		/// <para>
		///		Setting this value toggles the "Selected" flag in the CSS class list
		///		and Fires PropertyChanged for both 
		/// </para>
		/// </summary>

		public bool IsSelected {
			get { return isSelected; }
			set {
				if ( value == isSelected ) return;

				isSelected = value;
				domElement.ClassList.Toggle( CodeBehind.CSS_SELECTED_CLASS );
				NotifyPropertyChanged( "IsSelected" );
			}
		}

		/// <summary> Marker CSS class used to signal a DataGridRow.
		/// <para>
		///		This is not to be confused with the other styles used for formatting
		///		purposes, but is can certainly be used for formatting.
		/// </para>
		/// </summary>

		public static string CSS_CLASS {
			get { return "SystemQUT_DataGridRow"; }
		}

		/// <summary> Gets the index of this row in the collection.
		/// </summary>

		public int Index {
			get { return index; }
		}

		/// <summary> Gets the item associated with the row.
		/// </summary>

		public object Item {
			get { return item; }
		}

		/// <summary> Returns a string containing the index and textual representation of the associated item. 
		/// </summary>
		/// <returns></returns>

		public override string ToString () {
			return string.Format( "Row {0}: {1}", index, item );
		}
	}
}
