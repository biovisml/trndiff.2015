// Button.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace SystemQut.Controls {

	/// <summary> Codebehind class for buttons.
	/// </summary>

	public class Button : CodeBehind {
		public event ElementEventHandler Clicked;

		/// <summary> Construct a new button and add wire up event notifications when the DOM element is 
		///		clicked.
		/// </summary>
		/// <param name="domElement">
		///		A (possibly null) HTML element which will fire "onclick" events. If the value supplied is null,
		///		a new HTML Button is created.
		/// </param>

		public Button( Element domElement )
			: base( domElement != null ? domElement : domElement = Document.CreateElement( "button" ) ) {

			DomElement.AddEventListener( "click", delegate ( ElementEvent eventArgs ) {
				if (Clicked != null) Clicked( this, new ElementEventArgs( eventArgs ) );
			}, false );
		}

	}
}
