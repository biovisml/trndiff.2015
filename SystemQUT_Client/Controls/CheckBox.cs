// CheckBox.cs
//

using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;
using System.Html;

namespace SystemQut.Controls {

	public class CheckBox : CodeBehind, INotifyPropertyChanged {

		public CheckBox (
			CheckBoxElement domElement
		)
			: base( domElement != null ? domElement :
				domElement = (CheckBoxElement) HtmlUtil.CreateElement( "input", null, new Dictionary<string, string>( "type", "checkbox" ) ) ) {

			domElement.AddEventListener( "click", CheckBoxClicked, false );
		}

		void CheckBoxClicked ( ElementEvent e ) {
			NotifyPropertyChanged( "IsChecked" );
		}

		/// <summary> Get or set the "IsChecked" state of the associated checkbox.
		/// </summary>

		public bool IsChecked {
			get { return ( (CheckBoxElement) domElement ).Checked; }
			set {
				if ( value == ( (CheckBoxElement) domElement ).Checked ) return;

				( (CheckBoxElement) domElement ).Checked = value;
				NotifyPropertyChanged( "IsChecked" );
			}
		}
	}
}
