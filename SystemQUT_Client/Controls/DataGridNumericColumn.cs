// DataGridTextColumnDefinition.cs
//

using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;
using System.Html;

namespace SystemQut.Controls {

	public class DataGridNumericColumn : DataGridColumnDefinition {
		private const string FORMAT_NAME = "data-format";

		private string format = null;
		private  Binding binding;

		public DataGridNumericColumn ( Element domElement )
			: base( domElement != null ? domElement : domElement = Document.CreateElement( "td" ) ) { }

		/// <summary> Get or set the format string  used to display the value.
		/// </summary>

		public string Format {
			get {
				if ( format == null ) {
					format = GetAttribute<string>( FORMAT_NAME );

					if ( Script.IsNullOrUndefined( format ) ) {
						format = "{0}";
					}
				}

				return format;
			}
			set {
				if ( format == value ) return;
				format = value;
				SetAttribute( FORMAT_NAME, value );
			}
		}

		/// <summary> Create a numeric display cell.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="sourcePropertyName"></param>
		/// <param name="dataBindingMode"></param>
		/// <returns></returns>

		public override DataGridCell CreateCell (
			object item,
			string sourcePropertyName,
			DataBindingMode dataBindingMode
		) {
			// TODO: Editable text box for two-way data binding.

			DataGridNumericCell text = new DataGridNumericCell( Format );
			binding = text.Bind( sourcePropertyName, "Value", DataBindingMode.OneWay );
			text.DataContext = item;
			return text;
		}


		public override int OrderBy ( object x, object y ) {
			Number xValue = (Number) binding.GetSourceValue( x );
			Number yValue = (Number) binding.GetSourceValue( y );
			return xValue < yValue ? -1 : xValue == yValue ? 0 : 1;
		}
	}
}
