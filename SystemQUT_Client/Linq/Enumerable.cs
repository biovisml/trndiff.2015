// Enumerable.cs
//

using System;
using System.Collections;
using System.Collections.Generic;

namespace SystemQut.Linq {
	
	/// <summary> Some handy methods for list comprehensions and the like.
	/// </summary>

	public static class Enumerable {
		
		/// <summary> Gets the first element in the collection that satisifes the supplied predicate,
		///		or null if no such element exists.
		/// <para>
		///		The predicate may be null, in which case the first element is returned.
		/// </para>
		/// </summary>
		/// <typeparam name="T"> 
		///		The set from which the list elements are drawn. 
		/// </typeparam>
		/// <param name="list"> 
		///		A list of objects having type T. 
		///	</param>
		/// <param name="predicate"> 
		///		A (possibly null) predicate used to seach the list.
		/// </param>
		/// <returns>
		///		The first element in the collection that satisifes the supplied predicate,
		///		or null if no such element exists.
		/// <para>
		///		The predicate may be null, in which case the first element is returned.
		/// </para>
		/// </returns>

		public static T FirstOrDefault<T>( IEnumerable<T> list, Func<T,bool> predicate ) {
			foreach( T item in list ) {
				if ( predicate == null || predicate( item ) ) return item;
			}

			return (T) (object) null;
		}
		
		/// <summary> Returns true iff all elements in the list match the supplied predicate, or the predicate is null.
		/// </summary>
		/// <typeparam name="T">
		///		the data type of the list.
		/// </typeparam>
		/// <param name="list">
		///		A list of items of type T.
		/// </param>
		/// <param name="predicate">
		///		A (possibly null) predicate which is used to test the list elements.
		/// </param>
		/// <returns>
		///		Returns true iff all elements in the list match the supplied predicate, or the predicate is null.
		/// </returns>

		public static bool All<T> ( IEnumerable<T> list, Func<T,bool> predicate ) {
			foreach( T item in list ) {
				if ( predicate != null && ! predicate( item ) ) return false;
			}

			return true;
		} 
		
		public static bool Any<T> ( IEnumerable<T> list, Func<T,bool> predicate ) {
			foreach( T item in list ) {
				if ( predicate == null || predicate( item ) ) return true;
			}

			return false;
		}
	}
}
