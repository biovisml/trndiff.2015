using System;
using System.Collections.Generic;

namespace SystemQut {

	public static class Assert {

		/// <summary> Evnt to which you can subscribe to take custom actions in the event
		///		that an assertion failed. This is needful when working on asynchronous 
		///		programs.
		/// </summary>
		public static event Action<Exception> Process;

		/// <summary> Asserts two numbers are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>

		static public void NumbersEqual ( Number expected, Number actual, string location ) {
			if ( expected != actual ) {
				try {
					throw new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
						actual,
						expected,
						location
					) );
				}
				catch ( Exception e ) {
					if ( Process != null ) Process( e );
					throw e;
				}
			}
		}

		/// <summary> Asserts two strings are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>

		static public void StringsEqual ( String expected, String actual, string location ) {
			//Console.Log( "Expected = " + expected );
			//Console.Log( "Actual = " + actual );

			if ( String.Compare( expected, actual ) != 0 ) {
				try {
					throw new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
					actual,
					expected,
					location
				) );
				}
				catch ( Exception e ) {
					if ( Process != null ) Process( e );
					throw e;
				}
			}
		}

		/// <summary> Asserts two comparable values are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>

		static public void ComparablesEqual ( IComparable expected, IComparable actual, string location ) {
			if ( expected.CompareTo( actual ) != 0 ) {
				try {
					throw new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
						actual,
						expected,
						location
					) );
				}
				catch ( Exception e ) {
					if ( Process != null ) Process( e );
					throw e;
				}
			}
		}

		/// <summary> Asserts two numbers are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>

		static public void CharsEqual ( char expected, char actual, string location ) {
			if ( expected != actual ) {
				try {
					throw new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
						actual,
						expected,
						location
					) );
				}
				catch ( Exception e ) {
					if ( Process != null ) Process( e );
					throw e;
				}
			}
		}

		/// <summary> Asserts two numbers are approximately equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="epsilon"></param>
		/// <param name="location"></param>

		static public void NumbersApproxEqual ( Number expected, Number actual, Number epsilon, string location ) {
			if ( Math.Abs( expected - actual ) > epsilon ) {
				try {
					throw new Exception( String.Format( "Assertion failed: {0}: Difference between actual value <{0}> and expected value <{1}> exceeds <{2}>",
					actual,
					expected,
					epsilon
				) );
				}
				catch ( Exception e ) {
					if ( Process != null ) Process( e );
					throw e;
				}
			}
		}

		/// <summary> Asserts that the supplied argument is true.
		/// </summary>
		/// <param name="condition"></param>
		/// <param name="location"></param>

		static public void IsTrue ( bool condition, string location ) {
			if ( !condition ) {
				try {
					throw new Exception( String.Format( "Assertion failed: {0}: Condition is not true as expected.", location ) );
				}
				catch ( Exception e ) {
					if ( Process != null ) Process( e );
					throw e;
				}
			}
		}

		/// <summary> Asserts that the supplied argument is false.
		/// </summary>
		/// <param name="condition"></param>
		/// <param name="location"></param>

		static public void IsFalse ( bool condition, string location ) {
			if ( condition ) {
				try {
					throw new Exception( String.Format( "Assertion failed: {0}: Condition is not false as expected.", location ) );
				}
				catch ( Exception e ) {
					if ( Process != null ) Process( e );
					throw e;
				}
			}
		}

		/// <summary> Fail unconditionally.
		/// </summary>
		/// <param name="message"></param>

		static public void Fail ( string message ) {
			try {
				throw new Exception( String.Format( "Assertion failed: {0}.", message ) );
			}
			catch ( Exception e ) {
				if ( Process != null ) Process( e );
				throw e;
			}
		}

		/// <summary> Asserts two comparable values are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>

		static public void ArraysEqual<T> ( T[] expected, T[] actual, string location ) {
			if ( expected == null && actual == null ) return;
			if ( expected != null && actual == null ) Fail( location + ": Arrays must both be non-null if either is non-null." );
			if ( expected == null && actual != null ) Fail( location + ": Arrays must both be non-null if either is non-null." );
			if ( expected.Length != actual.Length ) Fail( location + ": Array lengths must be equal." );

			for ( int i = 0; i < expected.Length; i++ ) {
				if ( (object) expected[i] != (object) actual[i] ) {
					try {
						throw new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
							actual,
							expected,
							location
						) );
					}
					catch ( Exception e ) {
						if ( Process != null ) Process( e );
						throw e;
					}
				}
			}
		}
	}
}
