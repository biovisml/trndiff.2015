// AttrQualifier.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	/// <summary> Attribute qualifiers.
	/// <para>[att] Represents an element with the att attribute, whatever the value of the attribute.</para>
	/// <para>[att=val] Represents an element with the att attribute whose value is exactly "val". </para>
	/// <para>[att~=val] Represents an element with the att attribute whose value is a whitespace-separated 
	///		list of words, one of which is exactly "val". If "val" contains whitespace, it will never represent 
	///		anything (since the words are separated by spaces). Also if "val" is the empty string, it will never 
	///		represent anything. </para>
	/// <para>[att|=val] Represents an element with the att attribute, its value either being exactly "val" or 
	///		beginning with "val" immediately followed by "-" (U+002D). This is primarily intended to allow language 
	///		subcode matches (e.g., the hreflang attribute on the a element in HTML) as described in BCP 47 ([BCP47]) 
	///		or its successor. For lang (or xml:lang) language subcode matching, please see the :lang pseudo-class.</para>
	/// <para>Attribute values must be CSS identifiers or strings. [CSS21] The case-sensitivity of attribute names 
	///		and values in selectors depends on the document language. </para>
	/// <para>Three additional attribute selectors are provided for matching substrings in the value of an attribute:</para>
	/// <para>[att^=val] Represents an element with the att attribute whose value begins with the prefix "val". 
	///		If "val" is the empty string then the selector does not represent anything.</para>
	/// <para> [att$=val] Represents an element with the att attribute whose value ends with the suffix "val". 
	///		If "val" is the empty string then the selector does not represent anything. </para>
	/// <para> [att*=val] Represents an element with the att attribute whose value contains at least one 
	///		instance of the substring "val". If "val" is the empty string then the selector does not represent 
	///		anything.</para>
	/// </summary>

	public class AttrQualifier {
		private string attr;
		private string op;
		private string value;

		public const string EQUALS = "=";
		public const string CONTAINS_WORD = "~=";
		public const string EQUAL_OR_HYPHENATED = "|=";
		public const string STARTS_WITH = "^=";
		public const string ENDS_WITH = "$=";
		public const string CONTAINS = "*=";

#pragma warning disable 824
		/// <summary> Initialises a qualifier which tests whether an attribute is set.
		/// </summary>
		/// <param name="attr"> The non-null, non-blank attribute name. </param>

		public extern AttrQualifier ( string attr );
#pragma warning restore 824

		/// <summary> Initialises a qualifier which tests whether an attribute has a value
		///		which matches according to the operator.
		/// </summary>
		/// <param name="attr"> 
		///		The non-null, non-blank attribute name. 
		///	</param>
		/// <param name="op"> The operator, which must be one of: EQUALS, CONTAINS_WORD, 
		///		EQUAL_OR_HYPHENATED, STARTS_WITH, ENDS_WITH or CONTAINS.
		///	</param>
		///	<param name="value"> 
		///		The value to be matched with the operator.
		/// </param>

		public AttrQualifier (
			string attr,
			string op,
			string value
		) {
			Assert.IsFalse( String.IsNullOrWhiteSpace( attr ), "AttrQualifier.ctor: attr may not be null or blank" );

			this.attr = attr;

			if ( Arguments.Current.Length == 1 ) {
				this.op = null;
				this.value = null;
				return;
			}

			string [] validOps = { 
				EQUALS,
				CONTAINS_WORD,
				EQUAL_OR_HYPHENATED,
				STARTS_WITH,
				ENDS_WITH,
				CONTAINS,
			};

			Assert.IsTrue( validOps.Contains( op ), "AttrQualifier.ctor: invalid operator." );
			Assert.IsFalse( String.IsNullOrWhiteSpace( value ), "AttrQualifier.ctor: attr may not be null or blank" );

			this.op = op;
			this.value = value;
		}

		/// <summary> Emits the Css Asstribute qualifier.
		/// </summary>
		/// <returns></returns>

		public override string ToString () {
			return string.Format( "[{0}{1}]", attr, ( op == null ? "" : op + value ) );
		}
	}
}
