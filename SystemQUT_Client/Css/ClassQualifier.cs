// IdentQualifier.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	/// <summary> Represents a Css qualifier of the form "#xyzzy".
	/// </summary>

	public class ClassQualifier: CssQualifier {
		private string name;

		public ClassQualifier ( string name ) {
			Assert.IsFalse( String.IsNullOrWhiteSpace( name ), "ClassQualifier.ctor: name may not be null or blank/" );

			this.name = name;
		}

		public override string ToString () {
			return "." + name;
		}
	}
}
