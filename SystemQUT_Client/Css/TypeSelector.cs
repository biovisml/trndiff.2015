// TypeSelector.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	/// <summary>
	///		type_selector
	///		  : [ namespace_prefix ]? element_name
	///		  ;
	///		
	///		namespace_prefix
	///		  : [ IDENT | '*' ]? '|'
	///		  ;
	/// </summary>

	public class TypeSelector : SimpleSelector {
		private string nameSpacePrefix;
		private string elementName;

		/// <summary> Initialises a TypeSelector.
		/// </summary>
		/// <param name="nameSpacePrefix"> 
		///		An optional namespace prefix. If null, element-name is emitted by itself.
		///		If not null, the full "IDENT | element-name" is emitted.
		/// </param>
		/// <param name="elementName"> 
		///		The non-null, non-empty name of elements to be matched. "*" matches any element. 
		///	</param>

		public TypeSelector (
			string nameSpacePrefix,
			string elementName
		) {
			Assert.IsFalse( string.IsNullOrWhiteSpace( elementName ), "TypeSelector.ctor: elementName may not be null" );

			this.elementName = elementName;
			this.nameSpacePrefix = nameSpacePrefix;
		}

		/// <summary> Emits a Css type selector.
		/// </summary>
		/// <returns> Emits a Css type selector. </returns>

		public override string ToString () {
			return ( nameSpacePrefix == null ? "" : nameSpacePrefix + "|" ) + elementName;
		}
	}
}
