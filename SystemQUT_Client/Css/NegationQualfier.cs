// NegationQualfier.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	/// <summary> Css Selector helper of the form :NOT( argument ). 
	/// </summary>

	public class NegationQualfier: SimpleSelector {
		SimpleSelector negationArg;

		/// <summary> Initialises a NegationQualifier which negates the given SimpleSelector.
		/// </summary>
		/// <param name="negationArg"> A SimpleSelector which may not be a NegationQualifier. </param>

		public NegationQualfier( SimpleSelector negationArg ) {
			Assert.IsTrue( Script.IsValue( negationArg ), "NegationQualfier.ctor: negatedArg must have a value." );
			Assert.IsFalse( negationArg is NegationQualfier, "NegationQualfier.ctor: the negated selector may not be a Negation." );
			
			this.negationArg = negationArg;
		}

		public override string ToString () {
			return string.Format( ":NOT({0})", negationArg );
		}
	}
}
