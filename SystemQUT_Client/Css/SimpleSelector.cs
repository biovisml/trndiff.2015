// Class1.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	/// <summary>
	/// <pre>
	///		simple_selector_sequence
	///		  : [ type_selector | universal ]
	///		    [ HASH | class | attrib | pseudo | negation ]*
	///		  | [ HASH | class | attrib | pseudo | negation ]+
	///		  ;
	///	</pre>
	/// </summary>

	public abstract class SimpleSelector: Selector {
	}

}
