// TypeSelector.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	/// <summary>
	///	<pre>
	///		simple_selector_sequence
	///		  : [ type_selector | universal ] [ HASH | class | attrib | pseudo | negation ]*
	///		  | [ HASH | class | attrib | pseudo | negation ]+
	///		  ;
	///	</pre>
	/// </summary>

	public class SimpleSelectorSeq : Selector {
		private TypeSelector typeSelector;
		private List<CssQualifier> qualifiers;

		/// <summary> Initialises a simple selector sequence.
		/// </summary>
		/// <param name="typeSelector">
		///		The optional type selector which heads this sequence.
		/// </param>
		///	<param name="qualifiers">
		///		An optional list of qualifiers
		/// </param>

		public SimpleSelectorSeq (
			TypeSelector typeSelector,
			List<CssQualifier> qualifiers
		) {
			this.typeSelector = typeSelector;
			this.qualifiers = new List<CssQualifier>();

			if ( Script.IsValue( qualifiers) ) {
				for ( int i = 0; i < qualifiers.Length; i++ ) {
					this.qualifiers.Add( qualifiers[i] );
				}
			}
		}

		/// <summary> Adds a qualifier to the list of qualfiers in this TypeSelector.
		/// <para>
		///		The current TypeSelector is returned to permit method chaining.
		/// </para>
		/// </summary>
		/// <param name="qualifier"> A non-null, defined CssQualifier which will be appended to the list of qualifiers. </param>
		/// <returns></returns>

		public SimpleSelectorSeq Add ( CssQualifier qualifier ) {
			Assert.IsTrue ( Script.IsValue( qualifier ), "TypeSelector.Add: qualifier must have a value." );
			this.qualifiers.Add( qualifier );
			return this;
		}

		public override string ToString () {
			return ( Script.IsValue( typeSelector ) ? "" : typeSelector.ToString() ) + qualifiers.Join( "" );
		}
	}
}
