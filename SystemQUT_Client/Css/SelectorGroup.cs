// SelectorGroup.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	/// <summary> A comma-separated group of selectors:
	/// <pre>
	///		selectors_group
	///		  : selector [ COMMA S* selector ]*
	///		  ;
	/// </pre>
	/// </summary>

	public class SelectorGroup: Selector {
		private Selector head;
		private List<Selector> tail;
		
		/// <summary> Initialises a new SelectorSequence.
		/// </summary>
		/// <param name="head"> The first selector in the sequence. </param>
		/// <param name="tail"> An optional list of additional selectors. </param>

		public SelectorGroup( Selector head, List<Selector> tail ) {
			Assert.IsTrue( Script.IsValue( head ), "SelectorGroup.ctor: head may not be null." );

			this.head = head;
			this.tail = new List<Selector>();
			
			if ( tail != null ) {
				for ( int i = 0; i < tail.Length; i++ ) {
					this.tail.Add( tail[i] );
				}
			}
		}

		/// <summary> Adds a new selector to the tail of the SelectorGroup.
		/// </summary>
		/// <param name="tailItem"> A non-null selector. </param>

		public void Add( Selector tailItem ) {
			Assert.IsTrue( tailItem != null, "SelectorGroup.Add: tailItem may not be null." );

			tail.Add( tailItem );
		}

		/// <summary> Emits a string containing the contents of this SelectorGroup in a form 
		///		that is consistent with the grammar snippet above.
		/// </summary>
		/// <returns></returns>

		public override string ToString () {
			string result = head.ToString();
			
			if ( tail != null ) {
				result += ",";
				result += tail.Join(",");
			}

			return result;
		}
	}
}
