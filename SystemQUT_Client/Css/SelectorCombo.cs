﻿using System;
using System.Collections.Generic;


namespace SystemQut.Css {
	public class SelectorCombo: Selector {
		SimpleSelector head;
		List<CombinedSelector> tail;

		/// <summary> Initialises a Selector.
		/// </summary>
		/// <param name="head"></param>
		/// <param name="tail"></param>

		public SelectorCombo (
			SimpleSelector head,
			List<CombinedSelector> tail
		) {
			Assert.IsTrue( Script.IsValue( head ), "Selector.ctor: head may not be null." );

			this.head = head;
			this.tail = new List<CombinedSelector>();

			if ( tail != null ) {
				for ( int i = 0; i <= tail.Length; i++ ) {
					this.tail.Add( tail[i] );
				}
			}
		}

		/// <summary> Adds a further combined selector to the expression.
		/// </summary>
		/// <param name="selector">
		/// </param>
		/// <returns></returns>

		public SelectorCombo Add( CombinedSelector selector ) {
			Assert.IsTrue( Script.IsValue( selector), "SelectorCombo.Add: selector must have a value." );
			this.tail.Add( selector );
			return this;
		}

		/// <summary> Emits the Css selector represented by this SelectorCombo.
		/// </summary>
		/// <returns> Returns The Css selector represented by this SelectorCombo. </returns>

		override public string ToString () {
			return head.ToString() + tail.Join("");
		}
	}
}
