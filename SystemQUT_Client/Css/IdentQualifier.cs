// IdentQualifier.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	/// <summary> Represents a Css qualifier of the form "#xyzzy".
	/// </summary>

	public class IdentQualifier: CssQualifier {
		private string name;

		/// <summary> Initialises a Css qualifier of the form "#xyzzy".
		/// </summary>
		/// <param name="name">
		///		The ID to be processed.
		/// </param>

		public IdentQualifier( string name ) {
			Assert.IsFalse( String.IsNullOrWhiteSpace( name ), "IdentQualifier.ctor: name may not be null or blank/" );

			this.name = name;
		}

		/// <summary> Emits a string in the form: "#xyzzy".
		/// </summary>
		/// <returns>
		/// </returns>

		public override string ToString () {
			return "#" + name;
		}
	}
}
