// CombinedSimpleSelectorSeq.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut.Css {

	public class CombinedSelector {
		public const string DESCENDANT = " ";
		public const string CHILD = ">";
		public const string IMMEDIATELY_FOLLOWED_BY = "+";
		public const string FOLLOWED_BY = "~";

		private string combinator;
		private SimpleSelectorSeq selector;

		/// <summary> Initialises a CombinedSelector.
		/// </summary>
		/// <param name="combinator">
		///		The combination operator, which must be one of: DESCENDANT, CHILD, IMMEDIATELY_FOLLOWED_BY, FOLLOWED_BY.
		/// </param>
		/// <param name="selector">
		///		The simple selector sequence to be combined in a chain.
		/// </param>

		public CombinedSelector( 
			string combinator, 
			SimpleSelectorSeq selector 
		) {
			string [] validOps = {
				 DESCENDANT,
				 CHILD,
				 IMMEDIATELY_FOLLOWED_BY,
				 FOLLOWED_BY,
			};

			Assert.IsTrue( validOps.Contains( combinator), "CombinedSimpleSelector.ctor: invalid combinator." );
			Assert.IsTrue( Script.IsValue( selector ), "CombinedSimpleSelector.ctor: selector must have a value." );

			this.combinator = combinator;
			this.selector = selector;
		}

		/// <summary> Emits the Css Selector defined by this expression.
		/// </summary>
		/// <returns> Returns the Css Selector defined by this expression. </returns>

		public override string ToString () {
			return combinator + selector.ToString();
		}
	}
}
