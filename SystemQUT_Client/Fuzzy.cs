// Fuzzy.cs
//

using System;
using System.Collections.Generic;

namespace SystemQut {

	/// <summary> An enumeration representing a three-state boolean-like value, "yes-no-maybe".
	/// </summary>

	public enum Fuzzy {
		Maybe = 0,
		No = 1,
		Yes = 2,
	}
}
