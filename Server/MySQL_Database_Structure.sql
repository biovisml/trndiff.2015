CREATE DATABASE  IF NOT EXISTS `trndiff` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `trndiff`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: trndiff
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `genes`
--

DROP TABLE IF EXISTS `genes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genes` (
  `regulon_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `locus_tag` varchar(255) NOT NULL,
  `vimss_id` int(11) NOT NULL,
  `gene_function` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`locus_tag`,`vimss_id`,`regulon_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `genome_stats`
--

DROP TABLE IF EXISTS `genome_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genome_stats` (
  `genome_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `taxonomy_id` int(11) DEFAULT NULL,
  `tf_regulon_count` int(11) DEFAULT NULL,
  `tf_site_count` int(11) DEFAULT NULL,
  `rna_regulon_count` int(11) DEFAULT NULL,
  `rna_site_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`genome_id`),
  UNIQUE KEY `genome_id_UNIQUE` (`genome_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `genomes`
--

DROP TABLE IF EXISTS `genomes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genomes` (
  `genome_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `taxonomy_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`genome_id`),
  UNIQUE KEY `genome_id_UNIQUE` (`genome_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `go_regprecise_matches`
--

DROP TABLE IF EXISTS `go_regprecise_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `go_regprecise_matches` (
  `RP_geneFunction` varchar(255) NOT NULL,
  `GO_id` int(11) DEFAULT NULL,
  `Top_most_GO_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`RP_geneFunction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `go_term`
--

DROP TABLE IF EXISTS `go_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `go_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `term_type` varchar(55) NOT NULL,
  `acc` varchar(255) NOT NULL,
  `is_obsolete` int(11) NOT NULL DEFAULT '0',
  `is_root` int(11) NOT NULL DEFAULT '0',
  `is_relation` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `acc` (`acc`),
  UNIQUE KEY `t0` (`id`),
  KEY `t1` (`name`),
  KEY `t2` (`term_type`),
  KEY `t3` (`acc`),
  KEY `t4` (`id`,`acc`),
  KEY `t5` (`id`,`name`),
  KEY `t6` (`id`,`term_type`),
  KEY `t7` (`id`,`acc`,`name`,`term_type`)
) ENGINE=MyISAM AUTO_INCREMENT=44427 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `go_term2term`
--

DROP TABLE IF EXISTS `go_term2term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `go_term2term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_type_id` int(11) NOT NULL,
  `term1_id` int(11) NOT NULL,
  `term2_id` int(11) NOT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `term1_id` (`term1_id`,`term2_id`,`relationship_type_id`),
  KEY `tt1` (`term1_id`),
  KEY `tt2` (`term2_id`),
  KEY `tt3` (`term1_id`,`term2_id`),
  KEY `tt4` (`relationship_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=90997 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `go_term_synonym`
--

DROP TABLE IF EXISTS `go_term_synonym`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `go_term_synonym` (
  `term_id` int(11) NOT NULL,
  `term_synonym` varchar(996) DEFAULT NULL,
  `acc_synonym` varchar(255) DEFAULT NULL,
  `synonym_type_id` int(11) NOT NULL,
  `synonym_category_id` int(11) DEFAULT NULL,
  UNIQUE KEY `term_id` (`term_id`,`term_synonym`),
  KEY `synonym_type_id` (`synonym_type_id`),
  KEY `synonym_category_id` (`synonym_category_id`),
  KEY `ts1` (`term_id`),
  KEY `ts2` (`term_synonym`),
  KEY `ts3` (`term_id`,`term_synonym`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `previous_requests`
--

DROP TABLE IF EXISTS `previous_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `previous_requests` (
  `url` varchar(256) NOT NULL,
  PRIMARY KEY (`url`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regulators`
--

DROP TABLE IF EXISTS `regulators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulators` (
  `regulon_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `locus_tag` varchar(255) NOT NULL,
  `vimss_id` int(11) NOT NULL,
  `regulator_family` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`regulon_id`,`name`,`locus_tag`,`vimss_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regulog_collections`
--

DROP TABLE IF EXISTS `regulog_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulog_collections` (
  `class_name` varchar(255) DEFAULT NULL,
  `collection_id` int(11) NOT NULL,
  `collection_type` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`collection_id`,`collection_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regulogs`
--

DROP TABLE IF EXISTS `regulogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulogs` (
  `effector` varchar(255) DEFAULT NULL,
  `regulation_type` varchar(255) DEFAULT NULL,
  `regulator_family` varchar(255) DEFAULT NULL,
  `regulator_name` varchar(255) DEFAULT NULL,
  `regulog_id` int(10) unsigned NOT NULL,
  `taxon_name` varchar(255) DEFAULT NULL,
  `pathway` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`regulog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regulogs_regulog_collections`
--

DROP TABLE IF EXISTS `regulogs_regulog_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulogs_regulog_collections` (
  `collection_id` int(11) NOT NULL,
  `regulog_id` int(11) NOT NULL,
  `collection_type` varchar(255) NOT NULL,
  PRIMARY KEY (`collection_id`,`regulog_id`,`collection_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regulons`
--

DROP TABLE IF EXISTS `regulons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulons` (
  `regulon_id` int(11) NOT NULL,
  `regulog_id` int(11) DEFAULT NULL,
  `genome_id` int(11) DEFAULT NULL,
  `genome_name` varchar(255) DEFAULT NULL,
  `regulator_name` varchar(255) DEFAULT NULL,
  `regulator_family` varchar(255) DEFAULT NULL,
  `regulation_type` varchar(255) DEFAULT NULL,
  `effector` varchar(255) DEFAULT NULL,
  `pathway` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`regulon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `gene_locus_tag` varchar(255) NOT NULL,
  `gene_vimss_id` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `regulon_id` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL,
  `sequence` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`gene_locus_tag`,`regulon_id`,`gene_vimss_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'trndiff'
--

--
-- Dumping routines for database 'trndiff'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-13 15:15:47
