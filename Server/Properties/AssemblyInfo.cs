﻿// AssemblyInfo.cs
//

using System;
using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "TrnDiffNode" )]
[assembly: AssemblyDescription( "" )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "QUT" )]
[assembly: AssemblyProduct( "TrnDiffNode" )]
[assembly: AssemblyCopyright( "Copyright © QUT 2013" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

[assembly: ScriptAssembly( "TrnDiffNode" )]

// A script template allows customization of the generated script.
[assembly: ScriptTemplate( @"
// {name}.js {version}

{dependenciesLookup}
var $global = this;

{script}
" )]
