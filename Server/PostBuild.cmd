if exist bin\debug (
	cd bin\debug

	rem if not exist node_modules (
	rem 	md node_modules
	rem 	copy ss.js node_modules
	rem 	copy ss.min.js node_modules
	rem )
	rem 
	rem copy ..\..\..\RegPrecise_Server\bin\Debug\*.js node_modules

	cd ..

	if exist TRNDiff (
		rd TRNDiff /S /Q
	)

	md TRNDiff
	xcopy ..\Installer TRNDiff /E /I
	xcopy Debug TRNDiff\Application /E /I

	rem dir TRNDiff /S

	cd ..
)

if exist bin\TRNDiff\Application\WebSite\RegulonExplorer (
	cd bin\TRNDiff\Application\WebSite\RegulonExplorer
	if exist build_date.txt (
		del build_date.txt
	)
	echo %date% > build_date.txt
)