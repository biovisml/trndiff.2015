﻿using System;
using System.Collections.Generic;
using NodeApi.IO;
using NodeApi.Network;
using RegPrecise;
using System.Serialization;
using SystemQut;
using SystemQut.Web;
using Console = NodeApi.IO.Console;
using RegPrecise.Test;

namespace QUT.Bio.Server {

	static internal class Server {

		/// <summary> Maps file extensions to content type for statically served files.
		/// </summary>
		private static Dictionary<string, string> MimeTypes = new Dictionary<string, string>();

		/// <summary> Gets a list of the default documents that will be searched for if a
		/// requested Url looks like a directory.
		/// </summary>
		private static string[] DefaultDocuments = {
			"Index.html", "index.html",
			"Index.htm", "index.htm",
			"Default.html", "default.htm",
		};

		/// <summary> Gets a list of web services implemented by this server.
		/// </summary>

		private static readonly Dictionary<string, Func<IService>> WebServices = new Dictionary<string, Func<IService>>();

		/// <summary> The location in the files system where the web application static content
		/// is to be found.
		/// </summary>
		private static string WebSiteLocation = "WebSite";

		/// <summary> Static initialisation.
		/// </summary>
		static Server () {
			#region Initialise MIME types
			string[][] mimeMappings = {
				#region MIME types.
				new string [] { ".323", "text/h323" },
				new string [] { ".aaf", "application/octet-stream"},
				new string [] { ".aca", "application/octet-stream"},
				new string [] { ".accdb", "application/msaccess"},
				new string [] { ".accde", "application/msaccess"},
				new string [] { ".accdt", "application/msaccess"},
				new string [] { ".acx", "application/internet-property-stream"},
				new string [] { ".afm", "application/octet-stream"},
				new string [] { ".ai", "application/postscript"},
				new string [] { ".aif", "audio/x-aiff"},
				new string [] { ".aifc", "audio/aiff"},
				new string [] { ".aiff", "audio/aiff"},
				new string [] { ".application", "application/x-ms-application"},
				new string [] { ".art", "image/x-jg"},
				new string [] { ".asd", "application/octet-stream"},
				new string [] { ".asf", "video/x-ms-asf"},
				new string [] { ".asi", "application/octet-stream"},
				new string [] { ".asm", "text/plain"},
				new string [] { ".asr", "video/x-ms-asf"},
				new string [] { ".asx", "video/x-ms-asf"},
				new string [] { ".atom", "application/atom+xml"},
				new string [] { ".au", "audio/basic"},
				new string [] { ".avi", "video/x-msvideo"},
				new string [] { ".axs", "application/olescript"},
				new string [] { ".bas", "text/plain"},
				new string [] { ".bcpio", "application/x-bcpio"},
				new string [] { ".bin", "application/octet-stream"},
				new string [] { ".bmp", "image/bmp"},
				new string [] { ".c", "text/plain"},
				new string [] { ".cab", "application/octet-stream"},
				new string [] { ".calx", "application/vnd.ms-office.calx"},
				new string [] { ".cat", "application/vnd.ms-pki.seccat"},
				new string [] { ".cdf", "application/x-cdf"},
				new string [] { ".chm", "application/octet-stream"},
				new string [] { ".class", "application/x-java-applet"},
				new string [] { ".clp", "application/x-msclip"},
				new string [] { ".cmx", "image/x-cmx"},
				new string [] { ".cnf", "text/plain"},
				new string [] { ".cod", "image/cis-cod"},
				new string [] { ".cpio", "application/x-cpio"},
				new string [] { ".cpp", "text/plain"},
				new string [] { ".crd", "application/x-mscardfile"},
				new string [] { ".crl", "application/pkix-crl"},
				new string [] { ".crt", "application/x-x509-ca-cert"},
				new string [] { ".csh", "application/x-csh"},
				new string [] { ".css", "text/css"},
				new string [] { ".csv", "application/octet-stream"},
				new string [] { ".cur", "application/octet-stream"},
				new string [] { ".dcr", "application/x-director"},
				new string [] { ".deploy", "application/octet-stream"},
				new string [] { ".der", "application/x-x509-ca-cert"},
				new string [] { ".dib", "image/bmp"},
				new string [] { ".dir", "application/x-director"},
				new string [] { ".disco", "text/xml"},
				new string [] { ".dll", "application/x-msdownload"},
				new string [] { ".dll.config", "text/xml"},
				new string [] { ".dlm", "text/dlm"},
				new string [] { ".doc", "application/msword"},
				new string [] { ".docm", "application/vnd.ms-word.document.macroEnabled.12"},
				new string [] { ".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
				new string [] { ".dot", "application/msword"},
				new string [] { ".dotm", "application/vnd.ms-word.template.macroEnabled.12"},
				new string [] { ".dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
				new string [] { ".dsp", "application/octet-stream"},
				new string [] { ".dtd", "text/xml"},
				new string [] { ".dvi", "application/x-dvi"},
				new string [] { ".dwf", "drawing/x-dwf"},
				new string [] { ".dwp", "application/octet-stream"},
				new string [] { ".dxr", "application/x-director"},
				new string [] { ".eml", "message/rfc822"},
				new string [] { ".emz", "application/octet-stream"},
				new string [] { ".eot", "application/octet-stream"},
				new string [] { ".eps", "application/postscript"},
				new string [] { ".etx", "text/x-setext"},
				new string [] { ".evy", "application/envoy"},
				new string [] { ".exe", "application/octet-stream"},
				new string [] { ".exe.config", "text/xml"},
				new string [] { ".faa", "text/plain"},
				new string [] { ".fasta", "text/plain"},
				new string [] { ".fdf", "application/vnd.fdf"},
				new string [] { ".fif", "application/fractals"},
				new string [] { ".fla", "application/octet-stream"},
				new string [] { ".flr", "x-world/x-vrml"},
				new string [] { ".flv", "video/x-flv"},
				new string [] { ".fna", "text/plain"},
				new string [] { ".gb", "text/plain"},
				new string [] { ".gbk", "text/plain"},
				new string [] { ".gbk", "text/plain"},
				new string [] { ".gif", "image/gif"},
				new string [] { ".gif", "image/gif"},
				new string [] { ".gtar", "application/x-gtar"},
				new string [] { ".gz", "application/x-gzip"},
				new string [] { ".h", "text/plain"},
				new string [] { ".hdf", "application/x-hdf"},
				new string [] { ".hdml", "text/x-hdml"},
				new string [] { ".hhc", "application/x-oleobject"},
				new string [] { ".hhk", "application/octet-stream"},
				new string [] { ".hhp", "application/octet-stream"},
				new string [] { ".hlp", "application/winhlp"},
				new string [] { ".hqx", "application/mac-binhex40"},
				new string [] { ".hta", "application/hta"},
				new string [] { ".htc", "text/x-component"},
				new string [] { ".htm", "text/html"},
				new string [] { ".html", "text/html"},
				new string [] { ".htt", "text/webviewhtml"},
				new string [] { ".hxt", "text/html"},
				new string [] { ".ico", "image/x-icon"},
				new string [] { ".ics", "application/octet-stream"},
				new string [] { ".ief", "image/ief"},
				new string [] { ".iii", "application/x-iphone"},
				new string [] { ".inf", "application/octet-stream"},
				new string [] { ".ins", "application/x-internet-signup"},
				new string [] { ".isp", "application/x-internet-signup"},
				new string [] { ".IVF", "video/x-ivf"},
				new string [] { ".jar", "application/java-archive"},
				new string [] { ".java", "application/octet-stream"},
				new string [] { ".jck", "application/liquidmotion"},
				new string [] { ".jcz", "application/liquidmotion"},
				new string [] { ".jfif", "image/pjpeg"},
				new string [] { ".jpb", "application/octet-stream"},
				new string [] { ".jpe", "image/jpeg"},
				new string [] { ".jpeg", "image/jpeg"},
				new string [] { ".jpg", "image/jpeg"},
				new string [] { ".js", "application/x-javascript"},
				new string [] { ".json", "application/json"},
				new string [] { ".jsx", "text/jscript"},
				new string [] { ".latex", "application/x-latex"},
				new string [] { ".lit", "application/x-ms-reader"},
				new string [] { ".lpk", "application/octet-stream"},
				new string [] { ".lsf", "video/x-la-asf"},
				new string [] { ".lsx", "video/x-la-asf"},
				new string [] { ".lzh", "application/octet-stream"},
				new string [] { ".m13", "application/x-msmediaview"},
				new string [] { ".m14", "application/x-msmediaview"},
				new string [] { ".m1v", "video/mpeg"},
				new string [] { ".m3u", "audio/x-mpegurl"},
				new string [] { ".man", "application/x-troff-man"},
				new string [] { ".manifest", "application/x-ms-manifest"},
				new string [] { ".map", "text/plain"},
				new string [] { ".mdb", "application/x-msaccess"},
				new string [] { ".mdp", "application/octet-stream"},
				new string [] { ".me", "application/x-troff-me"},
				new string [] { ".mht", "message/rfc822"},
				new string [] { ".mhtml", "message/rfc822"},
				new string [] { ".mid", "audio/mid"},
				new string [] { ".midi", "audio/mid"},
				new string [] { ".mix", "application/octet-stream"},
				new string [] { ".mmf", "application/x-smaf"},
				new string [] { ".mno", "text/xml"},
				new string [] { ".mny", "application/x-msmoney"},
				new string [] { ".mov", "video/quicktime"},
				new string [] { ".movie", "video/x-sgi-movie"},
				new string [] { ".mp2", "video/mpeg"},
				new string [] { ".mp3", "audio/mpeg"},
				new string [] { ".mpa", "video/mpeg"},
				new string [] { ".mpe", "video/mpeg"},
				new string [] { ".mpeg", "video/mpeg"},
				new string [] { ".mpg", "video/mpeg"},
				new string [] { ".mpp", "application/vnd.ms-project"},
				new string [] { ".mpv2", "video/mpeg"},
				new string [] { ".ms", "application/x-troff-ms"},
				new string [] { ".msi", "application/octet-stream"},
				new string [] { ".mso", "application/octet-stream"},
				new string [] { ".mvb", "application/x-msmediaview"},
				new string [] { ".mvc", "application/x-miva-compiled"},
				new string [] { ".nc", "application/x-netcdf"},
				new string [] { ".nsc", "video/x-ms-asf"},
				new string [] { ".nws", "message/rfc822"},
				new string [] { ".ocx", "application/octet-stream"},
				new string [] { ".oda", "application/oda"},
				new string [] { ".odc", "text/x-ms-odc"},
				new string [] { ".ods", "application/oleobject"},
				new string [] { ".one", "application/onenote"},
				new string [] { ".onea", "application/onenote"},
				new string [] { ".onetoc", "application/onenote"},
				new string [] { ".onetoc2", "application/onenote"},
				new string [] { ".onetmp", "application/onenote"},
				new string [] { ".onepkg", "application/onenote"},
				new string [] { ".osdx", "application/opensearchdescription+xml"},
				new string [] { ".p10", "application/pkcs10"},
				new string [] { ".p12", "application/x-pkcs12"},
				new string [] { ".p7b", "application/x-pkcs7-certificates"},
				new string [] { ".p7c", "application/pkcs7-mime"},
				new string [] { ".p7m", "application/pkcs7-mime"},
				new string [] { ".p7r", "application/x-pkcs7-certreqresp"},
				new string [] { ".p7s", "application/pkcs7-signature"},
				new string [] { ".pbm", "image/x-portable-bitmap"},
				new string [] { ".pcx", "application/octet-stream"},
				new string [] { ".pcz", "application/octet-stream"},
				new string [] { ".pdf", "application/pdf"},
				new string [] { ".pfb", "application/octet-stream"},
				new string [] { ".pfm", "application/octet-stream"},
				new string [] { ".pfx", "application/x-pkcs12"},
				new string [] { ".pgm", "image/x-portable-graymap"},
				new string [] { ".pko", "application/vnd.ms-pki.pko"},
				new string [] { ".pma", "application/x-perfmon"},
				new string [] { ".pmc", "application/x-perfmon"},
				new string [] { ".pml", "application/x-perfmon"},
				new string [] { ".pmr", "application/x-perfmon"},
				new string [] { ".pmw", "application/x-perfmon"},
				new string [] { ".png", "image/png"},
				new string [] { ".pnm", "image/x-portable-anymap"},
				new string [] { ".pnz", "image/png"},
				new string [] { ".pot", "application/vnd.ms-powerpoint"},
				new string [] { ".potm", "application/vnd.ms-powerpoint.template.macroEnabled.12"},
				new string [] { ".potx", "application/vnd.openxmlformats-officedocument.presentationml.template"},
				new string [] { ".ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12"},
				new string [] { ".ppm", "image/x-portable-pixmap"},
				new string [] { ".pps", "application/vnd.ms-powerpoint"},
				new string [] { ".ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
				new string [] { ".ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
				new string [] { ".ppt", "application/vnd.ms-powerpoint"},
				new string [] { ".pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
				new string [] { ".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
				new string [] { ".prf", "application/pics-rules"},
				new string [] { ".prm", "application/octet-stream"},
				new string [] { ".prx", "application/octet-stream"},
				new string [] { ".ps", "application/postscript"},
				new string [] { ".psd", "application/octet-stream"},
				new string [] { ".psm", "application/octet-stream"},
				new string [] { ".psp", "application/octet-stream"},
				new string [] { ".pub", "application/x-mspublisher"},
				new string [] { ".qt", "video/quicktime"},
				new string [] { ".qtl", "application/x-quicktimeplayer"},
				new string [] { ".qxd", "application/octet-stream"},
				new string [] { ".ra", "audio/x-pn-realaudio"},
				new string [] { ".ram", "audio/x-pn-realaudio"},
				new string [] { ".rar", "application/octet-stream"},
				new string [] { ".ras", "image/x-cmu-raster"},
				new string [] { ".rf", "image/vnd.rn-realflash"},
				new string [] { ".rgb", "image/x-rgb"},
				new string [] { ".rm", "application/vnd.rn-realmedia"},
				new string [] { ".rmi", "audio/mid"},
				new string [] { ".roff", "application/x-troff"},
				new string [] { ".rpm", "audio/x-pn-realaudio-plugin"},
				new string [] { ".rtf", "application/rtf"},
				new string [] { ".rtx", "text/richtext"},
				new string [] { ".scd", "application/x-msschedule"},
				new string [] { ".sct", "text/scriptlet"},
				new string [] { ".sea", "application/octet-stream"},
				new string [] { ".setpay", "application/set-payment-initiation"},
				new string [] { ".setreg", "application/set-registration-initiation"},
				new string [] { ".sgml", "text/sgml"},
				new string [] { ".sh", "application/x-sh"},
				new string [] { ".shar", "application/x-shar"},
				new string [] { ".sit", "application/x-stuffit"},
				new string [] { ".sldm", "application/vnd.ms-powerpoint.slide.macroEnabled.12"},
				new string [] { ".sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide"},
				new string [] { ".smd", "audio/x-smd"},
				new string [] { ".smi", "application/octet-stream"},
				new string [] { ".smx", "audio/x-smd"},
				new string [] { ".smz", "audio/x-smd"},
				new string [] { ".snd", "audio/basic"},
				new string [] { ".snp", "application/octet-stream"},
				new string [] { ".spc", "application/x-pkcs7-certificates"},
				new string [] { ".spl", "application/futuresplash"},
				new string [] { ".src", "application/x-wais-source"},
				new string [] { ".ssm", "application/streamingmedia"},
				new string [] { ".sst", "application/vnd.ms-pki.certstore"},
				new string [] { ".stl", "application/vnd.ms-pki.stl"},
				new string [] { ".sv4cpio", "application/x-sv4cpio"},
				new string [] { ".sv4crc", "application/x-sv4crc"},
				new string [] { ".svg", "image/svg+xml"},
				new string [] { ".swf", "application/x-shockwave-flash"},
				new string [] { ".t", "application/x-troff"},
				new string [] { ".tar", "application/x-tar"},
				new string [] { ".tcl", "application/x-tcl"},
				new string [] { ".tex", "application/x-tex"},
				new string [] { ".texi", "application/x-texinfo"},
				new string [] { ".texinfo", "application/x-texinfo"},
				new string [] { ".tgz", "application/x-compressed"},
				new string [] { ".thmx", "application/vnd.ms-officetheme"},
				new string [] { ".thn", "application/octet-stream"},
				new string [] { ".tif", "image/tiff"},
				new string [] { ".tiff", "image/tiff"},
				new string [] { ".toc", "application/octet-stream"},
				new string [] { ".tr", "application/x-troff"},
				new string [] { ".trm", "application/x-msterminal"},
				new string [] { ".tsv", "text/tab-separated-values"},
				new string [] { ".ttf", "application/octet-stream"},
				new string [] { ".txt", "text/plain"},
				new string [] { ".u32", "application/octet-stream"},
				new string [] { ".uls", "text/iuls"},
				new string [] { ".ustar", "application/x-ustar"},
				new string [] { ".vbs", "text/vbscript"},
				new string [] { ".vcf", "text/x-vcard"},
				new string [] { ".vcs", "text/plain"},
				new string [] { ".vdx", "application/vnd.ms-visio.viewer"},
				new string [] { ".vml", "text/xml"},
				new string [] { ".vsd", "application/vnd.visio"},
				new string [] { ".vss", "application/vnd.visio"},
				new string [] { ".vst", "application/vnd.visio"},
				new string [] { ".vsto", "application/x-ms-vsto"},
				new string [] { ".vsw", "application/vnd.visio"},
				new string [] { ".vsx", "application/vnd.visio"},
				new string [] { ".vtx", "application/vnd.visio"},
				new string [] { ".wav", "audio/wav"},
				new string [] { ".wax", "audio/x-ms-wax"},
				new string [] { ".wbmp", "image/vnd.wap.wbmp"},
				new string [] { ".wcm", "application/vnd.ms-works"},
				new string [] { ".wdb", "application/vnd.ms-works"},
				new string [] { ".wks", "application/vnd.ms-works"},
				new string [] { ".wm", "video/x-ms-wm"},
				new string [] { ".wma", "audio/x-ms-wma"},
				new string [] { ".wmd", "application/x-ms-wmd"},
				new string [] { ".wmf", "application/x-msmetafile"},
				new string [] { ".wml", "text/vnd.wap.wml"},
				new string [] { ".wmlc", "application/vnd.wap.wmlc"},
				new string [] { ".wmls", "text/vnd.wap.wmlscript"},
				new string [] { ".wmlsc", "application/vnd.wap.wmlscriptc"},
				new string [] { ".wmp", "video/x-ms-wmp"},
				new string [] { ".wmv", "video/x-ms-wmv"},
				new string [] { ".wmx", "video/x-ms-wmx"},
				new string [] { ".wmz", "application/x-ms-wmz"},
				new string [] { ".wps", "application/vnd.ms-works"},
				new string [] { ".wri", "application/x-mswrite"},
				new string [] { ".wrl", "x-world/x-vrml"},
				new string [] { ".wrz", "x-world/x-vrml"},
				new string [] { ".wsdl", "text/xml"},
				new string [] { ".wvx", "video/x-ms-wvx"},
				new string [] { ".x", "application/directx"},
				new string [] { ".xaf", "x-world/x-vrml"},
				new string [] { ".xaml", "application/xaml+xml"},
				new string [] { ".xap", "application/x-silverlight-app"},
				new string [] { ".xbap", "application/x-ms-xbap"},
				new string [] { ".xbm", "image/x-xbitmap"},
				new string [] { ".xdr", "text/plain"},
				new string [] { ".xht", "application/xhtml+xml"},
				new string [] { ".xhtml", "application/xhtml+xml"},
				new string [] { ".xla", "application/vnd.ms-excel"},
				new string [] { ".xlam", "application/vnd.ms-excel.addin.macroEnabled.12"},
				new string [] { ".xlc", "application/vnd.ms-excel"},
				new string [] { ".xlm", "application/vnd.ms-excel"},
				new string [] { ".xls", "application/vnd.ms-excel"},
				new string [] { ".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
				new string [] { ".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12"},
				new string [] { ".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
				new string [] { ".xlt", "application/vnd.ms-excel"},
				new string [] { ".xltm", "application/vnd.ms-excel.template.macroEnabled.12"},
				new string [] { ".xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
				new string [] { ".xlw", "application/vnd.ms-excel"},
				new string [] { ".xml", "text/xml"},
				new string [] { ".xof", "x-world/x-vrml"},
				new string [] { ".xpm", "image/x-xpixmap"},
				new string [] { ".xps", "application/vnd.ms-xpsdocument"},
				new string [] { ".xsd", "text/xml"},
				new string [] { ".xsf", "text/xml"},
				new string [] { ".xsl", "text/xml"},
				new string [] { ".xslt", "text/xml"},
				new string [] { ".xsn", "application/octet-stream"},
				new string [] { ".xtp", "application/octet-stream"},
				new string [] { ".xwd", "image/x-xwindowdump"},
				new string [] { ".z", "application/x-compress"},
				new string [] { ".zip", "application/x-zip-compressed"},
			#endregion
			};

			for ( int i = 0; i < mimeMappings.Length; i++ ) {
				MimeTypes[mimeMappings[i][0]] = mimeMappings[i][1];
			}
			#endregion

			#region Initialise services
			WebServices["RegPrecise.test"] = delegate() { return new RegPrecise_TestService(); };
			WebServices["RegPrecise.svc"] = delegate() { return new RegPrecise_Service(); };
			WebServices["LocalMySQL.svc"] = delegate() { return new LocalMySQL_Service(); };
			#endregion
		}

		/// <summary> Simple character-based encoding of a string.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>

		static public int Encode ( String s ) {
			int result = 0;
			int a = "a".CharCodeAt( 0 );

			s = s.ToLowerCase();

			for ( int i = 0; i < s.Length; i++ ) {
				int c = s.CharCodeAt( i );
				result *= 26;
				result += c - a;
			}

			return result & 0xffff;
		}

		/// <summary> The server request handler.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="response"></param>

		static public void Process ( HttpServerRequest request, HttpServerResponse response ) {
			if ( request.Method == HttpVerb.GET || request.Method == HttpVerb.POST ) {
				Console.Log( String.Format( "URL: '{0}'", request.Url ) );

				UrlData urlData = Url.Parse( request.Url );

				Console.Log( "Headers: " );

				foreach ( string key in request.Headers.Keys ) {
					Console.Log( String.Format( "\t{0}: {1}", key, request.Headers[key] ) );
				}

				Console.Log( String.Format( "Path: '{0}'", urlData.Path ) );
				Console.Log( String.Format( "PathName: '{0}'", urlData.PathName ) );
				Console.Log( String.Format( "QueryString: '{0}'", urlData.QueryString ) );

				// TODO: Replace this with some other, non-hard-coded, page reference.
				if ( urlData.PathName.Length == 1 ) {
					ReturnTestPage( response );
					return;
				}

				Dictionary<string, string> query = ParseQuery( urlData.QueryString );

				foreach ( string key in query.Keys ) {
					Console.Log( String.Format( "Query: {0} = {1}", key, query[key] ) );
				}

				string absolutePath = urlData.PathName.Substring( 1 );

				Console.Log( String.Format( "Absolute path = {0}", absolutePath ) );

				try {
					string[] pathFragments = absolutePath.Split( '/' );

					string webSite = pathFragments[0];

					if ( webSite == RegPreciseTest.WebSiteName ) {
						try {
							OperationType operation = (OperationType) (object) pathFragments[1]; // This works in generated JavaScript.
							RegPreciseTest.Process( operation, query, response );
						}
						catch ( Exception ex ) {
							if ( ex.Message.IndexOf( "Format error" ) >= 0 ) {
								HttpHelpers.ReturnErrorPage( response, HttpStatusCode.NotFound, "", "" );
							}
							else {
								throw ex;
							}
						}
					}
					else if ( WebServices.ContainsKey( webSite ) ) {
						try {
							Func<IService> factory = WebServices[webSite];
							IService service = factory();
							service.Process( pathFragments, query, request, response );
						}
						catch ( Exception ex ) {
							if ( ex.Message.IndexOf( "Format error" ) >= 0 ) {
								HttpHelpers.ReturnErrorPage( response, HttpStatusCode.NotFound, "", "" );
							}
							else {
								throw ex;
							}
						}
					}
					else {
						ReturnStaticContent( response, absolutePath );
					}
				}
				catch ( Exception ex ) {
					Console.Log( String.Format( "Error: {0}", ex.ToString() ) );
					HttpHelpers.ReturnErrorPage(
						response,
						HttpStatusCode.InternalServerError,
						String.Format( "Error while processing {0}", urlData.PathName ),
						ex.Message
					);
				}
			}
			else {
				ReturnTestPage( response );
			}
		}

		/// <summary> Parses the query string into name/value pairs.
		/// </summary>
		/// <param name="p"></param>
		/// <returns></returns>

		private static Dictionary<string, string> ParseQuery ( string p ) {
			// TODO: Determine whether the query string received is uri-encoded or not. The node.js documentation does not state the behaviour of the Url.Parse() fully.

			Dictionary<string, string> query = new Dictionary<string, string>();

			if ( string.IsNullOrWhiteSpace( p ) )
				return query;

			string[] expressions = p.Split( '&' );

			foreach ( string expr in expressions ) {
				string[] terms = expr.Split( '=' );

				if ( terms.Length == 2 ) {
					query[terms[0]] = terms[1];
				}
				else if ( terms.Length == 1 ) {
					query[terms[0]] = "true";
				}
				else
					throw new Exception( String.Format( "Invalid query expression: '{0}'", expr ) );
			}

			return query;
		}

		/// <summary> Emits a string as a HTML page.
		/// </summary>
		/// <param name="response">
		///		The Http response.
		/// </param>
		/// <param name="html">
		///		A string which is presumed to contain well-formed xhtml consistent with HTML5.
		/// </param>
		/// <param name="statusCode">
		///		The status code to return.
		/// </param>

		public static void ReturnHtml ( HttpServerResponse response, HttpStatusCode statusCode, string html ) {
			string mimeType = "text/html";
			HttpHelpers.ReturnContent( response, statusCode, html, mimeType );
		}

		/// <summary> Emits the test page (copied to the target folder on build) with no
		///		preprocessing.
		/// </summary>
		/// <param name="response">
		///		The Http response.
		/// </param>

		private static void ReturnTestPage ( HttpServerResponse response ) {
			string sourceFileName = "TestPage.html";
			ReturnStaticContent( response, sourceFileName );
		}

		/// <summary> Emits the contents of the named file with no preprocessing.
		/// </summary>
		/// <param name="response">
		///		The Http response.
		/// </param>
		/// <param name="sourceFileName">
		///		The name of the file to be served. This may be a relative or fully qualified path name.
		///		Relatively named files should be located within the directory subtree in which the application
		///		starts. this will probably be a copy of the bin/Debug or bin/Release directories.
		/// </param>

		private static void ReturnStaticContent ( HttpServerResponse response, string sourceFileName ) {
			try {
				string fileName = LocateActualFile( sourceFileName );

				if ( !Script.IsValue( fileName ) ) {
					string message = String.Format( "Error when reading '{0}'.", sourceFileName );
					Console.Log( message );
					HttpHelpers.ReturnErrorPage( response, HttpStatusCode.NotFound, message, "Requested document not found." );
				}

				string extension = fileName.Substring( fileName.LastIndexOf( '.' ) );

				if ( !MimeTypes.ContainsKey( extension ) ) {
					throw new Exception( string.Format( "Unrecognised document type {0}", extension ) );
				}

				FileSystem.ReadFile( fileName, delegate( Exception error, Buffer buffer ) {
					if ( Script.IsValue( error ) ) {
						string errorMessage = error.Message;
						HttpStatusCode statusCode = HttpStatusCode.InternalServerError;
						string message = String.Format( "Error when reading '{0}': {1}.", sourceFileName, errorMessage );

						HttpHelpers.ReturnErrorPage(
							response,
							statusCode,
							String.Format( "Error when reading '{0}'.", sourceFileName ),
							errorMessage
						);
					}
					else {
						HttpHelpers.ReturnBufferContent( response, HttpStatusCode.OK, buffer, MimeTypes[extension] );
					}
				} );
			}
			catch ( Exception ex ) {
				Console.Log( ex.Message );

				HttpHelpers.ReturnErrorPage(
					response,
					HttpStatusCode.InternalServerError,
					string.Format( "An error occurred while attempting to load page {0}", sourceFileName ),
					ex.Message
				);
			}
		}

		/// <summary> Returns the name of an existing file which "matches"
		///		file path in an incoming request.
		///	<para>
		///		If the file supplied is an actual file, then it is returned unaltered.
		/// </para>
		/// <para>
		///		If the file name supplied ends with a '/' or is the name of an
		///		existing directory, then it is treated as a directory and each of the
		///		default document names are searched for in that directory. The first
		///		match is returned.
		/// </para>
		/// <para>
		///		If no suitable file is found, returns null.
		/// </para>
		/// </summary>
		/// <param name="sourceFileName">
		///		The file or directory name specified in
		/// </param>
		/// <returns></returns>

		private static string LocateActualFile ( string sourceFileName ) {
			string fileName = WebSiteLocation + "/" + sourceFileName;

			bool isDir = fileName.EndsWith( '/' );

			if ( !isDir && IsDirectory( fileName ) ) {
				fileName = fileName + '/';
				isDir = true;
			}

			if ( isDir ) {
				Console.Log( String.Format( "Source file '{0}' appears to be a directory.", fileName ) );

				for ( int i = 0; i < DefaultDocuments.Length; i++ ) {
					string tryFileName = fileName + DefaultDocuments[i];
					Console.Log( String.Format( "Trying '{0}'.", tryFileName ) );

					if ( FileSystem.ExistsSync( tryFileName ) ) {
						Console.Log( "\tFound!" );
						return tryFileName;
					}
				}
			}
			else if ( FileSystem.ExistsSync( fileName ) ) {
				return fileName;
			}

			return null;
		}

		/// <summary> Returns true iff fileName exists and it is a directory.
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>

		private static bool IsDirectory ( string fileName ) {
			if ( !FileSystem.ExistsSync( fileName ) ) return false;

			FileStats stats = FileSystem.StatSync( fileName );
			return stats.IsDirectory();
		}

		/// <summary> Returns true iff fileName exists and it is a file.
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>

		private static bool IsFile ( string fileName ) {
			if ( !FileSystem.ExistsSync( fileName ) ) return false;

			FileStats stats = FileSystem.StatSync( fileName );
			return stats.IsFile();
		}
	}
}
