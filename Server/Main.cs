// Class1.cs
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using NodeApi;
using NodeApi.Network;
using NodeApi.IO;
using Console = NodeApi.IO.Console;
using QUT.Bio.Server;

[ScriptModule]
internal static class Main {

	static Main () {
		int port = 80;

		Node.Process.UncaughtException += delegate( Exception arg ) {
			// Console.Log( arg.Message );
			if ( port == 80 && arg.Message.StartsWith( "listen EACCES" ) ) {
				port = Server.Encode("TRNDiff");
				Http.CreateServer( Server.Process ).Listen( port );
				Console.Log( "Server ACTUALLY running on port " + port );
				return;
			}
			
			Console.Log( arg.StackTrace );

			if ( Script.IsValue( arg.InnerException ) ) {
				Console.Log( "Inner exception:" );
				Console.Log( arg.InnerException.StackTrace + "\n" );
			}
		};

		Http.CreateServer( Server.Process ).Listen( port );
		Console.Log( "Server running on port " + port );
	}
}
