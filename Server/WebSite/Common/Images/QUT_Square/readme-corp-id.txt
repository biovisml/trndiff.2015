Please find the QUT logo(s) as requested.

Please be advised that the logo is a Registered Trade Mark and its use is subject to approval by the Publications Manager (email a.isaak@qut.edu.au). Full details on correct usage of the logo are provided in the QUT Corporate Identity Manual (http://www.pubs.qut.edu.au/online/corporate_id/).

The QUT Corporate colour is from the Pantone Matching System and is specified as PMS 541 blue.  The logo may also be printed in other colours as detailed in the QUT Corporate Identity Manual.

The logo may be used with or without the University name underneath. If it is used with the name, it must be as the complete graphic supplied and not with the name typeset separately.

The logo may not be distorted in any way and there must be a minimum clear area around the logo as specified in the Corporate Identity Manual.

Please note: QUT promotional material may only be produced by QUT Publications or one of their approved, preferred suppliers.  Contact Publications on 3138 3141 for details. 

All promotional material must be approved for logo usage and Corporate Identity by the Publications Manager: email a.isaak@qut.edu.au or fax 3138 3573, prior to production.

If you require the logo in other sizes or file types, please email details to a.isaak@qut.edu.au

