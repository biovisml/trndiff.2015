var selected = [];              // used to store selected spoke of the wheels

var tooltip;                    // a mouseover tooltip showing info of the node

var colourSacle;                 // used to colour edges and nodes

var idList = [];                // used to store a list of wheel ids

var prefix = "wheelData-";      // used to name wheel data in session storage

var bioFunctionList = [];       // used to store a list of bioFunctions

var colourArray = [];            // used to store key (bioFunction) - value (colour code) pairs

var sideBySideExists = false;   // used when switching among views

// diameter of wheels
var smallSize = 250; 
var medSize = 400;
var fullSize = 550;
var defaultWheelGap = 60;
var fullSizeWheelGap = 100;


/**
* delete all wheels.
*/
function deleteAllWheels() {
    $("#allWheels").empty();
    //d3.selectAll("div.wheel").remove();
}

/**
* reset all wheels so that nothing is highlighted.
*/
function resetAllWheels() {
    clearSelected();
    repaint();
}

/**
* highlight the selected.
*/
function repaint() {
    
    // disable the find list when at least one node is selected
    $("#findList").attr("disabled", selected.length == 0 ? false : true);

    // change the opacity and the stroke as well for each node
    d3.selectAll(".node").attr("opacity", function () {
        var currentNode = d3.select(this);  // select the current node
        var spoke = currentNode.attr("spoke");

        if (selected.length == 0) { // if none is selected
            currentNode.style("stroke", "none");
            currentNode.select("circle").style("fill", function (d) { return getcolour(d); });
            return 1;
        } else if (selected.indexOf(spoke) != -1) { // if the current spoke is selected
            currentNode.style("stroke", "#302833");            
            return 1;
        } else {
            currentNode.style("stroke", "none");
            return 0.2;
        }        
    });

    // similar changes to links
    d3.selectAll(".link").attr("opacity", function () {
        var currentLink = d3.select(this);  // select the current link
        var spoke = currentLink.attr("spoke");

        if (selected.length == 0) { // if none is selected  
            currentLink.style("stroke", function (d) { return getcolour(d.target); });
            return 0.5;
        } else if (selected.indexOf(spoke) != -1) { // if the current spoke is selected  
            currentLink.style("stroke", "#302833");
            return 0.5;
        } else {
            currentLink.style("stroke", "#e7e7eb");
            return 0.2;
        }
    });

}

/**
* get the radius of the gene.
* @param gene This is a gene object.
* @return {number} This returns a the radius in number.
*/
function getRadius(gene) {
    if (isTF(gene)) {
        return 8;
    } else if (isTG(gene)) {
        return 4;
    } else {
        return 0;
    }
}

/**
* get the colour of the gene.
* @param gene This is a gene object.
* @return {string} This returns a colour code.
*/
function getcolour(gene) {

    // when the gene is a TF
    // colour rule order for TF    
    if (isTF(gene)) {

        // keep reference TF gray
        if ((gene.Genome == "reference") || (getLengthOfcolourArray() > 0)) {
            return "#e7e7eb";
        } else {
            return "#e2041b";
        }

    }
    
    // when the gene is a TG
    // colour rule order for TG
    // operation result > findMe > ref > none of all above
    if (isTG(gene)) {
        
        // this part is used to colour nodes in operations (AND, OR and XOR)
        if (gene.owner != null) {

            // owner = -1  ==>  wheel1
            // owner =  1  ==>  wheel2
            // owner =  0  ==>  both wheels
            if (gene.owner == 0) {
                return "#895b8a";
            } else if (gene.owner == -1) {
                return "#E6B422";
            } else if (gene.owner == 1) {
                return "#007b43";
            } else { 
                return "#e7e7eb";
            }            
        }

        // find me that bioFunction
        if (getLengthOfcolourArray() > 0) {
            
            if (getcolourFromcolourArray(gene.bioFunction) != undefined) {
                return getcolourFromcolourArray(gene.bioFunction);
            } else {
                return "#e7e7eb";
            }

        }

        // keep reference TG gray
        if (gene.parent.Genome == "reference") {
            return "#e7e7eb";
        } else {
            return colourSacle(gene.bioFunction);
        }
        
    }
}

/**
* tell if the gene has edge.
* @param gene This is a gene object.
* @return {bool} This returns a bool value.
*/
function hasEdge(gene) {
    return gene.weight != undefined && gene.weight == 1;
}

/**
* tell if the gene is a TF.
* @param gene This is a gene object.
* @return {bool} This returns a bool value.
*/
function isTF(gene) {
    return gene.TF != null;
}

/**
* tell if the gene is a TG.
* @param gene This is a gene object.
* @return {bool} This returns a bool value.
*/
function isTG(gene) {
    return gene.TG != null && gene.TG != undefined;
}

/**
* get the name of the gene.
* @param gene This is a gene object.
* @return {bool} This returns a bool value.
*/
function getName(gene) {
    return isTF(gene) ? gene.TF : gene.TG;
}

/**
* get the tooltip info of the gene.
* @param gene This is a gene object.
* @return {bool} This returns a bool value.
*/
function getTooltipInfo(gene) {

    if (isTG(gene)) {
        return getName(gene.parent) + " ==> " + getName(gene) + "<br />bioFunction: " + gene.bioFunction;
    } else if (isTF(gene)) {
        return getName(gene);
    } else {
        return "No tooltip info found.";
    }
}

/**
* parse the wheel data. so that no need to change the format of input file.
* @param jsonData This is a wheel data object.
* @return This returns a wheel data object which could be used by d3js tree layout.
*/
function parseWheelDataFromJsonData(jsonData) {
    var tempWheelData = new Object();
    tempWheelData.Genome = jsonData.Genome;
    tempWheelData.TF = jsonData.TF;
    tempWheelData.children = jsonData.TGs;  // used by d3js tree layout
    
    return tempWheelData;
}

/**
* draw a wagon wheel using wheel data. 
*
* code is adapted from "http://bl.ocks.org/4063550" and
* "http://bl.ocks.org/2952964".
*
* for a normal simple tree layout example,
* please go to "http://bl.ocks.org/1312406"
*
* @param wheelData This is a wheel data object.
* @param {pixel} dimSize This is the dimension of the wheel. Draw in full size if it is true.
* @param {string} locationId This is the id of where the wagon wheel will be appended to.
*/
function drawAWagonWheelFromWheelData(wheelData, dimSize, locationId) {
    
    var diameter;
    var size;
    var padding = 0;
    var location;    
    var gap = (dimSize == fullSize) ? fullSizeWheelGap : defaultWheelGap;
    
    diameter = dimSize;    
    size = [360, diameter / 2 - gap];

    // default location
    if (locationId == null) {
        location = "#allWheels";
    } else {
        location = locationId;
    }     
    
    // create the tooltip.
    if (tooltip == null) {
        tooltip = d3.select("body").append("div")
                    .attr("class", "tooltip")
                    .style("opacity", 0);
    }

    // used to colour the nodes.
    if (colourSacle == null) {
        colourSacle = d3.scale.category20();
    }

    // the d3 tree layout
    var tree = d3.layout.tree()
        .size(size)
        .separation(function (a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });

    // radial is used, therefore x means angle and y means diameter.
    var diagonal = d3.svg.diagonal.radial()
        .projection(function (d) {

            // ignore the control points to keep links straight.
            if (isTG(d) && hasEdge(d)) {
                return [d.y, d.x / 180 * Math.PI];
            } else {
                return [0, 0];
            }
        });
    
    // create parent element of the svg
    var div = d3.select(location).append("div")
        .attr("class", "wheel")
        .attr("id", wheelData.Genome)
    
    // draw the svg
    var svg = div.append("svg")    
        .attr("width", diameter + padding)
        .attr("height", diameter + padding)    
        .append("g")
        .attr("transform", "translate(" + (diameter + padding) / 2 + "," + (diameter + padding) / 2 + ")")
        .on("mousedown", repaint);

    // let d3js tree layout calculate the coordinates of all nodes and links.
    var nodes = tree.nodes(wheelData);
    var links = tree.links(nodes);
    
    // about the spoke
    // like a clock face
    // starting with the first TG (spoke = 1) passing 12 o'clock
    // TF spoke = 0

    // draw links
    var link = svg.selectAll(".link")
          .data(links)
        .enter().append("path")
          .attr("class", "link")
          .attr("spoke", function (d, i) { return i + 1; }) // used for highlighting
          .attr("d", diagonal)
          .style("stroke", function (d) { return getcolour(d.target); })
          .attr("opacity", 0.5);
    
    // draw nodes
    var node = svg.selectAll(".node")
          .data(nodes)
        .enter().append("g")
          .attr("class", "node")
          .attr("spoke", function (d, i) { return i; }) // used for highlighting
          .attr("transform", function (d) {
                        
              // make the label of TF easier to read.
              if (isTF(d)) {
                  return "rotate(-90)translate(0)";
              } else {
                  return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")";
              }
          })
          .on("mousedown", function () {
              var spoke = d3.select(this).attr("spoke");
              var indexOfSpoke = selected.indexOf(spoke);

              // push the spoke if not already in selected, vice versa.
              if (indexOfSpoke == -1) {
                  selected.push(spoke);
              } else {
                  selected.splice(indexOfSpoke, 1);
              }
          })
          .on("mouseover", showTooltip)
          .on("mousemove", function (d) { moveTooltip(d); })
          .on("mouseout", hideTooltip);
    
    // append circles
    node.append("circle")
          .attr("r", function (d) { return getRadius(d); })
          .style("fill", function (d) { return getcolour(d); });
          
    // append label to each node.
    if (dimSize == fullSize) {
        node.append("text")
          .attr("dy", ".31em")
          .attr("class", "geneLabel")
          .attr("text-anchor", function (d) {
              // make the label of TF easier to read
              if (isTF(d)) {
                  return "start";
              } else {
                  return d.x < 180 ? "start" : "end";
              }
          })
          .attr("transform", function (d) {
              // make the label of TF easier to read
              if (isTF(d)) {
                  return "rotate(90)translate(10)";
              } else {
                  return d.x < 180 ? "translate(8)" : "rotate(180)translate(-8)";
              }
          })
          .text(function (d) { return getName(d); });    
    }

    // append label of the genome
    svg.append("text")
            .attr("class", "genomeLabel")
            .attr("x", 0)
            .attr("y", diameter / 2 - 20)
            .text(wheelData.Genome)
            .attr("text-anchor", "middle");
}

/**
* show the tooltip. 
*/
function showTooltip() {
    tooltip.transition()
                .duration(300)
                .style("opacity", 1);
}

/**
* move the tooltip and get the info. 
*/
function moveTooltip(d) {   
    tooltip.html(getTooltipInfo(d))
                .style("left", (d3.event.pageX + 20) + "px")
                .style("top", (d3.event.pageY) + "px");
}

/**
* hide the tooltip. 
*/
function hideTooltip() {
    tooltip.transition()
                .duration(300)
                .style("opacity", 0);
}

/**
* remove the wheel data from session storage. 
*/
function clearWheelData() {

    // use idList to iterate through all elements and remove them all from session storage.
    for (var i in idList) {
        sessionStorage.removeItem(prefix + idList[i]);
    }

    // empty the list too.
    idList = [];
}

/**
* tell if the input is a wheel id.
* @param {string} id This is a input id.
* @return {bool} This returns a bool value.
*/
function isWheelDataId(id) {
    return id.substring(0, prefix.length) == prefix;
}

/**
* store all wheel data from json to session storage.
* @param {string} jsonUrl This is a input url.
*/
function storeAllWheelDataFromJson(jsonUrl) {

    clearWheelData();
    
    d3.json(jsonUrl, function (error, jsonDataArray) { 
        storeAllWheelDataFromJsonDataArray(jsonDataArray, displayAllWheels); 
    });
}

/**
* store all wheel data from json data array.
* @param jsonDataArray This is a input json data array.
* @param callback This is a callback function.
*/
function storeAllWheelDataFromJsonDataArray(jsonDataArray, callback) {
    // iterate through all elements
    jsonDataArray.forEach(function (d) {
        var wheelData = parseWheelDataFromJsonData(d);
        idList.push(wheelData.Genome);
        storeWheelData(wheelData);
    });
    callback(medSize);
}

/**
* get wheel data by id.
* @param {string} id This is a input id.
* @return This returns a wheel data object.
*/
function getWheelData(Id) {
    return JSON.parse(sessionStorage.getItem(prefix + Id));
}

/**
* store a wheel data object to session storage.
* @param wheelData This is a wheel data object. 
*/
function storeWheelData(wheelData) {
    sessionStorage.setItem(prefix + wheelData.Genome, JSON.stringify(wheelData));
}

/**
* draw all wagon wheels stored in session storage.
* @param {pixel} dimSize This is the wheel dimension. Draw in full size if it is true.
*/
function drawAllWagonWheelsFromSessionStorage(dimSize) { 

    for (var i in idList) {
        drawAWagonWheelById(idList[i], dimSize, null);        
    }     
}

/**
* draw a wagon wheel by id.
* @param {string} id This is a id of the wagon wheel.
* @param {pixel} dimSize This is the dimension size of the wheel. Draw in full size if it is true.
* @param {string} locationId This is the id of where the wagon wheel will be appended to.
*/
function drawAWagonWheelById(id, dimSize, locationId) {
    var wheelData = getWheelData(id);    
    drawAWagonWheelFromWheelData(wheelData, dimSize, locationId);    
}

/**
* display all wheels.
* @param {pixel} dimSize This the wheel dimension. Draw in full size if it is true.
*/
function displayAllWheels(dimSize) {    
    deleteAllWheels();
    cleanSideBySide();
    drawAllWagonWheelsFromSessionStorage(dimSize);
    displayFind();
    repaint();
}

/**
* clear selected. 
*/
function clearSelected() {
    selected = [];
}

/**
* clear all. 
*/
function clearAll() {
    clearSelected();
    clearWheelData();
    clearcolourArray();
    resetSideBySideExists();
    deleteAllWheels();
    deleteSideBySide();
    deleteFindList();    
}

/**
* load data from json url. 
*/
function loadDataFromJson() {
    clearAll();
    hideCenter();
    storeAllWheelDataFromJson($("#inputJsonUrl").val());
}

/**
* display two wheels side by side with two drop down list and the results of operations. 
*/
function displaySideBySide() {
    deleteAllWheels();
    showSideBySide();

    if (!sideBySideExists && idList.length > 0) {    
        createDropDownList("list1", idList, "#left", "#wheel1");
        createDropDownList("list2", idList, "#right", "#wheel2");
        sideBySideExists = true;
    }

    displayFind();
    performOperations();
    repaint();
}

/**
* set sideBySideExists to false. 
*/
function resetSideBySideExists() {
    sideBySideExists = false;
}

/**
* delete side by side view. 
*/
function deleteSideBySide() {
    $("#right").empty();
    $("#left").empty();
    $("#wheel1").empty();
    $("#wheel2").empty();
    $("#operationResult").empty();
}

/**
* show side by side view. 
*/
function showSideBySide() {
    $("#right").show();
    $("#left").show();
    $("#sideBySide").show();
    
    $("#right").append("Right TRN:");
    $("#left").append("Left TRN:")
}

/**
* clean side by side view. 
*/
function cleanSideBySide() {
    $("#right").hide();
    $("#left").hide();    
    $("#sideBySide").hide();    
}

/**
* perform AND, OR and XOR operations between two selected wheels.
*/
function performOperations() {    
    var id1 = $("#list1").val();
    var id2 = $("#list2").val();

    if (id1 != null && id2 != null) {
        $("#operationResult").html("<p><b>Set Operations</b></p><br/><img class='setOpLegend' src='images/setOperationLegend.png'/>");
        operation(id1, id2, logicOfAND, "AND");
        operation(id1, id2, logicOfOR, "OR");
        operation(id1, id2, logicOfXOR, "XOR");
    }
    repaint();
}

/**
* append a drop down list which draws a wheel and performs operations on change.
* @param {string} listId This is the id of the list.
* @param listOptions This is an array of options.
* @param {string} parentId This is the id of element where the list will be appended to.
* @param {string} locationId This is id of element where the list will append a wheel to.
*/
function createDropDownList(listId, listOptions, parentId, locationId) {

    $("#" + listId).remove();

    var select = $("<select />", { id: listId });

    for (var i in listOptions) {
        $("<option />", { value: listOptions[i], text: listOptions[i] }).appendTo(select);
    }
    
    select.appendTo(parentId);

    $("#" + listId).change(function () {        
        $(locationId).empty();
        drawAWagonWheelById($(this).val(), fullSize, locationId);        
        repaint();
        performOperations();
    });

    $(locationId).empty();
    drawAWagonWheelById(listOptions[0], fullSize, locationId);
}

/**
* perform an operation then draw the result.
* @param {string} wheelId1 This is a wheel id.
* @param {string} wheelId2 This is a wheel id.
* @param operationLogic This is function.
* @param {string} label This is the label text for the result.
*/
function operation(wheelId1, wheelId2, operationLogic, label) {
    
    // get the wheel data
    var wheelData1 = getWheelData(wheelId1);
    var wheelData2 = getWheelData(wheelId2);

    // prepare a temp wheel data object
    var tempWheelData = new Object();
    tempWheelData.Genome = label;
    tempWheelData.TF = "TF";

    // prepare a temp TG array
    var tempTGs = [];

    // iterate through all elements
    for (var i = 0; i < wheelData1.children.length; i++) {
        
        var tempTG = new Object();
        tempTG.TG = "TG" + i;

        // used to tell the owner of a gene in order to colour the nodes
        var ownedByWheel1 = isTG(wheelData1.children[i]) ? 1 : 0;
        var ownedByWheel2 = isTG(wheelData2.children[i]) ? 1 : 0;

        // use operation logic to complete the temp TG array
        if (operationLogic(isTG(wheelData1.children[i]), isTG(wheelData2.children[i]))) {
            tempTG.owner = ownedByWheel1 - ownedByWheel2;
            tempTG.weight = 
                operationLogic(hasEdge(wheelData1.children[i]), hasEdge(wheelData2.children[i])) ? 1 : 0;
            tempTGs.push(tempTG);
        } else {
            tempTGs.push(new Object());
        }
    }
    tempWheelData.children = tempTGs;
    drawAWagonWheelFromWheelData(tempWheelData, smallSize, "#operationResult");
}

/**
* perform AND operation.
* @param {bool} a This is a bool value.
* @param {bool} b This is a bool value.
* @return {bool} This returns a bool value.
*/
function logicOfAND(a, b) {
    return a && b;
}

/**
* perform OR operation.
* @param {bool} a This is a bool value.
* @param {bool} b This is a bool value.
* @return {bool} This returns a bool value.
*/
function logicOfOR(a, b) {
    return a || b;
}

/**
* perform XOR operation.
* @param {bool} a This is a bool value.
* @param {bool} b This is a bool value.
* @return {bool} This returns a bool value.
*/
function logicOfXOR(a, b) {
    return !(a == b);
}

/**
* update the bioFunction list.
*/
function updateBioFunctionList() {
    bioFunctionList = [];

    for (var i in idList) {
        storeBioFunctionFromWheelData(getWheelData(idList[i]));
    }
}

/**
* store bioFunction from wheeldata to list.
* @param wheelData This is a wheelData object.
*/
function storeBioFunctionFromWheelData(wheelData) {

    for (var i in wheelData.children) {

        var bioFunction = wheelData.children[i].bioFunction;
        bioFunction = bioFunction == undefined ? "Undefined" : bioFunction;
        var indexOfBioFunction = bioFunctionList.indexOf(bioFunction);

        // store the bioFunction if not already stored
        if (indexOfBioFunction == -1) {
            bioFunctionList.push(bioFunction);
        }        
    }
}

/**
* append a drop down list which colour the selected bioFunction on change.
* @param {string} id This is the id of the list.
* @param options This is an array of options.
* @param {string} parentId This is the id of element where the list will be appended to.
*/
function createFindList(id, options, parentId) {
        
    $("#" + id).remove();     
      
    var select = $("<select />", { id: id });

    $("<option />", { value: "", text: "Find me a bioFunction..." }).appendTo(select);

    for (var i in options) {
        $("<option />", { value: options[i], text: options[i] }).appendTo(select);
    }
        
    select.appendTo(parentId);

    $("#" + id).change(function () {
        var selectedBioFunction = $(this).val();
        clearcolourArray();

        if (selectedBioFunction != ""){             
            setcolourArray(selectedBioFunction, "#E2041B");
        }    
        
        repaint();
    });    
}

/**
* clear the colour array.
*/
function clearcolourArray() {
    colourArray = [];
}

/**
* set a colour for a bioFunction.
* @param {string} bioFunction This is a bioFunction.
* @param {string} colour This is a colour code.
*/
function setcolourArray(bioFunction, colour) {
    colourArray[bioFunction] = colour;
}

/**
* get the colour code of a bioFunction.
* @param {string} bioFunction This is a bioFunction.
* @return {string} This returns a colour code.
*/
function getcolourFromcolourArray(bioFunction) {
    return colourArray[bioFunction];
}

/**
* get the length of colourArray. It is a key-value array so that the .length does not work.
* @return {number} This returns a number.
*/
function getLengthOfcolourArray() {
    var count = 0;

    for (var key in colourArray) {
        count += 1;
    }

    return count;
}

/**
* display the find list.
*/
function displayFind() {
    
    if (!($("#findList").length > 0) && dataLoaded()) {
        updateBioFunctionList();
        createFindList("findList", bioFunctionList, "#header");
    }
}

/**
* tell if data is loaded.
* @return {bool} This returns a bool value.
*/
function dataLoaded() {    
    return idList.length > 0;
}

/**
* delete the find list.
*/
function deleteFindList() {
    $("#findList").remove();
}

/**
* show the center div.
*/
function showCenter() {
    $("#center").show();
}

/**
* hide the center div.
*/
function hideCenter() {
    $("#center").hide();
}
