﻿using System;
using System.Collections.Generic;

using NodeApi.Network;
using QUT.Bio;
using SystemQut.Xml;
using SystemQut;
using Enum = SystemQut.Enum;
using SystemQut.ServiceModel;
using QUT.Bio.Server;

namespace RegPrecise.Test {
	internal class RegPreciseTest {

		/// <summary> The name of the pseudo-web site which is served up by the methods in the RegPreciseTest class.
		/// </summary>
		public static string WebSiteName = "RegPrecise.test";

		/// <summary> If the requested operation invokes a web service test, this will produce the response.
		/// </summary>
		/// <param name="operation">
		///		An enumerated (numeric) value which represents the requested operation. 
		/// </param>
		/// <param name="query">
		///		A dictionary containing the query as a set of name-value pairs.
		/// </param>
		/// <param name="response">
		///		The Http response object.
		/// </param>

		public static void Process (
			OperationType operation,
			Dictionary<string, string> query,
			HttpServerResponse response
		) {
			IRegPreciseService service = new RegPreciseService();

			switch ( operation ) {
				case OperationType.ListRegulogCollections:
					ReturnRegulogCollectionListHtml( query, response, service );
					break;
				case OperationType.ListGenomes:
					ReturnGenomeListHtml( response, service );
					break;
				case OperationType.ListRegulogs:
					ReturnRegulogListHtml( query, response, service );
					break;
				case OperationType.ListRegulonsInRegulog:
					ReturnRegulonsInRegulogHtml( query, response, service );
					break;
				default:
					throw new Exception( String.Format( "Operation '{0}' not implemented.", operation ) );
			}
		}

		/// <summary> Returns a HTML document containing the list of regulogs for a regulog collection.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="response"></param>
		/// <param name="service"></param>

		private static void ReturnRegulogListHtml (
			Dictionary<string, string> query,
			HttpServerResponse response,
			IRegPreciseService service
		) {
			RegulogCollectionType collectionType = (RegulogCollectionType) Enum.Parse( typeof( RegulogCollectionType ), query["collectionType"] );
			Number collectionId = Number.Parse( query["collectionId"] );

			service.ListRegulogs( collectionType, collectionId, delegate( ServiceResponse<Regulog []> result ) {
				if ( result.Error != null ) {
					HttpHelpers.ReturnErrorPage( response, HttpStatusCode.InternalServerError, result.Error, string.Empty );
					return;
				}

				Element html = HtmlUtil.CreateDocument( string.Format( "Regulogs for collection = {0}, id = {1}", query["collectionType"], collectionId ) );
				Element table = ( html.Children[1] as Element ).AppendNode( null, "table" );
				table.AddAttributes( new string[] { "border", "1", "cellspacing", "0" } );
				HtmlUtil.AppendRow( table, new string[] { 
					"Effector",
					"RegulationType",
					"RegulatorFamily",
					"RegulatorName",
					"RegulogId",
					"TaxonName",
					"Pathway",
					"Display"
				} );

				result.Content.Sort( delegate( object x, object y ) {
					return String.Compare( ( (RegulogCollection) x ).Name, ( (RegulogCollection) y ).Name );
				} );

				foreach ( Regulog regulog in result.Content ) {
					OperationType opType = OperationType.ListRegulonsInRegulog;
					string paramName = "regulogId";
					Number paramValue = regulog.RegulogId;
					string hrefText = "List members";
					Element memberlink = LinkToMembersOfCollectionForTest( opType, paramName, paramValue, hrefText );

					HtmlUtil.AppendRow( table, new object[] { 
						regulog.Effector,
						regulog.RegulationType,
						regulog.RegulatorFamily,
						regulog.RegulatorName,
						regulog.RegulogId,
						regulog.TaxonName,
						regulog.Pathway,
						memberlink,
					} );
				}

				Server.ReturnHtml( response, HttpStatusCode.OK, html.ToString() );
			} );
		}


		/// <summary> Generates a Html page containing thje results of the /genomes operation.
		/// </summary>
		/// <param name="response"></param>
		/// <param name="service"></param>

		private static void ReturnGenomeListHtml (
			HttpServerResponse response,
			IRegPreciseService service
		) {
			service.ListGenomes( delegate( ServiceResponse<Genome[]> result ) {
				Element html = HtmlUtil.CreateDocument( "RegPrecise Genomes with at least one regulon" );
				Element table = ( html.Children[1] as Element ).AppendNode( null, "table" );
				table.AddAttributes( new string[] { "border", "1", "cellspacing", "0" } );
				HtmlUtil.AppendRow( table, new string[] { "Genome Id", "Genome Name", "NCBI Taxonomy Id" } );

				result.Content.Sort( delegate( object x, object y ) {
					return String.Compare( ( (Genome) x ).Name, ( (Genome) y ).Name );
				} );

				foreach ( Genome genome in result.Content ) {
					HtmlUtil.AppendRow( table, new object[] { genome.GenomeId, genome.Name, genome.TaxonomyId } );
				}

				Server.ReturnHtml( response, HttpStatusCode.OK, html.ToString() );
			} );
		}

		/// <summary> Generates a Html page containing the results of the listRegulons operation.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="response"></param>
		/// <param name="service"></param>

		private static void ReturnRegulogCollectionListHtml (
			Dictionary<string, string> query,
			HttpServerResponse response,
			IRegPreciseService service
		) {
			RegulogCollectionType collectionType = (RegulogCollectionType) Enum.Parse( typeof( RegulogCollectionType ), query["collectionType"] );

			service.ListRegulogCollections(
				collectionType,
				delegate( ServiceResponse<RegulogCollection []> result ) {
					Element html = HtmlUtil.CreateDocument( "Regulog Collection " + query["collectionType"] );
					Element table = ( html.Children[1] as Element ).AppendNode( null, "table" );
					table.AddAttributes( new string[] { "border", "1", "cellspacing", "0" } );
					HtmlUtil.AppendRow( table, new string[] { "CollectionType", "CollectionId", "Name", "Class Name", "Display" } );

					result.Content.Sort( delegate( RegulogCollection x, RegulogCollection y ) {
						return String.Compare( x.Name, y.Name );
					} );

					foreach ( RegulogCollection collection in result.Content ) {
						Element memberlink = new Element( null, "a",
							new string[] { 
								"href", 
								string.Format( "listRegulogs?collectionType={0}&collectionId={1}", collection.CollectionType,  collection.CollectionId ) 
							},
							null
						);

						memberlink.AppendText( null, "List members" );

						HtmlUtil.AppendRow( table, new object[] { 
							collection.CollectionType, 
							collection.CollectionId, 
							collection.Name, 
							collection.ClassName,
							memberlink
						} );
					}

					Server.ReturnHtml( response, HttpStatusCode.OK, html.ToString() );
				}
			);
		}

		/// <summary> Returns a HTML document containing a tabular display of the regulons in a regulog.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="response"></param>
		/// <param name="service"></param>

		private static void ReturnRegulonsInRegulogHtml (
			Dictionary<string, string> query,
			HttpServerResponse response,
			IRegPreciseService service
		) {
			Number regulogId = Number.Parse( query["regulogId"] );

			// Console.Log( String.Format( "regulogId = {0}", regulogId ) );

			service.ListRegulonsInRegulog( regulogId, delegate( ServiceResponse<Regulon []> result ) {
				Element html = HtmlUtil.CreateDocument( "Regulons in Regulog " + query["regulogId"] );
				Element table = ( html.Children[1] as Element ).AppendNode( null, "table" );
				AddRegulonsToTable( result.Content, table );
				Server.ReturnHtml( response, HttpStatusCode.OK, html.ToString() );
			} );

		}

		/// <summary> Returns a HTML document containing a tabular display of the regulons in a regulog.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="response"></param>
		/// <param name="service"></param>

		private static void ReturnRegulonsInGenomeHtml (
			Dictionary<string, string> query,
			HttpServerResponse response,
			IRegPreciseService service
		) {
			Number genomeId = Number.Parse( query["genomeId"] );

			// Console.Log( String.Format( "regulogId = {0}", regulogId ) );

			service.ListRegulonsInGenome( genomeId, delegate( ServiceResponse<Regulon[]> result ) {
				Element html = HtmlUtil.CreateDocument( "Regulons in Genome " + query["genomeId"] );
				Element table = ( html.Children[1] as Element ).AppendNode( null, "table" );
				AddRegulonsToTable( result.Content, table );
				Server.ReturnHtml( response, HttpStatusCode.OK, html.ToString() );
			} );
		}

		private static void AddRegulonsToTable ( Regulon[] regulons, Element table ) {
			table.AddAttributes( new string[] { "border", "1", "cellspacing", "0" } );
			HtmlUtil.AppendRow( table, new string[] { 
					"RegulonId",
					"RegulogId",
					"GenomeId",
					"GenomeName",
					"RegulatorName",
					"RegulatorFamily",
					"RegulationType",
					"Effector",
					"Pathway" 
				} );

			regulons.Sort( delegate( object x, object y ) {
				return String.Compare( ( (Regulon) x ).GenomeName, ( (Regulon) y ).GenomeName );
			} );

			foreach ( Regulon regulon in regulons ) {
				HtmlUtil.AppendRow( table, new object[] { 
						regulon.RegulonId,
						regulon.RegulogId,
						HyperlinkToRegulonsInGenome(regulon.GenomeId),
						regulon.GenomeName,
						regulon.RegulatorName,
						regulon.RegulatorFamily,
						regulon.RegulationType,
						regulon.Effector,
						regulon.Pathway 
					} );
			}
		}

		private static object HyperlinkToRegulonsInGenome ( Number number ) {
			return HtmlUtil.CreateHyperlink( "", "Nowhere" );
		}

		/// <summary> Creates a hyperlink to the page in this application that displays the members of a given collection.
		/// </summary>
		/// <param name="opType"></param>
		/// <param name="paramName"></param>
		/// <param name="paramValue"></param>
		/// <param name="hrefText"></param>
		/// <returns></returns>

		private static Element LinkToMembersOfCollectionForTest ( OperationType opType, string paramName, Number paramValue, string hrefText ) {
			string href = string.Format( "{0}?{1}={2}", opType, paramName, paramValue );
			Element memberlink = HtmlUtil.CreateHyperlink( href, hrefText );
			return memberlink;
		}

	}
}
