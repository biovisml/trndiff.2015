﻿using System;
using System.Collections.Generic;

using System.Serialization;

namespace RegPrecise {

	/// <summary> Class ehich represents the regulation type used in a Regulog record. 
	/// </summary>

	public class RegulationTypes {
		private string name;

		private RegulationTypes ( string name ) {
			this.name = name;
		}

		public override string ToString () {
			return name;
		}

		public static readonly RegulationTypes TranscriptionFactor = new RegulationTypes( "TF" );

		public static readonly RegulationTypes RNA = new RegulationTypes( "RNA" );

		public static RegulationTypes Parse( object s ) {
			if ( s == null ) return null;

			if ( s is RegulationTypes ) return (RegulationTypes) s;

			if ( s.ToString() == "TF" ) return TranscriptionFactor;

			if ( s.ToString() == "RNA" ) return RNA;

			if ( s.GetType().Name == "Object" ) return Parse( (string) Script.Literal( "s._name" ) ); 

			Console.Log( "RegulationType = " + s );
			return null;
		}
	}
}
