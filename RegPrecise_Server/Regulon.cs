﻿using System;
using System.Collections.Generic;

using SystemQut;
using Enum = SystemQut.Enum;

namespace RegPrecise {

	/// <summary> The summary of a single regulon.
	/// <para>
	///		This class explicitly extends JSObject to provide a convenient extension mechanism
	///		for storage of additional application-specific properties.
	/// </para>
	/// </summary>

	public class Regulon : JSObject {

		/// <summary>  identifier of a regulon
		/// </summary>
		public Number RegulonId;

		/// <summary>  identifier of a regulog
		/// </summary>
		public Number RegulogId;

		/// <summary>  identifier of a genome
		/// </summary>
		public Number GenomeId;

		/// <summary>  name of a genome
		/// </summary>
		public string GenomeName;

		/// <summary>  name of a regulator
		/// </summary>
		public string RegulatorName;

		/// <summary>  famliy of a regulator
		/// </summary>
		public string RegulatorFamily;

		/// <summary>  type of regulation: either TF (transcription factor) or RNA
		/// </summary>
		public RegulationTypes RegulationType;

		/// <summary>  effector molecule or environmentla signal of a regulator
		/// </summary>
		public string Effector;

		/// <summary>  metabolic pathway or biological process controlled by regulator
		/// </summary>
		public string Pathway;

		//// <summary>  group number for the group function
		//// </summary>
		//public Number GroupNumber;

		//// <summary>  The distance from the left of the container when using
        //// Hammer.JS
		//// </summary>
        //public double PositionLeft = -1;

		//// <summary>  The distance from the top of the container when using
        //// Hammer.JS
		//// </summary>
        //public double PositionTop = -1;

		/// <summary> Construct a new Regulon record.
		/// </summary>
		/// <param name="regulonId"></param>
		/// <param name="regulogId"></param>
		/// <param name="genomeId"></param>
		/// <param name="genomeName"></param>
		/// <param name="regulatorName"></param>
		/// <param name="regulatorFamily"></param>
		/// <param name="regulationType"></param>
		/// <param name="effector"></param>
		/// <param name="pathway"></param>

		public Regulon (
			Number regulonId,
			Number regulogId,
			Number genomeId,
			string genomeName,
			string regulatorName,
			string regulatorFamily,
			RegulationTypes regulationType,
			string effector,
			string pathway
		) {
			IArrayLike<object> args = Arguments.Current;
			switch ( args.Length ) {
				case 0:
					break;
				case 1:
					InitFromObject( args[0] );
					break;
				case 2:
					JSObject obj = Objects.FromArrays<string>( (string[]) args[0], (string[]) args[1] );
					InitFromObject( obj );
					break;
				case 9:
					InitAllFields( regulonId, regulogId, genomeId, genomeName, regulatorName, regulatorFamily, regulationType, effector, pathway );
					break;
				default:
					throw new Exception( "Required arguments not provided." );
			}
		}

		private void InitAllFields (
			Number regulonId,
			Number regulogId,
			Number genomeId,
			string genomeName,
			string regulatorName,
			string regulatorFamily,
			RegulationTypes regulationType,
			string effector,
			string pathway
		) {
			this.RegulonId = regulonId;
			this.RegulogId = regulogId;
			this.GenomeId = genomeId;
			this.GenomeName = genomeName;
			this.RegulatorName = regulatorName;
			this.RegulatorFamily = regulatorFamily;
			this.RegulationType = regulationType;
			this.Effector = effector;
			this.Pathway = pathway;
		}

		private void InitFromObject ( object parms ) {
			JSObject rec = (JSObject) parms;

            InitAllFields(
				Number.Parse( Objects.ToString( rec["regulonId"], "" ) ),
				Number.Parse( Objects.ToString( rec["regulogId"], "" ) ),
				Number.Parse( Objects.ToString( rec["genomeId"], "" ) ),
				Objects.ToString( rec["genomeName"], "" ),
				Objects.ToString( rec["regulatorName"], "" ),
				Objects.ToString( rec["regulatorFamily"], "" ),
				RegulationTypes.Parse( rec["regulationType"] ),
				Objects.ToString( rec["effector"], "" ),
				Objects.ToString( rec["pathway"], "" )
			);

			Objects.ShallowCopy( rec, this, new string[] {
				"regulonId", "regulogId", "genomeId", "genomeName", "regulatorName", "regulatorFamily", "regulationType", "effector", "pathway"
			} );
		}

		/// <summary> Creates a new "empty" regulon.
		/// </summary>

		extern public Regulon ();

		/// <summary> Initialises a regulon record from a JavaScript object in which the fields
		///		are assumed to able to be converted to strings having suitable values.
		/// </summary>
		/// <param name="rec"></param>

		extern public Regulon ( JSObject rec );

		/// <summary> Initialises a regulon record from an array of strings, such as might be obtained from a CSV
		///		parser.
		/// </summary>
		/// <param name="fields"></param>

		extern public Regulon ( string[] fields );

		/// <summary> Initialises a Regulon from parallel arrays of strings
		///		which contain the keys and values as might be obtained by parsing a CSV file.
		/// </summary>
		/// <param name="keys"></param>
		/// <param name="values"></param>

		extern public Regulon ( string[] keys, string[] values );

		/// <summary> Factory method which constructs a Regulon using data obtained from a
		///		JavaScript object in which all fields are encoded as strings. Parse/Convert
		///		methods are used to extract the data.
		/// </summary>
		/// <param name="rec"> A JavaScript object which contains the required data encoded as strings. </param>
		/// <returns></returns>

		public static Regulon Parse ( JSObject rec ) {
			return new Regulon( rec );
		}

		public static int OrderById ( Regulon x, Regulon y ) {
			return x.RegulonId - y.RegulonId;
		}
	}
}
