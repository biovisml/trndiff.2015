﻿using System;
using System.Collections.Generic;

using SystemQut;
using Enum = SystemQut.Enum;

namespace RegPrecise {

	/// <summary> Record type returned by SearchRegulons and SearchExtRegulons.
	/// </summary>

	public class RegulonReference {
		/// <summary> identifier of regulon
		/// </summary>

		public int RegulonId;

		/// <summary> name of genome
		/// </summary>

		public string GenomeName;

		/// <summary> the name of regulator
		/// </summary>

		public string RegulatorName;

		/// <summary> found object type (either 'gene' or 'regulator')
		/// </summary>

		public ObjectTypes FoundObjType;

		/// <summary> found object name (or locusTag)
		/// </summary>

		public string FoundObjName;

		public RegulonReference (
			int regulonId,
			string genomeName,
			string regulatorName,
			ObjectTypes foundObjType,
			string foundObjName
		) {
			this.RegulonId = regulonId;
			this.GenomeName = genomeName;
			this.RegulatorName = regulatorName;
			this.FoundObjType = foundObjType;
			this.FoundObjName = foundObjName;
		}


		/// <summary> constructs a RegulonReference record from a JavaScript object which is presumed to contain the
		///		appropriate named fields, represented as strings.
		/// </summary>
		/// <param name="obj"> The source JavaScript object. </param>
		/// <returns></returns>

		public static RegulonReference Parse ( JSObject obj ) {
			return new RegulonReference(
				int.Parse( (string) obj["regulonId"] ),
				String.Convert( obj["genomeName"] ),
				String.Convert( obj["regulatorName"] ),
				ObjectTypes.Parse( (string) obj["foundObjType"] ),
				(string) obj["foundObjName"]
			);
		}

		/// <summary> Compares two RegulonReference objects by id.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>

		public static int OrderById ( RegulonReference x, RegulonReference y ) {
			return x.RegulonId - y.RegulonId;
		}

	}
}
