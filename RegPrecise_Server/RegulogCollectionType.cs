﻿using System;
using System.Collections.Generic;


namespace RegPrecise {

	/// <summary> Collection type stored in RegPregcise
	/// </summary>

	public class RegulogCollectionType {
		private string name;

		private RegulogCollectionType ( string name ) {
			this.name = name;
		}

		/// <summary> collections by taxonomic groups
		/// </summary>

		public readonly static RegulogCollectionType TaxGroup = new RegulogCollectionType( "taxGroup" );

		/// <summary> collections classified by orthologous transcription factor
		/// </summary>

		public readonly static RegulogCollectionType TranscriptionFactor = new RegulogCollectionType( "tf" );

		/// <summary> collections classified by transcription factor family
		/// </summary>

		public readonly static RegulogCollectionType TranscriptionFactorFamily = new RegulogCollectionType( "tfFam" );

		/// <summary> collections classified by RNA regulatory element family
		/// </summary>

		public readonly static RegulogCollectionType RnaFamily = new RegulogCollectionType( "rnaFam" );

		/// <summary> collections classified by metabolic pathway or biological process
		/// </summary>

		public readonly static RegulogCollectionType Pathway = new RegulogCollectionType( "pathway" );

		/// <summary> collections classified by effector molecule or environmental signal
		/// </summary>

		public readonly static RegulogCollectionType Effector = new RegulogCollectionType( "effector" );

		private static RegulogCollectionType[] all;

		public override string ToString () {
			return name.ToString();
		}

		public static RegulogCollectionType Parse ( object s ) {
			if ( s == null ) return null;

			if ( s is RegulogCollectionType ) return (RegulogCollectionType) s;

			if ( s is string ) {
				if ( all == null ) {
					all = new RegulogCollectionType[] {
						RegulogCollectionType.Effector,
						RegulogCollectionType.Pathway,
						RegulogCollectionType.RnaFamily,
						RegulogCollectionType.TaxGroup,
						RegulogCollectionType.TranscriptionFactor,
						RegulogCollectionType.TranscriptionFactorFamily,
					};
				}

				for ( int i = 0; i < all.Length; i++ ) {
					RegulogCollectionType current = all[i];
					if ( s.ToString() == current.name ) return current;
				}
			}

			if ( s.GetType().Name == "Object" ) return Parse( (string) Script.Literal( "s._name" ) );

			Console.Log( "RegulationType = " + s );
			return null;
		}
	}
}
