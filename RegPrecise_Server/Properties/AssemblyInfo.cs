﻿// AssemblyInfo.cs
//

using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "RegPrecise_Server" )]
[assembly: AssemblyDescription( "Server-side implementation of RegPrecise REST Web Service proxy." )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "Queensland University of Technology" )]
[assembly: AssemblyProduct( "RegPrecise_Server" )]
[assembly: AssemblyCopyright( "Copyright © 2013 Queensland University of Technology" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

[assembly: ScriptAssembly( "RegPrecise_Server" )]

// A script template allows customization of the generated script.
[assembly: ScriptTemplate( @"
// {name}.js {version}
// {description}

{dependenciesLookup}
var $global = this;

{script}
ss.extend(exports, $exports);
" )]
