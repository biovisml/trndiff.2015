﻿using System;
using System.Collections.Generic;

#if NOT_JS
using System.IO;
#else
using SystemQut.Linq;
using SystemQut;
using SystemQut.IO;
#endif

using SystemQUT.Csv;

namespace RegPrecise {
	/// <summary> A dataset is a collection of tables containing a snapshot of 
	///		a group of regulogs.
	///	<para>
	///		Note that some datasets may not contain values for some or all members.
	/// </para>
	/// </summary>

	public class DataSet {
		public Genome [] Genomes;
		public Regulog [] Regulogs;
		public Regulon [] Regulons;
		public Gene [] Genes;
		public Regulator [] Regulators;
		public RegulogCollection [] RegulogCollections;
		public Site [] Sites;

		public static DataSet Parse ( string text ) {
			#region Parse CSV into records.
			CsvIO csv = new CsvIO();
			StringReader reader = new StringReader( text );
			string [][] records = csv.Read( reader );
			#endregion

			#region Split records up into a collection of tables.
			List<string[]> regulogRecords = new List<string[]>();
			List<string[]> regulonRecords = new List<string[]>();
			List<string[]> geneRecords = new List<string[]>();
			List<string[]> regulatorRecords = new List<string[]>();
			List<string[]> siteRecords = new List<string[]>();
			List<string[]> genomeRecords = new List<string[]>();

			List<string[]> [] tables = { regulogRecords, regulonRecords, geneRecords, regulatorRecords, siteRecords, genomeRecords };
			string [] tableNames     = { "regulogs", "regulons", "genes", "regulators", "sites", "genomes" };

			List<string[]> currentTable = null;

			foreach ( string [] record in records ) {
				if ( record.Length == 0 ) continue;
				if ( Enumerable.All( record, delegate( string r ) { return string.IsNullOrEmpty( r ); } ) ) continue;

				int nameIndex = tableNames.IndexOf( record[0].ToLowerCase() );

				if ( nameIndex >= 0 ) {
					currentTable = tables[nameIndex];
				}
				else {
					currentTable.Add( record );
				}
			}
			#endregion

			#region Parse tables into lists of objects
			DataSet dataset = new DataSet();
			dataset.Regulogs = Objects.DeserializeRecords( regulogRecords ).Map<JSObject, Regulog>( Regulog.Parse );
			dataset.Regulons = Objects.DeserializeRecords( regulonRecords ).Map<JSObject, Regulon>( Regulon.Parse );
			dataset.Genes = Objects.DeserializeRecords( geneRecords ).Map<JSObject, Gene>( Gene.Parse );
			dataset.Regulators = Objects.DeserializeRecords( regulatorRecords ).Map<JSObject, Regulator>( Regulator.Parse );
			dataset.Sites = Objects.DeserializeRecords( siteRecords ).Map<JSObject, Site>( Site.Parse );
			dataset.Genomes = Objects.DeserializeRecords( siteRecords ).Map<JSObject, Genome>( Genome.Parse );
			#endregion

			return dataset;
		}

	}
}
