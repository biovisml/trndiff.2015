﻿using System;
using System.Collections.Generic;

using SystemQut.Web;
using NodeApi.Network;
using SystemQut;
using System.Serialization;
using SystemQut.ServiceModel;
using Enum = SystemQut.Enum;

namespace RegPrecise {

	/// <summary> A wrapper for the RegPrecise service proxy.
	/// </summary>

	public class RegPrecise_Service : IService {

		/// <summary> Processes a web request for the RegPrecise service.
		/// </summary>
		/// <param name="pathFragments"> The collection of slash-separated path fragments. </param>
		/// <param name="query"> The query part of the url. </param>
		/// <param name="request"> The HttpRequest object, which is needed if a POST request is encountered. </param>
		/// <param name="response"> A HttpResponse object which is used to transport the resulting document back to the caller. </param>

		public void Process (
			string[] pathFragments,
			Dictionary<string, string> query,
			HttpServerRequest request,
			HttpServerResponse response
		) {
			if ( request.Method != HttpVerb.POST ) {
				Console.Log( "non-post method received." );
				response.StatusCode = 405;
				response.EndOfData( "405 - Method not supported." );
			}

			StringBuilder sb = new StringBuilder();

			request.Readable += delegate() {
				sb.Append( request.ReadString() );
			};

			request.End += delegate() {
				JSObject req = (JSObject) Json.Parse( sb.ToString() );

				Console.Log( sb.ToString() );

				IRegPreciseService service = new RegPreciseService();
				OperationType operation = (OperationType) Enum.Parse( typeof( OperationType ), String.Convert( req["operation"] ) );

				Action<object> sendResponse = delegate( object responseObject ) {
					string responseBody = Json.Stringify( responseObject );
					response.SetHeader( "Content-Type", "application/json" );
					response.EndOfData( responseBody );
				};

				switch ( operation ) {
					case OperationType.ListGenomes:
						service.ListGenomes( (ResponseProcessor<Genome[]>) (object) sendResponse );
						break;
					case OperationType.GetRegulog:
						service.GetRegulog( Number.Convert( req["regulogId"] ), (ResponseProcessor<Regulog>) (object) sendResponse );
						break;
					case OperationType.SearchRegulons:
						service.SearchRegulons(
							ObjectTypes.Parse( req["objectType"] ),
							String.Convert( req["objectName"] ),
							(ResponseProcessor<RegulonReference[]>) (object) sendResponse
						);
						break;
					case OperationType.SearchExtRegulons:
						service.SearchExtRegulons(
							Number.Convert( req["ncbiTaxonomyId"] ),
							(string[]) req["objectName"],
							(ResponseProcessor<RegulonReference[]>) (object) sendResponse
						);
						break;

					case OperationType.ListGenomeStats:
						service.ListGenomeStats( (ResponseProcessor<GenomeStats[]>) (object) sendResponse );
						break;

					case OperationType.ListRegulatorsInRegulon:
						service.ListRegulatorsInRegulon(
							(int) req["regulonId"],
							(ResponseProcessor<Regulator[]>) (object) sendResponse
						);
						break;

					case OperationType.ListRegulatorsInRegulog:
						service.ListRegulatorsInRegulog(
							(int) req["regulogId"],
							(ResponseProcessor<Regulator[]>) (object) sendResponse
						);
						break;

					case OperationType.ListRegulogs:
						service.ListRegulogs(
							(RegulogCollectionType) req["collectionType"],
							(Number) req["collectionId"],
							(ResponseProcessor<Regulog[]>) (object) sendResponse
						);
						break;

					case OperationType.ListRegulogCollections:
						service.ListRegulogCollections(
							(RegulogCollectionType) req["collectionType"],
							(ResponseProcessor<RegulogCollection[]>) (object) sendResponse
						);
						break;

					case OperationType.ListSitesInRegulog:
						service.ListSitesInRegulog(
							(Number) req["regulogId"],
							(ResponseProcessor<Site[]>) (object) sendResponse
						);
						break;

					case OperationType.ListSitesInRegulon:
						service.ListSitesInRegulon(
							(Number) req["regulonId"],
							(ResponseProcessor<Site[]>) (object) sendResponse
						);
						break;

					case OperationType.GetRegulon:
						service.GetRegulon(
							(int) req["regulonId"],
							(ResponseProcessor<Regulon>) (object) sendResponse
						);
						break;

					case OperationType.ListGenesInRegulog:
						service.ListGenesInRegulog(
							(int) req["regulogId"],
							(ResponseProcessor<Gene[]>) (object) sendResponse
						);
						break;

					case OperationType.ListGenesInRegulon:
						service.ListGenesInRegulon(
							(int) req["regulonId"],
							(ResponseProcessor<Gene[]>) (object) sendResponse
						);
						break;

					case OperationType.ListRegulonsInGenome:
						service.ListRegulonsInGenome(
							(int) req["genomeId"],
							(ResponseProcessor<Regulon[]>) (object) sendResponse
						);
						break;

					case OperationType.ListRegulonsInRegulog:
						service.ListRegulonsInRegulog(
							(int) req["regulogId"],
							(ResponseProcessor<Regulon[]>) (object) sendResponse
						);
						break;

					default:
						sendResponse( new ServiceResponse<object>( null, "Not implemented." ) );
						break;
				}
			};
		}
	}
}
