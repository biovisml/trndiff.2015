﻿using System;
using System.Collections.Generic;


namespace RegPrecise {

	/// <summary> Enumeration of valid object types for the SearchRegulons operation.
	/// </summary>

	public class ObjectTypes {
		private string Name;

		private ObjectTypes ( string name ) {
			this.Name = name;
		}

		public static readonly ObjectTypes Gene = new ObjectTypes( "gene" );
		public static readonly ObjectTypes Regulator = new ObjectTypes( "regulator" );

		public override string ToString () {
			return Name;
		}

		public static ObjectTypes Parse( object s ) {
			if ( s == null ) return null;
			
			string value = s.ToString();

			if ( value == "gene" ) return Gene;
			
			if ( value == "regulator" ) return Regulator;
			
			if ( s.GetType().Name == "Object" )  return Parse( (string) Script.Literal( "s.name" ) ); 
			
			Console.Log( "RegulationType = " + s );
			return null;
		}
	}
}
