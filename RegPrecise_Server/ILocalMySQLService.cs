﻿using System;
using System.Collections.Generic;

using SystemQut.ServiceModel;

namespace RegPrecise
	//// <summary> This interface is the operation contract for accessing the
    //// Gene Ontology database via MySQL (currently locally)
	//// </summary>
{
    public interface ILocalMySQLService
    {

		/// <summary> Retrieves a list of all GO terms in the Gene Ontology
        /// database
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"> A callback that will process the results. </param>

		void GO_ListTerms (
			ResponseProcessor<GOTerm[]> processResults
		);

        /// <summary> Retrieves a list of all the synonyms of the specified
        /// GO term (from the database id)
		/// <para>
		///		Result is a list of term synoynms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>term_id</item>
        /// <item>term_synonym</item>
        /// <item>acc_synonym</item>
        /// <item>synonym_type_id</item>
        /// <item>synonym_category_id</item>
		/// </list>
		/// </summary>
		/// <param name="termId"></param>
		/// <param name="processResults"> A callback that will process the results. </param>

		void GO_ListSynonymsOfTerm (
            int termId,
			ResponseProcessor<GOTermSynonym[]> processResults
		);

        /// <summary> Retrieves a list of all the relationships that the
        /// specified GO term particpates in as a child
		/// <para>
		///		Result is a list of term relationships with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>relationship_type_id</item>
        /// <item>term1_id</item>
        /// <item>term2_id</item>
        /// <item>complete</item>
		/// </list>
		/// </summary>
		/// <param name="termId"></param>
		/// <param name="processResults"> A callback that will process the results. </param>

        void GO_ListRelationshipsOfTermAsChild (
            int termId,
            ResponseProcessor<GOTermRelationship[]> processResults
        );

		/// <summary> Retrieves the specified GO term
		/// <para>
		///		Result is a term with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
		/// </list>
		/// </summary>
        /// <param name="termId"></param>
		/// <param name="processResults"> A callback that will process the results. </param>

		void GO_GetTerm (
            int termId,
			ResponseProcessor<GOTerm> processResults
		);



        /// <summary> Retrieves a list of all GO terms in the Gene Ontology
        /// database, with all of their synonyms (if any)
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
        /// <item>term_synonyms</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"> A callback that will process the results. </param>

		void GO_ListTermsWithSynonyms (
			ResponseProcessor<GOTerm[]> processResults
		);

        /// <summary> Retrieves a list of all GO terms in the Gene Ontology
        /// database, with all of the relationships where they are a child
        /// (if any)
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
        /// <item>term_relationships</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"> A callback that will process the results. </param>

		void GO_ListTermsWithParents (
            ResponseProcessor<GOTerm[]> processResults
        );

        /// <summary> I have created a list of RegPrecise gene function terms
        /// matched up to the closest GO terms in the molecular function name
        /// space, and the (first) "top most parent" of that term. This
        /// retrives a list from that, with each gene function and their "top
        /// most parent"
		/// <para>
		///		Result is a JSObject dictionary with the gene function as the
        ///		key and the "category" as the value
        /// </para>
		/// <list type="bullet">
        /// <item>RP_geneFunction</item>
        /// <item>Top_most_GO_id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"> A callback that will process the results. </param>

		void GO_GetRegPreciseCategoryMatches (
            ResponseProcessor<GORegPreciseMatch[]> processResults
        );

        /// <summary> Gets the general statistics on regulons and regulatory sites in all genomes.
		/// </summary>
		/// <param name="processResults"> A delegate that will be invoked when the results are available. </param>

		void ListGenomeStats ( ResponseProcessor<GenomeStats[]> processResults );

		/// <summary> Lists the regulators in a regulon.
		/// </summary>
		/// <param name="regulonId"> The id of a regulon. </param>
		/// <param name="processResults"> A delegate that will process the results. </param>

		void ListRegulatorsInRegulon (
			int regulonId,
			ResponseProcessor<Regulator[]> processResults
		);

		/// <summary> Lists the regulators in a regulog.
		/// </summary>
		/// <param name="regulogId"> The id of a regulog. </param>
		/// <param name="processResults"> A delegate that will process the results. </param>

		void ListRegulatorsInRegulog (
			int regulogId,
			ResponseProcessor<Regulator[]> processResults
		);

		/// <summary> Gets the specified regulog descriptor.
		/// </summary>
		/// <param name="regulogId">The id of a regulog.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void GetRegulog (
			int regulogId,
			ResponseProcessor<Regulog> processResults
		);

		/// <summary> Gets the specified regulon descriptor.
		/// </summary>
		/// <param name="regulonId">The id of a regulon.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void GetRegulon (
			int regulonId,
			ResponseProcessor<Regulon> processResults
		);

        /// <summary> Retrieves a list of genomes that have at least one reconstructed regulon.
		/// <para>
		///		Result is a list of genomes with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>genomeId - genome identifier</item>
		/// <item>name - genome name</item>
		/// <item>taxonomyId - NCBI taxonomy id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"> A callback that will process the results. </param>

		void ListGenomes (
			ResponseProcessor<Genome[]> processResults
		);

		/// <summary> Retrieves a list of regulogs that belongs to a specific collection
		///	<para>
		///		Requires type and identifier of a regulog collection
		///	</para>
		///	<para>
		///		Returns a list of regulogs. Each regulog is provided with the following data:
		///	</para>
		///	<list type="bullet">
		///	<item>
		///		•regulogId - identifier of regulog
		///	</item>
		///	<item>
		///		•regulatorName - name of regulator
		///	</item>
		///	<item>
		///		•regulatorFamily - family of regulator
		///	</item>
		///	<item>
		///		•regulationType - type of regulation: either TF (transcription factor) or RNA
		///	</item>
		///	<item>
		///		•taxonName - name of taxonomic group
		///	</item>
		///	<item>
		///		•effector - effector molecule or environmental signal of a regulator
		///	</item>
		///	<item>
		///		•pathway - metabolic pathway or biological process controlled by a regulator
		///	</item>
		///	</list>
		/// </summary>

		void ListRegulogs (
			RegulogCollectionType collectionType,
			Number collectionId,
			ResponseProcessor<Regulog[]> processResults
		);

		/// <summary> Retrieves a list of regulons belonging to either a particular regulog.
		/// </summary>
		/// <param name="regulogId">
		///		The numeric regulog Id.
		///	</param>
		/// <param name="processResults">
		///		A delegate which will process the results.
		///	</param>

		void ListRegulonsInRegulog (
			Number regulogId,
			ResponseProcessor<Regulon[]> processResults
		);

		/// <summary> Retrieves a list of regulons belonging to a particular genome.
		/// </summary>
		/// <param name="genomeId">
		///		The genome Id.
		/// </param>
		/// <param name="processResults">
		///		A delegate which will process the results.
		///	</param>

		void ListRegulonsInGenome (
			Number genomeId,
			ResponseProcessor<Regulon[]> processResults
		);

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void ListSitesInRegulon (
			Number regulonId,
			ResponseProcessor<Site[]> processResults
		);

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void ListSitesInRegulog (
			Number regulogId,
			ResponseProcessor<Site[]> processResults
		);

		/// <summary> Gets the list of genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void ListGenesInRegulon (
			Number regulonId,
			ResponseProcessor<Gene[]> processResults
		);

		/// <summary> Gets the list of genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void ListGenesInRegulog (
			Number regulogId,
			ResponseProcessor<Gene[]> processResults
		);

		/// <summary> Get a list of regulog collections or the specified type.
		/// <para>
		///		Result is a list of regulog collections. Each regulog collection is provided with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>collectionType - type of regulog collection</item>
		/// <item>collectionId - identifier of collection</item>
		/// <item>name - collection name</item>
		/// <item>className - name of collection class </item>
		/// </list>
		/// </summary>
		/// <param name="collectionType"> The idneity of the required collection type. </param>
		/// <param name="processResults"> A callback that will process the results. </param>

		void ListRegulogCollections (
			RegulogCollectionType collectionType,
			ResponseProcessor<RegulogCollection[]> processResults
		);

        /// <summary>
        /// Adds the specified list of regulogs to the MySQL database
        /// <para>Will only add regulogs that do not already exist</para>
        /// <para>Should be used when the regulog is retrieved from RegPrecise using the ListRegulogs request</para>
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="collectionId"></param>
        /// <param name="regulogs"> The regulogs to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        void AddRegulogs_ListRegulogs (
			RegulogCollectionType collectionType,
			Number collectionId,
            Regulog[] regulogs,
            ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified regulog to the MySQL database
        /// <para>Will only add regulogs that do not already exist</para>
        /// <para>Should be used when the regulog is retrieved from RegPrecise using the GetRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulog"> The regulog to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        void AddRegulogs_GetRegulog (
			int regulogId,
            Regulog regulog,
            ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified regulons to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulons are retrieved from RegPrecise using the ListRegulonsInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulons"> The regulons to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        void AddRegulons_ListRegulonsInRegulog (
			Number regulogId,
            Regulon[] regulons,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified regulons to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulons are retrieved from RegPrecise using the ListRegulonsInRegulog request</para>
        /// </summary>
        /// <param name="genomeId"></param>
        /// <param name="regulons"> The regulons to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        void AddRegulons_ListRegulonsInGenome (
			Number genomeId,
            Regulon[] regulons,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified regulog collections to the MySQL database
        /// <para>Will only add regulog collections that do not already exist</para>
        /// <para>Should be used when the regulog collections are retrieved from RegPrecise using the ListRegulogCollections request</para>
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="regulogCollections"> The regulog collections to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddRegulogCollections_ListRegulogCollections (
			RegulogCollectionType collectionType,
            RegulogCollection[] regulogCollections,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified genomes to the MySQL database
        /// <para>Will only add genomes that do not already exist</para>
        /// <para>Should be used when the genomes are retrieved from RegPrecise using the ListGenomes request</para>
        /// </summary>
        /// <param name="genomes"> The genomes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddGenomes_ListGenomes (
            Genome[] genomes,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified sites to the MySQL database
        /// <para>Will only add sites that do not already exist</para>
        /// <para>Should be used when the sites are retrieved from RegPrecise using the ListSitesInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="sites"> The sites to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddSites_ListSitesInRegulon (
			Number regulonId,
            Site[] sites,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified sites to the MySQL database
        /// <para>Will only add sites that do not already exist</para>
        /// <para>Should be used when the sites are retrieved from RegPrecise using the ListSitesInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="sites"> The sites to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddSites_ListSitesInRegulog (
			Number regulogId,
            Site[] sites,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified regulon to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulon is retrieved from RegPrecise using the GetRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="regulon"> The regulon to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        void AddRegulons_GetRegulon (
			int regulonId,
            Regulon regulon,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified regulators to the MySQL database
        /// <para>Will only add regulators that do not already exist</para>
        /// <para>Should be used when the regulators are retrieved from RegPrecise using the ListRegulatorsInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="regulators"> The regulators to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddRegulators_ListRegulatorsInRegulon (
			int regulonId,
            Regulator[] regulators,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified regulators to the MySQL database
        /// <para>Will only add regulators that do not already exist</para>
        /// <para>Should be used when the regulators are retrieved from RegPrecise using the ListRegulatorsInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulators"> The regulators to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddRegulators_ListRegulatorsInRegulog (
			int regulogId,
            Regulator[] regulators,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified genes to the MySQL database
        /// <para>Will only add genes that do not already exist</para>
        /// <para>Should be used when the genes are retrieved from RegPrecise using the ListGenesInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="genes"> The genes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddGenes_ListGenesInRegulon (
			Number regulonId,
            Gene[] genes,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified genes to the MySQL database
        /// <para>Will only add genes that do not already exist</para>
        /// <para>Should be used when the genes are retrieved from RegPrecise using the ListGenesInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="genes"> The genes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddGenes_ListGenesInRegulog (
			Number regulogId,
            Gene[] genes,
			ResponseProcessor<Number> processResults
        );

        /// <summary>
        /// Adds the specified genome stats to the MySQL database
        /// <para>Will only add genome states that do not already exist</para>
        /// <para>Should be used when the genome stats are retrieved from RegPrecise using the ListGenomeStats request</para>
        /// </summary>
        /// <param name="genomeStats"> The genome stats to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        void AddGenomeStats_ListGenomeStats (
            GenomeStats[] genomeStats,
            ResponseProcessor<Number> processResults
        );
    }
}
