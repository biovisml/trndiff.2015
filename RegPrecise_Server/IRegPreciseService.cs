﻿using System;
using System.Collections.Generic;

using SystemQut.ServiceModel;

namespace RegPrecise {
	/// <summary> This interface the RegPrecise web service operation contract.
	/// </summary>

	public interface IRegPreciseService {

		/// <summary> Retrieves a list of regulon references by genome taxonomy id and comma-delimited list of gene locusTags.
		/// </summary>
		/// <param name="ncbiTaxonomyId"> The NCBI taxonomy Id of the containing organism. </param>
		/// <param name="locusTags"> A list of locus tags. </param>
		/// <param name="processResults"> A delegate that will be invoked to process the results. </param>

		void SearchExtRegulons (
			int ncbiTaxonomyId,
			string[] locusTags,
			ResponseProcessor<RegulonReference[]> processResults
		);

		/// <summary> Retrieves a list of regulon references by the name/locus tag of either regulator or target regulated genes 
		/// </summary>
		/// <param name="objectType"> The type of the target object. </param>
		/// <param name="objectName"> The regulator name or gene locus tag of the target object. </param>
		/// <param name="processResults"> A delegate that will be invoked to process the results when the operation is complete. </param>
		void SearchRegulons (
			ObjectTypes objectType,
			string objectName,
			ResponseProcessor<RegulonReference[]> processResults
		);

		/// <summary> Gets the general statistics on regulons and regulatory sites in all genomes.
		/// </summary>
		/// <param name="processResults"> A delegate that will be invoked when the results are available. </param>

		void ListGenomeStats ( ResponseProcessor<GenomeStats[]> processResults );

		/// <summary> Lists the regulators in a regulon.
		/// </summary>
		/// <param name="regulonId"> The id of a regulon. </param>
		/// <param name="processResults"> A delegate that will process the results. </param>

		void ListRegulatorsInRegulon (
			int regulonId,
			ResponseProcessor<Regulator[]> processResults
		);

		/// <summary> Lists the regulators in a regulog.
		/// </summary>
		/// <param name="regulogId"> The id of a regulog. </param>
		/// <param name="processResults"> A delegate that will process the results. </param>

		void ListRegulatorsInRegulog (
			int regulogId,
			ResponseProcessor<Regulator[]> processResults
		);

		/// <summary> Gets the specified regulog descriptor.
		/// </summary>
		/// <param name="regulogId">The id of a regulog.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void GetRegulog (
			int regulogId,
			ResponseProcessor<Regulog> processResults
		);

		/// <summary> Gets the specified regulon descriptor.
		/// </summary>
		/// <param name="regulonId">The id of a regulon.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void GetRegulon (
			int regulonId,
			ResponseProcessor<Regulon> processResults
		);

		/// <summary> Get a list of regulog collections or the specified type.
		/// <para>
		///		Result is a list of regulog collections. Each regulog collection is provided with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>collectionType - type of regulog collection</item>
		/// <item>collectionId - identifier of collection</item>
		/// <item>name - collection name</item>
		/// <item>className - name of collection class </item>
		/// </list>
		/// </summary>
		/// <param name="collectionType"> The idneity of the required collection type. </param>
		/// <param name="processResults"> A callback that will process the results. </param>

		void ListRegulogCollections (
			RegulogCollectionType collectionType,
			ResponseProcessor<RegulogCollection[]> processResults
		);

		/// <summary> Retrieves a list of genomes that have at least one reconstructed regulon.
		/// <para>
		///		Result is a list of genomes with the following data:
		/// </para>		
		/// <list type="bullet">
		/// <item>genomeId - genome identifier</item>
		/// <item>name - genome name</item>
		/// <item>taxonomyId - NCBI taxonomy id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		void ListGenomes (
			ResponseProcessor<Genome[]> processResults
		);

		/// <summary> Retrieves a list of regulogs that belongs to a specific collection
		///	<para>
		///		Requires type and identifier of a regulog collection 
		///	</para>	
		///	<para>
		///		Returns a list of regulogs. Each regulog is provided with the following data: 
		///	</para>	
		///	<list type="bullet">
		///	<item>
		///		•regulogId - identifier of regulog
		///	</item>
		///	<item>
		///		•regulatorName - name of regulator
		///	</item>
		///	<item>
		///		•regulatorFamily - family of regulator
		///	</item>
		///	<item>
		///		•regulationType - type of regulation: either TF (transcription factor) or RNA
		///	</item>
		///	<item>
		///		•taxonName - name of taxonomic group
		///	</item>
		///	<item>
		///		•effector - effector molecule or environmental signal of a regulator
		///	</item>
		///	<item>
		///		•pathway - metabolic pathway or biological process controlled by a regulator 
		///	</item>
		///	</list>
		/// </summary>

		void ListRegulogs (
			RegulogCollectionType collectionType,
			Number collectionId,
			ResponseProcessor<Regulog[]> processResults
		);

		/// <summary> Retrieves a list of regulons belonging to either a particular regulog.
		/// </summary>
		/// <param name="regulogId"> 
		///		The numeric regulog Id. 
		///	</param>
		/// <param name="processResults"> 
		///		A delegate which will process the results.
		///	</param>

		void ListRegulonsInRegulog (
			Number regulogId,
			ResponseProcessor<Regulon[]> processResults
		);

		/// <summary> Retrieves a list of regulons belonging to a particular genome.
		/// </summary>
		/// <param name="genomeId">
		///		The genome Id.
		/// </param>
		/// <param name="processResults"> 
		///		A delegate which will process the results.
		///	</param>

		void ListRegulonsInGenome (
			Number genomeId,
			ResponseProcessor<Regulon[]> processResults
		);

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void ListSitesInRegulon (
			Number regulonId,
			ResponseProcessor<Site[]> processResults
		);

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void ListSitesInRegulog (
			Number regulogId,
			ResponseProcessor<Site[]> processResults
		);

		/// <summary> Gets the list of genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void ListGenesInRegulon (
			Number regulonId,
			ResponseProcessor<Gene[]> processResults
		);

		/// <summary> Gets the list of genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		void ListGenesInRegulog (
			Number regulogId,
			ResponseProcessor<Gene[]> processResults
		);
	}
}
