﻿using System;
using System.Collections.Generic;
using SystemQut;

namespace RegPrecise
{
    public class GOTermSynonym : JSObject
    {
        /// <summary>
        /// The database id for the term this is a synonym of
        /// </summary>
        private int term_id;
        
        /// <summary>
        /// The synonym name
        /// </summary>
        private string term_synonym;
        
        /// <summary>
        /// The synonym's unique identifier
        /// </summary>
        private string acc_synonym;
        
        /// <summary>
        /// Something
        /// </summary>
        private int synonym_type_id;
        
        /// <summary>
        /// Something
        /// </summary>
        private int synonym_category_id;        

        public GOTermSynonym (int term_id, string term_synonym, string acc_synonym, int synonym_type_id, int synonym_category_id) {
            IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0: return;
				case 1: 
					SetFromJSObject( (JSObject) args[0] ); 
					break;
				case 5: 
					SetAllFields( term_id, term_synonym, acc_synonym, synonym_type_id, synonym_category_id ); 
					break;
				default: 
					throw new Exception( Globals.REQUIRED_ARGS );
			}
        }

		private void SetAllFields (int term_id, string term_synonym, string acc_synonym, int synonym_type_id, int synonym_category_id) {
			this.term_id = term_id;
            this.term_synonym = term_synonym;
            this.acc_synonym = acc_synonym;
            this.synonym_type_id = synonym_type_id;
            this.synonym_category_id = synonym_category_id;
		}

		/// <summary> Factory method to convert from JavaScript object to GOTermSynonym.
		/// </summary>
		/// <param name="obj">
		///		A JavaScript object that contains the required fields, encoded as strings.
		/// </param>
		/// <returns></returns>

		public static GOTermSynonym Parse ( JSObject obj ) {        
			return new GOTermSynonym( obj );
		}

		public static int OrderById ( GOTermSynonym x, GOTermSynonym y ) {
			return string.Compare(x.acc_synonym, y.acc_synonym);
		}

		/// <summary> Parameterless constructor.
		/// </summary>

		public extern GOTermSynonym ();

		/// <summary> Copy constructor accepting an arbitrary JavaScript object such as one obtains from Json.Parse.
		/// </summary>

		public extern GOTermSynonym ( JSObject obj );       

		/// <summary> Parses a GOTerm from a JavaScript object which is assumed to contain the required fields.
		/// </summary>

		private void SetFromJSObject ( JSObject record ) {
			SetAllFields(
                int.Parse(Objects.ToString(record["term_id"], "")),
                Objects.ToString(record["term_synonym"], ""),
                Objects.ToString(record["acc_synonym"], ""),
                int.Parse(Objects.ToString(record["synonym_type_id"], "")),
                int.Parse(Objects.ToString(record["synonym_category_id"], ""))
			);
			string [] fieldsExcludedFromCopy = {
				"term_id", "term_synonym", "acc_synonym", "synonym_type_id", "synonym_category_id"
			};
			Objects.ShallowCopy( record, this, fieldsExcludedFromCopy );
		}

        public int TermDatabaseId
        {
            get { return term_id; }
            set { term_id = value; }
        }

        public string Name
        {
            get { return term_synonym; }
            set { term_synonym = value; }
        }

        public string UniqueIdentifier
        {
            get { return acc_synonym; }
            set { acc_synonym = value; }
        }

        public int TypeId
        {
            get { return synonym_type_id; }
            set { synonym_type_id = value; }
        }        

        public int CategoryId
        {
            get { return synonym_category_id; }
            set { synonym_category_id = value; }
        }        
    }
}
