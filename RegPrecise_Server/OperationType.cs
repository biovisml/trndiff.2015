﻿using System;
using System.Collections.Generic;


namespace RegPrecise {

	/// <summary> The operations available in the RegPrecise proxy.
	/// </summary>

	// [ScriptConstants(UseNames=true)]
	public enum OperationType {
		ListRegulogCollections = 0,
		ListGenomes = 1,
		ListRegulogs = 2,
		ListRegulonsInRegulog = 3,
		ListRegulonsInGenome = 4,
		ListSitesInRegulon = 5,
		ListSitesInRegulog = 6,
		SearchExtRegulons = 7,
		SearchRegulons = 8,
		ListGenomeStats = 9,
		ListRegulatorsInRegulon = 10,
		ListRegulatorsInRegulog = 11,
		GetRegulog = 12,
		GetRegulon = 13,
		ListGenesInRegulon = 14,
		ListGenesInRegulog = 15,
	}
}
