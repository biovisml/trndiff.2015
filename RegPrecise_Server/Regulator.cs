﻿using System;
using System.Collections.Generic;

using SystemQut;

namespace RegPrecise {

	/// <summary> A Regulator
	/// </summary>

	public class Regulator: JSObject {
		/// <summary> identifier of regulon to which a regulator belongs to
		/// </summary>

		public int RegulonId;

		/// <summary> name of regulator
		/// </summary>

		public string Name;

		/// <summary> locus tag of regulator gene in GeneBank
		/// </summary>

		public string LocusTag;

		/// <summary> identifier of regulator gene in MicrobesOnline database 
		/// </summary>

		public int VimssId;

		/// <summary> family of a regulator
		/// </summary>

		public string RegulatorFamily;

		/// <summary> Initialises a new "empty" regulator.
		/// </summary>

		extern public Regulator ();

		/// <summary> Initialise a Regulator record, setting all fields explicitly.
		/// </summary>
		/// <param name="regulonId"></param>
		/// <param name="name"></param>
		/// <param name="locusTag"></param>
		/// <param name="vimssId"></param>
		/// <param name="regulatorFamily"></param>

		public Regulator (
			int regulonId,
			string name,
			string locusTag,
			int vimssId,
			string regulatorFamily
		) {
			IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0:
					return;
				case 1:
					SetFieldsFromObject( args[0] );
					return;
				case 5:
					SetAllFields( regulonId, name, locusTag, vimssId, regulatorFamily );
					return;
				default:
					throw new Exception( "Required arguments not supplied." );
			}
		}

		/// <summary> Sets the field values of the regulator from a JavaScript object
		///		which contains the required data, liekly represented as text values
		///		rather than native.
		/// </summary>
		/// <param name="obj"></param>

		extern public Regulator ( JSObject obj );

		/// <summary> Sets the field values of the regulator from a JavaScript object
		///		which contains the required data, liekly represented as text values
		///		rather than native.
		/// </summary>
		/// <param name="parms"></param>

		private void SetFieldsFromObject ( object parms ) {
			JSObject obj = (JSObject) parms;

			SetAllFields(
				Number.Parse( Objects.ToString( obj["regulonId"], "" ) ),
				Objects.ToString( obj["name"], "" ),
				Objects.ToString( obj["locusTag"], "" ),
				Number.Parse( Objects.ToString( obj["vimssId"], "" ) ),
				Objects.ToString( obj["regulatorFamily"], "" )
			);

			Objects.ShallowCopy( obj, this, new string [] {
				"regulonId", "name", "locusTag", "vimssId", "regulatorFamily"
			} );
		}

		/// <summary> Sets all fields of the regulator.
		/// </summary>
		/// <param name="regulonId"></param>
		/// <param name="name"></param>
		/// <param name="locusTag"></param>
		/// <param name="vimssId"></param>
		/// <param name="regulatorFamily"></param>

		public void SetAllFields (
			int regulonId,
			string name,
			string locusTag,
			int vimssId,
			string regulatorFamily
		) {
			this.RegulonId = regulonId;
			this.Name = name;
			this.LocusTag = locusTag;
			this.VimssId = vimssId;
			this.RegulatorFamily = regulatorFamily;
		}

		/// <summary> Creates a Regulator from a JavaScript object.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public static Regulator Parse ( JSObject obj ) {
			return new Regulator( obj );
		}

		/// <summary> Orders two regulators by their vimssid field.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>

		public static int OrderById ( Regulator x, Regulator y ) {
			return x.VimssId - y.VimssId;
		}
	}
}
