﻿using System;
using System.Collections.Generic;
using SystemQut;

namespace RegPrecise
{
    public class GOTermRelationship : JSObject
    {
        /// <summary>
        /// The database id
        /// </summary>
        private int id;
        
        /// <summary>
        /// The term that represents this relationship
        /// </summary>
        private int relationship_type_id;
        
        /// <summary>
        /// The term that is the "parent" of this relationship
        /// </summary>
        private int term1_id;
        
        /// <summary>
        /// The term that is the "child" of this relationship
        /// </summary>
        private int term2_id;
        
        /// <summary>
        /// Currently not used with main GO, but it is whether this
        /// relationship forms a complete definition
        /// </summary>
        private bool complete;

        public GOTermRelationship (int id, int relationship_type_id, int term1_id, int term2_id, int complete) {
            IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0: return;
				case 1: 
					SetFromJSObject( (JSObject) args[0] ); 
					break;
				case 5: 
					SetAllFields( id, relationship_type_id, term1_id, term2_id, complete ); 
					break;
				default: 
					throw new Exception( Globals.REQUIRED_ARGS );
			}
        }

		private void SetAllFields (int id, int relationship_type_id, int term1_id, int term2_id, int complete) {
			this.id = id;
            this.relationship_type_id = relationship_type_id;
            this.term1_id = term1_id;
            this.term2_id = term2_id;
            this.complete = bool.Parse(complete.ToString());
		}

		/// <summary> Factory method to convert from JavaScript object to GOTermRelationship.
		/// </summary>
		/// <param name="obj">
		///		A JavaScript object that contains the required fields, encoded as strings.
		/// </param>
		/// <returns></returns>

		public static GOTermRelationship Parse ( JSObject obj ) {        
			return new GOTermRelationship( obj );
		}

		/// <summary> Parameterless constructor.
		/// </summary>

		public extern GOTermRelationship ();

		/// <summary> Copy constructor accepting an arbitrary JavaScript object such as one obtains from Json.Parse.
		/// </summary>

		public extern GOTermRelationship ( JSObject obj );       

		/// <summary> Parses a GOTerm from a JavaScript object which is assumed to contain the required fields.
		/// </summary>

		private void SetFromJSObject ( JSObject record ) {
			SetAllFields(
                int.Parse(Objects.ToString(record["id"], "")),
                int.Parse(Objects.ToString(record["relationship_type_id"], "")),
                int.Parse(Objects.ToString(record["term1_id"], "")),
                int.Parse(Objects.ToString(record["term2_id"], "")),
                int.Parse(Objects.ToString(record["complete"], ""))
			);
			string [] fieldsExcludedFromCopy = {
				"id", "relationship_type_id", "term1_id", "term2_id", "complete"
			};
			Objects.ShallowCopy( record, this, fieldsExcludedFromCopy );
		}

        public int DatabaseId
        {
            get { return id; }
            set { id = value; }
        }

        public int RelationshipDatabaseId
        {
            get { return relationship_type_id; }
            set { relationship_type_id = value; }
        }

        public int ParentDatabaseId
        {
            get { return term1_id; }
            set { term1_id = value; }
        }

        public int ChildDatabaseId
        {
            get { return term2_id; }
            set { term2_id = value; }
        }

        public bool Complete
        {
            get { return complete; }
            set { complete = value; }
        }
    }
}
