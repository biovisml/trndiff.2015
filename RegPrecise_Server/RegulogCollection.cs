﻿using System;
using System.Collections.Generic;

using Enum = SystemQut.Enum;
using SystemQut;

namespace RegPrecise {
	/// <summary> RegulogCollection represents the results obtained from the 
	/// IRegPreciseService.RegulogCollections operation.
	/// </summary>

	public class RegulogCollection: JSObject {
		public string ClassName;
		public Number CollectionId;
		public RegulogCollectionType CollectionType;
		public String Name;

		public RegulogCollection (
			string className,
			Number collectionId,
			RegulogCollectionType collectionType,
			string name
		) {
			this.ClassName = className;
			this.CollectionId = collectionId;
			this.CollectionType = collectionType;
			this.Name = name;
		}

		/// <summary> Factory method which creates a RegulogCollection from a JavaScript object
		///		which contains the required information encoded as string literals. 
		///		Data is imported via Parse/Convert methods.
		/// </summary>
		/// <param name="rec"> A JavaScript object which contains the required information encoded as string literals. </param>
		/// <returns> A RegulogCollection object with the equivalent information encoded as native types. </returns>

		public static RegulogCollection Parse ( JSObject rec ) {
			return new RegulogCollection(
				Objects.ToString( rec["className"], "" ) ,
				Number.Parse( Objects.ToString( rec["collectionId"], "" ) ),
				RegulogCollectionType.Parse( rec["collectionType"] ),
				Objects.ToString( rec["name"], "" ) 
			);
		}

		public static int OrderById ( RegulogCollection x, RegulogCollection y ) {
			return x.CollectionId - x.CollectionId;
		}
	}
}
