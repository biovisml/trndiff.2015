﻿using System;
using System.Collections.Generic;

using System.Runtime.Serialization;
using SystemQut;

namespace RegPrecise {

	/// <summary> A Genome record is the result of a RegPrecise /genomes query.
	/// </summary>
	public class Genome : JSObject {

		/// <summary> the Genome Id.
		/// </summary>
		public int GenomeId;

		/// <summary> the Genome name.
		/// </summary>
		public string Name;

		/// <summary> The NCBI Taxonomy Id.
		/// </summary>
		public int TaxonomyId;

		/// <summary> Constructor for the Genome class.
		/// </summary>
		/// <param name="GenomeId"></param>
		/// <param name="Name"></param>
		/// <param name="TaxonomyId"></param>

		public Genome (
			int GenomeId,
			string Name,
			int TaxonomyId
		) {
			IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0: return;
				case 1: 
					SetFromJSObject( (JSObject) args[0] ); 
					break;
				case 3: 
					SetAllFields( GenomeId, Name, TaxonomyId ); 
					break;
				default: 
					throw new Exception( Globals.REQUIRED_ARGS );
			}
		}

		private void SetAllFields ( int GenomeId, string Name, int TaxonomyId ) {
			this.GenomeId = GenomeId;
			this.Name = Name;
			this.TaxonomyId = TaxonomyId;
		}

		/// <summary> Parameterless constructor.
		/// </summary>

		public extern Genome ();

		/// <summary> Copy constructor accepting an arbitrary JavaScript object such as one obtains from Json.Parse.
		/// </summary>

		public extern Genome ( JSObject obj );

		/// <summary> Parses a Genome from a JavaScript object which is assumed to contain the required fields.
		/// </summary>

		private void SetFromJSObject ( JSObject record ) {
			SetAllFields(
				int.Parse( Objects.ToString( record["genomeId"], "" ) ),
				Objects.ToString( record["name"], "" ),
				int.Parse( Objects.ToString( record["taxonomyId"], "" ) )
			);
			string [] fieldsExcludedFromCopy = {
				"genomeId", "name", "taxonomyId"
			};
			Objects.ShallowCopy( record, this, fieldsExcludedFromCopy );
		}

		/// <summary> Factory method to convert from JavaScript object to Genome.
		/// </summary>
		/// <param name="obj">
		///		A JavaScript object that contains the required fields, encoded as strings.
		/// </param>
		/// <returns></returns>

		public static Genome Parse ( JSObject obj ) {
			return new Genome( obj );
		}

		public static int OrderById ( Genome x, Genome y ) {
			return x.GenomeId - y.GenomeId;
		}
	}
}
