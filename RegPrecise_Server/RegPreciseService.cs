﻿using System;
using System.Collections.Generic;

using NodeApi.Network;
using System.Serialization;
using NodeApi.IO;
using SystemQut;
using Enum = SystemQut.Enum;
using Console = System.Console;
using SystemQut.ServiceModel;

namespace RegPrecise {

	/// <summary> Implement IRegPreciseService.
	/// </summary>

	public class RegPreciseService : IRegPreciseService {
        private static string RegPreciseBaseAddr = "http://regprecise.lbl.gov/Services/rest/";

		/// <summary> Get a list of regulog collections or the specified type.
		/// <para>
		///		Result is a list of regulog collections. Each regulog collection is provided with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>collectionType - type of regulog collection.</item>
		/// <item>collectionId - identifier of collection.</item>
		/// <item>name - collection name.</item>
		/// <item>className - name of collection class.</item>
		/// </list>
		/// </summary>
		/// <param name="collectionType"> The idneity of the required collection type. </param>
		/// <param name="processResults"> A callback that will process the results. </param>

		public void ListRegulogCollections ( RegulogCollectionType collectionType, ResponseProcessor<RegulogCollection[]> processResults ) {
			// curl -i 'http://regprecise.lbl.gov/Services/rest/regulogCollections?collectionType=taxGroup'

			string address = String.Format( "{0}{1}?{2}={3}",
				RegPreciseBaseAddr,
				"regulogCollections",
				"collectionType",
				collectionType
			);

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<RegulogCollection[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						RegulogCollection [] regulogCollections = GetResponseArray( responseBody ).Map<JSObject, RegulogCollection>( RegulogCollection.Parse );
						processResults( new ServiceResponse<RegulogCollection[]>( regulogCollections, null ) );
					}
				);
			} );
		}

		/// <summary> Retrieves a list of genomes that have at least one reconstructed regulon.
		/// <para>
		///		Result is a list of genomes with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>genomeId - genome identifier</item>
		/// <item>name - genome name</item>
		/// <item>taxonomyId - NCBI taxonomy id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void ListGenomes ( ResponseProcessor<Genome[]> processResults ) {
			// curl -i 'http://regprecise.lbl.gov/Services/rest/genomes'

			string address = String.Format( "{0}{1}", RegPreciseBaseAddr, "genomes" );

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<Genome[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						Genome [] genomes = GetResponseArray( responseBody ).Map<JSObject, Genome>( Genome.Parse );
						processResults( new ServiceResponse<Genome[]>( genomes, null ) );
					}
				);
			} );
		}

		/// <summary> Retrieves a list of regulogs that belongs to a specific collection
		///	<para>
		///		Requires type and identifier of a regulog collection
		///	</para>
		///	<para>
		///		Returns a list of regulogs. Each regulog is provided with the following data:
		///	</para>
		///	<list type="bullet">
		///	<item>
		///		•regulogId - identifier of regulog
		///	</item>
		///	<item>
		///		•regulatorName - name of regulator
		///	</item>
		///	<item>
		///		•regulatorFamily - family of regulator
		///	</item>
		///	<item>
		///		•regulationType - type of regulation: either TF (transcription factor) or RNA
		///	</item>
		///	<item>
		///		•taxonName - name of taxonomic group
		///	</item>
		///	<item>
		///		•effector - effector molecule or environmental signal of a regulator
		///	</item>
		///	<item>
		///		•pathway - metabolic pathway or biological process controlled by a regulator
		///	</item>
		///	</list>
		/// </summary>

		public void ListRegulogs (
			RegulogCollectionType collectionType,
			Number regulogId,
			ResponseProcessor<Regulog[]> processResults
		) {
			// curl -i 'http://regprecise.lbl.gov/Services/rest/regulogs?collectionType=taxGroup&collectionId=1'

			string address = String.Format( "{0}{1}?collectionType={2}&collectionId={3}",
				RegPreciseBaseAddr,
				"regulogs",
				collectionType,
				regulogId
			);

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<Regulog[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						Regulog [] regulogs = GetResponseArray( responseBody ).Map<JSObject, Regulog>( Regulog.Parse );
						processResults( new ServiceResponse<Regulog[]>( regulogs, null ) );
					}
				);
			} );
		}

		/// <summary> Parses a JSON string returned by the RegPrecise service which may contain either a
		///		single object of some type, or an array of those objects. In either case the resulting data
		///		is returned as an array.
		/// </summary>
		/// <param name="json">
		///		A JSON formatted string containing either one or a list of objects returned by the RegPrecise web service.
		/// </param>
		/// <returns></returns>

		private static JSObject[] GetResponseArray ( string json ) {
			try {
				RegExp pattern = new RegExp( "[\"'](\\w+)[\"']" );
				string topLevelObject = pattern.Exec( json )[1];

				JSObject obj = (JSObject) Json.Parse( json );

				object responseContent = obj[topLevelObject];

				JSObject[] objectsFromServer = responseContent is Array
					? (JSObject[]) responseContent
					: new JSObject[] { (JSObject) responseContent };

				return objectsFromServer;
			}
			catch {
				return new JSObject[0];
			}

		}

		/// <summary> Retrieves a list of regulons belonging to a particular regulog.
		/// </summary>
		/// <param name="regulogId">
		///		The numeric regulog Id.
		///	</param>
		/// <param name="processResults">
		///		A delegate which will process the results.
		///	</param>

		public void ListRegulonsInRegulog (
			Number regulogId,
			ResponseProcessor<Regulon[]> processResults
		) {
			ListRegulons( processResults, "regulogId", regulogId );
		}

		/// <summary> Retrieves a list of regulons belonging to a particular genome.
		/// </summary>
		/// <param name="genomeId">
		///		The numeric regulog Id.
		///	</param>
		/// <param name="processResults">
		///		A delegate which will process the results.
		///	</param>

		public void ListRegulonsInGenome (
			Number genomeId,
			ResponseProcessor<Regulon[]> processResults
		) {
			ListRegulons( processResults, "genomeId", genomeId );
		}

		/// <summary> Helper method that invokes the /regulons operation on the RegPrecise web service.
		/// </summary>
		/// <param name="processResults"></param>
		/// <param name="parameterName"></param>
		/// <param name="parameterValue"></param>

		private void ListRegulons (
			ResponseProcessor<Regulon[]> processResults,
			string parameterName,
			Number parameterValue
		) {
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/regulons?regulogId=621'
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/regulons?genomeId=1'

			string address = String.Format( "{0}{1}?{2}={3}", RegPreciseBaseAddr, "regulons", parameterName, parameterValue );

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<Regulon[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						Regulon [] regulons = GetResponseArray( responseBody ).Map<JSObject, Regulon>( Regulon.Parse );
						processResults( new ServiceResponse<Regulon[]>( regulons, null ) );
					}
				);
			} );
		}

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListSitesInRegulon (
			Number regulonId,
			ResponseProcessor<Site[]> processResults
		) {
			ListSites( "regulonId", regulonId, processResults );
		}

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults"> A Delegate that will be called to process the results. </param>

		public void ListSitesInRegulog (
			Number regulogId,
			ResponseProcessor<Site[]> processResults
		) {
			ListSites( "regulogId", regulogId, processResults );
		}

		/// <summary> Lists the sites for a regulon of regulog.
		/// </summary>
		/// <param name="queryValue"></param>
		/// <param name="queryField"></param>
		/// <param name="processResults"></param>

		private void ListSites (
			string queryField,
			Number queryValue,
			ResponseProcessor<Site[]> processResults
		) {
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulonId=6423'
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulogId=647'
			string address = String.Format( "{0}{1}?{2}={3}", RegPreciseBaseAddr, "sites", queryField, queryValue );

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<Site[]>( null, error.Message ) );
					},
					delegate( string json ) {
						Site [] sites = GetResponseArray( json ).Map<JSObject, Site>( Site.Parse );
						processResults( new ServiceResponse<Site[]>( sites, null ) );
					}
				);
			} );
		}

		/// <summary> Helper method which buffers the response from a http request and calls the designated processor.
		/// </summary>
		/// <param name="response">The HttpResponse to process.</param>
		/// <param name="error">A delegate which will be invoked with an Exception object if a http error occurs.</param>
		/// <param name="process">A delegate which will be invoked with the response body text if there is no error.</param>

		static void ProcessResponse (
			HttpClientResponse response,
			Action<Exception> error,
			Action<string> process
		) {
			if ( response.StatusCode != HttpStatusCode.OK ) {
				error( new Exception( "Error in RegPrecise web service call: Http Status = " + response.StatusCode ) );
				return;
			}

			StringBuilder sb = new StringBuilder();
			bool errorDetected = false;

			// Handle error in response.
			response.Error += delegate( Exception ex ) {
				errorDetected = true;
				error( ex );
			};

			// Accumulate a chunk of response text.
			response.Readable += delegate() {
				if ( errorDetected )
					return;

				string chunk = response.ReadString();
				sb.Append( chunk );
			};

			// Process the complete response text.
			response.End += delegate() {
				if ( errorDetected )
					return;

				process( sb.ToString() );
			};
		}

		/// <summary> Gets the specified regulog descriptor.
		/// </summary>
		/// <param name="regulogId">The id of a regulog.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void GetRegulog (
			int regulogId,
			ResponseProcessor<Regulog> processResults
		) {
			// curl -i 'http://regprecise.lbl.gov/Services/rest/regulog?regulogId=621'

			string address = String.Format( "{0}{1}?{2}={3}",
				RegPreciseBaseAddr, "regulog", "regulogId", regulogId );

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<Regulog>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						Regulog regulog = Regulog.Parse( (JSObject) Json.Parse( responseBody ) );
						processResults( new ServiceResponse<Regulog>( regulog, null ) );
					}
				);
			} );
		}

		/// <summary> Gets the specified regulon descriptor.
		/// </summary>
		/// <param name="regulonId">The id of a regulon.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void GetRegulon (
			int regulonId,
			ResponseProcessor<Regulon> processResults
		) {
			//  curl -i 'http://regprecise.lbl.gov/Services/rest/regulon?regulonId=6423'

			string address = String.Format( "{0}{1}?{2}={3}",
				RegPreciseBaseAddr, "regulon", "regulonId", regulonId );

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<Regulon>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						Regulon regulon = Regulon.Parse( (JSObject) Json.Parse( responseBody ) );
						processResults( new ServiceResponse<Regulon>( regulon, null ) );
					}
				);
			} );
		}

		/// <summary> Lists the regulators in a regulon.
		/// </summary>
		/// <param name="regulonId"> The id of a regulon. </param>
		/// <param name="processResults"> A delegate that will process the results. </param>

		public void ListRegulatorsInRegulon (
			int regulonId,
			ResponseProcessor<Regulator[]> processResults
		) {
			ListRegulators( "regulonId", regulonId, processResults );
		}

		/// <summary> Lists the regulators in a regulon.
		/// </summary>
		/// <param name="regulogId"> The id of a regulog. </param>
		/// <param name="processResults"> A delegate that will process the results. </param>

		public void ListRegulatorsInRegulog (
			int regulogId,
			ResponseProcessor<Regulator[]> processResults
		) {
			ListRegulators( "regulogId", regulogId, processResults );
		}

		/// <summary> Implements the ListRegulators operation.
		/// </summary>
		/// <param name="fieldName"></param>
		/// <param name="collectionId"></param>
		/// <param name="processResults"></param>

		private void ListRegulators (
			string fieldName,
			int collectionId,
			ResponseProcessor<Regulator[]> processResults
		) {
			// curl -i 'http://regprecise.lbl.gov/Services/rest/regulators?regulonId=6423'
			// curl -i 'http://regprecise.lbl.gov/Services/rest/regulators?regulogId=647'

			string address = String.Format( "{0}{1}?{2}={3}",
				RegPreciseBaseAddr, "regulators", fieldName, collectionId );

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<Regulator[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						Regulator [] regulators = GetResponseArray( responseBody ).Map<JSObject, Regulator>( Regulator.Parse );
						processResults( new ServiceResponse<Regulator[]>( regulators, null ) );
					}
				);
			} );
		}

		/// <summary> Gets the list of genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListGenesInRegulon (
			Number regulonId,
			ResponseProcessor<Gene[]> processResults
		) {
			ListGenes( "regulonId", regulonId, processResults );
		}

		/// <summary> Gets the list of genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListGenesInRegulog (
			Number regulogId,
			ResponseProcessor<Gene[]> processResults
		) {
			ListGenes( "regulogId", regulogId, processResults );
		}

		/// <summary> Implements the ListGenes operation.
		/// </summary>
		/// <param name="fieldName"></param>
		/// <param name="collectionId"></param>
		/// <param name="processResults"></param>

		private void ListGenes (
			string fieldName,
			int collectionId,
			ResponseProcessor<Gene[]> processResults
		) {
			// curl -i 'http://regprecise.lbl.gov/Services/rest/genes?regulonId=6423'
			// curl -i 'http://regprecise.lbl.gov/Services/rest/genes?regulogId=647'

			string address = String.Format( "{0}{1}?{2}={3}",
				RegPreciseBaseAddr, "genes", fieldName, collectionId );

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<Gene[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						Gene[] records = GetResponseArray( responseBody ).Map<JSObject, Gene>( Gene.Parse );
						processResults( new ServiceResponse<Gene[]>( records, null ) );
					}
				);
			} );
		}

		/// <summary> Gets the general statistics on regulons and regulatory sites in all genomes.
		/// </summary>
		/// <param name="processResults"> A delegate that will be invoked when the results are available. </param>

		public void ListGenomeStats ( ResponseProcessor<GenomeStats[]> processResults ) {
			// curl -i 'http://regprecise.lbl.gov/Services/rest/genomeStats'

			string address = RegPreciseBaseAddr + "genomeStats";


			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<GenomeStats[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						GenomeStats[] records = GetResponseArray( responseBody ).Map<JSObject, GenomeStats>( GenomeStats.Parse );
						processResults( new ServiceResponse<GenomeStats[]>( records, null ) );
					}
				);
			} );
		}

		/// <summary> Retrieves a list of regulon references by the name/locus tag of either regulator or target regulated genes
		/// </summary>
		/// <param name="objectType"> The type of the taget object. </param>
		/// <param name="objectName"> The regulator name or gene locus tag of the target object. </param>
		/// <param name="processResults"> A delegate that will be invoked to process the results when the operation is complete. </param>

		public void SearchRegulons (
			ObjectTypes objectType,
			string objectName,
			ResponseProcessor<RegulonReference[]> processResults
		) {
			//curl -i 'http://regprecise.lbl.gov/Services/rest/searchRegulons?objType=regulator&text=argR'
			//curl -i 'http://regprecise.lbl.gov/Services/rest/searchRegulons?objType=gene&text=SO_2706'

			string address = String.Format( "{0}{1}?objType={2}&text={3}",
				RegPreciseBaseAddr, "searchRegulons", objectType.ToString(), objectName );

			// Console.Log ( "address = " + address );

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<RegulonReference[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						RegulonReference[] records = GetResponseArray( responseBody ).Map<JSObject, RegulonReference>( RegulonReference.Parse );
						processResults( new ServiceResponse<RegulonReference[]>( records, null ) );
					}
				);
			} );
		}

		/// <summary> Retrieves a list of regulon references by genome taxonomy id and comma-delimited list of gene locusTags.
		/// </summary>
		/// <param name="ncbiTaxonomyId"> The NCBI taxonomy Id of the containing organism. </param>
		/// <param name="locusTags"> A list of locus tags. </param>
		/// <param name="processResults"> A delegate that will be invoked to process the results. </param>

		public void SearchExtRegulons (
			int ncbiTaxonomyId,
			string[] locusTags,
			ResponseProcessor<RegulonReference[]> processResults
		) {
			// curl -i 'http://regprecise.lbl.gov/Services/rest/searchExtRegulons?taxonomyId=882&locusTags=DVU0043,DVU0694,DVU3234,DVU3233,DVU0910,DVU3384,DVU0863'

			string address = String.Format( "{0}{1}?{2}={3}&{3}={4}",
				RegPreciseBaseAddr, "searchExtRegulons",
				"taxonomyId", ncbiTaxonomyId,
				"locusTags", locusTags
			);

			Http.Get( address, delegate( HttpClientResponse response ) {
				ProcessResponse( response,
					delegate( Exception error ) {
						processResults( new ServiceResponse<RegulonReference[]>( null, error.Message ) );
					},
					delegate( String responseBody ) {
						RegulonReference[] records = GetResponseArray( responseBody ).Map<JSObject, RegulonReference>( RegulonReference.Parse );
						processResults( new ServiceResponse<RegulonReference[]>( records, null ) );
					}
				);
			} );
		}

        //// <summary>
        //// Gets the URL used to access the RegPrecise web services
        //// <para>Used so LocalMySQLService will use the same URL without having to change it there as well</para>
        //// </summary>
        /*public static string RegPreciseServiceURL
        {
            get { return RegPreciseService.RegPreciseBaseAddr; }
        }*/
	}
}
