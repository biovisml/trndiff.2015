﻿using System;
using System.Collections.Generic;
using SystemQut;

namespace RegPrecise
{
    public class GOTerm : JSObject
    {
        /// <summary>
        /// The database id
        /// </summary>
        private int id;

        /// <summary>
        /// The term name
        /// </summary>
        private string name;

        /// <summary>
        /// The term type
        /// </summary>
        private string term_type;

        /// <summary>
        /// The term's unique identifier
        /// </summary>
        private string acc;

        /// <summary>
        /// Whether the term is obsolete
        /// </summary>
        private bool is_obsolete;

        /// <summary>
        /// Whether the term is the root
        /// </summary>
        private bool is_root;

        /// <summary>
        /// Whether the term is a relation
        /// </summary>
        private bool is_relation;

        /// <summary>
        /// A list of all the synonyms of this term - these will need to be
        /// filled seperately
        /// (not specified in GO database schema - this is custom for this
        /// implementation)
        /// </summary>
        private List<GOTermSynonym> term_synonyms;

        /// <summary>
        /// A list of all the relationships of this term - these will need to
        /// be filled seperately
        /// (not specified in GO database schema - this is custom for this
        /// implementation)
        /// </summary>
        private List<GOTermRelationship> term_relationships;

        public GOTerm (int id, string name, string term_type, string acc, int is_obsolete, int is_root, int is_relation, List<GOTermSynonym> term_synonyms, List<GOTermRelationship> term_relationships) {
            /*this.id = id;
            this.name = name;
            this.termType = termType;
            this.obsolete = bool.Parse(obsolete.ToString());
            this.root = bool.Parse(root.ToString());
            this.relation = bool.Parse(relation.ToString());*/

            IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0: return;
				case 1: 
					SetFromJSObject( (JSObject) args[0] ); 
					break;
				case 7: 
					SetAllFields( id, name, term_type, acc, is_obsolete, is_root, is_relation ); 
					break;
				/*case 8: 
					SetAllFieldsWithSynonyms( id, name, term_type, acc, is_obsolete, is_root, is_relation, term_synonyms ); 
					break;*/
				case 9: 
					SetAllFieldsWithBothLists( id, name, term_type, acc, is_obsolete, is_root, is_relation, term_synonyms, term_relationships ); 
					break;
				default: 
					throw new Exception( Globals.REQUIRED_ARGS );
			}            

            /*if (this.term_synonyms == null) {
                Console.Log("This new term has no synonyms, or none were passed to this function.");
                this.term_synonyms = new List<GOTermSynonym>();
            } else {
                Console.Log("This new term has " + term_synonyms.Length + " synonyms.");
            }*/
            //if (this.term_relationships == null) {
                //this.term_relationships = new List<GOTermRelationship>();
            //}
        }

		private void SetAllFields (int id, string name, string termType, string acc, int obsolete, int root, int relation) {
			this.id = id;
            this.name = name;
            this.term_type = termType;
            this.acc = acc;
            this.is_obsolete = bool.Parse(obsolete.ToString());
            this.is_root = bool.Parse(root.ToString());
            this.is_relation = bool.Parse(relation.ToString());
		}

		private void SetAllFieldsWithSynonyms (int id, string name, string termType, string acc, int obsolete, int root, int relation, List<GOTermSynonym> term_synonyms) {
			this.id = id;
            this.name = name;
            this.term_type = termType;
            this.acc = acc;
            this.is_obsolete = bool.Parse(obsolete.ToString());
            this.is_root = bool.Parse(root.ToString());
            this.is_relation = bool.Parse(relation.ToString());
            if (term_synonyms != null) {
                this.term_synonyms = term_synonyms;
            }
		}

		private void SetAllFieldsWithBothLists (int id, string name, string termType, string acc, int obsolete, int root, int relation, List<GOTermSynonym> term_synonyms, List<GOTermRelationship> term_relationships) {
			this.id = id;
            this.name = name;
            this.term_type = termType;
            this.acc = acc;
            this.is_obsolete = bool.Parse(obsolete.ToString());
            this.is_root = bool.Parse(root.ToString());
            this.is_relation = bool.Parse(relation.ToString());
            if (term_synonyms != null) {
                this.term_synonyms = term_synonyms;
            }
            if (term_relationships != null) {
                this.term_relationships = term_relationships;
            }
		}

		/// <summary> Factory method to convert from JavaScript object to GOTerm.
		/// </summary>
		/// <param name="obj">
		///		A JavaScript object that contains the required fields, encoded as strings.
		/// </param>
		/// <returns></returns>

		public static GOTerm Parse ( JSObject obj ) {
			/*return new GOTerm(int.Parse(Objects.ToString(obj["id"], "")),
                        Objects.ToString(obj["name"], ""),
                        Objects.ToString(obj["term_type"], ""),
                        Objects.ToString(obj["acc"], ""),
                        int.Parse(Objects.ToString(obj["is_obsolete"], "")),
                        int.Parse(Objects.ToString(obj["is_root"], "")), 
                        int.Parse(Objects.ToString(obj["is_relation"], "")));*/           
			return new GOTerm( obj );
		}

		public static int OrderById ( GOTerm x, GOTerm y ) {
			return string.Compare(x.acc, y.acc);
		}

		public static int OrderByDatabaseId ( GOTerm x, GOTerm y ) {
			return x.id - y.id;
		}

		/// <summary> Parameterless constructor.
		/// </summary>

		public extern GOTerm ();

		/// <summary> Copy constructor accepting an arbitrary JavaScript object such as one obtains from Json.Parse.
		/// </summary>

		public extern GOTerm ( JSObject obj );       

		/// <summary> Parses a GOTerm from a JavaScript object which is assumed to contain the required fields.
		/// </summary>

		private void SetFromJSObject ( JSObject record ) {
			SetAllFieldsWithBothLists(
            //SetAllFieldsWithSynonyms(
                int.Parse(Objects.ToString(record["id"], "")),
                Objects.ToString(record["name"], ""),
                Objects.ToString(record["term_type"], ""),
                Objects.ToString(record["acc"], ""),
                int.Parse(Objects.ToString(record["is_obsolete"], "")),
                int.Parse(Objects.ToString(record["is_root"], "")), 
                int.Parse(Objects.ToString(record["is_relation"], "")), 
                (List<GOTermSynonym>)record["term_synonyms"], 
                (List<GOTermRelationship>)record["term_relationships"]
			);
			string [] fieldsExcludedFromCopy = {
				"id", "name", "term_type", "acc", "is_obsolete", "is_root", "is_relation", "term_synonyms", "term_relationships"
			};
			Objects.ShallowCopy( record, this, fieldsExcludedFromCopy );
            /*if (this.term_synonyms != null && this.term_synonyms.Length > 0) {
                Console.Log("SetAllFriendsWithSynonyms: Something with " + this.term_synonyms.Length + " synonyms passed through here.");
            }*/
		}

        public int DatabaseId
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }

        public string Namespace
        {
            get { return term_type; }
        }

        public string UniqueIdentifier
        {
            get { return acc; }
        }

        public bool Obsolete
        {
            get { return is_obsolete; }
        }

        public bool Root
        {
            get { return is_root; }
        }

        public bool Relation
        {
            get { return is_relation; }
            set { is_relation = value; }
        }        

        public List<GOTermSynonym> Synonyms
        {
            get { return term_synonyms; }
            set { term_synonyms = value; }
        }

        public List<GOTermRelationship> Relationships
        {
            get { return term_relationships; }
            set { term_relationships = value; }
        }
    }
}
