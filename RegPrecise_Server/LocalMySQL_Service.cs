﻿using System;
using System.Collections.Generic;

using SystemQut.Web;
using NodeApi.Network;
using SystemQut;
using System.Serialization;
using SystemQut.ServiceModel;
using Enum = SystemQut.Enum;

namespace RegPrecise {

	/// <summary> A wrapper for the Gene Ontology MySQL service proxy.
	/// </summary>
    ///
    public class LocalMySQL_Service : IService {

        /// <summary> Processes a web request for the RegPrecise service.
		/// </summary>
		/// <param name="pathFragments"> The collection of slash-separated path fragments. </param>
		/// <param name="query"> The query part of the url. </param>
		/// <param name="request"> The HttpRequest object, which is needed if a POST request is encountered. </param>
		/// <param name="response"> A HttpResponse object which is used to transport the resulting document back to the caller. </param>

		public void Process (
			string[] pathFragments,
			Dictionary<string, string> query,
			HttpServerRequest request,
			HttpServerResponse response
		) {
			if ( request.Method != HttpVerb.POST ) {
				Console.Log( "non-post method received." );
				response.StatusCode = 405;
				response.EndOfData( "405 - Method not supported." );
			}

			StringBuilder sb = new StringBuilder();

			request.Readable += delegate() {
				sb.Append( request.ReadString() );
			};

			request.End += delegate() {
				JSObject req = (JSObject) Json.Parse( sb.ToString() );

				Console.Log( sb.ToString() );

				ILocalMySQLService service = new LocalMySQLService();
				string operation = String.Convert( req["operation"] );

				Action<object> sendResponse = delegate( object responseObject ) {
					string responseBody = Json.Stringify( responseObject );
					response.SetHeader( "Content-Type", "application/json" );
					response.EndOfData( responseBody );
				};

				switch ( operation ) {
					case "GO_ListTerms":
						service.GO_ListTerms( (ResponseProcessor<GOTerm[]>) (object) sendResponse );
						break;
					case "GO_ListSynonymsOfTerm":
						service.GO_ListSynonymsOfTerm( Number.Convert( req["termId"] ), (ResponseProcessor<GOTermSynonym[]>) (object) sendResponse );
						break;
					case "GO_ListRelationshipsOfTermAsChild":
						service.GO_ListRelationshipsOfTermAsChild( Number.Convert( req["termId"] ), (ResponseProcessor<GOTermRelationship[]>) (object) sendResponse );
						break;
					case "GO_GetTerm":
						service.GO_GetTerm( Number.Convert( req["termId"] ), (ResponseProcessor<GOTerm>) (object) sendResponse );
						break;
					case "GO_ListTermsWithSynonyms":
						service.GO_ListTermsWithSynonyms( (ResponseProcessor<GOTerm[]>) (object) sendResponse );
						break;
					case "GO_ListTermsWithParents":
						service.GO_ListTermsWithParents( (ResponseProcessor<GOTerm[]>) (object) sendResponse );
						break;
					case "GO_GetRegPreciseCategoryMatches":
						service.GO_GetRegPreciseCategoryMatches( (ResponseProcessor<GORegPreciseMatch[]>) (object) sendResponse );
						break;
					case "ListGenomes":
						service.ListGenomes( (ResponseProcessor<Genome[]>) (object) sendResponse );
						break;
					case "GetRegulog":
						service.GetRegulog( Number.Convert( req["regulogId"] ), (ResponseProcessor<Regulog>) (object) sendResponse );
						break;
                    case "ListGenomeStats":
						service.ListGenomeStats( (ResponseProcessor<GenomeStats[]>) (object) sendResponse );
						break;
					case "ListRegulatorsInRegulon":
						service.ListRegulatorsInRegulon(
							(int) req["regulonId"],
							(ResponseProcessor<Regulator[]>) (object) sendResponse
						);
						break;
					case "ListRegulatorsInRegulog":
						service.ListRegulatorsInRegulog(
							(int) req["regulogId"],
							(ResponseProcessor<Regulator[]>) (object) sendResponse
						);
						break;
					case "ListRegulogs":
						service.ListRegulogs(
							(RegulogCollectionType) req["collectionType"],
							(Number) req["collectionId"],
							(ResponseProcessor<Regulog[]>) (object) sendResponse
						);
						break;

                    case "ListSitesInRegulog":
						service.ListSitesInRegulog(
							(Number) req["regulogId"],
							(ResponseProcessor<Site[]>) (object) sendResponse
						);
						break;
					case "ListSitesInRegulon":
						service.ListSitesInRegulon(
							(Number) req["regulonId"],
							(ResponseProcessor<Site[]>) (object) sendResponse
						);
						break;

					case "GetRegulon":
						service.GetRegulon(
							(int) req["regulonId"],
							(ResponseProcessor<Regulon>) (object) sendResponse
						);
						break;
					case "ListGenesInRegulog":
						service.ListGenesInRegulog(
							(int) req["regulogId"],
							(ResponseProcessor<Gene[]>) (object) sendResponse
						);
						break;
					case "ListGenesInRegulon":
						service.ListGenesInRegulon(
							(int) req["regulonId"],
							(ResponseProcessor<Gene[]>) (object) sendResponse
						);
						break;
					case "ListRegulonsInGenome":
						service.ListRegulonsInGenome(
							(int) req["genomeId"],
							(ResponseProcessor<Regulon[]>) (object) sendResponse
						);
						break;
					case "ListRegulonsInRegulog":
						service.ListRegulonsInRegulog(
							(int) req["regulogId"],
							(ResponseProcessor<Regulon[]>) (object) sendResponse
						);
						break;
					case "ListRegulogCollections":
						service.ListRegulogCollections(
							(RegulogCollectionType) req["collectionType"],
							(ResponseProcessor<RegulogCollection[]>) (object) sendResponse
						);
						break;
					case "AddRegulogs_ListRegulogs":
						service.AddRegulogs_ListRegulogs(
							(RegulogCollectionType) req["collectionType"],
							(Number) req["collectionId"],
							(Regulog[]) req["regulogs"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
						break;
					case "AddRegulogs_GetRegulog":
						service.AddRegulogs_GetRegulog(
                            Number.Convert( req["regulogId"] ),
							(Regulog) req["regulog"],
                            (ResponseProcessor<Number>) (object) sendResponse );
						break;
					case "AddRegulons_ListRegulonsInRegulog":
						service.AddRegulons_ListRegulonsInRegulog(
                            Number.Convert( req["regulogId"] ),
							(Regulon[]) req["regulons"],
                            (ResponseProcessor<Number>) (object) sendResponse );
						break;
					case "AddRegulons_ListRegulonsInGenome":
						service.AddRegulons_ListRegulonsInGenome(
                            Number.Convert( req["genomeId"] ),
							(Regulon[]) req["regulons"],
                            (ResponseProcessor<Number>) (object) sendResponse );
						break;
                    case "AddRegulogCollections_ListRegulogCollections":
						service.AddRegulogCollections_ListRegulogCollections(
							(RegulogCollectionType) req["collectionType"],
                            (RegulogCollection[]) req["regulogCollections"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
                        break;
                    case "AddGenomes_ListGenomes":
						service.AddGenomes_ListGenomes( (Genome[]) req["genomes"],
                            (ResponseProcessor<Number>) (object) sendResponse );
                        break;
                    case "AddSites_ListSitesInRegulon":
						service.AddSites_ListSitesInRegulon(
							(Number) req["regulonId"],
                            (Site[]) req["sites"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
                        break;
                    case "AddSites_ListSitesInRegulog":
						service.AddSites_ListSitesInRegulog(
							(Number) req["regulogId"],
                            (Site[]) req["sites"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
                        break;
                    case "AddRegulons_GetRegulon":
						service.AddRegulons_GetRegulon(
							(int) req["regulonId"],
                            (Regulon) req["regulon"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
                        break;
                    case "AddRegulators_ListRegulatorsInRegulon":
						service.AddRegulators_ListRegulatorsInRegulon(
							(int) req["regulonId"],
                            (Regulator[]) req["regulators"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
                        break;
                    case "AddRegulators_ListRegulatorsInRegulog":
						service.AddRegulators_ListRegulatorsInRegulog(
							(int) req["regulogId"],
                            (Regulator[]) req["regulators"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
                        break;
                    case "AddGenes_ListGenesInRegulon":
						service.AddGenes_ListGenesInRegulon(
							(int) req["regulonId"],
                            (Gene[]) req["genes"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
                        break;
                    case "AddGenes_ListGenesInRegulog":
						service.AddGenes_ListGenesInRegulog(
							(int) req["regulogId"],
                            (Gene[]) req["genes"],
							(ResponseProcessor<Number>) (object) sendResponse
						);
                        break;
                    case "AddGenomeStats_ListGenomeStats":
						service.AddGenomeStats_ListGenomeStats( (GenomeStats[]) req["genomeStats"],
                            (ResponseProcessor<Number>) (object) sendResponse );
                        break;
					default:
						sendResponse( new ServiceResponse<object>( null, "Not implemented." ) );
						break;
				}
			};
		}

    }
}
