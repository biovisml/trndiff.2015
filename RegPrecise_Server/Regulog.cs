﻿using System;
using System.Collections.Generic;

using Enum = SystemQut.Enum;
using SystemQut;

namespace RegPrecise {

	/// <summary> A Regulog records information about a regulog -- i.e. a collection of conserved regulons.
	/// </summary>

	public class Regulog : JSObject {
		/*
		 * {
		 *		"effector":"Tyr-tRNA",
		 *		"regulationType":"RNA", // TF or RNA
		 *		"regulatorFamily":"RF00230",
		 *		"regulatorName":"T-box(Tyr)",
		 *		"regulogId":"2952",
		 *		"taxonName":"Bacillales"
		 *		"pathway": "???"
		 * }
		 */

		public string Effector;
		public RegulationTypes RegulationType;
		public string RegulatorFamily;
		public string RegulatorName;
		public Number RegulogId;
		public string TaxonName;
		public string Pathway;
        //public Number NumGroups;

		/// <summary> Creates a new "empty" regulon.
		/// </summary>

		extern public Regulog ();

		/// <summary> Creates a new regulon which is populated with fields parsed out of a
		///		JavaScript object.
		/// </summary>

		extern public Regulog ( JSObject obj );

		/// <summary> Creates a new regulon which is populated with values presented as
		///		parallel arrays in which one array contains the field names and the other
		///		contains the field values, such as might be obtained by parsing a CSV document.
		/// </summary>
		/// <param name="keys">
		///		An array containing the field names.
		/// </param>
		/// <param name="values">
		///		An array containing field values for the corresponding fields, by position.
		/// </param>

		extern public Regulog ( string[] keys, string[] values );

		/// <summary> Populates a new Regulog object with all fields explcitly.
		/// </summary>
		/// <param name="effector"></param>
		/// <param name="regulationType"></param>
		/// <param name="regulatorFamily"></param>
		/// <param name="regulatorName"></param>
		/// <param name="regulogId"></param>
		/// <param name="taxonName"></param>
		/// <param name="pathway"></param>

		public Regulog (
			string effector,
			RegulationTypes regulationType,
			string regulatorFamily,
			string regulatorName,
			Number regulogId,
			string taxonName,
			string pathway
		) {
			IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0:
					return;
				case 1:
					InitFromObject( args[0] );
					return;
				case 2:
					JSObject obj = Objects.FromArrays<string>( (string[]) args[0], (string[]) args[1] );
					InitFromObject( obj );
					return;
				case 7:
					InitAllFields( effector, regulationType, regulatorFamily, regulatorName, regulogId, taxonName, pathway );
					return;
				default:
					throw new Exception( "Required arguments not provided." );
			}
		}

		private void InitFromObject ( object parms ) {
			JSObject rec = (JSObject) parms;

            InitAllFields(
				Objects.ToString( rec["effector"], "" ),
				RegulationTypes.Parse( rec["regulationType"] ),
				Objects.ToString( rec["regulatorFamily"], "" ),
				Objects.ToString( rec["regulatorName"], "" ),
				Number.Parse( Objects.ToString( rec["regulogId"], "" ) ),
				Objects.ToString( rec["taxonName"], "" ),
				Objects.ToString( rec["pathway"], "" )
			);

			Objects.ShallowCopy( rec, this, new string [] {
				"effector", "regulationType", "regulatorFamily", "regulatorName", "regulogId", "taxonName", "pathway",
			} );
		}

		private void InitAllFields (
			string effector,
			RegulationTypes regulationType,
			string regulatorFamily,
			string regulatorName,
			Number regulogId,
			string taxonName,
			string pathway
		) {
			this.Effector = effector;
			this.RegulationType = regulationType;
			this.RegulatorFamily = regulatorFamily;
			this.RegulatorName = regulatorName;
			this.RegulogId = regulogId;
			this.TaxonName = taxonName;
			this.Pathway = Script.IsNullOrUndefined( pathway ) ? "" : pathway;
		}

		public static Regulog Parse ( JSObject obj ) {
			return new Regulog( obj );
		}

		public static int OrderById ( object x, object y ) {
			return ( (Regulog) x ).RegulogId - ( (Regulog) y ).RegulogId;
		}

		public static bool Identical ( object x, object y ) {
			Regulog rX = (Regulog) x;
			Regulog rY = (Regulog) y;
			return rX.Effector == rY.Effector
			&& rX.Pathway == rY.Pathway
			&& rX.RegulationType == rY.RegulationType
			&& rX.RegulatorFamily == rY.RegulatorFamily
			&& rX.RegulatorName == rY.RegulatorName
			&& rX.RegulogId == rY.RegulogId;
		}
	}
}
