﻿using System;
using System.Collections.Generic;

using SystemQut;

namespace RegPrecise {

	/// <summary> Record type which represents a regulatory site, usually found upstream 
	///		of the affected gene.
	/// </summary>

	public class Site : JSObject {
		/*
		 * {
		 * 	"geneLocusTag": "SO_0443",
		 * 	"geneVIMSSId": "199635",
		 * 	"position": "-94",
		 * 	"regulonId": "6423",
		 * 	"score": "6.9617",
		 * 	"sequence": "ACCTTGGAGTAGGCTCCAAGGT"
		 * }
		 */

		public String GeneLocusTag;
		public Number GeneVIMSSId;
		public Number Position;
		public Number RegulonId;
		public Number Score;
		public String Sequence;

		/// <summary> Initialise an empty Site object.
		/// </summary>

		extern public Site ();

		/// <summary> Initialise Site object from a javaScript object which contains the required 
		///		fields, although the values may be represented as strings.
		/// </summary>

		extern public Site ( JSObject obj );

		/// <summary> Initialise a Site, setting all fields explicitly.
		/// </summary>
		/// <param name="GeneLocusTag"></param>
		/// <param name="GeneVIMSSId"></param>
		/// <param name="Position"></param>
		/// <param name="RegulonId"></param>
		/// <param name="Score"></param>
		/// <param name="Sequence"></param>

		public Site (
		   String GeneLocusTag,
		   Number GeneVIMSSId,
		   Number Position,
		   Number RegulonId,
		   Number Score,
		   String Sequence
		) {
			IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0:
					return;
				case 1:
					SetFieldsFromObject( args[0] );
					return;
				case 6:
					SetAllFields( GeneLocusTag, GeneVIMSSId, Position, RegulonId, Score, Sequence );
					return;
				default:
					throw new Exception( "Required arguments not supplied." );
			}
		}

		private void SetFieldsFromObject ( object parms ) {
			JSObject obj = (JSObject) parms;

			SetAllFields(
				Objects.ToString( obj["geneLocusTag"], "" ),
				int.Parse( Objects.ToString( obj["geneVIMSSId"], "" ) ),
				int.Parse( Objects.ToString( obj["position"], "" ) ),
				int.Parse( Objects.ToString( obj["regulonId"], "" ) ),
				double.Parse( Objects.ToString( obj["score"], "" ) ),
				Objects.ToString( obj["sequence"], "" )
			);

			Objects.ShallowCopy( obj, this, new string[] {
				"geneLocusTag", "geneVIMSSId", "position", "regulonId", "score", "sequence", 
			} );
		}

		/// <summary> Set all fields in the object.
		/// </summary>
		/// <param name="GeneLocusTag"></param>
		/// <param name="GeneVIMSSId"></param>
		/// <param name="Position"></param>
		/// <param name="RegulonId"></param>
		/// <param name="Score"></param>
		/// <param name="Sequence"></param>

		public void SetAllFields (
			String GeneLocusTag,
			Number GeneVIMSSId,
			Number Position,
			Number RegulonId,
			Number Score,
			String Sequence
		) {
			this.GeneLocusTag = GeneLocusTag;
			this.GeneVIMSSId = GeneVIMSSId;
			this.Position = Position;
			this.RegulonId = RegulonId;
			this.Score = Score;
			this.Sequence = Sequence;
		}

		/// <summary> Converts an arbitrary JavaScript object which contains 
		///		values for the required fields represented as strings.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public static Site Parse ( JSObject obj ) {
			return new Site( obj );
		}

		/// <summary> Compares two sites by Gene Locus Tag.
		/// </summary>
		/// <param name="x">
		///		A (possibly null) site to compare.
		/// </param>
		/// <param name="y">
		///		A (possibly null) site to compare.
		/// </param>
		/// <returns>
		///		The comparison result between x.GeneLocusTag and y.GeneLocusTag
		/// </returns>

		public static int OrderByLocusTag ( Site x, Site y ) {
			if ( x == null ) {
				return y == null ? 0 : -1;
			}
			else if ( y == null ) {
				return 1;
			}
			else {
				return String.Compare( x.GeneLocusTag, y.GeneLocusTag );
			}
		}
	}
}
