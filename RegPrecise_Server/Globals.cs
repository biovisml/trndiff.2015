﻿using System;
using System.Collections.Generic;


namespace RegPrecise {

	/// <summary> REpository for shared constants.
	/// </summary>

	public class Globals {

		/// <summary> "Required arguments not supplied."
		/// </summary>
		public const string REQUIRED_ARGS = "Required arguments not supplied.";

	}
}
