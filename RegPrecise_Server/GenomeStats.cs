﻿using System;
using System.Collections.Generic;

using SystemQut;

namespace RegPrecise {
	/// <summary> General statistics on regulons and regulatory sites in genomes
	/// </summary>
	public class GenomeStats: JSObject {
		/// <summary> genome identifier
		/// </summary>

		public int GenomeId;

		/// <summary> genome name
		/// </summary>

		public string Name;

		/// <summary> NCBI taxonomy id
		/// </summary>

		public int TaxonomyId;

		/// <summary> total number of TF-controlled regulons reconstructed in genome
		/// </summary>

		public int TfRegulonCount;

		/// <summary> total number of TF binding sites in genome
		/// </summary>

		public int TfSiteCount;

		/// <summary> total number of RNA-controlled regulons reconstructed in genome
		/// </summary>

		public int RnaRegulonCount;

		/// <summary> total number of RNA regulatory sites in genome
		/// </summary>

		public int RnaSiteCount;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="genomeId"> genome identifier </param>
		/// <param name="name"> genome name </param>
		/// <param name="taxonomyId"> NCBI taxonomy id </param>
		/// <param name="tfRegulonCount"> total number of TF-controlled regulons reconstructed in genome </param>
		/// <param name="tfSiteCount"> total number of TF binding sites in genome </param>
		/// <param name="rnaRegulonCount"> total number of RNA-controlled regulons reconstructed in genome </param>
		/// <param name="rnaSiteCount"> total number of RNA regulatory sites in genome </param>

		public GenomeStats (
			int genomeId,
			string name,
			int taxonomyId,
			int tfRegulonCount,
			int tfSiteCount,
			int rnaRegulonCount,
			int rnaSiteCount
		) {
			IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0:
					return;
				case 1:
					SetFromJSObject( (JSObject) args[0] );
					return;
				case 7:
					SetAllValues( genomeId, name, taxonomyId, tfRegulonCount, tfSiteCount, rnaRegulonCount, rnaSiteCount );
					return;
				default:
					throw new Exception( Globals.REQUIRED_ARGS );
			}
		}

		/// <summary> Parameterless constructor.
		/// </summary>

		public extern GenomeStats ();

		/// <summary> Copy constructor accepting an arbitrary JavaScript Object.
		/// </summary>

		public extern GenomeStats ( JSObject obj );

		private void SetAllValues ( int genomeId, string name, int taxonomyId, int tfRegulonCount, int tfSiteCount, int rnaRegulonCount, int rnaSiteCount ) {
			this.GenomeId = genomeId;
			this.Name = name;
			this.TaxonomyId = taxonomyId;
			this.TfRegulonCount = tfRegulonCount;
			this.TfSiteCount = tfSiteCount;
			this.RnaRegulonCount = rnaRegulonCount;
			this.RnaSiteCount = rnaSiteCount;
		}

		/// <summary> constructs a GenomeStats record from a JavaScript object which is presumed to contain the
		///		appropriate named fields, represented as strings.
		/// </summary>
		/// <param name="obj"> The source JavaScript object. </param>
		/// <returns></returns>

		private void SetFromJSObject ( JSObject obj ) {
			SetAllValues(
				int.Parse( (string) obj["genomeId"] ),
				String.Convert( obj["name"] ),
				int.Parse( (string) obj["taxonomyId"] ),
				int.Parse( (string) obj["tfRegulonCount"] ),
				int.Parse( (string) obj["tfSiteCount"] ),
				int.Parse( (string) obj["rnaRegulonCount"] ),
				int.Parse( (string) obj["rnaSiteCount"] )
			);

			string [] fieldsExcludedFromCopy = {
				"genomeId", "name", "taxonomyId", "tfRegulonCount", "tfSiteCount", "rnaRegulonCount", "rnaSiteCount"
			};

			Objects.ShallowCopy( obj, this, fieldsExcludedFromCopy );
		}

		/// <summary> constructs a GenomeStats record from a JavaScript object which is presumed to contain the
		///		appropriate named fields, represented as strings.
		/// </summary>
		/// <param name="obj"> The source JavaScript object. </param>
		/// <returns></returns>

		public static GenomeStats Parse ( JSObject obj ) {
			return new GenomeStats( obj );
		}

		/// <summary> Compares two GenomeStats objects by id.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>

		public static int OrderById( GenomeStats x, GenomeStats y ) {
			return x.GenomeId - y.GenomeId;
		}
	}
}
