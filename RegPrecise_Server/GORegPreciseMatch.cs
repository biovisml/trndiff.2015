﻿using System;
using System.Collections.Generic;
using SystemQut;

namespace RegPrecise
{
    /// <summary>
    /// This is intended to connect RegPrecise gene functions to GO terms, to
    /// allow high-level GO terms to be used for colouring
    /// </summary>
    public class GORegPreciseMatch : JSObject
    {
        /// <summary>
        /// The gene function that this match is for
        /// </summary>
        private string RP_geneFunction;

        /// <summary>
        /// The name of the highest parent GO term of the match
        /// </summary>
        private string GO_parentName;

        /// <summary>
        /// The name of the gene ontology term it was matched to
        /// </summary>
        private string GO_name;

        /// <summary>
        /// Gets the RegPrecise gene function in the match
        /// </summary>
        public string GeneFunction
        {
            get { return RP_geneFunction; }
        }

        /// <summary>
        /// Gets the GO parent term in the match
        /// </summary>
        public string ParentTermName
        {
            get { return GO_parentName; }
        }

        /// <summary>
        /// Gets the GO term in the match
        /// </summary>
        public string TermName
        {
            get { return GO_name; }
        }

        /// <summary>
        /// Creates a new object based on imported data
        /// </summary>
        /// <param name="RP_geneFunction">The RegPrecise gene function part of the match</param>
        /// <param name="GO_name">The GO term part of the match</param>
        /// <param name="GO_parentName">The parent GO term part of the match</param>
        public GORegPreciseMatch (string RP_geneFunction, string GO_name, string GO_parentName) {

            IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {

                // Don't do anything if there are no arguments
				case 0: return;

                // If there is one argument, then it is a JSObject and the
                // object must be created from that
				case 1:
					SetFromJSObject( (JSObject) args[0] );
					break;

                // If there are three arguments, then they are regular parameters
                // and so set the RegPrecise gene function, GO term and parent
                // GO term normally
				case 3:
					SetAllFields( RP_geneFunction, GO_name, GO_parentName );
					break;
				default:
					throw new Exception( Globals.REQUIRED_ARGS );
			}
        }

        /// <summary>
        /// Sets all of the fields of the objcct based on given parameters
        /// </summary>
        /// <param name="RP_geneFunction">The RegPrecise gene function part of the match</param>
        /// <param name="GO_name">The GO term part of the match</param>
        /// <param name="GO_parentName">The parent GO term part of the match</param>
        private void SetAllFields(string RP_geneFunction, string GO_name, string GO_parentName) {
            this.RP_geneFunction = RP_geneFunction;
            this.GO_parentName = GO_parentName;
            this.GO_name = GO_name;
        }

		/// <summary> Factory method to convert from JavaScript object to GORegPreciseMatch.
		/// </summary>
		/// <param name="obj">
		///		A JavaScript object that contains the required fields, encoded as strings.
		/// </param>
		/// <returns></returns>

        public static GORegPreciseMatch Parse ( JSObject obj ) {
            return new GORegPreciseMatch( obj );
        }

		/// <summary> Parameterless constructor.
		/// </summary>

		public extern GORegPreciseMatch ();

		/// <summary> Copy constructor accepting an arbitrary JavaScript object such as one obtains from Json.Parse.
		/// </summary>

		public extern GORegPreciseMatch ( JSObject obj );

		/// <summary> Parses a GORegPreciseMatch from a JavaScript object which is assumed to contain the required fields.
		/// </summary>

		private void SetFromJSObject ( JSObject record ) {

            // Pass the data from the JSObject to the regular field setting
            // function
            SetAllFields(
                Objects.ToString(record["RP_geneFunction"], ""),
                Objects.ToString(record["name"], ""),
                Objects.ToString(record["parentName"], "")
			);
			string [] fieldsExcludedFromCopy = {
				"RP_geneFunction", "name", "parentName"
			};
			Objects.ShallowCopy( record, this, fieldsExcludedFromCopy );
        }
    }
}
