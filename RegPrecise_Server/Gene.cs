﻿using System;
using System.Collections.Generic;

using SystemQut;

namespace RegPrecise {

	/// <summary> Gene descriptor from the RegPrecise service.
	/// </summary>

	public class Gene: JSObject {
		/// <summary> identifier of a regulon
		/// </summary>

		public int RegulonId;

		/// <summary> name of gene
		/// </summary>

		public string Name;

		/// <summary> locus tag of a gene in GeneBank
		/// </summary>

		public string LocusTag;

		/// <summary> identifier of gene in MicrobesOnline database
		/// </summary>

		public int VimssId;

		/// <summary> gene function
		/// </summary>

		public string GeneFunction;

        /// <summary> the matched GO term, if any
        /// </summary>

        public string GoTerm;

        /// <summary>
        /// the matched parent GO term, if any
        /// </summary>

        public string GoParentTerm;

		/// <summary> Initialise a new Gene Descriptor.
		/// </summary>
		/// <param name="regulonId"> identifier of a regulon  </param>
		/// <param name="name"> name of gene </param>
		/// <param name="locusTag"> locus tag of a gene in GeneBank </param>
		/// <param name="vimssId"> identifier of gene in MicrobesOnline database  </param>
        /// <param name="geneFunction"> gene function </param>
        /// <param name="goTerm"> the matched GO term, if any</param>
        /// <param name="goParentTerm"> the matched parent GO term, if any</param>

		public Gene (
			int regulonId,
			string name,
			string locusTag,
			int vimssId,
			string geneFunction,
            string goTerm,
            string goParentTerm
		) {
			IArrayLike<object> args = Arguments.Current;

			switch ( args.Length ) {
				case 0:
					return;
				case 1:
					SetFromJSObject( (JSObject) args[0] );
					return;
				case 5:
                    SetAllFields2( regulonId, name, locusTag, vimssId, geneFunction, string.Empty, string.Empty );
                    return;
				case 6:
                    SetAllFields2( regulonId, name, locusTag, vimssId, geneFunction, string.Empty, string.Empty );
                    return;
				case 7:
                    // For backwards compatibility with CSV, use the goTerm as
                    // the goParentTerm since that's what it was before
                    SetAllFields2( regulonId, name, locusTag, vimssId, geneFunction, string.Empty, goTerm );
                    return;
                case 8:
                    SetAllFields2( regulonId, name, locusTag, vimssId, geneFunction, goTerm, goParentTerm );
                    return;
				default:
					throw new Exception( Globals.REQUIRED_ARGS );
			}
		}

		/// <summary> Initialises the Gene from a JavaScript object.
		/// </summary>
		/// <param name="obj"></param>

		private void SetFromJSObject ( JSObject obj ) {

            // For backwards compatibility with CSV, use the goTerm as
            // the goParentTerm since that's what it was before
            if ( obj["goParentTerm"] == null && obj["goTerm"] != null ) {
                SetAllFields2(
				    int.Parse( (string) obj["regulonId"] ),
				    String.Convert( obj["name"] ),
				    String.Convert( obj["locusTag"] ),
				    int.Parse( (string) obj["vimssId"] ),
				    String.Convert( Either( obj, "geneFunction", "function" ) ),
                    string.Empty,
                    Objects.ToString( obj["goTerm"], string.Empty)
			    );
            } else {
                SetAllFields2(
				    int.Parse( (string) obj["regulonId"] ),
				    String.Convert( obj["name"] ),
				    String.Convert( obj["locusTag"] ),
				    int.Parse( (string) obj["vimssId"] ),
				    String.Convert( Either( obj, "geneFunction", "function" ) ),
                    Objects.ToString( obj["goTerm"], string.Empty),
                    Objects.ToString( obj["goParentTerm"], string.Empty)
			    );
            }

			string [] fieldsExcludedFromCopy = {
				"regulonId", "name", "locusTag", "vimssId", "geneFunction", "function", "goTerm", "goParentTerm"
			};

			Objects.ShallowCopy( obj, this, fieldsExcludedFromCopy );
		}

		/// <summary> Initialises all fields of the Gene.
		/// </summary>
		/// <param name="regulonId"></param>
		/// <param name="name"></param>
		/// <param name="locusTag"></param>
		/// <param name="vimssId"></param>
		/// <param name="geneFunction"></param>

		public void SetAllFields (
			int regulonId,
			string name,
			string locusTag,
			int vimssId,
			string geneFunction
		) {
			this.RegulonId = regulonId;
			this.Name = name;
			this.LocusTag = locusTag;
			this.VimssId = vimssId;
            this.GeneFunction = geneFunction;
		}

		public void SetAllFields2 (
			int regulonId,
			string name,
			string locusTag,
			int vimssId,
			string geneFunction,
            string goTerm,
            string goParentTerm
		) {
			this.RegulonId = regulonId;
			this.Name = name;
			this.LocusTag = locusTag;
			this.VimssId = vimssId;
            this.GeneFunction = geneFunction;
            this.GoTerm = goTerm;
            this.GoParentTerm = goParentTerm;
		}

		/// <summary> Creates an "empty" Gene record.
		/// </summary>

		public extern Gene ();

		/// <summary> Populate a Gene from a JavaScript object which holds the required
		///		field encoded as strings.
		/// </summary>
		/// <param name="obj"></param>

		public extern Gene ( JSObject obj );

		/// <summary> Constructs a Gene descriptor from a JavaScript record containing the fields
		///		encoded as strings. Convert/Parse methods are used to extract the required values.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public static Gene Parse ( JSObject obj ) {
			return new Gene( obj );
		}

		/// <summary> Gets the value associated with either key from the object.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="key1"></param>
		/// <param name="key2"></param>
		/// <returns></returns>

		private static object Either ( JSObject obj, string key1, string key2 ) {
			return obj.ContainsKey ( key1 ) ? obj[key1] : obj[key2];
		}

		/// <summary> Rank by VIMSS Id.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>

		public static int OrderById ( Gene x, Gene y ) {
			return x.VimssId - y.VimssId;
		}
	}
}
