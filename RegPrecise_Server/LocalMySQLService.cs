﻿using System;
using System.Collections.Generic;

using NodeApi.Network;
using System.Serialization;
using NodeApi.IO;
using SystemQut;
using Enum = SystemQut.Enum;
using Console = System.Console;
using SystemQut.ServiceModel;

namespace RegPrecise {
	/// <summary> Implementation of the IGeneOntologySQLService interface
	/// </summary>

    public class LocalMySQLService : ILocalMySQLService {

		/// <summary> Retrieves a list of all terms in the Gene Ontology
        /// database
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void GO_ListTerms (	ResponseProcessor<GOTerm[]> processResults ) {
            Console.Log("Running GO_ListTerms");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GO_ListTerms - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTerm[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('GO_ListTerms - Connection to local MySQL database established'); });");
            Script.Literal("con.query('SELECT * FROM go_term', function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTerm[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GO_ListTerms - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                // Get the returned rows and put them in the correct object
                GOTerm[] terms = new GOTerm[result.Length];
                for (int i = 0; i < result.Length; i++) {
                    terms[i] = new GOTerm(int.Parse(Objects.ToString(result[i]["id"], "")),
                        Objects.ToString(result[i]["name"], ""),
                        Objects.ToString(result[i]["term_type"], ""),
                        Objects.ToString(result[i]["acc"], ""),
                        int.Parse(Objects.ToString(result[i]["is_obsolete"], "")),
                        int.Parse(Objects.ToString(result[i]["is_root"], "")),
                        int.Parse(Objects.ToString(result[i]["is_relation"], "")),
                        null,
                        null
                        );
                    //Console.Log(terms[i].Name + " added to list of terms");
                }
                Console.Log(terms.Length + " terms retrieved");
                processResults( new ServiceResponse<GOTerm[]>(terms, null));
                Script.Literal("con.end(function(err) { console.log('GO_ListTerms - Connection to local MySQL database closed.'); })");
            Script.Literal("})");
		}


        /// <summary> Retrieves a list of all the synonyms of the specified
        /// GO term (from the database id)
		/// <para>
		///		Result is a list of term synonyms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>term_id</item>
        /// <item>term_synonym</item>
        /// <item>acc_synonym</item>
        /// <item>synonym_type_id</item>
        /// <item>synonym_category_id</item>
		/// </list>
		/// </summary>
        /// <param name="termId"></param>
		/// <param name="processResults"></param>

		public void GO_ListSynonymsOfTerm ( int termId, ResponseProcessor<GOTermSynonym[]> processResults ) {
            Console.Log("Running GO_ListSynonymsOfTerm with termId " + termId);;

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GO_ListSynonymsOfTerm - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTermSynonym[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('GO_ListSynonymsOfTerm - Connection to local MySQL database established'); });");
            Script.Literal("con.query('SELECT * FROM go_term_synonym WHERE term_id = ' + termId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTermSynonym[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GO_ListSynonymsOfTerm - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                // Get the returned rows and put them in the correct object
                GOTermSynonym[] synonyms = new GOTermSynonym[result.Length];
                for (int i = 0; i < result.Length; i++) {
                    synonyms[i] = new GOTermSynonym(int.Parse(Objects.ToString(result[i]["term_id"], "")),
                        Objects.ToString(result[i]["term_synonym"], ""),
                        Objects.ToString(result[i]["acc_synonym"], ""),
                        int.Parse(Objects.ToString(result[i]["synonym_type_id"], "")),
                        int.Parse(Objects.ToString(result[i]["synonym_category_id"], ""))
                        );
                    //Console.Log(synonyms[i].Name + " added to list of synonyms");
                }
                Console.Log(synonyms.Length + " synonyms of term " + termId + " retrieved");
                processResults( new ServiceResponse<GOTermSynonym[]>(synonyms, null));
                Script.Literal("con.end(function(err) { console.log('GO_ListSynonymsOfTerm - Connection to local MySQL database closed.'); })");
            Script.Literal("})");
        }

        /// <summary> Retrieves a list of all the relationships that the
        /// specified GO term particpates in as a child
		/// <para>
		///		Result is a list of term relationships with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>relationship_type_id</item>
        /// <item>term1_id</item>
        /// <item>term2_id</item>
        /// <item>complete</item>
		/// </list>
		/// </summary>
		/// <param name="termId"></param>
		/// <param name="processResults"></param>

        public void GO_ListRelationshipsOfTermAsChild ( int termId, ResponseProcessor<GOTermRelationship[]> processResults ) {
            Console.Log("Running GO_ListRelationshipsOfTermAsChild with termId " + termId);

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GO_ListRelationshipsOfTermAsChild - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTermRelationship[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('GO_ListRelationshipsOfTermAsChild - Connection to local MySQL database established'); });");
            Script.Literal("con.query('SELECT * FROM go_term2term WHERE term2_id = ' + termId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTermRelationship[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GO_ListRelationshipsOfTermAsChild - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                // Get the returned rows and put them in the correct object
                GOTermRelationship[] relationships = new GOTermRelationship[result.Length];
                for (int i = 0; i < result.Length; i++) {
                    relationships[i] = new GOTermRelationship(int.Parse(Objects.ToString(result[i]["id"], "")),
                        int.Parse(Objects.ToString(result[i]["relationship_type_id"], "")),
                        int.Parse(Objects.ToString(result[i]["term1_id"], "")),
                        int.Parse(Objects.ToString(result[i]["term2_id"], "")),
                        int.Parse(Objects.ToString(result[i]["complete"], ""))
                        );
                    //Console.Log("Relationship ID " + relationships[i].DatabaseId + " added to list of synonyms");
                }
                Console.Log(relationships.Length + " relationships where " + termId + " is a child retrieved");
                processResults( new ServiceResponse<GOTermRelationship[]>(relationships, null));
                Script.Literal("con.end(function(err) { console.log('GO_ListRelationshipsOfTermAsChild - Connection to local MySQL database closed.'); })");
            Script.Literal("})");
        }

		/// <summary> Retrieves the specified GO term
		/// <para>
		///		Result is a term with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
		/// </list>
		/// </summary>
        /// <param name="termId"></param>
		/// <param name="processResults"></param>

		public void GO_GetTerm ( int termId, ResponseProcessor<GOTerm> processResults ) {
            Console.Log("Running GO_GetTerm with termId " + termId);

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GO_GetTerm - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTerm>(null, errorMessage));
            Script.Literal("return; }\n console.log('GO_GetTerm - Connection to local MySQL database established'); });");
            Script.Literal("con.query('SELECT * FROM go_term WHERE id = ' + termId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTerm>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GO_GetTerm - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                // Get the returned row and put them in the correct object
                // but first check that we actually got a single row
                if (result.Length != 1) {
                    processResults( new ServiceResponse<GOTerm>(null, "Error: too many rows retrieved from the database."));
                    Script.Literal("con.end(function(err) { console.log('GO_GetTerm - Connection to local MySQL database closed due to an error.'); })");
                    return;
                }
                GOTerm term = new GOTerm(int.Parse(Objects.ToString(result[0]["id"], "")),
                        Objects.ToString(result[0]["name"], ""),
                        Objects.ToString(result[0]["term_type"], ""),
                        Objects.ToString(result[0]["acc"], ""),
                        int.Parse(Objects.ToString(result[0]["is_obsolete"], "")),
                        int.Parse(Objects.ToString(result[0]["is_root"], "")),
                        int.Parse(Objects.ToString(result[0]["is_relation"], "")),
                        null,
                        null
                        );
                Console.Log(term.Name + " retrieved");
                processResults( new ServiceResponse<GOTerm>(term, null));
                Script.Literal("con.end(function(err) { console.log('GO_GetTerm - Connection to local MySQL database closed.'); })");
            Script.Literal("})");
		}

        /// <summary> Retrieves a list of all GO terms in the Gene Ontology
        /// database, with all of their synonyms (if any)
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
        /// <item>term_synonyms</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void GO_ListTermsWithSynonyms (	ResponseProcessor<GOTerm[]> processResults ) {
            Console.Log("Running GO_ListTermsWithSynonyms");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GO_ListTermsWithSynonyms - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTerm[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('GO_ListTermsWithSynonyms - Connection to local MySQL database established'); });");
            Script.Literal("con.query('select * from go_term LEFT JOIN go_term_synonym ON (go_term.id = go_term_synonym.term_id) ORDER BY go_term.id', function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTerm[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GO_ListTermsWithSynonyms - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");
                List<GOTerm> terms = new List<GOTerm>();

                // Because we will have multiple instances of some terms, we
                // store the current id so we know when to only add a synonym
                int currentId = 0;

                // Get the returned rows and put them in the correct object
                for (int i = 0; i < result.Length; i++) {

                    // Only create a new term if the currentId is different
                    if (currentId != (int)result[i]["id"]) {
                        currentId = (int)result[i]["id"];
                        terms[currentId] = new GOTerm(int.Parse(Objects.ToString(result[i]["id"], "")),
                            Objects.ToString(result[i]["name"], ""),
                            Objects.ToString(result[i]["term_type"], ""),
                            Objects.ToString(result[i]["acc"], ""),
                            int.Parse(Objects.ToString(result[i]["is_obsolete"], "")),
                            int.Parse(Objects.ToString(result[i]["is_root"], "")),
                            int.Parse(Objects.ToString(result[i]["is_relation"], "")),
                            null,
                            null
                            );
                        //Console.Log(terms[currentId].Name + " added to list of terms");
                    }

                    // If there is a synonym, create a new synonym object
                    if (result[i]["term_id"] != null) {
                        GOTermSynonym synonym = new GOTermSynonym(int.Parse(Objects.ToString(result[i]["term_id"], "")),
                            Objects.ToString(result[i]["term_synonym"], ""),
                            Objects.ToString(result[i]["acc_synonym"], ""),
                            int.Parse(Objects.ToString(result[i]["synonym_type_id"], "")),
                            int.Parse(Objects.ToString(result[i]["synonym_category_id"], ""))
                            );

                        //Adding this because my messing with the GOTerm class made
                        //this default to null, I think?
                        if (terms[currentId].Synonyms == null) {
                            terms[currentId].Synonyms = new List<GOTermSynonym>();
                        }

                        // Add the synonym to the term with the current id
                        terms[currentId].Synonyms.Add(synonym);
                        //Console.Log(synonym.Name + " added to the above term's (" + terms[currentId].Name + ") list of synonyms");
                    }
                }
                // We're getting errors where a null entry is present, I think this
                // is because the database starts at ID 1
                if (terms[0] == null) {
                    //Console.Log("Removing null 0 entry in terms variable.");
                    terms.RemoveAt(0);
                }
                Console.Log("Returning a list of " + terms.Length + " terms with synonyms");
                processResults( new ServiceResponse<GOTerm[]>(terms, null));
                Script.Literal("con.end(function(err) { console.log('GO_ListTermsWithSynonyms - Connection to local MySQL database closed.'); })");
            Script.Literal("})");
		}

        /// <summary> Retrieves a list of all GO terms in the Gene Ontology
        /// database, with all of the relationships where they are a child
        /// (if any)
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
        /// <item>term_relationships</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void GO_ListTermsWithParents (	ResponseProcessor<GOTerm[]> processResults ) {
            Console.Log("Running GO_ListTermsWithParents");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GO_ListTermsWithParents - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTerm[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('GO_ListTermsWithParents - Connection to local MySQL database established'); });");
            Script.Literal("con.query('select go_term.*, go_term2term.id as relationship_id, relationship_type_id, term1_id, term2_id, complete from go_term LEFT JOIN go_term2term ON (go_term.id = go_term2term.term2_id) ORDER BY go_term.id', function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GOTerm[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GO_ListTermsWithParents - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");
                List<GOTerm> terms = new List<GOTerm>();

                // Because we will have multiple instances of some terms, we
                // store the current id so we know when to only add a relationship
                int currentId = 0;

                // Get the returned rows and put them in the correct object
                for (int i = 0; i < result.Length; i++) {

                    // Only create a new term if the currentId is different
                    if (currentId != (int)result[i]["id"]) {
                        currentId = (int)result[i]["id"];
                        terms[currentId] = new GOTerm(int.Parse(Objects.ToString(result[i]["id"], "")),
                            Objects.ToString(result[i]["name"], ""),
                            Objects.ToString(result[i]["term_type"], ""),
                            Objects.ToString(result[i]["acc"], ""),
                            int.Parse(Objects.ToString(result[i]["is_obsolete"], "")),
                            int.Parse(Objects.ToString(result[i]["is_root"], "")),
                            int.Parse(Objects.ToString(result[i]["is_relation"], "")),
                            null,
                            null
                            );
                        //Console.Log(terms[currentId].Name + " added to list of terms");
                    }

                    // If there is a relationship, create a new relationship object
                    if (result[i]["relationship_id"] != null) {
                        GOTermRelationship relationship = new GOTermRelationship(int.Parse(Objects.ToString(result[i]["relationship_id"], "")),
                            int.Parse(Objects.ToString(result[i]["relationship_type_id"], "")),
                            int.Parse(Objects.ToString(result[i]["term1_id"], "")),
                            int.Parse(Objects.ToString(result[i]["term2_id"], "")),
                            int.Parse(Objects.ToString(result[i]["complete"], ""))
                            );

                        //Adding this because my messing with the GOTerm class made
                        //this default to null, I think?
                        if (terms[currentId].Relationships == null) {
                            terms[currentId].Relationships = new List<GOTermRelationship>();
                        }

                        // Add the relationship to the term with the current id
                        terms[currentId].Relationships.Add(relationship);
                        //Console.Log("Relationship id " + relationship.DatabaseId + " added to the above term's (" + terms[currentId].Name + ") list of relationships");
                    }
                }
                // We're getting errors where a null entry is present, I think this
                // is because the database starts at ID 1
                if (terms[0] == null) {
                    //Console.Log("Removing null 0 entry in terms variable.");
                    terms.RemoveAt(0);
                }
                Console.Log("Returning a list of " + terms.Length + " terms with parents");
                processResults( new ServiceResponse<GOTerm[]>(terms, null));
                Script.Literal("con.end(function(err) { console.log('GO_ListTermsWithParents - Connection to local MySQL database closed.'); })");
            Script.Literal("})");
		}

        /// <summary> I have created a list of RegPrecise gene function terms
        /// matched up to the closest GO terms in the molecular function name
        /// space, and the (first) "top most parent" of that term. This
        /// retrives a list from that, with each gene function and their "top
        /// most parent"
		/// <para>
		///		Result is a JSObject dictionary with the gene function as the
        ///		key and the "category" as the value
        /// </para>
		/// <list type="bullet">
        /// <item>RP_geneFunction</item>
        /// <item>Top_most_GO_id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void GO_GetRegPreciseCategoryMatches (	ResponseProcessor<GORegPreciseMatch[]> processResults ) {
            Console.Log("Running GO_GetRegPreciseCategoryMatches");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GO_GetRegPreciseCategoryMatches - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GORegPreciseMatch[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('GO_GetRegPreciseCategoryMatches - Connection to local MySQL database established'); });");
            Script.Literal("con.query('SELECT RP_geneFunction, t2.name as name, t1.name as parentName FROM go_regprecise_matches, go_term t1, go_term t2 WHERE Top_most_GO_id = t1.id AND GO_id = t2.id', function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GORegPreciseMatch[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GO_GetRegPreciseCategoryMatches - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                // Get the returned rows and put them in the correct object
                GORegPreciseMatch[] matches = new GORegPreciseMatch[result.Length];
                for (int i = 0; i < result.Length; i++) {
                    matches[i] = new GORegPreciseMatch( Objects.ToString(result[i]["RP_geneFunction"], ""),
                        Objects.ToString(result[i]["name"], ""),
                        Objects.ToString(result[i]["parentName"], "")
                        );
                }

                processResults( new ServiceResponse<GORegPreciseMatch[]>(matches, null));
                Script.Literal("con.end(function(err) { console.log('GO_GetRegPreciseCategoryMatches - Connection to local MySQL database closed.'); })");
            Script.Literal("})");
		}

        /// <summary> Gets the general statistics on regulons and regulatory sites in all genomes.
		/// </summary>
		/// <param name="processResults"> A delegate that will be invoked when the results are available. </param>

		public void ListGenomeStats ( ResponseProcessor<GenomeStats[]> processResults ) {
            Console.Log("Running ListGenomeStats");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListGenomeStats - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GenomeStats[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListGenomeStats - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListGenomeStats);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<GenomeStats[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListGenomeStats - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<GenomeStats[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListGenomeStats - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM genome_stats', function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<GenomeStats[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListGenomeStats - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    GenomeStats[] genomeStats = new GenomeStats[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        genomeStats[i] = new GenomeStats(int.Parse(Objects.ToString(result[i]["genome_id"], "")),
                            Objects.ToString(result[i]["name"], ""),
                            int.Parse(Objects.ToString(result[i]["taxonomy_id"], "")),
                            int.Parse(Objects.ToString(result[i]["tf_regulon_count"], "")),
                            int.Parse(Objects.ToString(result[i]["tf_site_count"], "")),
                            int.Parse(Objects.ToString(result[i]["rna_regulon_count"], "")),
                            int.Parse(Objects.ToString(result[i]["rna_site_count"], ""))
                            );
                    }
                    Console.Log(genomeStats.Length + " genome stats retrieved");
                    processResults( new ServiceResponse<GenomeStats[]>(genomeStats, null));
                    Script.Literal("con.end(function(err) { console.log('ListGenomeStats - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Lists the regulators in a regulon.
		/// </summary>
		/// <param name="regulonId"> The id of a regulon. </param>
		/// <param name="processResults"> A delegate that will process the results. </param>

		public void ListRegulatorsInRegulon (
			int regulonId,
			ResponseProcessor<Regulator[]> processResults
		) {
            Console.Log("Running ListRegulatorsInRegulon");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListRegulatorsInRegulon - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulator[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListRegulatorsInRegulon - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListRegulatorsInRegulon, regulonId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulator[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListRegulatorsInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Regulator[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListRegulatorsInRegulon - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM regulators WHERE regulon_id = ' + regulonId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Regulator[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListRegulatorsInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Regulator[] regulators = new Regulator[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        regulators[i] = new Regulator(int.Parse(Objects.ToString(result[i]["regulon_id"], "")),
                            Objects.ToString(result[i]["name"], ""),
                            Objects.ToString(result[i]["locus_tag"], ""),
                            int.Parse(Objects.ToString(result[i]["vimss_id"], "")),
                            Objects.ToString(result[i]["regulator_family"], "")
                            );
                    }
                    Console.Log(regulators.Length + " regulators retrieved");
                    processResults( new ServiceResponse<Regulator[]>(regulators, null));
                    Script.Literal("con.end(function(err) { console.log('ListRegulatorsInRegulon - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Lists the regulators in a regulog.
		/// </summary>
		/// <param name="regulogId"> The id of a regulog. </param>
		/// <param name="processResults"> A delegate that will process the results. </param>

		public void ListRegulatorsInRegulog (
			int regulogId,
			ResponseProcessor<Regulator[]> processResults
		) {
            Console.Log("Running ListRegulatorsInRegulog");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListRegulatorsInRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulator[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListRegulatorsInRegulog - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListRegulatorsInRegulog, regulogId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulator[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListRegulatorsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Regulator[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListRegulatorsInRegulog - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('select regulators.* from regulators, regulons where regulators.regulon_id = regulons.regulon_id AND regulons.regulog_id = ' + regulogId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Regulator[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListRegulatorsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Regulator[] regulators = new Regulator[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        regulators[i] = new Regulator(int.Parse(Objects.ToString(result[i]["regulon_id"], "")),
                            Objects.ToString(result[i]["name"], ""),
                            Objects.ToString(result[i]["locus_tag"], ""),
                            int.Parse(Objects.ToString(result[i]["vimss_id"], "")),
                            Objects.ToString(result[i]["regulator_family"], "")
                            );
                    }
                    Console.Log(regulators.Length + " regulators retrieved");
                    processResults( new ServiceResponse<Regulator[]>(regulators, null));
                    Script.Literal("con.end(function(err) { console.log('ListRegulatorsInRegulog - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Gets the specified regulog descriptor.
		/// </summary>
		/// <param name="regulogId">The id of a regulog.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void GetRegulog (
			int regulogId,
			ResponseProcessor<Regulog> processResults
		) {
            Console.Log("Running GetRegulog");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GetRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulog>(null, errorMessage));
            Script.Literal("return; }\n console.log('GetRegulog - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_GetRegulog, regulogId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulog>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GetRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Regulog>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('GetRegulog - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM regulogs WHERE regulog_id = ' + regulogId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Regulog>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('GetRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned row and put them in the correct object
                    // but first check that we actually got a single row
                    if (result.Length != 1) {
                        processResults( new ServiceResponse<Regulog>(null, "Error: too many rows retrieved from the database."));
                        Script.Literal("con.end(function(err) { console.log('GetRegulog - Connection to local MySQL database closed due to an error.'); })");
                        return;
                    }
                    Regulog regulog = new Regulog(
                            Objects.ToString(result[0]["effector"], ""),
                            RegulationTypes.Parse(Objects.ToString(result[0]["regulation_type"], "")),
                            Objects.ToString(result[0]["regulator_family"], ""),
                            Objects.ToString(result[0]["regulator_name"], ""),
                            int.Parse(Objects.ToString(result[0]["regulog_id"], "")),
                            Objects.ToString(result[0]["taxon_name"], ""),
                            Objects.ToString(result[0]["pathway"], "")
                            );
                    Console.Log("Regulog " + regulog.RegulogId + " retrieved");
                    processResults( new ServiceResponse<Regulog>(regulog, null));
                    Script.Literal("con.end(function(err) { console.log('GetRegulog - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Gets the specified regulon descriptor.
		/// </summary>
		/// <param name="regulonId">The id of a regulon.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void GetRegulon (
			int regulonId,
			ResponseProcessor<Regulon> processResults
		) {
            Console.Log("Running GetRegulon");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'GetRegulon - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulon>(null, errorMessage));
            Script.Literal("return; }\n console.log('GetRegulon - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListGenomeStats);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulon>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('GetRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Regulon>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('GetRegulon - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM regulons WHERE regulon_id = ' + regulonId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Regulon>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('GetRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned row and put them in the correct object
                    // but first check that we actually got a single row
                    if (result.Length != 1) {
                        processResults( new ServiceResponse<Regulon>(null, "Error: too many rows retrieved from the database."));
                        Script.Literal("con.end(function(err) { console.log('GetRegulon - Connection to local MySQL database closed due to an error.'); })");
                        return;
                    }
                    Regulon regulon = new Regulon(
                            int.Parse(Objects.ToString(result[0]["regulon_id"], "")),
                            int.Parse(Objects.ToString(result[0]["regulog_id"], "")),
                            int.Parse(Objects.ToString(result[0]["genome_id"], "")),
                            Objects.ToString(result[0]["genome_name"], ""),
                            Objects.ToString(result[0]["regulator_name"], ""),
                            Objects.ToString(result[0]["regulator_family"], ""),
                            RegulationTypes.Parse(Objects.ToString(result[0]["regulation_type"], "")),
                            Objects.ToString(result[0]["effector"], ""),
                            Objects.ToString(result[0]["pathway"], "")
                            );
                    Console.Log("Regulon " + regulon.RegulonId + " retrieved");
                    processResults( new ServiceResponse<Regulon>(regulon, null));
                    Script.Literal("con.end(function(err) { console.log('GetRegulon - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

        /// <summary> Retrieves a list of genomes that have at least one reconstructed regulon.
		/// <para>
		///		Result is a list of genomes with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>genomeId - genome identifier</item>
		/// <item>name - genome name</item>
		/// <item>taxonomyId - NCBI taxonomy id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void ListGenomes (
			ResponseProcessor<Genome[]> processResults
		) {
            Console.Log("Running ListGenomes");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListGenomes - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Genome[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListGenomes - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListGenomeStats);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Genome[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListGenomes - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Genome[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListGenomes - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM genomes', function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Genome[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListGenomes - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Genome[] genomes = new Genome[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        genomes[i] = new Genome(int.Parse(Objects.ToString(result[i]["genome_id"], "")),
                            Objects.ToString(result[i]["name"], ""),
                            int.Parse(Objects.ToString(result[i]["taxonomy_id"], ""))
                            );
                    }
                    Console.Log(genomes.Length + " genomes retrieved");
                    processResults( new ServiceResponse<Genome[]>(genomes, null));
                    Script.Literal("con.end(function(err) { console.log('ListGenomes - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Retrieves a list of regulogs that belongs to a specific collection
		///	<para>
		///		Requires type and identifier of a regulog collection
		///	</para>
		///	<para>
		///		Returns a list of regulogs. Each regulog is provided with the following data:
		///	</para>
		///	<list type="bullet">
		///	<item>
		///		•regulogId - identifier of regulog
		///	</item>
		///	<item>
		///		•regulatorName - name of regulator
		///	</item>
		///	<item>
		///		•regulatorFamily - family of regulator
		///	</item>
		///	<item>
		///		•regulationType - type of regulation: either TF (transcription factor) or RNA
		///	</item>
		///	<item>
		///		•taxonName - name of taxonomic group
		///	</item>
		///	<item>
		///		•effector - effector molecule or environmental signal of a regulator
		///	</item>
		///	<item>
		///		•pathway - metabolic pathway or biological process controlled by a regulator
		///	</item>
		///	</list>
		/// </summary>

		public void ListRegulogs (
			RegulogCollectionType collectionType,
			Number collectionId,
			ResponseProcessor<Regulog[]> processResults
		) {
            Console.Log("Running ListRegulogs");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListRegulogs - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulog[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListRegulogs - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListRegulogs, collectionType, collectionId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulog[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListRegulogs - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Regulog[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListRegulogs - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('select regulogs.* from regulogs, regulogs_regulog_collections WHERE regulogs_regulog_collections.collection_id = '  + collectionId + ' AND regulogs_regulog_collections.collection_type = \"' + collectionType + '\" AND regulogs_regulog_collections.regulog_id = regulogs.regulog_id', function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Regulog[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListRegulogs - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Regulog[] regulogs = new Regulog[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        regulogs[i] = new Regulog(
                            Objects.ToString(result[i]["effector"], ""),
                            RegulationTypes.Parse(Objects.ToString(result[i]["regulation_type"], "")),
                            Objects.ToString(result[i]["regulator_family"], ""),
                            Objects.ToString(result[i]["regulator_name"], ""),
                            int.Parse(Objects.ToString(result[i]["regulog_id"], "")),
                            Objects.ToString(result[i]["taxon_name"], ""),
                            Objects.ToString(result[i]["pathway"], "")
                        );
                    }
                    Console.Log(regulogs.Length + " regulogs retrieved");
                    processResults( new ServiceResponse<Regulog[]>(regulogs, null));
                    Script.Literal("con.end(function(err) { console.log('ListRegulogs - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Retrieves a list of regulons belonging to either a particular regulog.
		/// </summary>
		/// <param name="regulogId">
		///		The numeric regulog Id.
		///	</param>
		/// <param name="processResults">
		///		A delegate which will process the results.
		///	</param>

		public void ListRegulonsInRegulog (
			Number regulogId,
			ResponseProcessor<Regulon[]> processResults
		) {
            Console.Log("Running ListRegulonsInRegulog");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListRegulonsInRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulon[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListRegulonsInRegulog - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListRegulonsInRegulog, regulogId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulon[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListRegulonsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Regulon[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListRegulonsInRegulog - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM regulons WHERE regulog_id = ' + regulogId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Regulon[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListRegulonsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Regulon[] regulons = new Regulon[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        regulons[i] = new Regulon(
                            int.Parse(Objects.ToString(result[i]["regulon_id"], "")),
                            int.Parse(Objects.ToString(result[i]["regulog_id"], "")),
                            int.Parse(Objects.ToString(result[i]["genome_id"], "")),
                            Objects.ToString(result[i]["genome_name"], ""),
                            Objects.ToString(result[i]["regulator_name"], ""),
                            Objects.ToString(result[i]["regulator_family"], ""),
                            RegulationTypes.Parse(Objects.ToString(result[i]["regulation_type"], "")),
                            Objects.ToString(result[i]["effector"], ""),
                            Objects.ToString(result[i]["pathway"], "")
                            );
                    }
                    Console.Log(regulons.Length + " regulons retrieved");
                    processResults( new ServiceResponse<Regulon[]>(regulons, null));
                    Script.Literal("con.end(function(err) { console.log('ListRegulonsInRegulog - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Retrieves a list of regulons belonging to a particular genome.
		/// </summary>
		/// <param name="genomeId">
		///		The genome Id.
		/// </param>
		/// <param name="processResults">
		///		A delegate which will process the results.
		///	</param>

		public void ListRegulonsInGenome (
			Number genomeId,
			ResponseProcessor<Regulon[]> processResults
		) {
            Console.Log("Running ListRegulonsInGenome");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListRegulonsInGenome - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulon[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListRegulonsInGenome - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListRegulonsInGenome, genomeId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Regulon[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListRegulonsInGenome - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Regulon[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListRegulonsInGenome - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM regulons WHERE genome_id = ' + genomeId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Regulon[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListRegulonsInGenome - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Regulon[] regulons = new Regulon[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        regulons[i] = new Regulon(
                            int.Parse(Objects.ToString(result[i]["regulon_id"], "")),
                            int.Parse(Objects.ToString(result[i]["regulog_id"], "")),
                            int.Parse(Objects.ToString(result[i]["genome_id"], "")),
                            Objects.ToString(result[i]["genome_name"], ""),
                            Objects.ToString(result[i]["regulator_name"], ""),
                            Objects.ToString(result[i]["regulator_family"], ""),
                            RegulationTypes.Parse(Objects.ToString(result[i]["regulation_type"], "")),
                            Objects.ToString(result[i]["effector"], ""),
                            Objects.ToString(result[i]["pathway"], "")
                            );
                    }
                    Console.Log(regulons.Length + " regulons retrieved");
                    processResults( new ServiceResponse<Regulon[]>(regulons, null));
                    Script.Literal("con.end(function(err) { console.log('ListRegulonsInGenome - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListSitesInRegulon (
			Number regulonId,
			ResponseProcessor<Site[]> processResults
		) {
            Console.Log("Running ListSitesInRegulon");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListSitesInRegulon - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Site[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListSitesInRegulon - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListSitesInRegulon, regulonId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Site[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListSitesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<Site[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListSitesInRegulon - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM sites WHERE regulon_id = ' + regulonId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Site[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListSitesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Site[] sites = new Site[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        sites[i] = new Site(
                            Objects.ToString(result[i]["gene_locus_tag"], ""),
                            int.Parse(Objects.ToString(result[i]["gene_vimss_id"], "")),
                            int.Parse(Objects.ToString(result[i]["position"], "")),
                            int.Parse(Objects.ToString(result[i]["regulon_id"], "")),
                            int.Parse(Objects.ToString(result[i]["score"], "")),
                            Objects.ToString(result[i]["sequence"], "")
                            );
                    }
                    Console.Log(sites.Length + " sites retrieved");
                    processResults( new ServiceResponse<Site[]>(sites, null));
                    Script.Literal("con.end(function(err) { console.log('ListSitesInRegulon - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListSitesInRegulog (
			Number regulogId,
			ResponseProcessor<Site[]> processResults
		) {
            Console.Log("Running ListSitesInRegulog");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListSitesInRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Site[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListSitesInRegulog - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListSitesInRegulog, regulogId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Site[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListSitesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                    // If it isn't found, return an empty object
                    if (result.Length == 0 || result[0]["url"] != requestURL) {
                        Console.Log("The url " + requestURL + " has not yet been used");
                        processResults( new ServiceResponse<Site[]>(null, "No data to retrieve"));
                        Script.Literal("con.end(function(err) { console.log('ListSitesInRegulog - Connection to local MySQL database closed.'); })");
                        return;
                    }

                    // Otherwise, proceed to load the data
                Script.Literal("con.query('select sites.* from sites, regulons where sites.regulon_id = regulons.regulon_id AND regulons.regulog_id = ' + regulogId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Site[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListSitesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Site[] sites = new Site[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        sites[i] = new Site(
                            Objects.ToString(result[i]["gene_locus_tag"], ""),
                            int.Parse(Objects.ToString(result[i]["gene_vimss_id"], "")),
                            int.Parse(Objects.ToString(result[i]["position"], "")),
                            int.Parse(Objects.ToString(result[i]["regulon_id"], "")),
                            int.Parse(Objects.ToString(result[i]["score"], "")),
                            Objects.ToString(result[i]["sequence"], "")
                            );
                    }
                    Console.Log(sites.Length + " sites retrieved");
                    processResults( new ServiceResponse<Site[]>(sites, null));
                    Script.Literal("con.end(function(err) { console.log('ListSitesInRegulog - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Gets the list of genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListGenesInRegulon (
			Number regulonId,
			ResponseProcessor<Gene[]> processResults
		) {
            Console.Log("Running ListGenesInRegulon");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListGenesInRegulon - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Gene[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListGenesInRegulon - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListGenesInRegulon, regulonId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Gene[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListGenesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                    // If it isn't found, return an empty object
                    if (result.Length == 0 || result[0]["url"] != requestURL) {
                        Console.Log("The url " + requestURL + " has not yet been used");
                        processResults( new ServiceResponse<Gene[]>(null, "No data to retrieve"));
                        Script.Literal("con.end(function(err) { console.log('ListGenesInRegulon - Connection to local MySQL database closed.'); })");
                        return;
                    }

                    // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM genes WHERE regulon_id = ' + regulonId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Gene[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListGenesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Gene[] genes = new Gene[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        genes[i] = new Gene(
                            int.Parse(Objects.ToString(result[i]["regulon_id"], "")),
                            Objects.ToString(result[i]["name"], ""),
                            Objects.ToString(result[i]["locus_tag"], ""),
                            int.Parse(Objects.ToString(result[i]["vimss_id"], "")),
                            Objects.ToString(result[i]["gene_function"], ""),
                            null,
                            null
                            );
                    }
                    Console.Log(genes.Length + " genes retrieved");
                    processResults( new ServiceResponse<Gene[]>(genes, null));
                    Script.Literal("con.end(function(err) { console.log('ListGenesInRegulon - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Gets the list of genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListGenesInRegulog (
			Number regulogId,
			ResponseProcessor<Gene[]> processResults
		) {
            Console.Log("Running ListGenesInRegulog");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListGenesInRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Gene[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListGenesInRegulog - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListGenesInRegulog, regulogId);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Gene[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListGenesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                    // If it isn't found, return an empty object
                    if (result.Length == 0 || result[0]["url"] != requestURL) {
                        Console.Log("The url " + requestURL + " has not yet been used");
                        processResults( new ServiceResponse<Gene[]>(null, "No data to retrieve"));
                        Script.Literal("con.end(function(err) { console.log('ListGenesInRegulog - Connection to local MySQL database closed.'); })");
                        return;
                    }

                    // Otherwise, proceed to load the data
                Script.Literal("con.query('select genes.* from genes, regulons where genes.regulon_id = regulons.regulon_id AND regulons.regulog_id = ' + regulogId, function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Gene[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListGenesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    Gene[] genes = new Gene[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        genes[i] = new Gene(
                            int.Parse(Objects.ToString(result[i]["regulon_id"], "")),
                            Objects.ToString(result[i]["name"], ""),
                            Objects.ToString(result[i]["locus_tag"], ""),
                            int.Parse(Objects.ToString(result[i]["vimss_id"], "")),
                            Objects.ToString(result[i]["gene_function"], ""),
                            null,
                            null
                            );
                    }
                    Console.Log(genes.Length + " genes retrieved");
                    processResults( new ServiceResponse<Gene[]>(genes, null));
                    Script.Literal("con.end(function(err) { console.log('ListGenesInRegulog - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

		/// <summary> Get a list of regulog collections or the specified type.
		/// <para>
		///		Result is a list of regulog collections. Each regulog collection is provided with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>collectionType - type of regulog collection</item>
		/// <item>collectionId - identifier of collection</item>
		/// <item>name - collection name</item>
		/// <item>className - name of collection class </item>
		/// </list>
		/// </summary>
		/// <param name="collectionType"> The idneity of the required collection type. </param>
		/// <param name="processResults"> A callback that will process the results. </param>

		public void ListRegulogCollections (
			RegulogCollectionType collectionType,
			ResponseProcessor<RegulogCollection[]> processResults
		) {
            Console.Log("Running ListRegulogCollections");

            // Stores the result of the query
            JSObject[] result = new JSObject[0];

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); var con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'ListRegulogCollections - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<RegulogCollection[]>(null, errorMessage));
            Script.Literal("return; }\n console.log('ListRegulogCollections - Connection to local MySQL database established'); });");

            // First, check if the results from this request have already been
            // put into the database, by checking for the url query string in
            // the special table
            string requestURL = String.Format(regPreciseURL_ListRegulogCollections, collectionType);
            Script.Literal("con.query('SELECT url FROM previous_requests WHERE url = \"' + requestURL + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to check for request URL in the database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<RegulogCollection[]>(null, errorMessage));
            Script.Literal("con.end(function(err) { console.log('ListRegulogCollections - Connection to local MySQL database closed due to an error.'); }); \n return; }\n");

                // If it isn't found, return an empty object
                if (result.Length == 0 || result[0]["url"] != requestURL) {
                    Console.Log("The url " + requestURL + " has not yet been used");
                    processResults( new ServiceResponse<RegulogCollection[]>(null, "No data to retrieve"));
                    Script.Literal("con.end(function(err) { console.log('ListRegulogCollections - Connection to local MySQL database closed.'); })");
                    return;
                }

                // Otherwise, proceed to load the data
                Script.Literal("con.query('SELECT * FROM regulog_collections WHERE collection_type = \"' + collectionType + '\"', function(err, result) {\n if(err) {\n errorMessage = 'Unable to retrieve rows from the database: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<RegulogCollection[]>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('ListRegulogCollections - Connection to local MySQL database closed due to an error.'); }); \n return; }\n  console.log(result.length + ' rows received from the database\\n');");

                    // Get the returned rows and put them in the correct object
                    RegulogCollection[] collections = new RegulogCollection[result.Length];
                    for (int i = 0; i < result.Length; i++) {
                        collections[i] = new RegulogCollection(
                            Objects.ToString(result[i]["class_name"], ""),
                            int.Parse(Objects.ToString(result[i]["collection_id"], "")),
                            RegulogCollectionType.Parse(Objects.ToString(result[i]["collection_type"], "")),
                            Objects.ToString(result[i]["name"], "")
                            );
                    }
                    Console.Log(collections.Length + " regulog collections retrieved");
                    processResults( new ServiceResponse<RegulogCollection[]>(collections, null));
                    Script.Literal("con.end(function(err) { console.log('ListRegulogCollections - Connection to local MySQL database closed.'); })");
                Script.Literal("})");
            Script.Literal("})");
        }

        // Request URLs for all supported RegPrecise service requests
        private const string regPreciseURL_ListRegulogs =              "http://regprecise.lbl.gov/Services/rest/regulogs?collectionType={0}&collectionId={1}";
        private const string regPreciseURL_ListRegulogCollections =    "http://regprecise.lbl.gov/Services/rest/regulogCollections?collectionType={0}";
        private const string regPreciseURL_ListGenomes =               "http://regprecise.lbl.gov/Services/rest/genomes";
        private const string regPreciseURL_ListRegulonsInRegulog =     "http://regprecise.lbl.gov/Services/rest/regulons?regulogId={0}";
        private const string regPreciseURL_ListRegulonsInGenome =      "http://regprecise.lbl.gov/Services/rest/regulons?genomeId={0}";
        private const string regPreciseURL_ListSitesInRegulon =        "http://regprecise.lbl.gov/Services/rest/sites?regulonId={0}";
        private const string regPreciseURL_ListSitesInRegulog =        "http://regprecise.lbl.gov/Services/rest/sites?regulogId={0}";
        private const string regPreciseURL_GetRegulog =                "http://regprecise.lbl.gov/Services/rest/regulog?regulogId={0}";
        private const string regPreciseURL_GetRegulon =                "http://regprecise.lbl.gov/Services/rest/regulon?regulonId={0}";
        private const string regPreciseURL_ListRegulatorsInRegulon =   "http://regprecise.lbl.gov/Services/rest/regulators?regulonId={0}";
        private const string regPreciseURL_ListRegulatorsInRegulog =   "http://regprecise.lbl.gov/Services/rest/regulators?regulogId={0}";
        private const string regPreciseURL_ListGenesInRegulon =        "http://regprecise.lbl.gov/Services/rest/genes?regulonId={0}";
        private const string regPreciseURL_ListGenesInRegulog =        "http://regprecise.lbl.gov/Services/rest/genes?regulogId={0}";
        private const string regPreciseURL_ListGenomeStats =           "http://regprecise.lbl.gov/Services/rest/genomeStats";

        /// <summary>
        /// Adds the specified list of regulogs to the MySQL database
        /// <para>Will only add regulogs that do not already exist</para>
        /// <para>Should be used when the regulog is retrieved from RegPrecise using the GetRegulog request</para>
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="collectionId"></param>
        /// <param name="regulogs"> The regulogs to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>

		public void AddRegulogs_ListRegulogs (
			RegulogCollectionType collectionType,
			Number collectionId,
            Regulog[] regulogs,
            ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddRegulogs_ListRegulogs - attempting to add {0} regulogs from collection {1} with type {2}", regulogs.Length, collectionId, collectionType));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[regulogs.Length][];
            string[][] values2 = new string[regulogs.Length][];
            for (int i = 0; i < regulogs.Length; i++) {
                values[i] = new string[7];
                values[i][0] = regulogs[i].Effector;
                values[i][1] = Objects.ToString(RegulationTypes.Parse(regulogs[i].RegulationType), String.Empty);
                values[i][2] = regulogs[i].RegulatorFamily;
                values[i][3] = regulogs[i].RegulatorName;
                values[i][4] = Objects.ToString(regulogs[i].RegulogId, String.Empty);
                values[i][5] = regulogs[i].TaxonName;
                values[i][6] = regulogs[i].Pathway;

                values2[i] = new string[3];
                values2[i][0] = Objects.ToString(regulogs[i].RegulogId, String.Empty);
                values2[i][1] = Objects.ToString(collectionId, String.Empty);
                values2[i][2] = Objects.ToString(RegulogCollectionType.Parse(collectionType), String.Empty);;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListRegulogs, collectionType, collectionId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddRegulogs_ListRegulogs - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddRegulogs_ListRegulogs - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddRegulogs_ListRegulogs - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddRegulogs_ListRegulogs - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO regulogs (effector, regulation_type, regulator_family, regulator_name, regulog_id, taxon_name, pathway) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddRegulogs_ListRegulogs - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    Console.Log(addedRows + " added to the database");

                    // Next, add an entry to the table that keeps track of
                    // what collections a regulog is part of
                    Script.Literal("con.query('INSERT IGNORE INTO regulogs_regulog_collections (regulog_id, collection_id, collection_type) VALUES ?', [values2], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulogs_ListRegulogs - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // If the query was successful, note how many rows were added
                        // to the database
                        Number addedRows3 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                        Console.Log(addedRows3 + " added to the database");

                        // If there was no error in adding the regulogs to the database,
                        // store the RegPrecise url that originally supplied them for later
                        // reference

                        // The query string that will be used, containing the
                        // RegPrecise URL
                        string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListRegulogs + "')",
				            collectionType,
				            collectionId
                            );

                        Console.Log("Attempting to add RegPrecise query URL to the database");

                        Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulogs_ListRegulogs - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // If the query was successful, note how many rows were added
                        // to the database
                        Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                        if (addedRows2 == 1) {
                            Console.Log(queryString2 + " successful - 1 row added");
                        } else {
                            Console.Log(queryString2 + " successful - no rows added");
                        }

                        // Commit the transaction
                        Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                                Script.Literal("con.rollback(function () {");
                                    processResults( new ServiceResponse<Number>(null, errorMessage));
                                Script.Literal("con.end(function(err) { console.log('AddRegulogs_ListRegulogs - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                            Script.Literal("}\n");

                            // Send the response to the client
                            processResults(new ServiceResponse<Number>(addedRows, null));

                            Script.Literal("con.end(function(err) { console.log('AddRegulogs_ListRegulogs - Connection to local MySQL database closed.'); }); \n})");
                        Script.Literal("})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified regulog to the MySQL database
        /// <para>Will only add regulogs that do not already exist</para>
        /// <para>Should be used when the regulog is retrieved from RegPrecise using the GetRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulog"> The regulog to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulogs_GetRegulog (
			int regulogId,
            Regulog regulog,
            ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddRegulogs_GetRegulog - attempting to add regulog {0}", regulogId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the values to add
            /*string newEffector = regulog.Effector;
            string newRegulationType = Objects.ToString(RegulationTypes.Parse(regulog.RegulationType), String.Empty);
            string newRegulatorFamily = regulog.RegulatorFamily;
            string newRegulatorName = regulog.RegulatorName;
            string newRegulogId = Objects.ToString(regulog.RegulogId, String.Empty);
            string newTaxonName = regulog.TaxonName;
            string newPathway = regulog.Pathway;*/
            string[][] values = new string[1][];
            values[0] = new string[7];
            values[0][0] = regulog.Effector;
            values[0][1] = Objects.ToString(RegulationTypes.Parse(regulog.RegulationType), String.Empty);
            values[0][2] = regulog.RegulatorFamily;
            values[0][3] = regulog.RegulatorName;
            values[0][4] = Objects.ToString(regulog.RegulogId, String.Empty);
            values[0][5] = regulog.TaxonName;
            values[0][6] = regulog.Pathway;

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_GetRegulog, regulogId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddRegulogs_GetRegulog - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddRegulogs_GetRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddRegulogs_GetRegulog - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddRegulogs_GetRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                //Script.Literal("con.query('INSERT IGNORE INTO regulogs (effector, regulation_type, regulator_family, regulator_name, regulog_id, taxon_name, pathway) VALUES (\"' + newEffector + '\", \"' + newRegulationType + '\", \"' + newRegulatorFamily + '\", \"' + newRegulatorName + '\", \"' + newRegulogId + '\", \"' + newTaxonName + '\", \"' + newPathway + '\")', function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
            Script.Literal("con.query('INSERT IGNORE INTO regulogs (effector, regulation_type, regulator_family, regulator_name, regulog_id, taxon_name, pathway) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddRegulogs_GetRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulogs to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_GetRegulog + "')",
				        regulogId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddRegulogs_GetRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulogs_GetRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddRegulogs_GetRegulog - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified regulons to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulons are retrieved from RegPrecise using the ListRegulonsInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulons"> The regulons to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulons_ListRegulonsInRegulog (
			Number regulogId,
            Regulon[] regulons,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddRegulons_ListRegulonsInRegulog - attempting to add {0} regulons from regulog {1}", regulons.Length, regulogId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[regulons.Length][];
            for (int i = 0; i < regulons.Length; i++) {
                values[i] = new string[9];
                values[i][0] = Objects.ToString(regulons[i].RegulonId, String.Empty);
                values[i][1] = Objects.ToString(regulons[i].RegulogId, String.Empty);
                values[i][2] = Objects.ToString(regulons[i].GenomeId, String.Empty);
                values[i][3] = regulons[i].GenomeName;
                values[i][4] = regulons[i].RegulatorName;
                values[i][5] = regulons[i].RegulatorFamily;
                values[i][6] = Objects.ToString(RegulationTypes.Parse(regulons[i].RegulationType), String.Empty);
                values[i][7] = regulons[i].Effector;
                values[i][8] = regulons[i].Pathway;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListRegulonsInRegulog, regulogId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddRegulons_ListRegulonsInRegulog - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddRegulons_ListRegulonsInRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddRegulons_ListRegulonsInRegulog - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO regulons (regulon_id, regulog_id, genome_id, genome_name, regulator_name, regulator_family, regulation_type, effector, pathway) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListRegulonsInRegulog + "')",
				        regulogId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInRegulog - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified regulons to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulons are retrieved from RegPrecise using the ListRegulonsInRegulog request</para>
        /// </summary>
        /// <param name="genomeId"></param>
        /// <param name="regulons"> The regulons to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulons_ListRegulonsInGenome (
			Number genomeId,
            Regulon[] regulons,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddRegulons_ListRegulonsInGenome - attempting to add {0} regulons from genome {1}", regulons.Length, genomeId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[regulons.Length][];
            for (int i = 0; i < regulons.Length; i++) {
                values[i] = new string[9];
                values[i][0] = Objects.ToString(regulons[i].RegulonId, String.Empty);
                values[i][1] = Objects.ToString(regulons[i].RegulogId, String.Empty);
                values[i][2] = Objects.ToString(regulons[i].GenomeId, String.Empty);
                values[i][3] = regulons[i].GenomeName;
                values[i][4] = regulons[i].RegulatorName;
                values[i][5] = regulons[i].RegulatorFamily;
                values[i][6] = Objects.ToString(RegulationTypes.Parse(regulons[i].RegulationType), String.Empty);
                values[i][7] = regulons[i].Effector;
                values[i][8] = regulons[i].Pathway;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListRegulonsInGenome, genomeId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddRegulons_ListRegulonsInGenome - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddRegulons_ListRegulonsInGenome - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddRegulons_ListRegulonsInGenome - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInGenome - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO regulons (regulon_id, regulog_id, genome_id, genome_name, regulator_name, regulator_family, regulation_type, effector, pathway) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInGenome - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListRegulonsInGenome + "')",
				        genomeId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInGenome - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInGenome - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddRegulons_ListRegulonsInGenome - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified regulog collections to the MySQL database
        /// <para>Will only add regulog collections that do not already exist</para>
        /// <para>Should be used when the regulog collections are retrieved from RegPrecise using the ListRegulogCollections request</para>
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="regulogCollections"> The regulog collections to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulogCollections_ListRegulogCollections (
			RegulogCollectionType collectionType,
            RegulogCollection[] regulogCollections,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddRegulogCollections_ListRegulogCollections - attempting to add {0} regulog collections of type {1}", regulogCollections.Length, collectionType));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[regulogCollections.Length][];
            for (int i = 0; i < regulogCollections.Length; i++) {
                values[i] = new string[4];
                values[i][0] = regulogCollections[i].ClassName;
                values[i][1] = Objects.ToString(regulogCollections[i].CollectionId, String.Empty);
                values[i][2] = Objects.ToString(RegulogCollectionType.Parse(regulogCollections[i].CollectionType), String.Empty);
                values[i][3] = regulogCollections[i].Name;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListRegulogCollections, collectionType);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddRegulogCollections_ListRegulogCollections - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddRegulogCollections_ListRegulogCollections - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddRegulogCollections_ListRegulogCollections - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddRegulogCollections_ListRegulogCollections - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO regulog_collections (class_name, collection_id, collection_type, name) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddRegulogCollections_ListRegulogCollections - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListRegulogCollections + "')",
				        collectionType
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddRegulogCollections_ListRegulogCollections - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulogCollections_ListRegulogCollections - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddRegulogCollections_ListRegulogCollections - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified genomes to the MySQL database
        /// <para>Will only add genomes that do not already exist</para>
        /// <para>Should be used when the genomes are retrieved from RegPrecise using the ListGenomes request</para>
        /// </summary>
        /// <param name="genomes"> The genomes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddGenomes_ListGenomes (
            Genome[] genomes,
			ResponseProcessor<Number> processResults
        )  {
            Console.Log(String.Format("Running AddGenomes_ListGenomes - attempting to add {0} genomes", genomes.Length));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[genomes.Length][];
            for (int i = 0; i < genomes.Length; i++) {
                values[i] = new string[7];
                values[i][0] = Objects.ToString(genomes[i].GenomeId, String.Empty);
                values[i][1] = genomes[i].Name;
                values[i][2] = Objects.ToString(genomes[i].TaxonomyId, String.Empty);
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListGenomes);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddGenomes_ListGenomes - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddGenomes_ListGenomes - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddGenomes_ListGenomes - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddGenomes_ListGenomes - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO genomes (genome_id, name, taxonomy_id) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddGenomes_ListGenomes - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListGenomes + "')");

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddGenomes_ListGenomes - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddGenomes_ListGenomes - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddGenomes_ListGenomes - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified sites to the MySQL database
        /// <para>Will only add sites that do not already exist</para>
        /// <para>Should be used when the sites are retrieved from RegPrecise using the ListSitesInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="sites"> The sites to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddSites_ListSitesInRegulon (
			Number regulonId,
            Site[] sites,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddSites_ListSitesInRegulon - attempting to add {0} sites from regulon {1}", sites.Length, regulonId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[sites.Length][];
            for (int i = 0; i < sites.Length; i++) {
                values[i] = new string[6];
                values[i][0] = sites[i].GeneLocusTag;
                values[i][1] = Objects.ToString(sites[i].GeneVIMSSId, String.Empty);
                values[i][2] = Objects.ToString(sites[i].Position, String.Empty);
                values[i][3] = Objects.ToString(sites[i].RegulonId, String.Empty);
                values[i][4] = Objects.ToString(sites[i].Score, String.Empty);
                values[i][5] = sites[i].Sequence;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListSitesInRegulon, regulonId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddSites_ListSitesInRegulon - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddSites_ListSitesInRegulon - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddSites_ListSitesInRegulon - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO sites (gene_locus_tag, gene_vimss_id, position, regulon_id, score, sequence) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListSitesInRegulon + "')",
				        regulonId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulon - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified sites to the MySQL database
        /// <para>Will only add sites that do not already exist</para>
        /// <para>Should be used when the sites are retrieved from RegPrecise using the ListSitesInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="sites"> The sites to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddSites_ListSitesInRegulog (
			Number regulogId,
            Site[] sites,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddSites_ListSitesInRegulog - attempting to add {0} sites from regulog {1}", sites.Length, regulogId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[sites.Length][];
            for (int i = 0; i < sites.Length; i++) {
                values[i] = new string[6];
                values[i][0] = sites[i].GeneLocusTag;
                values[i][1] = Objects.ToString(sites[i].GeneVIMSSId, String.Empty);
                values[i][2] = Objects.ToString(sites[i].Position, String.Empty);
                values[i][3] = Objects.ToString(sites[i].RegulonId, String.Empty);
                values[i][4] = Objects.ToString(sites[i].Score, String.Empty);
                values[i][5] = sites[i].Sequence;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListSitesInRegulog, regulogId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddSites_ListSitesInRegulog - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddSites_ListSitesInRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddSites_ListSitesInRegulog - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO sites (gene_locus_tag, gene_vimss_id, position, regulon_id, score, sequence) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListSitesInRegulog + "')",
				        regulogId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddSites_ListSitesInRegulog - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified regulon to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulon is retrieved from RegPrecise using the GetRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="regulon"> The regulon to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulons_GetRegulon (
			int regulonId,
            Regulon regulon,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddRegulons_GetRegulon - attempting to add regulon {0}", regulonId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[1][];
            values[0] = new string[9];
            values[0][0] = Objects.ToString(regulon.RegulonId, String.Empty);
            values[0][1] = Objects.ToString(regulon.RegulogId, String.Empty);
            values[0][2] = Objects.ToString(regulon.GenomeId, String.Empty);
            values[0][3] = regulon.GenomeName;
            values[0][4] = regulon.RegulatorName;
            values[0][5] = regulon.RegulatorFamily;
            values[0][6] = Objects.ToString(RegulationTypes.Parse(regulon.RegulationType), String.Empty);
            values[0][7] = regulon.Effector;
            values[0][8] = regulon.Pathway;

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_GetRegulon, regulonId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddRegulons_GetRegulon - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddRegulons_GetRegulon - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddRegulons_GetRegulon - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddRegulons_GetRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO regulons (regulon_id, regulog_id, genome_id, genome_name, regulator_name, regulator_family, regulation_type, effector, pathway) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddRegulons_GetRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_GetRegulon + "')",
				        regulonId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddRegulons_GetRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulons_GetRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddRegulons_GetRegulon - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified regulators to the MySQL database
        /// <para>Will only add regulators that do not already exist</para>
        /// <para>Should be used when the regulators are retrieved from RegPrecise using the ListRegulatorsInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="regulators"> The regulators to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulators_ListRegulatorsInRegulon (
			int regulonId,
            Regulator[] regulators,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddRegulators_ListRegulatorsInRegulon - attempting to add {0} regulators from regulon {1}", regulators.Length, regulonId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[regulators.Length][];
            for (int i = 0; i < regulators.Length; i++) {
                values[i] = new string[5];
                values[i][0] = Objects.ToString(regulators[i].RegulonId, String.Empty);
                values[i][1] = regulators[i].Name;
                values[i][2] = regulators[i].LocusTag;
                values[i][3] = Objects.ToString(regulators[i].VimssId, String.Empty);
                values[i][4] = regulators[i].RegulatorFamily;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListRegulatorsInRegulon, regulonId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddRegulators_ListRegulatorsInRegulon - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddRegulators_ListRegulatorsInRegulon - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddRegulators_ListRegulatorsInRegulon - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO regulators (regulon_id, name, locus_tag, vimss_id, regulator_family) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListRegulatorsInRegulon + "')",
				        regulonId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulon - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified regulators to the MySQL database
        /// <para>Will only add regulators that do not already exist</para>
        /// <para>Should be used when the regulators are retrieved from RegPrecise using the ListRegulatorsInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulators"> The regulators to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulators_ListRegulatorsInRegulog (
			int regulogId,
            Regulator[] regulators,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddRegulators_ListRegulatorsInRegulog - attempting to add {0} regulators from regulog {1}", regulators.Length, regulogId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[regulators.Length][];
            for (int i = 0; i < regulators.Length; i++) {
                values[i] = new string[5];
                values[i][0] = Objects.ToString(regulators[i].RegulonId, String.Empty);
                values[i][1] = regulators[i].Name;
                values[i][2] = regulators[i].LocusTag;
                values[i][3] = Objects.ToString(regulators[i].VimssId, String.Empty);
                values[i][4] = regulators[i].RegulatorFamily;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListRegulatorsInRegulog, regulogId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddRegulators_ListRegulatorsInRegulog - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddRegulators_ListRegulatorsInRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddRegulators_ListRegulatorsInRegulog - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO regulators (regulon_id, name, locus_tag, vimss_id, regulator_family) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListRegulatorsInRegulog + "')",
				        regulogId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddRegulators_ListRegulatorsInRegulog - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified genes to the MySQL database
        /// <para>Will only add genes that do not already exist</para>
        /// <para>Should be used when the genes are retrieved from RegPrecise using the ListGenesInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="genes"> The genes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddGenes_ListGenesInRegulon (
			Number regulonId,
            Gene[] genes,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddGenes_ListGenesInRegulon - attempting to add {0} genes from regulon {1}", genes.Length, regulonId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[genes.Length][];
            for (int i = 0; i < genes.Length; i++) {
                values[i] = new string[5];
                values[i][0] = Objects.ToString(genes[i].RegulonId, String.Empty);
                values[i][1] = genes[i].Name;
                values[i][2] = genes[i].LocusTag;
                values[i][3] = Objects.ToString(genes[i].VimssId, String.Empty);
                values[i][4] = genes[i].GeneFunction;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListGenesInRegulon, regulonId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddGenes_ListGenesInRegulon - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddGenes_ListGenesInRegulon - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddGenes_ListGenesInRegulon - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO genes (regulon_id, name, locus_tag, vimss_id, gene_function) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListGenesInRegulon + "')",
				        regulonId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulon - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulon - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified genes to the MySQL database
        /// <para>Will only add genes that do not already exist</para>
        /// <para>Should be used when the genes are retrieved from RegPrecise using the ListGenesInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="genes"> The genes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddGenes_ListGenesInRegulog (
			Number regulogId,
            Gene[] genes,
			ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddGenes_ListGenesInRegulon - attempting to add {0} genes from regulog {1}", genes.Length, regulogId));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[genes.Length][];
            for (int i = 0; i < genes.Length; i++) {
                values[i] = new string[5];
                values[i][0] = Objects.ToString(genes[i].RegulonId, String.Empty);
                values[i][1] = genes[i].Name;
                values[i][2] = genes[i].LocusTag;
                values[i][3] = Objects.ToString(genes[i].VimssId, String.Empty);
                values[i][4] = genes[i].GeneFunction;
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListGenesInRegulog, regulogId);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddGenes_ListGenesInRegulon - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddGenes_ListGenesInRegulog - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddGenes_ListGenesInRegulog - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO genes (regulon_id, name, locus_tag, vimss_id, gene_function) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListGenesInRegulog + "')",
				        regulogId
                        );

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulog - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddGenes_ListGenesInRegulog - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        /// <summary>
        /// Adds the specified genome stats to the MySQL database
        /// <para>Will only add genome states that do not already exist</para>
        /// <para>Should be used when the genome stats are retrieved from RegPrecise using the ListGenomeStats request</para>
        /// </summary>
        /// <param name="genomeStats"> The genome stats to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddGenomeStats_ListGenomeStats (
            GenomeStats[] genomeStats,
            ResponseProcessor<Number> processResults
        ) {
            Console.Log(String.Format("Running AddGenomeStats_ListGenomeStats - attempting to add {0} genome stats", genomeStats.Length));

            // Stores the result of the query
            JSObject result = new JSObject();

            // Whether the query encountered an error
            string errorMessage = string.Empty;

            // Create the list of values to add
            string[][] values = new string[genomeStats.Length][];
            for (int i = 0; i < genomeStats.Length; i++) {
                values[i] = new string[7];
                values[i][0] = Objects.ToString(genomeStats[i].GenomeId, String.Empty);
                values[i][1] = genomeStats[i].Name;
                values[i][2] = Objects.ToString(genomeStats[i].TaxonomyId, String.Empty);
                values[i][3] = Objects.ToString(genomeStats[i].TfRegulonCount, String.Empty);
                values[i][4] = Objects.ToString(genomeStats[i].TfSiteCount, String.Empty);
                values[i][5] = Objects.ToString(genomeStats[i].RnaRegulonCount, String.Empty);
                values[i][6] = Objects.ToString(genomeStats[i].RnaSiteCount, String.Empty);
            }

            // The RegPrecise query URL that initially retrieved the data
            /*string currentRegPreciseURL = String.Format(regPreciseURL_ListGenomeStats);

            // If the query URL already exists in the list, return immediately
            // without trying to connect to the database
            if (addsCurrentlyProcessing.Contains(currentRegPreciseURL)) {
                errorMessage = "AddGenomeStats_ListGenomeStats - an operation is already running to add results of query " + currentRegPreciseURL + " - will not attempt to connect";
                processResults( new ServiceResponse<Number>(null, errorMessage));
                return;
            // Otherwise add the URL to the list
            } else {
                addsCurrentlyProcessing.Add(currentRegPreciseURL);
            }*/

            // Use the mysql node module to query the database. I do feel that
            // all these script literals are ugly, but I'm not sure how else
            // to do it yet
            Script.Literal("var mysql = require('mysql'); con = mysql.createConnection({ host: 'localhost', user: 'GOaccess', password: 'GeneOntology1', database: 'trndiff'})");
            Script.Literal("con.connect(function(err){\n if(err){\n errorMessage = 'AddGenomeStats_ListGenomeStats - Unable to connect to local MySQL database: ' + err.message; \n console.log(errorMessage)");
                processResults( new ServiceResponse<Number>(null, errorMessage));
            Script.Literal("return; }\n console.log('AddGenomeStats_ListGenomeStats - Connection to local MySQL database established'); });");
            Script.Literal("con.beginTransaction(function (err) {");
                Script.Literal("if (err) {\n errorMessage = 'Unable to begin transaction: ' + err.message; \n console.log(errorMessage)");
                    processResults( new ServiceResponse<Number>(null, errorMessage));
                Script.Literal("con.end(function(err) { console.log('AddGenomeStats_ListGenomeStats - Connection to local MySQL database closed due to an error.'); }); \n return; }");
                Script.Literal("con.query('INSERT IGNORE INTO genome_stats (genome_id, name, taxonomy_id, tf_regulon_count, tf_site_count, rna_regulon_count, rna_site_count) VALUES ?', [values], function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                    Script.Literal("con.rollback(function () {");
                        processResults( new ServiceResponse<Number>(null, errorMessage));
                    Script.Literal("con.end(function(err) { console.log('AddGenomeStats_ListGenomeStats - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                Script.Literal("}\n");

                // If the query was successful, note how many rows were added
                // to the database
                Number addedRows = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                Console.Log(addedRows + " added to the database");

                    // If there was no error in adding the regulons to the database,
                    // store the RegPrecise url that originally supplied them for later
                    // reference

                    // The query string that will be used, containing the
                    // RegPrecise URL
                    string queryString2 = String.Format("INSERT IGNORE INTO previous_requests (url) VALUES ('" + regPreciseURL_ListGenomeStats + "')");

                    Console.Log("Attempting to add RegPrecise query URL to the database");

                    Script.Literal("con.query(queryString2, function(err, result) {\n if(err) {\n errorMessage = 'Unable to insert rows into the database: ' + err.message; \n console.log(errorMessage)");
                        Script.Literal("con.rollback(function () {");
                            processResults( new ServiceResponse<Number>(null, errorMessage));
                        Script.Literal("con.end(function(err) { console.log('AddGenomeStats_ListGenomeStats - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                    Script.Literal("}\n");

                    // If the query was successful, note how many rows were added
                    // to the database
                    Number addedRows2 = Number.Parse(Objects.ToString(result["affectedRows"], "0"));
                    if (addedRows2 == 1) {
                        Console.Log(queryString2 + " successful - 1 row added");
                    } else {
                        Console.Log(queryString2 + " successful - no rows added");
                    }

                    // Commit the transaction
                    Script.Literal("con.commit (function (err) {\n if (err) {\n errorMessage = 'Unable to commit transaction: ' + err.message; \n console.log(errorMessage)");
                            Script.Literal("con.rollback(function () {");
                                processResults( new ServiceResponse<Number>(null, errorMessage));
                            Script.Literal("con.end(function(err) { console.log('AddGenomeStats_ListGenomeStats - Connection to local MySQL database closed due to an error.'); }); \n return; })");
                        Script.Literal("}\n");

                        // Send the response to the client
                        processResults(new ServiceResponse<Number>(addedRows, null));

                        Script.Literal("con.end(function(err) { console.log('AddGenomeStats_ListGenomeStats - Connection to local MySQL database closed.'); }); \n})");
                    Script.Literal("})");
                Script.Literal("})");
            Script.Literal("})");

            Console.Log("Exiting function");
        }

        //// <summary>
        //// Stores a list of the currently processing add operations, to prevent the same one being done multiple times at once
        //// <para>The identifier is the RegPrecise query URL</para>
        //// </summary>
        //private List<string> addsCurrentlyProcessing = new List<string>();
    }
}
