﻿using System;
using System.Collections.Generic;

namespace SystemQut {
	public class Rand {
		object rng;

		extern public Rand ();

		public Rand ( string seed ) {
			Arguments _arguments = Arguments.Current;

			if ( _arguments.Length == 0 ) {
				rng = Script.Literal("seedrandom()");
			}
			else {
				rng = Script.Literal( "seedrandom(seed+String.fromCharCode(0))" );
			}
		}

		/// <summary> Gets the next floating point value in the half-open interval [0,1)
		/// </summary>
		/// <returns></returns>
		public double NextDouble () {
			return (double) Script.Literal("this._rng()");
		}

		/// <summary> Gets the next unsigned integer in the subrange 0..max-1 
		/// </summary>
		/// <returns></returns>
		public uint NextUint ( uint max ) {
			return (uint) Math.Truncate( NextDouble() * max );
		}

		/// <summary>  Gets the next signed integer in the subrange 0..max-1 
		/// </summary>
		/// <returns></returns>
		public int NextInt ( int max ) {
			return (int) Math.Truncate( NextDouble() * max );
		}

	}
}
