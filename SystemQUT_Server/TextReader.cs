﻿using System;
using System.Collections.Generic;

namespace SystemQut.IO {
	/// <summary> A class that provides similar functionality to the CLR TextReader.
	/// <para>
	///		Differences are introduced due to different handling for char data type in JS.
	/// </para>
	/// </summary>
	public abstract class TextReader {
		/// <summary> Character returned when at EOF.
		/// </summary>
		public const char NUL = '\u0000';

		/// <summary>
		/// 
		/// </summary>
		public const char CR = '\r';

		/// <summary>
		/// 
		/// </summary>
		public const char LF = '\n';

		/// <summary> Return true iff the underlying character sequence has been exhausted.
		/// </summary>
		public abstract bool EndOfStream { get; }

		/// <summary> Closes the System.IO.TextReader and releases any system resources associated
		///		with the TextReader.
		/// </summary>

		public abstract void Close ();

		/// <summary> Reads the next character without changing the state of the reader or the
		///    character source. Returns the next available character without actually reading
		///    it from the input stream.
		/// </summary>
		/// <returns>
		///    The next character to be read, or NUL if no more characters
		///    are available or the stream does not support seeking.
		/// </returns>

		public abstract char Peek ();

		/// <summary> Reads the next character from the input stream and advances the character
		/// 		  position by one character.
		/// </summary>
		/// <returns> The next character from the input stream, or NUL if no more characters are
		/// 		  available.
		/// </returns>

		public abstract char ReadChar ();

		/// <summary> Reads the next character from the input stream and advances the character
		/// 		  position by one character.
		/// </summary>
		/// <returns> The next character from the input stream, or NUL if no more characters are
		/// 		  available.
		/// </returns>

		public int ReadCharCode () {
			return EndOfStream ? -1 : ((string) ReadChar()).CharCodeAt(0);
		}

		/// <summary>  Reads a maximum of count characters from the current stream and writes the
		///  data to buffer, beginning at index.
		/// </summary>
		/// <param name="buffer">
		/// 	When this method returns, contains the specified character array with the
		/// 	values between index and (index + count - 1) replaced by the characters read
		/// 	from the current source.
		/// </param>
		/// <param name="index">
		///		The position in buffer at which to begin writing.
		/// </param>
		/// <param name="count">
		/// 	The maximum number of characters to read. If the end of the stream is reached
		/// 	before count of characters is read into buffer, the current method returns.
		/// </param>
		/// <returns> 
		///		The number of characters that have been read. The number will be less than
		/// 	or equal to count, depending on whether the data is available within the
		/// 	stream. This method returns zero if called when no more characters are left
		/// 	to read.
		/// </returns>
		
		public abstract int Read ( char[] buffer, int index, int count );

		//
		// Summary:
		//     Reads a maximum of count characters from the current stream, and writes the
		//     data to buffer, beginning at index.
		//
		// Parameters:
		//   buffer:
		//     When this method returns, this parameter contains the specified character
		//     array with the values between index and (index + count -1) replaced by the
		//     characters read from the current source.
		//
		//   index:
		//     The position in buffer at which to begin writing.
		//
		//   count:
		//     The maximum number of characters to read.
		//
		// Returns:
		//     The position of the underlying stream is advanced by the number of characters
		//     that were read into buffer.The number of characters that have been read.
		//     The number will be less than or equal to count, depending on whether all
		//     input characters have been read.
		//
		// Exceptions:
		//   System.ArgumentNullException:
		//     buffer is null.
		//
		//   System.ArgumentException:
		//     The buffer length minus index is less than count.
		//
		//   System.ArgumentOutOfRangeException:
		//     index or count is negative.
		//
		//   System.ObjectDisposedException:
		//     The System.IO.TextReader is closed.
		//
		//   System.IO.IOException:
		//     An I/O error occurs.
		public abstract int ReadBlock ( char[] buffer, int index, int count );

		//
		// Summary:
		//     Reads a line of characters from the current stream and returns the data as
		//     a string.
		//
		// Returns:
		//     The next line from the input stream, or null if all characters have been
		//     read.
		//
		// Exceptions:
		//   System.IO.IOException:
		//     An I/O error occurs.
		//
		//   System.OutOfMemoryException:
		//     There is insufficient memory to allocate a buffer for the returned string.
		//
		//   System.ObjectDisposedException:
		//     The System.IO.TextReader is closed.
		//
		//   System.ArgumentOutOfRangeException:
		//     The number of characters in the next line is larger than System.Int32.MaxValue

		public abstract string ReadLine ();
		
		//
		// Summary:
		//     Reads all characters from the current position to the end of the TextReader
		//     and returns them as one string.
		//
		// Returns:
		//     A string containing all characters from the current position to the end of
		//     the TextReader.
		//
		// Exceptions:
		//   System.IO.IOException:
		//     An I/O error occurs.
		//
		//   System.ObjectDisposedException:
		//     The System.IO.TextReader is closed.
		//
		//   System.OutOfMemoryException:
		//     There is insufficient memory to allocate a buffer for the returned string.
		//
		//   System.ArgumentOutOfRangeException:
		//     The number of characters in the next line is larger than System.Int32.MaxValue

		public abstract string ReadToEnd ();

		/// <summary> Consumes and returns all characters up to but not including the first 
		///		occurrence of the nominated query substring.
		/// <para>
		///		If no further occurrence of the pattern exists, has the same effect as ReadToEnd.
		/// </para>
		/// <para>
		///		Beware: this method is not available in the CLR.
		/// </para>
		/// </summary>
		/// <param name="querySubString">
		///		A query substring which comes after the returned content.
		/// </param>
		/// <returns></returns>

		public abstract string ReadToFirst ( string querySubString );

		/// <summary> Consumes and returns all characters up to but not including the first 
		///		occurrence of the nominated query substring.
		/// <para>
		///		If no further occurrence of the pattern exists, has the same effect as ReadToEnd.
		/// </para>
		/// <para>
		///		Beware: this method is not available in the CLR.
		/// </para>
		/// </summary>
		/// <param name="terminator">
		///		A query substring which comes after the returned content.
		/// </param>
		/// <returns></returns>

		public abstract string ReadToFirstChar ( char terminator );
	}
}
