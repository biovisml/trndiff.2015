﻿using System;
using System.Collections.Generic;

namespace SystemQut {
	
	/// <summary> Work-around to restore broken functionality from Enum class.
	/// <para>
	///		Unfortunately ScriptSharps optimised take on Enums has eliminated toString and parse.
	///		This class is a rather clumsy workaround, but it will suffice for the present purpose.
	/// </para>
	/// </summary>

	public static class Enum {
		public static object Parse( Type type, string text ) {
			Dictionary<string,int> members = (Dictionary<string,int>) (object) type;
			
			foreach ( string key in members.Keys ) {
				if ( String.Compare( key, text, true ) == 0 ) return members[key];
			}

			return Script.Undefined;
		}

		public static string ToString( Type type, object obj ) {
			Dictionary<string,int> members = (Dictionary<string,int>) (object) type;
			
			foreach ( KeyValuePair<string,int> kvp in members ) {
				if ( kvp.Value == (int) obj ) return kvp.Key;
			}

			return (string) Script.Undefined;
		}
	}
}
