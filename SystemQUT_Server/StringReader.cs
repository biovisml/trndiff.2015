﻿using System;
using System.Collections.Generic;


namespace SystemQut.IO {

	/// <summary> JS implementation of StringReader.
	/// </summary>

	public class StringReader : TextReader {
		private string source;
		private int idx;

		public StringReader ( string source ) {
			if ( !Script.IsValue( source ) ) throw new Exception( "Source must be defined and not null." );
			this.source = source;
			this.idx = 0;
		}

		/// <summary> Closes the System.IO.TextReader and releases any system resources associated
		///		with the TextReader.
		/// </summary>

		public override void Close () {
			// Noop.
		}

		/// <summary> returns true iff we have gone past the end of the string.
		/// </summary>
		/// <returns></returns>

		public override bool EndOfStream {
			get {
				return idx >= source.Length;
			}
		}

		/// <summary>
		///    Reads the next character without changing the state of the reader or the
		///    character source. Returns the next available character without actually reading
		///    it from the input stream.
		/// </summary>
		/// <returns>
		///		An integer representing the next character to be read, or -1 if no more characters
		/// 	are available or the stream does not support seeking.
		/// </returns>

		public override char Peek () {
			return EndOfStream ? NUL : source[idx];
		}

		/// <summary>
		///		Reads the next character from the input stream and advances the character
		///		position by one character.
		/// </summary>
		/// <returns>
		///		The next character from the input stream, or NUL if no more characters are
		///		available. The default implementation returns -1.
		/// </returns>

		public override char ReadChar () {
			return EndOfStream ? NUL : source[idx++];
		}

		/// <summary> 
		///		Reads a maximum of count characters from the current stream and writes the
		///		data to buffer, beginning at index.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="index"></param>
		/// <param name="count"></param>
		/// <returns>
		///		The number of characters that have been read. The number will be less than
		///		or equal to count, depending on whether the data is available within the
		///		stream. This method returns zero if called when no more characters are left
		///		to read.
		/// </returns>

		public override int Read ( char[] buffer, int index, int count ) {
			int charactersRead = 0;

			while ( charactersRead < count && !EndOfStream ) {
				buffer[index + charactersRead] = ReadChar();
				charactersRead++;
			}

			return charactersRead;
		}

		/// <summary>
		///		Reads a maximum of count characters from the current stream, and writes the
		///		data to buffer, beginning at index.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="index"></param>
		/// <param name="count"></param>
		/// <returns>
		///		The position of the underlying stream is advanced by the number of characters
		///		that were read into buffer.The number of characters that have been read.
		///		The number will be less than or equal to count, depending on whether all
		///		input characters have been read.
		/// </returns>

		public override int ReadBlock ( char[] buffer, int index, int count ) {
			return Read( buffer, index, count );
		}

		/// <summary>
		///		Reads a line of characters from the current stream and returns the data as
		///		a string.
		/// </summary>
		/// <returns>
		///		The next line from the input stream, or null if all characters have been
		///		read.
		/// </returns>

		public override string ReadLine () {
			if ( EndOfStream ) return null;

			StringBuilder result = new StringBuilder();

			while ( !EndOfStream && source[idx] != CR && source[idx] != LF ) {
				result.Append( ReadChar() );
			}

			if ( !EndOfStream && source[idx] == CR ) idx++;
			if ( !EndOfStream && source[idx] == LF ) idx++;

			return result.ToString();
		}

		/// <summary>
		///		Reads all characters from the current position to the end of the TextReader
		///		and returns them as one string.
		/// </summary>
		/// <returns>
		///		A string containing all characters from the current position to the end of
		///		the TextReader.
		/// </returns>

		public override string ReadToEnd () {
			string result = EndOfStream ? string.Empty : source.Substring( idx );
			idx = source.Length;
			return result;
		}

		/// <summary> Consumes and returns all characters up to but not including the first 
		///		occurrence of the nominated query substring.
		/// <para>
		///		If no further occurrence of the pattern exists, has the same effect as ReadToEnd.
		/// </para>
		/// </summary>
		/// <param name="querySubString">
		///		A query substring which comes after the returned content.
		/// </param>
		/// <returns></returns>

		public override string ReadToFirst ( string querySubString ) {
			int loc = source.IndexOf( querySubString, idx );
			string result = loc < 0 ? source.Substring( idx ) : source.Substring( idx, loc );
			idx += result.Length;
			return result;
		}

		/// <summary> Consumes and returns all characters up to but not including the first 
		///		occurrence of the nominated query substring.
		/// <para>
		///		If no further occurrence of the pattern exists, has the same effect as ReadToEnd.
		/// </para>
		/// </summary>
		/// <param name="terminator">
		///		A query substring which comes after the returned content.
		/// </param>
		/// <returns></returns>

		public override string ReadToFirstChar ( char terminator ) {
			int loc = source.IndexOf( terminator, idx );
			string result = loc < 0 ? source.Substring( idx ) : source.Substring( idx, loc );
			idx += result.Length;
			return result;
		}
	}
}
