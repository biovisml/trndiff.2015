﻿using System;
using System.Collections.Generic;

namespace SystemQut.IO {
	internal class NullTextWriter: TextWriter {
		public override void WriteString ( string value ) {
			// noop
		}
	}
}
