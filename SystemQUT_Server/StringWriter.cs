﻿using System;
using System.Collections.Generic;


namespace SystemQut.IO {
	public class StringWriter : TextWriter {
		private StringBuilder stringBuilder = new StringBuilder();

		public override void WriteString ( string value ) {
			stringBuilder.Append( value );
		}

		public override string ToString () {
			return stringBuilder.ToString();
		}
	}
}
