using System;
using System.Collections.Generic;

namespace SystemQut {

	public static class Assert {

#if CLIENT
		public static event Action<Exception> Process;
#endif

		/// <summary> Asserts two numbers are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>
		static public void NumbersEqual ( Number expected, Number actual, string location ) {
			if ( expected != actual ) {
				Exception lastError = new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
					actual,
					expected,
					location
				) );
#if CLIENT
				if ( Process != null ) Process( lastError ); else throw lastError;
#else
throw lastError;
#endif
			}
		}

		/// <summary> Asserts two strings are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>
		static public void StringsEqual ( String expected, String actual, string location ) {
			//Console.Log( "Expected = " + expected );
			//Console.Log( "Actual = " + actual );

			if ( String.Compare( expected, actual ) != 0 ) {
				Exception lastError = new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
					actual,
					expected,
					location
				) );
#if CLIENT
				if ( Process != null ) Process( lastError ); else throw lastError;
#else
throw lastError;
#endif
			}
		}

		/// <summary> Asserts two comparable values are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>
		static public void ComparablesEqual ( IComparable expected, IComparable actual, string location ) {
			if ( expected.CompareTo( actual ) != 0 ) {
				Exception lastError = new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
					actual,
					expected,
					location
				) );
#if CLIENT
				if ( Process != null ) Process( lastError ); else throw lastError;
#else
throw lastError;
#endif
			}
		}

		/// <summary> Asserts two numbers are equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="location"></param>
		static public void CharsEqual ( char expected, char actual, string location ) {
			if ( expected != actual ) {
				Exception lastError = new Exception( String.Format( "Assertion failed: {2}: Actual value <{0}> is not equal to expected value <{1}>",
					actual,
					expected,
					location
				) );
#if CLIENT
				if ( Process != null ) Process( lastError ); else throw lastError;
#else
throw lastError;
#endif
			}
		}

		/// <summary> Asserts two numbers are approximately equal.
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="epsilon"></param>
		/// <param name="location"></param>
		static public void NumbersApproxEqual ( Number expected, Number actual, Number epsilon, string location ) {
			if ( Math.Abs( expected - actual ) > epsilon ) {
				Exception lastError = new Exception( String.Format( "Assertion failed: {0}: Difference between actual value <{0}> and expected value <{1}> exceeds <{2}>",
					actual,
					expected,
					epsilon
				) );
#if CLIENT
				if ( Process != null ) Process( lastError ); else throw lastError;
#else
throw lastError;
#endif
			}
		}

		/// <summary> Asserts that the supplied argument is true.
		/// </summary>
		/// <param name="condition"></param>
		/// <param name="location"></param>
		static public void IsTrue ( bool condition, string location ) {
			if ( !condition ) {
				Exception lastError = new Exception( String.Format( "Assertion failed: {0}: Condition is not true as expected.", location ) );
#if CLIENT
				if ( Process != null ) Process( lastError ); else throw lastError;
#else
throw lastError;
#endif
			}
		}

		/// <summary> Asserts that the supplied argument is false.
		/// </summary>
		/// <param name="condition"></param>
		/// <param name="location"></param>
		static public void IsFalse ( bool condition, string location ) {
			if ( condition ) {
				Exception lastError = new Exception( String.Format( "Assertion failed: {0}: Condition is not false as expected.", location ) );
#if CLIENT
				if ( Process != null ) Process( lastError ); else throw lastError;
#else
throw lastError;
#endif
			}
		}

		public static void Fail () {
			Exception lastError = new Exception( "Assertion failed." );
#if CLIENT
			if ( Process != null ) Process( lastError ); else throw lastError;
#else
			throw lastError;
#endif
		}
	}
}
