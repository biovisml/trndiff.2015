﻿using System;
using System.Collections.Generic;

namespace SystemQut.Xml {

	/// <summary> The lexical tokens for XmlReader.
	/// </summary>

	enum XmlTokenType {
		/// <summary> An error </summary>
		Error = 0,

		/// <summary> EOF encountered </summary>
		Eof = 1,

		/// <summary> Sequence of spaces. </summary>
		Space = 2,

		/// <summary> single &lt; character. </summary>
		OpenTagStart = 3,

		/// <summary> Single &gt; character. </summary>
		TagEnd = 4,

		/// <summary> Xml identifier </summary>
		Name = 5,

		/// <summary> &lt;! </summary>
		ProcessingInstructionStart = 6,

		/// <summary> &lt;? </summary>
		XmlDeclStart = 7,

		/// <summary> ?&gt; </summary>
		XmlDeclEnd = 8,

		/// <summary> &amp;NAME; </summary>
		EntityRef = 9,

		/// <summary> % NAME ; </summary>
		PEReference = 10,

		/// <summary> &amp;#9999; or &amp;#x9999; </summary>
		CharRef = 11,

		/// <summary> Only generated inside a tag. Either double or single quote delimited. Dquotes permit embedded  SQuotes and vice versa. String literals should only appear </summary>
		StringLiteral = 12,

		/// <summary> /&gt; -- the end of a self-closing tag. </summary>
		SelfClosingTagEnd = 13,

		/// <summary> &lt;/ -- the start of a closing tag. </summary>
		ClosingTagStart = 14,

		/// <summary> The = sign, detected only within tag. </summary>
		Equals = 15,

		/// <summary> anything else. </summary>
		CharData = 16,
	}

}
