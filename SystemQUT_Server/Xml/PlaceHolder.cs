﻿using System;
using System.Collections.Generic;

namespace SystemQut.Xml {

	/// <summary> A placeholder is a document structuring element which is replaced by 
	///		the textual representation of its content.
	/// </summary>
	
	public class PlaceHolder: Element {

		public PlaceHolder (
			string id,
			string [] attributeValuePairs,
			Node [] children
		) : base( "placeholder", id, attributeValuePairs, children ) { }

		public override string ToString () {
			StringBuilder sb = new StringBuilder();

			for( int i = 0; i < children.Count; i++ ) {
				sb.Append( children[i].ToString() );
			}

			return sb.ToString();
		}		
	}
}
