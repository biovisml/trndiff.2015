﻿using System;
using System.Collections.Generic;
using SystemQut.IO;

namespace SystemQut.Xml {

	/// <summary> Simplified lexical scanner for Xml documents.
	/// <para>
	///		Beware: it does not handle CDATA and comments properly.
	/// </para>
	/// </summary>

	class XmlLexer {
		private TextReader input;
		private XmlTokenType token;
		private char currentChar;
		private List<object> tokenValue = new List<object>();
		private bool processingTag;
		const string SPACES = " \n\r\f\t";

		public XmlLexer ( TextReader input ) {
			this.input = input;
			currentChar = input.ReadChar();
			Advance();
		}

		public XmlTokenType Token {
			get { return token; }
		}

		public string TokenValue {
			get { return input.EndOfStream ? string.Empty : tokenValue.Join(""); }
		}

		/// <summary> Consumes characters as determined by the current charCode 
		///		and state until a complete token has been absorbed.
		///	<para>
		///		Precondition: The
		/// </para>
		/// </summary>

		internal XmlTokenType Advance () {
			tokenValue.Clear();

			XmlTokenType token;

			if ( currentChar == TextReader.NUL ) {
				token = XmlTokenType.Eof;
			}
			else if ( processingTag ) {
				if ( currentChar == '?' ) {
					AppendAndGet();
					if ( currentChar == '>' ) {
						// process ?>
						AppendAndGet();
						token = XmlTokenType.XmlDeclEnd;
						processingTag = false;
					}
					else {
						// just a (possibly misplaced) ?
						token = XmlTokenType.CharData;
					}
				}
				else if ( currentChar == '/' ) {
					AppendAndGet();

					if ( currentChar == '>' ) {
						// process />
						AppendAndGet();
						token = XmlTokenType.SelfClosingTagEnd;
						processingTag = false;
					}
					else {
						// just a (possibly misplaced) /
						token = XmlTokenType.CharData;
					}
				}
				else if ( currentChar == '>' ) {
					AppendAndGet(); 
					token = XmlTokenType.TagEnd;
					processingTag = false;
				}
				else if ( currentChar == '=' ) {
					AppendAndGet(); 
					token = XmlTokenType.Equals;
				}
				else if ( currentChar == '"' || currentChar == '\'' ) {
					char terminator = currentChar;
					tokenValue.Add( terminator );
					tokenValue.Add( this.input.ReadToFirstChar( terminator ) );
					tokenValue.Add( terminator );
					currentChar = input.ReadChar(); // read the terminator
					currentChar = input.ReadChar(); // and move on ready for the next call.
					token = XmlTokenType.StringLiteral;
				}
				else if ( SPACES.IndexOf( currentChar ) >= 0 ) {
					AppendAndGet();

					while ( SPACES.IndexOf( currentChar ) >= 0 ) {
						AppendAndGet();
					}

					token = XmlTokenType.Space;
				}
				else {
					// TODO_LATER: this also incorrectly classifies comment markers and cdata markers as names. 
					// TODO_LATER: it also incorrectly recognises <> in comments or CDATA as markup.
					AppendAndGet();

					while( "?!\"'>= \n\f\t\r".IndexOf( currentChar ) < 0 ) {
						AppendAndGet();
					}

					token = XmlTokenType.Name;
				}
			}
			else if ( currentChar == '<' ) {
				processingTag = true;
				AppendAndGet();
				if ( currentChar == '!' ) {
					AppendAndGet();
					token = XmlTokenType.ProcessingInstructionStart;
				}
				else if ( currentChar == '?' ) {
					AppendAndGet();
					token = XmlTokenType.XmlDeclStart;
				}
				else if ( currentChar == '/' ) {
					AppendAndGet();
					token = XmlTokenType.ClosingTagStart;
				}
				else {
					token = XmlTokenType.OpenTagStart;
				}
			}
			else {
				tokenValue.Add( currentChar );
				tokenValue.Add( this.input.ReadToFirst( "<" ) );
				currentChar = input.ReadChar();
				token = XmlTokenType.CharData;
			}

			this.token = token;

			return token;
		}

		private void AppendAndGet () {
			tokenValue.Add( currentChar );
			currentChar = input.ReadChar();
		}
	}
}
