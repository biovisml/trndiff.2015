﻿using System;
using System.Collections.Generic;


namespace SystemQut.Xml {
	
	public class TextNode: Node {
		private  string text;

		public TextNode ( string id, string text) : base( id ) {
			Text = text;
		}

		public string Text {
			get { return text; }
			set { this.text = value ?? String.Empty; }
		}

		public override string ToString () {
			return Element.Encode( this.text );
		}
	}
}
