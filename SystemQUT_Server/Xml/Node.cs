﻿using System;
using System.Collections.Generic;

namespace SystemQut.Xml {
	public abstract class Node {
		
		protected string id;

		public Node ( string id ) {
			Id = id;
		}

		public String Id {
			get {
				return this.id;
			}
			private set {
				this.id = value ?? string.Empty;
			}
		}

		/// <summary> Force implementatin of ToString to convert the node to text.
		/// </summary>
		/// <returns></returns>

		public abstract override string ToString ();
	}
}
