﻿using System;
using System.Collections.Generic;
using SystemQut.IO;

namespace SystemQut.Xml {

	/// <summary> A minimal Xml Reader which provides <em>just enough</em> functionality to support the NCBI BLAST parser. 
	/// <para>
	///		Although it follows the spirit of the CLR XmlReader, it is certainly not guaranteed to provide 
	///		the full repertoire of element types.
	/// </para>
	/// <para>
	///		Beware: it does not handle CDATA and comments properly.
	/// </para>
	/// </summary>

	public class XmlReader {
		private XmlNodeType nodeType;
		private string value = string.Empty;
		private string name;
		private XmlLexer lexer;
		private Dictionary<string, string> attributes = new Dictionary<string, string>();
		private StringBuilder content = new StringBuilder();
		private  bool isEmptyElement;

		/// <summary>
		///		Creates a new System.Xml.XmlReader instance with the specified System.IO.TextReader.
		/// </summary>
		/// <param name="input">
		/// 	 The System.IO.TextReader from which to read the XML data. Because a System.IO.TextReader
		/// 	 returns a stream of Unicode characters, the encoding specified in the XML
		/// 	 declaration is not used by the System.Xml.XmlReader to decode the data stream.
		/// </param>
		/// <returns>
		///		An System.Xml.XmlReader object to read the XML data.
		/// </returns>
		/// <exception cref="Exception">
		///		If input is null.
		/// </exception>

		public static XmlReader Create ( TextReader input ) {
			return new XmlReader( input );
		}

		/// <summary> Initialises an Xml reader ready to read the supplied document.
		/// </summary>
		/// <param name="input">
		///		A well-defined TextReader which provides access to the document content.
		/// </param>

		private XmlReader ( TextReader input ) {
			if ( Script.IsNullOrUndefined( input ) )
				throw new Exception( "input is null." );

			lexer = new XmlLexer( input );
		}

		/// <summary> Reads the next node from the stream.
		/// </summary>
		/// <returns></returns>

		public bool Read () {
			XmlTokenType token = lexer.Token;

			if ( token == XmlTokenType.Eof ) return false;

			ResetCurrentElement();

			if ( token == XmlTokenType.OpenTagStart ) {
				ReadOpenTag();
			}
			else if ( token == XmlTokenType.ClosingTagStart ) {
				ReadClosingTag();
			}
			else if ( token == XmlTokenType.XmlDeclStart ) {
				ReadXmlDeclTag();
			}
			else if ( token == XmlTokenType.ProcessingInstructionStart ) {
				ReadProcessingInstruction();
			}
			else {
				ReadText();
			}

			return true;
		}

		/// <summary> Reads a standard Xml tag and classifies it as empty or non-empty.
		/// </summary>

		private void ReadOpenTag () {
			XmlTokenType token = lexer.Advance();

			while ( token != XmlTokenType.Eof && token != XmlTokenType.SelfClosingTagEnd && token != XmlTokenType.TagEnd ) {
				if ( token == XmlTokenType.Name ) {
					if ( name == null ) {
						name = lexer.TokenValue;
					}
					else {
						ReadAttribute();
					}
				}

				token = lexer.Advance();
			}

			token = lexer.Advance();
			nodeType = XmlNodeType.Element;
			isEmptyElement = token == XmlTokenType.SelfClosingTagEnd;
		}

		/// <summary> Reads a closing tag. It is up to the caller to decide if the tag matches.
		/// </summary>

		private void ReadClosingTag () {
			XmlTokenType token = lexer.Advance();

			while ( token != XmlTokenType.Eof && token != XmlTokenType.TagEnd ) {
				if ( token == XmlTokenType.Name ) {
					if ( name == null ) {
						name = lexer.TokenValue;
					}
				}

				token = lexer.Advance();
			}

			token = lexer.Advance();
			nodeType = XmlNodeType.EndElement;
		}

		/// <summary> Reads an xml declaration.
		/// </summary>

		private void ReadXmlDeclTag () {
			XmlTokenType token = lexer.Advance();

			while ( token != XmlTokenType.Eof && token != XmlTokenType.XmlDeclEnd ) {
				if ( token == XmlTokenType.Name ) {
					if ( name == null ) {
						name = lexer.TokenValue;
					}
					else {
						ReadAttribute();
					}
				}

				token = lexer.Advance();
			}

			token = lexer.Advance();
			nodeType = XmlNodeType.XmlDeclaration;
		}

		/// <summary> Reads a single attribute.
		/// <para>
		///		Precondition: That the current token is a name.
		/// </para>
		/// <para>
		///		Postcondition: The lexer has matched '=' followed by a string,
		///		and the resulting name-value pair has been added to the list of attributes
		///		OR an '=' has been consumed if present but string was found and no change has been made to the list of attributes. 
		/// </para>
		/// </summary>

		private void ReadAttribute () {
			string attributeName = lexer.TokenValue;
			XmlTokenType token = lexer.Advance();

			if ( token == XmlTokenType.Equals ) {
				token = lexer.Advance();

				if ( token == XmlTokenType.StringLiteral ) {
					string value = lexer.TokenValue;
					attributes[attributeName] = value.Substring( 1, value.Length - 1 );
					// TODO: decode entities.
				}
			}
		}

		/// <summary> Reads a tag instruction of the form "&lt;!" NAME CharData "&gt;" and
		/// then categorises it as one of DocumentType, Entity or ProcessingInstruction.
		/// <para>
		///		Pre: current token in lexer is "&lt;!"
		///		Post: the name and all character data up to the next "&gt;" have been consumed. 
		///		The lexical scanner is positioned at the token following the "&gt;".
		/// </para>
		/// </summary>

		private void ReadProcessingInstruction () {
			XmlTokenType token = lexer.Advance();

			while ( token != XmlTokenType.Eof && token != XmlTokenType.TagEnd ) {
				if ( lexer.Token == XmlTokenType.Name && name == null ) {
					name = lexer.TokenValue;
				}
				else {
					content.Append( lexer.TokenValue );
				}

				token = lexer.Advance();
			}

			token = lexer.Advance();

			nodeType = name == "DOCTYPE" ? XmlNodeType.DocumentType :
				name == "ENTITY" ? XmlNodeType.Entity :
					XmlNodeType.ProcessingInstruction;
			value = content.ToString();
		}

		/// <summary> resets the current element information.
		/// </summary>

		private void ResetCurrentElement () {
			name = null;
			attributes.Clear();
			content.Clear();
			isEmptyElement = true;
		}

		/// <summary> Consumes characters from the lexical scanner until a &lt; character is encountered.
		/// <para>
		///		This method does not distinguish whitespace from other characters. 
		///		Upon return, this.NodeType == XmlNodeType.Text and this.Value is the 
		///		sequence of characters that have been scanned.
		/// </para>
		/// </summary>

		private void ReadText () {
			name = "TEXT";
			nodeType = XmlNodeType.Text;
			value = lexer.TokenValue;
			XmlTokenType token = lexer.Advance();
		}

		/// <summary> Gets the type of the current node.
		/// </summary>

		public XmlNodeType NodeType {
			get {
				return nodeType;
			}
		}

		/// <summary>
		///		When overridden in a derived class, gets the text value of the current node.
		///	<para>
		///		The value returned depends on the System.Xml.XmlReader.NodeType of the node.
		/// 	The following table lists node types that have a value to return. All other
		/// 	node types return String.Empty.
		/// </para>
		/// 	<list type="bullet">
		/// 	<item>Attribute: The value of the attribute. </item>
		/// 	<item>CDATA: The content of the CDATA section. </item>
		/// 	<item>Comment: The content of the comment. </item>
		/// 	<item>DocumentType: The internal subset. </item>
		/// 	<item>ProcessingInstruction: The entire content, excluding the target. </item>
		/// 	<item>SignificantWhitespace: The white space between markup in a mixed content model. </item>
		/// 	<item>Text: The content of the text node. </item>
		/// 	<item>Whitespace: The white space between markup. </item>
		/// 	<item>XmlDeclaration: The content of the declaration.</item>
		/// 	</list>
		/// </summary>

		public string Value {
			get {
				return value;
			}
		}

		/// <summary> When overridden in a derived class, gets the qualified name of the current
		///     node.
		///	<para>
		///	</para>
		///		Returns the qualified name of the current node. For example, Name is bk:book for
		///     the element &lt;bk:book&gt;.The name returned is dependent on the System.Xml.XmlReader.NodeType
		///     of the node. The following node types return the listed values. All other
		///     node types return an empty string.
		///     <list type="bullet">
		///     <item>Attribute: The name of the attribute. </item>
		///     <item>DocumentType: The document type name. </item>
		///     <item>Element: The tag name. </item>
		///     <item>EntityReference: The name of the entity referenced. </item>
		///     <item>ProcessingInstruction: The target of the processing instruction. </item>
		///     <item>XmlDeclaration: The literal string xml.</item>
		/// </list>
		/// </summary>

		public string Name {
			get {
				return name;
			}
		}

		/// <summary> Indicates if the current Element has content. That is, it was not self-closing.
		/// <para>
		///		Beware: this is almost certainly different from the 
		/// </para>
		/// </summary>

		public bool IsEmptyElement {
			get {
				return isEmptyElement;
			}
		}

		/// <summary> Returns true iff the current lexical token is a start tag of some kind.
		/// </summary>

		public bool IsStartToken {
			get {
				XmlTokenType token = lexer.Token;
				return token == XmlTokenType.OpenTagStart
					|| token == XmlTokenType.ProcessingInstructionStart
					|| token == XmlTokenType.XmlDeclStart
					|| token == XmlTokenType.ClosingTagStart;
			}
		}
	}
}
