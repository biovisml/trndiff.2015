﻿using System;
using System.Collections.Generic;


namespace SystemQut.Xml {
	public static class HtmlUtil {

		/// <summary> Creates a simple hyperlink with text content.
		/// </summary>
		/// <param name="href"></param>
		/// <param name="content"></param>
		/// <returns></returns>

		public static Element CreateHyperlink ( string href, object content ) {
			Element memberlink = new Element( null, "a", new string[] { "href", href }, null );
			memberlink.Append( content );
			return memberlink;
		}

		/// <summary> A convenience method which appends a row to a table and populates it with the 
		///		supplied textual content. The text is inserted as lireal HTML, with no encoding.
		///		Preprocessing, if any, should be done before reaching this point.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="cells"></param>

		public static void AppendRow ( Element table, object[] cells ) {
			Element row = table.AppendNode( null, "tr" );

			for ( int i = 0; i < cells.Length; i++ ) {
				row.AppendNode( null, "td" ).Append( cells[i] );
			}
		}

		/// <summary> Convenience method which creates a bare bones HTML document and inserts
		///		a title. The title appears as the page title and also as a top-level heading at the
		///		start of the page.
		/// </summary>
		/// <param name="title">
		///		The title.
		/// </param>
		/// <returns>
		///		An element which encodes a HTML DOM.
		/// </returns>

		public static Element CreateDocument ( string title ) {
			TextNode titleNode = new TextNode( null, title );
			Element html = new Element( null, "html", null, null );
			Element head = html.AppendNode( null, "head" );
			Element body = html.AppendNode( null, "body" );

			head.AppendNode( null, "title" ).AppendText( null, title );
			body.AppendNode( null, "h1" ).AppendText( null, title );

			return html;
		}

	}
}
