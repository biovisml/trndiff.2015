﻿using System;
using System.Collections.Generic;

namespace SystemQut.Xml {
	public class Element : Node {
		protected string tag;
		protected Dictionary<string,string> attributes = new Dictionary<string, string>();
		protected List<Node> children = new List<Node>();

		public Element (
			string id,
			string tag,
			string [] attributeValuePairs,
			Node [] children
		) : base ( id ) {
			Tag = tag;
			
			AddAttributes( attributeValuePairs );

			if ( children != null ) {
				foreach ( Node child in children ) {
					this.children.Add( child );
				}
			}
		}

		public void AddAttributes ( string[] attributeValuePairs ) {
			if ( attributeValuePairs != null ) {
				for ( int i = 0; i < attributeValuePairs.Length - 1; i += 2 ) {
					string key = attributeValuePairs[i];
					string value = attributeValuePairs[i + 1];
					attributes[key] = value;
				}
			}
		}

		public string Tag {
			get {
				return tag;
			}
			set {
				tag = value ?? string.Empty;
			}
		}

		public Dictionary<string, string> Attributes {
			get {
				return attributes;
			}
		}

		public List<Node> Children {
			get {
				return children;
			}
		}

		public void ForeachChild ( Action<Node,int> action ) {
			for ( int i = 0; i < children.Count; i++ ) action( children[i], i );
		}

		public void ForeachChildElement ( Action<Element,int> action ) {
			for ( int i = 0; i < children.Count; i++ ) {
				if( children[i] is Element ) action( (Element) children[i], i );
			}
		}

		public override string ToString () {
			StringBuilder sb = new StringBuilder();
			
			foreach (string key in attributes.Keys ) {
				sb.Append( String.Format( " {0}=\"{1}\"", key, Encode( attributes[key] ) ) );
			}

			string attrs = sb.ToString();

			sb.Clear();
			
			foreach (Node child in children) {
				sb.Append( child.ToString() );
			}

			string content = sb.ToString();

			return children.Count > 0 
				? String.Format( "<{0}{1}>{2}</{0}>", tag, attrs, content )
				: String.Format( "<{0}{1}/>", tag, attrs );
		}

		public static string Encode ( string p ) {
			/*
				Name Character [Unicode code point (decimal)] Standard Description
				quot " U+0022 (34) XML 1.0 double quotation mark 
				amp & U+0026 (38) XML 1.0 ampersand 
				apos ' U+0027 (39) XML 1.0 apostrophe (= apostrophe-quote) 
				lt < U+003C (60) XML 1.0 less-than sign 
				gt > U+003E (62) XML 1.0 greater-than sign 
			 */
			return p.Replace( "&", "&amp;" ).Replace( "\"", "&quot;" ).Replace( "<", "&lt;" ).Replace( ">", "&gt;" );
		}

		public TextNode AppendText ( string id, string text ) {
			TextNode node = new TextNode( id, text );
			children.Add( node );
			return node;
		}

		public Element AppendNode( string id, string tag ) {
			Element element = new Element( id, tag, null, null );
			children.Add( element );
			return element;
		}

		public void AppendNodes( Node [] nodes ) {
			// children.AddRange( nodes );
			for (int i = 0; i < nodes.Length; i++ ) {
				children.Add( nodes[i] );
			}
		}

		/// <summary> Appends a list of nodes or literal text objects to the list of child elements of this
		///		element.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public Node Append ( object obj ) {
			if ( obj is Node ) {
				Node node = (Node) obj;
				children.Add( node );
				return node;
			}
			else {
				if ( Script.IsNull( obj ) ) obj = "null";
				if ( Script.IsUndefined( obj ) ) obj = "undefined";

				return AppendText( null, obj.ToString() );
			}
		}

		public Element [] GetChildrenByTagName( string tagName ) {
			List<Element> results = new List<Element>();
			GetChildrenByTagNameAux( tagName, results );
			return (Element []) results;
		}

		/// <summary> Returns a list of child elements which match on Id.
		/// </summary>
		/// <param name="id">The element Id to locate.</param>
		/// <returns>An array of Nodes </returns>

		public Node [] GetChildrenById( string id ) {
			List<Node> results = new List<Node>();
			GetChildrenByIdAux( id, results );
			return (Element []) results;
		}

		private void GetChildrenByTagNameAux( string tagName, List<Element> results ) {
			for( int i = 0; i < children.Count; i++ ) {
				if ( children[i] is Element ) {
					Element element = (Element) children[i];
					
					if ( element.tag == tagName ) results.Add( element );

					element.GetChildrenByTagNameAux( tagName, results );
				}
			}
		}

		private void GetChildrenByIdAux( string id, List<Node> results ) {
			for( int i = 0; i < children.Count; i++ ) {
				if ( children[i].Id == id ) results.Add( children[i] );

				if ( children[i] is Element ) {
					Element element = (Element) children[i];

					element.GetChildrenByIdAux( id, results );
				}
			}
		}
	}
}
