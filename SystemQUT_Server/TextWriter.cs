﻿using System;
using System.Collections.Generic;

namespace SystemQut.IO {

	public abstract class TextWriter {

		/// <summary> Provides a TextWriter with no backing store that can be written to, but not
		///    read from.
		/// </summary>

		public static readonly TextWriter Nul = new NullTextWriter();

		private string newLine = "\n";

		/// <summary> Initializes a new instance of the System.IO.TextWriter class.
		/// </summary>

		protected TextWriter () {}
		
		/// <summary> Gets or sets the line terminator string used by the current TextWriter.
		/// </summary>

		public virtual string NewLine {
			get {
				return this.newLine;
			}
			set {
				this.newLine = value;
			}
		}

		/// <summary> Closes the current writer and releases any system resources associated with
		/// the writer.
		/// </summary>
		
		public virtual void Close () {
			// Noop.
		}

		/// <summary> Clears all buffers for the current writer and causes any buffered data to
		/// be written to the underlying device.
		/// </summary>

		public virtual void Flush () {
			// Noop;
		}

		/// <summary> Writes a character array to the text stream.
		/// </summary>
		/// <param name="buffer"> The character array to write to the text stream.</param>

		public virtual void WriteChar ( char buffer ) {
			WriteString( (string) buffer );
		}

		/// <summary> Writes a character array to the text stream.
		/// </summary>
		/// <param name="buffer"> The character array to write to the text stream.</param>

		public virtual void WriteChars ( char[] buffer ) {
			WriteString( buffer.Join() );
		}

		/// <summary>Writes the text representation of an object to the text stream by calling
		///    ToString on that object.
		/// </summary>
		/// <param name="value"></param>
		
		public virtual void WriteObject ( object value ) {
			WriteString( value.ToString() );
		}
	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>

		public abstract void WriteString ( string value );

		//
		// Summary:
		//     Writes out a formatted string, using the same semantics as System.String.Format(System.String,System.Object).
		//
		// Parameters:
		//   format:
		//     The formatting string.
		//
		//   arg0:
		//     An object to write into the formatted string.
		//
		// Exceptions:
		//   System.ArgumentNullException:
		//     format is null.
		//
		//   System.ObjectDisposedException:
		//     The System.IO.TextWriter is closed.
		//
		//   System.IO.IOException:
		//     An I/O error occurs.
		//
		//   System.FormatException:
		//     The format specification in format is invalid.-or- The number indicating
		//     an argument to be formatted is less than zero, or larger than or equal to
		//     the number of provided objects to be formatted.


		public virtual void Write ( string format, object [] args ){
			WriteString( string.Format( format, args ) );
		}
		
		/// <summary>   Writes a line terminator to the text stream.
		/// </summary>
		
		public virtual void WriteLn () {
			WriteString( newLine );
		}

		/// <summary> Writes a character array to the text stream.
		/// </summary>
		/// <param name="buffer"> The character array to write to the text stream.</param>

		public virtual void WriteLineChars ( char[] buffer ) {
			WriteString( buffer.Join() );
		}

		/// <summary>Writes the text representation of an object to the text stream by calling
		///    ToString on that object.
		/// </summary>
		/// <param name="value"></param>

		public virtual void WriteLineObject ( object value ) {
			WriteString( value.ToString() );
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>

		public virtual void WriteLineString ( string value ){
			WriteString( value );
			WriteLn();
		}

		//
		// Summary:
		//     Writes out a formatted string, using the same semantics as System.String.Format(System.String,System.Object).
		//
		// Parameters:
		//   format:
		//     The formatting string.
		//
		//   arg0:
		//     An object to write into the formatted string.
		//
		// Exceptions:
		//   System.ArgumentNullException:
		//     format is null.
		//
		//   System.ObjectDisposedException:
		//     The System.IO.TextWriter is closed.
		//
		//   System.IO.IOException:
		//     An I/O error occurs.
		//
		//   System.FormatException:
		//     The format specification in format is invalid.-or- The number indicating
		//     an argument to be formatted is less than zero, or larger than or equal to
		//     the number of provided objects to be formatted.

		public virtual void WriteLine ( string format, object[] args ) {
			WriteString( string.Format( format, args ) );
			WriteLn();
		}

	}
}
