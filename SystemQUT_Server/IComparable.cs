﻿using System;
using System.Collections.Generic;

namespace SystemQut {

	public interface IComparable {
		int CompareTo( object other );
	}
}
