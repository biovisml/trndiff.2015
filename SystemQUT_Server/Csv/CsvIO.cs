﻿using System;
using System.Collections.Generic;
using SystemQut.IO;

namespace SystemQUT.Csv {

	/// <summary> CsvIO provides provides low-level I/O 
	///		functions to parse and generate Comma Separated Value (.CSV) text 
	///		streams as implicitly defined by the behaviour of Microsoft Excel.
	///	<para>
	///		The definition can be found in RFC 4180 - Common Format and MIME 
	///		Type for CSV Files - October 2005:<br />
	///		<a href="http://tools.ietf.org/html/rfc4180">http://tools.ietf.org/html/rfc4180</a>  
	///	</para>
	///	<para>
	///		This is a JavaScript port of source code originally implemented as part of the 
	///		CIEAM HAMISH Reliability Basics product.
	///	</para>
	/// </summary>

	public class CsvIO {
		/// <summary> The default field separator: a comma.
		/// </summary>
		public const char DefaultSeparator = ',';

		/// <summary> The default escape character: a double quote.
		/// </summary>
		public const char DefaultQuoteChar = '"';

		/// <summary> Internal representation of the separator character.
		/// </summary>
		private readonly int separator;

		/// <summary> Internal representation of the escape (quote) character.
		/// </summary>
		private readonly int quoteChar;

		/// <summary> Initialises a CsvIo object with default separator and quote character.
		/// </summary>

		extern public CsvIO();

		/// <summary> Initialises a CsvIo object with explicit separator and default quote character.
		/// </summary>

		extern public CsvIO( char separator );

		/// <summary> Initialises a CsvIO object, setting the field separator and quote character.
		///		<para>Records are always separated by either cariage return (CR) or line feed (LF) characters.</para>
		///		<para><strong>NB:</strong> This is available only if the assembly is referenced directly.</para>
		/// </summary>
		/// <param name="separator">
		///		The character that will be used to separate fields within a record.
		///		<para>
		///			This may not be carriage-return, line-feed or the same as the "quote" character.
		///		</para>
		/// </param>
		/// <param name="quoteChar">
		///		The escape (quote) character that will be used to mark up fields which contain
		///		embedded carriage-return, line-feed, separator or quote characters.
		/// </param>
		/// <exception cref="Exception">
		///		Thrown if invalid separator or quote char are suuplied.
		/// </exception>

		public CsvIO ( char separator, char quoteChar ) {

			IArrayLike<object> args = Arguments.Current;

			if ( args.Length < 1 ) separator = DefaultSeparator;
			if ( args.Length < 2 ) quoteChar = DefaultQuoteChar; 

			// Very ugly indeed... JavaScript does not actually posses a true char type. All chars are strings.
			// So an assignment from char to int, or a cast from int to char, have completely unexpected results!
			int separator_ = ((string) separator).CharCodeAt(0);
			int quoteChar_ = ( (string) quoteChar ).CharCodeAt( 0 );

			if ( separator == CR || separator == LF || separator == quoteChar ) {
				throw new Exception( "separator may not be CR, LF or the escape (quote) character." );
			}

			if ( quoteChar == CR || quoteChar == LF ) {
				throw new Exception( "quote character may not be CR or LF." );
			}

			this.separator = separator_;
			this.quoteChar = quoteChar_;
		}

		/* Excerpt from RFC 4180       Common Format and MIME Type for CSV Files    October 2005
		 *		http://tools.ietf.org/html/rfc4180
		 *		
		 *		The grammar, modified from RFC by removing the optional header, is:
		 *		
		 *		file = record *(CRLF record) [CRLF]
		 *		record = field *(COMMA field)
		 *		field = (escaped / non-escaped)
		 *		escaped = DQUOTE *(TEXTDATA / COMMA / CR / LF / 2DQUOTE) DQUOTE
		 *		non-escaped = *TEXTDATA
		 *		COMMA = %x2C
		 *		CR = %x0D ;as per section 6.1 of RFC 2234 [2]
		 *		DQUOTE =  %x22 ;as per section 6.1 of RFC 2234 [2]
		 *		LF = %x0A ;as per section 6.1 of RFC 2234 [2]
		 *		CRLF = CR LF ;as per section 6.1 of RFC 2234 [2]
		 *		TEXTDATA =  %x20-21 / %x23-2B / %x2D-7E
		 */

		#region Variables and constants used by Parse function.
		const int NUL = 0;
		const int EOF = -1;
		const int CR = 0x0D;
		const int LF = 0x0A;
		const int CRLF = -2;
		const int EOS = -3;

		private int currentChar = NUL;
		private int nextChar = EOF;
		private readonly List<List<string>> rows = new List<List<string>>();
		private TextReader reader;
		private bool parsingString;
		private List<string> currentRow;
		private readonly StringBuilder currentField = new StringBuilder();
		private int position;
		private int lineNumber;
		#endregion

		/// <summary> Returns true iff the current character is an end-of-line marker (CR, LF or CRLF).
		/// </summary>
		private bool IsEOL { get { return currentChar == CR || currentChar == LF || currentChar == CRLF; } }

		/// <summary> Returns true iff the end-of-file has been encountered.
		/// </summary>

		private bool IsEOF { get { return currentChar == EOF; } }

		/// <summary> Returns true iff the parser is at the beginning of the file.
		/// </summary>

		private bool IsBOF { get { return currentChar == NUL; } }

		/// <summary> Returns true iff the parser is at a separator (by default, COMMA).
		/// </summary>

		private bool IsCOMMA { get { return currentChar == separator; } }

		/// <summary> Returns true iff the parse is at the closing quote that marks the end of a string.
		/// </summary>

		private bool IsEOS { get { return currentChar == EOS; } }

		/// <summary> Convert the internal collection of records into a ragged array of strings.
		/// </summary>
		/// <returns></returns>

		private string[][] ToRaggedArray () {
			return (string[][]) rows;
		}

		/// <summary> Parses a CSV-formatted dataset from the supplied text reader.
		///		<para><strong>NB:</strong> This is available only if the assembly is referenced directly.</para>
		/// </summary>
		/// <param name="reader">
		///		A TextReader from which input is obtained.
		/// </param>
		/// <returns>
		///		Returns a list of records, one for each row in the dataset. Each of record 
		///		is an array of string containing one element for each field in the row. 
		/// </returns>

		public string[][] Read ( TextReader reader ) {
			this.reader = reader;
			this.rows.Clear();
			LowLevelRead();
			ReadFile();
			return ToRaggedArray();
		}

		/// <summary> Consumes a character from the input stream, stores it in nextChar and updates the position.
		/// </summary>

		private void LowLevelRead () {
			nextChar = reader.ReadCharCode();

			if ( nextChar != EOF ) position++;
		}

		/// <summary> Copies the buffered nextChar value to currentChar,
		///		with appropriate processing for digraphs such as CRLF and 2DQUOTE.
		///		<para>
		///			When this returns, currentChar contains the "most recently read symbol",
		///			and nextChar contains the first character of the next symbol to be read.
		///			This will _usually_ be the next character, but it may be the first character
		///			of a digraph.
		///		</para>
		/// </summary>

		private void ReadChar () {
			currentChar = nextChar;

			if ( nextChar == CR ) {
				#region convert CR LF to CRLF
				LowLevelRead();

				if ( nextChar == LF ) {
					LowLevelRead();
					currentChar = CRLF;
				}

				lineNumber++;
				position = 0;
				#endregion
			}

			else if ( nextChar == LF ) {
				#region Process EOL
				LowLevelRead();

				lineNumber++;
				position = 0;
				#endregion
			}
			else if ( parsingString && nextChar == quoteChar ) {
				#region Convert DQUOTE DQUOTE to either single quote or end of string.
				nextChar = reader.ReadCharCode();

				if ( nextChar == quoteChar ) {
					nextChar = reader.ReadCharCode();
				}
				else {
					currentChar = EOS;
				}
				#endregion
			}
			else {
				#region No other conversion necessary... get next char.
				nextChar = reader.ReadCharCode();
				#endregion
			}
		}

		/// <summary> Reads an entire file, which is defined by a production of the form
		///		File = record *(CRLF record) [CRLF]
		/// <para>
		///		On entry, one character from the text reader has been consumed and stored in
		///		nextChar while currentChar is uninitialised.
		/// </para>
		/// <para>
		///		On exit, all available characters in the textreader have been consumed and 
		///		currentChar is EOF.
		/// </para>
		/// </summary>

		private void ReadFile () {
			do ReadRecord(); while ( IsEOL );

			if ( !( currentChar == EOF ) ) throw new Exception( "Expected EOF but found something else." );

			if ( currentRow != null && currentRow.Count == 1 && currentRow[0].Length == 0 ) {
				rows.RemoveAt( rows.Count - 1 );
			}
		}

		/// <summary> Reads a record, of the form 
		///		Record = Field *(COMMA field)
		///	<para>
		///		On entry, it is assumed that currentChar is either uninitialised or retains
		///		the end-of-line marker which terminated the previous record. Either way, this 
		///		value is discarded. 
		///		As an attempt to continue the parse in the event of malformed input, we skip 
		///		characters until this assumption becomes true.
		///	</para>
		///	<para>
		///		On exit, currentChar is either CRLF or EOF, currentRow has been set back to null,
		///		and a new row has been added to the rows collection with the fields garnered from 
		///		this row.
		///	</para>
		/// </summary>

		private void ReadRecord () {
			while ( !( IsBOF || IsEOF || IsEOL ) ) ReadChar();

			if ( IsEOF ) return;

			rows.Add( currentRow = new List<string>() );

			do ReadField(); while ( currentChar == separator );
		}

		/// <summary> Reads a single field, implementing the three productions:
		///		field = (escaped / non-escaped)
		/// 	escaped = DQUOTE *(TEXTDATA / COMMA / CR / LF / 2DQUOTE) DQUOTE
		/// 	non-escaped = *TEXTDATA
		/// <para>
		///		On entry, currentChar should be either NUL, EOLN or COMMA. That is,
		///		we are about to process a newly encountered field. If this is not 
		///		the case, we skip forward until EOF or one of these is found to e
		/// </para>
		/// </summary>

		private void ReadField () {
			while ( !( IsBOF || IsEOF || IsEOL || IsCOMMA ) ) ReadChar();

			if ( IsEOF ) return;

			currentField.Clear();

			if ( nextChar == quoteChar ) ReadEscaped(); else ReadNonEscaped();

			currentRow.Add( currentField.ToString() );
		}

		/// <summary> Reads an unquoted field, implementing the production:
		/// 	non-escaped = *TEXTDATA
		/// 	<para>
		/// 		On entry, current char is: beginning of file, separator or end of line 
		/// 		(the previous line). 
		/// 	</para>
		/// 	<para>
		/// 		On exit, parse has advanced to the end of the current record or the next 
		/// 		separator or the end of file.
		/// 	</para>
		/// </summary>

		private void ReadNonEscaped () {
			for ( ; ; ) {
				ReadChar();

				if ( IsEOF || IsEOL || IsCOMMA ) break;

				currentField.Append( String.FromCharCode(currentChar) );
			}
		}

		/// <summary> Reads an escaped (quoted) field, implementing the production:
		///		escaped = DQUOTE *(TEXTDATA / COMMA / CR / LF / 2DQUOTE) DQUOTE
		///		<para>
		///			On entry, it is assumed that the parser is looking at the opening quote
		///			of an escaped field. This value is discarded and we set parsingString to 
		///			indicate that special treatment is required for quote characters. The field 
		///			is terminated by a quote char followed by anything other than a quote char, 
		///			including EOF.
		///		</para>
		///		<para>
		///			According to the RFC, there can be no other content in the field 
		///			outside (particularly, _after_) the quotes.
		///			Therfore, once the string terminator is found, we advance until EOF, EOL or COMMA
		///			is seen.
		///		</para>
		/// </summary>

		private void ReadEscaped () {
			try {
				ReadChar();

				if ( !( currentChar == quoteChar ) ) throw new Exception( "Expecting quote character at beginning of quoted field." );

				parsingString = true;

				for ( ; ; ) {
					ReadChar();

					if ( IsEOF || IsEOS ) break;

					currentField.Append( IsEOL ? "\n" : String.FromCharCode( currentChar ) );
				}

				while ( !( IsEOF || IsCOMMA || IsEOL ) ) ReadChar();
			}
			finally {
				parsingString = false;
			}
		}

		/// <summary> Writes a collection of records to a stream in CSV format, encoding 
		/// fields as required.
		///		<para><strong>NB:</strong> This is available only if the assembly is referenced directly.</para>
		/// </summary>
		/// <param name="records">
		///		A list of records, each of which is a list of strings which are to be serialized in 
		///		CSV format.
		/// </param>
		/// <param name="writer">
		///		A TextWriter to which the resulting character stream will be written.
		/// </param>

		public void Write ( string [][] records, TextWriter writer ) {

			foreach ( string [] record in records ) {
				WriteRecord( record, writer );
			}
		}

		public void WriteRecord ( string[] record, TextWriter writer ) {
			// The .NET CLR implementation casts the ints to char, but that produces bad unexpected
			// outcome in JavaScript. You get a numeric literal rather than a character.
			char sepC = string.FromCharCode( separator )[0];
			char quoteC = string.FromCharCode( quoteChar )[0];
			char crC = string.FromCharCode( CR )[0];
			char lfC = string.FromCharCode( LF )[0];

			char [] escapeWorthyCharacters = { sepC, quoteC, crC, lfC };

			int fieldCount = record.Length;
			int i = 0;

			foreach ( string field in record ) {
				string field_ = field == null ? "" : field;

				bool escaped = IndexOfAny( field_, escapeWorthyCharacters ) >= 0;

				if ( escaped ) writer.WriteChar( quoteC );

				for ( int j = 0; j < field_.Length; j++ ) {
					char c= field_[j];

					if ( c == quoteC ) writer.WriteChar( c );

					writer.WriteChar( c );
				}

				if ( escaped ) writer.WriteChar( quoteC );

				if ( ++i < fieldCount ) writer.WriteChar( sepC );
			}

			writer.WriteLn();
		}

		/// <summary> Returns the position of the first occurrence of any of the supplied characters.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="chars"></param>
		/// <returns></returns>

		private int IndexOfAny ( string text, char[] chars ) {
			int result = -1;

			for( int i = 0; i < chars.Length; i++ ) {
				int idx = text.IndexOf( chars[i] );

				if ( idx >= 0 && ( result < 0 || idx < result ) ) result = idx;
			}

			return result;
		}
	}
}
