﻿using System;
using System.Collections.Generic;
using SystemQUT.Csv;
using SystemQut.IO;
using System.Serialization;

namespace SystemQut {
	/// <summary> General-purpose static methods that I would have preferred to place in the Object class if I could...
	/// </summary>

	static public class Objects {

		/// <summary> Determines if two objects are deeply identical.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>

		static public bool Identical ( object x, object y ) {
			if ( Script.IsNullOrUndefined( x ) || Script.IsNullOrUndefined( y ) )
				return x == y;

			if ( x is Array ) {
				if ( y is Array ) {
					Array xA = (Array) x;
					Array yA = (Array) y;

					if ( xA.Length != yA.Length )
						return false;

					for ( int i = 0; i < xA.Length; i++ ) {
						if ( !Identical( xA[i], yA[i] ) )
							return false;
					}

					return true;
				}
				else {
					return false;
				}
			}
			else if ( x.GetType() != y.GetType() ) {
				return false;
			}
			else if ( x is String || x is Number || x is RegExp || x is Action ) {
				return x == y;
			}
			else {
				try {
					Dictionary<string, object> xD = (Dictionary<string, object>) x;
					IReadonlyCollection<string> xKeys = xD.Keys;

					if ( xKeys.Count > 0 ) {
						Dictionary<string, object> yD = (Dictionary<string, object>) y;
						IReadonlyCollection<string> yKeys = yD.Keys;

						if ( yKeys.Count != xKeys.Count )
							return false;

						( (string[]) (object) xKeys ).Sort();
						( (string[]) (object) yKeys ).Sort();

						for ( int i = 0; i < xKeys.Count; i++ ) {
							if ( xKeys[i] != yKeys[i] )
								return false;
						}

						foreach ( string key in xKeys ) {
							if ( !Identical( xD[key], yD[key] ) )
								return false;
						}

						return true;
					}
					else {
						return x == y;
					}
				}
#pragma warning disable 168
				catch ( Exception ex ) {
					return x == y;
				}
#pragma warning restore 168
			}
		}

		/// <summary> Compares two Equatable objects, looking out for null and undefined values.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>

		static public bool AreEqual<T> ( IEquatable<T> x, T y ) {
			if ( Script.IsNullOrUndefined( x ) ) {
				return (object) x == (object) y;
			}
			else {
				return Script.IsNullOrUndefined( y ) ? false : x.Equals( y );
			}
		}

		/// <summary> Gets the string representing the supplied value.
		/// <para>
		///		If value is null or undefined, then the substitute value is used instead.
		/// </para>
		/// </summary>
		/// <param name="value">
		///		The (possibly null or undefined) value to process.
		/// </param>
		/// <param name="substitute">
		///		The text returned in the event that the supplied value is null or undefined. 
		/// </param>
		/// <returns>
		///		Returns <c>Script.IsNullOrUndefined( value ) ? substitute : value.ToString()</c>.
		/// </returns>

		public static string ToString ( object value, string substitute ) {
			return Script.IsNullOrUndefined( value ) ? substitute : value.ToString();
		}

		/// <summary> Folds a pair of parallel arrays which represent the field names and values of 
		///		a record into a JavaScript object.
		/// </summary>
		/// <typeparam name="T">
		///		The type of the values, typically object or string.
		/// </typeparam>
		/// <param name="keys">
		///		The field names of the record.
		/// </param>
		/// <param name="values">
		///		The field values of the record.
		/// </param>
		/// <returns>
		///		Returns a JavaScript object which represents the record.
		/// </returns>

		public static JSObject FromArrays<T> ( string[] keys, T[] values ) {
			JSObject obj = new JSObject();

			for ( int i = 0; i < keys.Length; i++ ) {
				obj[keys[i]] = values[i];
			}

			return obj;
		}

		/// <summary> Serialises a table containing (preferably homgeneous) record-type
		///		variables to CSV using default separator (comma) and quote-character 
		///		(double quote).
		/// </summary>
		/// <param name="objects">
		///		A collection which is to be serialized as a table.
		/// </param>
		/// <returns>
		///		A string containing comma-separated fields, quoted as required and formatted according 
		///		to RFC 4180 - Common Format and MIME 
		///		Type for CSV Files - October 2005:<br />
		///		<a href="http://tools.ietf.org/html/rfc4180">http://tools.ietf.org/html/rfc4180</a> 
		///	<para>
		///		The first row contains column headings which are inferred from the property names found 
		///		in the collection of objects.
		/// </para>
		/// </returns>

		public static string SerializeTable ( JSObject[] objects ) {
			// Get the complete key collection.
			List<string> keys = new List<string>();

			foreach ( JSObject obj in objects ) {
				foreach ( KeyValuePair<string, object> kvp in obj ) {
					//Console.Log( kvp.Key + ": " + kvp.Value.GetType().Name ); 

					if ( kvp.Value.GetType().Name == "Function" || keys.Contains( kvp.Key ) ) continue; 
					
					keys.Add( kvp.Key );
				}
			}

			// Create a list to hold the records and add the the header, containing the key rows.
			List<List<string>> records = new List<List<string>>();
			records.Add( keys );

			// Append records containing field values to list.
			foreach ( JSObject obj in objects ) {
				List<string> fields = new List<string>();
				
				foreach ( string key in keys ) {
					object value = obj[key];
					fields.Add( ToString( value, String.Empty ) );
				}
				
				records.Add( fields );
			}

			// Generate serialised Csv.
			CsvIO csv = new CsvIO();
			StringWriter writer = new StringWriter();
			csv.Write( (string [][]) records, writer );
			return writer.ToString();
		}

		/// <summary> Deserialises a table into a collection of JavaScript objects.
		/// <para>
		///		The table is assumed to have been correctly serialised according to
		///		RFC 4180 - Common Format and MIME Type for CSV Files - October 2005:<br />
		///		<a href="http://tools.ietf.org/html/rfc4180">http://tools.ietf.org/html/rfc4180</a>
		/// </para>
		/// </summary>
		/// <param name="table">
		///		A string containing the csv-formatted table which is to be parsed.
		/// </param>
		/// <returns>
		///		An array of JavaScript objects which contain the parsed content with all fields 
		///		represented as strings.
		///	<para>
		///		You will need to perform downstream action to derive field values in their correct 
		///		native type.
		/// </para>
		/// </returns>

		public static JSObject [] DeserializeTable (string table ){
			CsvIO csv = new CsvIO();
			StringReader reader = new StringReader( table );
			string [][] records = csv.Read( reader );
			return DeserializeRecords( records );
		}

		/// <summary> Deserialises a flat-file of text records represented as arrays of strings 
		///		into a collection of JavaScript objects.
		/// </summary>
		/// <param name="records">
		///		A list of string arrays which cointains one row pre record, with a header row containing 
		///		the field names.
		/// </param>
		/// <returns>
		///		An array of JavaScript objects which contain the parsed content with all fields 
		///		represented as strings.
		///	<para>
		///		You will need to perform downstream action to convert field values to their correct 
		///		native type.
		/// </para>
		/// </returns>

		public static JSObject[] DeserializeRecords ( string[][] records ) {
			List<JSObject> objects = new List<JSObject>();

			if ( records.Length < 2 ) return objects;

			string[] keys = records[0];

			for ( int i = 1; i < records.Length; i++ ) {
				string[] values = records[i];
				objects.Add( FromArrays( keys, values ) );
			}

			return objects;
		}


		/// <summary> Copies all fields from the source object to the destination object.
		/// <para>
		///		Fields named in the excludedFields argument will not be copied.
		/// </para>
		/// </summary>
		/// <param name="source">
		///		The source object.
		/// </param>
		/// <param name="dest">
		///		The destination object.
		/// </param>
		/// <param name="excludedFields">
		///		A (possibly null) list of field names to exclude from the copy.
		/// </param>

		public static void ShallowCopy ( JSObject source, JSObject dest, string[] excludedFields ) {
			foreach ( KeyValuePair<string,object> kvp in source ) {
				if ( excludedFields == null || !excludedFields.Contains( kvp.Key ) ) {
					dest[kvp.Key] = source[kvp.Key];
				}
			}
		}

	}
}
