﻿using System;
using System.Collections.Generic;
using NodeApi.Network;
using SystemQut.Xml;
using NodeApi.IO;

namespace SystemQut {

	/// <summary> Helper methods for 
	/// </summary>

	public static class HttpHelpers {


		/// <summary> Helper method which transmits a successful JSON representation of the results back to the client.
		/// </summary>
		/// <param name="response">
		///		The Http Rresponse object that will transport results.
		/// </param>
		/// <param name="json">
		///		Service call results encoded as a JS object literal.
		/// </param>

		public static void ReturnJson ( HttpServerResponse response, string json ) {
			response.WriteHead( HttpStatusCode.OK, new Dictionary<string, string>( "Content-Type", "application/json" ) );
			response.EndOfData( json );
		}

		/// <summary> Returns a page describing an error condition.
		/// </summary>
		/// <param name="response">
		///		The Http response object.
		///	</param>
		/// <param name="errorMessage">
		///		The high level error message.
		/// </param>
		/// <param name="errorDetail">
		///		Detailed error message.
		/// </param>
		/// <param name="statusCode">
		///		The status code to return.
		/// </param>

		public static void ReturnErrorPage ( HttpServerResponse response, HttpStatusCode statusCode, string errorMessage, string errorDetail ) {
			// Console.Log( String.Format( "Error: {0}\n\t\t{1}\n\t\t{1}", statusCode, errorMessage, errorDetail ) );

			Element page = HtmlUtil.CreateDocument( "Error" );
			Element body = page.GetChildrenByTagName( "body" )[0];
			body.AppendNode( null, "p" ).AppendText( null, errorMessage );
			body.AppendNode( null, "p" ).AppendText( null, errorDetail );
			ReturnContent( response, statusCode, page.ToString(), "text/html" );
		}

		/// <summary> Emits the provided 
		/// </summary>
		/// <param name="response"></param>
		/// <param name="html"></param>
		/// <param name="mimeType"></param>
		/// <param name="statusCode"></param>

		public static void ReturnContent ( HttpServerResponse response, HttpStatusCode statusCode, string html, string mimeType ) {
			response.WriteHead( statusCode, new Dictionary<string, string>( "Content-Type", mimeType ) );
			response.EndOfData( html );
		}

		/// <summary> Emits the provided 
		/// </summary>
		/// <param name="response"></param>
		/// <param name="buffer"></param>
		/// <param name="mimeType"></param>
		/// <param name="statusCode"></param>

		public static void ReturnBufferContent ( HttpServerResponse response, HttpStatusCode statusCode, Buffer buffer, string mimeType ) {
			response.WriteHead( statusCode, new Dictionary<string, string>( "Content-Type", mimeType ) );
			response.EndOfData( buffer );
		}

	}
}
