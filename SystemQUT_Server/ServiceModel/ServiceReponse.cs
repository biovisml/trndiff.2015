﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SystemQut.ServiceModel {

	public class ServiceResponse<T> {
		public T Content;
		public string Error;

		public ServiceResponse (
			T records,
			string error
		) {
			this.Content = records;
			this.Error = error;
		}
	}
}
