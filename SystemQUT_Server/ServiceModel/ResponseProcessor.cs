﻿using System;
using System.Collections.Generic;

namespace SystemQut.ServiceModel {

	/// <summary> Callback function used by consumer to action response from web service.
	/// </summary>
	/// <typeparam name="ResultType"> The type of the value returned by the service. </typeparam>
	/// <param name="response"> The response that encapsulates the result plus an accompanying Exception object. </param>

	public delegate void ResponseProcessor<ResultType>( ServiceResponse<ResultType> response );
}
