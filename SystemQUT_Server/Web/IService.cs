// IService.cs
//

using System;
using System.Collections.Generic;
using NodeApi.Network;

namespace SystemQut.Web {
	
	/// <summary> Very simple interface for web service.
	/// </summary>

	public interface IService {

		/// <summary> Processes a web request.
		/// </summary>
		/// <param name="pathFragments"> The collection of slash-separated path fragments. </param>
		/// <param name="query"> The query part of the url. </param>
		/// <param name="request"> The HttpRequest object, which is needed if a POST request is encountered. </param>
		/// <param name="response"> A HttpResponse object which is used to transport the resulting document back to the caller. </param>

		void Process( 
			string [] pathFragments,
			Dictionary<string, string> query,
			HttpServerRequest request,
			HttpServerResponse response
		);
	}
}
