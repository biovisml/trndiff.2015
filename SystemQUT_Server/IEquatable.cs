namespace SystemQut {

	public interface IEquatable<T> {
		bool Equals( T other );
	}
}
