﻿using System;
using System.Collections.Generic;

namespace SystemQut {
	
	/// <summary>
	/// General purpose tuple class, intended to replace KeyValuePair which is treated 
	/// oddly by the ScriptSharp compiler in some situations.
	/// </summary>
	/// <typeparam name="T1"></typeparam>
	/// <typeparam name="T2"></typeparam>

	public class Pair<T1, T2> {
		public T1 Key;
		public T2 Value;

		extern public Pair();

		public Pair( T1 key, T2 value ) { 
			Arguments args = Arguments.Current;

			if ( args.Length == 2 ) {
				Key = key;
				Value = value;
			}
			else if ( args.Length != 0 ) {
				throw new Exception( "Expecting 0 or 2 arguments." );
			}
		}
	}
}
