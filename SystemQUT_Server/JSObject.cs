﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Collections;

namespace SystemQut {
	/// <summary> More convenient access to the dictionary aspects of a javascript object.
	/// <para>
	///		I expect to add the full ECMAScript 5 object extensions to this class, and possibly to 
	///		move it into mscorlib.
	/// </para>
	/// </summary>

    [ScriptImport, ScriptIgnoreNamespace, ScriptName("Object")]

	public class JSObject: IEnumerable {

		//public int Count {
		//	get {
		//		return 0;
		//	}
		//}

		//public IReadonlyCollection<string> Keys {
		//	get {
		//		return null;
		//	}
		//}

        [ScriptField]
        public object this[string key] {
            get {
                return null;
            }
            set {
            }
        }

        [ScriptAlias("ss.clearKeys")]
        public void Clear() {
        }

        [ScriptAlias("ss.keyExists")]
        public bool ContainsKey(string key) {
            return false;
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator() {
            return null;
        }

        public void Remove(string key) {
        }

		//public static implicit operator JSObject(object obj) {
		//    return null;
		//}

		//public static implicit operator object(JSObject dictionary) {
		//    return null;
		//}

		IEnumerator IEnumerable.GetEnumerator () {
			return null;
		}
	}
}
