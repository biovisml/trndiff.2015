﻿// AssemblyInfo.cs
//

using System;
using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "SystemQut" )]
[assembly: AssemblyDescription( "" )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "" )]
[assembly: AssemblyProduct( "SystemQut" )]
[assembly: AssemblyCopyright( "Copyright © Queensland University of Technology 2013" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

[assembly: ScriptAssembly( "SystemQut" )]

// A script template providing a CommonJS module structure around
// the generated script.
[assembly: ScriptTemplate( @"
// {name}.js {version}
// {description}

{dependenciesLookup}

var seedrandom = require('seedrandom');

var $global = this;

{script}
ss.extend(exports, $exports);
" )]
