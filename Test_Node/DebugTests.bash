if [ -d bin ]; then 
	cd bin/debug; 
fi

kill `ps | grep nodejs/node | awk '{ print $1 }'`
node-inspector & node --debug-brk Test_Node.js
echo Done.
read ignored