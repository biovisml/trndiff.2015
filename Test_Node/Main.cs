// Class1.cs
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using NodeApi;
using NodeApi.Network;
using Test_Node;
using NodeApi.IO;
using NodeApi.Compute;
using Console = NodeApi.IO.Console;

[ScriptModule]
internal static class Main {

	static Main () {
		Node.Process.UncaughtException += delegate ( Exception arg ) {
			Console.Log( arg.StackTrace );

			if ( Script.IsValue( arg.InnerException ) ) {
				 Console.Log( "Inner exception:" );
				 Console.Log( arg.InnerException.StackTrace + "\n" );
			}
		};

		Action<Action[]> runTests = delegate ( Action [] tests ) {
			foreach ( Action test in tests ) {
				try {
					test();
				}
				catch ( Exception ex ) {
					Console.Log( ex.StackTrace + "\n" );
				}
			}
		};

		runTests( new TestRegPrecise().Tests );
		// runTests( new TestNcbiBlastHandler().Tests );
	}

}
