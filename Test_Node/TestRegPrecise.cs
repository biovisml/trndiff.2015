// TestRegPrecise.cs
//

using System;
using System.Collections.Generic;
using RegPrecise;
using SystemQut;
using NodeApi.IO;
using System.Serialization;
using SystemQut.ServiceModel;
using Console = System.Console;

namespace Test_Node {
	public class TestRegPrecise {
		public Action[] Tests;

		public TestRegPrecise () {
			Tests = new Action[] {
				GetRegulonWithExtendedProperties,
				GetRegulogWithExtendedProperties,
				GetRegulatorWithExtendedProperties,
				GetSiteWithExtendedProperties,
				GetGeneFromJSON,
				GetGeneWithExtendedProperties,
				GetGenomeWithExtendedProperties,
				GetGenomeStatsWithExtendedProperties,
				GetRegulogFromJSON,
				GetRegulogFromParallelArrays,
				GetRegulogFromService,
				GetRegulonFromJSON,
				GetRegulonFromParallelArrays,
				GetRegulonFromService,
				GetSiteFromJSON,
				ListGenesInRegulog,
				ListGenesInRegulon,
				ListGenomeStats,
				ListRegulatorsInRegulog,
				ListRegulatorsInRegulon,
				ListRegulogs,
				ListRegulonsInGenome,
				ListRegulonsInRegulog,
				ListSitesInRegulog,
				ListSitesInRegulon,
				SearchRegulonsByGene,
				SearchRegulonsByRegulator,
				SerializeGenesToCsv,
				SerializeRegulatorsToCsv,
				SerializeRegulogsToCsv,
				SerializeRegulonsToCsv,
				SerializeSitesToCsv,
				TestListGenomes,
				TestListRegulogCollections,
			};
		}

		IRegPreciseService service = new RegPreciseService();

		#region Helper methods
		/// <summary> Returns the contents of a file as a string.
		/// </summary>
		/// <param name="fileName">The path (relative or absolute) of the file.</param>
		/// <returns>Returns the content of the file as a string.</returns>

		static string ReadAllText ( string fileName ) {
			Buffer buffer = FileSystem.ReadFileSync( fileName );
			return buffer.ToString();
		}

		/// <summary> Parses the content of a file from json into an array of JavaScript Objects
		/// </summary>
		/// <param name="json"></param>
		/// <param name="topLevelObject"></param>
		/// <returns></returns>

		static JSObject[] GetRecords ( string json, string topLevelObject ) {
			object result = ( (JSObject) Json.Parse( json ) )[topLevelObject];

			return result is Array ? (JSObject[]) result : new JSObject[] { (JSObject) result };
		}

		/// <summary> Gets the contents of a file as an array of javascript objects.
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>

		static JSObject[] ReadRecords ( string fileName ) {
			string json = ReadAllText( fileName );
			RegExp pattern = new RegExp( "[\"'](\\w+)[\"']" );
			string topLevelObject = pattern.Exec( json )[1];
			return GetRecords( json, topLevelObject );
		}

		private bool IsReallyNumber ( Number x ) {
			return x == x + 0;
		}
		#endregion

		#region Genomes
		void TestListGenomes () {
			string T = "TestListGenomes";
			service.ListGenomes( delegate( ServiceResponse<Genome[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Genome[] expected = ReadRecords( "Resources/genomes.json" ).Map<JSObject, Genome>( Genome.Parse );
				Genome[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<Genome>( Genome.OrderById );
				actual.Sort<Genome>( Genome.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					// Assert.IsTrue( IsReallyNumber( expected[i].GenomeId ), T + ": valid genome Id" );
					Assert.IsTrue( IsReallyNumber( actual[i].GenomeId ), T + ": valid genome Id" );
					Assert.IsTrue( IsReallyNumber( actual[i].TaxonomyId ), T + ": valid taxonomy Id" );
					Assert.IsTrue( actual[i].Name.Length > 0, T + ": valid name" );
					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": genomes identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		void GetGenomeWithExtendedProperties () {
			string T = "GetGenomeWithExtendedProperties";

			Genome expected = new Genome( 601, "Acetobacter pasteurianus IFO 3283-01", 634452 );
			expected["extendedProperty1"] = "To be or not to be...";
			expected["extendedProperty2"] = "That is the question.";

			string json = @"{
				""genomeId"":""601"",
				""name"":""Acetobacter pasteurianus IFO 3283-01"",
				""taxonomyId"":""634452"",
				""extendedProperty1"":""To be or not to be..."",
				""extendedProperty2"":""That is the question.""
			}";

			JSObject obj = (JSObject) Json.Parse( json );
			Genome actual = new Genome( obj );

			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": genomes identical" );

			Console.Log( T + ": Passed!" );
		}

		void GetGenomeStatsWithExtendedProperties () {
			string T = "GetGenomeStatsWithExtendedProperties";

			GenomeStats expected = new GenomeStats( 601, "Acetobacter pasteurianus IFO 3283-01", 634452, 6, 16, 0, 0 );
			expected["extendedProperty1"] = "To be or not to be...";
			expected["extendedProperty2"] = "That is the question.";

			string json = @"{
				""genomeId"":""601"",
				""name"":""Acetobacter pasteurianus IFO 3283-01"",
				""rnaRegulonCount"":""0"",
				""rnaSiteCount"":""0"",
				""taxonomyId"":""634452"",
				""tfRegulonCount"":""6"",
				""tfSiteCount"":""16"",
				""extendedProperty1"":""To be or not to be..."",
				""extendedProperty2"":""That is the question.""
			}";

			JSObject obj = (JSObject) Json.Parse( json );
			GenomeStats actual = new GenomeStats( obj );

			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": genomes identical" );

			Console.Log( T + ": Passed!" );
		}
		#endregion

		#region RegulogCollections
		void TestListRegulogCollections () {
			string T = "TestListRegulogCollections";
			service.ListRegulogCollections( RegulogCollectionType.TaxGroup, delegate( ServiceResponse<RegulogCollection[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				RegulogCollection[] expected = ReadRecords( "Resources/regulogCollections.json" ).Map<JSObject, RegulogCollection>( RegulogCollection.Parse );
				RegulogCollection[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<RegulogCollection>( RegulogCollection.OrderById );
				actual.Sort<RegulogCollection>( RegulogCollection.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].CollectionId ), T + ": valid regulog Id" );
					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		#endregion

		#region Sites
		void GetSiteFromJSON () {
			string T = "GetSiteFromJSON";

			Site expected = new Site(
				 "SO_0443",
				199635,
				-94,
				6423,
				6.9617,
				"ACCTTGGAGTAGGCTCCAAGGT"
			);

			Site actual = ReadRecords( "Resources/sitesByRegulon.json" ).Map<JSObject, Site>( Site.Parse )[0];

			Assert.IsTrue( IsReallyNumber( actual.GeneVIMSSId ), T + ": valid GeneVIMSSId" );
			Assert.IsTrue( IsReallyNumber( actual.Position ), T + ": valid Position" );
			Assert.IsTrue( IsReallyNumber( actual.RegulonId ), T + ": valid RegulonId" );
			Assert.IsTrue( IsReallyNumber( actual.Score ), T + ": valid Score" );
			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": objects identical" );

			Console.Log( T + ": Passed!" );
		}

		void GetSiteWithExtendedProperties () {
			string T = "GetSiteWithExtendedProperties";

			Site expected = new Site(
				 "SO_0443",
				199635,
				-94,
				6423,
				6.9617,
				"ACCTTGGAGTAGGCTCCAAGGT"
			);
			expected["extendedProperty1"] = "To be or not to be...";
			expected["extendedProperty2"] = "That is the question.";

			string json = @"{
				""geneLocusTag"":""SO_0443"",
				""geneVIMSSId"":""199635"",
				""position"":""-94"",
				""regulonId"":""6423"",
				""score"":""6.9617"",
				""sequence"":""ACCTTGGAGTAGGCTCCAAGGT"",
				""extendedProperty1"":""To be or not to be..."",
				""extendedProperty2"":""That is the question.""
			}";

			Site actual = Site.Parse( (JSObject) Json.Parse( json ) );

			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": objects identical" );

			Console.Log( T + ": Passed!" );
		}

		void ListSitesInRegulon () {
			string T = "ListSitesInRegulon";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulonId=6423'	
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulogId=647'

			service.ListSitesInRegulon( 6423, delegate( ServiceResponse<Site[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Site[] expected = ReadRecords( "Resources/sitesByRegulon.json" ).Map<JSObject, Site>( Site.Parse );
				Site[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<RegulogCollection>( RegulogCollection.OrderById );
				actual.Sort<RegulogCollection>( RegulogCollection.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].GeneVIMSSId ), T + ": valid GeneVIMSSId" );
					Assert.IsTrue( IsReallyNumber( actual[i].Position ), T + ": valid Position" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
					Assert.IsTrue( IsReallyNumber( actual[i].Score ), T + ": valid Score" );
					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": objects identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		void ListSitesInRegulog () {
			string T = "ListSitesInRegulog";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulonId=6423'	
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulogId=647'

			service.ListSitesInRegulog( 647, delegate( ServiceResponse<Site[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Site[] expected = ReadRecords( "Resources/sitesByRegulog.json" ).Map<JSObject, Site>( Site.Parse );
				Site[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<RegulogCollection>( RegulogCollection.OrderById );
				actual.Sort<RegulogCollection>( RegulogCollection.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].GeneVIMSSId ), T + ": valid GeneVIMSSId" );
					Assert.IsTrue( IsReallyNumber( actual[i].Position ), T + ": valid Position" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
					Assert.IsTrue( IsReallyNumber( actual[i].Score ), T + ": valid Score" );
					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": objects identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		void SerializeSitesToCsv () {
			string T = "SerializeSitesToCsv";

			Site[] expected = ReadRecords( "Resources/sitesByRegulog.json" ).Map<JSObject, Site>( Site.Parse );
			string serialized = Objects.SerializeTable( expected );

			Site[] actual = Objects.DeserializeTable( serialized ).Map<JSObject, Site>( Site.Parse );

			Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

			expected.Sort<Site>( Site.OrderByLocusTag );
			actual.Sort<Site>( Site.OrderByLocusTag );

			for ( int i = 0; i < expected.Length; i++ ) {
				Assert.IsTrue( IsReallyNumber( actual[i].GeneVIMSSId ), T + ": valid GeneVIMSSId" );
				Assert.IsTrue( IsReallyNumber( actual[i].Position ), T + ": valid Position" );
				Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
				Assert.IsTrue( IsReallyNumber( actual[i].Score ), T + ": valid Score" );
				Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": objects identical" );
			}

			Console.Log( T + ": Passed!" );
		}
		#endregion

		#region Regulogs
		void ListRegulogs () {
			string T = "TestListRegulogs";
			service.ListRegulogs( RegulogCollectionType.TaxGroup, 1, delegate( ServiceResponse<Regulog[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Regulog[] expected = ReadRecords( "Resources/regulogs.json" ).Map<JSObject, Regulog>( Regulog.Parse );
				Regulog[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<Regulog>( Regulog.OrderById );
				actual.Sort<Regulog>( Regulog.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					// Assert.IsTrue( IsReallyNumber( expected[i].GenomeId ), T + ": valid genome Id" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulogId ), T + ": valid regulog Id" );
					Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		void SerializeRegulogsToCsv () {
			string T = "SerializeRegulogsToCsv";

			Regulog[] expected = ReadRecords( "Resources/regulogs.json" ).Map<JSObject, Regulog>( Regulog.Parse );

			string serialized = Objects.SerializeTable( expected );

			JSObject[] deserialized = Objects.DeserializeTable( serialized );
			Regulog[] actual = deserialized.Map<JSObject, Regulog>( Regulog.Parse );

			Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

			expected.Sort<Regulog>( Regulog.OrderById );
			actual.Sort<Regulog>( Regulog.OrderById );

			for ( int i = 0; i < expected.Length; i++ ) {
				// Assert.IsTrue( IsReallyNumber( expected[i].GenomeId ), T + ": valid genome Id" );
				Assert.IsTrue( IsReallyNumber( actual[i].RegulogId ), T + ": valid regulog Id" );
				Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
			}

			Console.Log( T + ": Passed!" );
		}

		void GetRegulogFromJSON () {
			string T = "GetRegulogFromJSON";
			int regulogId = 621;

			Regulog expected = new Regulog();
			expected.Effector = "Sucrose";
			expected.Pathway = "Sucrose utilization";
			expected.RegulationType = RegulationTypes.TranscriptionFactor;
			expected.RegulatorFamily = "LacI";
			expected.RegulatorName = "ScrR";
			expected.RegulogId = 621;
			expected.TaxonName = "Shewanellaceae";

			Regulog actual = Regulog.Parse( (JSObject) Json.Parse( ReadAllText( "Resources/regulog" + regulogId + ".json" ) ) );

			Assert.NumbersEqual( regulogId, actual.RegulogId, T + ": correct regulog Id" );
			Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulogs identical" );

			Console.Log( T + ": Passed!" );
		}

		void GetRegulogWithExtendedProperties () {
			string T = "GetRegulogWithExtendedProperties";
			int regulogId = 621;

			Regulog expected = new Regulog();
			expected.Effector = "Sucrose";
			expected.Pathway = "Sucrose utilization";
			expected.RegulationType = RegulationTypes.TranscriptionFactor;
			expected.RegulatorFamily = "LacI";
			expected.RegulatorName = "ScrR";
			expected.RegulogId = 621;
			expected.TaxonName = "Shewanellaceae";
			expected["extendedProperty1"] = "To be or not to be...";
			expected["extendedProperty2"] = "That is the question.";

			string json = @"{
				""effector"":""Sucrose"",
				""pathway"":""Sucrose utilization"",
				""regulationType"":""TF"",
				""regulatorFamily"":""LacI"",
				""regulatorName"":""ScrR"",
				""regulogId"":""621"",
				""taxonName"":""Shewanellaceae"",
				""extendedProperty1"":""To be or not to be..."",
				""extendedProperty2"":""That is the question.""
			}";

			Regulog actual = Regulog.Parse( (JSObject) Json.Parse( json ) );

			Assert.NumbersEqual( regulogId, actual.RegulogId, T + ": correct regulog Id" );
			Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulogs identical" );

			Console.Log( T + ": Passed!" );
		}

		void GetRegulogFromService () {
			string T = "GetRegulogFromService";
			int regulogId = 621;

			service.GetRegulog( regulogId, delegate( ServiceResponse<Regulog> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Regulog expected = new Regulog();
				expected.Effector = "Sucrose";
				expected.Pathway = "Sucrose utilization";
				expected.RegulationType = RegulationTypes.TranscriptionFactor;
				expected.RegulatorFamily = "LacI";
				expected.RegulatorName = "ScrR";
				expected.RegulogId = 621;
				expected.TaxonName = "Shewanellaceae";

				Regulog actual = response.Content;

				Assert.NumbersEqual( regulogId, actual.RegulogId, T + ": correct regulog Id" );
				Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
				Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulogs identical" );

				Console.Log( T + ": Passed!" );
			} );
		}

		void GetRegulogFromParallelArrays () {
			string T = "GetRegulogFromParallelArrays";
			int regulogId = 621;

			Regulog expected = new Regulog();
			expected.Effector = "Sucrose";
			expected.Pathway = "Sucrose utilization";
			expected.RegulationType = RegulationTypes.TranscriptionFactor;
			expected.RegulatorFamily = "LacI";
			expected.RegulatorName = "ScrR";
			expected.RegulogId = 621;
			expected.TaxonName = "Shewanellaceae";

			string[] keys = { 
				"effector",
				"pathway",
				"regulationType",
				"regulatorFamily",
				"regulatorName",
				"regulogId",
				"taxonName",
			};

			string[] values = {
			"Sucrose",
			"Sucrose utilization",
			"TF",
			"LacI",
			"ScrR",
			"621",
			"Shewanellaceae",
			};

			Regulog actual = new Regulog( keys, values );

			Assert.NumbersEqual( regulogId, actual.RegulogId, T + ": correct regulog Id" );
			Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulogs identical" );

			Console.Log( T + ": Passed!" );
		}
		#endregion

		#region Regulons
		void GetRegulonFromJSON () {
			string T = "GetRegulonFromJSON";
			int regulonId = 6423;

			Regulon expected = new Regulon();
			expected.Effector = "Zinc ion, (Zn2+); Cadmium, ion (Cd2+)";
			expected.GenomeId = 1;
			expected.GenomeName = "Shewanella oneidensis MR-1";
			expected.Pathway = "Zinc resistance; Cadmium resistance";
			expected.RegulationType = RegulationTypes.TranscriptionFactor;
			expected.RegulatorFamily = "MerR";
			expected.RegulatorName = "ZntR";
			expected.RegulogId = 647;
			expected.RegulonId = 6423;

			Regulon actual = Regulon.Parse( (JSObject) Json.Parse( ReadAllText( "Resources/regulon" + regulonId + ".json" ) ) );

			Assert.NumbersEqual( regulonId, actual.RegulonId, T + ": correct regulon Id" );

			Assert.IsTrue( IsReallyNumber( actual.RegulonId ), T + ": numeric regulon Id" );
			Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
			Assert.IsTrue( IsReallyNumber( actual.GenomeId ), T + ": numeric genome Id" );

			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulons identical" );

			Console.Log( T + ": Passed!" );
		}

		void GetRegulonWithExtendedProperties () {
			string T = "GetRegulonWithExtendedProperties";
			int regulonId = 6423;

			Regulon expected = new Regulon();
			expected.Effector = "Zinc ion, (Zn2+); Cadmium, ion (Cd2+)";
			expected.GenomeId = 1;
			expected.GenomeName = "Shewanella oneidensis MR-1";
			expected.Pathway = "Zinc resistance; Cadmium resistance";
			expected.RegulationType = RegulationTypes.TranscriptionFactor;
			expected.RegulatorFamily = "MerR";
			expected.RegulatorName = "ZntR";
			expected.RegulogId = 647;
			expected.RegulonId = 6423;
			expected["extendedProperty1"] = "To be or not to be...";
			expected["extendedProperty2"] = "That is the question.";

			string json = @"{
				""effector"":""Zinc ion, (Zn2+); Cadmium, ion (Cd2+)"",
				""genomeId"":""1"",
				""genomeName"":""Shewanella oneidensis MR-1"",
				""pathway"":""Zinc resistance; Cadmium resistance"",
				""regulationType"":""TF"",
				""regulatorFamily"":""MerR"",
				""regulatorName"":""ZntR"",
				""regulogId"":""647"",
				""regulonId"":""6423"",
				""extendedProperty1"":""To be or not to be..."",
				""extendedProperty2"":""That is the question.""
			}";

			Regulon actual = Regulon.Parse( (JSObject) Json.Parse( json ) );

			Assert.NumbersEqual( regulonId, actual.RegulonId, T + ": correct regulon Id" );

			Assert.IsTrue( IsReallyNumber( actual.RegulonId ), T + ": numeric regulon Id" );
			Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
			Assert.IsTrue( IsReallyNumber( actual.GenomeId ), T + ": numeric genome Id" );

			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulons identical" );

			Console.Log( T + ": Passed!" );
		}

		void GetRegulonFromService () {
			string T = "GetRegulonFromService";
			int regulonId = 6423;

			service.GetRegulon( regulonId, delegate( ServiceResponse<Regulon> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Regulon expected = new Regulon();
				expected.Effector = "Zinc ion, (Zn2+); Cadmium, ion (Cd2+)";
				expected.GenomeId = 1;
				expected.GenomeName = "Shewanella oneidensis MR-1";
				expected.Pathway = "Zinc resistance; Cadmium resistance";
				expected.RegulationType = RegulationTypes.TranscriptionFactor;
				expected.RegulatorFamily = "MerR";
				expected.RegulatorName = "ZntR";
				expected.RegulogId = 647;
				expected.RegulonId = 6423;

				Regulon actual = response.Content;

				Assert.NumbersEqual( regulonId, actual.RegulonId, T + ": correct regulon Id" );

				Assert.IsTrue( IsReallyNumber( actual.RegulonId ), T + ": numeric regulon Id" );
				Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
				Assert.IsTrue( IsReallyNumber( actual.GenomeId ), T + ": numeric genome Id" );

				Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulons identical" );

				Console.Log( T + ": Passed!" );
			} );
		}

		void GetRegulonFromParallelArrays () {
			string T = "ConstructRegulonFromParallelArrays";

			string[] keys = { "effector", "genomeId", "genomeName", "pathway", "regulationType", "regulatorFamily", "regulatorName", "regulogId", "regulonId" };
			string[] values = { "Zinc ion, (Zn2+); Cadmium, ion (Cd2+)", "1", "Shewanella oneidensis MR-1", "Zinc resistance; Cadmium resistance", "TF", "MerR", "ZntR", "647", "6423" };
			Regulon actual = new Regulon( keys, values );

			Regulon expected = new Regulon();
			expected.Effector = "Zinc ion, (Zn2+); Cadmium, ion (Cd2+)";
			expected.GenomeId = 1;
			expected.GenomeName = "Shewanella oneidensis MR-1";
			expected.Pathway = "Zinc resistance; Cadmium resistance";
			expected.RegulationType = RegulationTypes.TranscriptionFactor;
			expected.RegulatorFamily = "MerR";
			expected.RegulatorName = "ZntR";
			expected.RegulogId = 647;
			expected.RegulonId = 6423;

			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulons identical" );

			Console.Log( T + ": Passed!" );
		}

		void SerializeRegulonsToCsv () {
			string T = "SerializeRegulonsToCsv";

			Regulon[] expected = ReadRecords( "Resources/regulonsInGenome1.json" ).Map<JSObject, Regulon>( Regulon.Parse );

			string serialized = Objects.SerializeTable( expected );

			JSObject[] deserialized = Objects.DeserializeTable( serialized );
			Regulon[] actual = deserialized.Map<JSObject, Regulon>( Regulon.Parse );

			Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

			expected.Sort<Regulon>( Regulon.OrderById );
			actual.Sort<Regulon>( Regulon.OrderById );

			for ( int i = 0; i < expected.Length; i++ ) {
				// Assert.IsTrue( IsReallyNumber( expected[i].GenomeId ), T + ": valid genome Id" );
				Assert.IsTrue( IsReallyNumber( actual[i].RegulogId ), T + ": valid regulog Id" );
				Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
			}

			Console.Log( T + ": Passed!" );
		}

		void ListRegulonsInGenome () {
			string T = "ListRegulonsInGenome";
			int genomeId = 1;

			service.ListRegulonsInGenome( genomeId, delegate( ServiceResponse<Regulon[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Regulon[] expected = ReadRecords( "Resources/regulonsInGenome" + genomeId + ".json" ).Map<JSObject, Regulon>( Regulon.Parse );
				Regulon[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<Regulon>( Regulon.OrderById );
				actual.Sort<Regulon>( Regulon.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.NumbersEqual( genomeId, actual[i].GenomeId, T + ": correct regulon Id" );

					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": numeric regulon Id" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulogId ), T + ": numeric regulog Id" );
					Assert.IsTrue( IsReallyNumber( actual[i].GenomeId ), T + ": numeric genome Id" );

					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulons identical" );
				}
				Console.Log( T + ": Passed!" );
			} );
		}

		void ListRegulonsInRegulog () {
			string T = "ListRegulonsInRegulog";
			int regulogId = 621;

			service.ListRegulonsInRegulog( regulogId, delegate( ServiceResponse<Regulon[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Regulon[] expected = ReadRecords( "Resources/regulonsInRegulog" + regulogId + ".json" ).Map<JSObject, Regulon>( Regulon.Parse );
				Regulon[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<Regulon>( Regulon.OrderById );
				actual.Sort<Regulon>( Regulon.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.NumbersEqual( regulogId, actual[i].RegulogId, T + ": correct regulon Id" );

					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": numeric regulon Id" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulogId ), T + ": numeric regulog Id" );
					Assert.IsTrue( IsReallyNumber( actual[i].GenomeId ), T + ": numeric genome Id" );

					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulons identical" );
				}
				Console.Log( T + ": Passed!" );
			} );
		}
		#endregion

		#region ListRegulators
		void GetRegulatorFromJSON () {
			string T = "GetRegulatorFromJSON";
			int regulonId = 6423;

			Regulator expected = new Regulator();
			expected.LocusTag = "SO_0443";
			expected.Name = "ZntR";
			expected.RegulatorFamily = "MerR";
			expected.RegulonId = 6423;
			expected.VimssId = 199635;

			Regulator actual = ReadRecords( "Resources/regulatorsInRegulon" + regulonId + ".json" ).Map<JSObject, Regulator>( Regulator.Parse )[0];

			Assert.NumbersEqual( regulonId, actual.RegulonId, T + ": incorrect regulon Id" );
			Assert.IsTrue( IsReallyNumber( actual.VimssId ), T + ": invalid numeric Vimssid" );
			Assert.IsTrue( IsReallyNumber( actual.RegulonId ), T + ": invalid numeric genome Id" );
			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulators not identical" );

			Console.Log( T + ": Passed!" );
		}

		void ListRegulatorsInRegulon () {
			string T = "ListRegulatorsInRegulon";
			int regulonId = 6423;

			service.ListRegulatorsInRegulon( regulonId, delegate( ServiceResponse<Regulator[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error not null" );

				Regulator[] expected = ReadRecords( "Resources/regulatorsInRegulon" + regulonId + ".json" ).Map<JSObject, Regulator>( Regulator.Parse );
				Regulator[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length not equal" );

				expected.Sort<Regulator>( Regulator.OrderById );
				actual.Sort<Regulator>( Regulator.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.NumbersEqual( regulonId, actual[i].RegulonId, T + ": incorrect regulon Id" );

					Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid numeric Vimssid" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid numeric genome Id" );

					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulators not identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		void ListRegulatorsInRegulog () {
			string T = "ListRegulatorsInRegulog";
			int regulogId = 647;

			service.ListRegulatorsInRegulog( regulogId, delegate( ServiceResponse<Regulator[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Regulator[] expected = ReadRecords( "Resources/regulatorsInRegulog" + regulogId + ".json" ).Map<JSObject, Regulator>( Regulator.Parse );
				Regulator[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length not equal" );

				expected.Sort<Regulator>( Regulator.OrderById );
				actual.Sort<Regulator>( Regulator.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid numeric vimss id" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid numeric regulon Id" );

					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulators not identical" );
				}
				Console.Log( T + ": Passed!" );
			} );
		}

		void SerializeRegulatorsToCsv () {
			string T = "SerializeRegulatorsToCsv";

			Regulator[] expected = ReadRecords( "Resources/regulatorsInRegulog647.json" ).Map<JSObject, Regulator>( Regulator.Parse );
			string serialized = Objects.SerializeTable( expected );

			Regulator[] actual = Objects.DeserializeTable( serialized ).Map<JSObject, Regulator>( Regulator.Parse );

			Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

			expected.Sort<Regulator>( Regulator.OrderById );
			actual.Sort<Regulator>( Regulator.OrderById );

			for ( int i = 0; i < expected.Length; i++ ) {
				Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
				Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": valid VimssId" );
				Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
				Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": objects identical" );
			}

			Console.Log( T + ": Passed!" );
		}

		void GetRegulatorWithExtendedProperties () {
			string T = "GetRegulatorWithExtendedProperties";

			Regulator expected = new Regulator( 6423, "ZntR", "SO_0443", 199635, "MerR" );
			expected["extendedProperty1"] = "To be or not to be...";
			expected["extendedProperty2"] = "That is the question.";

			string json = @"{
				""locusTag"":""SO_0443"",
				""name"":""ZntR"",
				""regulatorFamily"":""MerR"",
				""regulonId"":""6423"",
				""vimssId"":""199635"",
				""extendedProperty1"":""To be or not to be..."",
				""extendedProperty2"":""That is the question.""
			}";

			Regulator actual = Regulator.Parse( (JSObject) Json.Parse( json ) );

			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulons identical" );

			Console.Log( T + ": Passed!" );
		}

		#endregion

		#region Genes
		void GetGeneFromJSON () {
			string T = "GetGeneFromJSON";
			//Gene expected = new Gene( 6423, "zntR", "SO_0443", 199635, "zinc and cadmium (II) responsive transcriptional activator", false, string.Empty, string.Empty);
            Gene expected = new Gene( 6423, "zntR", "SO_0443", 199635, "zinc and cadmium (II) responsive transcriptional activator", string.Empty, string.Empty);
			Gene actual = ReadRecords( "Resources/genesInRegulon6423.json" ).Map<JSObject, Gene>( Gene.Parse )[0];
			Assert.IsTrue( IsReallyNumber( actual.VimssId ), T + ": invalid VimssId" );
			Assert.IsTrue( IsReallyNumber( actual.RegulonId ), T + ": invalid RegulonId" );
			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": objects not equal" );

			Console.Log( T + ": Passed!" );
		}

		void ListGenesInRegulon () {
			string T = "ListGenesInRegulon";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulonId=6423'	
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulogId=647'

			int regulonId = 6423;
			service.ListGenesInRegulon( regulonId, delegate( ServiceResponse<Gene[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error != null" );

				Gene[] expected = ReadRecords( "Resources/genesInRegulon6423.json" ).Map<JSObject, Gene>( Gene.Parse );
				Gene[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length unequal" );

				expected.Sort<Gene>( Gene.OrderById );
				actual.Sort<Gene>( Gene.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.NumbersEqual( regulonId, actual[i].RegulonId, T + ": incorrect regulon id" );
					Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid VimssId" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid RegulonId" );
					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": objects not equal" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		void ListGenesInRegulog () {
			string T = "ListGenesInRegulog";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/genes?regulonId=6423'	
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/genes?regulogId=647'

			service.ListGenesInRegulog( 647, delegate( ServiceResponse<Gene[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error != null" );

				Gene[] expected = ReadRecords( "Resources/genesInRegulog647.json" ).Map<JSObject, Gene>( Gene.Parse );
				Gene[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length unequal" );

				expected.Sort<Gene>( Gene.OrderById );
				actual.Sort<Gene>( Gene.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid VimssId" );
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid RegulonId" );
					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": objects not equal" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		void SerializeGenesToCsv () {
			string T = "SerializeGenesToCsv";

			Gene[] expected = ReadRecords( "Resources/genesInRegulog647.json" ).Map<JSObject, Gene>( Gene.Parse );
			string serialized = Objects.SerializeTable( expected );

			Gene[] actual = Objects.DeserializeTable( serialized ).Map<JSObject, Gene>( Gene.Parse );

			Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

			expected.Sort<Gene>( Gene.OrderById );
			actual.Sort<Gene>( Gene.OrderById );

			for ( int i = 0; i < expected.Length; i++ ) {
				Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid VimssId" );
				Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid RegulonId" );
				Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": objects not equal" );
			}

			Console.Log( T + ": Passed!" );
		}

		void GetGeneWithExtendedProperties () {
			string T = "GetGeneWithExtendedProperties";

			//Gene expected = new Gene( 6423, "zntR", "SO_0443", 199635, "zinc and cadmium (II) responsive transcriptional activator", false, string.Empty, string.Empty);
            Gene expected = new Gene( 6423, "zntR", "SO_0443", 199635, "zinc and cadmium (II) responsive transcriptional activator", string.Empty, string.Empty);
			expected["extendedProperty1"] = "To be or not to be...";
			expected["extendedProperty2"] = "That is the question.";

			string json = @"{
				""function"":""zinc and cadmium (II) responsive transcriptional activator"",
				""locusTag"":""SO_0443"",
				""name"":""zntR"",
				""regulonId"":""6423"",
				""vimssId"":""199635"",
				""extendedProperty1"":""To be or not to be..."",
				""extendedProperty2"":""That is the question.""
			}";

			JSObject obj = (JSObject) Json.Parse( json );
			Gene actual = Gene.Parse( obj );
			Assert.IsTrue( Objects.Identical( expected, actual ), T + ": objects not equal" );

			Console.Log( T + ": Passed!" );
		}
		#endregion

		#region GenomeStats
		void ListGenomeStats () {
			string T = "ListGenomeStats";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/genomeStats'

			service.ListGenomeStats( delegate( ServiceResponse<GenomeStats[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error != null" );

				GenomeStats[] expected = ReadRecords( "Resources/genomeStats.json" ).Map<JSObject, GenomeStats>( GenomeStats.Parse );
				GenomeStats[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length unequal" );

				expected.Sort<GenomeStats>( GenomeStats.OrderById );
				actual.Sort<GenomeStats>( GenomeStats.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].GenomeId ), T + ": invalid GenomeId" );
					Assert.IsTrue( IsReallyNumber( actual[i].TaxonomyId ), T + ": invalid TaxonomyId" );
					Assert.IsTrue( IsReallyNumber( actual[i].TfRegulonCount ), T + ": invalid TfRegulonCount" );
					Assert.IsTrue( IsReallyNumber( actual[i].TfSiteCount ), T + ": invalid TfSiteCount" );
					Assert.IsTrue( IsReallyNumber( actual[i].RnaRegulonCount ), T + ": invalid RnaRegulonCount" );
					Assert.IsTrue( IsReallyNumber( actual[i].RnaSiteCount ), T + ": invalid RnaSiteCount" );
					Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": objects not equal" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}
		#endregion

		#region SearchRegulogs
		void SearchRegulonsByRegulator () {
			string T = "SearchRegulonsByRegulator";
			service.SearchRegulons( ObjectTypes.Regulator, "argR", delegate( ServiceResponse<RegulonReference[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				RegulonReference[] expected = ReadRecords( "Resources/searchRegulons_Regulator_argR.json" ).Map<JSObject, RegulonReference>( RegulonReference.Parse );
				RegulonReference[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<RegulonReference>( RegulonReference.OrderById );
				actual.Sort<RegulonReference>( RegulonReference.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
					Assert.StringsEqual( "argR".ToLowerCase(), actual[i].RegulatorName.ToLowerCase(), T + ": correct RegulatorName" );
					Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}

		void SearchRegulonsByGene () {
			string T = "SearchRegulonsByGene";
			service.SearchRegulons( ObjectTypes.Gene, "SO_2706", delegate( ServiceResponse<RegulonReference[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				RegulonReference[] expected = ReadRecords( "Resources/searchRegulons_Gene_SO_2706.json" ).Map<JSObject, RegulonReference>( RegulonReference.Parse );
				RegulonReference[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<RegulonReference>( RegulonReference.OrderById );
				actual.Sort<RegulonReference>( RegulonReference.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
					Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}
		#endregion

		#region SearchExtRegulons
		void SearchExtRegulonsByRegulator () {
			string T = "SearchExtRegulonsByRegulator";

			int ncbiTaxonomyId = 882;
			string[] locusTags = { "DVU0043", "DVU0694", "DVU3234", "DVU3233", "DVU0910", "DVU3384", "DVU0863" };

			service.SearchExtRegulons( ncbiTaxonomyId, locusTags, delegate( ServiceResponse<RegulonReference[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				RegulonReference[] expected = ReadRecords( "Resources/searchRegulons_Regulator_argR.json" ).Map<JSObject, RegulonReference>( RegulonReference.Parse );
				RegulonReference[] actual = response.Content;

				Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

				expected.Sort<RegulonReference>( RegulonReference.OrderById );
				actual.Sort<RegulonReference>( RegulonReference.OrderById );

				for ( int i = 0; i < expected.Length; i++ ) {
					Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
					Assert.StringsEqual( "argR".ToLowerCase(), actual[i].RegulatorName.ToLowerCase(), T + ": correct RegulatorName" );
					Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
				}

				Console.Log( T + ": Passed!" );
			} );
		}
		#endregion
	}
}
