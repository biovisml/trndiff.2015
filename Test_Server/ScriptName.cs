﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System {

	/// <summary> Dummy attribute covering for a Script# attribute.
	/// </summary>

	public class ScriptNameAttribute: Attribute {
		public ScriptNameAttribute ( string name ) {
		}
	}
}
