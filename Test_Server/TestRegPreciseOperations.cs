﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegPrecise;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Runtime.Serialization;

namespace QUT.Bio.Test {

	/// <summary> Unit tests for RegPrecise REST web service wrapper.
	/// </summary>

	[TestClass]
	public class TestRegPreciseOperations {

		private const string RegPreciseBaseAddr = "http://regprecise.lbl.gov/Services/rest/";

		[DataContract]
		internal class GenomeList {
			[DataMember]
			public Genome [] genome = null;
		}

		/// <summary> Test the ListGenomes operation.
		/// </summary>

		[TestMethod]
		public void TestListGenomes () {
			string localUrl = TestServer.TrnDiffUrl("RegPrecise.svc/listGenomes");
			WebClient localClient = new WebClient();
			byte [] localJson = localClient.DownloadData( localUrl );

			DataContractJsonSerializer localSer = new DataContractJsonSerializer( typeof(Genome[]) );
			Genome[] actualGenomes = (Genome[]) localSer.ReadObject( new MemoryStream( localJson ) );
	
			OperationType operationType = OperationType.ListGenomes;
			string query = null;

			string remoteUrl = RegPreciseBaseAddr + operationType + ( query == null ? "" : "?" + query );
			WebClient remoteClient = new WebClient();
			byte [] remoteJson = remoteClient.DownloadData( remoteUrl );

			DataContractJsonSerializer ser = new DataContractJsonSerializer( typeof(GenomeList) );
			GenomeList expectedGenomes = (GenomeList) ser.ReadObject( new MemoryStream( remoteJson ) );
		}
	}
}
