﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System {

	/// <summary> Dummy attribute covering for a Script# attribute.
	/// </summary>

	class ScriptConstantsAttribute: Attribute {
		public ScriptConstantsAttribute () {
		}
		
		public bool UseNames { get; set; }
	}
}
