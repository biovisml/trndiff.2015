﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Net;

namespace QUT.Bio.Test {

	/// <summary> These tests require the TrnDiff node server to be running at 
	///		http://localhost:32207/
	/// </summary>

	[TestClass]
	public class TestServer {
		private const string TrnDiffServer = "http://localhost:32207/{0}";

		/// <summary> Gets th e url of an item in the TrnDiff test server.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>

		public static string TrnDiffUrl ( string path = "" ) {
			return string.Format( TrnDiffServer, path );
		}

		/// <summary> Tests to ensure that the RegPrecise test page is returned as a default value.
		/// </summary>

		[TestMethod, DeploymentItem( "Resources\\TestPage.html", "Resources" )]
		public void TestServerDeliversTestPage () {
			string expected = File.ReadAllText( "Resources\\TestPage.html" );
			Assert.IsTrue( expected.Length > 0 );

			WebClient client = new WebClient();
			string actual = client.DownloadString( TrnDiffUrl() );

			Assert.AreEqual( expected, actual );
		}

		/// <summary> Tests to ensure that the TrnDiff demo index page is returned as a default value.
		/// </summary>

		[TestMethod, DeploymentItem( "Resources\\index.html", "Resources" )]
		public void TestServerDeliversDemoIndex () {
			string expected = File.ReadAllText( "Resources\\index.html" );
			Assert.IsTrue( expected.Length > 0 );

			WebClient client = new WebClient();
			string actual = client.DownloadString( TrnDiffUrl( "Demo" ) );

			Assert.AreEqual( expected, actual );
		}

		/// <summary> Tests to ensure that the index page is returned when requested explicitly.
		/// </summary>

		[TestMethod, DeploymentItem( "Resources\\index.html", "Resources" )]
		public void TestServerDeliversDemoIndexExplicitly () {
			string expected = File.ReadAllText( "Resources\\index.html" );
			Assert.IsTrue( expected.Length > 0 );

			WebClient client = new WebClient();
			string actual = client.DownloadString( TrnDiffUrl( "Demo/index.html" ) );

			Assert.AreEqual( expected, actual );
		}

		/// <summary> Tests to ensure that the index page is returned when requested explicitly.
		/// </summary>

		[TestMethod, DeploymentItem( "Resources\\Yersinia_Fur.json", "Resources" )]
		public void TestServerDeliversNestedDataFile () {
			string expected = File.ReadAllText( "Resources\\Yersinia_Fur.json" );
			Assert.IsTrue( expected.Length > 0 );

			WebClient client = new WebClient();
			string actual = client.DownloadString( TrnDiffUrl( "Demo/data/Yersinia_Fur.json" ) );

			Assert.AreEqual( expected, actual );
		}

		/// <summary> Tests to ensure that HTTP404 occurs if non-existent page at top level of 
		///		demo site is requested.
		/// </summary>

		[TestMethod, ExpectedException( typeof( WebException ) )]

		public void TestServerDelivers404 () {
			WebClient client = new WebClient();
			
			try {
				client.DownloadString( TrnDiffUrl( "Demo/humbug.html" ) );
			}
			catch ( WebException ex ) {
				Assert.IsTrue( ex.Response is HttpWebResponse );
				Assert.AreEqual( HttpStatusCode.NotFound, ( ex.Response as HttpWebResponse).StatusCode );
				throw;
			}
		}

		/// <summary> Tests to ensure that HTTP404 occurs if page outside the directory tree is
		///		requested via relative path.
		/// </summary>

		[TestMethod, ExpectedException( typeof( WebException ) )]

		public void TestServerDelivers404_OutsideWebSite () {
			WebClient client = new WebClient();
			
			try {
				client.DownloadString( TrnDiffUrl( "../TrnDiffNode.js" ) );
			}
			catch ( WebException ex ) {
				Assert.IsTrue( ex.Response is HttpWebResponse );
				Assert.AreEqual( HttpStatusCode.NotFound, ( ex.Response as HttpWebResponse).StatusCode );
				throw;
			}
		}
	}
}
