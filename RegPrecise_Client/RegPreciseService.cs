﻿using System;
using System.Collections.Generic;

using System.Net;
using SystemQut.ServiceModel;
using System.Serialization;
using SystemQut;
using System.Collections;
using Enum = SystemQut.Enum;

namespace RegPrecise {
	
	/// <summary> Proxy for the RegPrecise web service, implementing the IRegPreciseService operation contract.
	/// </summary>

	public class RegPreciseService : IRegPreciseService {
		private readonly string serviceUrl;
		private  bool logNormalResponses;

		/// <summary> Constructor: initialises the proxy and stores the service URL.
		/// </summary>
		/// <param name="serviceUrl">
		///		The url of the service.
		/// </param>
		/// <param name="logNormalResponses">
		///		Controls ouput of detailed output for non-error responses from the server.
		///		If true, all received response text is logged to the console. If false,
		///		only error details are logged.
		/// </param>

		public RegPreciseService ( string serviceUrl, bool logNormalResponses ) {
			this.serviceUrl = serviceUrl;
			this.logNormalResponses = logNormalResponses;
		}

		/// <summary> Does a Http Post to the service to request an array of records.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="arguments"></param>
		/// <param name="processResults"></param>
		/// <param name="parseFunction"></param>

		private void DoRequest<T> (
			Dictionary<String, string> arguments,
			ResponseProcessor<T[]> processResults,
			ArrayItemMapCallback<JSObject, T> parseFunction
		) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						if ( logNormalResponses ) Console.Log( "Response received: " + req.ResponseText );

						ServiceResponse<JSObject[]> serviceResponse = (ServiceResponse<JSObject[]>) Json.Parse( req.ResponseText );

						T[] result = null;

						if ( Script.IsValue( serviceResponse ) && ( !Script.IsValue( serviceResponse.Error ) ) && serviceResponse.Content.Length > 0 ) {
							result = serviceResponse.Content.Map<JSObject, T>( parseFunction );
						}

						processResults( new ServiceResponse<T[]>( result, serviceResponse.Error ) );
					}
					else {
						Console.Log( "Error received: " + req.Status );

						processResults( new ServiceResponse<T[]>( null,
							arguments["operation"] + " failed. Http status = " + req.Status + " (" + req.StatusText + ")"
						) );
					}
				}
			};
			req.Open( HttpVerb.Post, serviceUrl );
			req.Send( Json.Stringify( arguments ) );
		}

		/// <summary> Does a Http Post to the service to request a single record.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="arguments"></param>
		/// <param name="processResults"></param>
		/// <param name="parseFunction"></param>

		private void DoRequestSingle<T> (
			Dictionary<String, object> arguments,
			ResponseProcessor<T> processResults,
			ArrayItemMapCallback<JSObject, T> parseFunction
		) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						if ( logNormalResponses ) Console.Log( "Response received: " + req.ResponseText );

						ServiceResponse<JSObject> serviceResponse = (ServiceResponse<JSObject>) Json.Parse( req.ResponseText );

						if ( Script.IsValue( serviceResponse ) && ( !Script.IsValue( serviceResponse.Error ) ) ) {
							T result = parseFunction( serviceResponse.Content );
							processResults( new ServiceResponse<T>( result, serviceResponse.Error ) );
						}
						else {
							processResults( (ServiceResponse<T>) (object) new ServiceResponse<object>( null, serviceResponse.Error ) );
						}
					}
					else {
						Console.Log( "Error received: " + req.Status );

						processResults( (ServiceResponse<T>) (object) new ServiceResponse<object>( null,
							arguments["operation"] + " failed. Http status = " + req.Status + " (" + req.StatusText + ")"
						) );
					}
				}
			};
			req.Open( HttpVerb.Post, serviceUrl );
			req.Send( Json.Stringify( arguments ) );
		}

		/// <summary> Proxy for the SearchExtRegulons web service operation.
		/// <para>
		///		WARNING!!! At the time of writing this operation seems to be broken at the
		///		LBL end. Nothing good comes of it.
		/// </para>
		/// </summary>
		/// <param name="ncbiTaxonomyId"> The NCBI taxonomy Id of the </param>
		/// <param name="locusTags"></param>
		/// <param name="processResults"></param>

		public void SearchExtRegulons ( int ncbiTaxonomyId, string[] locusTags, ResponseProcessor<RegulonReference[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "SearchExtRegulons",
				"ncbiTaxonomyId", ncbiTaxonomyId.ToString(),
				"locusTags", locusTags.ToString()
			);

			DoRequest( arguments, processResults, RegulonReference.Parse );
		}

		/// <summary> Proxy for the SearchRegulons web service operation.
		/// <para>
		///		Retrieves a list of regulon references by the name/locus tag of either regulator or target regulated genes. 
		/// </para>
		/// </summary>
		/// <param name="objectType"></param>
		/// <param name="objectName"></param>
		/// <param name="processResults"></param>

		public void SearchRegulons ( ObjectTypes objectType, string objectName, ResponseProcessor<RegulonReference[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "SearchRegulons",
				"objectType", objectType.ToString(),
				"objectName", objectName
			);

			DoRequest( arguments, processResults, RegulonReference.Parse );
		}

		/// <summary> Proxy for the ListGenomeStats web service operation.
		/// <para>
		///		Gets the general statistics on regulons and regulatory sites in all genomes.
		/// </para>
		/// </summary>
		/// <param name="processResults"></param>

		public void ListGenomeStats ( ResponseProcessor<GenomeStats[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListGenomeStats"
			);

			DoRequest( arguments, processResults, GenomeStats.Parse );
		}

		/// <summary> Proxy for the ListRegulatorsInRegulon web service operation.
		/// <para>
		///		Lists the regulators in a regulon.
		/// </para>
		/// </summary>
		/// <param name="regulonId"></param>
		/// <param name="processResults"></param>

		public void ListRegulatorsInRegulon ( int regulonId, ResponseProcessor<Regulator[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulatorsInRegulon",
				"regulonId", regulonId.ToString()
			);

			DoRequest( arguments, processResults, Regulator.Parse );
		}

		/// <summary> Proxy for the ListRegulatorsInRegulog web service operation.
		///	<para>
		///		Lists the regulators in a regulog.
		/// </para>
		/// </summary>
		/// <param name="regulogId"></param>
		/// <param name="processResults"></param>

		public void ListRegulatorsInRegulog ( int regulogId, ResponseProcessor<Regulator[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulatorsInRegulog",
				"regulogId", regulogId.ToString()
			);

			DoRequest( arguments, processResults, Regulator.Parse );
		}

		/// <summary> Gets the specified regulog descriptor.
		/// </summary>
		/// <param name="regulogId"></param>
		/// <param name="processResults"></param>

		public void GetRegulog ( int regulogId, ResponseProcessor<Regulog> processResults ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "GetRegulog",
				"regulogId", regulogId
			);

			DoRequestSingle( arguments, processResults, Regulog.Parse );
		}

		/// <summary> Gets the specified regulon descriptor.
		/// </summary>
		/// <param name="regulonId">The id of a regulon.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void GetRegulon ( int regulonId, ResponseProcessor<Regulon> processResults ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "GetRegulon",
				"regulonId", regulonId
			);

			DoRequestSingle( arguments, processResults, Regulon.Parse );
		}

		/// <summary> Get a list of regulog collections or the specified type.
		/// <para>
		///		Result is a list of regulog collections. Each regulog collection is provided with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>collectionType - type of regulog collection</item>
		/// <item>collectionId - identifier of collection</item>
		/// <item>name - collection name</item>
		/// <item>className - name of collection class </item>
		/// </list>
		/// </summary>
		/// <param name="collectionType"> The idneity of the required collection type. </param>
		/// <param name="processResults"> A callback that will process the results. </param>

		public void ListRegulogCollections ( RegulogCollectionType collectionType, ResponseProcessor<RegulogCollection[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulogCollections",
				"collectionType", collectionType.ToString()
			);

			DoRequest( arguments, processResults, RegulogCollection.Parse );
		}

		/// <summary> Retrieves a list of genomes that have at least one reconstructed regulon.
		/// <para>
		///		Result is a list of genomes with the following data:
		/// </para>		
		/// <list type="bullet">
		/// <item>genomeId - genome identifier</item>
		/// <item>name - genome name</item>
		/// <item>taxonomyId - NCBI taxonomy id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>
		
		public void ListGenomes ( ResponseProcessor<Genome[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>();
			arguments["operation"] = "ListGenomes";

			DoRequest( arguments, processResults, Genome.Parse );
		}

		/// <summary> Retrieves a list of regulogs that belongs to a specific collection
		///	<para>
		///		Requires type and identifier of a regulog collection 
		///	</para>	
		///	<para>
		///		Returns a list of regulogs. Each regulog is provided with the following data: 
		///	</para>	
		///	<list type="bullet">
		///	<item>
		///		•regulogId - identifier of regulog
		///	</item>
		///	<item>
		///		•regulatorName - name of regulator
		///	</item>
		///	<item>
		///		•regulatorFamily - family of regulator
		///	</item>
		///	<item>
		///		•regulationType - type of regulation: either TF (transcription factor) or RNA
		///	</item>
		///	<item>
		///		•taxonName - name of taxonomic group
		///	</item>
		///	<item>
		///		•effector - effector molecule or environmental signal of a regulator
		///	</item>
		///	<item>
		///		•pathway - metabolic pathway or biological process controlled by a regulator 
		///	</item>
		///	</list>
		/// </summary>

		public void ListRegulogs ( 
			RegulogCollectionType collectionType, 
			Number collectionId, 
			ResponseProcessor<Regulog[]> processResults 
		) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulogs",
				"collectionType", collectionType.ToString(),
				"collectionId", collectionId.ToString()
			);

			DoRequest( arguments, processResults, Regulog.Parse );
		}

		/// <summary> Retrieves a list of regulons belonging to either a particular regulog.
		/// </summary>
		/// <param name="regulogId"> 
		///		The numeric regulog Id. 
		///	</param>
		/// <param name="processResults"> 
		///		A delegate which will process the results.
		///	</param>

		public void ListRegulonsInRegulog ( 
			Number regulogId, 
			ResponseProcessor<Regulon[]> processResults 
		) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulonsInRegulog",
				"regulogId", regulogId.ToString()
			);

			DoRequest( arguments, processResults, Regulon.Parse );
		}

		/// <summary> Retrieves a list of regulons belonging to a particular genome.
		/// </summary>
		/// <param name="genomeId">
		///		The genome Id.
		/// </param>
		/// <param name="processResults"> 
		///		A delegate which will process the results.
		///	</param>

		public void ListRegulonsInGenome ( 
			Number genomeId, 
			ResponseProcessor<Regulon[]> processResults 
		) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulonsInGenome",
				"genomeId", genomeId.ToString()
			);

			DoRequest( arguments, processResults, Regulon.Parse );
		}

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListSitesInRegulon ( 
			Number regulonId, 
			ResponseProcessor<Site[]> processResults 
		) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListSitesInRegulon",
				"regulonId", regulonId.ToString()
			);

			DoRequest( arguments, processResults, Site.Parse );
		}

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListSitesInRegulog ( Number regulogId, ResponseProcessor<Site[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListSitesInRegulog",
				"regulogId", regulogId.ToString()
			);

			DoRequest( arguments, processResults, Site.Parse );
		}

		/// <summary> Gets the list of genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListGenesInRegulon ( Number regulonId, ResponseProcessor<Gene[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListGenesInRegulon",
				"regulonId", regulonId.ToString()
			);

			DoRequest( arguments, processResults, Gene.Parse );
		}

		/// <summary> Gets the list of genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListGenesInRegulog ( Number regulogId, ResponseProcessor<Gene[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListGenesInRegulog",
				"regulogId", regulogId.ToString()
			);

			DoRequest( arguments, processResults, Gene.Parse );
		}
	}
}
