﻿using System;
using System.Collections.Generic;

using System.Net;
using SystemQut.ServiceModel;
using System.Serialization;
using SystemQut;
using System.Collections;
using Enum = SystemQut.Enum;

namespace RegPrecise {
    /// <summary> Implementation of the IGeneOntologySQLService interface
	/// </summary>

    public class LocalMySQLService : ILocalMySQLService {
		private readonly string serviceUrl;
		private  bool logNormalResponses;

		/// <summary> Constructor: initialises the proxy and stores the service URL.
		/// </summary>
		/// <param name="serviceUrl">
		///		The url of the service.
		/// </param>
		/// <param name="logNormalResponses">
		///		Controls ouput of detailed output for non-error responses from the server.
		///		If true, all received response text is logged to the console. If false,
		///		only error details are logged.
		/// </param>

		public LocalMySQLService ( string serviceUrl, bool logNormalResponses ) {
			this.serviceUrl = serviceUrl;
			this.logNormalResponses = logNormalResponses;
		}



        /// <summary> Does a Http Post to the service to request an array of records.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="arguments"></param>
		/// <param name="processResults"></param>
		/// <param name="parseFunction"></param>

		private void DoRequest<T> (
			Dictionary<String, string> arguments,
			ResponseProcessor<T[]> processResults,
			ArrayItemMapCallback<JSObject, T> parseFunction
		) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						if ( logNormalResponses ) Console.Log( "Response received: " + req.ResponseText );

						ServiceResponse<JSObject[]> serviceResponse = (ServiceResponse<JSObject[]>) Json.Parse( req.ResponseText );

						T[] result = null;

						if ( Script.IsValue( serviceResponse ) && ( !Script.IsValue( serviceResponse.Error ) ) && serviceResponse.Content.Length > 0 ) {
							result = serviceResponse.Content.Map<JSObject, T>( parseFunction );
						}

						processResults( new ServiceResponse<T[]>( result, serviceResponse.Error ) );
					}
					else {
						Console.Log( "Error received: " + req.Status );

						processResults( new ServiceResponse<T[]>( null,
							arguments["operation"] + " failed. Http status = " + req.Status + " (" + req.StatusText + ")"
						) );
					}
				}
			};
			req.Open( HttpVerb.Post, serviceUrl );
			req.Send( Json.Stringify( arguments ) );
		}

		/// <summary> Does a Http Post to the service to request a single record.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="arguments"></param>
		/// <param name="processResults"></param>
		/// <param name="parseFunction"></param>

		private void DoRequestSingle<T> (
			Dictionary<String, object> arguments,
			ResponseProcessor<T> processResults,
			ArrayItemMapCallback<JSObject, T> parseFunction
		) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						if ( logNormalResponses ) Console.Log( "Response received: " + req.ResponseText );

						ServiceResponse<JSObject> serviceResponse = (ServiceResponse<JSObject>) Json.Parse( req.ResponseText );

						if ( Script.IsValue( serviceResponse ) && ( !Script.IsValue( serviceResponse.Error ) ) ) {
							T result = parseFunction( serviceResponse.Content );
							processResults( new ServiceResponse<T>( result, serviceResponse.Error ) );
						}
						else {
							processResults( (ServiceResponse<T>) (object) new ServiceResponse<object>( null, serviceResponse.Error ) );
						}
					}
					else {
						Console.Log( "Error received: " + req.Status );

						processResults( (ServiceResponse<T>) (object) new ServiceResponse<object>( null,
							arguments["operation"] + " failed. Http status = " + req.Status + " (" + req.StatusText + ")"
						) );
					}
				}
			};
			req.Open( HttpVerb.Post, serviceUrl );
			req.Send( Json.Stringify( arguments ) );
		}

        /// <summary>
        /// Performs an HTTP post to add record(s) to the database
		/// </summary>
		/// <param name="arguments"></param>
		/// <param name="processResults"></param>
        /// <returns>The number of added rows</returns>

		private void DoRequestAdd (
			Dictionary<String, object> arguments,
			ResponseProcessor<Number> processResults
		) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						if ( logNormalResponses ) Console.Log( "Response received: " + req.ResponseText );

						ServiceResponse<JSObject> serviceResponse = (ServiceResponse<JSObject>) Json.Parse( req.ResponseText );

						if ( Script.IsValue( serviceResponse ) && ( !Script.IsValue( serviceResponse.Error ) ) ) {
							Number result = Number.Parse ( serviceResponse.Content.ToString() );
							processResults( new ServiceResponse<Number>( result, serviceResponse.Error ) );
						}
						else {
							processResults( (ServiceResponse<Number>) (object) new ServiceResponse<object>( null, serviceResponse.Error ) );
						}
					}
					else {
						Console.Log( "Error received: " + req.Status );

						processResults( (ServiceResponse<Number>) (object) new ServiceResponse<object>( null,
							arguments["operation"] + " failed. Http status = " + req.Status + " (" + req.StatusText + ")"
						) );
					}
				}
			};
			req.Open( HttpVerb.Post, serviceUrl );
			req.Send( Json.Stringify( arguments ) );
        }

		/// <summary> Retrieves a list of all GO terms in the Gene Ontology
        /// database
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void GO_ListTerms (	ResponseProcessor<GOTerm[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>();
			arguments["operation"] = "GO_ListTerms";

			DoRequest( arguments, processResults, GOTerm.Parse );
		}

        /// <summary> Retrieves a list of all the synonyms of the specified
        /// GO term (from the database id)
		/// <para>
		///		Result is a list of term synonyms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>term_id</item>
        /// <item>term_synonym</item>
        /// <item>acc_synonym</item>
        /// <item>synonym_type_id</item>
        /// <item>synonym_category_id</item>
		/// </list>
		/// </summary>
		/// <param name="termId"></param>
		/// <param name="processResults"></param>

		public void GO_ListSynonymsOfTerm ( int termId, ResponseProcessor<GOTermSynonym[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "GO_ListSynonymsOfTerm",
				"termId", termId.ToString()
			);

			DoRequest( arguments, processResults, GOTermSynonym.Parse );
        }

        /// <summary> Retrieves a list of all the relationships that the
        /// specified GO term particpates in as a child
		/// <para>
		///		Result is a list of term relationships with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>relationship_type_id</item>
        /// <item>term1_id</item>
        /// <item>term2_id</item>
        /// <item>complete</item>
		/// </list>
		/// </summary>
		/// <param name="termId"></param>
		/// <param name="processResults"></param>

        public void GO_ListRelationshipsOfTermAsChild ( int termId, ResponseProcessor<GOTermRelationship[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "GO_ListRelationshipsOfTermAsChild",
				"termId", termId.ToString()
			);

			DoRequest( arguments, processResults, GOTermRelationship.Parse );
        }

		/// <summary> Retrieves the specified GO term
		/// <para>
		///		Result is a term with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
		/// </list>
		/// </summary>
        /// <param name="termId"></param>
		/// <param name="processResults"></param>

		public void GO_GetTerm ( int termId, ResponseProcessor<GOTerm> processResults ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "GO_GetTerm",
				"termId", termId
			);

			DoRequestSingle( arguments, processResults, GOTerm.Parse );
		}

        /// <summary> Retrieves a list of all GO terms in the Gene Ontology
        /// database, with all of their synonyms (if any)
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
        /// <item>term_synonyms</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void GO_ListTermsWithSynonyms (	ResponseProcessor<GOTerm[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>();
			arguments["operation"] = "GO_ListTermsWithSynonyms";

			DoRequest( arguments, processResults, GOTerm.Parse );
		}

        /// <summary> Retrieves a list of all GO terms in the Gene Ontology
        /// database, with all of the relationships where they are a child
        /// (if any)
		/// <para>
		///		Result is a list of terms with the following data:
		/// </para>
		/// <list type="bullet">
        /// <item>id</item>
        /// <item>name</item>
        /// <item>term_type</item>
        /// <item>acc</item>
        /// <item>is_obsolete</item>
        /// <item>is_root</item>
        /// <item>is_relation</item>
        /// <item>term_relationships</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void GO_ListTermsWithParents (	ResponseProcessor<GOTerm[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>();
			arguments["operation"] = "GO_ListTermsWithParents";

			DoRequest( arguments, processResults, GOTerm.Parse );
		}

        /// <summary> I have created a list of RegPrecise gene function terms
        /// matched up to the closest GO terms in the molecular function name
        /// space, and the (first) "top most parent" of that term. This
        /// retrives a list from that, with each gene function and their "top
        /// most parent"
		/// <para>
		///		Result is a JSObject dictionary with the gene function as the
        ///		key and the "category" as the value
        /// </para>
		/// <list type="bullet">
        /// <item>RP_geneFunction</item>
        /// <item>Top_most_GO_id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void GO_GetRegPreciseCategoryMatches (	ResponseProcessor<GORegPreciseMatch[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>();
			arguments["operation"] = "GO_GetRegPreciseCategoryMatches";

			DoRequest( arguments, processResults, GORegPreciseMatch.Parse );
		}

        /// <summary> Proxy for the ListGenomeStats web service operation.
		/// <para>
		///		Gets the general statistics on regulons and regulatory sites in all genomes.
		/// </para>
		/// </summary>
		/// <param name="processResults"></param>

		public void ListGenomeStats ( ResponseProcessor<GenomeStats[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListGenomeStats"
			);

			DoRequest( arguments, processResults, GenomeStats.Parse );
		}

		/// <summary> Proxy for the ListRegulatorsInRegulon web service operation.
		/// <para>
		///		Lists the regulators in a regulon.
		/// </para>
		/// </summary>
		/// <param name="regulonId"></param>
		/// <param name="processResults"></param>

		public void ListRegulatorsInRegulon ( int regulonId, ResponseProcessor<Regulator[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulatorsInRegulon",
				"regulonId", regulonId.ToString()
			);

			DoRequest( arguments, processResults, Regulator.Parse );
		}

		/// <summary> Proxy for the ListRegulatorsInRegulog web service operation.
		///	<para>
		///		Lists the regulators in a regulog.
		/// </para>
		/// </summary>
		/// <param name="regulogId"></param>
		/// <param name="processResults"></param>

		public void ListRegulatorsInRegulog ( int regulogId, ResponseProcessor<Regulator[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulatorsInRegulog",
				"regulogId", regulogId.ToString()
			);

			DoRequest( arguments, processResults, Regulator.Parse );
		}

		/// <summary> Gets the specified regulog descriptor.
		/// </summary>
		/// <param name="regulogId"></param>
		/// <param name="processResults"></param>

		public void GetRegulog ( int regulogId, ResponseProcessor<Regulog> processResults ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "GetRegulog",
				"regulogId", regulogId
			);

			DoRequestSingle( arguments, processResults, Regulog.Parse );
		}

		/// <summary> Gets the specified regulon descriptor.
		/// </summary>
		/// <param name="regulonId">The id of a regulon.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void GetRegulon ( int regulonId, ResponseProcessor<Regulon> processResults ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "GetRegulon",
				"regulonId", regulonId
			);

			DoRequestSingle( arguments, processResults, Regulon.Parse );
		}

        /// <summary> Retrieves a list of genomes that have at least one reconstructed regulon.
		/// <para>
		///		Result is a list of genomes with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>genomeId - genome identifier</item>
		/// <item>name - genome name</item>
		/// <item>taxonomyId - NCBI taxonomy id</item>
		/// </list>
		/// </summary>
		/// <param name="processResults"></param>

		public void ListGenomes ( ResponseProcessor<Genome[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>();
			arguments["operation"] = "ListGenomes";

			DoRequest( arguments, processResults, Genome.Parse );
		}

		/// <summary> Retrieves a list of regulogs that belongs to a specific collection
		///	<para>
		///		Requires type and identifier of a regulog collection
		///	</para>
		///	<para>
		///		Returns a list of regulogs. Each regulog is provided with the following data:
		///	</para>
		///	<list type="bullet">
		///	<item>
		///		•regulogId - identifier of regulog
		///	</item>
		///	<item>
		///		•regulatorName - name of regulator
		///	</item>
		///	<item>
		///		•regulatorFamily - family of regulator
		///	</item>
		///	<item>
		///		•regulationType - type of regulation: either TF (transcription factor) or RNA
		///	</item>
		///	<item>
		///		•taxonName - name of taxonomic group
		///	</item>
		///	<item>
		///		•effector - effector molecule or environmental signal of a regulator
		///	</item>
		///	<item>
		///		•pathway - metabolic pathway or biological process controlled by a regulator
		///	</item>
		///	</list>
		/// </summary>

		public void ListRegulogs (
			RegulogCollectionType collectionType,
			Number collectionId,
			ResponseProcessor<Regulog[]> processResults
		) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulogs",
				"collectionType", collectionType.ToString(),
				"collectionId", collectionId.ToString()
			);

			DoRequest( arguments, processResults, Regulog.Parse );
		}

		/// <summary> Retrieves a list of regulons belonging to either a particular regulog.
		/// </summary>
		/// <param name="regulogId">
		///		The numeric regulog Id.
		///	</param>
		/// <param name="processResults">
		///		A delegate which will process the results.
		///	</param>

		public void ListRegulonsInRegulog (
			Number regulogId,
			ResponseProcessor<Regulon[]> processResults
		) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulonsInRegulog",
				"regulogId", regulogId.ToString()
			);

			DoRequest( arguments, processResults, Regulon.Parse );
		}

		/// <summary> Retrieves a list of regulons belonging to a particular genome.
		/// </summary>
		/// <param name="genomeId">
		///		The genome Id.
		/// </param>
		/// <param name="processResults">
		///		A delegate which will process the results.
		///	</param>

		public void ListRegulonsInGenome (
			Number genomeId,
			ResponseProcessor<Regulon[]> processResults
		) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulonsInGenome",
				"genomeId", genomeId.ToString()
			);

			DoRequest( arguments, processResults, Regulon.Parse );
		}

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListSitesInRegulon (
			Number regulonId,
			ResponseProcessor<Site[]> processResults
		) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListSitesInRegulon",
				"regulonId", regulonId.ToString()
			);

			DoRequest( arguments, processResults, Site.Parse );
		}

		/// <summary> Gets the list of TF binding sites or RNA regulatory elements and associated genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListSitesInRegulog ( Number regulogId, ResponseProcessor<Site[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListSitesInRegulog",
				"regulogId", regulogId.ToString()
			);

			DoRequest( arguments, processResults, Site.Parse );
		}

		/// <summary> Gets the list of genes for a regulon.
		/// </summary>
		/// <param name="regulonId">The regulon Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListGenesInRegulon ( Number regulonId, ResponseProcessor<Gene[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListGenesInRegulon",
				"regulonId", regulonId.ToString()
			);

			DoRequest( arguments, processResults, Gene.Parse );
		}

		/// <summary> Gets the list of genes for a regulog.
		/// </summary>
		/// <param name="regulogId">The regulog Id.</param>
		/// <param name="processResults">A delegate that will process the results.</param>

		public void ListGenesInRegulog ( Number regulogId, ResponseProcessor<Gene[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListGenesInRegulog",
				"regulogId", regulogId.ToString()
			);

			DoRequest( arguments, processResults, Gene.Parse );
		}

		/// <summary> Get a list of regulog collections or the specified type.
		/// <para>
		///		Result is a list of regulog collections. Each regulog collection is provided with the following data:
		/// </para>
		/// <list type="bullet">
		/// <item>collectionType - type of regulog collection</item>
		/// <item>collectionId - identifier of collection</item>
		/// <item>name - collection name</item>
		/// <item>className - name of collection class </item>
		/// </list>
		/// </summary>
		/// <param name="collectionType"> The idneity of the required collection type. </param>
		/// <param name="processResults"> A callback that will process the results. </param>

		public void ListRegulogCollections ( RegulogCollectionType collectionType, ResponseProcessor<RegulogCollection[]> processResults ) {
			Dictionary<String, string> arguments = new Dictionary<string, string>(
				"operation", "ListRegulogCollections",
				"collectionType", collectionType.ToString()
			);

			DoRequest( arguments, processResults, RegulogCollection.Parse );
		}

		/// <summary>
        /// Adds the specified list of regulogs to the MySQL database after a RegPrecise ListRegulogs call
        /// <para>Will only add regulogs that do not already exist</para>
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="collectionId"></param>
        /// <param name="regulogs"> The regulogs to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>

		public void AddRegulogs_ListRegulogs (
			RegulogCollectionType collectionType,
			Number collectionId,
            Regulog[] regulogs,
            ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
                "collectionType", collectionType.ToString(),
                "collectionId", collectionId.ToString(),
				"operation", "AddRegulogs_ListRegulogs",
				"regulogs", regulogs
			);

			DoRequestAdd( arguments, processResults );
        }

        /// <summary>
        /// Adds the specified regulog to the MySQL database
        /// <para>Will only add regulogs that do not already exist</para>
        /// <para>Should be used when the regulog is retrieved from RegPrecise using the GetRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulog"> The regulog to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulogs_GetRegulog (
			int regulogId,
            Regulog regulog,
            ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
                "regulogId", regulogId.ToString(),
				"operation", "AddRegulogs_GetRegulog",
				"regulog", regulog
			);

			DoRequestAdd( arguments, processResults );
        }

        /// <summary>
        /// Adds the specified regulons to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulons are retrieved from RegPrecise using the ListRegulonsInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulons"> The regulons to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulons_ListRegulonsInRegulog (
			Number regulogId,
            Regulon[] regulons,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
                "regulogId", regulogId.ToString(),
				"operation", "AddRegulons_ListRegulonsInRegulog",
				"regulons", regulons
			);

			DoRequestAdd( arguments, processResults );
        }

        /// <summary>
        /// Adds the specified regulons to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulons are retrieved from RegPrecise using the ListRegulonsInRegulog request</para>
        /// </summary>
        /// <param name="genomeId"></param>
        /// <param name="regulons"> The regulons to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulons_ListRegulonsInGenome (
			Number genomeId,
            Regulon[] regulons,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
                "genomeId", genomeId.ToString(),
				"operation", "AddRegulons_ListRegulonsInGenome",
				"regulons", regulons
			);

			DoRequestAdd( arguments, processResults );
        }

        /// <summary>
        /// Adds the specified regulog collections to the MySQL database
        /// <para>Will only add regulog collections that do not already exist</para>
        /// <para>Should be used when the regulog collections are retrieved from RegPrecise using the ListRegulogCollections request</para>
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="regulogCollections"> The regulog collections to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulogCollections_ListRegulogCollections (
			RegulogCollectionType collectionType,
            RegulogCollection[] regulogCollections,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddRegulogCollections_ListRegulogCollections",
				"collectionType", collectionType.ToString(),
                "regulogCollections", regulogCollections
			);

			DoRequestAdd( arguments, processResults );
		}

        /// <summary>
        /// Adds the specified genomes to the MySQL database
        /// <para>Will only add genomes that do not already exist</para>
        /// <para>Should be used when the genomes are retrieved from RegPrecise using the ListGenomes request</para>
        /// </summary>
        /// <param name="genomes"> The genomes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddGenomes_ListGenomes (
            Genome[] genomes,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>();
			arguments["operation"] = "AddGenomes_ListGenomes";
            arguments["genomes"] = genomes;

			DoRequestAdd( arguments, processResults );
		}

        /// <summary>
        /// Adds the specified sites to the MySQL database
        /// <para>Will only add sites that do not already exist</para>
        /// <para>Should be used when the sites are retrieved from RegPrecise using the ListSitesInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="sites"> The sites to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddSites_ListSitesInRegulon (
			Number regulonId,
            Site[] sites,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddSites_ListSitesInRegulon",
				"regulonId", regulonId.ToString(),
                "sites", sites
			);

			DoRequestAdd( arguments, processResults );
		}

        /// <summary>
        /// Adds the specified sites to the MySQL database
        /// <para>Will only add sites that do not already exist</para>
        /// <para>Should be used when the sites are retrieved from RegPrecise using the ListSitesInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="sites"> The sites to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddSites_ListSitesInRegulog (
			Number regulogId,
            Site[] sites,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddSites_ListSitesInRegulog",
				"regulogId", regulogId.ToString(),
                "sites", sites
			);

			DoRequestAdd( arguments, processResults );
		}

        /// <summary>
        /// Adds the specified regulon to the MySQL database
        /// <para>Will only add regulons that do not already exist</para>
        /// <para>Should be used when the regulon is retrieved from RegPrecise using the GetRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="regulon"> The regulon to add to the database</param>
		/// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulons_GetRegulon (
			int regulonId,
            Regulon regulon,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddRegulons_GetRegulon",
				"regulonId", regulonId,
                "regulon", regulon
			);

			DoRequestAdd( arguments, processResults );
		}

        /// <summary>
        /// Adds the specified regulators to the MySQL database
        /// <para>Will only add regulators that do not already exist</para>
        /// <para>Should be used when the regulators are retrieved from RegPrecise using the ListRegulatorsInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="regulators"> The regulators to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulators_ListRegulatorsInRegulon (
			int regulonId,
            Regulator[] regulators,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddRegulators_ListRegulatorsInRegulon",
				"regulonId", regulonId.ToString(),
                "regulators", regulators
			);

			DoRequestAdd( arguments, processResults );
        }

        /// <summary>
        /// Adds the specified regulators to the MySQL database
        /// <para>Will only add regulators that do not already exist</para>
        /// <para>Should be used when the regulators are retrieved from RegPrecise using the ListRegulatorsInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="regulators"> The regulators to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddRegulators_ListRegulatorsInRegulog (
			int regulogId,
            Regulator[] regulators,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddRegulators_ListRegulatorsInRegulog",
				"regulogId", regulogId.ToString(),
                "regulators", regulators
			);

			DoRequestAdd( arguments, processResults );
        }

        /// <summary>
        /// Adds the specified genes to the MySQL database
        /// <para>Will only add genes that do not already exist</para>
        /// <para>Should be used when the genes are retrieved from RegPrecise using the ListGenesInRegulon request</para>
        /// </summary>
        /// <param name="regulonId"></param>
        /// <param name="genes"> The genes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddGenes_ListGenesInRegulon (
			Number regulonId,
            Gene[] genes,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddGenes_ListGenesInRegulon",
				"regulonId", regulonId.ToString(),
                "genes", genes
			);

			DoRequestAdd( arguments, processResults );
		}

        /// <summary>
        /// Adds the specified genes to the MySQL database
        /// <para>Will only add genes that do not already exist</para>
        /// <para>Should be used when the genes are retrieved from RegPrecise using the ListGenesInRegulog request</para>
        /// </summary>
        /// <param name="regulogId"></param>
        /// <param name="genes"> The genes to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddGenes_ListGenesInRegulog (
			Number regulogId,
            Gene[] genes,
			ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddGenes_ListGenesInRegulog",
				"regulogId", regulogId.ToString(),
                "genes", genes
			);

			DoRequestAdd( arguments, processResults );
		}

        /// <summary>
        /// Adds the specified genome stats to the MySQL database
        /// <para>Will only add genome states that do not already exist</para>
        /// <para>Should be used when the genome stats are retrieved from RegPrecise using the ListGenomeStats request</para>
        /// </summary>
        /// <param name="genomeStats"> The genome stats to add to the database</param>
        /// <param name="processResults"> A callback that will process the results. </param>
        public void AddGenomeStats_ListGenomeStats (
            GenomeStats[] genomeStats,
            ResponseProcessor<Number> processResults
        ) {
			Dictionary<String, object> arguments = new Dictionary<string, object>(
				"operation", "AddGenomeStats_ListGenomeStats",
                "genomeStats", genomeStats
			);

			DoRequestAdd( arguments, processResults );
        }
    }
}
