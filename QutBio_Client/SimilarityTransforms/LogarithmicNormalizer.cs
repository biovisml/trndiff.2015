﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.SimilarityTransforms {

	/// <summary>
	/// Non-linear, invertible, scaling function that maps node edges
	/// to the interval [0,1] using a stretched log10 scale.
	/// </summary>

	public class LogarithmicNormalizer : Normalizer {

		public LogarithmicNormalizer(
			double lowerBound,
			double upperBound,
			Stretcher stretcher
		)
			: base( lowerBound, upperBound, stretcher )
		{}

		public override double Map( double x ) {
			if ( x < lowerBound ) {
				return 0;
			}

			if ( x > upperBound ) {
				return 1;
			}

			return Math.Log( x / lowerBound ) / Math.Log( upperBound / lowerBound );
		}

		public override double InverseMap( double x ) {
			if ( x >= 1.0 ) {
				return upperBound;
			}

			if ( x < Map( lowerBound ) ) {
				return lowerBound;
			}

			double a = upperBound / lowerBound;
			double b = Math.Log(a);
			double c = x * b;
			double d = Math.Log(lowerBound) + c;
			double e = Math.Exp(d);
			return e;
		}
	}

}
