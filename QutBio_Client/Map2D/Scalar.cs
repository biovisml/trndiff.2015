﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {
	/// <summary>
	/// A scalar selectedTaxa that can report selectedTaxa changes.
	/// </summary>
	
	public class Scalar {
		private double value = 1.0;

		public double Value {
			get {
				return value;
			}
			set {
				if ( this.value != value ) {
					this.value = value;

					if ( ValueChanged != null ) {
						ValueChanged();
					}
				}
			}
		}

		public event Action ValueChanged;
	}
}
