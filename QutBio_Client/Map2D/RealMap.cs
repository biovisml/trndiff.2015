﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {
	/// <summary>
	/// A delegate representing a function mapping R to R.
	/// </summary>
	/// <param name="x">The argument.</param>
	/// <returns>Some selectedTaxa that presumably depends on the arugment.</returns>

	public delegate double RealMap ( double x );
}
