﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace QUT.Bio.Map2D {
	public class RadiusManipulator: DependencyObject, INotifyPropertyChanged {
		private PositionedElement element;
		private double minimum;
		private double maximum;
		private RealMap map;

		public RadiusManipulator(
			PositionedElement element,
			double minimum,
			double maximum,
			RealMap map
		) {
			this.element = element;
			this.minimum = minimum;
			this.maximum = maximum;
			this.map = map;
		}

		public RadiusManipulator(
			PositionedElement element,
			double minimum,
			double maximum
		) : 
			this( element, minimum, maximum, Identity )
		{}
		
		private static double Identity( double x ) {
			return x;
		}
		
		public static DependencyProperty RadiusProperty = DependencyProperty.Register(
			"Radius",
			typeof( double ),
			typeof( RadiusManipulator ),
			new PropertyMetadata( 0.0, RadiusChanged )
		);

		public double Radius {
			get {
				return ( (double) ( base.GetValue( RadiusManipulator.RadiusProperty ) ) );
			}
			set {
				base.SetValue( RadiusManipulator.RadiusProperty, value );
			}
		}
		
		public double Minimum {
			get {
				return minimum;
			}
		}
		
		public double Maximum {
			get {
				return maximum;
			}
		}

		private static void RadiusChanged( DependencyObject d, DependencyPropertyChangedEventArgs e ) {
			double newValue = (double) e.NewValue;
			double oldValue = (double) e.OldValue;

			if ( oldValue != newValue ) {
				RadiusManipulator radius = (RadiusManipulator) d;

				if ( newValue >= radius.minimum && newValue <= radius.maximum ) {
					double r = radius.map( newValue );
					radius.element.Element.Width = r * 2;
					radius.element.Element.Height = r * 2;
					radius.element.OffsetX = -r;
					radius.element.OffsetY = -r;
					radius.NotifyPropertyChanged( "Radius" );
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		
		public void NotifyPropertyChanged( string propertyName ) {
			if ( PropertyChanged != null ) {
				PropertyChanged( this, new PropertyChangedEventArgs( propertyName ) );
			}
		}
	}
}
