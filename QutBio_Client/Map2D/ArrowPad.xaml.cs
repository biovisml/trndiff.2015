﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {
	public partial class ArrowPad : UserControl {
		public enum Action {
			Nothing, Left, Right, Up, Down, Reset
		};
		
		public delegate void Callback( Action action );
		
		public event Callback ActionEvent;
		
		public ArrowPad() {
			InitializeComponent();

			NavigateLeft.MouseLeftButtonDown += delegate( object sender, MouseButtonEventArgs args ) {
				Broadcast( Action.Left );
			};

			NavigateRight.MouseLeftButtonDown += delegate( object sender, MouseButtonEventArgs args ) {
				Broadcast( Action.Right );
			};

			NavigateUp.MouseLeftButtonDown += delegate( object sender, MouseButtonEventArgs args ) {
				Broadcast( Action.Up );
			};

			NavigateDown.MouseLeftButtonDown += delegate( object sender, MouseButtonEventArgs args ) {
				Broadcast( Action.Down );
			};

			NavigateReset.MouseLeftButtonDown += delegate( object sender, MouseButtonEventArgs args ) {
				Broadcast( Action.Reset );
			};
		}
		
		private void Broadcast ( Action action ) {
			if ( ActionEvent != null ) {
				ActionEvent( action );
			}
		}
	}
}
