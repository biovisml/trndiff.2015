﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {

	/// <summary>
	/// Creates a positioned element anchored at bottom right of its virtual rectangle.
	/// </summary>

	public class AnchoredElement : PositionedElement {
		private double offsetX;
		private double offsetY;
		private HorizontalAlignment horizontalPlacement;
		private VerticalAlignment verticalPlacement;
		
		/// <summary> Initialise the object, anchoring a user interface element at its bottom right corner.
		/// <para>
		///		Note that this will overwrite the element's render transform.
		/// </para>
		/// </summary>
		/// <param name="element">A user interface element to position.</param>
		/// <param name="x">The horizontal coordinate at which the element is to be anchored.</param>
		/// <param name="y">The vertical coordinate at which the element is to be anchored.</param>
		/// <param name="offsetX">The distance from the anchor point to displace the element.</param>
		/// <param name="offsetY">The distance from the anchor point to displace the element.</param>
		/// <param name="elementUnitScale">The first of two transforms applied to the element - I think this one relates to the coordinate frame of reference.</param>
		/// <param name="elementScale">The second scale transform to apply to the element.</param>
		/// <param name="displayCentre">A translation used to displace the display centre.</param>
		/// <param name="preserveStrokeThickness">True iff you want the element's border thickness to be preserved.</param>
		/// <param name="horizontalPlacement">Horizontal placement of the anchor point relative to the element</param>
		/// <param name="verticalPlacement">Vertical placement of the anchor point relative to the element.</param>
		
		public AnchoredElement(
			FrameworkElement element,
			double x,
			double y,
			double offsetX,
			double offsetY,
			ScaleTransform elementUnitScale,
			ScaleTransform elementScale,
			TranslateTransform displayCentre,
			bool preserveStrokeThickness,
			HorizontalAlignment horizontalPlacement,
			VerticalAlignment verticalPlacement
		)
			: base( element, x, y, 
				HorizontalOffset( offsetX, element.Width, horizontalPlacement ), 
				VerticalOffset( offsetY, element.Height, verticalPlacement ), 
				elementUnitScale, elementScale, displayCentre, preserveStrokeThickness ) 
		{
			this.offsetX = offsetX;
			this.offsetY = offsetY;
			this.horizontalPlacement = horizontalPlacement;
			this.verticalPlacement = verticalPlacement;
		}

		private static double HorizontalOffset (
			double offsetX,
			double elementWidth,
			HorizontalAlignment horizontalPlacement
		) {
			return horizontalPlacement == HorizontalAlignment.Left ? -offsetX
			: horizontalPlacement == HorizontalAlignment.Center ? - elementWidth / 2
			: offsetX - elementWidth;
		}

		private static double VerticalOffset (
			double offsetY,
			double elementHeight,
			VerticalAlignment verticalPlacement
		) {
			return verticalPlacement == VerticalAlignment.Top ? - offsetY
			: verticalPlacement == VerticalAlignment.Center ? - elementHeight / 2
			: offsetY - elementHeight;
		}

		/// <summary> Override the resize handler to maintain the right position if the element changes size.
		/// This event listener is hooked up in the base class.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		
		public override void Resized( object sender, SizeChangedEventArgs args ) {
			offset.X = HorizontalOffset( offsetX, element.ActualWidth, horizontalPlacement );
			offset.Y = VerticalOffset( offsetY, element.ActualHeight, verticalPlacement );
		}
	}
}
