﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QUT.Bio.Map2D {
	/// <summary>
	/// Interface for objects whose location is determined by a pair (X,Y).
	/// </summary>

	public interface IPositioned {
		/// <summary>
		/// The horizontal coordinate of the position.
		/// </summary>

		double X {
			get;
			set;
		}
		
		/// <summary>
		/// the vertical coordinate of the position.
		/// </summary>
		
		double Y {
			get;
			set;
		}
	}
}
