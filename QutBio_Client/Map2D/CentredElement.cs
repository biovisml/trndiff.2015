﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {

	public class CentredElement : PositionedElement {
		public CentredElement(
			FrameworkElement element,
			double x,
			double y,
			ScaleTransform elementUnitScale,
			ScaleTransform elementScale,
			TranslateTransform displayCentre,
			bool preserveStrokeThickness
		)
			: base( element, x, y, -element.Width / 2, -element.Height / 2, elementUnitScale, elementScale, displayCentre, preserveStrokeThickness ) {
		}


		public override void Resized( object sender, SizeChangedEventArgs args ) {
			offset.X = -element.ActualWidth / 2;
			offset.Y = -element.ActualHeight / 2;
		}
	}
}
