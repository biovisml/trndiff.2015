﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {
	/// <summary>
	/// An Edge is a line segment that scales appropriately when embedded in a Map2dCanvas.
	/// </summary>

	public class Edge {
		Line line = new Line();
		TranslateTransform offset = new TranslateTransform();
		ScaleTransform stretch = new ScaleTransform();
		MatrixTransform rotate = new MatrixTransform();
		TransformGroup renderTransform = new TransformGroup();

		public Edge(
			double x1,
			double y1,
			double x2,
			double y2,
			double thickness,
			Brush brush,
			ScaleTransform strokeScale,
			TranslateTransform displayCentre
		) {
			line.Stroke = brush;
			line.StrokeThickness = thickness;
			line.X1 = 0;
			line.Y1 = 0;
			line.X2 = 0;
			line.Y2 = 1;
			offset.X = thickness / 2;
			offset.Y = 0;

			double length = Math.Sqrt( ( x2 - x1 ) * ( x2 - x1 ) + ( y2 - y1 ) * ( y2 - y1 ) );
			double c = ( x2 - x1 ) / length;
			double s = ( y2 - y1 ) / length;

			stretch.ScaleY = length;

			rotate.Matrix = new Matrix( s, c, -c, s, x2, -y2 );

			// renderTransform.Children.Add( offset );
			renderTransform.Children.Add( strokeScale );
			renderTransform.Children.Add( stretch );
			renderTransform.Children.Add( rotate );
			renderTransform.Children.Add( displayCentre );
			line.RenderTransform = renderTransform;
		}
		
		public FrameworkElement Element {
			get {
				return line;
			}
		}
	}
}
