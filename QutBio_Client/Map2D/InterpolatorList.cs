﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using QUT.Bio.Util;

namespace QUT.Bio.Map2D {
	public class InterpolatorList: DependencyObject {
		private List<PositionInterpolator> items = new List<PositionInterpolator>();

		public InterpolatorList () {
			Parameter = 0.0;
		}

		public void Add( PositionInterpolator p ) {
			items.Add( p );
		}
		
		public void Clear () {
			items.Clear();
		}

		public static DependencyProperty ParameterProperty = DependencyProperty.Register(
			"Parameter",
			typeof( double ),
			typeof( InterpolatorList ),
			new PropertyMetadata( 0.0, ParameterChanged )
		);

		public double Parameter {
			get {
				return ( (double) ( base.GetValue( InterpolatorList.ParameterProperty ) ) );
			}
			set {
				base.SetValue( InterpolatorList.ParameterProperty, value );
			}
		}

		private static void ParameterChanged( DependencyObject d, DependencyPropertyChangedEventArgs e ) {
			double newValue = (double) e.NewValue;
			double oldValue = (double) e.OldValue;

			if ( oldValue != newValue ) {
				InterpolatorList l = (InterpolatorList) d;
				
				foreach ( PositionInterpolator p in l.items ) {
					p.T = newValue;
				}
			}
		}
		
		public IEnumerable<PositionInterpolator> Items {
			get {
				return items;
			}
		}
	}
}
