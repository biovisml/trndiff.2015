﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {
	public partial class Status : UserControl {
		public double Scale {
			set {
				scale.Text = value.ToString();
			}
		}

		public double X {
			set { 
				centreX.Text = value.ToString();
			}
		}

		public double Y {
			set { 
				centreY.Text = value.ToString();
			}
		}

		public Status() {
			InitializeComponent();
		}
	}
}
