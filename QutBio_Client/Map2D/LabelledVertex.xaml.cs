﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using SystemQut.Controls;
using System.Html;
using SystemQut;
using SystemQut.Svg;
using System;

namespace QUT.Bio.Map2D {
	/// <summary> A LabelledVertex represents a glyph with a textual label.
	/// </summary>
	
	public partial class LabelledVertex : ICodeBehind<LabelledVertex> {
		private string label;
		private SvgElement group, glyph, textBox;

		/// <summary> Constructor( glyph ). Adds the supplied glyph to the control and positions
		/// the label relative to the glyph
		/// </summary>
		/// <param name="glyph">The glyph representing the node.</param>
		/// <param name="horizontalPlacement">Horizontal placement of the glyph relative to the label.</param>
		/// <param name="verticalPlacement">Vertical placement of the glyph relative to the label.</param>

		public LabelledVertex ( 
			SvgElement glyph, 
			HorizontalAlignment horizontalPlacement, 
			VerticalAlignment verticalPlacement 
		) {
			this.group = Svg.CreateElement( "g", null, null );
			this.textBox = Svg.CreateElement( "text", group, null );
			this.glyph = glyph;
			group.AppendChild( glyph );

			PlaceGlyph( horizontalPlacement, verticalPlacement );
		}

		private void PlaceGlyph(HorizontalAlignment horizontalPlacement,VerticalAlignment verticalPlacement)
		{
 			throw new Exception( "Not implemented." );
		}

		/// <summary> Constructor( glyph ). Adds the supplied glyph to the control and positions
		/// the label relative to the glyph
		/// </summary>
		/// <param name="glyph">The glyph representing the node.</param>

		public LabelledVertex ( SvgElement glyph ) 
			: this( glyph, HorizontalAlignment.Right, VerticalAlignment.Bottom ) {
		}

		/// <summary> Default constructor, renders a labelled ellipse.
		/// </summary>

		public LabelledVertex()
			: this( Svg.CreateElement( "circle", 
				Width = 5,
				Height = 5,
				Fill = new SolidColorBrush( Colors.Magenta )
			} ) {
		}

		/// <summary> Get or set the label.
		/// </summary>
		
		public string Label {
			get {
				return label;
			}
			set {
				textBlock.Text = label = value;
				textBlock.Visibility = string.IsNullOrEmpty(label) ? Visibility.Collapsed : Visibility.Visible;
			}
		}
		
		/// <summary> Get the glyph.
		/// </summary>

		public FrameworkElement Glyph {
			get {
				return glyph;
			}
		}

		/// <summary> Get the textblock that renders the label, permitting advanced manipulation of the appearance of the vertex.
		/// </summary>

		public TextBlock TextBlock {
			get {
				return textBlock;
			}
		}
	
public Control<LabelledVertex>  Element
{
	get { throw new global::System.NotImplementedException(); }
}
}
}
