﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {
	public static class MouseWheel {
		public delegate void Action( double delta );
		
		public static event Action Callback;

		static MouseWheel() {
			HtmlElement pluginElement = HtmlPage.Plugin;
			pluginElement.AttachEvent( "DOMMouseScroll", OnMouseWheelTurned );
			pluginElement.AttachEvent( "onmousewheel", OnMouseWheelTurned );
		}
		
		// Based on code from http://www.wintellect.com/CS/blogs/jprosise/archive/2008/03/18/mousewheel-zooms-in-silverlight-2-0.aspx

		private static void OnMouseWheelTurned( Object sender, HtmlEventArgs args ) {
			double delta = 0;
			ScriptObject e = args.EventObject;

			if ( e.GetProperty( "wheelDelta" ) != null ) {
				delta = (double) e.GetProperty( "wheelDelta" );

				if ( HtmlPage.Window.GetProperty( "opera" ) != null ) {
					delta = -delta;
				}
			}
			else if ( e.GetProperty( "detail" ) != null ) {
				// Mozilla and Safari
				delta = -(double) e.GetProperty( "detail" );
			}
			
			if ( delta != 0 ) {
				if ( Callback != null ) {
					Callback( delta );
				}
				args.PreventDefault();
				e.SetProperty( "returnValue", false );
			}
		}
	}
}
