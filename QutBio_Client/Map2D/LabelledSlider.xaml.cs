﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Map2D {
	public partial class LabelledSlider : UserControl {
		public delegate string StringConverter( double value );

		private StringConverter valueConverter = DefaultConverter;
		
		public LabelledSlider() {
			InitializeComponent();
			slider.ValueChanged += ValueChanged;
		}
		
		public void Bind( object source, string valueName, string minimumName, string maximumName ) {
			Binding binding = new Binding( valueName );
			binding.Source = source;
			binding.Mode = BindingMode.TwoWay;
			slider.SetBinding( Slider.ValueProperty, binding );

			if ( minimumName != null ) {
				binding = new Binding( minimumName );
				binding.Source = source;
				binding.Mode = BindingMode.OneWay;
				slider.SetBinding( Slider.MinimumProperty, binding );
			}

			if ( maximumName != null ) {
				binding = new Binding( maximumName );
				binding.Source = source;
				binding.Mode = BindingMode.OneWay;
				slider.SetBinding( Slider.MaximumProperty, binding );
			}
		}

		private void ValueChanged( object sender, RoutedPropertyChangedEventArgs<double> e ) {
			Refresh();
		}
		
		private static string DefaultConverter( double v ) {
			return v.ToString("g2");
		}

		public StringConverter ValueConverter {
			get {
				return valueConverter;
			}
			set {
				valueConverter = value;
			}
		}
		
		public string Label {
			get {
				return title.Text;
			}
			set {
				title.Text = value;
			}
		}
		
		public void Refresh() {
			literal.Text = valueConverter( slider.Value );
		}
	}
}
