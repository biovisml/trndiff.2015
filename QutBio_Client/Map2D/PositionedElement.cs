﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;

namespace QUT.Bio.Map2D {
	/// <summary>
	/// Takes care of positioning, scaling and tr5ansforming a UI element so it will behave well in the Map2DCanvas class.
	/// </summary>
	
	public class PositionedElement: IPositioned {
		protected FrameworkElement element;
		protected TranslateTransform position = new TranslateTransform();
		protected TranslateTransform offset = new TranslateTransform();
		protected TransformGroup transformGroup = new TransformGroup();
		protected double strokeThickness = double.NaN;

		public PositionedElement(
			FrameworkElement element,
			double x,
			double y,
			double offsetX,
			double offsetY,
			ScaleTransform elementUnitScale,
			ScaleTransform elementScale,
			TranslateTransform displayCentre,
			bool preserveStrokeThickness
		) {
			this.element = element;
			position.X = x;
			position.Y = -y;
			offset.X = offsetX;
			offset.Y = offsetY;
			transformGroup.Children.Add( offset );

			if ( elementUnitScale != null ) {
				transformGroup.Children.Add( elementUnitScale );
			}

			if ( elementScale != null ) {
				transformGroup.Children.Add( elementScale );
			}

			transformGroup.Children.Add( displayCentre );
			transformGroup.Children.Add( position );
			element.RenderTransform = transformGroup;
			element.SizeChanged += Resized;
			
			if ( preserveStrokeThickness && element is Shape ) {
				strokeThickness = ((Shape) element).StrokeThickness;
			}
		}

		public virtual void Resized( object sender, SizeChangedEventArgs args ) {
		}

		public FrameworkElement Element {
			get {
				return element;
			}
		}

		public double X {
			get {
				return position.X;
			}
			set {
				position.X = value;
			}
		}

		public double Y {
			get {
				return -position.Y;
			}
			set {
				position.Y = -value;
			}
		}

		public double OffsetX {
			get {
				return offset.X;
			}
			set {
				offset.X = value;
			}
		}

		public double OffsetY {
			get {
				return offset.Y;
			}
			set {
				offset.Y = value;
			}
		}

		public TransformGroup TransformGroup {
			get {
				return transformGroup;
			}
		}

		public double StrokeThickness {
			get {
				return strokeThickness;
			}
			set {
				strokeThickness = value;
			}
		}
	}
}
