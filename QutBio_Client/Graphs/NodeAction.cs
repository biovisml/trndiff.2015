﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Graphs {

	/// <summary>
	/// Action event values used to passa clicks and double clicks back to client.
	/// </summary>

	#pragma warning disable 1591
	public enum NodeAction {
		NodeClicked,
		NodeDoubleClicked,
		LabelClicked
	};
	#pragma warning restore 1591
}
