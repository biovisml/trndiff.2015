﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace QUT.Bio.Graphs {

	/// <summary>
	/// An Adapter provides a minimal set of functionality that must be implemented 
	/// by a consumer of the GraphView control, mapping from abstract consumer space 
	/// to concrete Map2D space.
	/// </summary>
	/// <typeparam name="NodeType">The type used to index and identify nodes.</typeparam>

	public interface IGraphAdapter<NodeType>
	where NodeType : IComparable<NodeType>, IEquatable<NodeType> {

		#region GetNodeShape
		/// <summary>
		/// Provides a glyph for each node.
		/// </summary>

		FrameworkElement GetNodeShape ( Node<NodeType> node ); 
		#endregion

		#region GetNodeLabel
		/// <summary>
		/// Provides a textual label for each node.
		/// </summary>

		string GetNodeLabel ( Node<NodeType> node ); 
		#endregion

		#region DoLayout
		/// <summary>
		/// Provides coordinates for each node.
		///		This will be provided with a fully populated Dictionary mapping node id's
		///		to XY pairs.
		///		The function should fill in the X and y coordinates for each node.
		/// </summary>

		void DoLayout ( Dictionary<NodeType, XY> locations ); 
		#endregion

		#region PrepareNodeView
		/// <summary>
		/// Called when a node view is created, allowing the consumer to
		/// attach event handlers etc to a node view.
		/// </summary>
		/// <param name="nodeView"></param>

		void PrepareNodeView ( NodeView<NodeType> nodeView ); 
		#endregion
		
		#region UpdateNodes
		/// <summary>
		/// Called by GraphView.RethinkDisplay as a step when recomputing the layout.
		/// You can use this as a chance to maintain internal state associated with the node
		/// collection. For example, DefaultSilverMapAdapter needs a sorted array of nodes 
		/// for its layout engine; this is an opportunity to keep this list up to date.
		/// </summary>
		/// <param name="nodes">The list of nodes in the current graph.</param>
		
		void UpdateNodes ( IEnumerable<Node<NodeType>> nodes );
		#endregion
	}
}
