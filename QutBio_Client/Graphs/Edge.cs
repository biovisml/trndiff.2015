﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using QUT.Bio.Util;

namespace QUT.Bio.Graphs {
	
	/// <summary>
	/// An edge is an intermediate object layer that binds an application specific object containing 
	/// the properties associated with a directed edge to a pair of node id values that determine the 
	/// endpoints of the edge.
	/// </summary>
	/// <typeparam name="NodeType">The type used to identify nodes.</typeparam>

	public class Edge<NodeType> : ObjectWrapper 
		where NodeType: IComparable<NodeType>, IEquatable<NodeType> {
		
		private NodeType fromId;
		private NodeType toId;

		/// <summary>
		/// Binds an application specific object containing properties associated with a single edge
		/// to a (from, to) pair of nodes.
		/// </summary>
		/// <param name="fromId">The id of the source node.</param>
		/// <param name="toId">The id of the destination node.</param>
		/// <param name="content">The content attached to this edge.</param>
		
		public Edge (
			NodeType fromId,
			NodeType toId,
			object content
		)
			: base( content ) {
			this.fromId = fromId;
			this.toId = toId;
		}

		/// <summary>
		/// The id of the source node for the edge.
		/// </summary>
		
		public NodeType FromId {
			get {
				return fromId;
			}
		}
		
		/// <summary>
		/// The id of the destination node of the edge.
		/// </summary>

		public NodeType ToId {
			get {
				return toId;
			}
		}
	}
}
