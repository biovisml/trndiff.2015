﻿/*
 *	Copyright (c) 2009 Queensland University of Technology. All rights reserved.
 *	The QUT Bioinformatics Collection is open source software released under the 
 *	Microsoft Public License (Ms-PL): http://www.microsoft.com/opensource/licenses.mspx.
 */
using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using QUT.Bio.Map2D;
using QUT.Bio.Util;

namespace QUT.Bio.Graphs {

	/// <summary>
	/// Node does two things:
	/// <list>
	/// <item>Adapts an object to make its properties indexable by string.</item>
	/// </list>
	/// <para>NodeView then uses the string->object indexer to access and (possibly) mutate the properties of an underlying object.</para>
	/// </summary>
	/// <typeparam name="NodeType">The type used for data ids.</typeparam>

	public class Node<NodeType> : ObjectWrapper
		where NodeType : IComparable<NodeType>, IEquatable<NodeType> {

		#region Constructor - all properties
		/// <summary>
		/// Initialises a Node.
		/// </summary>
		/// <param name="nodeContent">The content of this node.</param>

		public Node ( NodeType nodeContent )
			: base( nodeContent ) {
		}
		#endregion

		#region Method: Equals
		/// <summary>
		/// Determines if the Node is equal to an object..
		/// </summary>
		/// <param name="other">An object for comparison.</param>
		/// <returns>True iff the other object is a Node and the id's of the two nodes are equal.</returns>

		public override bool Equals ( object other ) {
			return Content.Equals( ( (Node<NodeType>) other ).Content );
		} 
		#endregion

		#region Method: GetHashCode
		/// <summary>
		/// Gets a hashcode for the Node.
		/// </summary>
		/// <returns>The hash code of the node's Id.</returns>

		public override int GetHashCode () {
			return Content.GetHashCode();
		} 
		#endregion

		#region Method: ToString
		/// <summary>
		/// Produces basic textual view of this node.
		/// </summary>
		/// <returns>A basic textual view of this node.</returns>

		public override string ToString () {
			return Content.ToString();
		} 
		#endregion
		
		#region Property: Content
		/// <summary>
		/// Gets the content of this node.
		/// </summary>
		
		public new NodeType Content {
			get {
				return (NodeType) base.Content;
			}
		}
		#endregion
	}
}
