﻿using System;
using System.Collections.Generic;

using System.Runtime.CompilerServices;

namespace D3JS {

	[ScriptImport, ScriptIgnoreNamespace]
	public class UpdateSelection : Selection {

		/// <summary> Returns the entering selection: placeholder nodes for each data element for which no 
		///		corresponding existing DOM element was found in the current selection. This method is only 
		///		defined on a selection returned by the data operator. In addition, the entering selection 
		///		only defines append, insert, select and call operators; you must use these operators to 
		///		instantiate the entering nodes before modifying any content. (Enter selections also support 
		///		empty to check if they are empty.) Note that the enter operator merely returns a reference 
		///		to the entering selection, and it is up to you to add the new nodes.
		///	<para>
		///		As a simple example, consider the case where the existing selection is empty, and we wish 
		///		to create new nodes to match our data:
		///	</para>
		///	<pre>
		///		d3.select("body").selectAll("div")
		///		    .data([4, 8, 15, 16, 23, 42])
		///		  .enter().append("div")
		///		    .text(function(d) { return d; });
		///	</pre>
		///	<para>
		///		Assuming that the body is initially empty, the above code will create six new DIV elements, 
		///		append them to the body in order, and assign their text content as the associated 
		///		(string-coerced) number:
		///	</para>
		///	<pre>
		///		<div>4</div>
		///		<div>8</div>
		///		<div>15</div>
		///		<div>16</div>
		///		<div>23</div>
		///		<div>42</div>
		///	</pre>
		///	<para>
		///		Another way to think about the entering placeholder nodes is that they are pointers to 
		///		the parent node (in this example, the document body); however, they only support append 
		///		and insert.
		///	</para>
		///	<para>
		///		The enter selection merges into the update selection when you append or insert. This 
		///		approach reduces code duplication between enter and update. Rather than applying 
		///		operators to both the enter and update selection separately, you can now apply them 
		///		to the update selection after entering the nodes. In the rare case that you want to 
		///		run operators only on the updating nodes, you can run them on the update selection 
		///		before entering new nodes.
		///	</para>
		/// </summary>
		/// <returns>
		///		Returns the Entering Selection.
		/// </returns>

		public EnteringSelection Enter () {
			return null;
		}

		/// <summary> Returns the exiting selection: existing DOM elements in the current selection 
		///		for which no new data element was found. This method is only defined on a selection 
		///		returned by the data operator. The exiting selection defines all the normal operators, 
		///		though typically the main one you'll want to use is remove; the other operators exist 
		///		primarily so you can define an exiting transition as desired. Note that the exit operator 
		///		merely returns a reference to the exiting selection, and it is up to you to remove the 
		///		new nodes.
		///	<para>
		///		As a simple example, consider updating the six DIV elements created in the above example 
		///		for the enter operator. Here we bind those elements to a new array of data with some new 
		///		and some old:
		///	</para>
		///	<pre>
		///		var div = d3.select("body").selectAll("div")
		///		    .data([1, 2, 4, 8, 16, 32], function(d) { return d; });
		///	</pre>
		///	<para>
		///		Now div—the result of the data operator—refers to the updating selection. Since we specified a key function using the identity function, and the new data array contains the numbers [4, 8, 16] which also exist in the old data array, this updating selection contains three DIV elements. Let's say we leave those elements as-is. We can instantiate and add the new elements [1, 2, 32] using the entering selection:
		///	</para>
		///	<pre>
		///		div.enter().append("div")
		///		    .text(function(d) { return d; });
		///	</pre>
		///	<para>
		///		Likewise, we can remove the exiting elements [15, 23, 42]:
		///	</para>
		///	<pre>
		///		div.exit().remove();
		///	</pre>
		///	<para>
		///		Now the document body looks like this:
		///	</para>
		///	<pre>
		///		<div>4</div>
		///		<div>8</div>
		///		<div>16</div>
		///		<div>1</div>
		///		<div>2</div>
		///		<div>32</div>
		///	</pre>
		///	<para>
		///		Note that the DOM elements are now out-of-order. However, the selection index i (the second argument to operator functions), will correctly match the new data array. For example, we could assign an index attribute:
		///	</para>
		///	<pre>
		///		d3.selectAll("div").attr("index", function(d, i) { return i; });
		///	</pre>
		///	<para>
		///		This would result in:
		///	</para>
		///	<pre>
		///		<div index="2">4</div>
		///		<div index="3">8</div>
		///		<div index="4">16</div>
		///		<div index="0">1</div>
		///		<div index="1">2</div>
		///		<div index="5">32</div>
		///	</pre>
		///	<para>
		///		If you want the document traversal order to match the selection data order, you can use sort or order.
		///	</para>
		/// </summary>
		/// <returns>
		///		Returns the exiting selection.
		/// </returns>

		public ExitingSelection Exit () {
			return null;
		}
	}
}
