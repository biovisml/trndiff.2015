﻿using System;
using System.Collections.Generic;

using System.Html;
using System.Runtime.CompilerServices;

namespace D3JS {
	[ScriptImport, ScriptIgnoreNamespace]
	public class EnteringSelection {

		/// <summary> Appends a new element with the specified name as the last child of 
		///		each element in the current selection, returning a new selection containing 
		///		the appended elements. Each new element inherits the data of the current 
		///		elements, if any, in the same manner as select for subselections.
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		/// </summary>
		/// <param name="elementName">
		///		The (optionally namespace-qualified) element to create and add.
		/// </param>
		/// <returns></returns>

		public Selection Append ( string elementName ) {
			return null;
		}

		/// <summary> Appends a new element with the specified name as the last child of 
		///		each element in the current selection, returning a new selection containing 
		///		the appended elements. Each new element inherits the data of the current 
		///		elements, if any, in the same manner as select for subselections.
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		/// </summary>
		/// <param name="dataFunction">
		///		A function that will be used to provide elements to be appended. 
		/// </param>
		/// <returns>
		///		Returns the current Selection.
		/// </returns>

		[ScriptName( "append" )]
		public Selection Append<DataType> ( DataFunction<DataType, Element> dataFunction ) {
			// TODO_LATER: verify that the delegate type I'm using here is correct. This is not clearly specified in the API reference.
			return null;
		}

		/// <summary># selection.insert(name[, before])
		///	<para>
		///		Inserts a new element with the specified name before the element matching 
		///		the specified before selector, for each element in the current selection, 
		///		returning a new selection containing the inserted elements. If the before 
		///		selector does not match any elements, then the new element will be the last 
		///		child as with append. Each new element inherits the data of the current elements 
		///		(if any), in the same manner as select for subselections.
		///	</para>
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		///	<para>
		///		Likewise, the before selector may be specified as a selector string or a 
		///		function which returns a DOM element. For instance, insert("div", ":first-child") 
		///		will prepend child div nodes to the current selection. For enter selections, the 
		///		before selector may be omitted, in which case entering elements will be inserted 
		///		immediately before the next following sibling in the update selection, if any. 
		///		This allows you to insert elements into the DOM in an order consistent with bound 
		///		data. Note, however, the slower selection.order may still be required if updating 
		///		elements change order.
		///	</para>
		/// </summary>
		/// <param name="elementName"></param>
		/// <returns></returns>

		public Selection Insert( string elementName ) {
			return null;
		}

		/// <summary># selection.insert(name[, before])
		///	<para>
		///		Inserts a new element with the specified name before the element matching 
		///		the specified before selector, for each element in the current selection, 
		///		returning a new selection containing the inserted elements. If the before 
		///		selector does not match any elements, then the new element will be the last 
		///		child as with append. Each new element inherits the data of the current elements 
		///		(if any), in the same manner as select for subselections.
		///	</para>
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		///	<para>
		///		Likewise, the before selector may be specified as a selector string or a 
		///		function which returns a DOM element. For instance, insert("div", ":first-child") 
		///		will prepend child div nodes to the current selection. For enter selections, the 
		///		before selector may be omitted, in which case entering elements will be inserted 
		///		immediately before the next following sibling in the update selection, if any. 
		///		This allows you to insert elements into the DOM in an order consistent with bound 
		///		data. Note, however, the slower selection.order may still be required if updating 
		///		elements change order.
		///	</para>
		/// </summary>
		/// <param name="elementName"></param>
		/// <param name="cssSelector"></param>
		/// <returns></returns>

		public Selection Insert( string elementName, string cssSelector ) {
			return null;
		}

		/// <summary># selection.insert(name[, before])
		///	<para>
		///		Inserts a new element with the specified name before the element matching 
		///		the specified before selector, for each element in the current selection, 
		///		returning a new selection containing the inserted elements. If the before 
		///		selector does not match any elements, then the new element will be the last 
		///		child as with append. Each new element inherits the data of the current elements 
		///		(if any), in the same manner as select for subselections.
		///	</para>
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		///	<para>
		///		Likewise, the before selector may be specified as a selector string or a 
		///		function which returns a DOM element. For instance, insert("div", ":first-child") 
		///		will prepend child div nodes to the current selection. For enter selections, the 
		///		before selector may be omitted, in which case entering elements will be inserted 
		///		immediately before the next following sibling in the update selection, if any. 
		///		This allows you to insert elements into the DOM in an order consistent with bound 
		///		data. Note, however, the slower selection.order may still be required if updating 
		///		elements change order.
		///	</para>
		/// </summary>
		/// <param name="dataFunction"></param>
		/// <returns></returns>

		[ScriptName( "insert" )]
		public Selection Insert<DataType> ( DataFunction<DataType, Element> dataFunction ) {
			// TODO_LATER: verify that this is the right data type.
			return null;
		}

		/// <summary># selection.insert(name[, before])
		///	<para>
		///		Inserts a new element with the specified name before the element matching 
		///		the specified before selector, for each element in the current selection, 
		///		returning a new selection containing the inserted elements. If the before 
		///		selector does not match any elements, then the new element will be the last 
		///		child as with append. Each new element inherits the data of the current elements 
		///		(if any), in the same manner as select for subselections.
		///	</para>
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		///	<para>
		///		Likewise, the before selector may be specified as a selector string or a 
		///		function which returns a DOM element. For instance, insert("div", ":first-child") 
		///		will prepend child div nodes to the current selection. For enter selections, the 
		///		before selector may be omitted, in which case entering elements will be inserted 
		///		immediately before the next following sibling in the update selection, if any. 
		///		This allows you to insert elements into the DOM in an order consistent with bound 
		///		data. Note, however, the slower selection.order may still be required if updating 
		///		elements change order.
		///	</para>
		/// </summary>
		/// <param name="dataFunction"></param>
		/// <param name="cssSelector"></param>
		/// <returns></returns>

		[ScriptName( "insert" )]
		public Selection Insert<DataType> ( DataFunction<DataType, Element> dataFunction, string cssSelector ) {
			return null;
		}
	}
}
