using System.Runtime.CompilerServices;

namespace D3JS {

	/// <summary> Delegate type for the data provider functions which are used extensively in D3 programs.
	/// </summary>
	/// <typeparam name="DataType"></typeparam>
	/// <typeparam name="ResultType"></typeparam>
	/// <param name="datum"></param>
	/// <param name="index"></param>
	/// <returns></returns>
	
	[ScriptImport, ScriptIgnoreNamespace]
	public delegate ResultType DataFunction<DataType,ResultType>( DataType datum, int index );

	/// <summary> Delegate type for the data provider functions which are used extensively in D3 programs.
	/// </summary>
	/// <typeparam name="DataType"></typeparam>
	/// <param name="datum"></param>
	/// <param name="index"></param>
	/// <returns></returns>
	
	[ScriptImport, ScriptIgnoreNamespace]
	public delegate void DataAction<DataType>( DataType datum, int index );

	/// <summary> 
	/// </summary>
	/// <param name="selection"></param>

	[ScriptImport, ScriptIgnoreNamespace]
	public delegate void SelectionAction ( Selection selection ); 

	/// <summary> 
	/// </summary>
	/// <param name="selection"></param>
	/// <param name="args"></param>

	[ScriptImport, ScriptIgnoreNamespace]
	public delegate void SelectionActionWithParam ( Selection selection, object [] args ); 
}