﻿using System;
using System.Collections.Generic;

using System.Runtime.CompilerServices;

namespace D3JS {

	/// <summary> A selction containing existing DOM elements in the current selection for which no new data was found.
	/// </summary>

	[ScriptImport, ScriptIgnoreNamespace]
	public class ExitingSelection: Selection {
	}
}
