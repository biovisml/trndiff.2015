﻿using System;
using System.Collections.Generic;

using System.Html;
using System.Runtime.CompilerServices;

namespace D3JS {
	[ScriptImport, ScriptIgnoreNamespace]
	public class Selection : IArrayLike<IArrayLike<Element>> {

		/// <summary> Get the number of element lists in the selection.
		/// </summary>

		[ScriptField]
		public int Length {
			get {
				return 0;
			}
		}

		/// <summary> Accesses an element list in the collection.
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>

		[ScriptField]
		public IArrayLike<Element> this[int i] {
			get { return null; }
		}

		/// <summary> Gets the value of the named attribute for the first non-null 
		///		element in the selection.  This is generally useful only if you know 
		///		that the selection contains exactly one element.
		/// </summary>
		/// <param name="name"> The attribute name. </param>
		/// <returns> The value of the attribute for the first non-null element. </returns>

		public object Attr ( string name ) { return null; }

		/// <summary> Sets the attribute with the specified name to the specified 
		///		value on all selected elements. A null value will remove the specified attribute.
		/// </summary>
		/// <param name="name"> The attribute name. </param>
		/// <param name="value"> The constant value that will be applied to all elements in the selection. </param>
		/// <returns> The selection. </returns>

		public Selection Attr<AttributeType> (
			string name,
			AttributeType value
		) { return this; }

		/// <summary> Sets the name attribute value for each element in the selection. The supplied function is 
		///		evaluated for each selected element (in order), being passed the current datum d and the current 
		///		index i, with the this context as the current DOM element. The function's return value is then 
		///		used to set each element's attribute.
		/// </summary>
		/// <typeparam name="DataType"></typeparam>
		/// <typeparam name="AttributeType"></typeparam>
		/// <param name="name"></param>
		/// <param name="valueProvider"></param>
		/// <returns></returns>

		[ScriptName( "attr" )]
		public Selection Attr<DataType, AttributeType> (
			string name,
			DataFunction<DataType, AttributeType> valueProvider
		) {
			return this;
		}

		/// <summary> Invokes the specified function for each element in the current selection, 
		///		passing in the current datum d and index i, with the this context of the current 
		///		DOM element. This operator is used internally by nearly every other operator, 
		///		and can be used to invoke arbitrary code for each selected element. The each 
		///		operator can be used to process selections recursively, by using d3.select(this) 
		///		within the callback function.
		///	</summary>
		/// <typeparam name="DataType"></typeparam>
		/// <param name="action"></param>
		/// <returns></returns>

		[ScriptName( "each" )]
		public Selection Each<DataType> ( DataAction<DataType> action ) {
			return this;
		}

		/// <summary> Returns true if and only if the first non-null element in this selection 
		///		has the specified class. This is generally useful only if you know the 
		///		selection contains exactly one element.
		/// </summary>
		/// <param name="name">
		///		The class name to test.
		/// </param>
		/// <returns>
		///		Returns true if and only if the first non-null element in this selection 
		///		has the specified class.
		/// </returns>

		[ScriptName( "classed" )]
		public bool IsClassed ( string name ) {
			return false;
		} 

		/// <summary> This operator is a convenience routine for setting the "class" attribute; 
		///		it understands that the "class" attribute is a set of tokens separated by spaces. 
		///		Under the hood, it will use the classList if available, for convenient adding, 
		///		removing and toggling of CSS classes.
		/// <para>
		///		Value determines whether or not the specified class will be associated 
		///		with the selected elements. If value is a constant and truthy, then all elements 
		///		are assigned the specified class, if not already assigned; if falsey, then the class 
		///		is removed from all selected elements, if assigned.
		/// </para>
		/// </summary>
		/// <param name="name">
		///		The class name to add or remove.
		/// </param>
		/// <param name="value">
		///		Determines whether or not the specified class is associated 
		///		with the selected elements. Truthy implies class will be added; falsey
		///		implies cals will be removed.
		/// </param>
		/// <returns>
		///		The current Selection.
		/// </returns>

		public Selection Classed ( string name, bool value ) {
			return this;
		}

		/// <summary> This operator is a convenience routine for setting the "class" attribute; 
		///		it understands that the "class" attribute is a set of tokens separated by spaces. 
		///		Under the hood, it will use the classList if available, for convenient adding, 
		///		removing and toggling of CSS classes.
		/// <para>
		///		Value determines whether or not the specified class will be associated 
		///		with the selected elements. If value is a function, then the function is evaluated 
		///		for each selected element (in order), being passed the current datum d and the 
		///		current index i, with the "this" context as the current DOM element. The function's 
		///		return value is then used to assign or unassign the specified class on each element.
		/// </para>
		/// </summary>
		/// <param name="name">
		///		The class name to add or remove.
		/// </param>
		/// <param name="func">
		///		Function that is evaluated once per each element in the selection and 
		///		determines whether or not the specified class is associated 
		///		with the selected elements. Truthy implies class will be added; falsey
		///		implies cals will be removed.
		/// </param>
		/// <returns>
		///		The current Selection.
		/// </returns>

		[ScriptName( "classed" )]
		public Selection Classed<DataType> ( string name, DataFunction<DataType, bool> func ) {
			return this;
		}

		/// <summary> Appends a new element with the specified name as the last child of 
		///		each element in the current selection, returning a new selection containing 
		///		the appended elements. Each new element inherits the data of the current 
		///		elements, if any, in the same manner as select for subselections.
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		/// </summary>
		/// <param name="elementName">
		///		The (optionally namespace-qualified) element to create and add.
		/// </param>
		/// <returns></returns>

		public Selection Append ( string elementName ) {
			return this;
		}

		/// <summary> Appends a new element with the specified name as the last child of 
		///		each element in the current selection, returning a new selection containing 
		///		the appended elements. Each new element inherits the data of the current 
		///		elements, if any, in the same manner as select for subselections.
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		/// </summary>
		/// <param name="dataFunction">
		///		A function that will be used to provide elements to be appended. 
		/// </param>
		/// <returns>
		///		Returns the current Selection.
		/// </returns>

		[ScriptName( "append" )]
		public Selection Append<DataType> ( DataFunction<DataType, Element> dataFunction ) {
			// TODO_LATER: verify that the delegate type I'm using here is correct. This is not clearly specified in the API reference.
			return this;
		}

		/// <summary> For each element in the current selection, selects descendant elements 
		///		that match the specified selector string. The returned selection is grouped 
		///		by the ancestor node in the current selection. If no element matches the 
		///		specified selector for the current element, the group at the current index 
		///		will be empty in the returned selection. The subselection does not inherit 
		///		data from the current selection; however, if the data value is specified as 
		///		a function, this function will be based the data d of the ancestor node and 
		///		the group index i.
		///	<para>
		///		Grouping by selectAll also affects subsequent entering placeholder nodes. Thus, 
		///		to specify the parent node when appending entering nodes, use select followed 
		///		by selectAll:
		///	</para>
		///	<para>
		///		d3.select("body").selectAll("div")
		///	</para>
		///	<para>
		///		You can see the parent node of each group by inspecting the parentNode property 
		///		of each group array, such as selection[0].parentNode.
		///	</para>
		///	<para>
		///		The selector may also be specified as a function that returns an array of elements 
		///		(or a NodeList), or the empty array if there are no matching elements. In this case, 
		///		the specified selector is invoked in the same manner as other operator functions, 
		///		being passed the current datum d and index i, with the this context as the current 
		///		DOM element. 
		///	</para>
		/// </summary>
		/// <returns></returns>

		public Selection SelectAll ( string cssSelector ) {
			return this;
		}

		/// <summary> For each element in the current selection, selects descendant elements 
		///		that match the specified selector string. The returned selection is grouped 
		///		by the ancestor node in the current selection. If no element matches the 
		///		specified selector for the current element, the group at the current index 
		///		will be empty in the returned selection. The subselection does not inherit 
		///		data from the current selection; however, if the data value is specified as 
		///		a function, this function will be based the data d of the ancestor node and 
		///		the group index i.
		///	<para>
		///		Grouping by selectAll also affects subsequent entering placeholder nodes. Thus, 
		///		to specify the parent node when appending entering nodes, use select followed 
		///		by selectAll:
		///	</para>
		///	<para>
		///		d3.select("body").selectAll("div")
		///	</para>
		///	<para>
		///		You can see the parent node of each group by inspecting the parentNode property 
		///		of each group array, such as selection[0].parentNode.
		///	</para>
		///	<para>
		///		The selector may also be specified as a function that returns an array of elements 
		///		(or a NodeList), or the empty array if there are no matching elements. In this case, 
		///		the specified selector is invoked in the same manner as other operator functions, 
		///		being passed the current datum d and index i, with the this context as the current 
		///		DOM element. 
		///	</para>
		/// </summary>
		/// <param name="dataFunction">
		///		A function that returns an array-like collection of elements or the empty array.
		/// </param>
		/// <returns></returns>

		[ScriptName( "selectAll" )]
		public Selection SelectAll<DataType> ( DataFunction<DataType, IArrayLike<Element>> dataFunction ) {
			return this;
		}

		/// <summary> For each element in the current selection, selects the first descendant 
		///		element that matches the specified selector string. If no element matches the 
		///		specified selector for the current element, the element at the current index 
		///		will be null in the returned selection; operators (with the exception of data) 
		///		automatically skip null elements, thereby preserving the index of the existing 
		///		selection. If the current element has associated data, this data is inherited 
		///		by the returned subselection, and automatically bound to the newly selected 
		///		elements. If multiple elements match the selector, only the first matching 
		///		element in document traversal order will be selected.
		///	<para>
		///		The selector may also be specified as a function that returns an element, or 
		///		null if there is no matching element. In this case, the specified selector is 
		///		invoked in the same manner as other operator functions, being passed the current 
		///		datum d and index i, with the this context as the current DOM element. 
		///	</para>	
		/// </summary>
		/// <param name="cssSelector">
		///		The Css Selection string used to identify elements.
		/// </param>
		/// <returns>
		///		Returns the current Selection.
		/// </returns>

		public Selection Select ( string cssSelector ) {
			return this;
		}

		/// <summary> For each element in the current selection, selects the first descendant 
		///		element that matches the specified selector string. If no element matches the 
		///		specified selector for the current element, the element at the current index 
		///		will be null in the returned selection; operators (with the exception of data) 
		///		automatically skip null elements, thereby preserving the index of the existing 
		///		selection. If the current element has associated data, this data is inherited 
		///		by the returned subselection, and automatically bound to the newly selected 
		///		elements. If multiple elements match the selector, only the first matching 
		///		element in document traversal order will be selected.
		///	<para>
		///		The selector may also be specified as a function that returns an element, or 
		///		null if there is no matching element. In this case, the specified selector is 
		///		invoked in the same manner as other operator functions, being passed the current 
		///		datum d and index i, with the this context as the current DOM element. 
		///	</para>	
		/// </summary>
		/// <param name="dataFunction">
		///		A function that returns an element or null.
		/// </param>
		/// <returns>
		///		Returns the current selection.
		/// </returns>

		[ScriptName( "select" )]
		public Selection Select<DataType> ( DataFunction<DataType, Element> dataFunction ) {
			return this;
		}

		/// <summary> Joins the specified array of data with the current selection. The specified 
		///		values is an array of data values, such as an array of numbers or objects, or a 
		///		function that returns an array of values. If a key function is not specified, then 
		///		the first datum in the specified array is assigned to the first element in the current 
		///		selection, the second datum to the second selected element, and so on. When data is 
		///		assigned to an element, it is stored in the property __data__, thus making the data 
		///		"sticky" so that the data is available on re-selection.
		///	<para>
		///		The values array specifies the data for each group in the selection. Thus, if the 
		///		selection has multiple groups (such as a d3.selectAll followed by a 
		///		selection.selectAll), then data should be specified as a function that returns an 
		///		array (assuming that you want different data for each group). For example, you may 
		///		bind a two-dimensional array to an initial selection, and then bind the contained 
		///		inner arrays to each subselection. The values function in this case is the identity 
		///		function: it is invoked for each group of child elements, being passed the data 
		///		bound to the parent element, and returns this array of data.
		///	</para>	
		///	<pre>
		///		var matrix = [
		///		  [11975,  5871, 8916, 2868],
		///		  [ 1951, 10048, 2060, 6171],
		///		  [ 8010, 16145, 8090, 8045],
		///		  [ 1013,   990,  940, 6907]
		///		];
		///		
		///		var tr = d3.select("body").append("table").selectAll("tr")
		///		    .data(matrix)
		///		  .enter().append("tr");
		///		
		///		var td = tr.selectAll("td")
		///		    .data(function(d) { return d; })
		///		  .enter().append("td")
		///		    .text(function(d) { return d; });
		///	</pre>	
		///	<para>
		///		To control how data is joined to elements, a key function may be specified. This replaces 
		///		the default by-index behavior; the key function is invoked once for each element in the 
		///		new data array, and once again for each existing element in the selection. In both cases 
		///		the key function is passed the datum d and the index i. When the key function is evaluated 
		///		on new data elements, the this context is the data array; when the key function is evaluated 
		///		on the existing selection, the this context is the associated DOM element. The key function 
		///		returns a string which is used to join a datum with its corresponding element, based on the 
		///		previously-bound data. For example, if each datum has a unique field name, the join might be 
		///		specified as:
		///	</para>	
		///	<pre>
		///		.data(data, function(d) { return d.name; })
		///	</pre>	
		///	<para>
		///		For a more detailed example of how the key function affects the data join, see the tutorial 
		///		A Bar Chart, Part 2.
		///	</para>
		///	<para>
		///		The result of the data operator is the update selection; this represents the selected DOM 
		///		elements that were successfully bound to the specified data elements. The update selection 
		///		also contains a reference to the enter and exit selections, for adding and removing nodes 
		///		in correspondence with data. For example, if the default by-index key is used, and the existing 
		///		selection contains fewer elements than the specified data, then the enter selection will contain 
		///		placeholders for the new data. On the other hand, if the existing contains more elements than 
		///		the data, then the exit selection will contain the extra elements. And, if the existing selection 
		///		exactly matches the data, then both the enter and exit selections will be empty, with all nodes 
		///		in the update selection. For more details, see the short tutorial Thinking With Joins.
		///	</para>
		///	<para>
		///		If a key function is specified, the data operator also affects the index of nodes; this index 
		///		is passed as the second argument i to any operator function values. However, note that existing 
		///		DOM elements are not automatically reordered; use sort or order as needed.
		///	</para>
		///	<para>
		///		If values is not specified, then this method returns the array of data for the first group in 
		///		the selection. The length of the returned array will match the length of the first group, and 
		///		the index of each datum in the returned array will match the corresponding index in the selection. 
		///		If some of the elements in the selection are null, or if they have no associated data, then the 
		///		corresponding element in the array will be undefined.
		///	</para>
		///	<para>
		///		Note: the data method cannot be used to clear previously-bound data; use selection.datum instead.
		///	</para>
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>

		[ScriptName( "data" )]
		public UpdateSelection Data<T> ( T[] data ) {
			return null;
		}

		/// <summary> Joins the specified array of data with the current selection. The specified 
		///		values is an array of data values, such as an array of numbers or objects, or a 
		///		function that returns an array of values. If a key function is not specified, then 
		///		the first datum in the specified array is assigned to the first element in the current 
		///		selection, the second datum to the second selected element, and so on. When data is 
		///		assigned to an element, it is stored in the property __data__, thus making the data 
		///		"sticky" so that the data is available on re-selection.
		///	<para>
		///		The values array specifies the data for each group in the selection. Thus, if the 
		///		selection has multiple groups (such as a d3.selectAll followed by a 
		///		selection.selectAll), then data should be specified as a function that returns an 
		///		array (assuming that you want different data for each group). For example, you may 
		///		bind a two-dimensional array to an initial selection, and then bind the contained 
		///		inner arrays to each subselection. The values function in this case is the identity 
		///		function: it is invoked for each group of child elements, being passed the data 
		///		bound to the parent element, and returns this array of data.
		///	</para>	
		///	<pre>
		///		var matrix = [
		///		  [11975,  5871, 8916, 2868],
		///		  [ 1951, 10048, 2060, 6171],
		///		  [ 8010, 16145, 8090, 8045],
		///		  [ 1013,   990,  940, 6907]
		///		];
		///		
		///		var tr = d3.select("body").append("table").selectAll("tr")
		///		    .data(matrix)
		///		  .enter().append("tr");
		///		
		///		var td = tr.selectAll("td")
		///		    .data(function(d) { return d; })
		///		  .enter().append("td")
		///		    .text(function(d) { return d; });
		///	</pre>	
		///	<para>
		///		To control how data is joined to elements, a key function may be specified. This replaces 
		///		the default by-index behavior; the key function is invoked once for each element in the 
		///		new data array, and once again for each existing element in the selection. In both cases 
		///		the key function is passed the datum d and the index i. When the key function is evaluated 
		///		on new data elements, the this context is the data array; when the key function is evaluated 
		///		on the existing selection, the this context is the associated DOM element. The key function 
		///		returns a string which is used to join a datum with its corresponding element, based on the 
		///		previously-bound data. For example, if each datum has a unique field name, the join might be 
		///		specified as:
		///	</para>	
		///	<pre>
		///		.data(data, function(d) { return d.name; })
		///	</pre>	
		///	<para>
		///		For a more detailed example of how the key function affects the data join, see the tutorial 
		///		A Bar Chart, Part 2.
		///	</para>
		///	<para>
		///		The result of the data operator is the update selection; this represents the selected DOM 
		///		elements that were successfully bound to the specified data elements. The update selection 
		///		also contains a reference to the enter and exit selections, for adding and removing nodes 
		///		in correspondence with data. For example, if the default by-index key is used, and the existing 
		///		selection contains fewer elements than the specified data, then the enter selection will contain 
		///		placeholders for the new data. On the other hand, if the existing contains more elements than 
		///		the data, then the exit selection will contain the extra elements. And, if the existing selection 
		///		exactly matches the data, then both the enter and exit selections will be empty, with all nodes 
		///		in the update selection. For more details, see the short tutorial Thinking With Joins.
		///	</para>
		///	<para>
		///		If a key function is specified, the data operator also affects the index of nodes; this index 
		///		is passed as the second argument i to any operator function values. However, note that existing 
		///		DOM elements are not automatically reordered; use sort or order as needed.
		///	</para>
		///	<para>
		///		If values is not specified, then this method returns the array of data for the first group in 
		///		the selection. The length of the returned array will match the length of the first group, and 
		///		the index of each datum in the returned array will match the corresponding index in the selection. 
		///		If some of the elements in the selection are null, or if they have no associated data, then the 
		///		corresponding element in the array will be undefined.
		///	</para>
		///	<para>
		///		Note: the data method cannot be used to clear previously-bound data; use selection.datum instead.
		///	</para>
		/// </summary>
		/// <typeparam name="DataType"> 
		///		The type of each entry in the data array.
		/// </typeparam>
		/// <typeparam name="SelectedType">
		///		The type of the selected key values used to do the join.
		/// </typeparam>
		/// <param name="data">
		///		An array of data values which will be cound to each group in the selection.
		/// </param>
		/// <param name="keyFunction">
		///		A function that will be called to get keys from the existing elements and from the
		///		data items.
		/// </param>
		/// <returns>
		///		Returns the update selection.
		/// </returns>

		[ScriptName( "data" )]
		public UpdateSelection Data<DataType, SelectedType> (
			DataType[] data,
			DataFunction<DataType, SelectedType> keyFunction
		) {
			return null;
		}

		/// <summary> Joins the specified array of data with the current selection. The specified 
		///		values is an array of data values, such as an array of numbers or objects, or a 
		///		function that returns an array of values. If a key function is not specified, then 
		///		the first datum in the specified array is assigned to the first element in the current 
		///		selection, the second datum to the second selected element, and so on. When data is 
		///		assigned to an element, it is stored in the property __data__, thus making the data 
		///		"sticky" so that the data is available on re-selection.
		///	<para>
		///		The values array specifies the data for each group in the selection. Thus, if the 
		///		selection has multiple groups (such as a d3.selectAll followed by a 
		///		selection.selectAll), then data should be specified as a function that returns an 
		///		array (assuming that you want different data for each group). For example, you may 
		///		bind a two-dimensional array to an initial selection, and then bind the contained 
		///		inner arrays to each subselection. The values function in this case is the identity 
		///		function: it is invoked for each group of child elements, being passed the data 
		///		bound to the parent element, and returns this array of data.
		///	</para>	
		///	<pre>
		///		var matrix = [
		///		  [11975,  5871, 8916, 2868],
		///		  [ 1951, 10048, 2060, 6171],
		///		  [ 8010, 16145, 8090, 8045],
		///		  [ 1013,   990,  940, 6907]
		///		];
		///		
		///		var tr = d3.select("body").append("table").selectAll("tr")
		///		    .data(matrix)
		///		  .enter().append("tr");
		///		
		///		var td = tr.selectAll("td")
		///		    .data(function(d) { return d; })
		///		  .enter().append("td")
		///		    .text(function(d) { return d; });
		///	</pre>	
		///	<para>
		///		To control how data is joined to elements, a key function may be specified. This replaces 
		///		the default by-index behavior; the key function is invoked once for each element in the 
		///		new data array, and once again for each existing element in the selection. In both cases 
		///		the key function is passed the datum d and the index i. When the key function is evaluated 
		///		on new data elements, the this context is the data array; when the key function is evaluated 
		///		on the existing selection, the this context is the associated DOM element. The key function 
		///		returns a string which is used to join a datum with its corresponding element, based on the 
		///		previously-bound data. For example, if each datum has a unique field name, the join might be 
		///		specified as:
		///	</para>	
		///	<pre>
		///		.data(data, function(d) { return d.name; })
		///	</pre>	
		///	<para>
		///		For a more detailed example of how the key function affects the data join, see the tutorial 
		///		A Bar Chart, Part 2.
		///	</para>
		///	<para>
		///		The result of the data operator is the update selection; this represents the selected DOM 
		///		elements that were successfully bound to the specified data elements. The update selection 
		///		also contains a reference to the enter and exit selections, for adding and removing nodes 
		///		in correspondence with data. For example, if the default by-index key is used, and the existing 
		///		selection contains fewer elements than the specified data, then the enter selection will contain 
		///		placeholders for the new data. On the other hand, if the existing contains more elements than 
		///		the data, then the exit selection will contain the extra elements. And, if the existing selection 
		///		exactly matches the data, then both the enter and exit selections will be empty, with all nodes 
		///		in the update selection. For more details, see the short tutorial Thinking With Joins.
		///	</para>
		///	<para>
		///		If a key function is specified, the data operator also affects the index of nodes; this index 
		///		is passed as the second argument i to any operator function values. However, note that existing 
		///		DOM elements are not automatically reordered; use sort or order as needed.
		///	</para>
		///	<para>
		///		If values is not specified, then this method returns the array of data for the first group in 
		///		the selection. The length of the returned array will match the length of the first group, and 
		///		the index of each datum in the returned array will match the corresponding index in the selection. 
		///		If some of the elements in the selection are null, or if they have no associated data, then the 
		///		corresponding element in the array will be undefined.
		///	</para>
		///	<para>
		///		Note: the data method cannot be used to clear previously-bound data; use selection.datum instead.
		///	</para>
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>

		[ScriptName( "data" )]
		public UpdateSelection Data<T> ( DataFunction<Element[], T[]> data ) {
			// TODO_LATER: Verify that this (very poorly specified) operation has been interpreted correctly.
			return null;
		}

		/// <summary> Joins the specified array of data with the current selection. The specified 
		///		values is an array of data values, such as an array of numbers or objects, or a 
		///		function that returns an array of values. If a key function is not specified, then 
		///		the first datum in the specified array is assigned to the first element in the current 
		///		selection, the second datum to the second selected element, and so on. When data is 
		///		assigned to an element, it is stored in the property __data__, thus making the data 
		///		"sticky" so that the data is available on re-selection.
		///	<para>
		///		The values array specifies the data for each group in the selection. Thus, if the 
		///		selection has multiple groups (such as a d3.selectAll followed by a 
		///		selection.selectAll), then data should be specified as a function that returns an 
		///		array (assuming that you want different data for each group). For example, you may 
		///		bind a two-dimensional array to an initial selection, and then bind the contained 
		///		inner arrays to each subselection. The values function in this case is the identity 
		///		function: it is invoked for each group of child elements, being passed the data 
		///		bound to the parent element, and returns this array of data.
		///	</para>	
		///	<pre>
		///		var matrix = [
		///		  [11975,  5871, 8916, 2868],
		///		  [ 1951, 10048, 2060, 6171],
		///		  [ 8010, 16145, 8090, 8045],
		///		  [ 1013,   990,  940, 6907]
		///		];
		///		
		///		var tr = d3.select("body").append("table").selectAll("tr")
		///		    .data(matrix)
		///		  .enter().append("tr");
		///		
		///		var td = tr.selectAll("td")
		///		    .data(function(d) { return d; })
		///		  .enter().append("td")
		///		    .text(function(d) { return d; });
		///	</pre>	
		///	<para>
		///		To control how data is joined to elements, a key function may be specified. This replaces 
		///		the default by-index behavior; the key function is invoked once for each element in the 
		///		new data array, and once again for each existing element in the selection. In both cases 
		///		the key function is passed the datum d and the index i. When the key function is evaluated 
		///		on new data elements, the this context is the data array; when the key function is evaluated 
		///		on the existing selection, the this context is the associated DOM element. The key function 
		///		returns a string which is used to join a datum with its corresponding element, based on the 
		///		previously-bound data. For example, if each datum has a unique field name, the join might be 
		///		specified as:
		///	</para>	
		///	<pre>
		///		.data(data, function(d) { return d.name; })
		///	</pre>	
		///	<para>
		///		For a more detailed example of how the key function affects the data join, see the tutorial 
		///		A Bar Chart, Part 2.
		///	</para>
		///	<para>
		///		The result of the data operator is the update selection; this represents the selected DOM 
		///		elements that were successfully bound to the specified data elements. The update selection 
		///		also contains a reference to the enter and exit selections, for adding and removing nodes 
		///		in correspondence with data. For example, if the default by-index key is used, and the existing 
		///		selection contains fewer elements than the specified data, then the enter selection will contain 
		///		placeholders for the new data. On the other hand, if the existing contains more elements than 
		///		the data, then the exit selection will contain the extra elements. And, if the existing selection 
		///		exactly matches the data, then both the enter and exit selections will be empty, with all nodes 
		///		in the update selection. For more details, see the short tutorial Thinking With Joins.
		///	</para>
		///	<para>
		///		If a key function is specified, the data operator also affects the index of nodes; this index 
		///		is passed as the second argument i to any operator function values. However, note that existing 
		///		DOM elements are not automatically reordered; use sort or order as needed.
		///	</para>
		///	<para>
		///		If values is not specified, then this method returns the array of data for the first group in 
		///		the selection. The length of the returned array will match the length of the first group, and 
		///		the index of each datum in the returned array will match the corresponding index in the selection. 
		///		If some of the elements in the selection are null, or if they have no associated data, then the 
		///		corresponding element in the array will be undefined.
		///	</para>
		///	<para>
		///		Note: the data method cannot be used to clear previously-bound data; use selection.datum instead.
		///	</para>
		/// </summary>
		/// <typeparam name="DataType"> 
		///		The type of each entry in the data array.
		/// </typeparam>
		/// <typeparam name="SelectedType">
		///		The type of the selected key values used to do the join.
		/// </typeparam>
		/// <param name="data">
		///		An array of data values which will be bound to each group in the selection.
		/// </param>
		/// <param name="keyFunction">
		///		A function that will be called to get keys from the existing elements and from the
		///		data items.
		/// </param>
		/// <returns>
		///		Returns the update selection.
		/// </returns>

		[ScriptName( "data" )]
		public UpdateSelection Data<DataType, SelectedType> (
			DataFunction<Element[], DataType[]> data,
			DataFunction<DataType, SelectedType> keyFunction
		) {
			// TODO_LATER: Verify that this (very poorly specified) operation has been interpreted correctly.
			return null;
		}

		/// <summary> Returns the text content of the first non-null element in the current selection.
		/// <para>
		///		This is generally only useful if the selection is known to contain exactly one element.
		/// </para>
		/// </summary>
		/// <returns>
		///		Returns the text content for the first nun-null element in the selection.
		/// </returns>

		public string Text () { return null; }

		/// <summary>The text operator is based on the textContent property; setting 
		///		the text content will replace any existing child elements.
		///	<para>
		///		If value is specified, sets the text content to the specified value on 
		///		all selected elements. If value is a constant, then all elements are 
		///		given the same text content; otherwise, if value is a function, then 
		///		the function is evaluated for each selected element (in order), being 
		///		passed the current datum d and the current index i, with the this context 
		///		as the current DOM element. The function's return value is then used to 
		///		set each element's text content. A null value will clear the content.
		///	</para>	
		///	<para>
		///		If value is not specified, returns the text content for the first non-null 
		///		element in the selection. This is generally useful only if you know the 
		///		selection contains exactly one element.
		///	</para>
		/// </summary>
		/// <param name="value">
		///		The text with which to overwrite the contne of the selected elements.
		/// </param>
		/// <returns>
		///		Returns the current selection.
		/// </returns>

		public Selection Text ( string value ) {
			return this;
		}

		/// <summary>The text operator is based on the textContent property; setting 
		///		the text content will replace any existing child elements.
		///	<para>
		///		If value is specified, sets the text content to the specified value on 
		///		all selected elements. If value is a constant, then all elements are 
		///		given the same text content; otherwise, if value is a function, then 
		///		the function is evaluated for each selected element (in order), being 
		///		passed the current datum d and the current index i, with the this context 
		///		as the current DOM element. The function's return value is then used to 
		///		set each element's text content. A null value will clear the content.
		///	</para>	
		///	<para>
		///		If value is not specified, returns the text content for the first non-null 
		///		element in the selection. This is generally useful only if you know the 
		///		selection contains exactly one element.
		///	</para>
		/// </summary>
		/// <param name="value">
		///		The text with which to overwrite the contne of the selected elements.
		/// </param>
		/// <returns>
		///		Returns the current selection.
		/// </returns>

		[ScriptName( "text" )]
		public Selection Text<DataType> ( DataFunction<DataType, string> value ) {
			return this;
		}

		/// <summary> # selection.style(name[, value[, priority]])
		///	<para>
		///		If value is specified, sets the CSS style property with the specified 
		///		name to the specified value on all selected elements. If value is a 
		///		constant, then all elements are given the same style value; otherwise, 
		///		if value is a function, then the function is evaluated for each 
		///		selected element (in order), being passed the current datum d and 
		///		the current index i, with the this context as the current DOM element. 
		///		The function's return value is then used to set each element's style 
		///		property. A null value will remove the style property. An optional 
		///		priority may also be specified, either as null or the string "important" 
		///		(without the exclamation point).
		///	</para>
		///	<para>
		///		If you want to set several style properties at once, use an object literal 
		///		like so: selection.style({'stroke': 'black', 'stroke-width': 2})
		///	</para>
		///	<para>
		///		If value is not specified, returns the current computed value of the specified 
		///		style property for the first non-null element in the selection. This is 
		///		generally useful only if you know the selection contains exactly one element. 
		///		Note that the computed value may be different than the value that was previously 
		///		set, particularly if the style property was set using a shorthand property 
		///		(such as the "font" style, which is shorthand for "font-size", "font-face", 
		///		etc.).
		///	</para>
		/// </summary>
		/// <typeparam name="DataType"></typeparam>
		/// <param name="propertyName"></param>
		/// <param name="value"></param>
		/// <returns></returns>

		[ScriptName( "style" )]
		public Selection Style<DataType> ( string propertyName, DataFunction<DataType, string> value ) {
			return this;
		}

		/// <summary> # selection.style(name[, value[, priority]])
		///	<para>
		///		If value is specified, sets the CSS style property with the specified 
		///		name to the specified value on all selected elements. If value is a 
		///		constant, then all elements are given the same style value; otherwise, 
		///		if value is a function, then the function is evaluated for each 
		///		selected element (in order), being passed the current datum d and 
		///		the current index i, with the this context as the current DOM element. 
		///		The function's return value is then used to set each element's style 
		///		property. A null value will remove the style property. An optional 
		///		priority may also be specified, either as null or the string "important" 
		///		(without the exclamation point).
		///	</para>
		///	<para>
		///		If you want to set several style properties at once, use an object literal 
		///		like so: selection.style({'stroke': 'black', 'stroke-width': 2})
		///	</para>
		///	<para>
		///		If value is not specified, returns the current computed value of the specified 
		///		style property for the first non-null element in the selection. This is 
		///		generally useful only if you know the selection contains exactly one element. 
		///		Note that the computed value may be different than the value that was previously 
		///		set, particularly if the style property was set using a shorthand property 
		///		(such as the "font" style, which is shorthand for "font-size", "font-face", 
		///		etc.).
		///	</para>
		/// </summary>
		/// <typeparam name="DataType"></typeparam>
		/// <param name="propertyName"></param>
		/// <param name="value"></param>
		/// <returns></returns>

		[ScriptName( "style" )]
		public Selection Style<DataType> ( string propertyName, string value ) {
			return this;
		}

		/// <summary> # selection.style(name[, value[, priority]])
		///	<para>
		///		If value is specified, sets the CSS style property with the specified 
		///		name to the specified value on all selected elements. If value is a 
		///		constant, then all elements are given the same style value; otherwise, 
		///		if value is a function, then the function is evaluated for each 
		///		selected element (in order), being passed the current datum d and 
		///		the current index i, with the this context as the current DOM element. 
		///		The function's return value is then used to set each element's style 
		///		property. A null value will remove the style property. An optional 
		///		priority may also be specified, either as null or the string "important" 
		///		(without the exclamation point).
		///	</para>
		///	<para>
		///		If you want to set several style properties at once, use an object literal 
		///		like so: selection.style({'stroke': 'black', 'stroke-width': 2})
		///	</para>
		///	<para>
		///		If value is not specified, returns the current computed value of the specified 
		///		style property for the first non-null element in the selection. This is 
		///		generally useful only if you know the selection contains exactly one element. 
		///		Note that the computed value may be different than the value that was previously 
		///		set, particularly if the style property was set using a shorthand property 
		///		(such as the "font" style, which is shorthand for "font-size", "font-face", 
		///		etc.).
		///	</para>
		/// </summary>
		/// <param name="values"></param>
		/// <returns></returns>

		public Selection Style ( Dictionary<string, string> values ) {
			return this;
		}

		/// <summary> # selection.style(name[, value[, priority]])
		///	<para>
		///		If value is specified, sets the CSS style property with the specified 
		///		name to the specified value on all selected elements. If value is a 
		///		constant, then all elements are given the same style value; otherwise, 
		///		if value is a function, then the function is evaluated for each 
		///		selected element (in order), being passed the current datum d and 
		///		the current index i, with the this context as the current DOM element. 
		///		The function's return value is then used to set each element's style 
		///		property. A null value will remove the style property. An optional 
		///		priority may also be specified, either as null or the string "important" 
		///		(without the exclamation point).
		///	</para>
		///	<para>
		///		If you want to set several style properties at once, use an object literal 
		///		like so: selection.style({'stroke': 'black', 'stroke-width': 2})
		///	</para>
		///	<para>
		///		If value is not specified, returns the current computed value of the specified 
		///		style property for the first non-null element in the selection. This is 
		///		generally useful only if you know the selection contains exactly one element. 
		///		Note that the computed value may be different than the value that was previously 
		///		set, particularly if the style property was set using a shorthand property 
		///		(such as the "font" style, which is shorthand for "font-size", "font-face", 
		///		etc.).
		///	</para>
		/// </summary>
		/// <returns></returns>

		[ScriptName( "style" )]
		public string GetStyle () {
			return null;
		} 

		/// <summary> # selection.size()
		///	<para>
		///		Returns the total number of elements in the current selection.
		///	</para>
		/// </summary>
		/// <returns>
		///		Returns the total number of elements in the current selection.
		/// </returns>

		public int Size () {
			return 0;
		}

		/// <summary> # selection.node()
		///	<para>
		///		Returns the first non-null element in the current selection. If the selection is empty, returns null.
		///	</para>
		/// </summary>
		/// <returns>
		///		Returns the first non-null element in the current selection. If the selection is empty, returns null.
		/// </returns>

		public Element Node () {
			return null;
		}

		/// <summary> # selection.empty()
		///	<para>
		///		Returns true if the current selection is empty; a selection is empty if it contains no non-null elements.
		///	</para>
		/// </summary>
		/// <returns>
		///		Returns true if the current selection is empty; a selection is empty if it contains no non-null elements.
		/// </returns>

		[ScriptName( "empty" )]
		public bool IsEmpty () {
			return false;
		}

		/// <summary># selection.call(function[, arguments…])
		///	<para>
		///		Invokes the specified function once, passing in the current selection 
		///		along with any optional arguments. The call operator always returns the 
		///		current selection, regardless of the return value of the specified function. 
		///		The call operator is identical to invoking a function by hand; but it makes 
		///		it easier to use method chaining. For example, say we want to set a number 
		///		of attributes the same way in a number of different places. So we take the 
		///		code and wrap it in a reusable function:
		///	</para>	
		///	<pre>
		///		function foo(selection) {
		///		  selection
		///		      .attr("name1", "value1")
		///		      .attr("name2", "value2");
		///		}
		///	</pre>	
		///	<para>
		///		Now, we can say this:
		///	</para>
		///	<pre>
		///		foo(d3.selectAll("div"))
		///	</pre>	
		///	<para>
		///		Or equivalently:
		///	</para>
		///	<pre>
		///		d3.selectAll("div").call(foo);
		///	</pre>
		///	<para>
		///		The this context of the called function is also the current selection. 
		///		This is slightly redundant with the first argument, which we might fix 
		///		in the future.
		///	</para>
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>

		public Selection Call ( SelectionAction action ) {
			return this;
		}

		/// <summary># selection.call(function[, arguments…])
		///	<para>
		///		Invokes the specified function once, passing in the current selection 
		///		along with any optional arguments. The call operator always returns the 
		///		current selection, regardless of the return value of the specified function. 
		///		The call operator is identical to invoking a function by hand; but it makes 
		///		it easier to use method chaining. For example, say we want to set a number 
		///		of attributes the same way in a number of different places. So we take the 
		///		code and wrap it in a reusable function:
		///	</para>	
		///	<pre>
		///		function foo(selection) {
		///		  selection
		///		      .attr("name1", "value1")
		///		      .attr("name2", "value2");
		///		}
		///	</pre>	
		///	<para>
		///		Now, we can say this:
		///	</para>
		///	<pre>
		///		foo(d3.selectAll("div"))
		///	</pre>	
		///	<para>
		///		Or equivalently:
		///	</para>
		///	<pre>
		///		d3.selectAll("div").call(foo);
		///	</pre>
		///	<para>
		///		The this context of the called function is also the current selection. 
		///		This is slightly redundant with the first argument, which we might fix 
		///		in the future.
		///	</para>
		/// </summary>
		/// <param name="action">
		///		The function to perform.
		/// </param>
		/// <param name="args"> 
		///		An array of objects containing the arguments. This will be passed on 
		///		to the action.
		/// </param>
		/// <returns></returns>

		public Selection Call ( SelectionActionWithParam action, object[] args ) {
			return this;
		}

		/// <summary># selection.property(name[, value])
		///	<para>
		///		Some HTML elements have special properties that are not addressable using 
		///		standard attributes or styles. For example, form text fields have a value 
		///		string property, and checkboxes have a checked boolean property. You can 
		///		use the property operator to get or set these properties, or any other 
		///		addressable field on the underlying element, such as className.
		///	</para>
		///	<para>
		///		If value is specified, sets the property with the specified name to the 
		///		specified value on all selected elements. If value is a constant, then 
		///		all elements are given the same property value; otherwise, if value is a 
		///		function, then the function is evaluated for each selected element (in order), 
		///		being passed the current datum d and the current index i, with the this context 
		///		as the current DOM element. The function's return value is then used to set each 
		///		element's property. A null value will delete the specified attribute.
		///	</para>
		///	<para>
		///		<em>Not </em> If value is not specified, returns the value of the specified 
		///		property for the first non-null element in the selection. This is generally 
		///		useful only if you know the selection contains exactly one element.
		///	</para>
		/// </summary>
		/// <typeparam name="T"> Data type of property. </typeparam>
		/// <param name="name"> Property name. </param>
		/// <param name="value"> Property value. </param>
		/// <returns>
		///		REturns the current selection.
		/// </returns>

		[ScriptName( "property" )]
		public Selection Property<T> ( String name, T value ) {
			return this;
		}

		/// <summary># selection.property(name[, value])
		///	<para>
		///		Some HTML elements have special properties that are not addressable using 
		///		standard attributes or styles. For example, form text fields have a value 
		///		string property, and checkboxes have a checked boolean property. You can 
		///		use the property operator to get or set these properties, or any other 
		///		addressable field on the underlying element, such as className.
		///	</para>
		///	<para>
		///		If value is specified, sets the property with the specified name to the 
		///		specified value on all selected elements. If value is a constant, then 
		///		all elements are given the same property value; otherwise, if value is a 
		///		function, then the function is evaluated for each selected element (in order), 
		///		being passed the current datum d and the current index i, with the this context 
		///		as the current DOM element. The function's return value is then used to set each 
		///		element's property. A null value will delete the specified attribute.
		///	</para>
		///	<para>
		///		<em>Not </em> If value is not specified, returns the value of the specified 
		///		property for the first non-null element in the selection. This is generally 
		///		useful only if you know the selection contains exactly one element.
		///	</para>
		/// </summary>
		/// <typeparam name="DataType"> 
		///		The element type in the collection that is bound to this selection. 
		/// </typeparam>
		/// <typeparam name="PropertyType">
		///		The property value type.
		/// </typeparam>
		/// <param name="name"> 
		///		Property name. 
		///	</param>
		/// <param name="value"> 
		///		Function that will supply property values. 
		///	</param>
		/// <returns>
		///		Returns the current selection.
		/// </returns>

		[ScriptName( "property" )]
		public Selection Property<DataType, PropertyType> ( String name, DataFunction<DataType, PropertyType> value ) {
			return this;
		}

		/// <summary># selection.on(type[, listener[, capture]])
		/// <para>
		///		Adds or removes an event listener to each element in the current selection, 
		///		for the specified type. The type is a string event type name, such as "click", 
		///		"mouseover", or "submit". The specified listener is invoked in the same manner 
		///		as other operator functions, being passed the current datum d and index i, with 
		///		the this context as the current DOM element. To access the current event within 
		///		a listener, use the global d3.event. The return value of the event listener is 
		///		ignored. 
		/// </para><para>
		///		If an event listener was already registered for the same type on the selected 
		///		element, the existing listener is removed before the new listener is added. To 
		///		register multiple listeners for the same event type, the type may be followed 
		///		by an optional namespace, such as "click.foo" and "click.bar".
		/// </para><para>
		///		To remove a listener, pass null as the listener. To remove all listeners for a 
		///		particular event type, pass null as the listener, and .type as the type, e.g. 
		///		selection.on(".foo", null).
		/// </para><para>
		///		An optional capture flag may be specified, which corresponds to the W3C useCapture 
		///		flag: "After initiating capture, all events of the specified type will be 
		///		dispatched to the registered EventListener before being dispatched to any 
		///		EventTargets beneath them in the tree. Events which are bubbling upward through 
		///		the tree will not trigger an EventListener designated to use capture."
		/// </para><para>
		///		If listener is not specified, returns the currently-assigned listener for the 
		///		specified type, if any.
		/// </para>
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="eventType"></param>
		/// <param name="listener"></param>
		/// <param name="capture"></param>
		/// <returns></returns>

		[ScriptName("on")]
		public Selection On<T> ( string eventType, DataAction<T> listener, bool capture ) {
			return this;
		}

		/// <summary># selection.on(type[, listener[, capture]])
		/// <para>
		///		Adds or removes an event listener to each element in the current selection, 
		///		for the specified type. The type is a string event type name, such as "click", 
		///		"mouseover", or "submit". The specified listener is invoked in the same manner 
		///		as other operator functions, being passed the current datum d and index i, with 
		///		the this context as the current DOM element. To access the current event within 
		///		a listener, use the global d3.event. The return value of the event listener is 
		///		ignored. 
		/// </para><para>
		///		If an event listener was already registered for the same type on the selected 
		///		element, the existing listener is removed before the new listener is added. To 
		///		register multiple listeners for the same event type, the type may be followed 
		///		by an optional namespace, such as "click.foo" and "click.bar".
		/// </para><para>
		///		To remove a listener, pass null as the listener. To remove all listeners for a 
		///		particular event type, pass null as the listener, and .type as the type, e.g. 
		///		selection.on(".foo", null).
		/// </para><para>
		///		An optional capture flag may be specified, which corresponds to the W3C useCapture 
		///		flag: "After initiating capture, all events of the specified type will be 
		///		dispatched to the registered EventListener before being dispatched to any 
		///		EventTargets beneath them in the tree. Events which are bubbling upward through 
		///		the tree will not trigger an EventListener designated to use capture."
		/// </para><para>
		///		If listener is not specified, returns the currently-assigned listener for the 
		///		specified type, if any.
		/// </para>
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="eventType"></param>
		/// <param name="listener"></param>
		/// <returns></returns>

		[ScriptName("on")]
		public Selection On<T> ( string eventType, DataAction<T> listener ) {
			return this;
		}

		/// <summary># selection.insert(name[, before])
		///	<para>
		///		Inserts a new element with the specified name before the element matching 
		///		the specified before selector, for each element in the current selection, 
		///		returning a new selection containing the inserted elements. If the before 
		///		selector does not match any elements, then the new element will be the last 
		///		child as with append. Each new element inherits the data of the current elements 
		///		(if any), in the same manner as select for subselections.
		///	</para>
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		///	<para>
		///		Likewise, the before selector may be specified as a selector string or a 
		///		function which returns a DOM element. For instance, insert("div", ":first-child") 
		///		will prepend child div nodes to the current selection. For enter selections, the 
		///		before selector may be omitted, in which case entering elements will be inserted 
		///		immediately before the next following sibling in the update selection, if any. 
		///		This allows you to insert elements into the DOM in an order consistent with bound 
		///		data. Note, however, the slower selection.order may still be required if updating 
		///		elements change order.
		///	</para>
		/// </summary>
		/// <param name="elementName"></param>
		/// <returns></returns>

		public Selection Insert ( string elementName ) {
			return null;
		}

		/// <summary># selection.insert(name[, before])
		///	<para>
		///		Inserts a new element with the specified name before the element matching 
		///		the specified before selector, for each element in the current selection, 
		///		returning a new selection containing the inserted elements. If the before 
		///		selector does not match any elements, then the new element will be the last 
		///		child as with append. Each new element inherits the data of the current elements 
		///		(if any), in the same manner as select for subselections.
		///	</para>
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		///	<para>
		///		Likewise, the before selector may be specified as a selector string or a 
		///		function which returns a DOM element. For instance, insert("div", ":first-child") 
		///		will prepend child div nodes to the current selection. For enter selections, the 
		///		before selector may be omitted, in which case entering elements will be inserted 
		///		immediately before the next following sibling in the update selection, if any. 
		///		This allows you to insert elements into the DOM in an order consistent with bound 
		///		data. Note, however, the slower selection.order may still be required if updating 
		///		elements change order.
		///	</para>
		/// </summary>
		/// <param name="elementName"></param>
		/// <param name="cssSelector"></param>
		/// <returns></returns>

		public Selection Insert ( string elementName, string cssSelector ) {
			return null;
		}

		/// <summary># selection.insert(name[, before])
		///	<para>
		///		Inserts a new element with the specified name before the element matching 
		///		the specified before selector, for each element in the current selection, 
		///		returning a new selection containing the inserted elements. If the before 
		///		selector does not match any elements, then the new element will be the last 
		///		child as with append. Each new element inherits the data of the current elements 
		///		(if any), in the same manner as select for subselections.
		///	</para>
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		///	<para>
		///		Likewise, the before selector may be specified as a selector string or a 
		///		function which returns a DOM element. For instance, insert("div", ":first-child") 
		///		will prepend child div nodes to the current selection. For enter selections, the 
		///		before selector may be omitted, in which case entering elements will be inserted 
		///		immediately before the next following sibling in the update selection, if any. 
		///		This allows you to insert elements into the DOM in an order consistent with bound 
		///		data. Note, however, the slower selection.order may still be required if updating 
		///		elements change order.
		///	</para>
		/// </summary>
		/// <param name="dataFunction"></param>
		/// <returns></returns>

		[ScriptName( "insert" )]
		public Selection Insert<DataType> ( DataFunction<DataType, Element> dataFunction ) {
			// TODO_LATER: verify that this is the right data type.
			return null;
		}

		/// <summary># selection.insert(name[, before])
		///	<para>
		///		Inserts a new element with the specified name before the element matching 
		///		the specified before selector, for each element in the current selection, 
		///		returning a new selection containing the inserted elements. If the before 
		///		selector does not match any elements, then the new element will be the last 
		///		child as with append. Each new element inherits the data of the current elements 
		///		(if any), in the same manner as select for subselections.
		///	</para>
		///	<para>
		///		The name may be specified either as a constant string or as a function that 
		///		returns the DOM element to append. When the name is specified as a string, 
		///		it may have a namespace prefix of the form "namespace:tag". For example, 
		///		"svg:text" will create a "text" element in the SVG namespace. By default, 
		///		D3 supports svg, xhtml, xlink, xml and xmlns namespaces. Additional namespaces 
		///		can be registered by adding to d3.ns.prefix. If no namespace is specified, 
		///		then the namespace will be inherited from the enclosing element; or, if the 
		///		name is one of the known prefixes, the corresponding namespace will be used 
		///		(for example, "svg" implies "svg:svg").
		///	</para>
		///	<para>
		///		Likewise, the before selector may be specified as a selector string or a 
		///		function which returns a DOM element. For instance, insert("div", ":first-child") 
		///		will prepend child div nodes to the current selection. For enter selections, the 
		///		before selector may be omitted, in which case entering elements will be inserted 
		///		immediately before the next following sibling in the update selection, if any. 
		///		This allows you to insert elements into the DOM in an order consistent with bound 
		///		data. Note, however, the slower selection.order may still be required if updating 
		///		elements change order.
		///	</para>
		/// </summary>
		/// <param name="dataFunction"></param>
		/// <param name="cssSelector"></param>
		/// <returns></returns>

		[ScriptName( "insert" )]
		public Selection Insert<DataType> ( DataFunction<DataType, Element> dataFunction, string cssSelector ) {
			return null;
		}

		/// <summary> # selection.remove()
		///	<para>
		///		Removes the elements in the current selection from the current document. Returns 
		///		the current selection (the same elements that were removed) which are now “off-screen”, 
		///		detached from the DOM. Note that there is not currently a dedicated API to add removed 
		///		elements back to the document; however, you can pass a function to selection.append or 
		///		selection.insert to re-add elements.
		///	</para>
		/// </summary>
		/// <returns>
		///		Returns the current selection.
		/// </returns>

		public Selection Remove () {
			return this;
		}

		/// <summary> # selection.sort(comparator)
		///	<para>
		///		Sorts the elements in the current selection according to the specified comparator 
		///		function. The comparator function is passed two data elements a and b to compare, 
		///		returning either a negative, positive, or zero value. If negative, then a should 
		///		be before b; if positive, then a should be after b; otherwise, a and b are considered 
		///		equal and the order is arbitrary. Note that the sort is not guaranteed to be stable; 
		///		however, it is guaranteed to have the same behavior as your browser's built-in sort 
		///		method on arrays.
		///	</para>
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="compare"></param>
		/// <returns></returns>

		[ScriptName( "sort" )]
		public Selection Sort<T> ( CompareCallback<T> compare ) {
			return this;
		}

		/// <summary># selection.order()
		/// <para>
		///		Re-inserts elements into the document such that the document order matches 
		///		the selection order. This is equivalent to calling sort() if the data is 
		///		already sorted, but much faster.
		/// </para>
		/// </summary>
		/// <returns></returns>

		public Selection Order() {
			return this;
		}

		/// <summary> # selection.transition()
		/// <para>
		///		Starts a transition for the current selection. Transitions behave much like selections, 
		///		except operators animate smoothly over time rather than applying instantaneously.
		/// </para>
		/// </summary>
		/// <returns></returns>

		public Transition Transition () { return null; }

		/// <summary># selection.interrupt()
		/// <para>
		///		Immediately interrupts the current transition, if any. Does not cancel any scheduled 
		///		transitions that have not yet started. To cancel scheduled transitions as well, simply 
		///		create a new zero-delay transition after interrupting the current transition:
		/// </para>
		/// <example>
		///		selection
		///		    .interrupt() // cancel the current transition
		///		    .transition(); // preempt any scheduled transitions
		/// </example>		
		/// </summary>
		/// <returns></returns>

		public Selection Interrupt () { return this; }

		/// <summary># selection.filter(selector)
		///	<para>
		/// Filters the selection, returning a new selection that contains only the elements for which the specified selector is true. The selector may be specified either as a function or as a selector string, such as ".foo". As with other operators, the function is passed the current datum d and index i, with the this context as the current DOM element. Like the built-in array filter method, the returned selection does not preserve the index of the original selection; it returns a copy with elements removed. If you want to preserve the index, use select instead. For example, to select every odd element (relative to the zero-based index):
		///	</para>
		/// <para>
		///		var odds = selection.select(function(d, i) { return i &amp; 1 ? this : null; });
		/// </para>
		/// <para>
		///		Equivalently, using a filter function:
		/// </para>
		/// <para>
		///		var odds = selection.filter(function(d, i) { return i &amp; 1; });
		/// </para>
		/// <para>
		///		Or a filter selector (noting that the :nth-child pseudo-class is a one-based index rather than a zero-based index):
		/// </para>
		/// <para>
		///		var odds = selection.filter(":nth-child(even)");
		/// </para>
		/// <para>
		///		Thus, you can use either select or filter to apply operators to a subset of elements.
		/// </para>
		/// </summary>
		/// <param name="cssSelector"></param>
		/// <returns></returns>

		public Selection Filter( string cssSelector ) {
			return null;
		}

		/// <summary># selection.filter(selector)
		///	<para>
		/// Filters the selection, returning a new selection that contains only the elements for which the specified selector is true. The selector may be specified either as a function or as a selector string, such as ".foo". As with other operators, the function is passed the current datum d and index i, with the this context as the current DOM element. Like the built-in array filter method, the returned selection does not preserve the index of the original selection; it returns a copy with elements removed. If you want to preserve the index, use select instead. For example, to select every odd element (relative to the zero-based index):
		///	</para>
		/// <para>
		///		var odds = selection.select(function(d, i) { return i &amp; 1 ? this : null; });
		/// </para>
		/// <para>
		///		Equivalently, using a filter function:
		/// </para>
		/// <para>
		///		var odds = selection.filter(function(d, i) { return i &amp; 1; });
		/// </para>
		/// <para>
		///		Or a filter selector (noting that the :nth-child pseudo-class is a one-based index rather than a zero-based index):
		/// </para>
		/// <para>
		///		var odds = selection.filter(":nth-child(even)");
		/// </para>
		/// <para>
		///		Thus, you can use either select or filter to apply operators to a subset of elements.
		/// </para>
		/// </summary>
		/// <param name="filter"></param>
		/// <returns></returns>

		[ScriptName( "filter" )]
		public Selection Filter<DataType>( DataFunction<DataType,bool> filter ) {
			return null;
		}

		/// <summary> Gets the data item bound to the first non-null element in the selection.
		/// </summary>
		/// <typeparam name="DataType"></typeparam>
		/// <returns></returns>

		[ScriptName( "datum" )]
		public DataType GetDatum<DataType> () {
			return (DataType) Script.Undefined;
		}

		/// <summary> Sets the data item bound to each element in the selection 
		///		to a (common) fixed value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value"></param>
		/// <returns></returns>

		[ScriptName( "datum" )]
		public Selection Datum<T> ( T value ) {
			return this;
		}

		/// <summary> Sets the data item bound to each element in the selection 
		///		to a computed value.
		///	<para>
		///		Gets or sets the bound data for each selected element. Unlike the selection.data method, this method does not compute a join (and thus does not compute enter and exit selections). 
		///	</para>
		///	<para>
		///		If value is specified, sets the element's bound data to the specified value on all selected elements. If value is a constant, all elements are given the same data; otherwise, if value is a function, then the function is evaluated for each selected element, being passed the previous datum d and the current index i, with the this context as the current DOM element. The function is then used to set each element's data. A null value will delete the bound data. This operator has no effect on the index.
		///	</para>
		///	<para>
		///		If value is not specified, returns the bound datum for the first non-null element in the selection. This is generally useful only if you know the selection contains exactly one element.
		///	</para>
		/// </summary>
		/// <typeparam name="OriginalType"></typeparam>
		/// <typeparam name="NewType"></typeparam>
		/// <param name="f"></param>
		/// <returns></returns>

		[ScriptName( "datum" )]
		public Selection Datum<OriginalType,NewType> ( DataFunction<OriginalType,NewType> f ) {
			return this;
		}
	}
}
