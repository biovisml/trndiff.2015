// Class1.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using System.Runtime.CompilerServices;

namespace D3JS {

	[ScriptImport, ScriptIgnoreNamespace]
	public class D3 {

		/// <summary> Gets a reference to the global D3 object.
		/// <para> Note: this may be of little practical use, we shall see.</para>
		/// </summary>

		[ScriptField, ScriptAlias( "d3" )]
		public static D3 Current { get { return null; } }

		/// <summary> # d3.event
		/// <para>
		///		Stores the current event, if any. This global is during an event listener callback 
		///		registered with the on operator. The current event is reset after the listener is 
		///		notified in a finally block. This allows the listener function to have the same form 
		///		as other operator functions, being passed the current datum d and index i.
		/// </para>
		/// <para>
		///		The d3.event object is a DOM event and implements the standard event fields like 
		///		timeStamp and keyCode as well as methods like preventDefault() and stopPropagation(). 
		///		While you can use the native event's pageX and pageY, it is often more convenient to 
		///		transform the event position to the local coordinate system of the container that 
		///		received the event. For example, if you embed an SVG in the normal flow of your page, 
		///		you may want the event position relative to the top-left corner of the SVG image. 
		///		If your SVG contains transforms, you might also want to know the position of the event 
		///		relative to those transforms. Use the d3.mouse operator for the standard mouse pointer, 
		///		and use d3.touches for multitouch events on iOS.
		/// </para>
		/// </summary>

		[ScriptField, ScriptAlias( "d3.event" )]
		public static ElementEvent Event { get { return null; } }

		/// <summary># d3.mouse(container)
		/// <para>
		///		Returns the x and y coordinates of the current d3.event, relative to the 
		///		specified container. The container may be an HTML or SVG container element, 
		///		such as an svg:g or svg:svg. The coordinates are returned as a two-element 
		///		array [ x, y].
		/// </para>
		/// </summary>

		[ScriptField, ScriptAlias( "d3.mouse" )]
		public static double [] Mouse  { get { return null; } }

		/// <summary># d3.touches(container[, touches])
		/// <para>
		///		Returns the x and y coordinates of each touch associated with the current d3.event, 
		///		based on the touches attribute, relative to the specified container. The container 
		///		may be an HTML or SVG container element, such as an svg:g or svg:svg. The coordinates 
		///		are returned as an array of two-element arrays [ [ x1, y1], [ x2, y2], � ]. If 
		///		touches is specified, returns the positions of the specified touches; if touches 
		///		is not specified, it defaults to the touches property on the current event.
		/// </para>
		/// </summary>

		[ScriptField, ScriptAlias( "d3.touches" )]
		public static double [][] Touches  { get { return null; } }

		/// <summary> Selects the first element that matches the specified selector string, 
		///		returning a single-element selection. If no elements in the current document 
		///		match the specified selector, returns the empty selection. If multiple 
		///		elements match the selector, only the first matching element (in document 
		///		traversal order) will be selected.
		/// </summary>
		/// <param name="selector"> A CSS selector. </param>
		/// <returns>
		///		Returns a Selection which can be used to construct and manipulate a 
		///		collection of Elements matching the selector. 
		///	</returns>

		public Selection Select ( string selector ) {
			return null;
		}

		/// <summary>Selects the specified node. This is useful if you already have a reference 
		///		to a node, such as d3.select(this) within an event listener, or a global such as 
		///		document.body.
		/// </summary>
		/// <param name="element"> The element to select. </param>
		/// <returns>
		///		Returns a Selection which can be used to manipulate the Element. 
		///	</returns>

		public Selection Select ( Element element ) {
			return null;
		}

		/// <summary> Selects all elements that match the specified selector. The elements will be 
		///		selected in document traversal order (top-to-bottom). If no elements in the current 
		///		document match the specified selector, returns the empty selection.
		/// </summary>
		/// <param name="selector"> 
		///		A CSS selector which defines a collection of elements.
		/// </param>
		/// <returns>
		///		Returns a Selection which can be used to construct and manipulate a 
		///		collection of Elements matching the selector. 
		///	</returns>

		public Selection SelectAll ( string selector ) {
			return null;
		}

		/// <summary> Selects the specified array of elements. This is useful if you already have a 
		///		reference to nodes, such as d3.selectAll(this.childNodes) within an event listener, 
		///		or a global such as document.links. The nodes argument doesn't have to be an array, 
		///		exactly; any pseudo-array that can be coerced into an array (e.g., a NodeList or 
		///		arguments) will work.
		/// </summary>
		/// <param name="nodes"> 
		///		An array-like collection of elements which are to be handled as a Selection. 
		///	</param>
		/// <returns>
		///		Returns a Selection which can be used to manipulate the specified Elements. 
		///	</returns>

		public Selection SelectAll ( IArrayLike<Element> nodes ) { return null; }
	}
}
