﻿// AssemblyInfo.cs
//

using System;
using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "Hammer_Client" )]
[assembly: AssemblyDescription( "Script sharp wrapper for Hammer.js" )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "QUT" )]
[assembly: AssemblyProduct( "TRNDiff" )]
[assembly: AssemblyCopyright( "Copyright © QUT 2016" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

[assembly: ScriptAssembly( "" )]

#if false
// A script template providing an AMD pattern-based structure around
// the generated script.
[assembly: ScriptTemplate( @"
/*! {name}.js {version}
 * {description}
 */

""use strict"";

define('{name}', [{requires}], function({dependencies}) {
  var $global = this;
  var $exports = null;
  {script}
  return $exports;
});
" )]

#endif