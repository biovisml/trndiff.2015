﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using SystemQut;

namespace Hammer {
	/// <summary>
	/// Base class for all Recognizers.
	/// </summary>

	[ScriptImport]
	public abstract class Recognizer {

		/// <summary>
		/// Create a new Recogniser.
		/// </summary>
		public Recognizer (){}
		
		/// <summary>
		/// Create a new Recogniser and set options.
		/// </summary>
		/// <param name="options">
		///		A collection of options.
		///		<b>TODO: Figure out what type this should really be.</b>
		/// </param>
		public Recognizer ( JSObject options ){}

		/// <summary>
		/// Change an option on the recognizer instance. Using this method is recommended, because it will update the touchAction value if needed.
		/// </summary>
		/// <param name="options">
		///		A collection of options.
		///		<b>TODO: Figure out what type this should really be.</b>
		/// </param>
		public void Set( JSObject options ) { }

		/// <summary>
		/// Run the recognizer simultaneous with the given other recognizer, in both directions. This is usable for like combining a pan with a swipe at the end, or a pinch with the ability to rotate the target as well. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// If the recognizer is added to a manager, then this method also accepts the other recognizer's event name as a string.
		/// </summary>
		/// <param name="other"></param>
		public void RecognizeWith( Recognizer other ) { }

		/// <summary>
		/// Run the recognizer simultaneous with the given other recognizer, in both directions. This is usable for like combining a pan with a swipe at the end, or a pinch with the ability to rotate the target as well. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// If the recognizer is added to a manager, then this method also accepts the other recognizer's event name as a string.
		/// </summary>
		/// <param name="other"></param>
		public void RecognizeWith( Recognizer [] other ) { }

		/// <summary>
		/// Run the recognizer simultaneous with the given other recognizer, in both directions. This is usable for like combining a pan with a swipe at the end, or a pinch with the ability to rotate the target as well. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// If the recognizer is added to a manager, then this method also accepts the other recognizer's event name as a string.
		/// </summary>
		/// <param name="other"></param>
		public void RecognizeWith( string other ) { }

		/// <summary>
		///  Ceases to run the recognizer simultaneous with the given other recognizer, in both directions. This is usable for like combining a pan with a swipe at the end, or a pinch with the ability to rotate the target as well. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// </summary>
		/// <param name="other"></param>
		public void DropRecognizeWith( Recognizer other ) { }

		/// <summary>
		///  Ceases to run the recognizer simultaneous with the given other recognizer, in both directions. This is usable for like combining a pan with a swipe at the end, or a pinch with the ability to rotate the target as well. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// </summary>
		/// <param name="other"></param>
		public void DropRecognizeWith( Recognizer [] other ) { }

		/// <summary>
		/// Run the recognizer only when the other recognizer fails. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// If the recognizer is added to a manager, then this method also accepts the other recognizer's event name as a string.
		/// </summary>
		/// <param name="other"></param>
		public void RequireFailure( Recognizer other ) { }

		/// <summary>
		/// Run the recognizer only when the other recognizer fails. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// If the recognizer is added to a manager, then this method also accepts the other recognizer's event name as a string.
		/// </summary>
		/// <param name="other"></param>
		public void RequireFailure( Recognizer [] other ) { }

		/// <summary>
		/// Run the recognizer only when the other recognizer fails. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// If the recognizer is added to a manager, then this method also accepts the other recognizer's event name as a string.
		/// </summary>
		/// <param name="other"></param>
		public void RequireFailure( string other ) { }

		/// <summary>
		/// Ceases to run the recognizer only when the other recognizer fails. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// If the recognizer is added to a manager, then this method also accepts the other recognizer's event name as a string.
		/// </summary>
		/// <param name="other"></param>
		public void DropRequireFailure( Recognizer other ) { }

		/// <summary>
		/// Ceases to run the recognizer only when the other recognizer fails. Dropping the connection only removes the link on the recognizer, not on the other recognizer. Both accept an array of recognizers.
		/// If the recognizer is added to a manager, then this method also accepts the other recognizer's event name as a string.
		/// </summary>
		/// <param name="other"></param>
		public void DropRequireFailure( Recognizer [] other ) { }
	}
}
