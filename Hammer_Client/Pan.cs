﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using SystemQut;

namespace Hammer {
	[ScriptImport, ScriptIgnoreNamespace, ScriptName( "Hammer.Pan" )]
	public class Pan : Recognizer {
		public Pan() { }

		public Pan( PanOptions options ) { }

		public Pan( JSObject options ) { }
	}

	[ScriptImport, ScriptIgnoreNamespace]
	public class PanOptions {
		[ScriptField]
		public string Event { get { return ""; } set { } }

		[ScriptField]
		public int Pointers { get { return 0; } set { } }

		[ScriptField]
		public int Threshold { get { return 10; } set { } }

		[ScriptField]
		public JSObject Direction { get { return null; } set { } }

	}
}
