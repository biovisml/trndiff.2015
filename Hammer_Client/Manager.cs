﻿using System;
using System.Runtime.CompilerServices;
using System.Html;
using SystemQut;

namespace Hammer {
	public delegate void HammerEventListener ( JSObject ev );

	[ScriptImport, ScriptIgnoreNamespace, ScriptName("Hammer.Manager")]
	public class Manager {

		public Manager( Element element ) { }

		public Manager( Element element, JSObject options ) { }

		public void On( string eventTypes, HammerEventListener eventHandler ) { }

		public void Off( string eventTypes ) { }

		public void Add( Recognizer[] recognizer ) { }

		public void Add( Recognizer recognizer ) { }

		// TODO: Add the remaining operations from http://hammerjs.github.io/api
		// Set, Get, Remove( Recognizer), Remove( Recognizer [] )

        public void Destroy () { }

        public void Emit ( string eventName, JSObject data ) { }

        //Can return null
        public Recognizer Get ( Recognizer recognizer ) { return null; }
        
        //Can return null
        public Recognizer Get ( string eventName ) { return null; }

        public void Recognise( JSObject inputData ) { }

        public Manager Remove ( Recognizer recognizer ) { return null; }

        public Manager Remove ( string eventName ) { return null; }

        public Manager Set ( JSObject options ) { return null; }
        
        public void Stop ( bool force ) { }
	}

	[ScriptImport, ScriptIgnoreNamespace]
	public class Hammer: Manager {
		public Hammer ( Element element ) : base ( element ) {}

		public Hammer ( Element element, JSObject options ) : base ( element, options ) {}

        public const string VERSION = "2.0.6";

        public const int DIRECTION_NONE = 1;
        public const int DIRECTION_LEFT = 2;
        public const int DIRECTION_RIGHT = 4;
        public const int DIRECTION_UP = 8;
        public const int DIRECTION_DOWN = 16;
        public const int DIRECTION_HORIZONTAL = 6;
        public const int DIRECTION_VERTICAL = 24;
        public const int DIRECTION_ALL = 30;
        
        public const int INPUT_START = 1;
        public const int INPUT_MOVE = 2;
        public const int INPUT_END = 4;
        public const int INPUT_CANCEL = 8;

        public const int STATE_POSSIBLE = 1;
        public const int STATE_BEGAN = 2;
        public const int STATE_CHANGED = 4;
        public const int STATE_ENDED = 8;
        public const int STATE_RECOGNIZED = STATE_ENDED;
        public const int STATE_CANCELLED = 16;
        public const int STATE_FAILED = 32;
	}
}
