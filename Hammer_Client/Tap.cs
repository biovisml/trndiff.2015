﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using SystemQut;

namespace Hammer {
	[ScriptImport, ScriptIgnoreNamespace, ScriptName( "Hammer.Tap" )]
	public class Tap : Recognizer {
		public Tap() { }

		public Tap( TapOptions options ) { }

		public Tap( JSObject options ) { }
	}

	[ScriptImport, ScriptIgnoreNamespace]
	public class TapOptions {
		[ScriptField]
		public string Event { get { return ""; } set { } }

		[ScriptField]
		public int Taps { get { return 0; } set { } }
	}
}
