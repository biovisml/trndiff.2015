﻿using System;
using System.Collections.Generic;

using RegPrecise;
using SystemQut;
using RegulonExplorer.Common;

namespace RegulonExplorer.ViewModel {

	/// <summary> SiteInfo is a selectable wrapper for a Site.
	/// </summary>

	public class SiteInfo : Selectable, IEquatable<SiteInfo> {
		private  Site site;
		private  GeneInfo gene = null;

		public SiteInfo ( Site site ) {
			this.site = site;
		}

		public string GeneLocusTag { get { return site.GeneLocusTag; } }

		public Number GeneVimssid { get { return site.GeneVIMSSId; } }

		public Number Position { get { return site.Position; } }

		public Number RegulonId { get { return site.RegulonId; } }

		public Number Score { get { return site.Score; } }

		public string Sequence { get { return site.Sequence; } }

		/// <summary> Get or set the GeneInfo object with which this site is associated.
		/// </summary>

		public GeneInfo Gene {
			get { return gene; }
			set {
				if ( value == gene ) return;

				if ( Script.IsValue( value ) && value.VimssId != GeneVimssid ) {
					if (Constants.ShowDebugMessages) Console.Log( string.Format( Constants.GENE_AND_SITE_ID_MUST_MATCH,
						value.VimssId,
						GeneVimssid
					) );
				}
				
				this.gene = value;
				NotifyPropertyChanged( "Gene" );
			}
		}

		/// <summary> Returns true iff this object matches another by GeneVimssid.
		/// </summary>
		/// <param name="other">
		///		A (possibly null) reference to a SiteInfo object.
		/// </param>
		/// <returns></returns>

		public bool Equals ( SiteInfo other ) {
			return other == null ? false : other.GeneVimssid == GeneVimssid;
		}

		/// <summary> Gets the RegPrecise Site record associated with this object.
		/// </summary>

		public Site Site {
			get { return site; }
		}

		/// <summary> Wrapper for Site.Parse.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public static SiteInfo Parse ( JSObject obj ) {
			return new SiteInfo( new Site( obj ) );
		}
	}
}
