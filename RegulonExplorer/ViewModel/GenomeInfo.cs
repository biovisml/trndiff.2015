﻿using System;
using System.Collections.Generic;

using RegPrecise;
using System.ComponentModel;
using SystemQut.ComponentModel;
using SystemQut.ServiceModel;
using System.Html;
using SystemQut;

namespace RegulonExplorer.ViewModel {

	/// <summary> The infromation required to display the summary of a genome.
	/// </summary>

	public class GenomeInfo : Selectable {
		private GenomeStats genomeStats;
		private Dictionary<int,RegulonInfo> regulons = new Dictionary<int, RegulonInfo>();
		private string sourceId;

		/// <summary> Initialises genome, saving the data and the source collection id.
		/// </summary>
		/// <param name="genomeStats"></param>
		/// <param name="sourceId"></param>

		public GenomeInfo (
			GenomeStats genomeStats,
			string sourceId
		) {
			Arguments args = Arguments.Current;
			this.genomeStats = genomeStats;
			this.sourceId = args.Length == 1 ? DataLayer.RegPreciseId : sourceId;
		}

		/// <summary> Initialises genome, saving the data and the default RegPrecise collection Id.
		/// </summary>
		/// <param name="genomeStats"></param>

		extern public GenomeInfo (
			GenomeStats genomeStats
		);

		public GenomeStats GenomeStats {
			get { return genomeStats; }
			set {
				if ( genomeStats == value ) return;

				genomeStats = value;

				NotifyPropertyChanged( "GenomeId,Name,TaxonomyId,TfRegulonCount,TfSiteCount,RnaRegulonCount,RnaSiteCount" );
			}
		}

		/// <summary> genome identifier
		/// </summary>

		public int GenomeId { get { return genomeStats.GenomeId; } }

		/// <summary> genome name
		/// </summary>

		public string Name { get { return genomeStats.Name; } }

		/// <summary> NCBI taxonomy id
		/// </summary>

		public int TaxonomyId { get { return genomeStats.TaxonomyId; } }

		/// <summary> total number of TF-controlled regulators reconstructed in genome
		/// </summary>

		public int TfRegulonCount { get { return genomeStats.TfRegulonCount; } }

		/// <summary> total number of TF binding sites in genome
		/// </summary>

		public int TfSiteCount { get { return genomeStats.TfSiteCount; } }

		/// <summary> total number of RNA-controlled regulators reconstructed in genome
		/// </summary>

		public int RnaRegulonCount { get { return genomeStats.RnaRegulonCount; } }

		/// <summary> total number of RNA regulatory sites in genome
		/// </summary>

		public int RnaSiteCount { get { return genomeStats.RnaSiteCount; } }

		/// <summary> Get or set the collection of regulons associated with a Genome.
		/// <para>
		///		Setting this fires NotifyPropertyChanged.
		/// </para>
		/// </summary>

		public RegulonInfo[] Regulons {
			get {
				List<RegulonInfo> result = new List<RegulonInfo>();

				foreach ( int id in regulons.Keys ) {
					result.Add( regulons[id] );
				}

				return result;
			}
			set {
				regulons.Clear();

				if ( value != null ) {
					foreach ( RegulonInfo regulon in value ) {
						AddRegulon( regulon );
					}
				}

				this.NotifyPropertyChanged( "Regulons" );
			}
		}

		public void AddRegulon (
			RegulonInfo regulon
		) {
			regulons[regulon.RegulonId] = regulon;
			regulon.Genome = this;
		}

		public static GenomeInfo Parse ( JSObject obj ) {
			return new GenomeInfo( new GenomeStats( obj ) );
		}

		/// <summary> Get the source collection Id.
		/// </summary>

		public string SourceId {
			get { return sourceId; }
		}

		internal RegulonInfo GetRegulon ( Number regulonId ) {
			return regulons[regulonId];
		}
	}
}
