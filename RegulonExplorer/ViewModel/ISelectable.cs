// ISelectable.cs
//

using System;
using System.Collections.Generic;

namespace RegulonExplorer.ViewModel {

	public interface ISelectable {
		bool IsSelected { get; set; }
	}
}
