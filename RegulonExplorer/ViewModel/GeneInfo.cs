﻿using System;
using System.Collections.Generic;
using RegPrecise;
using SystemQut;
using RegulonExplorer.Common;
using SystemQut.ComponentModel;

namespace RegulonExplorer.ViewModel {
	public class GeneInfo : Selectable, IGeneInfo, IEquatable<IGeneInfo> {
		private Gene gene;
		private RegulatorInfo regulator = null;
		private SiteInfo site = null;

		/// <summary> Initialises a GeneInfo object.
		/// </summary>
		/// <param name="gene"></param>

		public GeneInfo( Gene gene ) {
			this.gene = gene;
            if (gene["selected"] != null) {
			    this.IsSelected = bool.Parse(gene["selected"].ToString());
            }
		}

		public Gene Gene { get { return gene; } }
		public string GeneFunction { get { return gene.GeneFunction; } }
		public string LocusTag { get { return gene.LocusTag; } }
		public string Name { get { return gene.Name; } }
		public int RegulonId { get { return gene.RegulonId; } }
		public int VimssId { get { return gene.VimssId; } }
        //public bool Selected { get {return gene.Selected; } set {gene.Selected = value; }}

        /// <summary>
        /// Gets the annotation for this gene
        /// <para>
        /// This is an optional data field for a gene, which can be used in
        /// TRNDiff CSVfiles to include additional information about the gene
        /// </para>
        /// </summary>

		public string Annotation {
			get {
				return (string) gene["Annotation"];
			}
		}

		/// <summary> Get or set the Regulator that _may_ be associated with this object.
		/// <para>
		///		Setting this fires PropertyChanged for Regulator and IsTranscriptionFactor properties.
		/// </para>
		/// <para>
		///		So far, it seems that genes are sometimes also regulators, but it is not
		///		obvious that all regulators are genes. And so, I maintian parallel records
		///		and source data from the peer as required.
		/// </para>
		/// </summary>

		public RegulatorInfo Regulator {
			get { return regulator; }
			set {
				if ( Objects.AreEqual( regulator, value ) ) return;

				if ( Script.IsValue( value ) && value.LocusTag != gene.LocusTag ) {
					if (Constants.ShowDebugMessages) Console.Log( string.Format( Constants.GENE_AND_REGULATOR_ID_MUST_MATCH,
						gene.LocusTag,
						value.LocusTag
					) );
				}

				regulator = value;
				NotifyPropertyChanged( "IsTranscriptionFactor,Regulator" );
			}
		}

		/// <summary> Determines if the two GeneInfo objects have the same Locus tag.
		/// <para>
		///		Note that there are some genes in RegPrecise that do not have Vimssid numbers,
		///		so that is not an adequate key.
		/// </para>
		/// </summary>
		/// <param name="other">
		///		A IGeneInfo object to be compared.
		/// </param>
		/// <returns>
		///		Returns true iff the two geneInfo objects have the same VimssId.
		/// </returns>

		public bool Equals( IGeneInfo other ) {
			return ( (bool) (object) other ) && this.VimssId == other.VimssId;
		}

		/// <summary> Reveals if the current object is a transcription factor. Returns true iff Regulator != null.
		/// </summary>

		public bool IsTranscriptionFactor {
			get {
				return regulator != null;
			}
		}

        /// <summary>
        /// Gets or sets the site for this object
		/// <para>
		///		Setting this fires PropertyChanged for the Site property
		/// </para>
        /// </summary>
		public SiteInfo Site {
			get { return site; }
			set {

                // Don't do anything if the sites are the same
				if ( Objects.AreEqual( site, value ) ) return;

                // If the site's VimssID and this object's VimssID do not match,
                // then this is an invalid site for the object
				if ( Script.IsValue( value ) && value.GeneVimssid != VimssId ) {
					if (Constants.ShowDebugMessages) Console.Log( string.Format( Constants.GENE_AND_SITE_ID_MUST_MATCH,
						gene.VimssId,
						value.GeneVimssid
					) );
				}

                // Set the new site
				site = value;

                // Inform listeners that the site has changed
				NotifyPropertyChanged( "Site" );
			}
		}

		/// <summary> Wrapper for Gene.Parse.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public static GeneInfo Parse( JSObject obj ) {
			return new GeneInfo( new Gene( obj ) );
		}

        /// <summary>
        /// Gets or sets the parent GO term for this object
        /// <para>
        /// This is usually a high-level GO term rather than something more
        /// specific, and it is used for colouring nodes in displays with GO
        /// terms enabled
        /// </para>
        /// </summary>
        public string GoParentTerm { get {return gene.GoParentTerm; } set {gene.GoParentTerm = value; }}

        /// <summary>
        /// Gets or sets the GO term for this object
        /// </summary>
        public string GoTerm { get {return gene.GoTerm; } set {gene.GoTerm = value; }}

        /// <summary> Get or set the isSelected value. Setting this fires PropertyChanged.
		/// </summary>
		public override bool IsSelected {
			get {
				return base.IsSelected;
			}
			set {
                base.IsSelected = value;
				if ((bool)gene["selected"] != value) gene["selected"] = value;
			}
		}
	}
}
