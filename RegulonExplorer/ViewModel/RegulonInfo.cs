﻿using System;
using System.Collections.Generic;
using RegPrecise;
using SystemQut;
using Enum = SystemQut.Enum;
using SystemQut.ComponentModel;

namespace RegulonExplorer.ViewModel {
	public class RegulonInfo : Selectable {
		private Regulon regulon;
		private GeneInfo[] genes;
		private GenomeInfo genome;

		/// <summary> Initialises the RegulonInfo, saving the associated regulon and the
		///		identity of the collection to which it belongs.
		/// </summary>
		/// <param name="regulon">
		///		The Regulon.
		/// </param>

		public RegulonInfo ( Regulon regulon ) {
			this.regulon = regulon;
            if (regulon["selected"] != null) {
			    this.IsSelected = bool.Parse(regulon["selected"].ToString());
            }
		}

		public Number RegulonId { get { return regulon.RegulonId; } }
		public Number RegulogId { get { return regulon.RegulogId; } }
		public Number GenomeId { get { return regulon.GenomeId; } }
		public string GenomeName { get { return regulon.GenomeName; } }
		public string RegulatorName { get { return regulon.RegulatorName; } }
		public string RegulatorFamily { get { return regulon.RegulatorFamily; } }
		public string RegulationType { get { return regulon.RegulationType.ToString(); } }
		public string Effector { get { return regulon.Effector; } }
		public string Pathway { get { return regulon.Pathway; } }
        //public Number GroupNumber { get {return regulon.GroupNumber; } set {regulon.GroupNumber = value; }}
        //public double PositionLeft { get {return regulon.PositionLeft; } set {regulon.PositionLeft = value; }}
        //public double PositionTop { get {return regulon.PositionTop; } set {regulon.PositionTop = value; }}
        //public Number GroupNumber { get {return Number.Parse((string)regulon["groupNumber"]); } set {regulon["groupNumber"] = value; }}
        //public double PositionLeft { get {return Double.Parse((string)regulon["positionLeft"]); } set {regulon["positionLeft"] = value; }}
        //public double PositionTop { get {return Double.Parse((string)regulon["positionTop"]); } set {regulon["positionTop"] = value; }}
        //public Number Order { get {return Number.Parse((string)regulon["order"]); } set {regulon["order"] = value; }}
        public Number GroupNumber { get { return (Number)regulon["groupNumber"]; } set {regulon["groupNumber"] = value; }}
        public Number Order { get { return(Number)regulon["order"]; } set {regulon["order"] = value; }}

		public Regulon Regulon { get { return regulon; } }

		/// <summary> Get or set the genes associated with this regulon.
		/// <para>
		///		Setting fires PropertyChanged.
		/// </para>
		/// </summary>

		public GeneInfo[] Genes {
			get {
				return genes;
			}
			set {
				if ( genes == value ) return;
				genes = value;
				NotifyPropertyChanged( "Genes" );
			}
		}

		private RegulatorInfo[] regulators;

		/// <summary> Get or set the regulators associated with this regulon.
		/// <para>
		///		Setting fires PropertyChanged.
		/// </para>
		/// </summary>

		public RegulatorInfo[] Regulators {
			get { return regulators; }
			set {
				if ( regulators == value ) return;
				regulators = value;
				NotifyPropertyChanged( "Regulators" );
			}
		}

		/// <summary> Gets the list of genes that are not transcription factors in the current regulon.
		/// </summary>

		public IEnumerable<GeneInfo> TargetGenes {
			get { return genes.Filter( delegate( GeneInfo gene ) { return !gene.IsTranscriptionFactor; } ); }
		}

		/// <summary> Wrapper for Regulon.Parse.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public static RegulonInfo Parse ( JSObject obj ) {
			return new RegulonInfo( new Regulon( obj ) );
		}

		/// <summary> Get or set the (non-null) GenomeInfo associated with this regulon.
		/// </summary>

		public GenomeInfo Genome {
			get {
				return genome;
			}
			set {
				if ( value == null ) throw new Exception( "Argument null." );

				genome = value;

				NotifyPropertyChanged("Genome");
			}
		}



        /// <summary> Converts this to a string representation - currently the genome and regulator name
        /// </summary>
        ///
        public override string ToString() {
            return new string(regulon.GenomeName + " (" + regulon.RegulatorName + ")");
        }

        //To determine whether spokes should be scaled for this regulon
        /*private bool doScale;

        public bool DoScale
        {
            get { return doScale; }
            set
            {
                doScale = value;
                base.NotifyPropertyChanged("DoScale");
            }
        }*/

        /// <summary> Get or set the isSelected value. Setting this fires PropertyChanged.
		/// </summary>
		public override bool IsSelected {
			get {
				return base.IsSelected;
			}
			set {
                base.IsSelected = value;
				if ((bool)regulon["selected"] != value) regulon["selected"] = value;
			}
		}
	}
}
