// RegPreciseModel.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using RegPrecise;
using SystemQut.ServiceModel;
using SystemQut.ComponentModel;
using SystemQut;
using SystemQut.IO;
using SystemQUT.Csv;
using System.Html.Data.Files;
using RegulonExplorer.Common;
using SystemQut.Linq;
#if DEBUG
using System.Net;
#endif

namespace RegulonExplorer.ViewModel {

	/// <summary> Singleton class which encapsulates interaction with the web service that
	///		provides RegPrecise data.
	/// </summary>

	public class DataLayer : INotifyPropertyChanged {

		// TODO: these collections need to be cleaned up. I'm sure there is needless duplication.

		private Dictionary<RegulogCollectionType, List<RegulogCollectionInfo>> allItems;
		// private static Dictionary<Number, RegulogInfo> regulators;
		private Dictionary<Number, RegulonInfo> regulonsById  = new Dictionary<Number, RegulonInfo>();
		private Dictionary<Number, RegulonInfo[]> regulonsByRegulogId = new Dictionary<Number, RegulonInfo[]>();

		private Dictionary<Number, RegulatorInfo> regulatorsById;
		private Dictionary<Number, RegulatorInfo[]> regulatorsByRegulogId;

		/// <summary> Maintains a lookup table with a list of regulators for each combination
		///		of RegulogCollection Id.
		/// <para>
		///		This may be more complex than is needed, however the RegPrecise API requires both
		///		regulon type and Id, which implies that there may be a possibility of duplicated
		///		regulon Id's.
		/// </para>
		/// </summary>
		private Dictionary<RegulogCollectionType, Dictionary<int, RegulogInfo[]>> regulogsByCollectionId;

		private Dictionary<int, RegulogInfo> regulogsByRegulogId;
		private Dictionary<Number, GeneInfo> genesById;
		private Dictionary<Number, GeneInfo[]> genesByRegulonId;
		private Dictionary<Number, GeneInfo[]> genesByRegulogId;
		private Dictionary<Number, RegulatorInfo[]> regulatorsByRegulonId;

		/// <summary> Lookup table mapping genome key to GenomeInfo.
		/// <para>
		///		This will contain both regPrecise and locally loaded genomes.
		/// </para>
		/// </summary>
		private GenomeCollection genomes = new GenomeCollection();

		private static DataLayer instance;

        private GORegPreciseMatch[] goTermMatches;

        private bool localMySQLUp = true;

		private DataLayer () { }

		/// <summary> Get a reference to the instance.
		/// </summary>

		public static DataLayer Instance {
			get {
				if ( instance == null ) instance = new DataLayer();

				return instance;
			}
		}

		/// <summary> Requests a list of genome summaries from the server.
		/// <para>
		///		When the results arrive, they are stored in the Genomes property,
		///		which then fires the PropertyChanged event.
		/// </para>
		/// </summary>

		public void RequestGenomes () {
            if (Constants.UseLocalMySQLCache) {
                // Try loading from the local MySQL database first
                ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                localMySQL.ListGenomeStats( delegate(ServiceResponse<GenomeStats[]> databaseResponse ) {

                    // If there's an error loading from the database or nothing
                    // was retrieved, then proceed to RegPrecise
                    if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                        // If there was an error, display a message
                        if ( databaseResponse.Error != null ) {
						    string e = databaseResponse.Error;
						    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenomes: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                            // If the database connection was refused, set the
                            // local database as down for now
                            if (e.IndexOf("ECONNREFUSED") != -1) {
                                localMySQLUp = false;
                            }

                            // Otherwise, assume the connection is up
                            else {
                                localMySQLUp = true;
                            }
                        }

                        // If nothing was retrieved, display a message
                        else if ( databaseResponse.Content == null ) {
                            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenomes: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                        }

                        // Regular RegPrecise service
                        IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				        svc.ListGenomeStats( delegate( ServiceResponse<GenomeStats[]> response ) {
					        if ( response.Error != null ) {
						        string e = response.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenomes: Server returned error '{0}'", e ) );
						        return;
					        }

					        if ( response.Content == null ) {
						        Window.Alert( Constants.REGPRECISE_NO_GENOMES );
						        return;
					        };

					        foreach ( GenomeStats genomeStats in response.Content ) {
						        genomes.Add( new GenomeInfo( genomeStats ) );
					        }

					        NotifyPropertyChanged( "Genomes" );

                            // Inform listeners that RegPrecise was contacted
                            OnRegPreciseContact(new EventArgs());

                            if (response.Content != null) {
                            // Attempt to add the retrieved data to the local
                            // database if it is up
                            string currentAdd = "AddGenomeStats_ListGenomeStats";

                            if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                            } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                            } else if (localMySQLUp) {

                                // Note the current add operation to avoid
                                // multiples of the same
                                addsCurrentlyProcessing.Add(currentAdd);

                                // Call the service to add the data
                                localMySQL.AddGenomeStats_ListGenomeStats( response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                    if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} genome stat(s) to the local MySQL database", response.Content.Length));
					                if ( databaseResponseAdd.Error != null ) {
						                string e = databaseResponseAdd.Error;
						                if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add genome stats to the local MySQL database: Server returned error '{0}'", e ) );
						                return;
					                }

					                if ( databaseResponseAdd.Content != null ) {
                                        if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} genome stats added to the local MySQL database", databaseResponseAdd.Content));

                                        // Note that the current add operation
                                        // was successful
                                        addsProcessed.Add(currentAdd);
                                    }

                                    // Remove the current add operation from
                                    // the list so that it can be attempted
                                    // again if required
                                    addsCurrentlyProcessing.Remove(currentAdd);
                                });
                            }
                            }
				        } );
                    } else {

                        // Otherwise load in the retrieved content
                        foreach ( GenomeStats genomeStats in databaseResponse.Content ) {
						    genomes.Add( new GenomeInfo( genomeStats ) );
					    }

					    NotifyPropertyChanged( "Genomes" );

                        // If the connection was successful, set the local
                        // datbase as up
                        localMySQLUp = true;
                    }
                });
            } else {
                // Use the regular RegPrecise service
                IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				svc.ListGenomeStats( delegate( ServiceResponse<GenomeStats[]> response ) {
					if ( response.Error != null ) {
						string e = response.Error;
						if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenomes: Server returned error '{0}'", e ) );
						return;
					}

					if ( response.Content == null ) {
						Window.Alert( Constants.REGPRECISE_NO_GENOMES );
						return;
					};

					foreach ( GenomeStats genomeStats in response.Content ) {
						genomes.Add( new GenomeInfo( genomeStats ) );
					}

					NotifyPropertyChanged( "Genomes" );

                    // Inform listeners that RegPrecise was contacted
                    OnRegPreciseContact(new EventArgs());
				} );
            }
		}

		/// <summary> Get or set the list of available Genomes.
		/// <para>
		///		Setting the property fires PropertyChanged.
		/// </para>
		/// </summary>

		public GenomeCollection Genomes {
			get {
				return genomes;
			}
		}

		/// <summary> Requests a list of regulon regulon summaries and invokes the supplied callback
		///		function as soon as the data becomes available. If the data is already
		///		available, the callback is invoked immediately. Otherwise, it is called
		///		when the data has been received from the server.
		///	<para>
		///		The results are retained to avoid the need to recompute the results.
		/// </para>
		/// </summary>
		/// <param name="collectionType">
		///		The regulon type.
		/// </param>
		/// <param name="callback">
		///		A delegate that will be invoked to process the resulting regulon regulon summaries.
		/// </param>

		public void RequestRegulogCollections ( RegulogCollectionType collectionType, Action<RegulogCollectionInfo[]> callback ) {
			if ( allItems == null ) {
				allItems = new Dictionary<RegulogCollectionType, List<RegulogCollectionInfo>>();
			}

			if ( !Script.IsValue( allItems[collectionType] ) || forceReload ) {
                if (Constants.UseLocalMySQLCache) {
                    // Try loading from the local MySQL database first
                    ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                    localMySQL.ListRegulogCollections( collectionType, delegate(ServiceResponse<RegulogCollection[]> databaseResponse ) {

                        // If there's an error loading from the database or nothing
                        // was retrieved, then proceed to RegPrecise
                        if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                            // If there was an error, display a message
                            if ( databaseResponse.Error != null ) {
						        string e = databaseResponse.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulogCollections: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                // If the database connection was refused, set the
                                // local database as down for now
                                if (e.IndexOf("ECONNREFUSED") != -1) {
                                    localMySQLUp = false;
                                }

                                // Otherwise, assume the connection is up
                                else {
                                    localMySQLUp = true;
                                }
                            }

                            // If nothing was retrieved, display a message
                            else if ( databaseResponse.Content == null ) {
                                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulogCollections: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                            }

                            // Regular RegPrecise service
				            IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				            svc.ListRegulogCollections( collectionType, delegate( ServiceResponse<RegulogCollection[]> response ) {
					            if ( response.Error != null ) {
						            string e = response.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulogCollections: Server returned error '{0}'", e ) );
						            return;
					            }

					            List<RegulogCollectionInfo> items = new List<RegulogCollectionInfo>();
					            allItems[collectionType] = items;

					            RegulogCollection [] collections = response.Content == null ? new RegulogCollection[0] : response.Content;

					            foreach ( RegulogCollection g in collections ) {
						            items.Add( new RegulogCollectionInfo( g ) );
					            }

    /*#if DEBUG
                                // test for getting reuglogs as well
                                if (Constants.ShowDebugMessages) Console.Log("Attempting to add regulogs to collections");
                                foreach ( RegulogCollectionInfo c in items) {
                                    if (Constants.ShowDebugMessages) Console.Log(c.Name);
                                    this.RequestRegulogsInCollection(collectionType, c.CollectionId, (Action<RegulogInfo[]>)delegate( RegulogInfo[] newRegulogs ) {
                                        c.Regulogs = newRegulogs;
                                    });
                                    if (c.Regulogs != null) {
                                        if (Constants.ShowDebugMessages) Console.Log("Regulog added");
                                    } else {
                                        if (Constants.ShowDebugMessages) Console.Log("Regulog not added");
                                    }
                                }
    #endif*/

					            callback( (RegulogCollectionInfo[]) items );

                                // Inform listeners that RegPrecise was contacted
                                OnRegPreciseContact(new EventArgs());

                                if (response.Content != null) {
                                // Attempt to add the retrieved data to the local
                                // database if it is up
                                string currentAdd = "AddRegulogCollections_ListRegulogCollections_" + collectionType.ToString();

                                if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                } else if (localMySQLUp) {

                                    // Note the current add operation to avoid
                                    // multiples of the same
                                    addsCurrentlyProcessing.Add(currentAdd);

                                    // Call the service to add the data
                                    localMySQL.AddRegulogCollections_ListRegulogCollections( collectionType, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} regulog collections(s) to the local MySQL database", response.Content.Length));
					                    if ( databaseResponseAdd.Error != null ) {
						                    string e = databaseResponseAdd.Error;
						                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulog collections to the local MySQL database: Server returned error '{0}'", e ) );
						                    return;
					                    }

					                    if ( databaseResponseAdd.Content != null ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulog collections added to the local MySQL database", databaseResponseAdd.Content));

                                            // Note that the current add operation
                                            // was successful
                                            addsProcessed.Add(currentAdd);
                                        }

                                        // Remove the current add operation from
                                        // the list so that it can be attempted
                                        // again if required
                                        addsCurrentlyProcessing.Remove(currentAdd);
                                    });
                                }
                                }
				            } );
                        } else {

                            // Otherwise load in the retrieved content
                            List<RegulogCollectionInfo> items = new List<RegulogCollectionInfo>();
					        allItems[collectionType] = items;

					        RegulogCollection [] collections = databaseResponse.Content;

					        foreach ( RegulogCollection g in collections ) {
						        items.Add( new RegulogCollectionInfo( g ) );
					        }

					        callback( (RegulogCollectionInfo[]) items );

                            // If the connection was successful, set the local
                            // datbase as up
                            localMySQLUp = true;
                        }
                    });
                } else {
                    // Use the regular RegPrecise service
                    IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				    svc.ListRegulogCollections( collectionType, delegate( ServiceResponse<RegulogCollection[]> response ) {
					    if ( response.Error != null ) {
						    string e = response.Error;
						    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulogCollections: Server returned error '{0}'", e ) );
						    return;
					    }

					    List<RegulogCollectionInfo> items = new List<RegulogCollectionInfo>();
					    allItems[collectionType] = items;

					    RegulogCollection [] collections = response.Content == null ? new RegulogCollection[0] : response.Content;

					    foreach ( RegulogCollection g in collections ) {
						    items.Add( new RegulogCollectionInfo( g ) );
					    }

    /*#if DEBUG
                        // test for getting reuglogs as well
                        if (Constants.ShowDebugMessages) Console.Log("Attempting to add regulogs to collections");
                        foreach ( RegulogCollectionInfo c in items) {
                            if (Constants.ShowDebugMessages) Console.Log(c.Name);
                            this.RequestRegulogsInCollection(collectionType, c.CollectionId, (Action<RegulogInfo[]>)delegate( RegulogInfo[] newRegulogs ) {
                                c.Regulogs = newRegulogs;
                            });
                            if (c.Regulogs != null) {
                                if (Constants.ShowDebugMessages) Console.Log("Regulog added");
                            } else {
                                if (Constants.ShowDebugMessages) Console.Log("Regulog not added");
                            }
                        }
    #endif*/

					    callback( (RegulogCollectionInfo[]) items );

                        // Inform listeners that RegPrecise was contacted
                        OnRegPreciseContact(new EventArgs());
				    } );
                }
			}
			else {
				callback( (RegulogCollectionInfo[]) allItems[collectionType] );
			}
		}

		/// <summary> Requests a list of regulon regulon summaries and invokes the supplied callback
		///		function as soon as the data becomes available. If the data is already
		///		available, the callback is invoked immediately. Otherwise, it is called
		///		when the data has been received from the server.
		///	<para>
		///		The results are retained to avoid the need to recompute the results.
		/// </para>
		/// </summary>
		/// <param name="collectionType">
		///		The collectiopn type.
		/// </param>
		/// <param name="callback">
		///		A delegate that will be invoked to process the resulting regulon regulon summaries.
		/// </param>
		/// <param name="collectionId">
		///		The regulon regulog Id.
		/// </param>

		public void RequestRegulogsInCollection ( RegulogCollectionType collectionType, int collectionId, Action<RegulogInfo[]> callback ) {
			if ( regulogsByCollectionId == null ) {
				regulogsByCollectionId = new Dictionary<RegulogCollectionType, Dictionary<int, RegulogInfo[]>>();
				regulogsByRegulogId = new Dictionary<int, RegulogInfo>();
			}

			Dictionary<int, RegulogInfo[]> collectionsForId = regulogsByCollectionId[collectionType];

			if ( !Script.IsValue( collectionsForId ) ) {
				collectionsForId = new Dictionary<int, RegulogInfo[]>();
				regulogsByCollectionId[collectionType] = collectionsForId;
			}

			RegulogInfo[] regulogs = collectionsForId[collectionId];

			if ( !Script.IsValue( regulogs ) || forceReload ) {
                if (Constants.UseLocalMySQLCache) {
                    // Try loading from the local MySQL database first
                    ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                    localMySQL.ListRegulogs( collectionType, collectionId, delegate(ServiceResponse<Regulog[]> databaseResponse ) {

                        // If there's an error loading from the database or nothing
                        // was retrieved, then proceed to RegPrecise
                        if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                            // If there was an error, display a message
                            if ( databaseResponse.Error != null ) {
						        string e = databaseResponse.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulogsInCollection: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                // If the database connection was refused, set the
                                // local database as down for now
                                if (e.IndexOf("ECONNREFUSED") != -1) {
                                    localMySQLUp = false;
                                }

                                // Otherwise, assume the connection is up
                                else {
                                    localMySQLUp = true;
                                }
                            }

                            // If nothing was retrieved, display a message
                            else if ( databaseResponse.Content == null ) {
                                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulogsInCollection: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                            }

                            // Regular RegPrecise service
				            IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				            svc.ListRegulogs( collectionType, collectionId, delegate( ServiceResponse<Regulog[]> response ) {
					            if ( response.Error != null ) {
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulogsInCollection: Server returned error '{0}'", response.Error ) );
						            return;
					            }

					            regulogs = response.Content == null
						            ? new RegulogInfo[0]
						            : response.Content.Map( delegate( Regulog regulog ) { return new RegulogInfo( regulog ); } );

					            collectionsForId[collectionId] = regulogs;

					            foreach ( RegulogInfo r in regulogs ) {
						            regulogsByRegulogId[r.RegulogId] = r;
					            }

					            callback( regulogs );

                                // Inform listeners that RegPrecise was contacted
                                OnRegPreciseContact(new EventArgs());

                                if (response.Content != null) {
                                // Attempt to add the retrieved data to the local
                                // database if it is up
                                string currentAdd = "AddRegulogs_ListRegulogs_" + collectionType.ToString() + "_" + collectionId;

                                if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                } else if (localMySQLUp) {

                                    // Note the current add operation to avoid
                                    // multiples of the same
                                    addsCurrentlyProcessing.Add(currentAdd);

                                    // Call the service to add the data
                                    localMySQL.AddRegulogs_ListRegulogs( collectionType, collectionId, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} regulog(s) to the local MySQL database", response.Content.Length));
					                    if ( databaseResponseAdd.Error != null ) {
						                    string e = databaseResponseAdd.Error;
						                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulogs to the local MySQL database: Server returned error '{0}'", e ) );
						                    return;
					                    }

					                    if ( databaseResponseAdd.Content != null ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulogs added to the local MySQL database", databaseResponseAdd.Content));

                                            // Note that the current add operation
                                            // was successful
                                            addsProcessed.Add(currentAdd);
                                        }

                                        // Remove the current add operation from
                                        // the list so that it can be attempted
                                        // again if required
                                        addsCurrentlyProcessing.Remove(currentAdd);
                                    });
                                }
                                }
				            } );
                        } else {

                            // Otherwise load in the retrieved content
                            regulogs = databaseResponse.Content.Map( delegate( Regulog regulog ) { return new RegulogInfo( regulog ); } );

                            collectionsForId[collectionId] = regulogs;

					        foreach ( RegulogInfo r in regulogs ) {
						        regulogsByRegulogId[r.RegulogId] = r;
					        }

					        callback( regulogs );

                            // If the connection was successful, set the local
                            // datbase as up
                            localMySQLUp = true;
                        }
                    });
			    }
			    else {
                    // Use the regular RegPrecise service
                    IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				    svc.ListRegulogs( collectionType, collectionId, delegate( ServiceResponse<Regulog[]> response ) {
					    if ( response.Error != null ) {
						    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulogsInCollection: Server returned error '{0}'", response.Error ) );
						    return;
					    }

					    regulogs = response.Content == null
						    ? new RegulogInfo[0]
						    : response.Content.Map( delegate( Regulog regulog ) { return new RegulogInfo( regulog ); } );

					    collectionsForId[collectionId] = regulogs;

					    foreach ( RegulogInfo r in regulogs ) {
						    regulogsByRegulogId[r.RegulogId] = r;
					    }

					    callback( regulogs );

                        // Inform listeners that RegPrecise was contacted
                        OnRegPreciseContact(new EventArgs());
				    } );
			    }
            } else {
				callback( regulogs );
            }
		}

		/// <summary> Obtains the regulon summaries for a regulog. As soon as the results are available they
		///		are stored in <c>regulog.Regulons</c>.
		///	<para>
		///		The results are cached to avoid the need to re-fetch the results.
		/// </para>
		/// </summary>
		/// <param name="regulog">
		///		The RegPrecise regulog for which regulons are required and to which the results will be passed.
		/// </param>
        /// <param name="local">Whether the dataset is local</param>

		public void RequestRegulonsInRegulog ( RegulogInfo regulog, bool local ) {
			RegulonInfo [] regulons = regulonsByRegulogId[regulog.RegulogId];

			if ( Script.IsValue( regulons ) && !forceReload ) {
				regulog.Regulons = regulons;
			}
			else {
                if ( !local ) {
                    if (Constants.UseLocalMySQLCache) {
                        // Try loading from the local MySQL database first
                        ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                        localMySQL.ListRegulonsInRegulog( regulog.RegulogId, delegate(ServiceResponse<Regulon[]> databaseResponse ) {

                            // If there's an error loading from the database or nothing
                            // was retrieved, then proceed to RegPrecise
                            if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                                // If there was an error, display a message
                                if ( databaseResponse.Error != null ) {
						            string e = databaseResponse.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulonsInRegulog: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                    // If the database connection was refused, set the
                                    // local database as down for now
                                    if (e.IndexOf("ECONNREFUSED") != -1) {
                                        localMySQLUp = false;
                                    }

                                    // Otherwise, assume the connection is up
                                    else {
                                        localMySQLUp = true;
                                    }
                                }

                                // If nothing was retrieved, display a message
                                else if ( databaseResponse.Content == null ) {
                                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulonsInRegulog: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                                }

                                // Regular RegPrecise service
				                IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				                svc.ListRegulonsInRegulog( regulog.RegulogId, delegate( ServiceResponse<Regulon[]> response ) {
					                if ( response.Error != null ) {
						                string e = response.Error;
						                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulonsInRegulog: Server returned error '{0}'", e ) );
						                return;
					                }

					                regulons = response.Content == null ? new RegulonInfo[0] :
						                response.Content.Map<Regulon, RegulonInfo>( ProcessRegPreciseRegulon );

					                regulonsByRegulogId[regulog.RegulogId] = regulons;
					                regulog.Regulons = regulons;

                                    // Inform listeners that RegPrecise was contacted
                                    OnRegPreciseContact(new EventArgs());

                                    if (response.Content != null) {
                                    // Attempt to add the retrieved data to the local
                                    // database if it is up
                                    string currentAdd = "AddRegulons_ListRegulonsInRegulog_" + regulog.RegulogId;

                                    if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                    } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                    } else if (localMySQLUp) {

                                        // Note the current add operation to avoid
                                        // multiples of the same
                                        addsCurrentlyProcessing.Add(currentAdd);

                                        // Call the service to add the data
                                        localMySQL.AddRegulons_ListRegulonsInRegulog( regulog.RegulogId, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} regulon(s) to the local MySQL database", response.Content.Length));
					                        if ( databaseResponseAdd.Error != null ) {
						                        string e = databaseResponseAdd.Error;
						                        if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulons to the local MySQL database: Server returned error '{0}'", e ) );
						                        return;
					                        }

					                        if ( databaseResponseAdd.Content != null ) {
                                                if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulons added to the local MySQL database", databaseResponseAdd.Content));

                                                // Note that the current add operation
                                                // was successful
                                                addsProcessed.Add(currentAdd);
                                            }

                                            // Remove the current add operation from
                                            // the list so that it can be attempted
                                            // again if required
                                            addsCurrentlyProcessing.Remove(currentAdd);
                                        });
                                    }
                                    }
				                } );
                            } else {

                                // Otherwise load in the retrieved content
                                regulons = databaseResponse.Content.Map<Regulon, RegulonInfo>( ProcessRegPreciseRegulon );

					            regulonsByRegulogId[regulog.RegulogId] = regulons;
					            regulog.Regulons = regulons;

                                // If the connection was successful, set the local
                                // datbase as up
                                localMySQLUp = true;
                            }
                        });
                    } else {
                        // Use the regular RegPrecise service
                        IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				        svc.ListRegulonsInRegulog( regulog.RegulogId, delegate( ServiceResponse<Regulon[]> response ) {
					        if ( response.Error != null ) {
						        string e = response.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulonsInRegulog: Server returned error '{0}'", e ) );
						        return;
					        }

					        regulons = response.Content == null ? new RegulonInfo[0] :
						        response.Content.Map<Regulon, RegulonInfo>( ProcessRegPreciseRegulon );

					        regulonsByRegulogId[regulog.RegulogId] = regulons;
					        regulog.Regulons = regulons;

                            // Inform listeners that RegPrecise was contacted
                            OnRegPreciseContact(new EventArgs());
				        } );
                    }
                }
			}
		}

		/// <summary> Processes a RegPrecise regulon to ensure that it is (a) unique and
		///		(b) bound to its associated Genome correctly.
		/// </summary>
		/// <param name="regulon"></param>
		/// <returns></returns>

		RegulonInfo ProcessRegPreciseRegulon ( Regulon regulon ) {
			GenomeInfo g = Genomes.Get( DataLayer.RegPreciseId, regulon.GenomeId );

			RegulonInfo r = g == null ? null : g.GetRegulon( regulon.RegulonId );

			if ( g == null ) {
				GenomeStats gStats = new GenomeStats(
					regulon.GenomeId,
					regulon.GenomeName,
					0, 0, 0, 0, 0
				);
				g = new GenomeInfo( gStats );
			}

			if ( r == null ) {
				r = new RegulonInfo( regulon );
				g.AddRegulon( r );
			}

			regulonsById[r.RegulonId] = r;

			return r;
		}

		/// <summary> Requests a list of regulator records and invokes the supplied callback
		///		function as soon as the data becomes available. If the data is already
		///		available, the callback is invoked immediately. Otherwise, it is called
		///		when the data has been received from the server.
		///	<para>
		///		The results are retained to avoid the need to recompute the results.
		/// </para>
		/// </summary>
		/// <param name="callback">
		///		A delegate that will be invoked to process the resulting regulator records.
		///		The argument supplied to the callback will contain the resulting regulators. It should
		///		be tested against the current requested regulon to make the correct data is displayed
		///		in the event that async calls overlap.
		/// </param>
		/// <param name="regulon">
		///		The RegPrecise regulon for which regulators are required.
		/// </param>

		public void RequestGenesInRegulon ( RegulonInfo regulon, Action<RegulonInfo> callback ) {
			if ( !Script.IsValue( genesById ) ) {
				genesById = new Dictionary<Number, GeneInfo>();
			}

			if ( !Script.IsValue( genesByRegulonId ) ) {
				genesByRegulonId = new Dictionary<Number, GeneInfo[]>();
			}

			Number regulonId = regulon.RegulonId;

			GeneInfo[] genes = genesByRegulonId[regulonId];

			if ( !Script.IsValue( genes ) || forceReload ) {
                if (Constants.UseLocalMySQLCache) {
                    // Try loading from the local MySQL database first
                    ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                    localMySQL.ListGenesInRegulon( regulonId, delegate(ServiceResponse<Gene[]> databaseResponse ) {

                        // If there's an error loading from the database or nothing
                        // was retrieved, then proceed to RegPrecise
                        if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                            // If there was an error, display a message
                            if ( databaseResponse.Error != null ) {
						        string e = databaseResponse.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenesInRegulon: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                // If the database connection was refused, set the
                                // local database as down for now
                                if (e.IndexOf("ECONNREFUSED") != -1) {
                                    localMySQLUp = false;
                                }

                                // Otherwise, assume the connection is up
                                else {
                                    localMySQLUp = true;
                                }
                            }

                            // If nothing was retrieved, display a message
                            else if ( databaseResponse.Content == null ) {
                                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenesInRegulon: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                            }

                            // Regular RegPrecise service
				            IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				            svc.ListGenesInRegulon( regulonId, delegate( ServiceResponse<Gene[]> response ) {
					            if ( response.Error != null ) {
						            string e = response.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenesInRegulon: Server returned error '{0}'", e ) );
						            return;
					            }

					            if ( response.Content == null ) return;

					            genes = response.Content == null
						            ? new GeneInfo[0]
						            : response.Content.Map( delegate( Gene gene ) { return new GeneInfo( gene ); } );

                                // Added so we can match up GO terms with the genes we
                                // retrieved
                                if (Constants.UseGoTerms) {
                                    RetrieveLocalGOTerms_Regulon(genes, localMySQL, regulonId, regulon, callback);
                                // If GO terms are not enabled, go straight to
                                // setting the genes
                                } else {
					                genesByRegulonId[regulonId] = genes;
					                regulon.Genes = genes;

					                foreach ( GeneInfo r in genes ) {
						                genesById[r.VimssId] = r;
					                }

					                callback( regulon );
                                }

                                // Inform listeners that RegPrecise was contacted
                                OnRegPreciseContact(new EventArgs());

                                if (response.Content != null) {
                                // Attempt to add the retrieved data to the local
                                // database if it is up
                                string currentAdd = "AddGenes_ListGenesInRegulon_" + regulonId;

                                if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                } else if (localMySQLUp) {

                                    // Note the current add operation to avoid
                                    // multiples of the same
                                    addsCurrentlyProcessing.Add(currentAdd);

                                    // Call the service to add the data
                                    localMySQL.AddGenes_ListGenesInRegulon( regulonId, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} regulators(s) to the local MySQL database", response.Content.Length));
					                    if ( databaseResponseAdd.Error != null ) {
						                    string e = databaseResponseAdd.Error;
						                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulators to the local MySQL database: Server returned error '{0}'", e ) );
						                    return;
					                    }

					                    if ( databaseResponseAdd.Content != null ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulators added to the local MySQL database", databaseResponseAdd.Content));

                                            // Note that the current add operation
                                            // was successful
                                            addsProcessed.Add(currentAdd);
                                        }

                                        // Remove the current add operation from
                                        // the list so that it can be attempted
                                        // again if required
                                        addsCurrentlyProcessing.Remove(currentAdd);
                                    });
                                }
                                }
				            } );
                        } else {

                            // Otherwise load in the retrieved content
                            genes = databaseResponse.Content.Map( delegate( Gene gene ) { return new GeneInfo( gene ); } );

                            // If the connection was successful, set the local
                            // datbase as up
                            localMySQLUp = true;

                            // Added so we can match up GO terms with the genes we
                            // retrieved
                            if (Constants.UseGoTerms) {
                                RetrieveLocalGOTerms_Regulon(genes, localMySQL, regulonId, regulon, callback);
                            } else {
					            genesByRegulonId[regulonId] = genes;
					            regulon.Genes = genes;

					            foreach ( GeneInfo r in genes ) {
						            genesById[r.VimssId] = r;
					            }

					            callback( regulon );
                            }
                        }
                    });
			    }
			    else {
				    callback( regulon );
			    }
            } else {
                // Use the regular RegPrecise service
                IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				svc.ListGenesInRegulon( regulonId, delegate( ServiceResponse<Gene[]> response ) {
					if ( response.Error != null ) {
						string e = response.Error;
						if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenesInRegulon: Server returned error '{0}'", e ) );
						return;
					}

					if ( response.Content == null ) return;

					genes = response.Content == null
						? new GeneInfo[0]
						: response.Content.Map( delegate( Gene gene ) { return new GeneInfo( gene ); } );

                    // Added so we can match up GO terms with the genes we
                    // retrieved
                    if (Constants.UseGoTerms) {
                        LocalMySQLService localMySQL = new LocalMySQLService(Constants.LOCAL_MYSQL_SERVICE_URL, false);
                        RetrieveLocalGOTerms_Regulon(genes, localMySQL, regulonId, regulon, callback);
                    // If GO terms are not enabled, go straight to
                    // setting the genes
                    } else {
					    genesByRegulonId[regulonId] = genes;
					    regulon.Genes = genes;

					    foreach ( GeneInfo r in genes ) {
						    genesById[r.VimssId] = r;
					    }

					    callback( regulon );
                    }

                    // Inform listeners that RegPrecise was contacted
                    OnRegPreciseContact(new EventArgs());
				} );
            }
		}

		/// <summary> Requests the genes in a regulog. When the results are available they are
		///		assigned to the <c>regulog.Genes</c> property.
		/// </summary>
		/// <param name="regulog">
		///		A regulog for which genes are wanted.
		/// </param>
        /// <param name="local">Whether the dataset is local</param>

		public void RequestGenesInRegulog ( RegulogInfo regulog, bool local ) {
			if ( !Script.IsValue( genesById ) ) {
				genesById = new Dictionary<Number, GeneInfo>();
			}

			if ( !Script.IsValue( genesByRegulogId ) ) {
				genesByRegulogId = new Dictionary<Number, GeneInfo[]>();
			}

			Number regulogId = regulog.RegulogId;

			GeneInfo[] genes = genesByRegulogId[regulogId];

			if ( Script.IsValue( genes ) && !forceReload ) {
				regulog.Genes = genes;
			}
			else {
                if ( !local ) {
                    if (Constants.UseLocalMySQLCache) {
                        // Try loading from the local MySQL database first
                        ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                        localMySQL.ListGenesInRegulog( regulogId, delegate(ServiceResponse<Gene[]> databaseResponse ) {

                            // If there's an error loading from the database or nothing
                            // was retrieved, then proceed to RegPrecise
                            if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                                // If there was an error, display a message
                                if ( databaseResponse.Error != null ) {
						            string e = databaseResponse.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenesInRegulog: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                    // If the database connection was refused, set the
                                    // local database as down for now
                                    if (e.IndexOf("ECONNREFUSED") != -1) {
                                        localMySQLUp = false;
                                    }

                                    // Otherwise, assume the connection is up
                                    else {
                                        localMySQLUp = true;
                                    }
                                }

                                // If nothing was retrieved, display a message
                                else if ( databaseResponse.Content == null ) {
                                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenesInRegulog: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                                }

                                // Regular RegPrecise service
				                IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				                svc.ListGenesInRegulog( regulogId, delegate( ServiceResponse<Gene[]> response ) {
					                if ( response.Error != null ) {
						                string e = response.Error;
						                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenesInRegulog: Server returned error '{0}'", e ) );
						                return;
					                }

					                genes = response.Content == null
						                ? new GeneInfo[0]
						                : response.Content.Map( delegate( Gene gene ) { return new GeneInfo( gene ); } );

                                    // Added so we can match up GO terms with the genes we
                                    // retrieved
                                    if (Constants.UseGoTerms) {
                                        RetrieveLocalGOTerms_Regulog(genes, localMySQL, regulogId, regulog);

                                    // If GO terms are not enabled, go straight to
                                    // setting the genes
                                    } else {
					                    genesByRegulogId[regulogId] = genes;

					                    foreach ( GeneInfo r in genes ) {
						                    genesById[r.VimssId] = r;
					                    }

					                    regulog.Genes = genes;
                                    }

                                    // Inform listeners that RegPrecise was contacted
                                    OnRegPreciseContact(new EventArgs());

                                    if (response.Content != null) {
                                    // Attempt to add the retrieved data to the local
                                    // database if it is up
                                    string currentAdd = "AddGenes_ListGenesInRegulog_" + regulogId;

                                    if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                    } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                    } else if (localMySQLUp) {

                                        // Note the current add operation to avoid
                                        // multiples of the same
                                        addsCurrentlyProcessing.Add(currentAdd);

                                        // Call the service to add the data
                                        localMySQL.AddGenes_ListGenesInRegulog( regulogId, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} genes(s) to the local MySQL database", response.Content.Length));
					                        if ( databaseResponseAdd.Error != null ) {
						                        string e = databaseResponseAdd.Error;
						                        if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add genes to the local MySQL database: Server returned error '{0}'", e ) );
						                        return;
					                        }

					                        if ( databaseResponseAdd.Content != null ) {
                                                if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} genes added to the local MySQL database", databaseResponseAdd.Content));

                                                // Note that the current add operation
                                                // was successful
                                                addsProcessed.Add(currentAdd);
                                            }

                                            // Remove the current add operation from
                                            // the list so that it can be attempted
                                            // again if required
                                            addsCurrentlyProcessing.Remove(currentAdd);
                                        });
                                    }
                                    }
				                } );
                            } else {

                                // Otherwise load in the retrieved content
                                genes = databaseResponse.Content.Map( delegate( Gene gene ) { return new GeneInfo( gene ); } );

                                // If the connection was successful, set the local
                                // datbase as up
                                localMySQLUp = true;

                                // Added so we can match up GO terms with the genes we
                                // retrieved
                                if (Constants.UseGoTerms) {
                                    RetrieveLocalGOTerms_Regulog(genes, localMySQL, regulogId, regulog);
                                } else {
					                genesByRegulogId[regulogId] = genes;

					                foreach ( GeneInfo r in genes ) {
						                genesById[r.VimssId] = r;
					                }

					                regulog.Genes = genes;
                                }
                            }
                        });
                    } else {
                        // Use the regular RegPrecise service
                        IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				        svc.ListGenesInRegulog( regulogId, delegate( ServiceResponse<Gene[]> response ) {
					        if ( response.Error != null ) {
						        string e = response.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestGenesInRegulog: Server returned error '{0}'", e ) );
						        return;
					        }

					        genes = response.Content == null
						        ? new GeneInfo[0]
						        : response.Content.Map( delegate( Gene gene ) { return new GeneInfo( gene ); } );

                            // Added so we can match up GO terms with the genes we
                            // retrieved
                            if (Constants.UseGoTerms) {
                                ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );
                                RetrieveLocalGOTerms_Regulog(genes, localMySQL, regulogId, regulog);

                            // If GO terms are not enabled, go straight to
                            // setting the genes
                            } else {
					            genesByRegulogId[regulogId] = genes;

					            foreach ( GeneInfo r in genes ) {
						            genesById[r.VimssId] = r;
					            }

					            regulog.Genes = genes;
                            }

                            // Inform listeners that RegPrecise was contacted
                            OnRegPreciseContact(new EventArgs());
				        } );
                    }
			    }
            }
		}

		/// <summary> Gets a list containing the regulators in a regulog.
		/// <para>
		///		THIS APPEARS TO BE VERY SLOW!!!
		/// </para>
		/// </summary>
		/// <param name="regulog"></param>
        /// <param name="local">Whether the dataset is local</param>

		public void RequestRegulatorsInRegulog ( RegulogInfo regulog, bool local ) {
			if ( !Script.IsValue( regulatorsById ) ) {
				regulatorsById = new Dictionary<Number, RegulatorInfo>();
			}

			if ( !Script.IsValue( regulatorsByRegulogId ) ) {
				regulatorsByRegulogId = new Dictionary<Number, RegulatorInfo[]>();
			}

			Number regulogId = regulog.RegulogId;

			RegulatorInfo[] regulators = regulatorsByRegulogId[regulogId];

			if ( Script.IsValue( regulators ) && !forceReload ) {
				regulog.Regulators = regulators;
			}
			else {
                if ( !local ) {
                    if (Constants.UseLocalMySQLCache) {
                        // Try loading from the local MySQL database first
                        ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                        localMySQL.ListRegulatorsInRegulog( regulogId, delegate(ServiceResponse<Regulator[]> databaseResponse ) {

                            // If there's an error loading from the database or nothing
                            // was retrieved, then proceed to RegPrecise
                            if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                                // If there was an error, display a message
                                if ( databaseResponse.Error != null ) {
						            string e = databaseResponse.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulatorsInRegulog: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                    // If the database connection was refused, set the
                                    // local database as down for now
                                    if (e.IndexOf("ECONNREFUSED") != -1) {
                                        localMySQLUp = false;
                                    }

                                    // Otherwise, assume the connection is up
                                    else {
                                        localMySQLUp = true;
                                    }
                                }

                                // If nothing was retrieved, display a message
                                else if ( databaseResponse.Content == null ) {
                                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulatorsInRegulog: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                                }

                                // Regular RegPrecise service
				                IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				                svc.ListRegulatorsInRegulog( regulogId, delegate( ServiceResponse<Regulator[]> response ) {
					                if ( response.Error != null ) {
						                string e = response.Error;
						                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulatorsInRegulog: Server returned error '{0}'", e ) );
						                return;
					                }

					                regulators = response.Content == null
						                ? new RegulatorInfo[0]
						                : response.Content.Map( delegate( Regulator regulator ) { return new RegulatorInfo( regulator ); } );
					                regulatorsByRegulogId[regulogId] = regulators;

					                foreach ( RegulatorInfo r in regulators ) {
						                regulatorsById[r.VimssId] = r;
					                }

					                regulog.Regulators = regulators;

                                    // Inform listeners that RegPrecise was contacted
                                    OnRegPreciseContact(new EventArgs());

                                    if (response.Content != null) {
                                    // Attempt to add the retrieved data to the local
                                    // database if it is up
                                    string currentAdd = "AddRegulators_ListRegulatorsInRegulog_" + regulogId;

                                    if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                    } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                    } else if (localMySQLUp) {

                                        // Note the current add operation to avoid
                                        // multiples of the same
                                        addsCurrentlyProcessing.Add(currentAdd);

                                        // Call the service to add the data
                                        localMySQL.AddRegulators_ListRegulatorsInRegulog( regulogId, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} regulators(s) to the local MySQL database", response.Content.Length));
					                        if ( databaseResponseAdd.Error != null ) {
						                        string e = databaseResponseAdd.Error;
						                        if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulators to the local MySQL database: Server returned error '{0}'", e ) );
						                        return;
					                        }

					                        if ( databaseResponseAdd.Content != null ) {
                                                if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulators added to the local MySQL database", databaseResponseAdd.Content));

                                                // Note that the current add operation
                                                // was successful
                                                addsProcessed.Add(currentAdd);
                                            }

                                            // Remove the current add operation from
                                            // the list so that it can be attempted
                                            // again if required
                                            addsCurrentlyProcessing.Remove(currentAdd);
                                        });
                                    }
                                    }
				                } );
                            } else {

                                // Otherwise load in the retrieved content
                                regulators = databaseResponse.Content.Map( delegate( Regulator regulator ) { return new RegulatorInfo( regulator ); } );
					            regulatorsByRegulogId[regulogId] = regulators;

					            foreach ( RegulatorInfo r in regulators ) {
						            regulatorsById[r.VimssId] = r;
					            }

					            regulog.Regulators = regulators;

                                // If the connection was successful, set the local
                                // datbase as up
                                localMySQLUp = true;
                            }
                        });
                    } else {
                        // Use the regular RegPrecise serivce
                        IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				        svc.ListRegulatorsInRegulog( regulogId, delegate( ServiceResponse<Regulator[]> response ) {
					        if ( response.Error != null ) {
						        string e = response.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulatorsInRegulog: Server returned error '{0}'", e ) );
						        return;
					        }

					        regulators = response.Content == null
						        ? new RegulatorInfo[0]
						        : response.Content.Map( delegate( Regulator regulator ) { return new RegulatorInfo( regulator ); } );
					        regulatorsByRegulogId[regulogId] = regulators;

					        foreach ( RegulatorInfo r in regulators ) {
						        regulatorsById[r.VimssId] = r;
					        }

					        regulog.Regulators = regulators;

                            // Inform listeners that RegPrecise was contacted
                            OnRegPreciseContact(new EventArgs());
				        } );
                    }
			    }
            }
		}

        /// <summary>
        /// Gets a list containing all the regulators in a regulon and associates them with the regulon object
        /// </summary>
        /// <param name="regulon">The regulon ID</param>
        /// <param name="callback">The action to perform upon loading the regulators</param>
		public void RequestRegulatorsInRegulon ( RegulonInfo regulon, Action<RegulonInfo> callback ) {
			if ( !Script.IsValue( regulatorsById ) ) {
				regulatorsById = new Dictionary<Number, RegulatorInfo>();
			}

			if ( !Script.IsValue( regulatorsByRegulonId ) ) {
				regulatorsByRegulonId = new Dictionary<Number, RegulatorInfo[]>();
			}

			Number regulonId = regulon.RegulonId;

			RegulatorInfo[] regulators = regulatorsByRegulonId[regulonId];

			if ( !Script.IsValue( regulators ) || forceReload ) {
                if (Constants.UseLocalMySQLCache) {
                    // Try loading from the local MySQL database first
                    ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                    localMySQL.ListRegulatorsInRegulon( regulonId, delegate(ServiceResponse<Regulator[]> databaseResponse ) {

                        // If there's an error loading from the database or nothing
                        // was retrieved, then proceed to RegPrecise
                        if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                            // If there was an error, display a message
                            if ( databaseResponse.Error != null ) {
						        string e = databaseResponse.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulatorsInRegulon: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                // If the database connection was refused, set the
                                // local database as down for now
                                if (e.IndexOf("ECONNREFUSED") != -1) {
                                    localMySQLUp = false;
                                }

                                // Otherwise, assume the connection is up
                                else {
                                    localMySQLUp = true;
                                }
                            }

                            // If nothing was retrieved, display a message
                            else if ( databaseResponse.Content == null ) {
                                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulatorsInRegulon: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                            }

                            // Regular RegPrecise service
				            IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				            svc.ListRegulatorsInRegulon( regulonId, delegate( ServiceResponse<Regulator[]> response ) {
					            if ( response.Error != null ) {
						            string e = response.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulatorsInRegulon: Server returned error '{0}'", e ) );
						            return;
					            }

					            regulators = response.Content == null
						            ? new RegulatorInfo[0]
						            : response.Content.Map( delegate( Regulator regulator ) { return new RegulatorInfo( regulator ); } );
					            regulatorsByRegulonId[regulonId] = regulators;
					            regulon.Regulators = regulators;

					            foreach ( RegulatorInfo r in regulators ) {
						            regulatorsById[r.VimssId] = r;
					            }

					            callback( regulon );

                                // Inform listeners that RegPrecise was contacted
                                OnRegPreciseContact(new EventArgs());

                                if (response.Content != null) {
                                // Attempt to add the retrieved data to the local
                                // database if it is up
                                string currentAdd = "AddRegulators_ListRegulatorsInRegulon_" + regulonId;

                                if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                } else if (localMySQLUp) {

                                    // Note the current add operation to avoid
                                    // multiples of the same
                                    addsCurrentlyProcessing.Add(currentAdd);

                                    // Call the service to add the data
                                    localMySQL.AddRegulators_ListRegulatorsInRegulon( regulonId, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} regulators(s) to the local MySQL database", response.Content.Length));
					                    if ( databaseResponseAdd.Error != null ) {
						                    string e = databaseResponseAdd.Error;
						                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulators to the local MySQL database: Server returned error '{0}'", e ) );
						                    return;
					                    }

					                    if ( databaseResponseAdd.Content != null ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulators added to the local MySQL database", databaseResponseAdd.Content));

                                            // Note that the current add operation
                                            // was successful
                                            addsProcessed.Add(currentAdd);
                                        }

                                        // Remove the current add operation from
                                        // the list so that it can be attempted
                                        // again if required
                                        addsCurrentlyProcessing.Remove(currentAdd);
                                    });
                                }
                                }
				            } );
                        } else {

                            // Otherwise load in the retrieved content
                            regulators = databaseResponse.Content.Map( delegate( Regulator regulator ) { return new RegulatorInfo( regulator ); } );
					        regulatorsByRegulonId[regulonId] = regulators;
					        regulon.Regulators = regulators;

					        foreach ( RegulatorInfo r in regulators ) {
						        regulatorsById[r.VimssId] = r;
					        }

					        callback( regulon );

                            // If the connection was successful, set the local
                            // datbase as up
                            localMySQLUp = true;
                        }
                    });
			    } else {
                    // Use the regular RegPrecise service
                    IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				    svc.ListRegulatorsInRegulon( regulonId, delegate( ServiceResponse<Regulator[]> response ) {
					    if ( response.Error != null ) {
						    string e = response.Error;
						    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulatorsInRegulon: Server returned error '{0}'", e ) );
						    return;
					    }

					    regulators = response.Content == null
						    ? new RegulatorInfo[0]
						    : response.Content.Map( delegate( Regulator regulator ) { return new RegulatorInfo( regulator ); } );
					    regulatorsByRegulonId[regulonId] = regulators;
					    regulon.Regulators = regulators;

					    foreach ( RegulatorInfo r in regulators ) {
						    regulatorsById[r.VimssId] = r;
					    }

					    callback( regulon );

                        // Inform listeners that RegPrecise was contacted
                        OnRegPreciseContact(new EventArgs());
				    } );
                }
            } else {
			    callback( regulon );
		    }
		}

		/// <summary> PropertyChanged event handler.
		/// </summary>

		public event PropertyChangedEventHandler PropertyChanged;
		private  Dictionary<Number,SiteInfo> sitesById;
		private  Dictionary<Number,SiteInfo[]> sitesByRegulogId;
		private  Dictionary<Number,RegulonInfo[]> regulonsByGenomeId = new Dictionary<Number, RegulonInfo[]>();

		/// <summary> Fire PropertyChanged events.
		/// </summary>

		private void NotifyPropertyChanged ( string propertyName ) {
			if ( PropertyChanged != null ) {
				PropertyChanged( this, new PropertyChangedEventArgs( propertyName ) );
			}
		}

		/// <summary> Requests the list of regulatory sites available for a regulog.
		/// <para>
		///		When the data becomes available it is stored in the regulog's Sites property.
		/// </para>
		/// </summary>
		/// <param name="regulog"></param>
        /// <param name="local">Whether the dataset is local</param>

		internal void RequestSitesInRegulog ( RegulogInfo regulog, bool local ) {
			if ( !Script.IsValue( sitesById ) ) {
				sitesById = new Dictionary<Number, SiteInfo>();
			}

			if ( !Script.IsValue( sitesByRegulogId ) ) {
				sitesByRegulogId = new Dictionary<Number, SiteInfo[]>();
			}

			Number regulogId = regulog.RegulogId;

			SiteInfo [] sites = sitesByRegulogId[regulogId];

			if ( Script.IsValue( sites ) && !forceReload ) {
				regulog.Sites = sites;
			}
			else {
                if ( !local ) {
                    if (Constants.UseLocalMySQLCache) {
                        // Try loading from the local MySQL database first
                        ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                        localMySQL.ListSitesInRegulog( regulogId, delegate(ServiceResponse<Site[]> databaseResponse ) {

                            // If there's an error loading from the database or nothing
                            // was retrieved, then proceed to RegPrecise
                            if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                                // If there was an error, display a message
                                if ( databaseResponse.Error != null ) {
						            string e = databaseResponse.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestSitesInRegulog: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                    // If the database connection was refused, set the
                                    // local database as down for now
                                    if (e.IndexOf("ECONNREFUSED") != -1) {
                                        localMySQLUp = false;
                                    }

                                    // Otherwise, assume the connection is up
                                    else {
                                        localMySQLUp = true;
                                    }
                                }

                                // If nothing was retrieved, display a message
                                else if ( databaseResponse.Content == null ) {
                                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestSitesInRegulog: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                                }

                                // Regular RegPrecise service
				                IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				                svc.ListSitesInRegulog( regulogId, delegate( ServiceResponse<Site[]> response ) {
					                if ( response.Error != null ) {
						                string e = response.Error;
						                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestSitesInRegulog: Server returned error '{0}'", e ) );
						                return;
					                }

					                sites = response.Content == null
						                ? new SiteInfo[0]
						                : response.Content.Map( delegate( Site site ) { return new SiteInfo( site ); } );
					                sitesByRegulogId[regulogId] = sites;

					                foreach ( SiteInfo site in sites ) {
						                sitesById[site.GeneVimssid] = site;
					                }

					                regulog.Sites = sites;

                                    // Inform listeners that RegPrecise was contacted
                                    OnRegPreciseContact(new EventArgs());

                                    if (response.Content != null) {
                                    // Attempt to add the retrieved data to the local
                                    // database if it is up
                                    string currentAdd = "AddSites_ListSitesInRegulog_" + regulogId;

                                    if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                    } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                    } else if (localMySQLUp) {

                                        // Note the current add operation to avoid
                                        // multiples of the same
                                        addsCurrentlyProcessing.Add(currentAdd);

                                        // Call the service to add the data
                                        localMySQL.AddSites_ListSitesInRegulog( regulogId, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} regulators(s) to the local MySQL database", response.Content.Length));
					                        if ( databaseResponseAdd.Error != null ) {
						                        string e = databaseResponseAdd.Error;
						                        if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulators to the local MySQL database: Server returned error '{0}'", e ) );
						                        return;
					                        }

					                        if ( databaseResponseAdd.Content != null ) {
                                                if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulators added to the local MySQL database", databaseResponseAdd.Content));

                                                // Note that the current add operation
                                                // was successful
                                                addsProcessed.Add(currentAdd);
                                            }

                                            // Remove the current add operation from
                                            // the list so that it can be attempted
                                            // again if required
                                            addsCurrentlyProcessing.Remove(currentAdd);
                                        });
                                    }
                                    }

    #if DEBUG
                                    // Try loading additional operon sites from a RegPrecise text
                                    // file
                                    LoadOperonsFromText(regulog);
    #endif
				                } );
                            } else {

                                // Otherwise load in the retrieved content
                                sites = databaseResponse.Content.Map( delegate( Site site ) { return new SiteInfo( site ); } );
					            sitesByRegulogId[regulogId] = sites;

					            foreach ( SiteInfo site in sites ) {
						            sitesById[site.GeneVimssid] = site;
					            }

					            regulog.Sites = sites;

                                // If the connection was successful, set the local
                                // datbase as up
                                localMySQLUp = true;
                            }
                        });
                    } else {
                        // Use the regular RegPrecise service
                        IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				        svc.ListSitesInRegulog( regulogId, delegate( ServiceResponse<Site[]> response ) {
					        if ( response.Error != null ) {
						        string e = response.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestSitesInRegulog: Server returned error '{0}'", e ) );
						        return;
					        }

					        sites = response.Content == null
						        ? new SiteInfo[0]
						        : response.Content.Map( delegate( Site site ) { return new SiteInfo( site ); } );
					        sitesByRegulogId[regulogId] = sites;

					        foreach ( SiteInfo site in sites ) {
						        sitesById[site.GeneVimssid] = site;
					        }

					        regulog.Sites = sites;

                            // Inform listeners that RegPrecise was contacted
                            OnRegPreciseContact(new EventArgs());
#if DEBUG
                            // Try loading additional operon sites from a RegPrecise text
                            // file
                            LoadOperonsFromText(regulog);
#endif
				        } );
                    }
			    }
            }
		}

		/// <summary> Retrieves regulons for a genome from the server.
		/// <para>
		///		As soon as the results become available, they are stored in the
		///		Regulons property of the genome. NotifyPropertyChanged data flow then
		///		propagates the required data to those who need to know.
		/// </para>
		/// </summary>
		/// <param name="genome"></param>

		internal void RequestRegulonsInGenome ( GenomeInfo genome ) {
			Number genomeId = genome.GenomeId;

			RegulonInfo [] regulons = regulonsByGenomeId[genomeId];

			if ( Script.IsValue( regulons ) && !forceReload ) {
				genome.Regulons = regulons;
			}
			else {
                if (Constants.UseLocalMySQLCache) {
                    // Try loading from the local MySQL database first
                    ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                    localMySQL.ListRegulonsInGenome( genomeId, delegate(ServiceResponse<Regulon[]> databaseResponse ) {

                        // If there's an error loading from the database or nothing
                        // was retrieved, then proceed to RegPrecise
                        if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                            // If there was an error, display a message
                            if ( databaseResponse.Error != null ) {
						        string e = databaseResponse.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulonsInGenome: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                // If the database connection was refused, set the
                                // local database as down for now
                                if (e.IndexOf("ECONNREFUSED") != -1) {
                                    localMySQLUp = false;
                                }

                                // Otherwise, assume the connection is up
                                else {
                                    localMySQLUp = true;
                                }
                            }

                            // If nothing was retrieved, display a message
                            else if ( databaseResponse.Content == null ) {
                                if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulonsInGenome: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                            }

                            // Regular RegPrecise service
				            IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				            svc.ListRegulonsInGenome( genomeId, delegate( ServiceResponse<Regulon[]> response ) {
					            if ( response.Error != null ) {
						            string e = response.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulonsInGenome: Server returned error '{0}'", e ) );
						            return;
					            }

					            regulons = response.Content == null
						            ? new RegulonInfo[0]
						            : response.Content.Map<Regulon, RegulonInfo>( ProcessRegPreciseRegulon );

					            genome.NotifyPropertyChanged( "Regulons" );

					            regulonsByGenomeId[genomeId] = regulons;

                                // Inform listeners that RegPrecise was contacted
                                OnRegPreciseContact(new EventArgs());

                                if (response.Content != null) {
                                // Attempt to add the retrieved data to the local
                                // database if it is up
                                string currentAdd = "AddRegulons_ListRegulonsInGenome_" + genomeId;

                                if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                } else if (localMySQLUp) {

                                    // Note the current add operation to avoid
                                    // multiples of the same
                                    addsCurrentlyProcessing.Add(currentAdd);

                                    // Call the service to add the data
                                    localMySQL.AddRegulons_ListRegulonsInGenome( genomeId, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add {0} regulon(s) to the local MySQL database", response.Content.Length));
					                    if ( databaseResponseAdd.Error != null ) {
						                    string e = databaseResponseAdd.Error;
						                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulons to the local MySQL database: Server returned error '{0}'", e ) );
						                    return;
					                    }

					                    if ( databaseResponseAdd.Content != null ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulons added to the local MySQL database", databaseResponseAdd.Content));

                                            // Note that the current add operation
                                            // was successful
                                            addsProcessed.Add(currentAdd);
                                        }

                                        // Remove the current add operation from
                                        // the list so that it can be attempted
                                        // again if required
                                        addsCurrentlyProcessing.Remove(currentAdd);
                                    });
                                }
                                }
				            } );
                        } else {

                            // Otherwise load in the retrieved content
                            regulons = databaseResponse.Content.Map<Regulon, RegulonInfo>( ProcessRegPreciseRegulon );

				            genome.NotifyPropertyChanged( "Regulons" );

				            regulonsByGenomeId[genomeId] = regulons;

                            // If the connection was successful, set the local
                            // datbase as up
                            localMySQLUp = true;
                        }
                    });
			    } else {
                    //Use the regular RegPrecise service
                    IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				    svc.ListRegulonsInGenome( genomeId, delegate( ServiceResponse<Regulon[]> response ) {
					    if ( response.Error != null ) {
						    string e = response.Error;
						    if (Constants.ShowDebugMessages) Console.Log( string.Format( "RequestRegulonsInGenome: Server returned error '{0}'", e ) );
						    return;
					    }

					    regulons = response.Content == null
						    ? new RegulonInfo[0]
						    : response.Content.Map<Regulon, RegulonInfo>( ProcessRegPreciseRegulon );

					    genome.NotifyPropertyChanged( "Regulons" );

					    regulonsByGenomeId[genomeId] = regulons;

                        // Inform listeners that RegPrecise was contacted
                        OnRegPreciseContact(new EventArgs());
				    } );
                }
            }
		}

        /// <summary>
        /// Retrieves a specific regulog in order to perform a specified action to it
        /// </summary>
        /// <param name="number">The regulog ID</param>
        /// <param name="action">The action to perform upon loading the regulog</param>
		internal void GetRegulog ( Number number, Action<RegulogInfo> action ) {
			if ( regulogsByRegulogId == null ) {
				regulogsByRegulogId = new Dictionary<int, RegulogInfo>();
			}

			RegulogInfo regulog = regulogsByRegulogId[number];

			if ( Script.IsValue( regulog ) && !forceReload ) {
				action( regulog );

#if DEBUG
                    // Inform listeners that a regulog was loaded from RegPrecise
                    OnRegulogLoaded(new RegulogLoadedEventArgs(false));
#endif
			}
			else {
                if (Constants.UseLocalMySQLCache) {
                    // Try loading from the local MySQL database first
                    ILocalMySQLService localMySQL = new LocalMySQLService( Constants.LOCAL_MYSQL_SERVICE_URL, false );

                    localMySQL.GetRegulog( number, delegate(ServiceResponse<Regulog> databaseResponse ) {

                        // If there's an error loading from the database or nothing
                        // was retrieved, then proceed to RegPrecise
                        if ( databaseResponse.Error != null || databaseResponse.Content == null ) {

                            // If there was an error, display a message
                            if ( databaseResponse.Error != null ) {
						        string e = databaseResponse.Error;
						        if (Constants.ShowDebugMessages) Console.Log( string.Format( "GetRegulog: Local MySQL database returned error '{0}' - proceeding to attempt RegPrecise", e ) );

                                // If the database connection was refused, set the
                                // local database as down for now
                                if (e.IndexOf("ECONNREFUSED") != -1) {
                                    localMySQLUp = false;
                                }

                                // Otherwise, assume the connection is up
                                else {
                                    localMySQLUp = true;
                                }
                            }

                            // If nothing was retrieved, display a message
                            else if ( databaseResponse.Content == null ) {
                                if (Constants.ShowDebugMessages) Console.Log( string.Format( "GetRegulog: Local MySQL database does not contain the requested data - proceeding to attempt RegPrecise" ) );
                            }

                            // Regular RegPrecise service
                            IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				            svc.GetRegulog( number, delegate( ServiceResponse<Regulog> response ) {
					            if ( response.Error != null ) {
						            string e = response.Error;
						            if (Constants.ShowDebugMessages) Console.Log( string.Format( "GetRegulog: Server returned error '{0}'", e ) );
						            return;
					            }

					            regulog = response.Content == null
						            ? null
						            : new RegulogInfo( response.Content );

					            regulogsByRegulogId[number] = regulog;
					            action( regulog );

                                // Inform listeners that RegPrecise was contacted
                                OnRegPreciseContact(new EventArgs());

    #if DEBUG
                                // Inform listeners that a regulog was loaded from RegPrecise
                                OnRegulogLoaded(new RegulogLoadedEventArgs(true));
    #endif
                                if (response.Content != null) {
                                // Attempt to add the retrieved data to the local
                                // database if it is up
                                string currentAdd = "AddRegulogs_GetRegulog_" + number;

                                if (addsCurrentlyProcessing.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " is already being processed");
#endif
                                } else if (addsProcessed.Contains(currentAdd)) {
#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(currentAdd + " has already been processed this session");
#endif
                                } else if (localMySQLUp) {

                                    // Note the current add operation to avoid
                                    // multiples of the same
                                    addsCurrentlyProcessing.Add(currentAdd);

                                    // Call the service to add the data
                                    localMySQL.AddRegulogs_GetRegulog( number, response.Content, delegate( ServiceResponse<Number> databaseResponseAdd ) {
                                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Attempting to add regulog {0} to the local MySQL database", number));
					                    if ( databaseResponseAdd.Error != null ) {
						                    string e = databaseResponseAdd.Error;
						                    if (Constants.ShowDebugMessages) Console.Log( string.Format( "Failed to add regulogs to the local MySQL database: Server returned error '{0}'", e ) );
						                    return;
					                    }

					                    if ( databaseResponseAdd.Content != null ) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("{0} regulogs added to the local MySQL database", databaseResponseAdd.Content));

                                            // Note that the current add operation
                                            // was successful
                                            addsProcessed.Add(currentAdd);
                                        }

                                        // Remove the current add operation from
                                        // the list so that it can be attempted
                                        // again if required
                                        addsCurrentlyProcessing.Remove(currentAdd);
                                    });
                                }
                                }
				            } );
                        } else {

                            // Otherwise load in the retrieved content
                            regulog = new RegulogInfo( databaseResponse.Content );

                            action( regulog );

    #if DEBUG
                            // Inform listeners that a regulog was loaded from RegPrecise
                            OnRegulogLoaded(new RegulogLoadedEventArgs(true));
    #endif

                            // If the connection was successful, set the local
                            // datbase as up
                            localMySQLUp = true;
                        }
                    });
			    } else {
                    // Use the regular RegPrecise service
                    IRegPreciseService svc = new RegPreciseService( Constants.REG_PRECISE_SERVICE_URL, false );

				    svc.GetRegulog( number, delegate( ServiceResponse<Regulog> response ) {
					    if ( response.Error != null ) {
						    string e = response.Error;
						    if (Constants.ShowDebugMessages) Console.Log( string.Format( "GetRegulog: Server returned error '{0}'", e ) );
						    return;
					    }

					    regulog = response.Content == null
						    ? null
						    : new RegulogInfo( response.Content );

					    regulogsByRegulogId[number] = regulog;
					    action( regulog );

                        // Inform listeners that RegPrecise was contacted
                        OnRegPreciseContact(new EventArgs());

#if DEBUG
                        // Inform listeners that a regulog was loaded from RegPrecise
                        OnRegulogLoaded(new RegulogLoadedEventArgs(true));
#endif
				    } );
                }
            }
		}

		/// <summary> Saves a regulog together with its genes,
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="selectedRegulog"></param>

		internal static void SaveRegulogAsCsv ( string fileName, RegulogInfo selectedRegulog ) {
			GenomeStats [] genomes = selectedRegulog.Genomes.Map<GenomeInfo, GenomeStats>(
				delegate( GenomeInfo g ) { return g.GenomeStats; }
			);
			Regulog [] regulogs = { selectedRegulog.Regulog };
			Regulon [] regulons = selectedRegulog.Regulons.Map(
				delegate( RegulonInfo r ) { return r.Regulon; }
			);
			Gene [] genes = selectedRegulog.Genes.Map( delegate( GeneInfo g ) { return g.Gene; } );
			Regulator [] regulators = selectedRegulog.Regulators.Map( delegate( RegulatorInfo r ) { return r.Regulator; } );
			Site [] sites = selectedRegulog.Sites.Map( delegate( SiteInfo s ) { return s.Site; } );

			JSObject [][] collections = { genomes, regulogs, regulons, genes, regulators, sites };

			string [] tableNames = { "Genomes", "Regulogs", "Regulons", "Genes", "Regulators", "Sites" };

			StringWriter writer = new StringWriter();

			for ( int i = 0; i < tableNames.Length; i++ ) {
				writer.WriteLine( tableNames[i], null );
				writer.WriteLn();
				writer.WriteString( Objects.SerializeTable( collections[i] ) );
				writer.WriteLn();
			}

			HtmlUtil.SaveText( fileName, writer.ToString(), "text/csv" );
		}

		internal static void LoadRegulog (
			File file,
			Action<FileProgressEvent> onError,
			Action<RegulogInfo[]> onComplete
		) {
			FileReader reader = new FileReader();
			reader.OnError = onError;

			reader.OnLoadEnd = delegate( FileProgressEvent evt ) {
				try {
					RegulogInfo [] regulogs = ParseRegulog( (string) reader.Result );
					onComplete( regulogs );
				}
				catch {
#if DEBUG
					Window.Alert( Constants.Text_MessageErrorLoadingRegulog + "\n\nError: " + Script.Literal("$e1['message']") + "\n\nStack: " + Script.Literal("$e1['stack']") );
#else
                    Window.Alert( Constants.Text_MessageErrorLoadingRegulog );
#endif
#if DEBUG
                    // This isn't the intended way to call this, but it allows
                    // the loading overlay to be hidden
                    onError( null );
#endif
				}
			};

#if DEBUG
            try {
			    reader.ReadAsText( file );
            }
            catch {
                if (file == null) {
                    if (Constants.ShowDebugMessages) Console.Log("No file to load!");
                    // This isn't the intended way to call this, but it allows
                    // the loading overlay to be hidden
                    onError( null );
                } else {
                    if (Constants.ShowDebugMessages) Console.Log("Failed to load file in a way that wasn't handled by onError.");
                    Window.Alert(Constants.Text_MessageErrorLoadingRegulog);
                    // This isn't the intended way to call this, but it allows
                    // the loading overlay to be hidden
                    onError( null );
                }
            }
#else
			reader.ReadAsText( file );
#endif
		}

		static GenomeInfo ParseGenome ( JSObject obj ) {
			return new GenomeInfo( GenomeStats.Parse( obj ), DataLayer.LocalFile );
		}

		private static RegulogInfo[] ParseRegulog (
			string text
		) {
			#region Parse CSV into records.
			CsvIO csv = new CsvIO();
			StringReader reader = new StringReader( text );
			string [][] records = csv.Read( reader );
			#endregion

			#region Split records up into a collection of tables.
			List<string[]> genomeRecords = new List<string[]>();
			List<string[]> regulogRecords = new List<string[]>();
			List<string[]> regulonRecords = new List<string[]>();
			List<string[]> geneRecords = new List<string[]>();
			List<string[]> regulatorRecords = new List<string[]>();
			List<string[]> siteRecords = new List<string[]>();

			List<string[]> [] tables = { genomeRecords, regulogRecords, regulonRecords, geneRecords, regulatorRecords, siteRecords };
			string [] tableNames     = { "genomes", "regulogs", "regulons", "genes", "regulators", "sites" };

			List<string[]> currentTable = null;

			foreach ( string [] record in records ) {
				if ( record.Length == 0 || Enumerable.All( record, delegate( string r ) { return string.IsNullOrEmpty( r ); } ) ) continue;

				int nameIndex = tableNames.IndexOf( record[0].ToLowerCase() );

				if ( nameIndex >= 0 ) {
					currentTable = tables[nameIndex];
				}
				else {
					currentTable.Add( record );
				}
			}
			#endregion

			#region Parse tables into lists of xyzInfo objects
			GenomeInfo[] genomes = Objects.DeserializeRecords( genomeRecords )
										  .Map<JSObject, GenomeInfo>( ParseGenome );
			RegulogInfo [] regulogs = Objects.DeserializeRecords( regulogRecords ).Map<JSObject, RegulogInfo>( RegulogInfo.Parse );
			RegulonInfo [] regulons = Objects.DeserializeRecords( regulonRecords ).Map<JSObject, RegulonInfo>( RegulonInfo.Parse );
			GeneInfo [] genes = Objects.DeserializeRecords( geneRecords ).Map<JSObject, GeneInfo>( GeneInfo.Parse );
			RegulatorInfo [] regulators = Objects.DeserializeRecords( regulatorRecords ).Map<JSObject, RegulatorInfo>( RegulatorInfo.Parse );
			SiteInfo [] sites = Objects.DeserializeRecords( siteRecords ).Map<JSObject, SiteInfo>( SiteInfo.Parse );
			#endregion

			// Wire the records into the regulogs.
			Dictionary<int,List<GeneInfo>> regulogGenes = PartitionGenesByRegulog( regulons, genes );
			Dictionary<int,List<SiteInfo>> regulogSites = PartitionSitesByRegulog( regulons, sites );
			Dictionary<int,List<RegulatorInfo>> regulogRegulators = PartitionRegulatorsByRegulog( regulons, regulators );
			Dictionary<int, List<GenomeInfo>> regulogGenomes = PartitionGenomesByRegulog( regulons, genomes );

			foreach ( RegulogInfo regulog in regulogs ) {
				int regulogId = regulog.RegulogId;

				regulog.Regulons = regulons.Filter( delegate( RegulonInfo regulon ) {
					return regulogId == regulon.RegulogId;
				} );
				regulog.Genes = regulogGenes[regulogId];
				regulog.Sites = regulogSites[regulogId];
				regulog.Regulators = regulogRegulators[regulogId];
			}

			// Add the genomes to the data store.
			foreach ( GenomeInfo genome in genomes ) {
				Instance.Genomes.AddOrReplace( genome );
			}

			// Connect the Relulons to their associated Genomes.
			foreach ( RegulonInfo regulon in regulons ) {
				GenomeInfo genome = Instance.Genomes.Get( LocalFile, regulon.GenomeId );

				if ( genome == null ) {
					GenomeStats gStats = new GenomeStats(
						regulon.GenomeId,
						regulon.GenomeName,
						0, 0, 0, 0, 0
					);
					genome = new GenomeInfo( gStats );
				}

				genome.AddRegulon( regulon );
			}

			// Done
			return regulogs;
		}

		private static Dictionary<int, List<GeneInfo>> PartitionGenesByRegulog ( RegulonInfo[] regulons, GeneInfo[] genes ) {
			Dictionary<int,List<GeneInfo>> map = new Dictionary<int, List<GeneInfo>>();

			foreach ( RegulonInfo regulon in regulons ) {
				int regulogId = regulon.RegulogId;
				int regulonId = regulon.RegulonId;

				GeneInfo [] genes_ = genes.Filter( delegate( GeneInfo g ) { return g.RegulonId == regulonId; } );

				if ( map.ContainsKey( regulogId ) ) {
					foreach ( GeneInfo g in genes_ ) {
						map[regulogId].Add( g );
					}
				}
				else {
					map[regulogId] = (List<GeneInfo>) genes_;
				}
			}
			return map;
		}

		private static Dictionary<int, List<SiteInfo>> PartitionSitesByRegulog ( RegulonInfo[] regulons, SiteInfo[] sites ) {
			Dictionary<int,List<SiteInfo>> map = new Dictionary<int, List<SiteInfo>>();

			foreach ( RegulonInfo regulon in regulons ) {
				int regulogId = regulon.RegulogId;
				int regulonId = regulon.RegulonId;

				SiteInfo [] sites_ = sites.Filter( delegate( SiteInfo s ) { return s.RegulonId == regulonId; } );

				if ( map.ContainsKey( regulogId ) ) {
					foreach ( SiteInfo g in sites_ ) {
						map[regulogId].Add( g );
					}
				}
				else {
					map[regulogId] = (List<SiteInfo>) sites_;
				}
			}
			return map;
		}

		private static Dictionary<int, List<RegulatorInfo>> PartitionRegulatorsByRegulog ( RegulonInfo[] regulons, RegulatorInfo[] regulators ) {
			Dictionary<int,List<RegulatorInfo>> map = new Dictionary<int, List<RegulatorInfo>>();

			foreach ( RegulonInfo regulon in regulons ) {
				int regulogId = regulon.RegulogId;
				int regulonId = regulon.RegulonId;

				RegulatorInfo [] regulators_ = regulators.Filter( delegate( RegulatorInfo s ) { return s.RegulonId == regulonId; } );

				if ( map.ContainsKey( regulogId ) ) {
					foreach ( RegulatorInfo g in regulators_ ) {
						map[regulogId].Add( g );
					}
				}
				else {
					map[regulogId] = (List<RegulatorInfo>) regulators_;
				}
			}
			return map;
		}

		private static Dictionary<int, List<GenomeInfo>> PartitionGenomesByRegulog ( RegulonInfo[] regulons, GenomeInfo[] genomes ) {
			Dictionary<int, List<GenomeInfo>> map = new Dictionary<int, List<GenomeInfo>>();

			foreach ( RegulonInfo regulon in regulons ) {
				int regulogId = regulon.RegulogId;
				int genomeId = regulon.GenomeId;

				GenomeInfo[] genomes_ = genomes.Filter( delegate( GenomeInfo s ) { return s.GenomeId == genomeId; } );

				if ( map.ContainsKey( regulogId ) ) {
					foreach ( GenomeInfo g in genomes_ ) {
						map[regulogId].Add( g );
					}
				}
				else {
					map[regulogId] = (List<GenomeInfo>) genomes_;
				}
			}
			return map;
		}

		/// <summary> Constant identifier which defines the default source (RegPrecise).
		/// </summary>

		public static string RegPreciseId {
			get { return "RegPrecise"; }
		}

		/// <summary> Constant identifier which defines the default source (RegPrecise).
		/// </summary>

		public static string LocalFile {
			get { return "LocalFile"; }
		}

        /*/// <summary> Converts the current regulog into a binary vector and then saves it
        /// Does not save a file yet
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="selectedRegulog"></param>

        internal static void SaveRegulogAsBinaryVector(string fileName, RegulogInfo selectedRegulog)
        {
            //GenomeStats[] genomes = selectedRegulog.Genomes.Map<GenomeInfo, GenomeStats>(
            //    delegate(GenomeInfo g) { return g.GenomeStats; }
            //);
            //Regulog[] regulogs = { selectedRegulog.Regulog };
            //Regulon[] regulons = selectedRegulog.Regulons.Map(
            //    delegate(RegulonInfo r) { return r.Regulon; }
            //);
            //Gene[] genes = selectedRegulog.Genes.Map(delegate(GeneInfo g) { return g.Gene; });
            //Regulator[] regulators = selectedRegulog.Regulators.Map(delegate(RegulatorInfo r) { return r.Regulator; });
            //Site[] sites = selectedRegulog.Sites.Map(delegate(SiteInfo s) { return s.Site; });

            #region TestVector

            //Unfortunately, I'm going to have to make the same, possibly invalid, assumption here. Oh well...
            List<string> names = new List<string>();

            foreach (GeneInfo gene in selectedRegulog.TargetGenes)
            {
                // TODO: Allow alternate ortholog identification mechanism. One that actually works would be good.

                // We have to assume that orthologs have the same name. Otherwise, there
                // appears to be no way to connect them in RegPrecise. However, this is
                // probably an invalid assumption. nonetheless...

                string name = gene.Name;

                if (!names.Contains(name))
                {
                    names.Add(name);
                }
            }

            names.Sort();

            Dictionary<string, int> index = new Dictionary<string, int>();

            foreach (string name in names) index[name] = index.Count;

            int n = index.Keys.Length;

#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Creating vectors for regulog with regulator " + selectedRegulog.RegulatorName);
#endif
            int[][] vectors = new int[selectedRegulog.Regulons.Length][];
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("This regulog has " + selectedRegulog.Regulons.Length + " regulons");
#endif
            for (int i = 0; i < selectedRegulog.Regulons.Length; i++)
            {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Now creating vector for regulon " + selectedRegulog.Regulons[i].GenomeName);
#endif
                vectors[i] = new int[n];
                int count = 0;
                foreach (KeyValuePair<string, int> geneName in index)
                {
                    vectors[i][count] = 0;
                    foreach (GeneInfo regulonGene in selectedRegulog.Regulons[i].TargetGenes)
                    {

                        if (regulonGene.Name == geneName.Key)
                        {
                            vectors[i][count] = 1;
#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + selectedRegulog.Regulons[i].GenomeName + " - setting the entry in the vector to '1'");
#endif
                            break;
                        }
                    }
                    count = count + 1;
                }
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Vector for regulon " + selectedRegulog.Regulons[i].GenomeName + " created");
#endif
            }
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Vectors for regulog with regulator " + selectedRegulog.RegulatorName + " created");
#endif

            #endregion

            //JSObject[][] collections = { genomes, regulogs, regulons, genes, regulators, sites };

            //string[] tableNames = { "Genomes", "Regulogs", "Regulons", "Genes", "Regulators", "Sites" };

            //StringWriter writer = new StringWriter();

            //for (int i = 0; i < tableNames.Length; i++)
            //{
            //    writer.WriteLine(tableNames[i], null);
            //    writer.WriteLn();
            //    writer.WriteString(Objects.SerializeTable(collections[i]));
            //    writer.WriteLn();
            //}

            //HtmlUtil.SaveText(fileName, writer.ToString(), "text/csv");

#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Preparing contents of file for writing");
#endif

            StringWriter writer = new StringWriter();

#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Preparing array of all vector values");
#endif
            //At the moment, KMSig only recognises vectors of dimensions of multiples of 64
            int vectorLength = 64 + 64 * index.Keys.Length/64;
            int[] vectorValues = new int[selectedRegulog.Regulons.Length * vectorLength];
            //We need to get all of the values and convert them to byte format
            for (int i = 0; i < selectedRegulog.Regulons.Length; i++)
            {
                for (int j = 0; j < index.Keys.Length; j++)
                {
                    vectorValues[i*vectorLength+j] = (vectors[i][j]);
                }
            }
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Generating list of bytes");
#endif
            List<int> bytes = new List<int>();
            for (int i = 0; i < vectorValues.Length / 8; i++)
            {
                int b = 0;
                for (int j = 0; j < 8; j++)
                {
                    if ((i * 8 * j) < vectorValues.Length)
                    {
                        b = b << 1 | vectorValues[i * 8 * j];
                    }
                    else
                    {
                        b = b << 1;
                    }
                }
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Adding " + b + " to list");
#endif
                bytes.Add(b);
            }

#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Writing dimension of vectors in plaintext");
#endif
            writer.WriteLine(vectorLength.ToString(), null);

#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Writing bytes to file as characters");
#endif
            foreach (int b in bytes) {
                char character = new char();
                Script.Literal("character = String.fromCharCode(b)");
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Writing " + character);
#endif
                writer.WriteChar(character);
            }

#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Saving as " + fileName);
#endif
            HtmlUtil.SaveText(fileName, writer.ToString(), "text/plain");
        }*/

/*#if DEBUG
        /// <summary>
        /// Gets the output of KMeansSig and converts it into a dictionary
        /// </summary>
        private static Dictionary<int,int> ParseBinaryVectorClusters ( string text )
        {
            Dictionary<int, int> clusters = new Dictionary<int,int>();
            StringReader reader = new StringReader(text);
            string line = "";
            do
            {
                line = reader.ReadLine();
                if (line != null)
                {
                    string[] values = line.Split("");
                    if (values.Length == 2) {
                       clusters[int.Parse(values[0])] = int.Parse(values[1]);
                    }
                }
            } while (line != null);
            return clusters;
        }
#endif*/

#if DEBUG
        /// <summary> Returns the contents of a file as a string.
		/// </summary>
		/// <param name="fileName">The path (relative or absolute) of the file.</param>
		/// <param name="process"> A delegate that will be invoked with the content of the file in the event of a successful call, or with null otherwise. </param>
		/// <returns>Returns the content of the file as a string.</returns>

		private static void ReadAllText ( string fileName, Action<string> process ) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.Open( HttpVerb.Get, fileName, false );
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						process( req.ResponseText );
					}
					else {
						if (Constants.ShowDebugMessages) Console.Log( string.Format( "ReadAllText( \"{0}\" )", fileName ) );
						process( null );
					}
				}
			};
            Script.Literal("req.onloadend = function(event) { if (req.status == 404) {console.log('No document retrieved.') } }");
            try {
			    req.Send();
            }
            catch (Exception ex) {
                if (Constants.ShowDebugMessages) Console.Log("req.Send() failed because: " + ex.Message);
            }
		}
#endif
        /// <summary>
        /// Event handler for attempting to contact RegPrecise
        /// <para>Used to allow AppMain to update the status of RegPrecise</para>
        /// </summary>
        public event EventHandler RegPreciseContact;

        /// <summary>
        /// Called when an attempt is made to contact RegPrecise
        /// </summary>
        /// <param name="e">The parameters of the event</param>
        protected virtual void OnRegPreciseContact (EventArgs e) {
            if (RegPreciseContact != null) {
                RegPreciseContact(this, e);
            }
        }

#if DEBUG
        /// <summary>
        /// Event handler for loading a regulog
        /// <para>Used to allow RegPreciseRegulonGraphViewer2 to check if a regulog had to be downloaded from RegPrecise</para>
        /// </summary>
        public event RegulogLoadedEventHandler RegulogLoaded;

        /// <summary>
        /// Called when an a regulog is loaded
        /// </summary>
        /// <param name="e">The parameters of the event</param>
        protected virtual void OnRegulogLoaded (RegulogLoadedEventArgs e) {
            if (RegulogLoaded != null) {
                RegulogLoaded(this, e);
            }
        }
#endif

        /// <summary>
        /// Attempts to retrieve all of the matched GO terms in the local MySQL database for the genes in the current regulog
        /// </summary>
        /// <param name="genes">The list of genes to check</param>
        /// <param name="localMySQL">The local MySQL service</param>
        /// <param name="regulogId">The current regulog ID</param>
        /// <param name="regulog">The current regulog</param>
        private void RetrieveLocalGOTerms_Regulog(GeneInfo[] genes, ILocalMySQLService localMySQL, Number regulogId, RegulogInfo regulog) {
            if (goTermMatches != null) {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("GO term matches loaded before - retriving locally");
#endif
                foreach (GeneInfo gene in genes) {
                    foreach (GORegPreciseMatch match in goTermMatches) {
/*#if DEBUG
                        if (gene.GeneFunction.Trim().ToLowerCase().IndexOf("major facilitator superfamily") == 0 &&
                            match.GeneFunction.ToLowerCase().IndexOf("major facilitator superfamily") == 0) {
                            if (Constants.ShowDebugMessages) Console.Log("Trying to match '" + gene.GeneFunction.Trim().ToLowerCase() + "' with '" + match.GeneFunction.ToLowerCase() + "'");
                        }
#endif*/
                        if (gene.GeneFunction.Trim().ToLowerCase() == match.GeneFunction.Trim().ToLowerCase()) {
                            gene.GoParentTerm = match.ParentTermName;
                            gene.GoTerm = match.TermName;
                            break;
                        }
                    }
                }

				genesByRegulogId[regulogId] = genes;

				foreach ( GeneInfo r in genes ) {
					genesById[r.VimssId] = r;
				}

				regulog.Genes = genes;
            } else {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("GO term matches not loaded before - retriving from SQL");
#endif
                localMySQL.GO_GetRegPreciseCategoryMatches( delegate( ServiceResponse<GORegPreciseMatch[]> goResponse ) {
					if ( goResponse.Error != null ) {
						string e = goResponse.Error;
						if (Constants.ShowDebugMessages) Console.Log( string.Format( "GetRegPreciseCategoryMatches: Server returned error '{0}'", e ) );

                        // If the database connection was refused, set the
                        // local database as down for now
                        if (e.IndexOf("ECONNREFUSED") != -1) {
                            localMySQLUp = false;
                        }

                        // Otherwise, assume the connection is up
                        else {
                            localMySQLUp = true;
                        }
						return;
					}

					if ( goResponse.Content != null ) {

                        goTermMatches = goResponse.Content;

                        foreach (GeneInfo gene in genes) {
                            foreach (GORegPreciseMatch match in goTermMatches) {
/*#if DEBUG
                                if (gene.GeneFunction.Trim().ToLowerCase().IndexOf("major facilitator superfamily") == 0 &&
                                    match.GeneFunction.ToLowerCase().IndexOf("major facilitator superfamily") == 0) {
                                    if (Constants.ShowDebugMessages) Console.Log("Trying to match '" + gene.GeneFunction.Trim().ToLowerCase() + "' with '" + match.GeneFunction.ToLowerCase() + "'");
                                }
#endif*/
                            if (gene.GeneFunction.Trim().ToLowerCase() == match.GeneFunction.Trim().ToLowerCase()) {
                                gene.GoParentTerm = match.ParentTermName;
                                gene.GoTerm = match.TermName;
                                break;
                            }
                        }
                    }
                }

					genesByRegulogId[regulogId] = genes;

					foreach ( GeneInfo r in genes ) {
						genesById[r.VimssId] = r;
					}

					regulog.Genes = genes;
                });
            }
        }

        /// <summary>
        /// Attempts to retrieve all of the matched GO terms in the local MySQL database for the genes in the current regulon
        /// </summary>
        /// <param name="genes">The list of genes to check</param>
        /// <param name="localMySQL">The local MySQL service</param>
        /// <param name="regulonId">The current regulon ID</param>
        /// <param name="regulon">The current regulon</param>
        /// <param name="callback">The action to perform once things are loaded</param>
        private void RetrieveLocalGOTerms_Regulon(GeneInfo[] genes, ILocalMySQLService localMySQL, Number regulonId, RegulonInfo regulon, Action<RegulonInfo> callback) {
            if (goTermMatches != null) {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("GO term matches loaded before - retriving locally");
#endif
                foreach (GeneInfo gene in genes) {
                    foreach (GORegPreciseMatch match in goTermMatches) {
/*#if DEBUG
                        if (gene.GeneFunction.Trim().ToLowerCase().IndexOf("major facilitator superfamily") == 0 &&
                            match.GeneFunction.ToLowerCase().IndexOf("major facilitator superfamily") == 0) {
                            if (Constants.ShowDebugMessages) Console.Log("Trying to match '" + gene.GeneFunction.Trim().ToLowerCase() + "' with '" + match.GeneFunction.ToLowerCase() + "'");
                        }
#endif*/
                        if (gene.GeneFunction.Trim().ToLowerCase() == match.GeneFunction.Trim().ToLowerCase()) {
                            gene.GoParentTerm = match.ParentTermName;
                            gene.GoTerm = match.TermName;
                            break;
                        }
                    }
                }

				genesByRegulonId[regulonId] = genes;
				regulon.Genes = genes;

				foreach ( GeneInfo r in genes ) {
					genesById[r.VimssId] = r;
				}

				callback( regulon );

            } else if (localMySQLUp) {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("GO term matches not loaded before - retriving from SQL");
#endif
                localMySQL.GO_GetRegPreciseCategoryMatches( delegate( ServiceResponse<GORegPreciseMatch[]> goResponse ) {
					if ( goResponse.Error != null ) {
						string e = goResponse.Error;
						if (Constants.ShowDebugMessages) Console.Log( string.Format( "GetRegPreciseCategoryMatches: Server returned error '{0}'", e ) );

                        // If the database connection was refused, set the
                        // local database as down for now
                        if (e.IndexOf("ECONNREFUSED") != -1) {
                            localMySQLUp = false;
                        }

                        // Otherwise, assume the connection is up
                        else {
                            localMySQLUp = true;
                        }
						return;
					}

					if ( goResponse.Content != null ) {

                        goTermMatches = goResponse.Content;

                        foreach (GeneInfo gene in genes) {
                            foreach (GORegPreciseMatch match in goTermMatches) {
/*#if DEBUG
                                if (gene.GeneFunction.Trim().ToLowerCase().IndexOf("major facilitator superfamily") == 0 &&
                                    match.GeneFunction.ToLowerCase().IndexOf("major facilitator superfamily") == 0) {
                                    if (Constants.ShowDebugMessages) Console.Log("Trying to match '" + gene.GeneFunction.Trim().ToLowerCase() + "' with '" + match.GeneFunction.ToLowerCase() + "'");
                                }
#endif*/
                                if (gene.GeneFunction.Trim().ToLowerCase() == match.GeneFunction.Trim().ToLowerCase()) {
                                    gene.GoParentTerm = match.ParentTermName;
                                    gene.GoTerm = match.TermName;
                                    break;
                                }
                            }
                        }

                    }

					genesByRegulonId[regulonId] = genes;
					regulon.Genes = genes;

					foreach ( GeneInfo r in genes ) {
						genesById[r.VimssId] = r;
					}

					callback( regulon );
                });

            // If no GO terms can be retrieved from anywhere,
            // set the genes with the normal method
            } else {
				genesByRegulonId[regulonId] = genes;
				regulon.Genes = genes;

				foreach ( GeneInfo r in genes ) {
					genesById[r.VimssId] = r;
				}

				callback( regulon );
            }
        }

        /// <summary>
        /// Attempts to load additional operon sites from RegPrecise's text files
        /// <para>At the moment this only works for files that are downloaded beforehand and included on the server</para>
        /// </summary>
        /// <param name="regulog">The regulog to load operons for</param>
        private static void LoadOperonsFromText(RegulogInfo regulog)
        {
            try
            {
                ReadAllText("/RegulonExplorer/Text_Files/Regulated_Genes_" + regulog.RegulogId + ".txt", delegate(string text)
                //ReadAllText("http://regprecise.lbl.gov/RegPrecise/ExportServlet?type=gene&regulogId=" + regulog.RegulogId, delegate(string text)
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        if (Constants.ShowDebugMessages) Console.Log("loading operon sites from text file");

                        // We want to store a list of sites for each operon
                        // in each regulon
                        Dictionary<string, List<List<SiteInfo>>> currentRegulogSites = new Dictionary<string, List<List<SiteInfo>>>();
                        StringReader reader = new StringReader(text);
                        string currentLine = reader.ReadLine();
                        while (currentLine != null)
                        {

                            // Find the start of the next genome
                            if (currentLine[0] == '#')
                            {
                                string genomeName = currentLine.Remove(0, 1);
/*#if DEBUG
                                if (Constants.ShowDebugMessages) Console.Log("Getting operons for genome " + genomeName);
#endif*/
                                List<List<SiteInfo>> currentGenomeSites = new List<List<SiteInfo>>();
                                currentLine = reader.ReadLine();
                                List<SiteInfo> currentOperonSites = new List<SiteInfo>();
                                while (currentLine != null)
                                {
/*#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log("currentLine is " + currentLine + " (length " + currentLine.Length + ")");
 #endif*/

                                    // An empty line signifies a new operon
                                    if (currentLine.Length < 2)
                                    {
                                        if (currentOperonSites.Length > 0)
                                        {
                                            currentGenomeSites.Add(currentOperonSites);
                                            currentOperonSites = new List<SiteInfo>();
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log("Starting a new operon...");
#endif*/
                                        }
                                    }
                                    else
                                    {

                                        // We want to start a new genome if we encounter
                                        // a '#'
                                        if (currentLine[0] == '#')
                                        {
                                            break;
                                        }
                                        else
                                        {

                                            // Split each line into its three tokens
                                            string[] tokens = currentLine.Split('\t');
                                            SiteInfo currentSite = new SiteInfo(new RegPrecise.Site());
                                            currentSite.Site.GeneVIMSSId = Number.Parse(tokens[0]);
                                            currentSite.Site.GeneLocusTag = tokens[1];

                                            // Find the coresponding gene for this site
                                            foreach (RegulonInfo regulon in regulog.Regulons)
                                            {
                                                if (regulon.GenomeName == genomeName)
                                                {
                                                    foreach (GeneInfo gene in regulon.Genes)
                                                    {
                                                        if (gene.Name == tokens[2])
                                                        {
                                                            currentSite.Gene = gene;
/*#if DEBUG
                                                            if (Constants.ShowDebugMessages) Console.Log("Added site for " + gene.Name);
#endif*/
                                                            break;
                                                        }
                                                    }
                                                    currentSite.Site.RegulonId = regulon.RegulonId;
                                                    break;
                                                }
                                            }
                                            currentOperonSites.Add(currentSite);
                                        }
                                    }
                                    currentLine = reader.ReadLine();
                                }
                                currentRegulogSites[genomeName] = currentGenomeSites;
/*#if DEBUG
                                if (Constants.ShowDebugMessages) Console.Log("Finished sites for this genome.");
#endif*/
                            }
                        }
                        //Console.Log("Finished sites for this regulog.");

                        // Collect the existing sites into a list
                        List<SiteInfo> allSites = new List<SiteInfo>();
                        foreach (SiteInfo site in regulog.Sites)
                        {
                            allSites.Add(site);
                        }

                        // Go through all the sites we just made and add
                        // missing values
                        foreach (KeyValuePair<string, List<List<SiteInfo>>> currentGenomeSites in currentRegulogSites)
                        {
                            //Console.Log("Adding additional data for new sites in " + currentGenomeSites.Key);

                            foreach (List<SiteInfo> currentOperonSites in currentGenomeSites.Value)
                            {
                                // I believe the first site in each operon is supposed to be
                                // present already. If it is, we'll copy the other values
                                // from it
                                SiteInfo firstNewSite = null;
                                foreach (SiteInfo site in regulog.Sites)
                                {
                                    if (site.GeneLocusTag == currentOperonSites[0].GeneLocusTag &&
                                        site.GeneVimssid == currentOperonSites[0].GeneVimssid &&
                                        site.RegulonId == currentOperonSites[0].RegulonId)
                                    {
/*#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log("Existing site " + site.GeneVimssid + " is the first of this operon.");
#endif*/
                                        firstNewSite = site;
                                        break;
                                    }
                                }

                                // Copy the position, score and sequence, but only do so
                                // if the site doesn't already exist (and there is a first
                                // site)
                                int i;
                                if (firstNewSite != null)
                                {
                                    i = 1;
                                }
                                else
                                {
                                    i = 0;
                                }
                                for (; i < currentOperonSites.Length; i++)
                                {
                                    bool alreadyExists = false;
                                    foreach (SiteInfo site in regulog.Sites)
                                    {
                                        if (site.GeneLocusTag == currentOperonSites[i].GeneLocusTag &&
                                            site.GeneVimssid == currentOperonSites[i].GeneVimssid &&
                                            site.RegulonId == currentOperonSites[i].RegulonId)
                                        {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log("The site " + site.GeneVimssid + " already exists - we don't need to add it again.");
#endif*/
                                            alreadyExists = true;
                                            break;
                                        }
                                    }
                                    if (!alreadyExists)
                                    {
                                        SiteInfo newSite = currentOperonSites[i];
                                        if (firstNewSite != null)
                                        {
                                            newSite.Site.Position = firstNewSite.Position;
                                            newSite.Site.Score = firstNewSite.Score;
                                            newSite.Site.Sequence = firstNewSite.Sequence;
                                        }
/*#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log("Adding site " + newSite.GeneVimssid + " to list.");
#endif*/
                                        allSites.Add(newSite);
                                    }
                                }
                            }
                        }
                        if (Constants.ShowDebugMessages) Console.Log((allSites.Length - regulog.Sites.Length) + " new sites added.");
                        if ((allSites.Length - regulog.Sites.Length) > 0)
                        {
                            regulog.Sites = allSites;
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("Sites added to regulog " + regulog.RegulogId);
#endif*/
                        }
                        reader.Close();

                    }
                    else
                    {
                        if (Constants.ShowDebugMessages) Console.Log("No file retrieved for  " + regulog.RegulatorName);
                    }
                });
            }
            catch (Exception ex)
            {
                if (Constants.ShowDebugMessages) Console.Log("Failed to load operon sites for " + regulog.RegulatorName + " because:" + ex.Message);
            }
        }

        /// <summary>
        /// Stores a list of the currently processing add operations, to prevent the same operation being attempted multiple times at once
        /// </summary>
        private List<string> addsCurrentlyProcessing = new List<string>();

        /// <summary>
        /// Stores a list of the add operations that have been processed this session, to prevent unecessary attempts to do the same operation
        /// </summary>
        private List<string> addsProcessed = new List<string>();

        /// <summary>
        /// Specifies whether to load data from RegPrecise/local MySQL regardless of whether the data is already loaded
        /// <para>true = download regardless of existing data, false = regular behaviour</para>
        /// </summary>
        private bool forceReload = false;

        /// <summary>
        /// Specifies whether to load data from RegPrecise/local MySQL regardless of whether the data is already loaded
        /// <para>true = download regardless of existing data, false = regular behaviour</para>
        /// </summary>
        public bool ForceReload
        {
            get { return forceReload; }
            set { forceReload = value; }
        }
	}

#if DEBUG
    /// <summary>
    /// Event args class for loading a regulog
    /// <para>Used to allow RegPreciseRegulonGraphViewer2 to check if a regulog had to be downloaded from RegPrecise</para>
    /// </summary>
    public class RegulogLoadedEventArgs : EventArgs {

        /// <summary>
        /// Whether the regulog had to be downloaded from RegPrecise
        /// </summary>
        private bool downloadRequired = false;

        /// <summary>
        /// Creates a new set of Regulog Loaded event arguments
        /// </summary>
        /// <param name="downloadRequired">Whether the regulog had to be downloaded from RegPrecise</param>
        public RegulogLoadedEventArgs(bool downloadRequired) {
            this.downloadRequired = downloadRequired;
        }

        /// <summary>
        /// Gets whether the regulog had to be downloaded from RegPrecise
        /// </summary>
        public bool DownloadRequired {
            get { return downloadRequired; }
        }
    }

    /// <summary>
    /// Event handler class for loading a regulog
    /// <para>Used to allow RegPreciseRegulonGraphViewer2 to check if a regulog had to be downloaded from RegPrecise</para>
    /// </summary>
    public delegate void RegulogLoadedEventHandler(object sender, RegulogLoadedEventArgs e);
#endif
}
