﻿using System;
using System.Collections.Generic;

namespace RegulonExplorer.ViewModel {
	public class GenomeCollection {

		/// <summary> Genomes, indexed by Id and partitioned according to source.
		/// </summary>

		private Dictionary<string,Dictionary<Number,GenomeInfo>> genomes = 
			new Dictionary<string, Dictionary<Number, GenomeInfo>>();

		/// <summary> Tries to add a genome to the collection. If a record matching on
		///		source Id and genome Id, returns false and no changes occur. If genome does not 
		///		already exist, adds the genome to the collection and returns true.
		/// </summary>
		/// <param name="genomeInfo">
		///		The genome data.
		/// </param>
		/// <returns>
		///		True iff the genome did not already exist in the collection.
		/// </returns>

		public bool Add ( GenomeInfo genomeInfo ) {
			string sourceId = genomeInfo.SourceId;

			if ( !genomes.ContainsKey( sourceId ) ) {
				genomes[sourceId] = new Dictionary<Number, GenomeInfo>();
			}

			Dictionary<Number,GenomeInfo> table = genomes[sourceId];
			int genomeId = genomeInfo.GenomeId;

			if ( table.ContainsKey( genomeId ) ) {
				return false;
			}
			else {
				table[genomeId] = genomeInfo;
				return true;
			}
		}

		/// <summary> Tries to add a genome to the collection. If a record matching on
		///		source Id and genome Id, returns false and no changes occur. If genome does not 
		///		already exist, adds the genome to the collection and returns true.
		/// </summary>
		/// <param name="genomeInfo">
		///		The genome data.
		/// </param>
		/// <returns>
		///		True iff the genome did not already exist in the collection.
		/// </returns>

		public void AddOrReplace ( GenomeInfo genomeInfo ) {
			string sourceId = genomeInfo.SourceId;

			if ( !genomes.ContainsKey( sourceId ) ) {
				genomes[sourceId] = new Dictionary<Number, GenomeInfo>();
			}

			Dictionary<Number,GenomeInfo> table = genomes[sourceId];
			int genomeId = genomeInfo.GenomeId;

			table[genomeId] = genomeInfo;
		}

		/// <summary> Traverses the collection and performs the required action on each element.
		/// </summary>
		/// <param name="action"></param>

		public void ForEach ( Action<GenomeInfo> action ) {
			foreach ( KeyValuePair<string,Dictionary<Number,GenomeInfo>> kvp in genomes ) { 
				foreach ( KeyValuePair<Number, GenomeInfo> kvp2 in kvp.Value ) {
					action( kvp2.Value );
				}
			}
		}

		/// <summary> Traverses the collection and performs the required action on each element.
		/// </summary>
		/// <param name="action"></param>

		public void ForEachWhile ( Func<GenomeInfo,bool> action ) {
			foreach ( KeyValuePair<string,Dictionary<Number,GenomeInfo>> kvp in genomes ) { 
				foreach ( KeyValuePair<Number, GenomeInfo> kvp2 in kvp.Value ) {
					if ( !action( kvp2.Value ) ) {
						return;
					}
				}
			}
		}

		/// <summary> Searches for the requested GenomeInfo ion the collection.
		/// </summary>
		/// <param name="sourceId"></param>
		/// <param name="genomeId"></param>
		/// <returns></returns>

		public GenomeInfo Get ( string sourceId, int genomeId ) {
			if ( !genomes.ContainsKey( sourceId ) ) {
				return null;
			}

			Dictionary<Number,GenomeInfo> table = genomes[sourceId];

			return table[genomeId];
		}

		/// <summary> Gets the genomes in a specified collection.
		/// </summary>
		/// <param name="collectionId"></param>
		/// <returns></returns>

		internal GenomeInfo[] GetGenomes ( string collectionId ) {
			List<GenomeInfo> result = new List<GenomeInfo>();

			Dictionary<Number,GenomeInfo> table = genomes[collectionId];

			foreach ( KeyValuePair<Number,GenomeInfo> kvp in table ) {
				result.Add( kvp.Value );
			}

			return (GenomeInfo []) result;
		}
	}
}