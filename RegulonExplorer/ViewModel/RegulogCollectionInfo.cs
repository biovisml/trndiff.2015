// RegulonCollectionSummaryItem.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using RegPrecise;
using SystemQut.ComponentModel;
using SystemQut.ServiceModel;

namespace RegulonExplorer.ViewModel {

	public class RegulogCollectionInfo: Selectable {

		private RegulogCollection regulogCollection;
		private RegulogInfo[] regulogs;

		public RegulogCollectionInfo (
			RegulogCollection regulogCollection
		) {
			this.regulogCollection = regulogCollection;

            /* if (regulogs == null) {
                Script.SetTimeout((Action) delegate {
                    if (CollectionId != null && CollectionType != null) {
                        DataLayer.Instance.RequestRegulogsInCollection( CollectionType, CollectionId, (Action<RegulogInfo[]>)delegate( RegulogInfo[] newRegulogs ) {
				            this.Regulogs = newRegulogs;
			            });
                    }
                }, 10, null);
            } */ 
		}

		public string ClassName { get { return regulogCollection.ClassName; } }

		public Number CollectionId { get { return regulogCollection.CollectionId; } }

		public RegulogCollectionType CollectionType { get { return regulogCollection.CollectionType; } }

		public String Name { get { return regulogCollection.Name; } }

        /*public int NumRegulogs { get { if (regulogs == null) { return -1; } else { return regulogs.Length; } } }*/

		/// <summary> Get or set the list of regulators 
		/// </summary>

		public RegulogInfo [] Regulogs {
			get {
				return this.regulogs;
			}
			set {
				if ( value == this.regulogs ) return;

				this.regulogs = value;

				NotifyPropertyChanged( "Regulogs" );
			}
		}
	}
}
