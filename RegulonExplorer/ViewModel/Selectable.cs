// Selectable.cs 
//

using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;

namespace RegulonExplorer.ViewModel {

	/// <summary> Common subclass for selectable objects.
	/// </summary>

	public class Selectable :ISelectable, INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;
		private  bool isSelected;

		/// <summary> Fire PropertyChanged events.
		/// </summary>

		public void NotifyPropertyChanged ( string propertyName ) {
			if ( PropertyChanged != null ) {
#if DEBUG
                //Console.Log("Sending a notification that " + propertyName + " has changed.");
#endif
				PropertyChanged( this, new PropertyChangedEventArgs( propertyName ) );
			} else {
#if DEBUG
                //Console.Log("Not sending a notification that " + propertyName + " has changed because PropertyChanged is null.");
#endif
            }
		}

		/// <summary> Get or set the isSelected value. Setting this fires PropertyChanged.
		/// </summary>
		public virtual bool IsSelected {
			get {
				return this.isSelected;
			}
			set {
				if ( value == isSelected ) return;

				isSelected = value;
#if DEBUG
                //Console.Log("Attempting to fire NotifyPropertyChanged for a Selectable.");
#endif
				NotifyPropertyChanged( "IsSelected" );
			}
		}
	}
}
