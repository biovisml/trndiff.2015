﻿using System;
using System.Collections.Generic;
using RegPrecise;
using SystemQut;
using RegulonExplorer.Common;

namespace RegulonExplorer.ViewModel {
	public class RegulatorInfo : Selectable, IGeneInfo, IEquatable<IGeneInfo> {
		/// <summary> The RegPrecise Regulator record associated with this object.
		/// </summary>

		private Regulator regulator;

		/// <summary> The RegPrecise Gene associated with this object.
		/// <para>
		///		So far, it seems that genes are sometimes also regulators, but it is not
		///		obvious that all regulators are genes. And so, I maintian parallel records
		///		and source data from the peer as required.
		/// </para>
		/// </summary>

		private GeneInfo gene = null;

		/// <summary> Initialise the current object, storing the immutable Regulator reference.
		/// </summary>
		/// <param name="regulator"></param>

		public RegulatorInfo ( Regulator regulator ) {
			this.regulator = regulator;
		}

		public string LocusTag { get { return regulator.LocusTag; } }
		public string Name { get { return regulator.Name; } }
		public string RegulatorFamily { get { return regulator.RegulatorFamily; } }
		public int RegulonId { get { return regulator.RegulonId; } }
		public int VimssId { get { return regulator.VimssId; } }
        //public bool Selected { get {return gene.Selected; } set {gene.Selected = value; }}

		/// <summary> Gets the function of this object. If it is associated with a Gene, then that Gene's
		///		function is used. Otherwise, a text of the form ( "Regulator in the {0} family", RegulatorFamily )
		///		is returned.
		/// </summary>

		public string GeneFunction {
			get { return gene != null ? gene.GeneFunction : string.Format( "Regulator in the {0} family", RegulatorFamily ); }
		}

		/// <summary> Get or set the Gene associated with this object.
		/// <para>
		///		Setting this fires PropertyChanged for Gene, GeneFunction properties.
		/// </para>
		/// <para>
		///		So far, it seems that genes are sometimes also regulators, but it is not
		///		obvious that all regulators are genes. And so, I maintian parallel records
		///		and source data from the peer as required.
		/// </para>
		/// </summary>

		public GeneInfo Gene {
			get {
				return gene;
			}
			set {
				if ( Objects.AreEqual(gene, value ) ) return;
				if ( ((bool) (object) value) && value.VimssId != regulator.VimssId ) throw new Exception( Constants.GENE_AND_REGULATOR_ID_MUST_MATCH );
				gene = value;
				NotifyPropertyChanged("Gene,GeneFunction");
			}
		}

		/// <summary> Determines if the two geneInfo objects have the same VimssId.
		/// </summary>
		/// <param name="other">
		///		A IGeneInfo object to be compared.
		/// </param>
		/// <returns>
		///		Returns true iff the two geneInfo objects have the same VimssId.
		/// </returns>

		public bool Equals ( IGeneInfo other ) {
			return ( (bool) (object) other) && other.VimssId == VimssId;
		}

		/// <summary> Get the RegPrecise Regulator object wrapped by this object.
		/// </summary>

		public Regulator Regulator {
			get { return regulator; }
		}

		/// <summary> Wrapper for Regulator.Parse.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public static RegulatorInfo Parse ( JSObject obj ) {
			return new RegulatorInfo( new Regulator( obj ) );
		}

        /// <summary>
        /// The number of genes that this regulator affects
        /// <para>Used for regulator tooltips and the centre number of wedge wheels</para>
        /// </summary>
        private int numberOfTargetGenes = -1;

        /// <summary>
        /// Gets or sets the number of target genes that this regulator affects
        /// </summary>
        public int NumberOfTargetGenes
        {
            get { return numberOfTargetGenes; }
            set { numberOfTargetGenes = value; }
        }

        /// <summary>
        /// Gets the annotation for this gene
        /// <para>Taken from GeneInfo</para>
        /// <para>
        /// This is an optional data field for a gene, which can be used in
        /// TRNDiff CSVfiles to include additional information about the gene
        /// </para>
        /// </summary>

		public string Annotation {
			get {
                if ( gene != null ) {
				    return (string) gene.Annotation;
                }
                if ( regulator["Annotation"] != null ) {
                    return (string)regulator["Annotation"];
                }
                return null;
			}
		}

        /// <summary>
        /// Gets or sets the parent GO term for this object
        /// <para>Taken from GeneInfo</para>
        /// <para>
        /// This is usually a high-level GO term rather than something more
        /// specific, and it is used for colouring nodes in displays with GO
        /// terms enabled
        /// </para>
        /// </summary>
        public string GoParentTerm {
			get {
                if ( gene != null ) {
				    return (string) gene.GoParentTerm;
                }
                if ( regulator["goParentTerm"] != null ) {
                    return (string)regulator["goParentTerm"];
                }
                return null;
			}
        }

        /// <summary>
        /// Gets or sets the GO term for this object
        /// <para>Taken from GeneInfo</para>
        /// </summary>
        public string GoTerm {
			get {
                if ( gene != null ) {
				    return (string) gene.GoTerm;
                }
                if ( regulator["goTerm"] != null ) {
                    return (string)regulator["goTerm"];
                }
                return null;
			}
        }

	}
}
