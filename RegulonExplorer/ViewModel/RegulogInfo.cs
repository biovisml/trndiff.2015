﻿using System;
using System.Collections.Generic;

using Enum = SystemQut.Enum;
using SystemQut;
using SystemQut.ComponentModel;
using RegPrecise;
using System.Html;
using SystemQut.ServiceModel;

namespace RegulonExplorer.ViewModel {

	/// <summary> A Regulog records information about a regulon -- i.e. a regulon of conserved regulators.
	/// </summary>

	public class RegulogInfo : Selectable {
		private Regulog regulog;
		private RegulonInfo [] regulons = null;
		private GeneInfo[] genes = null;
		private RegulatorInfo[] regulators = null;
		private SiteInfo[] sites = null;

		public RegulogInfo (
			Regulog regulog
		) {
			this.regulog = regulog;
		}

		public string Effector {
			get {
				return regulog.Effector;
			}
		}
		public string RegulationType {
			get {
				return regulog.RegulationType.ToString();
			}
		}
		public string RegulatorFamily {
			get {
				return regulog.RegulatorFamily;
			}
		}
		public string RegulatorName {
			get {
				return regulog.RegulatorName;
			}
		}
		public Number RegulogId {
			get {
				return regulog.RegulogId;
			}
		}
		public string TaxonName {
			get {
				return regulog.TaxonName;
			}
		}
		public string Pathway {
			get {
				return regulog.Pathway;
			}
		}

		public Regulog Regulog {
			get {
				return regulog;
			}
		}

        //public Number NumGroups { get {return regulog.NumGroups; } set {regulog.NumGroups = value; }}
        public Number NumGroups { get {return (Number)regulog["numGroups"]; } set {regulog["numGroups"] = value; }}

		/// <summary> Get or set the collection of regulons in this regulog.
		/// <para>
		///		When this is set, if the genes and/or regulators are available,
		///		then each gene and/or regulator is attached to its respective regulon.
		/// </para>
		/// <para>
		///		PropertyChanged fires after that.
		/// </para>
		/// </summary>

		public RegulonInfo[] Regulons {
			get {
				return regulons;
			}
			set {
				regulons = value;

				if ( genes != null ) UpdateRegulonGenes();
				if ( regulators != null ) UpdateGeneRegulators();

				NotifyPropertyChanged( "Regulons" );
			}
		}

		/// <summary> Get or set the collection of genes in this regulog.
		/// <para>
		///		When this is set, if the regulons are not null,
		///		then each gene is attached to its respective regulon.
		///		Similarly w.r.t. SiteInfo.
		/// </para>
		/// <para>
		///		PropertyChanged fires after that.
		/// </para>
		/// </summary>

		public GeneInfo[] Genes {
			get {
				return genes;
			}
			set {
				genes = value;

				genes.Sort( delegate( GeneInfo x, GeneInfo y ) { return string.Compare( x.LocusTag, y.LocusTag ); } );

				if ( regulons != null ) UpdateRegulonGenes();
				if ( regulators != null ) UpdateGeneRegulators();
				if ( sites != null ) UpdateSites();

				NotifyPropertyChanged( "Genes" );
			}
		}

		/// <summary> Get or set the collection of regulators in this regulog.
		/// <para>
		///		When this is set, if the regulons are not null,
		///		then each regulator is attached to its respective regulon.
		///		Similarly w.r.t. genes.
		/// </para>
		/// <para>
		///		PropertyChanged fires after that.
		/// </para>
		/// </summary>

		public RegulatorInfo[] Regulators {
			get {
				return regulators;
			}
			set {
				regulators = value;

				regulators.Sort( delegate( RegulatorInfo x, RegulatorInfo y ) { return string.Compare( x.LocusTag, y.LocusTag ); } );

				if ( regulons != null ) UpdateRegulonRegulators();
				if ( genes != null ) UpdateGeneRegulators();

				NotifyPropertyChanged( "Regulators" );
			}
		}

		/// <summary> Get the collection of genomes in this regulog.
		/// </summary>

		public GenomeInfo [] Genomes {
			get {
				List<GenomeInfo> result = new List<GenomeInfo>();

				foreach ( RegulonInfo regulon in regulons ) {
					result.Add( regulon.Genome );
				}

				return (GenomeInfo []) result;
			}
		}

		/// <summary> Attaches genes to their associated regulator records, if they share a common VimssId.
		/// </summary>

		private void UpdateGeneRegulators () {
			foreach ( GeneInfo gene in genes ) {
				foreach ( RegulatorInfo regulator in regulators ) {
					if ( gene.VimssId == regulator.VimssId ) {
						gene.Regulator = regulator;
						regulator.Gene = gene;
					}
				}
			}
		}

		/// <summary> Attaches genes to each regulon.
		/// </summary>

		private void UpdateRegulonGenes () {
			foreach ( RegulonInfo regulon in regulons ) {
				regulon.Genes = genes.Filter( delegate( GeneInfo gene ) {
					return gene.RegulonId == regulon.RegulonId;
				} );
			}
		}

		/// <summary> Attaches regulators to each regulon.
		/// </summary>

		private void UpdateRegulonRegulators () {
			foreach ( RegulonInfo regulon in regulons ) {
				regulon.Regulators = regulators.Filter( delegate( RegulatorInfo regulator ) {
					return regulator.RegulonId == regulon.RegulonId;
				} );
			}
		}

		/// <summary> Gets a (newly created) list containing the distinct gene functions represented in in the
		///		regulog. The result is returned in case-insensitive alphanumeric order.
		/// </summary>

		public string[] GeneFunctions {
			get {
				Dictionary<string, bool> table = new Dictionary<string, bool>();

				foreach ( GeneInfo gene in genes ) {
					table[gene.GeneFunction] = true;
				}

				List<string> functions = new List<string>();

				foreach ( string s in table.Keys ) {
					functions.Add( s.ToLowerCase() );
				}

				functions.Sort( delegate( string x, string y ) {
					return String.Compare( x, y, true );
				} );

				return functions;
			}
		}

		/// <summary> Gets the list of genes that are not transcription factors in the current regulog.
		/// </summary>

		public GeneInfo[] TargetGenes {
			get {
				return genes.Filter( delegate( GeneInfo gene ) { return !gene.IsTranscriptionFactor; } );
			}
		}

		/// <summary> Get or set the collection of sites in this regulog.
		/// <para>
		///		When this is set, if the regulons are not null,
		///		then each regulator is attached to its respective regulon.
		///		Similarly w.r.t. genes.
		/// </para>
		/// <para>
		///		PropertyChanged fires after that.
		/// </para>
		/// </summary>

		public SiteInfo[] Sites {
			get { return sites; }
			set {
				sites = value;

				sites.Sort( delegate( SiteInfo x, SiteInfo y ) {
					return string.Compare( x.GeneLocusTag, y.GeneLocusTag );
				} );

				if ( genes != null ) UpdateSites();

				NotifyPropertyChanged( "Sites" );
			}
		}

		/// <summary> Sets up a bidirectional link between genes and sites.
		/// </summary>

		private void UpdateSites () {
			int geneIndex = 0;
			int siteIndex = 0;

			// Perform an ordered merge operation on genes and sites.
			while ( geneIndex < genes.Length && siteIndex < sites.Length ) {
				GeneInfo currentGene = genes[geneIndex];
				SiteInfo currentSite = sites[siteIndex];

				int comparison = string.Compare( currentGene.LocusTag, currentSite.GeneLocusTag );

				if ( comparison < 0 ) {
					geneIndex++;
				}
				else if ( comparison > 0 ) {
					siteIndex++;
				}
				else {
					currentGene.Site = currentSite;
					currentSite.Gene = currentGene;
					geneIndex++;
					siteIndex++;
				}
			}
		}

		/// <summary> Wrapper for Regulog.Parse.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>

		public static RegulogInfo Parse ( JSObject obj ) {
			return new RegulogInfo( new Regulog( obj ) );
		}

/*#if DEBUG
        /// <summary>
        /// Ensures that all sub-parts of the regulog are connected up properly
        /// </summary>
        public void UpdateReferences() {
            UpdateGeneRegulators();
            UpdateRegulonGenes();
            UpdateRegulonRegulators();
            UpdateSites();
        }
#endif*/
	}
}
