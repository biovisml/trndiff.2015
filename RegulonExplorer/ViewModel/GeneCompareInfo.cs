﻿using System;
using System.Collections.Generic;
using RegPrecise;
using SystemQut;
using RegulonExplorer.Common;

namespace RegulonExplorer.ViewModel
{
    /// <summary>
    /// Used to represent in which networks this gene or its regulation appears in
    /// <list type="bullet">
    /// <item>none (0) - appears in none of the networks (used as a temporary value)</item>
    /// <item>left (1) - appears in the left/first network only, or in the reference network only in a comparison of more than two networks</item>
    /// <item>right (2) - appears in the right/second network only, or in non-reference networks only in a comparison of more than two networks</item>
    /// <item>all (3) - appears in both networks, or all of the networks in a comparison of more than two networks</item>
    /// <item>some (4) - appears in the reference network and at least one non-reference network; only used in comparisons of more than two networks</item>
    /// </list>
    /// </summary>
    public enum PresentInStates { none = 0, left = 1, right = 2, all = 3, some = 4 };

    /// <summary>
    /// A special extension of the regular GeneInfo class, that is used when generating a comparison between two or more networks
    /// <para>Its primary purpose is to make it easier to colour nodes and spokes depending on which networks they occur in</para>
    /// </summary>
    public class GeneCompareInfo : GeneInfo
    {
        private PresentInStates genePresentIn;
        private PresentInStates regulationPresentIn;
        private LogicalOperations currentOperation;

        /// <summary>
        /// The network(s) that this gene is in
        /// </summary>
        public PresentInStates GenePresentIn
        {
            get { return genePresentIn; }
            set { genePresentIn = value; }
        }

        /// <summary>
        /// The network(s) that this gene's regulation is in
        /// </summary>
        public PresentInStates RegulationPresentIn
        {
            get { return regulationPresentIn; }
            set { regulationPresentIn = value; }
        }

        /// <summary>
        /// The logical operation that this gene is being used in
        /// </summary>
        public LogicalOperations CurrentOperation
        {
            get { return currentOperation; }
            set { currentOperation = value; }
        }

        public GeneCompareInfo(Gene gene) : base(gene) { }
    }
}