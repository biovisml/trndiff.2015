﻿using System;
using System.Collections.Generic;
using RegPrecise;

namespace RegulonExplorer.ViewModel {
	public interface IGeneInfo {
		int RegulonId { get; }
		string Name { get; }
		string LocusTag { get; }
		int VimssId { get; }
        string GeneFunction { get; }
        //bool Selected { get; set; }
	}
}
