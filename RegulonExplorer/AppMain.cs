// AppMain.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Controls;
using SystemQut;
using RegulonExplorer.View;
using RegulonExplorer.View.RegPreciseView;
using SystemQut.ComponentModel;
using RegulonExplorer.ViewModel;
using RegulonExplorer.View.TrnDiff;

// For checking if RegPrecise is up
using RegPrecise;
using SystemQut.ServiceModel;
using RegulonExplorer.Common;

// For the build date
using System.Net;

namespace RegulonExplorer {

	static class Main {
		public static void Run () {
			Window.Onerror = delegate( string message, string url, int line ) {
				Window.Alert( String.Format( "Error - {0}(1}: {2}", url, line, message ) );
				return false;
			};
			AppMain.Init();
            //Script.Literal("window.onload = AppMain.Init");
		}
	}

	/// <summary>
	///
	/// </summary>

	public class AppMain : CodeBehind {

        /// <summary>
        /// Initialises the elements in the main window when the application starts
        /// </summary>
		static public void Init () {
			try {

                // Attempt to bind controls
				RegisterControls();
				CodeBehind.BindControls( Document.Body );
				BindToolButtonsToTargets( Document.Body );
			}

            // Show an error message if something happens during binding
			catch ( Exception e ) {
				//Window.Alert( string.Format( "Init: error while trying to bind Document.Body - {0}: {1}", e.Message, e.StackTrace ) );
                Window.Alert ( string.Format( Constants.Text_MessageErrorInit, e.Message, e.StackTrace ) );
			}

            // Load in the build date from the text file and insert it into the
            // header
            XmlHttpRequest req = new XmlHttpRequest();
			req.Open( HttpVerb.Get, "/RegulonExplorer/build_date.txt", false );
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {

                    // If the status of the request was "OK", check if the file
                    // was retrieved
					if ( req.Status == 200 ) {

                        // If the contents of the text file were received
						if ( req.ResponseText != null ) {

                            // Create the text string, load in the correct
                            // element and then insert the text into it
                            string dateText = Constants.Text_HeaderBuildDate  + req.ResponseText;
                            Element creationDate = Document.GetElementById("creationDate");
                            creationDate.InnerHTML = dateText;
#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("Text from build_date.txt (" + req.ResponseText + ") added to header");
#endif

                        // Otherwise don't put anything as the build date
                        } else {
#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("No text in build_date.txt");
#endif
                        }
					}
                    // Otherwise don't put anything as the build date
					else {
#if DEBUG
						if (Constants.ShowDebugMessages) Console.Log("Failed to retrieve build_date.txt");
#endif
					}
				}
			};

            // The actual attempt to retrieve the text file
            try {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Attempting to retrieve build_date.txt");
#endif
			    req.Send();
            }

            // Dump the exception to the console rather than throw an exception
            // message to the user
            catch (Exception ex) {
                if (Constants.ShowDebugMessages) Console.Log("req.Send() failed because: " + ex.Message);
            }

            // Add a listener to the data layer to check when something is
            // loaded in it
            DataLayer.Instance.RegPreciseContact += DataLayer_RegPreciseContact;

            // Check and update RegPrecise status
            CheckRegPreciseStatus();
		}

		/// <summary> Factory methods for custom ICodeBehind implementations
		///		defined in the current assembly.
		///	<para>
		///		This will be improved some day, however the present ScriptSharp code
		///		generator does not guarantee that static initialisers are called in
		///		the right order, and SS also provides only limited reflection capabilities.
		///		So it must be done explicitly.
		/// </para>
		/// </summary>

		private static void RegisterControls () {
			Type[] codeBehindTypes = {
				typeof(Activated) ,
				typeof(AppContent) ,
				typeof(AppContentHolder) ,
				typeof(AppMain) ,
				typeof(RegPreciseCollectionChooser) ,
				typeof(RegPreciseGenomeChooser) ,
				typeof(RegPreciseRegulogChooser) ,
				typeof(RegPreciseRegulonChooser) ,
				typeof(RegPreciseRegulogPanel) ,
				typeof(RegPreciseGenomePanel) ,
				typeof(ToolBar) ,
				typeof(ToolButton) ,
				typeof(RegPreciseRegulonGraphViewer),
				typeof(RegPreciseRegulonGraphViewer2),
				typeof(LocalRegulonGraphViewer),
				typeof(LocalRegulonGraphViewerSideBySide),
				typeof(RegPreciseRegulonGraphViewerSideBySide),
				typeof(RegPreciseRegulonGraphViewer2SideBySide)
			};

			foreach ( Type t in codeBehindTypes ) {
				CodeBehind.RegisterCodeBehindType( t );
			}
		}

		public AppMain ( Element domElement )
			: base( domElement ) {
		}

		/// <summary> Gets the codebehind bound to the DOM element with the specified ID.
		/// </summary>
		/// <param name="name"> The id of the element to locate. </param>
		/// <returns>
		///		The code-behind of the requested codeBehind, or null if there is no such element, or
		///		undefined if there is an element but the codebehind is not defined.
		/// </returns>

		internal static IActivated GetIdentified ( string name ) {
			Control domElement = (Control) Document.GetElementById( name );

			return Script.IsValue( domElement ) ? (IActivated) domElement.CodeBehind : null;
		}

		/// <summary> Gets the CSS class name used to identify the AppContainer.
		/// </summary>

		public static string APP_CONTAINER_CSS_CLASS {
			get {
				return "AppContainer";
			}
		}

		/// <summary>
        /// Looks through a given element to find all of the tool button type elements and bind them to their target elements
		/// </summary>
		/// <param name="element">The element to search through</param>

		private static void BindToolButtonsToTargets ( Element element ) {

            // Look through all of the tool buttons in the element
			CodeBehind.ForallControlsOfType( typeof( ToolButton ), element, delegate( ToolButton toolButton ) {

                // Get the target element that this tool button activates
				IActivated activated = GetIdentified( toolButton.ActivatedId );

                // Throw an assertion if the target element does not exist
				Assert.IsTrue( Script.IsValue( activated ), string.Format( "Activated control {0} does not exist", toolButton.ActivatedId ) );

                // Store the target element in the tool button
				toolButton.ActivatedControl = activated;

                // Store the tool button in the target element
				activated.NominalActivator = toolButton;

#if DEBUG
                // Stores the four main header buttons so they can be virtually
                // pressed
                if (toolButton.ActivatedId == "RegPreciseGenomes") {
                    mainButtons[1] = toolButton;
                }
                if (toolButton.ActivatedId == "RegPreciseRegulogs") {
                    mainButtons[2] = toolButton;
                }
                if (toolButton.ActivatedId == "LocalRegulogs") {
                    mainButtons[3] = toolButton;
                }
                if (toolButton.ActivatedId == "HomeContent") {
                    mainButtons[0] = toolButton;
                }
#endif
			} );

#if DEBUG
            // Preparing listeners for the four options in the new drop down
            // menu in the header
			homeSelect = (SelectElement) Document.GetElementById("Home_Select");
            homeSelect.AddEventListener(MouseEventType.click, pageSelectClicked_Home, false);

			regPreciseGenomesSelect = (SelectElement) Document.GetElementById("RegPreciseGenomes_Select");
            regPreciseGenomesSelect.AddEventListener(MouseEventType.click, pageSelectClicked_Genomes, false);

			regPreciseRegulogsSelect = (SelectElement) Document.GetElementById("RegPreciseRegulogs_Select");
            regPreciseRegulogsSelect.AddEventListener(MouseEventType.click, pageSelectClicked_Regulogs, false);

			localRegulogsSelect = (SelectElement) Document.GetElementById("LocalRegulogs_Select");
            localRegulogsSelect.AddEventListener(MouseEventType.click, pageSelectClicked_Local, false);

            // For detecing whether the user has clicked on the page and hence
            // the drop down menu should not be checked
            Document.Body.AddEventListener(MouseEventType.click, pageClicked, false);

            // Make the drop down menu active
            homeSelect.Disabled = false;
            regPreciseGenomesSelect.Disabled = false;
            regPreciseRegulogsSelect.Disabled = false;
            localRegulogsSelect.Disabled = false;
#endif
		}

#if DEBUG

        // These seperate listeners are used so we know which one the click
        // came from

        /// <summary>
        /// Listener for clicking on the "home" option (at this time it appears
        /// as a dummy "(Select data source...)")
        /// </summary>
        /// <param name="e"></param>
        private static void pageSelectClicked_Home(ElementEvent e)
        {
            pageSelectClicked(homeSelect);
        }

        /// <summary>
        /// Listener for clicking on the "RegPrecise Genomes" option
        /// </summary>
        /// <param name="e"></param>
        private static void pageSelectClicked_Genomes(ElementEvent e)
        {
            pageSelectClicked(regPreciseGenomesSelect);
        }


        /// <summary>
        /// Listener for clicking on the "RegPrecise Regulogs" option
        /// </summary>
        /// <param name="e"></param>
        private static void pageSelectClicked_Regulogs(ElementEvent e)
        {
            pageSelectClicked(regPreciseRegulogsSelect);
        }


        /// <summary>
        /// Listener for clicking on the "Local file" option
        /// </summary>
        /// <param name="e"></param>
        private static void pageSelectClicked_Local(ElementEvent e)
        {
            pageSelectClicked(localRegulogsSelect);
        }

        // The four elements that are used in the new drop down
        private static SelectElement homeSelect;
        private static SelectElement regPreciseGenomesSelect;
        private static SelectElement regPreciseRegulogsSelect;
        private static SelectElement localRegulogsSelect;

        // List of the main tool buttons, so that they can be virtually pressed
        private static List<ToolButton> mainButtons = new List<ToolButton>();

        // To keep track of whether the drop down menu is open
        private static bool open = false;

        /// <summary>
        /// Controls the actions of clicking on an option in the drop down list
        /// based on which option was picked
        /// </summary>
        /// <param name="select">The select element that was the source of the
        /// event</param>
        private static void pageSelectClicked(SelectElement select)
        {
            // If the open variable is true therefore the drop down is assumed
            // to be open...
 	        if (open) {
                int index = select.SelectedIndex;
                if (index != 0) {

                    // Virtually click the menu button in order to switch to it
                    mainButtons[index].ButtonClicked(null);
                }

                // Set each of the seperate drop downs to show the entry that
                // corresponds to its parent page
                homeSelect.SelectedIndex = 0;
                regPreciseGenomesSelect.SelectedIndex = 1;
                regPreciseRegulogsSelect.SelectedIndex = 2;
                localRegulogsSelect.SelectedIndex = 3;
                open = false;

            // If the user has only just opened the drop down, set the open
            // variable to true
            } else {
                open = true;
            }
        }

        /// <summary>
        /// If the user clicks anywhere but the drop down lists, set the open
        /// variable to false
        /// </summary>
        /// <param name="e"></param>
        private static void pageClicked(ElementEvent e)
        {
            if (e.Target != homeSelect && e.Target != regPreciseGenomesSelect && e.Target != regPreciseRegulogsSelect && e.Target != localRegulogsSelect) {
                open = false;
            }
        }
#endif

        /// <summary>
        /// Checks whether the RegPrecise database is accessible through
        /// the webservice, and displays that to the user
        /// </summary>
        private static void CheckRegPreciseStatus()
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Checking RegPrecise web service status...");
#endif
            // Create a new RegPrecise service
            IRegPreciseService svc = new RegPreciseService(Constants.REG_PRECISE_SERVICE_URL, false);

            // Test the service by trying to retrieve the regulog with ID 1112
            // (Fur - Enterobacteriales)
            svc.GetRegulog(1112, delegate(ServiceResponse<Regulog> response)
            {
                // Get the element that shows the current status
                Element status = Document.GetElementById("regPreciseStatus");

                // status2 added for Xin-Yi's new intro page
                Element status2 = Document.GetElementById("regPreciseStatus2");

                // Show "checking..." as the status and colour it gray
                status.InnerHTML = Constants.Text_HeaderRegPreciseUnknown;
                status.ClassName = Constants.Text_CssClassRegPreciseUnknown;

                // status2 added for Xin-Yi's new intro page
                status2.InnerHTML = Constants.Text_HeaderRegPreciseUnknown;
                status2.ClassName = Constants.Text_CssClassRegPreciseUnknown;

                Script.SetTimeout( (Action) delegate {

                // If the response was an error...
                if (response.Error != null)
                {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Received an error response! RegPrecise web service is down or otherwise inaccessible.");
#endif
                    // Show "down" as the status and colour it dark red
                    status.InnerHTML = Constants.Text_HeaderRegPreciseDown;
                    status.ClassName = Constants.Text_CssClassRegPreciseDown;

                    // status2 added for Xin-Yi's new intro page
                    status2.InnerHTML = Constants.Text_HeaderRegPreciseDown;
                    status2.ClassName = Constants.Text_CssClassRegPreciseDown;
                }

                // Otherwise, assume that the response shows it is up
                else
                {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("RegPrecise web service is up and accessible.");
#endif
                    // Show "up" as the status and colour it dark green
                    status.InnerHTML = Constants.Text_HeaderRegPreciseUp;
                    status.ClassName = Constants.Text_CssClassRegPreciseUp;

                    // status2 added for Xin-Yi's new intro page
                    status2.InnerHTML = Constants.Text_HeaderRegPreciseUp;
                    status2.ClassName = Constants.Text_CssClassRegPreciseUp;
                }

                }, 10);
            });

            /*// Get the element that shows the current status
            Element status = Document.GetElementById("regPreciseStatus");

            // status2 added for Xin-Yi's new intro page
            Element status2 = Document.GetElementById("regPreciseStatus2");

            // Show "down" as the status and colour it dark red
            status.InnerHTML = Constants.Text_HeaderRegPreciseDown;
            status.ClassName = Constants.Text_CssClassRegPreciseDown;

            // status2 added for Xin-Yi's new intro page
            status2.InnerHTML = Constants.Text_HeaderRegPreciseDown;
            status2.ClassName = Constants.Text_CssClassRegPreciseDown;

            DataLayer.Instance.GetRegulog(568, delegate {
                if (Constants.ShowDebugMessages) Console.Log("RegPrecise web service is up and accessible.");
                // Show "up" as the status and colour it dark green
                status.InnerHTML = Constants.Text_HeaderRegPreciseUp;
                status.ClassName = Constants.Text_CssClassRegPreciseUp;

                // status2 added for Xin-Yi's new intro page
                status2.InnerHTML = Constants.Text_HeaderRegPreciseUp;
                status2.ClassName = Constants.Text_CssClassRegPreciseUp;
            });*/
        }

        /// <summary>
        /// Refreshes the RegPrecise status indicator whenever a property
        /// changes in the DataLayer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        static void DataLayer_RegPreciseContact(object sender, EventArgs args)
        {
            CheckRegPreciseStatus();
        }

        // Whether colour blind colours are used or not (false = no, true = yes)
        private static bool colourBlind;

        /// <summary>
        /// Gets or sets whether the visualisation should use colour blind colours
        /// <para>"Colour blind" colours are not actually implemented yet - this is to test the functionality</para>
        /// </summary>
        public static bool ColourBlind {
            get { return colourBlind; }
            set { colourBlind = value;
            /*NotifyPropertyChanged("ColourBlind");*/ }
        }
	}
}
