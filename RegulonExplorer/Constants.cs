// Constants.cs
//

using System;
using System.Collections.Generic;

namespace RegulonExplorer.Common {

	public static class Constants {
		private const string child_controls_wrong_type = "Child control has unexpected CodeBehind type.";
		public const string dom_element_may_not_be_null = "DOM Element may not be null.";
		private static string gene_and_regulator_id_must_match = "Gene and regulator id must match: gene = {0}, regulator = {1}.";
		private static string not_implemented = "Not implemented.";
		private static object regprecise_no_genomes = "Genome list not available from RegPrecise web service.";
		private static string gene_and_site_id_must_match = "Gene and site id must match: gene = {0}, site = {1}.";
		private static bool useWedgeWheels = false;                 // Specifies whether the networks use "wedge wheels" instead of "wagon wheels" (default: false)
		private static double regulonDisplayWidth = 2.0;
		private static double regulonDisplayHeight = 2.1;
		private static string regulonDisplayUnits = "in";
        private static bool useHammerTouchSupport = true;           // Specifies whether Hammer.js is used for touch handling (default: true)
        private static bool useGoTerms = true;                      // Specifies whether GO terms are loaded as gene information (from a local MySQL database) and used to colour target genes (default: true)
        private static bool detailedViewSelected = true;            // Specifies whether the detailed view is filtered to show only selected networks (default: true)
        private static int largeScaleWidthThreshold = /*1000;*/ 1920;
        private static int largeScaleHeightThreshold = /*600;*/ /*1200*/ 1080;
        private static bool showTooltipsOnHammerTap = true;         // Specifies whether gene tooltips appear on a tap when using Hammer.JS (default: true)
        private static int largeScaleNodeScaleFactor = 3;           // Specifies the factor that network elements are enlarged by at very high resolutions (default: 3)
        private static bool useGroupPalette = false;                // Specifies whether group views include a free "palette" area (default: false)
        private static bool useGraphLabelWrapping = true;           // Specifies whether network graph labels will appear on two lines (and be monospaced) (default: true)
        private static bool useCssNodeAttributes = true;            // Specifies whether node attributes will be determined by CSS files (default: true)
#if DEBUG
        private static bool useVariableHEDistance = false;          // (TESTING) Specifies whether the slider for Euclidean/Hamming distance ratio appears (default: false)
#endif
        private static bool noGroupLimit = true;                   // Specifies whether users can define any number of groups rather than just 1 to 5 (default: true)
#if DEBUG
        private static bool useColourBlindToggle = false;           // (TESTING) Specifies whether user can toggle between normal and colour blind colour palettes (colour blind friendly colours not actually implemented yet) (default: false)
#endif
#if DEBUG
        private static bool selectedNetworksAsCentroids = true;     // (TESTING) Specifies whether the user can decide to use the currently selected networks as centroids (default: true)
#endif
#if DEBUG
        private static bool useGroupDisplaysGlobally = true;        // (TESTING) Specifies whether to use the (Hammer)GroupRegulonDisplay even when there is only one group (default: true)
#endif
        private static int largeScaleGraphScaleFactor = 2;          // Specifies the factor that regulon graphs are enlarged by at very high resolutions (default: 2)
        private static bool useLocalMySQLCache = true;              // Specifies whether to use the local MySQL database (the same as the one containing the GO terms) to cache RegPrecise data as it is loaded - TRNDiff will attempt to load data from this database first before contacting RegPrecise (default: true)
        private static bool useAutoCentroidButton = false;          // Specifies whether the Auto centroid button is displayed (default: false)
        private static bool selectGenesOnHammerTap = false;         // Specifies whether genes are selected with a tap when using Hammer.JS (default: false)

        // ------------ Text constants ------------

        // Classes and text for the label that indicates whether RegPrecise is
        // accessible
        private static string text_HeaderRegPreciseUp = "up";
        private static string text_HeaderRegPreciseDown = "down";
        private static string text_CssClassRegPreciseUp = "RegPreciseStatusLabelUp";
        private static string text_CssClassRegPreciseDown = "RegPreciseStatusLabelDown";
        private static string text_HeaderRegPreciseUnknown = "checking...";
        private static string text_CssClassRegPreciseUnknown = "RegPreciseStatusLabelUnknown";

        // Legend captions for regulators
        private static string text_LegendCaptionRegulator = "regulator";   // Specifies the legend caption for regulators
        private static string text_LegendCaptionNoRegulator = "transcription factor";   // Specifies the legend caption for regulators without data

        // The following seven variables specify the legend captions for comparisons
        private static string text_LegendCaptionBoth = "Element is present in both networks";
        private static string text_LegendCaptionLeft = "Element is present in Regulon 1 only";
        private static string text_LegendCaptionRight = "Element is present in Regulon 2 only";
        private static string text_LegendCaptionBothMultiple = "Element is present in all networks";
        private static string text_LegendCaptionLeftMultiple = "Element is present in the reference network only";
        private static string text_LegendCaptionRightMultiple = "Element is present in a non-reference network only";
        private static string text_LegendCaptionSomeMultiple = "Element is present in the reference network and at least one non-refernece network (only appears in networks that actually have the element)";

        // Legend caption for unknown entry
        private static string text_LegendUnknown = "unknown";

        // Alert messages
        private static string text_MessageErrorLoadingRegulog = /*"This does not seem to be a valid Regulog dataset."*/ "There was an error while trying to load this file. Check if it is a valid Regulog dataset.";
        private static string text_MessageErrorDataLoading = "Please wait for data to finish loading.";
        private static string text_MessageErrorInit = "There was an error while trying to initalise the page. Please email the following details to j.hogan@qut.edu.au: \n\n {0}: {1}";
        private static string text_MessageNotEnoughColoursGF = "There are more gene functions in this data set  ({0})  then there are colours  ({1})! Some colours will have to represent more than one gene function.";
        private static string text_MessageNotEnoughColoursGO = "There are more GO terms in this data set  ({0})  then there are colours  ({1})! Some colours will have to represent more than one GO term.";
        private static string text_MessageTooManyGroups = "You cannot select more groups than there are networks in the data set.";

        // Used for the build date label in the header
        private static string text_HeaderBuildDate = "Build date: ";

        // Variable button labels
        private static string text_ButtonAutoGroupingOn = "Auto grouping on";
        private static string text_ButtonAutoGroupingOff = "Auto grouping off";
        private static string text_ButtonAutoCentroidOn = "Auto centroid on";
        private static string text_ButtonAutoCentroidOff = "Auto centroid off";
        private static string text_ButtonComparisonLockOn = "Comparison lock on";
        private static string text_ButtonComparisonLockOff = "Comparison lock off";

        // Used for network labels
        private static string text_NetworkLabelCentroid = "(centroid)";
        private static string text_NetworkLabelAnd = "AND";
        private static string text_NetworkLabelOr = "OR";
        private static string text_NetworkLabelXor = "XOR";
        private static string text_NetworkLabelReference = "(reference)";

        // Sort drop down items
        private static string text_DropDownSortAlpha = "Alphabetical order";
        private static string text_DropDownSortReverseAlpha = "Alphabetical order (reverse)";
        private static string text_DropDownSortNoOfGenes = "Number of genes (increasing order)";
        private static string text_DropDownSortReverseNoOfGenes = "Number of genes (decreasing order)";
        private static string text_DropDownSortDistToCentroid = "Distance from centroid";

        // Used for tooltips
        private static string text_TooltipLocusTag = "Locus tag";
        private static string text_TooltipName = "Name";
        private static string text_TooltipGeneFunction = "Gene function";
        private static string text_TooltipRegulonId = "Regulon ID";
        private static string text_TooltipGeneId = "Gene ID";
        private static string text_TooltipSitePosition = "Site position";
        private static string text_TooltipSiteScore = "Site score";
        private static string text_TooltipSiteSequence = "Site sequence";
        private static string text_TooltipSite = "Site";
        private static string text_TooltipAnnotation = "Annotation";
        private static string text_TooltipGoTerm = "GO term";
        private static string text_TooltipGoParentTerm = "Parent GO term";
        private static string text_TooltipRegulatorFamily = "Regulator family";
        private static string text_TooltipNumberOfTGs = "Number of target genes";
        private static string text_TooltipNotFound = "Not found";
        private static string text_TooltipNA = "N/A";

        // Used for selected homologues list
        private static string text_LabelSelectedGenes_None = "none";
        private static string text_LabelSelectedGenes_EntrySingle = "{0} in {1} regulon, ";
        private static string text_LabelSelectedGenes_EntryMultiple = "{0} in {1} regulons, ";

        // Used by drop down menus
        private static string text_OptionAllTerms = "All terms";
        private static string text_OptionAllFunctions = "All functions";
        private static string text_OptionPlaceholder = "------------";

        // Filter drop down items
        private static string text_DropDownFilterAll = "Show all networks";
        private static string text_DropDownFilterSelected = "Show selected networks only";

		public static string CHILD_CONTROLS_WRONG_TYPE {
			get {
				return child_controls_wrong_type;
			}
		}

		public static string DOM_ELEMENT_MAY_NOT_BE_NULL {
			get {
				return dom_element_may_not_be_null;
			}
		}

		public static string REG_PRECISE_SERVICE_URL {
			get { return "/RegPrecise.svc"; }
		}

		public static string GENE_AND_REGULATOR_ID_MUST_MATCH {
			get { return gene_and_regulator_id_must_match; }
		}

		public static string NOT_IMPLEMENTED {
			get { return not_implemented; }
		}


		public static object REGPRECISE_NO_GENOMES {
			get { return regprecise_no_genomes; }
		}

		public static string GENE_AND_SITE_ID_MUST_MATCH {
			get { return gene_and_site_id_must_match; }
		}

        /// <summary>
        /// Specifies whether the networks use "wedge wheels" instead of "wagon wheels"
        /// <para>(default: false)</para>
        /// </summary>
		public static bool UseWedgeWheels {
			get { return useWedgeWheels; }
		}

		public static double RegulonDisplayWidth {
			get { return regulonDisplayWidth; }
		}

		public static double RegulonDisplayHeight {
			get { return regulonDisplayHeight; }
		}

		public static string RegulonDisplayUnits {
			get { return regulonDisplayUnits; }
		}

		public static string LOCAL_MYSQL_SERVICE_URL {
			get { return "/LocalMySQL.svc"; }
		}

        /// <summary>
        /// Specifies whether Hammer.js is used for touch handling
        /// <para>(default: true)</para>
        /// </summary>
        public static bool UseHammerTouchSupport {
            get { return useHammerTouchSupport; }
        }

        /// <summary>
        /// Specifies whether GO terms are loaded as gene information (from a local MySQL database) and used to colour target genes
        /// <para>(default: true)</para>
        /// </summary>
        public static bool UseGoTerms
        {
            get { return Constants.useGoTerms; }
        }

        /// <summary>
        /// Specifies whether the detailed view is filtered to show only selected networks
        /// <para>(default: true)</para>
        /// </summary>
        public static bool DetailedViewSelected
        {
            get { return Constants.detailedViewSelected; }
        }

        public static int LargeScaleWidthThreshold
        {
            get { return Constants.largeScaleWidthThreshold; }
        }

        public static int LargeScaleHeightThreshold
        {
            get { return Constants.largeScaleHeightThreshold; }
        }

        /// <summary>
        /// Specifies whether gene tooltips appear on a tap when using Hammer.JS
        /// <para>(default: true)</para>
        /// <para>If false, a press is required</para>
        /// </summary>
        public static bool ShowTooltipsOnHammerTap
        {
            get { return Constants.showTooltipsOnHammerTap; }
        }

        /// <summary>
        /// Specifies the factor that network elements are enlarged by at very high resolutions
        /// <para>"Very high resolutions" are anything with dimensions greater than LargeScaleWidthThreshold and LargeScaleHeightThreshold</para>
        /// <para>(default: 3)</para>
        /// </summary>
        public static int LargeScaleNodeScaleFactor
        {
            get { return Constants.largeScaleNodeScaleFactor; }
        }

        /// <summary>
        /// Specifies whether group views include a free "palette" area
        /// <para>Currently does not appear correctly after the CSS styling changes to the group containers</para>
        /// <para>(default: false)</para>
        /// </summary>
        public static bool UseGroupPalette
        {
            get { return Constants.useGroupPalette; }
        }

        /// <summary>
        /// Specifies whether network graph labels will appear on two lines (and be monospaced)
        /// <para>(default: true)</para>
        /// </summary>
        public static bool UseGraphLabelWrapping
        {
            get { return Constants.useGraphLabelWrapping; }
        }

        /// <summary>
        /// Specifies whether node attributes will be determined by CSS files
        /// <para>(default: true)</para>
        /// </summary>
        public static bool UseCssNodeAttributes
        {
            get { return Constants.useCssNodeAttributes; }
        }

#if DEBUG
        /// <summary>
        /// (TESTING) Specifies whether the slider for Euclidean/Hamming distance ratio appears
        /// <para>(default: false)</para>
        /// </summary>
        public static bool UseVariableHEDistance
        {
            get { return Constants.useVariableHEDistance; }
        }
#endif

        /// <summary>
        /// Specifies whether users can define any number of groups rather than just 1 to 5
        /// <para>(default: true)</para>
        /// </summary>
        public static bool NoGroupLimit
        {
            get { return Constants.noGroupLimit; }
        }

#if DEBUG
        /// <summary>
        /// (TESTING) Specifies whether user can toggle between normal and colour blind colour palettes
        /// <para>(colour blind friendly colours not actually implemented yet)</para>
        /// <para>(default: false)</para>
        /// </summary>
        public static bool UseColourBlindToggle
        {
            get { return Constants.useColourBlindToggle; }
        }
#endif

#if DEBUG
        /// <summary>
        /// (TESTING) Specifies whether the user can decide to use the currently selected networks as centroids
        /// <para>(default: true)</para>
        /// </summary>
        public static bool SelectedNetworksAsCentroids
        {
            get { return Constants.selectedNetworksAsCentroids; }
        }
#endif

#if DEBUG
        /// <summary>
        /// (TESTING) Specifies whether to use the (Hammer)GroupRegulonDisplay even when there is only one group
        /// <para>(default: true)</para>
        /// </summary>
        public static bool UseGroupDisplaysGlobally
        {
            get { return Constants.useGroupDisplaysGlobally; }
        }
#endif

        /// <summary>
        /// Specifies the factor that regulon graphs are enlarged by at very high resolutions
        /// <para>(default: 2)</para>
        /// </summary>
        public static int LargeScaleGraphScaleFactor
        {
            get { return Constants.largeScaleGraphScaleFactor; }
        }

        /// <summary>
        /// Specifies whether to use the local MySQL database (the same as the one containing the GO terms) to cache RegPrecise data as it is loaded
        /// <para>TRNDiff will attempt to load data from this database first before contacting RegPrecise</para>
        /// <para>(default: true)</para>
        /// </summary>
        public static bool UseLocalMySQLCache
        {
            get { return Constants.useLocalMySQLCache; }
        }

        /// <summary>
        /// Specifies whether the Auto centroid button is displayed
        /// <para>(default: false)</para>
        /// </summary>
        public static bool UseAutoCentroidButton
        {
            get { return Constants.useAutoCentroidButton; }
        }

        /// <summary>
        /// Specifies whether genes are selected with a tap when using Hammer.JS
        /// <para>(default: false)</para>
        /// <para>If false, a press is required</para>
        /// </summary>
        public static bool SelectGenesOnHammerTap
        {
            get { return Constants.selectGenesOnHammerTap; }
        }

        // ------------ Text constants ------------

        public static string Text_HeaderRegPreciseUp
        {
            get { return Constants.text_HeaderRegPreciseUp; }
        }

        public static string Text_HeaderRegPreciseDown
        {
            get { return Constants.text_HeaderRegPreciseDown; }
        }

        public static string Text_CssClassRegPreciseUp
        {
            get { return Constants.text_CssClassRegPreciseUp; }
        }

        public static string Text_CssClassRegPreciseDown
        {
            get { return Constants.text_CssClassRegPreciseDown; }
        }

        public static string Text_HeaderRegPreciseUnknown
        {
            get { return Constants.text_HeaderRegPreciseUnknown; }
        }

        public static string Text_CssClassRegPreciseUnknown
        {
            get { return Constants.text_CssClassRegPreciseUnknown; }
        }

		public static string Text_LegendCaptionRegulator {
			get { return text_LegendCaptionRegulator; }
		}

		public static string Text_LegendCaptionNoRegulator {
			get { return text_LegendCaptionNoRegulator; }
		}

        public static string Text_LegendCaptionBoth
        {
            get { return Constants.text_LegendCaptionBoth; }
        }

        public static string Text_LegendCaptionLeft
        {
            get { return Constants.text_LegendCaptionLeft; }
        }

        public static string Text_LegendCaptionRight
        {
            get { return Constants.text_LegendCaptionRight; }
        }

        public static string Text_LegendCaptionBothMultiple
        {
            get { return Constants.text_LegendCaptionBothMultiple; }
        }

        public static string Text_LegendCaptionLeftMultiple
        {
            get { return Constants.text_LegendCaptionLeftMultiple; }
        }

        public static string Text_LegendCaptionRightMultiple
        {
            get { return Constants.text_LegendCaptionRightMultiple; }
        }

        public static string Text_LegendCaptionSomeMultiple
        {
            get { return Constants.text_LegendCaptionSomeMultiple; }
        }

        public static string Text_LegendUnknown
        {
            get { return Constants.text_LegendUnknown; }
        }

        public static string Text_MessageErrorLoadingRegulog
        {
            get { return Constants.text_MessageErrorLoadingRegulog; }
        }

        public static string Text_MessageErrorDataLoading
        {
            get { return Constants.text_MessageErrorDataLoading; }
        }

        public static string Text_MessageErrorInit
        {
            get { return Constants.text_MessageErrorInit; }
        }

        public static string Text_MessageNotEnoughColoursGF
        {
            get { return Constants.text_MessageNotEnoughColoursGF; }
        }

        public static string Text_MessageNotEnoughColoursGO
        {
            get { return Constants.text_MessageNotEnoughColoursGO; }
        }

        public static string Text_MessageTooManyGroups
        {
            get { return Constants.text_MessageTooManyGroups; }
        }

        public static string Text_HeaderBuildDate
        {
            get { return Constants.text_HeaderBuildDate; }
        }

        public static string Text_ButtonAutoGroupingOn
        {
            get { return Constants.text_ButtonAutoGroupingOn; }
        }

        public static string Text_ButtonAutoGroupingOff
        {
            get { return Constants.text_ButtonAutoGroupingOff; }
        }

        public static string Text_ButtonAutoCentroidOn
        {
            get { return Constants.text_ButtonAutoCentroidOn; }
        }

        public static string Text_ButtonAutoCentroidOff
        {
            get { return Constants.text_ButtonAutoCentroidOff; }
        }

        public static string Text_ButtonComparisonLockOn
        {
            get { return Constants.text_ButtonComparisonLockOn; }
        }

        public static string Text_ButtonComparisonLockOff
        {
            get { return Constants.text_ButtonComparisonLockOff; }
        }

        public static string Text_NetworkLabelCentroid
        {
            get { return Constants.text_NetworkLabelCentroid; }
        }

        public static string Text_NetworkLabelAnd
        {
            get { return Constants.text_NetworkLabelAnd; }
        }

        public static string Text_NetworkLabelOr
        {
            get { return Constants.text_NetworkLabelOr; }
        }

        public static string Text_NetworkLabelXor
        {
            get { return Constants.text_NetworkLabelXor; }
        }

        public static string Text_NetworkLabelReference
        {
            get { return Constants.text_NetworkLabelReference; }
        }

        public static string Text_DropDownSortAlpha
        {
            get { return Constants.text_DropDownSortAlpha; }
        }

        public static string Text_DropDownSortReverseAlpha
        {
            get { return Constants.text_DropDownSortReverseAlpha; }
        }

        public static string Text_DropDownSortNoOfGenes
        {
            get { return Constants.text_DropDownSortNoOfGenes; }
        }

        public static string Text_DropDownSortReverseNoOfGenes
        {
            get { return Constants.text_DropDownSortReverseNoOfGenes; }
        }

        public static string Text_DropDownSortDistToCentroid
        {
            get { return Constants.text_DropDownSortDistToCentroid; }
        }

        public static string Text_TooltipLocusTag
        {
            get { return Constants.text_TooltipLocusTag; }
        }

        public static string Text_TooltipName
        {
            get { return Constants.text_TooltipName; }
        }

        public static string Text_TooltipGeneFunction
        {
            get { return Constants.text_TooltipGeneFunction; }
        }

        public static string Text_TooltipRegulonId
        {
            get { return Constants.text_TooltipRegulonId; }
        }

        public static string Text_TooltipGeneId
        {
            get { return Constants.text_TooltipGeneId; }
        }

        public static string Text_TooltipSitePosition
        {
            get { return Constants.text_TooltipSitePosition; }
        }

        public static string Text_TooltipSiteScore
        {
            get { return Constants.text_TooltipSiteScore; }
        }

        public static string Text_TooltipSiteSequence
        {
            get { return Constants.text_TooltipSiteSequence; }
        }

        public static string Text_TooltipSite
        {
            get { return Constants.text_TooltipSite; }
        }

        public static string Text_TooltipAnnotation
        {
            get { return Constants.text_TooltipAnnotation; }
        }

        public static string Text_TooltipGoTerm
        {
            get { return Constants.text_TooltipGoTerm; }
        }

        public static string Text_TooltipGoParentTerm
        {
            get { return Constants.text_TooltipGoParentTerm; }
        }

        public static string Text_TooltipRegulatorFamily
        {
            get { return Constants.text_TooltipRegulatorFamily; }
        }

        public static string Text_TooltipNumberOfTGs
        {
            get { return Constants.text_TooltipNumberOfTGs; }
        }

        public static string Text_TooltipNotFound
        {
            get { return Constants.text_TooltipNotFound; }
        }

        public static string Text_TooltipNA
        {
            get { return Constants.text_TooltipNA; }
        }

        public static string Text_LabelSelectedGenes_None
        {
            get { return Constants.text_LabelSelectedGenes_None; }
        }

        public static string Text_LabelSelectedGenes_EntrySingle
        {
            get { return Constants.text_LabelSelectedGenes_EntrySingle; }
        }

        public static string Text_LabelSelectedGenes_EntryMultiple
        {
            get { return Constants.text_LabelSelectedGenes_EntryMultiple; }
        }

        public static string Text_OptionAllTerms
        {
            get { return Constants.text_OptionAllTerms; }
        }

        public static string Text_OptionAllFunctions
        {
            get { return Constants.text_OptionAllFunctions; }
        }

        public static string Text_OptionPlaceholder
        {
            get { return Constants.text_OptionPlaceholder; }
        }

        public static string Text_DropDownFilterAll
        {
            get { return Constants.text_DropDownFilterAll; }
        }

        public static string Text_DropDownFilterSelected
        {
            get { return Constants.text_DropDownFilterSelected; }
        }

#if DEBUG
        // ------------ Debug elements ------------

        private static bool showDebugMessasges = false;                     // Specifies whether debug messages are printed to the console (default: false)

        /// <summary>
        /// Specifies whether debug messages are printed to the console
        /// <para>(default: false)</para>
        /// </summary>
        public static bool ShowDebugMessages
        {
            get { return Constants.showDebugMessasges; }
        }

        /*// <summary>
        //// Prints a message to the console only if the constant ShowDebugMessages is enabled
        //// </summary>
        //// <param name="message">The message to display</param>
        public static void DebugMessageConsole(string message) {
            if (showDebugMessasges) {
                if (Constants.ShowDebugMessages) Console.Log(message);
                return;
            } else {
                return;
            }
        }*/

        /*/// <summary>
        /// Prints a message in an alert box only if the constant ShowDebugMessages is enabled
        /// </summary>
        /// <param name="message">The message to display</param>
        public static void DebugMessageAlert(string message) {
            if (showDebugMessasges) {
                System.Html.Window.Alert(message);
                return;
            } else {
                return;
            }
        }*/
#endif
	}
}
