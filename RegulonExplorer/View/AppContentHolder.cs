﻿using System;
using System.Collections.Generic;

using System.Html;
using SystemQut.Controls;
using SystemQut;

namespace RegulonExplorer.View {
	public class AppContentHolder : Activated {

		public AppContentHolder ( Element domElement )
			: base( domElement ) {
			HtmlUtil.BindControl( this, domElement );
		}
	}
}
