﻿using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.Common;
using SystemQut;
using SystemQut.Svg;
using Hammer;
using SystemQut.ComponentModel;

namespace RegulonExplorer.View.RegPreciseView
{
    
    
    /// <summary>
    /// A base class for a regulon display that uses Hammer.JS to manipulate
    /// graphs. Can also be used for any regulon display that will always
    /// require only one group
    /// </summary>
    public class HammerFreeRegulonDisplay : IRegulonDisplay, INotifyPropertyChanged
    {

        /// <summary> Used to check whether this is a new regulon display, so
        /// we can prevent sorting if we've loaded a new dataset in the local
        /// mode
        /// </summary>

        protected bool newDisplay = true;

        public bool NewDisplay
        {
            get { return newDisplay; }
            set { newDisplay = value; }
        }

        protected Hammer.Hammer containerHammer;

        // The currently selected regulons in the display, in order of selection
        // if possible
        protected List<RegulonInfo> selectedRegulons = new List<RegulonInfo>();

        public List<RegulonInfo> SelectedRegulons
        {
            get { return selectedRegulons; }
        }

        /// <summary> Displays the list of regulons in the specified container
		/// </summary>
		/// <param name="regulons"></param>
		/// <param name="container"></param>
		/// <param name="selectedRegulog"></param>
		/// <param name="nodeFactory"></param>
		/// <param name="colourChooser"></param>
        /// 
        virtual public void DisplayRegulons(
            List<RegulonInfo> regulons,
            Element container,
            RegulogInfo selectedRegulog,
            INodeFactory nodeFactory,
            IAttributeFactory colourChooser
        ) {
            this.selectedRegulog = selectedRegulog;
            this.container = container;

            JSObject managerOptions = new JSObject();
            managerOptions["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
            managerOptions["domEvents"] = true; //http://hammerjs.github.io/tips/
                
            // let the pan gesture support all directions.
            // this will block the vertical scrolling on a touch-device while on the element

            this.container.Style.Height = "auto";
            this.container.Style.Width = "auto";
            
            //remove all the children this regulon display has
            while (container.ChildNodes[0] != null)
            {
                container.RemoveChild(container.ChildNodes[0]);
            }

            // Initialise the matrix of regulons and their gene locations
            geneIndexes = new int[regulons.Count][];
            for (int i = 0; i < regulons.Count; i++) {
               geneIndexes[i] = new int[selectedRegulog.Genes.Length];
            }

            // Store the index of each gene in the matrix above
            foreach (GeneInfo gene in selectedRegulog.Genes) {
                geneDictionary[gene.Name] = selectedRegulog.Genes.IndexOf(gene);
            }

            foreach (RegulonInfo regulon in regulons)
            {
                RegulonGraph regulonGraph = new RegulonGraph(
                    selectedRegulog,
                    regulon,
                    nodeFactory,
                    colourChooser,
                    Constants.RegulonDisplayWidth,
                    Constants.RegulonDisplayHeight,
                    Constants.RegulonDisplayUnits
                );

                regulonGraph.DomElement.ID = regulon.GenomeName;

                foreach (GeneInfo geneInfo in regulonGraph.Regulon.Genes)
                {
                    ((SystemQut.ComponentModel.INotifyPropertyChanged)geneInfo).PropertyChanged += node_PropertyChanged;
                    if (geneInfo.IsSelected) {
                        selectedGenes.Add(geneInfo);
                    }

                    // Add the index of the current gene into the matrix
                    geneIndexes[selectedRegulog.Regulons.IndexOf(regulonGraph.Regulon)][geneDictionary[geneInfo.Name]] = regulonGraph.Regulon.Genes.IndexOf(geneInfo);
                }

                SetGraphClass(regulonGraph, false, false);
                regulonGraph.AddElementTo(container);
                regulonGraphs.Add(regulonGraph);
   
				HammerListeners[regulonGraph.DomElement.ID] = new Hammer.Hammer( regulonGraph.DomElement );

				// let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                HammerListeners[regulonGraph.DomElement.ID].Set(managerOptions);

                HammerListeners[regulonGraph.DomElement.ID].On( "panleft panright panup pandown", GraphOnPan );
                HammerListeners[regulonGraph.DomElement.ID].On( "doubletap", GraphOnTap );
                HammerListeners[regulonGraph.DomElement.ID].On( "panstart", GraphOnPanStart );
                HammerListeners[regulonGraph.DomElement.ID].On( "singletap", GraphOnPress );
                HammerListeners[regulonGraph.DomElement.ID].On( "panend", GraphOnPanEnd );
                HammerListeners[regulonGraph.DomElement.ID].On( "pinch", GraphOnPinch );
#if DEBUG
                HammerListeners[regulonGraph.DomElement.ID].On( "pinchstart", GraphOnPinchStart );
                HammerListeners[regulonGraph.DomElement.ID].On( "pinchend", GraphOnPinchEnd );
                HammerListeners[regulonGraph.DomElement.ID].On( "pinchcancel", GraphOnPinchEnd );
#endif

                JSObject pinchOptions = new JSObject();
                pinchOptions["enable"] = true;
                pinchOptions["threshold"] = 2;

                HammerListeners[regulonGraph.DomElement.ID].Get( "pinch" ).Set(pinchOptions);

                regulonGraph.Regulon.PropertyChanged += regulonGraph_PropertyChanged;
            }
        }

		/// <summary> Updates the visible regulons in the associated container,
		///		applying a filter and sorting function to 
		/// </summary>
		/// <param name="sort"></param>
		/// <param name="filter"></param>

		virtual public void UpdateVisibleRegulons(
			Func<RegulonInfo, RegulonInfo, int> sort,
			Func<RegulonInfo, bool> filter
		) {
            PositionGraphs(sort, filter);
            
            // We want to always sort after this
            newDisplay = false;
        }

        protected void PositionGraphs(Func<RegulonInfo, RegulonInfo, int> sort, Func<RegulonInfo, bool> filter)
        {
            List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

			for ( int i = 0; i < regulonGraphs.Count; i++ ) {
				RegulonGraph graph = regulonGraphs[i];

				if ( filter == null || filter( graph.Regulon ) ) {
					visibleGraphs.Add( graph );
				}
			}

			visibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return sort( x.Regulon, y.Regulon ); } );

			foreach ( RegulonGraph graph in regulonGraphs ) {
				graph.RemoveElement();
                FindParentOfType( graph.DomElement, "span" ).Style.Position = String.Empty;
                FindParentOfType( graph.DomElement, "span" ).Style.Left = String.Empty;
                FindParentOfType( graph.DomElement, "span" ).Style.Top = String.Empty;
            }

			foreach ( RegulonGraph graph in visibleGraphs ) {
				graph.AddElementTo( container );
			}

            foreach ( RegulonGraph graph in visibleGraphs ) {

                if (newDisplay && graph.Regulon.PositionLeft != -1 && graph.Regulon.PositionTop != -1) {
                    HammerPositions[graph.DomElement.ID] = new double[] { graph.Regulon.PositionLeft, graph.Regulon.PositionTop };
/*#if DEBUG
                Console.Log(graph.DomElement.ID + " origin loaded from the saved file as " + HammerPositions[graph.DomElement.ID][0] + ", " + HammerPositions[graph.DomElement.ID][1]);
#endif*/
                } else {
                    HammerPositions[graph.DomElement.ID] = new double[] { FindParentOfType( graph.DomElement, "span" ).OffsetLeft, FindParentOfType( graph.DomElement, "span" ).OffsetTop };
                    graph.Regulon.PositionLeft = HammerPositions[graph.DomElement.ID][0];
                    graph.Regulon.PositionTop = HammerPositions[graph.DomElement.ID][1];
/*#if DEBUG
                Console.Log(graph.DomElement.ID + " origin set as " + HammerPositions[graph.DomElement.ID][0] + ", " + HammerPositions[graph.DomElement.ID][1]);
#endif*/
                }
            }

            maxZLevel = 1;
            foreach ( RegulonGraph graph in visibleGraphs ) {
                FindParentOfType( graph.DomElement, "span" ).Style.Position = "absolute";
                FindParentOfType( graph.DomElement, "span" ).Style.Left = HammerPositions[graph.DomElement.ID][0] + "px";
                FindParentOfType( graph.DomElement, "span" ).Style.Top = HammerPositions[graph.DomElement.ID][1] + "px";
                FindParentOfType( graph.DomElement, "span" ).Style.ZIndex = maxZLevel;
                maxZLevel = (short)(maxZLevel + 1);

                /*if (numberOfGroups > 1) {
                    graph.Regulon.GroupNumber = DetermineGraphGroup(graph);
#if DEBUG
                    Console.Log(graph.DomElement.ID + " is in group " + graph.Regulon.GroupNumber);
#endif
                }*/
            }
        }        

		/// <summary> Removes all regulon graphs from the display and disposes them, leaving the 
		///		display blank.
		/// </summary>

        virtual public void Clear()
        {			
            foreach ( RegulonGraph graph in regulonGraphs ) {
                HammerListeners[graph.DomElement.ID].Destroy();
                graph.RemoveElement();
				graph.Dispose();
			}
            ClearSelectedRegulons();
			regulonGraphs.Clear();
		}

        protected void SetGraphAttributes(int graphIndex, double widthFactor, double heightFactor, TextAnchorType textAnchor, double textFactor, double labelY, double labelX, SvgRect newViewBox, bool nodeLabelVisibility) {
            Element target = FindParentOfType( regulonGraphs[graphIndex].DomElement, "span" );
            if (target.Style.Position == "absolute") {
                double hammerWidthFactor =  Constants.RegulonDisplayWidth * widthFactor / Double.Parse(regulonGraphs[graphIndex].GraphSvg.GetAttribute<string>("width"));
                double hammerHeightFactor =  Constants.RegulonDisplayHeight * heightFactor / Double.Parse(regulonGraphs[graphIndex].GraphSvg.GetAttribute<string>("height"));
                target.Style.Left = target.OffsetLeft * hammerWidthFactor + "px";
                target.Style.Top = target.OffsetTop * hammerHeightFactor + "px";
                HammerPositions[regulonGraphs[graphIndex].DomElement.ID][0] = target.OffsetLeft;
                HammerPositions[regulonGraphs[graphIndex].DomElement.ID][1] = target.OffsetTop;
            }

            if (Window.OuterWidth > Constants.LargeScaleWidth || Window.OuterHeight > Constants.LargeScaleHeight) {
                SetGraphSize(regulonGraphs[graphIndex], Constants.RegulonDisplayWidth * widthFactor * 2, Constants.RegulonDisplayHeight * heightFactor * 2);
            } else {
                SetGraphSize(regulonGraphs[graphIndex], Constants.RegulonDisplayWidth * widthFactor, Constants.RegulonDisplayHeight * heightFactor);
            }

            regulonGraphs[graphIndex].GraphLabel.TextAnchor = textAnchor;
            regulonGraphs[graphIndex].GraphLabel.FontSize = (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();
            regulonGraphs[graphIndex].GraphLabel.Y = labelY;
            regulonGraphs[graphIndex].GraphLabel.X = labelX;
            regulonGraphs[graphIndex].GraphSvg.ViewBox = newViewBox;
            regulonGraphs[graphIndex].SetNodeLabelVisibility(nodeLabelVisibility);
            regulonGraphs[graphIndex].GraphLabelBackground.Y = labelY + (double.Parse(regulonGraphs[graphIndex].GraphLabel.FontSize) * 0.75);
            if (labelX != 0) {
                regulonGraphs[graphIndex].GraphLabelBackground.Visibility = "hidden";
            } else {
                regulonGraphs[graphIndex].GraphLabelBackground.Visibility = "visible";
            }
        }        

        // Hammer testing

        // A list of the current centre positions of each graph, as well as
        // their listeners.
        // This setup is based on that used in the HammerJS homepage to store
        // the current location of the square in the example
        protected Dictionary<string, double[]> HammerPositions = new Dictionary<string, double[]>();
        protected Dictionary<string, Hammer.Hammer> HammerListeners = new Dictionary<string, Hammer.Hammer>();

        private double[] panStart = { -1, -1 };

        // On a pan event, move the graph along with the pan by altering its
        // absolute positioning in the container
        protected void GraphOnPan(JSObject ev) {
#if DEBUG
            if (!pinching) {
#endif
            Script.Literal("ev.preventDefault()");

            // On the Microsoft surface, we have some issues with weird delta
            // values. This bit will attempt to prevent odd results from that
            if (panStart[0] == -1) {
                panStart[0] = (double)(ev["center"] as JSObject)["x"];
                panStart[1] = (double)(ev["center"] as JSObject)["y"];
            }

            if (panStart[0] + (double)ev["deltaX"] < 0
                || panStart[0] + (double)ev["deltaY"] < 0) {
                Console.Log("Pan would go outside the bounds of the page; cancelling the pan function.");

                // Force a drop if anything was being dragged
                GraphOnPanEnd(ev);
                return;
            }

            Element target = FindParentOfType( (Element)ev["target"], "span" );
            
/*#if DEBUG
            Console.Log("HammerOnPan triggered on " + target.ID);
            Console.Log("HammerPositions[" + target.ID + "] = {" + HammerPositions[target.ID][0] + ", " + HammerPositions[target.ID][1] + "}");
            Console.Log("target.OffsetLeft = " + target.OffsetLeft + ", target.OffsetTop = " + target.OffsetTop);
#endif*/

            // for now, we block scrolling of the parent container so that we
            // don't get funny results if we accidentally scroll
            container.ParentNode.Style.Overflow = "hidden";

            // Place the graph in the new location, based on adding the current
            // distance panned to the stored distance. The distance is counted
            // from the initial touch position.
            // It is limited to a minimum of 0, 0, to prevent odd results by
            // having a negative coordinate
            target.Style.Left = Math.Max(HammerPositions[target.ID][0] + (double)ev["deltaX"], 0) + "px";
            target.Style.Top = Math.Max(HammerPositions[target.ID][1] + (double)ev["deltaY"], 0) + "px";
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.DomElement.ID == target.ID) {
                    graph.GraphSvg.DomElement.Style.BackgroundColor = "red";
                    break;
                }
            }     
            
#if DEBUG
            }
#endif
        }

        //Used to test when press events occur
        protected void GraphOnPress(JSObject ev) {
            Script.Literal("ev.preventDefault()");
            Element target = FindParentOfType( (Element)ev["target"], "span" );
/*#if DEBUG
            Console.Log("Press triggered on " + target.ID);
#endif*/
        }

        // At the end of a panning event, store the location of the graph. This
        // is used since the "isFinal" attribute of the pan event does not
        // always fire - it's mainly for internal HammerJS use
        protected virtual void GraphOnPanEnd(JSObject ev) {
#if DEBUG
            if (!pinching) {
#endif
            Script.Literal("ev.preventDefault()");
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            Console.Log("PanEnd triggered on " + target.ID);
#endif
            
            // Reset the overflow to auto to allow scrolling again
            container.ParentNode.Style.Overflow = "auto";

            // Store the new location
            HammerPositions[target.ID][0] = target.OffsetLeft;
            HammerPositions[target.ID][1] = target.OffsetTop;

            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.DomElement.ID == target.ID) {
                    graph.Regulon.PositionLeft = HammerPositions[target.ID][0];
                    graph.Regulon.PositionTop = HammerPositions[target.ID][1];
                    SetGraphClass(graph, false, false);
                    break;
                }
            }            

#if DEBUG
            Console.Log("PanEnd - HammerPositions[" + target.ID + "] = {" + HammerPositions[target.ID][0] + ", " + HammerPositions[target.ID][1] + "}");
#endif

            // For Surface fix
            panStart[0] = -1;
            panStart[1] = -1;
            
#if DEBUG
            panning = false;
            }
#endif
        }

        //Used to test when tap events occur
        protected void GraphOnTap(JSObject ev) {
            Script.Literal("ev.preventDefault()");
/*#if DEBUG
            Console.Log("Tap target is a " + (ev["target"] as Element).TagName);
#endif*/
            if ((String.Compare( (ev["target"] as Element).TagName, "circle", true )) != 0
                && (String.Compare( (ev["target"] as Element).TagName, "path", true )) != 0) {
                Element target = FindParentOfType( (Element)ev["target"], "span" );
/*#if DEBUG
                Console.Log("Tap triggered on " + target.ID);
#endif*/
                foreach (RegulonGraph graph in regulonGraphs) {
                    if (target == FindParentOfType(graph.DomElement, "span")) {
                        graph.IsSelected = !graph.IsSelected;
/*#if DEBUG
                        Console.Log(graph.Regulon.GenomeName + " has had its selection changed!");
#endif*/
                        SetGraphClass(graph, false, false);
                        break;
                    }
                }
            } else {
/*#if DEBUG
                Console.Log("Tap was triggered on a non background element and so we don't want to select the graph.");
#endif*/
            }
        }

        // On pinching out, increase the zoom level
        // Currently won't update the displayed zoom level in the tools panel
        protected void GraphOnPinchOut(JSObject ev) {
            Script.Literal("ev.preventDefault()");
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            Console.Log("PinchOut triggered on " + target.ID);
#endif

            // Don't do anything if it's already at the maximum
            if (currentZoomLevel < ZoomLevels.DetailView) {
                ChangeZoomLevel(currentZoomLevel + 1);
            }

#if DEBUG
            Console.Log("Zoom level changed to " + currentZoomLevel);
#endif
        }

        // On pinching in, decrease the zoom level
        // Currently won't update the displayed zoom level in the tools panel
        protected void GraphOnPinchIn(JSObject ev)
        {
            Script.Literal("ev.preventDefault()");
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            Console.Log("PinchIn triggered on " + target.ID);
#endif
            // Don't do anything if it's already at the minimum
            if (currentZoomLevel > ZoomLevels.IconList) {
                ChangeZoomLevel(currentZoomLevel - 1);
            }

#if DEBUG
            Console.Log("Zoom level changed to " + currentZoomLevel);
#endif
        }

        // On pinching in, change the zoom level
        // Currently won't update the displayed zoom level in the tools panel
        protected void GraphOnPinch(JSObject ev)
        {
#if DEBUG
            if (!panning) {
#endif

            Script.Literal("ev.preventDefault()");
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            Console.Log("Pinch triggered on " + target.ID);
#endif
            double scale = (double)ev["scale"];

            if (scale < 1 && currentZoomLevel > ZoomLevels.IconList) {
                ChangeZoomLevel(currentZoomLevel - 1);
            }

            if (scale > 1 && currentZoomLevel < ZoomLevels.DetailView) {
                ChangeZoomLevel(currentZoomLevel + 1);
            }

#if DEBUG
            Console.Log("Zoom level changed to " + currentZoomLevel);
#endif
#if DEBUG
            }
#endif
        }

        protected short maxZLevel;

        // At the start of a panning event, put the graph on the top layer
        protected void GraphOnPanStart(JSObject ev) {
#if DEBUG
            if (!pinching) {
                panning = true;
#endif
            Script.Literal("ev.preventDefault()");
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            Console.Log("PanStart triggered on " + target.ID);
#endif      
            double draggedTemp = double.Parse(target.Style.ZIndex.ToString());
/*#if DEBUG
            Console.Log("Current ZIndex for dragged " + target.ID + " is " + draggedTemp);
#endif*/

            foreach (RegulonGraph graph in regulonGraphs) {
                Element current = FindParentOfType(graph.DomElement, "span");
                if (current.ID != target.ID) {
                    double currentTemp = double.Parse(current.Style.ZIndex.ToString());
    /*#if DEBUG
                    Console.Log("(" + regulonGraphs.IndexOf(graph) + ") Current ZIndex for " + current.ID + " is " + currentTemp);
    #endif*/
                    if (currentTemp > draggedTemp) {
    /*#if DEBUG
                        Console.Log("Moving it down one!");
    #endif*/
                        current.Style.ZIndex = (short)(currentTemp - 1);
                    }
                }
            }

            target.Style.ZIndex = maxZLevel;
/*#if DEBUG
            Console.Log("Dragged " + target.ID + " ZLevel is now " + maxZLevel);
#endif*/
#if DEBUG
            }
#endif
        }
        
        // --------------------------------------------------------------------
        // Things copied from DefaultRegulonDisplay.cs
        // --------------------------------------------------------------------

		/// <summary> The container that holds these graphs.
		/// </summary>

		protected Element container;

		/// <summary> Maintain a list of GUI elements which correspond to the data model.
		/// </summary>

		protected List<RegulonGraph> regulonGraphs = new List<RegulonGraph>();

        protected List<GeneInfo> selectedGenes = new List<GeneInfo>();

        public List<GeneInfo> SelectedGenes
        {
            get { return selectedGenes; }
        }
        
        // The current zoom level of the display
        protected ZoomLevels currentZoomLevel = ZoomLevels.SmallOverview;

        public ZoomLevels CurrentZoomLevel
        {
            get { return currentZoomLevel; }
        }

        // Contains the current distances between regulons
        protected double[][] currentDistances;

        public double[][] CurrentDistances
        {
            get { return currentDistances; }
            set { currentDistances = value; }
        }
        

        // The ratio of Euclidean and Hamming distances
        protected double ratio = -1;

        public double Ratio
        {
            get { return ratio; }
            set { if (value == ratio) return;                
                ratio = value;
                /*ratioChanged = true;*/ }
        }        

        //Used to inform if the above has changed
        private bool ratioChanged = false;

        public bool RatioChanged {
            get { return ratioChanged; }
            set { ratioChanged = value; }
        }
        
        // Used to inform listeners if the selected genes are changed
        public event EventHandler SelectedGenesChanged;

        // Stores the current regulog for this display
        protected RegulogInfo selectedRegulog;

        private List<string> selectedHomologs = new List<string>();

        // These store the locations of genes inside the arrays in each regulon
        // for easier access
        private int[][] geneIndexes;
        private Dictionary<string, int> geneDictionary = new Dictionary<string,int>();

        protected void node_PropertyChanged(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs args)
        {
            if (!ClearAllSelectedGenes) {
                if (sender is GeneInfo) {
    /*#if DEBUG
                    Console.Log("node_PropertyChanged triggered for " + (sender as GeneInfo).Name);
    #endif*/
                    if (args.Includes("IsSelected"))
                    {
                        string currentGeneName = (sender as GeneInfo).Name;
    /*#if DEBUG
                        Console.Log("numProcessing is currently " + numProcessing);
    #endif*/
                        numProcessing = numProcessing + 1;
    /*#if DEBUG
                        Console.Log("numProcessing is now " + numProcessing);
    #endif*/
                        if (numProcessing > 0 && !isProcessing) {
                            isProcessing = true;
                            NotifyPropertyChanged("IsProcessing");
    #if DEBUG
                            Console.Log("isProcessing set to true");
    #endif
                        }
                        Script.SetTimeout((Action) delegate {

                            bool selected = (sender as ISelectable).IsSelected;
                            if (selected) {
        /*#if DEBUG
                                Console.Log("Adding " + (sender as GeneInfo).Name + " to selected list");
        #endif*/
                                selectedGenes.Add(sender as GeneInfo);
                            } else {
        /*#if DEBUG
                                Console.Log("Removing " + (sender as GeneInfo).Name + " from selected list");
        #endif*/
                                selectedGenes.Remove(sender as GeneInfo);
                            }
                            if (!selectedHomologs.Contains(currentGeneName)) {
            /*#if DEBUG
                                Console.Log("Selecting homologs for gene " + currentGeneName);
            #endif*/
                                selectedHomologs.Add(currentGeneName);
                                foreach (RegulonGraph regulonGraph in regulonGraphs)
                                {
                                    // Using the new dictionary
                                    int regulonIndex = selectedRegulog.Regulons.IndexOf(regulonGraph.Regulon);
                                    int geneIndex = geneDictionary[currentGeneName];
                                    if (geneIndexes[regulonIndex][geneIndex] != -1 &&
                                        regulonGraph.Regulon.Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected != selected) {
                                        regulonGraph.Regulon.Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = selected;
                                    }
                                }
        /*#if DEBUG
                            }
                            else {
                                Console.Log("Homologs already selected for gene " + currentGeneName + " during this selection action");
        #endif*/
                            }
                            OnSelectedGenesChanged();
                            if (numProcessing > 0) {
                                numProcessing = numProcessing - 1;
        /*#if DEBUG
                                Console.Log("numProcessing set back to " + numProcessing);
        #endif*/
                            }
        /*#if DEBUG
                            else {
                                Console.Log("For some reason we got to 0 before finishing");
                            }
        #endif*/
                            if (numProcessing < 1 && isProcessing) {
                                isProcessing = false;
                                NotifyPropertyChanged("IsProcessing");
        #if DEBUG
                                Console.Log("isProcessing set to false");
        #endif
                                selectedHomologs.Clear();
                            }
                        }, 10, null);
                    }
                }
    /*#if DEBUG
                Console.Log("Done selecting homologs");
    #endif*/
            }
        }

        protected void regulonGraph_PropertyChanged(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs args)
        {
            if (args.Includes("IsSelected")) {
                RegulonInfo regulon = (sender as RegulonInfo);
                if (regulon.IsSelected) {
                    if (!selectedRegulons.Contains(regulon)) {
                        selectedRegulons.Add(regulon);
                    }
                } else {
                    selectedRegulons.Remove(regulon);
                }
            }
        }

        private bool ClearAllSelectedGenes = false;

        public void ClearSelectedGenes() {
            ClearAllSelectedGenes = true;
            isProcessing = true;
            NotifyPropertyChanged("IsProcessing");
#if DEBUG
            Console.Log("isProcessing set to true");
#endif
            Script.SetTimeout((Action) delegate {
                while (selectedGenes[0] != null) {
                    selectedGenes[0].IsSelected = false;
                    selectedGenes.Remove(selectedGenes[0]);
                }
                ClearAllSelectedGenes = false;
                isProcessing = false;
                NotifyPropertyChanged("IsProcessing");
    #if DEBUG
                Console.Log("isProcessing set to false");
    #endif
            }, 10, null);
        }        

        public void ClearSelectedRegulons() {
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.IsSelected) {
                    graph.IsSelected = false;
                    SetGraphClass(graph, false, false);
                }
            }
            selectedRegulons.Clear();
        }

		protected static Element FindParentOfType ( Element element, string tagName ) {
			while ( element != null && String.Compare( element.TagName, tagName, true ) != 0 ) {
				element = element.ParentNode;
			}

			return element;
		}

        //This is used to ensure that the wagon wheel elements do not increase
        //or decrease in size when the user zooms in or out
        protected static void SetGraphSize(RegulonGraph graph, double newWidth, double newHeight)
        {
            double factor = newHeight / Double.Parse(graph.GraphSvg.GetAttribute<string>("height"));
            
#if DEBUG
            //Console.Log("Trying to set size for graph containing " + graph.Regulon.GenomeName);
#endif
            graph.GraphSvg.SetAttribute("width", newWidth + Constants.RegulonDisplayUnits);
            graph.GraphSvg.SetAttribute("height", newHeight + Constants.RegulonDisplayUnits);
            foreach (AbstractNode node in graph.Nodes) {
                if (node.Node is SvgCircle) {
                    (node.Node as SvgCircle).Radius = (node.Node as SvgCircle).Radius / factor;
                }
                if (node.Node is SvgRectangle) {
                    SvgRectangle rectangle = (node.Node as SvgRectangle);
                    rectangle.Height = rectangle.Height / factor;
                    rectangle.Width = rectangle.Width / factor;
                    rectangle.X = rectangle.X / factor;
                    rectangle.Y = rectangle.Y / factor;
                }
                if (node.Node is SvgPolygon) {
                    SvgPolygon polygon = (node.Node as SvgPolygon);
                    polygon.Height = polygon.Height / factor;
                    polygon.Width = polygon.Width / factor;
                }
                if (node is GeneNode) {
                    (node as GeneNode).Link.StrokeWidth = (node as GeneNode).Link.StrokeWidth / factor;                    
                }
                node.Node.StrokeWidth = node.Node.StrokeWidth / factor;
            }

            foreach (RegulatorNode node in graph.RegulatorNodes) {
                if (node.Node is SvgCircle) {
                    (node.Node as SvgCircle).Radius = (node.Node as SvgCircle).Radius / factor;
                }
                if (node.Node is SvgRectangle) {
                    SvgRectangle rectangle = (node.Node as SvgRectangle);
                    rectangle.Height = rectangle.Height / factor;
                    rectangle.Width = rectangle.Width / factor;
                    rectangle.X = rectangle.X / factor;
                    rectangle.Y = rectangle.Y / factor;
                }
                if (node.Node is SvgPolygon) {
                    SvgPolygon polygon = (node.Node as SvgPolygon);
                    polygon.Height = polygon.Height / factor;
                    polygon.Width = polygon.Width / factor;
                }
            }
        }

        virtual public void ChangeZoomLevel(ZoomLevels zoomLevel) {
            currentZoomLevel = zoomLevel;
            for (int i = 0; i < regulonGraphs.Length; i++) {
                ChangeGraphZoomLevel(zoomLevel, i);
            }
        }

        virtual protected void ChangeGraphZoomLevel(ZoomLevels zoomLevel, int graphIndex)
        {
            switch (zoomLevel)
            {
                case ZoomLevels.IconList:
                    SetGraphAttributes(graphIndex, 1.5, (1/3), TextAnchorType.start, 0.75, 0, 100, new SvgRect(190, -100, 200, 200), false);
                    break;
                case ZoomLevels.SmallOverview:
                    if (Constants.TwoLineGraphLabels) {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;
                case ZoomLevels.LargeOverview:
                    if (Constants.TwoLineGraphLabels) {
                        SetGraphAttributes(graphIndex, 1.5, 1.5, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1.5, 1.5, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;
                case ZoomLevels.DetailView:
                    if (Constants.TwoLineGraphLabels) {
                        SetGraphAttributes(graphIndex, 3.75, 3.75, TextAnchorType.middle, 0.3, 120, 0, new SvgRect(-125, -108.75, 250, 250), true);
                    } else {
                        SetGraphAttributes(graphIndex, 3.75, 3.75, TextAnchorType.middle, 0.3, 100, 0, new SvgRect(-125, -125, 250, 250), true);
                    }
                    break;
                default:
                    if (Constants.TwoLineGraphLabels) {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;
            }
        }
        
#if DEBUG
        private void DebugCheckSelectedList()
        {
            Console.Log("Checking the initial selected list for this display");
            if (selectedGenes.Length == 0)
            {
                Console.Log("Nothing is selected!");
            }
            else
            {
                foreach (GeneInfo gene in selectedGenes)
                {
                    Console.Log(gene.Name + " in Regulon ID " + gene.RegulonId + " is selected.");
                }
                Console.Log(selectedGenes.Length + " nodes in total are selected.");
            }
        }
#endif

        private void OnSelectedGenesChanged() {
            if (SelectedGenesChanged != null) {
                SelectedGenesChanged(this, null);
            }
        }

		public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        protected bool isProcessing = false;
        int numProcessing = 0;

        public bool IsProcessing
        {
            get { return isProcessing; }
        }

        protected virtual void SetGraphClass (RegulonGraph graph, bool dragged, bool flash) {
            string classText = "RegulonGraph";

            if (graph.IsSelected) {
                classText = classText + " RegulonGraph_Selected";
            }

            if (dragged) {
                classText = classText + " RegulonGraph_Dragged";
            }
            
            graph.GraphSvg.SetAttribute("class", classText);
        }

#if DEBUG
        protected bool pinching = false;
        protected bool panning = false;

        protected void GraphOnPinchStart(JSObject ev)
        {
            if (!panning) {
                Script.Literal("ev.preventDefault()");
    #if DEBUG
                Script.Literal("console.log(ev.type + ' triggered (GraphOnPinchStart)')");
    #endif
                pinching = true;         
            }
        }

        protected void GraphOnPinchEnd(JSObject ev)
        {
            if (!panning) {
                Script.Literal("ev.preventDefault()");

    #if DEBUG
                Script.Literal("console.log(ev.type + ' triggered (GraphOnPinchEnd)')");
    #endif
                pinching = false;
            }
        }
#endif
    }
}
