// HomeAbout.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.ViewModel;
using SystemQut.Controls;
using RegPrecise;
using SystemQut.ComponentModel;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.RegPreciseView {

	/// <summary> Code behind for the "Browse RegPrecise -> Genomes" content frame.
	/// <para>
	///		This codeBehind is not intended to be used dynamically. It must have a non-null domElement
	///		with the required fields present.
	/// </para>
	/// </summary>

	public class RegPreciseCollectionChooser : AppContent {
		private DataGrid dataGrid;
		private Select comboBox;

		private readonly Dictionary<RegulogCollectionType, RegulogCollectionInfo[]> collectionSummaries = new Dictionary<RegulogCollectionType, RegulogCollectionInfo[]>();
		private readonly List<RegulogCollectionType> requestedCollectionTypes = new List<RegulogCollectionType>();
		private readonly List<RegulogCollectionInfo> requestedRegulogCollections = new List<RegulogCollectionInfo>();

		// Set default value for regulon regulon type.
		private RegulogCollectionType collectionType = RegulogCollectionType.TaxGroup;

		private TextBox searchField;
		private Button searchButton;

		string[] comboBoxItems = {
			"Taxonomic group",
			"Transcription factor",
			"Transcription factor family",
			"RnaFamily",
			"Pathway",
			"Effector"
		};

		RegulogCollectionType [] comboBoxValues = {
			RegulogCollectionType.TaxGroup,
			RegulogCollectionType.TranscriptionFactor,
			RegulogCollectionType.TranscriptionFactorFamily,
			RegulogCollectionType.RnaFamily,
			RegulogCollectionType.Pathway,
			RegulogCollectionType.Effector
		};


		private RegulogCollectionInfo selectedCollection;
		private RegulogInfo selectedRegulog;
		private RegulogCollectionInfo[] currentCollections;

		public RegPreciseCollectionChooser ( Element domElement )
			: base( domElement ) {

			if ( domElement == null ) {
				throw new Exception( Constants.DOM_ELEMENT_MAY_NOT_BE_NULL );
			}

			FindChildControls();
			SetUpComboBox();
			comboBox.SelectionChanged += new SystemQut.Controls.ElementEventHandler( comboBox_SelectionChanged );

			ActivationChanged += RegPreciseGenomeChooser_ActivationChanged;

			// for development/debug purposes.
			if ( IsActivated ) RefreshRegulogCollections();

			searchButton.Clicked += searchButton_Clicked;
            searchField.DomElement.AddEventListener("keyup", searchField_KeyUp, false);
		}

		private void FindChildControls () {
			dataGrid = (DataGrid) FindControlByNameAndType( "RegulogCollectionTable", typeof( DataGrid ) );
			comboBox = (Select) FindControlByNameAndType( "CollectionType", typeof( Select ) );
			searchField = (TextBox) FindControlByNameAndType( "SearchText", typeof( TextBox ) );
			searchButton = (Button) FindControlByNameAndType( "SearchButton", typeof( Button ) );
		}

		void SetUpComboBox () {
			comboBox.Items = comboBoxItems;
			comboBox.SelectedIndex = 0;
		}

		void comboBox_SelectionChanged ( object sender, ElementEventArgs eventArgs ) {
			collectionType = comboBoxValues[ comboBox.SelectedIndex ];
			RefreshRegulogCollections();
		}

		/// <summary> Event handler for this.ActivationChanged.
		/// <para>
		///		When this control becomes active for the first time it will request
		///		the genome summaries from the server.
		/// </para>
		/// <para>
		///		Other than that, no further actions are taken.
		/// </para>
		/// </summary>
		/// <param name="activated"></param>

		private void RegPreciseGenomeChooser_ActivationChanged ( IActivated activated ) {
			if ( activated.IsActivated ) {
				RefreshRegulogCollections();
			}
		}

		private void RefreshRegulogCollections () {
			// Grab a copy of collectionType so we don't get odd effects in the event of overlapping asynchronous calls.
			RegulogCollectionType collectionType = this.collectionType;

			if ( requestedCollectionTypes.Contains( collectionType ) ) {
				ProcessRegulogCollections( collectionType );
			}
			else {
				requestedCollectionTypes.Add( collectionType );

				DataLayer.Instance.RequestRegulogCollections( collectionType, delegate( RegulogCollectionInfo[] collectionSummaries ) {
					this.collectionSummaries[collectionType] = collectionSummaries;
					ProcessRegulogCollections( collectionType );
				} );
			}
		}

		private void ProcessRegulogCollections ( RegulogCollectionType newCollectionType ) {
			RegulogCollectionInfo [] newCollections = collectionSummaries[newCollectionType];

			if ( newCollections == currentCollections ) return;

			// remove listeners from regulogCollections.
			if ( dataGrid.ItemsSource != null ) {
				foreach ( RegulogCollectionInfo item in currentCollections ) {
					item.PropertyChanged -= regulogCollection_PropertyChanged;
				}
			}

			dataGrid.ItemsSource = new ObservableList( newCollections );

			// Add listeners to new regulon.
			foreach ( RegulogCollectionInfo item in newCollections ) {
				item.PropertyChanged += regulogCollection_PropertyChanged;
			}

			currentCollections = newCollections;
		}

		/// <summary> Listen for property changes on RegulogCollectionSummaryItems.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>

		private void regulogCollection_PropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			if ( args.Includes( "IsSelected" ) ) return;

			RegulogCollectionInfo summaryItem = (RegulogCollectionInfo) sender;

			if ( summaryItem.IsSelected ) {
				if ( selectedCollection != null && selectedCollection != summaryItem ) {
					selectedCollection.IsSelected = false;
				}

				SelectedCollection = summaryItem;
			}
		}

		/// <summary> Listen for property changes on RegulogCollectionSummaryItems.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>

		private void regulog_PropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			if ( ! args.Includes( "IsSelected" ) ) return;

			RegulogInfo regulogInfo = (RegulogInfo) sender;

			if ( regulogInfo.IsSelected ) {
				if ( selectedRegulog != null ) {
					selectedRegulog.IsSelected = false;
				}

				selectedRegulog = regulogInfo;
			}
		}

		/// <summary> Get or set the current selected Collection.
		/// </summary>

		public RegulogCollectionInfo SelectedCollection {
			get {
				return selectedCollection;
			}
			set {
				if ( value == selectedCollection ) return;

				selectedCollection = value;
				selectedCollection.IsSelected = true;

				NotifyPropertyChanged( "SelectedCollection" );
			}
		}

		/// <summary> Get a reference to the enclosed data grid for data binding between elements.
		/// </summary>

		public DataGrid DataGrid {
			get { return this.dataGrid; }
		}

		/// <summary> Gets the name of the current regulon type.
		/// </summary>

		public string CollectionType {
			get { return collectionType.ToString(); }
		}

        /// <summary> When the search button is cliecked we apply the curernt filter
		///		(as a plain regexp) to the datagrid to reduce the number of displayed rows.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="eventArgs"></param>

		void searchButton_Clicked ( object sender, ElementEventArgs eventArgs ) {
			// TODO: replace this with an onchange listener.

			string searchText = searchField.Text;

			if ( string.IsNullOrWhiteSpace( searchText ) ) {
				dataGrid.Filter = null;
			}
			else {
				try {
					RegExp regexp = new RegExp( searchText, "i" );
					dataGrid.Filter = delegate( object item ) {
						RegulogCollectionInfo collectionSummary = (RegulogCollectionInfo) item;
						if (collectionSummary.ClassName.Match( regexp ) != null) {
                            return true;
                        }
                        if (collectionSummary.Name.Match( regexp ) != null) {
                            return true;
                        }
                        return false;
					};
				}
				catch ( Exception ex ) {
					Window.Alert( string.Format( "searchButton_Clicked: error {0}: {1}", ex.Message, ex.StackTrace ) );
				}
			}
		}

        /// <summary>
        /// Allows the filter text box to search on pressing enter
        /// </summary>
        /// <param name="e"></param>
        private void searchField_KeyUp(ElementEvent e)
        {
            if ((bool)Script.Literal("e.key == 'Enter'")) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("User pressed enter in the filter text box");
#endif*/
                searchButton_Clicked(searchField, null);
            }
        }
	}
}
