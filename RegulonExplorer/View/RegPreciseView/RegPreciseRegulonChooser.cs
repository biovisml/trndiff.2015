using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.ViewModel;
using SystemQut.Controls;
using RegPrecise;
using SystemQut.ComponentModel;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.RegPreciseView {

	/// <summary> Code behind for the "Browse RegPrecise -> Genomes" content frame.
	/// <para>
	///		This codeBehind is not intended to be used dynamically. It must have a non-null domElement
	///		with the required fields present.
	/// </para>
	/// </summary>

	public class RegPreciseRegulonChooser : AppContent {
		private readonly List<GenomeInfo> requestedGenomes = new List<GenomeInfo>();

		private DataGrid dataGrid;
		private RecordNavigator pager;
		private GenomeInfo currentGenome;
		private Binding pagerCurrentBinding;
		private Binding pagerMaxBinding;
		private Binding pagerMinBinding;
		private  RegPreciseGenomeChooser genomeChooser;
		private  DataGrid sourceDataGrid;

		private TextBox searchField;
		private Button searchButton;

        private ToolButton activatingButton;

		public RegPreciseRegulonChooser ( Element domElement )
			: base( domElement ) {

			if ( domElement == null ) {
				throw new Exception( Constants.DOM_ELEMENT_MAY_NOT_BE_NULL );
			}

			FindChildControls();
			BindChildControls();

			ActivationChanged += RegPreciseGenomeChooser_ActivationChanged;

			DataContext = this;

			// for development/debug purposes.
			if ( IsActivated ) RefreshRegulogs();

			searchButton.Clicked += searchButton_Clicked;
            searchField.DomElement.AddEventListener("keyup", searchField_KeyUp, false);

#if DEBUG
            CodeBehind.ForallControlsOfType( typeof( ToolButton ), Document.Body, delegate( ToolButton toolButton ) {
                if (toolButton.ActivatedId == "RegPreciseRegulonChooser") {
                    activatingButton = toolButton;
                }
            } );
#endif
		}

		private void BindChildControls () {
			pagerMinBinding = new Binding(
				this, "MinCollectionIndex",
				pager, "Min",
				DataBindingMode.OneWay
			);

			pagerMaxBinding = new Binding(
				this, "MaxCollectionIndex",
				pager, "Max",
				DataBindingMode.OneWay
			);

			pager.Formatter = Formatter;
		}

		private string Formatter ( string format, int current, int min, int max ) {
			if ( Script.IsValue( current ) && Script.IsValue( min ) && Script.IsValue( max ) ) {
				GenomeInfo selectedCollection = Script.IsValue( sourceDataGrid ) ? (GenomeInfo) sourceDataGrid.ItemAt( current ) : null;

				return ( Script.IsNullOrUndefined( selectedCollection ) )
					? string.Empty
					: string.Format( "{0} (Genome {1} of {2})", selectedCollection.Name, current + 1, max + 1 );
			}
			else {
				return "No genome has been selected.";
			}
		}

		private void FindChildControls () {
			dataGrid = (DataGrid) FindControlByNameAndType( "DataGrid", typeof( DataGrid ) );
			pager = (RecordNavigator) FindControlByNameAndType( "Pager", typeof( RecordNavigator ) );
			searchField = (TextBox) FindControlByNameAndType( "SearchText", typeof( TextBox ) );
			searchButton = (Button) FindControlByNameAndType( "SearchButton", typeof( Button ) );
		}

		/// <summary> Event handler for this.ActivationChanged.
		/// <para>
		///		Refreshes the regulon table.
		/// </para>
		/// </summary>
		/// <param name="activated"></param>

		private void RegPreciseGenomeChooser_ActivationChanged ( IActivated activated ) {
			if ( activated.IsActivated ) {
				RefreshRegulogs();

                // Renable the button that displays this view if it is disabled
                if (activatingButton.IsDisabled) {
                    activatingButton.IsDisabled = false;
                }
			}
		}

		private void RefreshRegulogs () {
			// Grab a copy of ccollectionType so we don't get odd effects in the event of overlapping asynchronous calls.
			GenomeInfo genome = this.currentGenome;

			if ( genome == null ) {
				dataGrid.ItemsSource = null;
			}
			else if ( this.requestedGenomes.Contains( genome ) ) {
				ProcessRegulons( genome );
			}
			else {
				requestedGenomes.Add( genome );
				genome.PropertyChanged += new PropertyChangedEventHandler( genome_PropertyChanged );
				DataLayer.Instance.RequestRegulonsInGenome( genome );
			}
		}

		void genome_PropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			if ( args.Includes( "Regulons" ) ) {
				ProcessRegulons( (GenomeInfo) sender );
			}
		}

		private void ProcessRegulons ( GenomeInfo genome ) {
			if ( genome != currentGenome || genome.Regulons == null ) return;

			dataGrid.ItemsSource = new ObservableList( genome.Regulons );
			currentGenome = genome;
		}

		void collectionChooser_PropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			if ( args.Includes( "ItemsSource" ) ) {
				NotifyPropertyChanged( "LabelText,MinCollectionIndex,MaxCollectionIndex" );
			}
			if ( args.Includes( "SelectedItem" ) ) {
				GenomeInfo genome = SelectedGenome;

				// no update is needed if the collection has not changed.
				if ( genome == currentGenome ) return;

				this.currentGenome = genome;

				if ( genome != null && genomeChooser.IsActivated ) {
					// Switch the view to this control when the user changes the selected
					// regulon in the regulon chooser. This will refresh the regulon
					// display as a side effect.
					IsActivated = true;
				}
				else if ( IsActivated ) {
					// The regulon datagrid will be refreshed when this control is activated.
					// So we only needto refresh if the control is already active.
					RefreshRegulogs();
				}

				// This is inexpensive, so just do it.
				NotifyPropertyChanged( "LabelText" );
			}
		}

		[ScriptName( "minCollectionIndex" )] // Unmangle generated name to allow databinding.
		private int MinCollectionIndex {
			get {
				ObservableList items = Script.IsValue( sourceDataGrid ) ? sourceDataGrid.ItemsSource : null;
				return Script.IsValue( items ) ? 0 : (int) Script.Undefined;
			}
		}

		[ScriptName( "maxCollectionIndex" )] // Unmangle generated name to allow databinding.
		private int MaxCollectionIndex {
			get {
				ObservableList items = Script.IsValue( sourceDataGrid ) ? sourceDataGrid.ItemsSource : null;
				return Script.IsValue( items ) ? items.Length - 1 : (int) Script.Undefined;
			}
		}

		[ScriptName( "labelText" )] // Unmangle generated name to allow databinding.
		private string LabelText {
			get {
				GenomeInfo item = SelectedGenome;
				return Script.IsValue( item ) ? String.Format( "Genome: {0}", item.Name ) : "Choose a genome to browse regulonRecords.";
			}
		}

		private GenomeInfo SelectedGenome {
			get {
				GenomeInfo item = Script.IsValue( sourceDataGrid ) ? (GenomeInfo) sourceDataGrid.SelectedItem : null;
				return item;
			}
		}

		public DataGrid DataGrid { get { return dataGrid; } }

		public RegPreciseGenomeChooser GenomeChooser {
			get {
				return this.genomeChooser;
			}
			set {
				if ( genomeChooser == value ) return;

				genomeChooser = value;
				sourceDataGrid = genomeChooser.DataGrid;
				sourceDataGrid.PropertyChanged += collectionChooser_PropertyChanged;

				pagerCurrentBinding = new Binding(
					sourceDataGrid, "SelectedIndex",
					pager, "Current",
					DataBindingMode.TwoWay
				);

				NotifyPropertyChanged( "LabelText,CollectionChooser,MinCollectionIndex,MaxCollectionIndex" );
			}
		}

        /// <summary> When the search button is cliecked we apply the curernt filter
		///		(as a plain regexp) to the datagrid to reduce the number of displayed rows.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="eventArgs"></param>

		void searchButton_Clicked ( object sender, ElementEventArgs eventArgs ) {
			// TODO: replace this with an onchange listener.

			string searchText = searchField.Text;

			if ( string.IsNullOrWhiteSpace( searchText ) ) {
				dataGrid.Filter = null;
			}
			else {
				try {
					RegExp regexp = new RegExp( searchText, "i" );
					dataGrid.Filter = delegate( object item ) {
						RegulonInfo regulonSummary = (RegulonInfo) item;
						if (regulonSummary.RegulatorName.Match( regexp ) != null) {
                            return true;
                        }
                        if (regulonSummary.RegulatorFamily.Match( regexp ) != null) {
                            return true;
                        }
                        if (regulonSummary.Effector.Match( regexp ) != null) {
                            return true;
                        }
                        if (regulonSummary.Pathway.Match( regexp ) != null) {
                            return true;
                        }
                        return false;
					};
				}
				catch ( Exception ex ) {
					Window.Alert( string.Format( "searchButton_Clicked: error {0}: {1}", ex.Message, ex.StackTrace ) );
				}
			}
		}

        /// <summary>
        /// Allows the filter text box to search on pressing enter
        /// </summary>
        /// <param name="e"></param>
        private void searchField_KeyUp(ElementEvent e)
        {
            if ((bool)Script.Literal("e.key == 'Enter'")) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("User pressed enter in the filter text box");
#endif*/
                searchButton_Clicked(searchField, null);
            }
        }
	}
}
