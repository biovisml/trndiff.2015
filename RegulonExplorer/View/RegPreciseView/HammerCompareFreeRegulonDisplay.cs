﻿using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.Common;
using SystemQut;
using SystemQut.Svg;
using Hammer;

namespace RegulonExplorer.View.RegPreciseView
{

    /// <summary>
    /// A variant of the Hammer regulon display that allows comparison of
    /// graphs. Can either compare two graphs and show all comparisons between
    /// them both, or show a single operation between many graphs, with the
    /// first graph assumed to be a reference
    /// </summary>
    class HammerCompareFreeRegulonDisplay : HammerFreeRegulonDisplay
    {
        
        private string[] labels = { "Off", "AND", "OR", "XOR" };        

		/// <summary> Displays the list of regulons in the specified container with a default of logical operation OR
		/// </summary>
		/// <param name="regulons"></param>
		/// <param name="container"></param>
		/// <param name="selectedRegulog"></param>
		/// <param name="nodeFactory"></param>
		/// <param name="colourChooser"></param>

		override public void DisplayRegulons (
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser
		) {
            DisplayRegulonsWithOperation(regulons, container, selectedRegulog, nodeFactory, colourChooser, LogicalOperations.OR);
		}

		/// <summary> Displays the list of regulons in the specified container, compared using the specified logical operation
		/// </summary>
		/// <param name="regulons"></param>
		/// <param name="container"></param>
		/// <param name="selectedRegulog"></param>
		/// <param name="nodeFactory"></param>
		/// <param name="colourChooser"></param>
		/// <param name="operation"></param>

		public void DisplayRegulonsWithOperation (
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser,
			LogicalOperations operation
		) {
            this.selectedRegulog = selectedRegulog;
            this.container = container;
            
            JSObject managerOptions = new JSObject();
            managerOptions["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
            managerOptions["domEvents"] = true; //http://hammerjs.github.io/tips/

            this.container.Style.Height = "auto";
            this.container.Style.Width = "auto";

			//remove all the children this regulon display has
			while ( container.ChildNodes[0] != null ) {
				container.RemoveChild( container.ChildNodes[0] );
			}

            List<RegulonInfo> comparedRegulons = GenerateComparisonRegulonList(regulons, operation);

			foreach ( RegulonInfo regulon in comparedRegulons ) {
				RegulonGraph regulonGraph = new RegulonGraph(
					selectedRegulog,
					regulon,
					nodeFactory,
					colourChooser,
					Constants.RegulonDisplayWidth,
					Constants.RegulonDisplayHeight,
					Constants.RegulonDisplayUnits
				);

                regulonGraph.DomElement.ID = "Compare graph " + comparedRegulons.IndexOf(regulon);

                // If there are more than two regulons, mark the first regulon
                // as the "reference"
                if (comparedRegulons.Length > 2 && comparedRegulons.IndexOf(regulon) == 0) {
                    if (Constants.TwoLineGraphLabels) {
					    regulonGraph.GraphLabelText = regulonGraph.GraphLabelText + " (reference)";
                    } else {
					    regulonGraph.GraphLabel.Text = regulonGraph.GraphLabel.Text + " (reference)";
                    }
                }

                foreach (GeneInfo geneInfo in regulonGraph.Regulon.Genes)
                {
                    ((SystemQut.ComponentModel.INotifyPropertyChanged)geneInfo).PropertyChanged += node_PropertyChanged;
                    if (geneInfo.IsSelected) {
                        selectedGenes.Add(geneInfo);
                    }
                }

                regulonGraph.GraphSvg.DomElement.Style.Border = "1px dashed #dddddd";
                regulonGraph.AddElementTo(container);
                regulonGraphs.Add(regulonGraph);
   
				HammerListeners[regulonGraph.DomElement.ID] = new Hammer.Hammer( regulonGraph.DomElement );
                
                // let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                HammerListeners[regulonGraph.DomElement.ID].Set(managerOptions);

                HammerListeners[regulonGraph.DomElement.ID].On( "panleft panright panup pandown", GraphOnPan );
                HammerListeners[regulonGraph.DomElement.ID].On( "doubletap", GraphOnTap );
                HammerListeners[regulonGraph.DomElement.ID].On( "panstart", GraphOnPanStart );
                HammerListeners[regulonGraph.DomElement.ID].On( "singletap", GraphOnPress );
                HammerListeners[regulonGraph.DomElement.ID].On( "panend", GraphOnPanEnd );
                HammerListeners[regulonGraph.DomElement.ID].On( "pinch", GraphOnPinch );

                JSObject pinchOptions = new JSObject();
                pinchOptions["enable"] = true;

                HammerListeners[regulonGraph.DomElement.ID].Get( "pinch" ).Set(pinchOptions);
            }
            
            // Graphs in the comparison are never sorted, so we need to do this here
            newDisplay = true;
		}

        /// <summary> Displays the list of regulons in the specified container, with each logical operation on the same display
		/// </summary>
		/// <param name="regulons"></param>
		/// <param name="container"></param>
		/// <param name="selectedRegulog"></param>
		/// <param name="nodeFactory"></param>
		/// <param name="colourChooser"></param>

	    public void DisplayRegulonsAllOperations (
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser
		) {
            this.selectedRegulog = selectedRegulog;
            this.container = container;
            
            JSObject managerOptions = new JSObject();
            managerOptions["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
            managerOptions["domEvents"] = true; //http://hammerjs.github.io/tips/

            this.container.Style.Height = "auto";
            this.container.Style.Width = "auto";
            
            //remove all the children this regulon display has
            while (container.ChildNodes[0] != null)
            {
                container.RemoveChild(container.ChildNodes[0]);
            }

            for (int i = (int)LogicalOperations.AND; i <= (int)LogicalOperations.XOR; i++) {
                RegulonInfo combinedRegulon = GenerateComparisonCombined(regulons, (LogicalOperations)i, selectedRegulog);
			
			    RegulonGraph regulonGraph = new RegulonGraph(
					selectedRegulog,
					combinedRegulon,
					nodeFactory,
					colourChooser,
					Constants.RegulonDisplayWidth,
					Constants.RegulonDisplayHeight,
					Constants.RegulonDisplayUnits
				);

                // Set the title of the graph based on the operation
                if (combinedRegulon.Genes[0] is GeneCompareInfo) {
                    switch ((combinedRegulon.Genes[0] as GeneCompareInfo).CurrentOperation) {
                        case LogicalOperations.AND:
                                if (Constants.TwoLineGraphLabels) {
					                regulonGraph.GraphLabelText = "AND";
                                } else {
								    regulonGraph.GraphLabel.Text = "AND";
                                }
								break;
							case LogicalOperations.OR:
								if (Constants.TwoLineGraphLabels) {
					                regulonGraph.GraphLabelText = "OR";
                                } else {
								    regulonGraph.GraphLabel.Text = "OR";
                                }
								break;
							case LogicalOperations.XOR:
								if (Constants.TwoLineGraphLabels) {
					                regulonGraph.GraphLabelText = "XOR";
                                } else {
								    regulonGraph.GraphLabel.Text = "XOR";
                                }
								break;
                        default:
#if DEBUG
                            Console.Log(regulonGraph.DomElement.ID + " has no operation type assigned to it for some reason!");
#endif
                            break;
                    }
                }
                
                foreach (GeneInfo geneInfo in regulonGraph.Regulon.Genes) {
                    ((SystemQut.ComponentModel.INotifyPropertyChanged)geneInfo).PropertyChanged += node_PropertyChanged;
                    if (geneInfo.IsSelected) {
                        selectedGenes.Add(geneInfo);
                    }
                }

                regulonGraph.GraphSvg.DomElement.Style.Border = "1px dashed #dddddd";
                regulonGraph.AddElementTo(container);
                regulonGraphs.Add(regulonGraph);
   
				HammerListeners[regulonGraph.DomElement.ID] = new Hammer.Hammer( regulonGraph.DomElement );
                
                // let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                HammerListeners[regulonGraph.DomElement.ID].Set(managerOptions);

                HammerListeners[regulonGraph.DomElement.ID].On( "panleft panright panup pandown", GraphOnPan );
                HammerListeners[regulonGraph.DomElement.ID].On( "doubletap", GraphOnTap );
                HammerListeners[regulonGraph.DomElement.ID].On( "panstart", GraphOnPanStart );
                HammerListeners[regulonGraph.DomElement.ID].On( "singletap", GraphOnPress );
                HammerListeners[regulonGraph.DomElement.ID].On( "panend", GraphOnPanEnd );
                HammerListeners[regulonGraph.DomElement.ID].On( "pinch", GraphOnPinch );

                JSObject pinchOptions = new JSObject();
                pinchOptions["enable"] = true;
                pinchOptions["threshold"] = 2;

                HammerListeners[regulonGraph.DomElement.ID].Get( "pinch" ).Set(pinchOptions);                
            }

            // Graphs in the comparison are never sorted, so we need to do this here
            newDisplay = true;
        }

        public void PositionGraphs()
        {
            
			foreach ( RegulonGraph graph in regulonGraphs ) {
				graph.RemoveElement();
                FindParentOfType( graph.DomElement, "span" ).Style.Position = String.Empty;
                FindParentOfType( graph.DomElement, "span" ).Style.Left = String.Empty;
                FindParentOfType( graph.DomElement, "span" ).Style.Top = String.Empty;

                //Insurance policy
                FindParentOfType( graph.DomElement, "span" ).Style.Position = "initial";
            }

			foreach ( RegulonGraph graph in regulonGraphs ) {
				graph.AddElementTo( container );
			}

            foreach (RegulonGraph graph in regulonGraphs)
            {

                HammerPositions[graph.DomElement.ID] = new double[] { FindParentOfType(graph.DomElement, "span").OffsetLeft, FindParentOfType(graph.DomElement, "span").OffsetTop };
                graph.Regulon.PositionLeft = HammerPositions[graph.DomElement.ID][0];
                graph.Regulon.PositionTop = HammerPositions[graph.DomElement.ID][1];
#if DEBUG
                Console.Log(graph.DomElement.ID + " origin set as " + HammerPositions[graph.DomElement.ID][0] + ", " + HammerPositions[graph.DomElement.ID][1]);
#endif
            }

            maxZLevel = 1;
            foreach (RegulonGraph graph in regulonGraphs)
            {
                FindParentOfType(graph.DomElement, "span").Style.Position = "absolute";
                FindParentOfType(graph.DomElement, "span").Style.Left = HammerPositions[graph.DomElement.ID][0] + "px";
                FindParentOfType(graph.DomElement, "span").Style.Top = HammerPositions[graph.DomElement.ID][1] + "px";
                FindParentOfType(graph.DomElement, "span").Style.ZIndex = maxZLevel;
                maxZLevel = (short)(maxZLevel + 1);
            }

            newDisplay = false;
        }

        private static List<RegulonInfo> GenerateComparisonRegulonList(List<RegulonInfo> regulons, LogicalOperations operation)
        {   
			List<RegulonInfo> comparedRegulons = new List<RegulonInfo>();

            //If there are more than two regulons, the first regulon will be
            //treated as a "reference"
			if ( regulons.Length > 1 ) {
#if DEBUG
                Console.Log("Setting up comparisons...");
                if (regulons.Length > 2) {
                    Console.Log("Network 1 (reference network): " + regulons[0].GenomeName);
                }
#endif
                GeneCompareInfo[] temp1 = new GeneCompareInfo[regulons[0].Genes.Length];
                for (int i = 0; i < regulons[0].Genes.Length; i++)
                {
                    temp1[i] = new GeneCompareInfo(regulons[0].Genes[i].Gene);
                    temp1[i].Regulator = regulons[0].Genes[i].Regulator;
                    temp1[i].Site = regulons[0].Genes[i].Site;
                    temp1[i].GenePresentIn = PresentInStates.left;
#if DEBUG
                    //Console.Log(temp1[i].Gene.Name + " is present in " + regulons[0].GenomeName);
#endif
                    numberOfHomologGenes[temp1[i].Name] = 1;
                    if (temp1[i].Site != null)
                    {
                        temp1[i].RegulationPresentIn = PresentInStates.left;
#if DEBUG
                        //Console.Log(temp1[i].Gene.Name + " is also regulated in " + regulons[0].GenomeName);
#endif
                    numberOfHomologRegulations[temp1[i].Name] = 1;
                    }
                    temp1[i].CurrentOperation = operation;
                }

                //For the rest of the networks, compare them to the first and
                //colour them accordingly
                for (int i = 1; i < regulons.Length; i++)
                {
#if DEBUG
                    //Console.Log("Network " + (i + 1) + ": " + regulons[i].GenomeName);
#endif
                    GeneCompareInfo[] temp2 = new GeneCompareInfo[regulons[i].Genes.Length];
                    for (int j = 0; j < regulons[i].Genes.Length; j++)
                    {
                        temp2[j] = new GeneCompareInfo(regulons[i].Genes[j].Gene);
                        temp2[j].Regulator = regulons[i].Genes[j].Regulator;
                        temp2[j].Site = regulons[i].Genes[j].Site;
                        temp2[j].GenePresentIn = PresentInStates.right;
#if DEBUG
                        //Console.Log(temp2[j].Gene.Name + " is present in " + regulons[i].GenomeName);
#endif
                        if (temp2[j].Site != null)
                        {
                            temp2[j].RegulationPresentIn = PresentInStates.right;
#if DEBUG
                            //Console.Log(temp2[j].Gene.Name + " is also regulated in " + regulons[i].GenomeName);
#endif
                        }
                        temp2[j].CurrentOperation = operation;
                    }

#if DEBUG
                    Console.Log("Determining which genes and regulations are in the reference and other networks...");
#endif
                    foreach (GeneCompareInfo gene1 in temp1)
                    {
                        foreach (GeneCompareInfo gene2 in temp2)
                        {
                            if (gene1.Name == gene2.Name)
                            {
#if DEBUG
                                //Console.Log(gene1.Gene.Name + " is present in both " + regulons[0].GenomeName + " and " + regulons[i].GenomeName);
#endif
                                gene1.GenePresentIn = PresentInStates.all;
                                gene2.GenePresentIn = PresentInStates.all;                                

                                numberOfHomologGenes[gene1.Name] = numberOfHomologGenes[gene1.Name] + 1;
                                if (gene1.Site != null && gene2.Site != null)
                                {
#if DEBUG
                                    //Console.Log(gene1.Gene.Name + " is also regulated in both " + regulons[0].GenomeName + " and " + regulons[i].GenomeName);
#endif
                                    gene1.RegulationPresentIn = PresentInStates.all;
                                    gene2.RegulationPresentIn = PresentInStates.all;
                                    
                                    numberOfHomologRegulations[gene1.Name] = numberOfHomologRegulations[gene1.Name] + 1;
                                }
                            }
                        }
                    }

                    RegulonInfo regulon2 = new RegulonInfo(regulons[i].Regulon);
                    regulon2.Genes = temp2;
                    regulon2.Genome = regulons[i].Genome;
                    regulon2.Regulators = regulons[i].Regulators;
                    comparedRegulons.Add(regulon2);
                }

                RegulonInfo regulon1 = new RegulonInfo(regulons[0].Regulon);
                regulon1.Genes = temp1;
                regulon1.Genome = regulons[0].Genome;
                regulon1.Regulators = regulons[0].Regulators;
                comparedRegulons.Insert(0, regulon1);
                
                foreach (KeyValuePair<string, int> pair in numberOfHomologGenes) {
                    foreach (RegulonInfo regulon in comparedRegulons) {
                        foreach (GeneCompareInfo gene in regulon.Genes) {
                            if (gene.Name == pair.Key) {
                                if (pair.Value < comparedRegulons.Length
                                    && gene.GenePresentIn == PresentInStates.all) {
                                    gene.GenePresentIn = PresentInStates.some;
#if DEBUG
                                    Console.Log(gene.Name + " gene in " + regulon.GenomeName + " set as a 'some'");
#endif
                                }
                                if (numberOfHomologRegulations.ContainsKey(pair.Key)
                                    && numberOfHomologRegulations[pair.Key] < comparedRegulons.Length
                                    && gene.RegulationPresentIn == PresentInStates.all) {
                                    gene.RegulationPresentIn = PresentInStates.some;
#if DEBUG
                                    Console.Log(gene.Name + " regulation in " + regulon.GenomeName + " set as a 'some'");
#endif
                                }
                                break;
                            }
                        }
                    }
                }
			}
			else {
#if DEBUG
                Console.Log("We tried to compare a collection with only one regulon. We probably want a special case for this.");
#endif
				comparedRegulons.Add( regulons[0] );
			}

            return comparedRegulons;
        }

        private static Dictionary<string, int> numberOfHomologGenes = new Dictionary<string,int>();
        private static Dictionary<string, int> numberOfHomologRegulations = new Dictionary<string,int>();

        private RegulonInfo GenerateComparisonCombined(List<RegulonInfo> regulons, LogicalOperations operation, RegulogInfo selectedRegulog) {
            RegulonInfo combinedRegulon;
            
            //If there are more than two regulons, the first regulon will be
            //treated as a "reference"
			if ( regulons.Length > 1 ) {
#if DEBUG
                Console.Log("Setting up comparisons...");
#endif

                List<string> names = new List<string>();
                Dictionary<string, int> geneList = new Dictionary<string, int>();

                foreach (GeneInfo gene in selectedRegulog.TargetGenes)
                {
                    // TODO: Allow alternate ortholog identification mechanism. One that actually works would be good.

                    // We have to assume that orthologs have the same name. Otherwise, there 
                    // appears to be no way to connect them in RegPrecise. However, this is
                    // probably an invalid assumption. nonetheless...

                    string name = gene.Name;

                    if (!names.Contains(name))
                    {
                        names.Add(name);
                        geneList[name] = selectedRegulog.TargetGenes.IndexOf(gene);
                    }
                }

                GeneCompareInfo[] combinedTemp = new GeneCompareInfo[names.Length];

                for (int i = 0; i < names.Length; i++) {
                    GeneInfo geneInfo = selectedRegulog.Genes[geneList[names[i]]];
                    combinedTemp[i] = new GeneCompareInfo(geneInfo.Gene);
                    combinedTemp[i].Regulator = geneInfo.Regulator;
                    combinedTemp[i].Site = null;
                    combinedTemp[i].CurrentOperation = operation;
                    combinedTemp[i].GenePresentIn = PresentInStates.none;
                    combinedTemp[i].RegulationPresentIn = PresentInStates.none;
                    
/*#if DEBUG
                    Console.Log("Network 1 (reference network): " + regulons[0].GenomeName);
#endif*/
                    for (int j = 0; j < regulons[0].Genes.Length; j++)
                    {
                        if (regulons[0].Genes[j].Name == combinedTemp[i].Name) {
                            combinedTemp[i].GenePresentIn = PresentInStates.left;
/*#if DEBUG
                            Console.Log(regulons[0].Genes[j].Name + " is present in " + regulons[0].GenomeName);
#endif*/
                            if (regulons[0].Genes[j].Site != null)
                            {
                                combinedTemp[i].Gene.VimssId = regulons[0].Genes[j].Site.GeneVimssid;
                                combinedTemp[i].Site = regulons[0].Genes[j].Site;
                                combinedTemp[i].RegulationPresentIn = PresentInStates.left;
/*#if DEBUG
                                Console.Log(regulons[0].Genes[j].Name + " is also regulated in " + regulons[0].GenomeName);
#endif*/
                            }
                            break;
                        }
                    }

                    //For the rest of the networks, compare them to the first and
                    //colour them accordingly
                    for (int j = 1; j < regulons.Length; j++)
                    {
/*#if DEBUG
                        Console.Log("Network " + (j + 1) + ": " + regulons[j].GenomeName);
#endif*/
                        for (int k = 0; k < regulons[j].Genes.Length; k++) {
/*#if DEBUG
                            Console.Log("Checking if " + regulons[j].Genes[k].Name + " equals " + combinedTemp[i].Name);
#endif*/
                            if (regulons[j].Genes[k].Name == combinedTemp[i].Name && !(combinedTemp[i].GenePresentIn == PresentInStates.all)) {
                                combinedTemp[i].GenePresentIn = combinedTemp[i].GenePresentIn == PresentInStates.left ? PresentInStates.all : PresentInStates.right;
/*#if DEBUG
                                Console.Log(regulons[j].Genes[k].Name + " is present in " + regulons[i].GenomeName);
#endif*/
                                if (regulons[j].Genes[k].Site != null && !(combinedTemp[i].RegulationPresentIn == PresentInStates.all))
                                {
                                    if (combinedTemp[i].Site == null) { 
                                        combinedTemp[i].Gene.VimssId = regulons[j].Genes[k].Site.GeneVimssid;
                                        combinedTemp[i].Site = regulons[j].Genes[k].Site;
                                    }
                                    combinedTemp[i].RegulationPresentIn = combinedTemp[i].RegulationPresentIn == PresentInStates.left ? PresentInStates.all : PresentInStates.right;
/*#if DEBUG
                                    Console.Log(regulons[j].Genes[k].Name + " is also regulated in " + regulons[i].GenomeName);
#endif*/
                                }
                                break;
                            }
                        }
                    }                 
                }
                combinedRegulon = new RegulonInfo(regulons[0].Regulon);
                combinedRegulon.Genes = combinedTemp;
                combinedRegulon.Genome = regulons[0].Genome;
                combinedRegulon.Regulators = regulons[0].Regulators;
			}
			else {
#if DEBUG
                Console.Log("We tried to compare a collection with only one regulon. We probably want a special case for this.");
#endif
				combinedRegulon = regulons[0];
			}

            return combinedRegulon;
        }
    }
}
