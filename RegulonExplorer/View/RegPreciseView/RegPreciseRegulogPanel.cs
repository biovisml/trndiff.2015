// RegPreciseRegulogPanel.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace RegulonExplorer.View.RegPreciseView {

	public class RegPreciseRegulogPanel : AppContentHolder {
		private RegPreciseCollectionChooser collectionChooser;
		private RegPreciseRegulogChooser regulogChooser;
        private RegPreciseRegulonGraphViewer regulogViewer;
        private RegPreciseRegulonGraphViewerSideBySide regulogViewerCompare;

		public RegPreciseRegulogPanel ( Element domElement )
			: base( domElement ) {

			FindChildControls();
			BindControls();
		}

		private void FindChildControls()
		{
 			collectionChooser = (RegPreciseCollectionChooser) FindControlByType( typeof(RegPreciseCollectionChooser) );
			regulogChooser= (RegPreciseRegulogChooser) FindControlByType( typeof(RegPreciseRegulogChooser) );
			regulogViewer = (RegPreciseRegulonGraphViewer) FindControlByType( typeof( RegPreciseRegulonGraphViewer ) );
			regulogViewerCompare = (RegPreciseRegulonGraphViewerSideBySide) FindControlByType( typeof( RegPreciseRegulonGraphViewerSideBySide ) );
		}

		private void BindControls()
		{
 			regulogChooser.CollectionChooser = collectionChooser;
            regulogViewer.SourceDataGrid = regulogChooser.DataGrid;
            regulogViewerCompare.SourceDataGrid = regulogChooser.DataGrid;
		}
	}
}
