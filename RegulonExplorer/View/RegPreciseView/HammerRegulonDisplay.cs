﻿using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.Common;
using SystemQut;
using SystemQut.Svg;
using Hammer;
#if DEBUG
    using SystemQut.Linq;
    using SystemQut.IO;
    using System.Html.Data.Files;
    using System.Net;
#endif
using SystemQut.ComponentModel;

namespace RegulonExplorer.View.RegPreciseView
{
    class HammerRegulonDisplay : IRegulonDisplay, INotifyPropertyChanged
    {

        // A list of the current centre positions of each graph, as well as
        // their listeners.
        // This setup is based on that used in the HammerJS homepage to store
        // the current location of the square in the example
        protected Dictionary<string, double[]> HammerPositions = new Dictionary<string, double[]>();
        protected Dictionary<string, Hammer.Hammer> HammerListeners = new Dictionary<string, Hammer.Hammer>();

        // Contains the Hammer listener for the primary container
        protected Hammer.Hammer containerHammer;

        // The currently selected regulons in the display, in order of selection
        // if possible
        protected List<int> selectedRegulons = new List<int>();

        /// <summary>
        /// Gets or sets the currently selected regulons in the display
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        public virtual List<RegulonInfo> SelectedRegulons
        {
            get {
                // Create a new list to store the regulon objects
                List<RegulonInfo> regulonInfos = new List<RegulonInfo>();

                // Go through all of the indexes in the list of selected
                // regulons and retrieve the regulon object that it points to
                foreach (int index in selectedRegulons) {

                    // Look through the list of graphs
                    foreach ( RegulonGraph graph in regulonGraphs ) {

                        // If this graph is showing the regulon that
                        // corresponds to the index, add that to the list
                        if ( graph.Regulon == selectedRegulog.Regulons[index] ) {
                            regulonInfos.Add(graph.Regulon);
                            break;
                        }
                    }
                }

                // Return the list of regulon objects
                return regulonInfos;
            }
// New set behaviour
            set {
                // Clear the current list of selected regulons
                selectedRegulons.Clear();

                // Look through the list of graphs
                foreach ( RegulonGraph graph in regulonGraphs ) {

                    // If the current regulon is contained in the supplied list
                    // of regulons, set it as selected
                    if (value.Contains(graph.Regulon)) {
                        graph.Regulon.IsSelected = true;

                        // Add the index of the regulon to the list of
                        // selected regulons
                        selectedRegulons.Add(selectedRegulog.Regulons.IndexOf(graph.Regulon));

                    // Otherwise ensure it is not selected
                    } else {
                        graph.Regulon.IsSelected = false;
                    }

                    // Ensure the graph has the correct class
                    SetGraphClass(graph, false);
                }
            }
        }

        // Contains the current distances between regulons and the current
        // centroids for each group
        private double[][] currentDistances;
        private int currentCentroid = -1;

        /// <summary>
        /// Gets or sets a matrix of all the distances between the networks
        /// <para>Allowing setting is primarily to let distances be transferred after changing the display manager</para>
        /// </summary>
        public double[][] CurrentDistances
        {
            get { return currentDistances; }
            set { currentDistances = value; }
        }

        /// <summary>
        /// Gets the network index for the current centroid
        /// </summary>
        public int CurrentCentroid
        {
            get { return currentCentroid; }
            set { currentCentroid = value;

                // Go through all of the graphs to ensure they have the right
                // appearance
                foreach (RegulonGraph graph in regulonGraphs) {

                    // Get the index of that graph's regulon in the list of
                    // regulon
                    int index = selectedRegulog.Regulons.IndexOf(graph.Regulon);

                    // If this graph is one of the current centroids, add it to the
                    // label
                    if (currentCentroid == index) {
                        if (graph.GraphLabelText.IndexOf(" " + Constants.Text_NetworkLabelCentroid) == -1) {
                            graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelCentroid;
                        }

                    // Otherwise, remove that from the label if it was an old
                    // centroid
                    } else {
                        graph.GraphLabelText = graph.GraphLabelText.Replace(" " + Constants.Text_NetworkLabelCentroid, "");
                    }

                    // Ensure this graph has the correct class
                    SetGraphClass(graph, false);
                }
            }
        }

        // The ratio of Euclidean and Hamming distances
        private double ratio = -1;

        /// <summary>
        /// Gets or sets the ratio of Euclidean and Hamming distances
        /// <para>0 = full Euclidean, 1 = full Hamming (default is 0.5)</para>
        /// </summary>
        public double Ratio
        {
            get { return ratio; }
            set { if (value == ratio) return;
                ratio = value;
                /*ratioChanged = true;*/ }
        }

        //Used to inform if the above has changed
        private bool ratioChanged = false;

        /// <summary>
        /// Gets or sets the status of the ratio changing
        /// </summary>
        public bool RatioChanged {
            get { return ratioChanged; }
            set { ratioChanged = value; }
        }

        /// <summary>
        /// Used to inform listeners if the selected genes are changed
        /// </summary>
        public event EventHandler SelectedGenesChanged;

        /// <summary>
        /// Displays the list of regulons in the specified container
		/// </summary>
		/// <param name="regulons">The list of regulons</param>
		/// <param name="container">The DOM object to act as the container</param>
		/// <param name="selectedRegulog">The currently selected regulog</param>
		/// <param name="nodeFactory">The node factory to use to create the nodes</param>
		/// <param name="colourChooser">The attribute factory to use to set the node attributes (mainly colour)</param>
        virtual public void DisplayRegulons(
            List<RegulonInfo> regulons,
            Element container,
            RegulogInfo selectedRegulog,
            INodeFactory nodeFactory,
            IAttributeFactory colourChooser
        ) {
            // Store the current regulog
            this.selectedRegulog = selectedRegulog;

            // Make the number of groups "1" if it is another value
            if (selectedRegulog.NumGroups != null && selectedRegulog.NumGroups != 1) {
                selectedRegulog.NumGroups = 1;
            }

            // Remove the resize listener if it exists already
            Window.RemoveEventListener("resize", Window_OnResize, false);

            // Remove the window click listener if it exists already
            Window.RemoveEventListener("click", Window_OnClick, false);

/*#if DEBUG
            Hammer.Hammer testHammer = new Hammer.Hammer(Document.Body);
            testHammer.On( "panleft panright panup pandown", OnPanTest);
#endif*/

            // Store the current container
            this.container = container;

            // If there is already a Hammer manager for this container, destroy
            // it
            if (containerHammer != null) {
                containerHammer.Destroy();
            }

            // Create a new Hammer manager for this container
            containerHammer = new Hammer.Hammer(container.ParentNode);

            // Sets the characteristics of the container's manager
            // let the pan gesture support all directions.
            // this will block the vertical scrolling on a touch-device while on the element
            JSObject managerOptions = new JSObject();
            managerOptions["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
            managerOptions["domEvents"] = true; //http://hammerjs.github.io/tips/
            containerHammer.Set(managerOptions);

            // Add a tap listener for the container's manager
            containerHammer.On( "tap" , Container_OnTap );

            // Set the attributes of the container
            this.container.Style.Height = "auto";
            this.container.Style.Width = "auto";

            //remove all the children this regulon display has
            while (container.ChildNodes[0] != null)
            {
                container.RemoveChild(container.ChildNodes[0]);
            }

            // Create a list for all the TG names
            List<string> geneNames = new List<string>();

            // Look through all the genes in the *regulog*. This is so
            // homologues can be handled correctly
            foreach (GeneInfo gene in selectedRegulog.TargetGenes)
            {
                // Add the TG's name to the list of names, if it is not already
                // in there
                string name = gene.Name;

                if (!geneNames.Contains(name))
                {
                    geneNames.Add(name);
                }
            }

            // For each name, add it in the dictionary, using the length of the
            // dictionary as its index
            geneDictionary.Clear();
            foreach (string name in geneNames) geneDictionary[name] = geneDictionary.Count;

            // Initialise the matrix of regulons and their gene locations
            geneIndexes = new int[regulons.Count][];
            for (int i = 0; i < regulons.Count; i++) {
                geneIndexes[i] = new int[geneDictionary.Count];
                for (int j = 0; j < geneIndexes[i].Length; j++) {
                    geneIndexes[i][j] = -1;
                }
            }

            // Try to load the distances between networks from a text file, if
            // they do not already exist or the ratio of Hamming to Euclidean
            // distance has changed
            if (currentDistances == null || ratioChanged == true) {
#if DEBUG
                try {
                    ReadAllText( "/RegulonExplorer/Text_Files/regulog" + selectedRegulog.RegulogId + "_distances_" + ratio.ToString() + ".txt", delegate( string text ) {
                        if ( !string.IsNullOrEmpty(text) ) {
                            if (Constants.ShowDebugMessages) Console.Log("loading distances from text file");
                            StringReader reader = new StringReader( text );

                            double[][] tempLoadedDistances = new double[selectedRegulog.Regulons.Length][];

                            for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                                string currentLine = reader.ReadLine();
                                string[] currentRegulonDistances = currentLine.Split(',');
                                tempLoadedDistances[i] = new double[selectedRegulog.Regulons.Length];
                                for (int j = 0; j < selectedRegulog.Regulons.Length; j++) {
                                    tempLoadedDistances[i][j] = double.Parse(currentRegulonDistances[j]);
                                }
                            }

                            this.currentDistances = tempLoadedDistances;
                        } else {
                            if (Constants.ShowDebugMessages) Console.Log("No file retrieved for  " + selectedRegulog.RegulatorName);

                            // If a file does not exist, calculate the distances now
                            this.currentDistances = CalculateDistances(regulons);
                        }
				    });
                } catch (Exception ex) {
                    if (Constants.ShowDebugMessages) Console.Log("Failed to load distances file for " + selectedRegulog.RegulatorName + " because:" + ex.Message);

                    // If a file couldn't be loaded, calculate the distances now
                    this.currentDistances = CalculateDistances(regulons);
				}
#else
                this.currentDistances = CalculateDistances(regulons);
#endif
                ratioChanged = false;
            }

            // Calculate the centroid of the current networks
            currentCentroid = CalculateCentroid(regulons);

            // Create and configure a graph for each regulon that will be
            // displayed
            foreach (RegulonInfo regulon in regulons)
            {
                RegulonGraph graph = new RegulonGraph(
                    selectedRegulog,
                    regulon,
                    nodeFactory,
                    colourChooser,
                    Constants.RegulonDisplayWidth,
                    Constants.RegulonDisplayHeight,
                    Constants.RegulonDisplayUnits
                );

                graph.DomElement.ID = regulon.GenomeName;

                // For each gene in the regulon
                foreach (GeneInfo geneInfo in graph.Regulon.TargetGenes)
                {
                    // Add a listerner for the gene's properties changing (used
                    // to detect selection)
                    //((SystemQut.ComponentModel.INotifyPropertyChanged)geneInfo).PropertyChanged += node_PropertyChanged;

                    // Add it to the list of selected genes if it is selected
                    if (geneInfo.IsSelected) {

                        // Get the index of the regulon and the gene, and
                        // combine them into a comma seperated string as the
                        // identifier
                        int regulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                        int geneIndex = geneDictionary[geneInfo.Name];
                        string temp = regulonIndex + "," + geneIndex;
                        selectedGenes.Add(temp);
                    }

                    // Add the index of the current gene into the matrix
                    geneIndexes[selectedRegulog.Regulons.IndexOf(graph.Regulon)][geneDictionary[geneInfo.Name]] = graph.Regulon.Genes.IndexOf(geneInfo);
                }

                // Add the centroid label if the current graph is a centroid
                if (regulons.IndexOf(regulon) == currentCentroid) {
                    graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelCentroid;
                }

                // Set the graph's attributes
                SetGraphClass(graph, false);

                // Add the graph to the container as well as the list of graphs
                graph.AddElementTo(container);
                regulonGraphs.Add(graph);

                // Set up a Hammer manager for this graph
				HammerListeners[graph.DomElement.ID] = new Hammer.Hammer( graph.DomElement );

                // Sets the characteristics of the graph's manager
                // let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                HammerListeners[graph.DomElement.ID].Set(managerOptions);

                // Add all the listeners to the graph's manager
                HammerListeners[graph.DomElement.ID].On( "panleft panright panup pandown", Graph_OnPan );
                HammerListeners[graph.DomElement.ID].On( "tap", Graph_OnTap );
                HammerListeners[graph.DomElement.ID].On( "panstart", Graph_OnPanStart );
                HammerListeners[graph.DomElement.ID].On( "panend", Graph_OnPanEnd );
                HammerListeners[graph.DomElement.ID].On( "pinch", Graph_OnPinch );

                // Set whether tooltips appear on a tap or a press
                if (Constants.ShowTooltipsOnHammerTap) {
                    HammerListeners[graph.DomElement.ID].On( "tap", GraphTooltipHandler );
                } else {
                    HammerListeners[graph.DomElement.ID].On( "press", GraphTooltipHandler );
                }
                HammerListeners[graph.DomElement.ID].On( "pancancel", Graph_OnPanEnd );
#if DEBUG
                HammerListeners[graph.DomElement.ID].On( "pinchstart", Graph_OnPinchStart );
                HammerListeners[graph.DomElement.ID].On( "pinchend", Graph_OnPinchEnd );
                HammerListeners[graph.DomElement.ID].On( "pinchcancel", Graph_OnPinchEnd );
#endif
                // Set whether TGs are selected on a tap or a press
                if (Constants.SelectGenesOnHammerTap) {
                    HammerListeners[graph.DomElement.ID].On( "tap", GraphGeneSelectHandler );
                } else {
                    HammerListeners[graph.DomElement.ID].On( "press", GraphGeneSelectHandler );
                }

                // Options for pinch for the graph's manager
                JSObject pinchOptions = new JSObject();
                pinchOptions["enable"] = true;
                pinchOptions["threshold"] = 2;

                HammerListeners[graph.DomElement.ID].Get( "pinch" ).Set(pinchOptions);

#if DEBUG
                // Set the regulon as selected if it is in the list of selected/
                // regulons
                if (selectedRegulons.Contains(regulon)) {
                    regulon.IsSelected = true;
                }
#endif

                // Add a listener for the regulon's properties changing (used
                // to detect selection)
                graph.Regulon.PropertyChanged += regulonGraph_PropertyChanged;
            }

            // Add a listener for when the browser window is resized
            Window.AddEventListener("resize", Window_OnResize, false);

            // Add a listener for when a click occurs in the browser window
            Window.AddEventListener("click", Window_OnClick, false);

            // Store the current window dimensions
            currentWindowWidth = Window.OuterWidth;
            currentWindowHeight = Window.OuterHeight;

            // If the window is larger than the threshold, set the threshold
            // as being passed (to allow size adjustment)
            if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                windowSizeThresholdPassed = true;
            }

            // Process selected genes
            OnSelectedGenesChanged();

            // Ensure the graphs have the right size and position of elements
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphZoomLevel(currentZoomLevel, i);
            }

            // Clear the status of passing the window size threshold
            windowSizeThresholdPassed = false;
/*#if DEBUG
            requireSelectionBugHandler = false;
            //checkForSelectionBug = true;
#endif*/
        }

        /// <summary> Removes all regulon graphs from the display and disposes them, leaving the
		///		display blank.
		/// </summary>

        virtual public void Clear()
        {

            // Hide the tooltip if it is present, and clear all selected
            // genes and regulons
            HideCurrentToolTip();
            ClearSelectedGenes(false);
            ClearSelectedRegulons();

            // Remove all the graphs from the display
            foreach ( RegulonGraph graph in regulonGraphs ) {

                // Destroy the Hammer manager that was watching this graph
                HammerListeners[graph.DomElement.ID].Destroy();

                // Remove the graph from the DOM
                graph.RemoveElement();

                // Properly dispose the graph
				graph.Dispose();
			}

            // Clear the list of regulon graphs
			regulonGraphs.Clear();

            // Clear the list of graphs in the detail view
            if (detailedViewGraphs != null && detailedViewGraphs.Length > 0) {
                detailedViewGraphs.Clear();
            }
		}

        // The list of all the graphs currently being dragged
        private List<RegulonGraph> draggedGraphs = new List<RegulonGraph>();

        // The starting location of the latest pan event
        private double[] panStart = { -1, -1 };

        /// <summary>
        /// Activates when a pan event is occuring on a graph
        /// <para>On a pan event, move the graph along with the pan by altering its absolute positioning in the container</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnPan(JSObject ev) {
#if DEBUG
            // Only proceed if a pinching action is not occuring
            if (!pinching) {
#endif
            Script.Literal("ev.preventDefault()");

            // On the Microsoft surface, we have some issues with weird delta
            // values. This bit will attempt to prevent odd results from that
            if (panStart[0] == -1) {
                panStart[0] = (double)(ev["center"] as JSObject)["x"];
                panStart[1] = (double)(ev["center"] as JSObject)["y"];
            }


            // If a drag goes outside the bounds of the window, stop the drag
            if (panStart[0] + (double)ev["deltaX"] < 0
                || panStart[0] + (double)ev["deltaY"] < 0
                || panStart[1] + (double)ev["deltaX"] > Window.OuterWidth
                || panStart[1] + (double)ev["deltaY"] > Window.OuterHeight) {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Pan would go outside the bounds of the page; cancelling the pan function.");
#endif

                // Force a drop if anything was being dragged
                if (draggedGraphs.Length > 0) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Attempting to drop all dragged graphs.");
#endif
                    Graph_OnPanEnd(ev);
                }
                return;
            }

            // Find the parent span of the dragged graph, to use it as the
            // target
            Element target = FindParentOfType( (Element)ev["target"], "span" );

#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("HammerOnPan triggered on " + target.ID);
            //Console.Log("HammerPositions[" + target.ID + "] = {" + HammerPositions[target.ID][0] + ", " + HammerPositions[target.ID][1] + "}");
            if (Constants.ShowDebugMessages) Console.Log("target.OffsetLeft = " + target.OffsetLeft + ", target.OffsetTop = " + target.OffsetTop);
#endif

            // for now, we block scrolling of the parent container so that we
            // don't get funny results if we accidentally scroll
            container.ParentNode.Style.Overflow = "hidden";

            // Create a new position storage for the target span if one does
            // not yet exist
            // This is used to keep track of the span's original position
            if (HammerPositions[target.ID] == null) {
                HammerPositions[target.ID] = new double[] { -1, -1 };
            }

            // Find the graph that is being dragged in the list of networks
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.DomElement.ID == target.ID) {

                    // Variables to store the current position of the graph as well as
                    // its height
                    double leftPosition = 0;
                    double topPosition = 0;
                    double graphHeight = graph.GraphSvg.DomElement.ClientHeight;

                    // Complete hack solution for Firefox since it always reports 0 for
                    // ClientHeight of SVG objects
                    // If the graph height is zero, find the *actual* graph height
                    if (graphHeight == 0) {

                        // Determine the default graph height (based on whether the
                        // window size is past the threshold)
                        int defaultGraphHeight = (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) ? 200 * Constants.LargeScaleNodeScaleFactor : 200;

                        // Get the actual height depending on the current zoom level
                        switch (currentZoomLevel)
                        {
                        case ZoomLevels.IconList:
                            graphHeight = defaultGraphHeight * (1/3);
                            break;
                        case ZoomLevels.SmallOverview:
                            graphHeight = defaultGraphHeight;
                            break;
                        case ZoomLevels.LargeOverview:
                            graphHeight = defaultGraphHeight * 1.5;
                            break;
                        case ZoomLevels.DetailView:
                            graphHeight = defaultGraphHeight * 3.75;
                            break;
                        default:
                            graphHeight = defaultGraphHeight;
                            break;
                        }
                    }
                    // End Firefox hack

                    // Script literals are used here since getBoundingClientRect()
                    // cannot be called here, only in JavaScript
                    // Get the bounding box of the graph and the container
                    Script.Literal("var boundingBoxGraph = target.getBoundingClientRect();");
                    Script.Literal("var boundingBoxContainer = this.container.getBoundingClientRect();");

                    // Use the two bounding boxes to determine the exact X and Y
                    // coordinates of the graph in the container
                    Script.Literal("leftPosition = boundingBoxGraph.left - boundingBoxContainer.left;");
                    Script.Literal("topPosition = (boundingBoxGraph.bottom - graphHeight) - boundingBoxContainer.top;");
                    Script.Literal("console.log('graphHeight = ' + graphHeight + '\\n boundingBoxGraph.left = ' + boundingBoxGraph.left + '\\n boundingBoxGraph.bottom = ' + boundingBoxGraph.bottom + '\\n boundingBoxContainer.left = ' + boundingBoxContainer.left + '\\n boundingBoxContainer.top = ' + boundingBoxContainer.top + '\\n leftPosition = ' + leftPosition + '\\n topPosition = ' + topPosition);");

                    // Round the X and Y values
                    leftPosition = Math.Round(leftPosition);
                    topPosition = Math.Round(topPosition);

                    // If the current location hasn't been stored yet, store the X and
                    // Y position from the above, as well as applying it as attributes
                    // to the graph, and making it absolute positioned
                    if (HammerPositions[target.ID][0] == -1) {
                        target.Style.Left = leftPosition + "px";
                        HammerPositions[target.ID][0] = leftPosition;
                        target.Style.Top = topPosition + "px";
                        HammerPositions[target.ID][1] = topPosition;
                        target.Style.Position = "absolute";
                    }

                    // Place the graph in the new location, based on adding the current
                    // distance panned to the stored distance. The distance is counted
                    // from the initial touch position.
                    // It is limited to a minimum of 0, 0, to prevent odd results by
                    // having a negative coordinate
                    target.Style.Left = Math.Max(HammerPositions[target.ID][0] + (double)ev["deltaX"], 0) + "px";
                    target.Style.Top = Math.Max(HammerPositions[target.ID][1] + (double)ev["deltaY"], 0) + "px";

                    // Set the graph's attributes
                    SetGraphClass(graph, true);

                    // Increase the graph's Z index so that appears above other
                    // graphs in the container
                    graph.GraphSvg.DomElement.Style.ZIndex = 5;

                    // If it is not already, put the graph in the list of
                    // dragged graphs
                    if (draggedGraphs.IndexOf(graph) == -1) {
                        draggedGraphs.Add(graph);
                    }
                    break;
                }
            }
#if DEBUG
            }
#endif
        }

/*#if DEBUG
        protected void OnPanTest(JSObject ev) {
            if (Constants.ShowDebugMessages) Console.Log("Current pan centre is " + (double)(ev["center"] as JSObject)["x"] + ", " + (double)(ev["center"] as JSObject)["y"]);
            if (Constants.ShowDebugMessages) Console.Log("Current pan delta is " + (double)ev["deltaX"] + ", " + (double)ev["deltaY"]);
        }
#endif*/

        /// <summary>
        /// Activates when a pan event ends on a graph
        /// <para>At the end of a panning event, place a dragged graph</para>
        /// <para>This is used since the "isFinal" attribute of the pan event does not always fire - it's mainly for internal HammerJS use</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected virtual void Graph_OnPanEnd(JSObject ev) {
#if DEBUG
            // Only proceed if a pinching action is not occuring
            if (!pinching) {
#endif
            Script.Literal("ev.preventDefault()");

            // Find the parent span of the dragged graph, to use it as the
            // target
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            //Console.Log("PanEnd triggered on " + target.ID);
            if (Constants.ShowDebugMessages) Console.Log("PanEnd (actual event is " + ev["type"] + ") triggered on " + target.ID);
#endif

            // Reset the overflow to auto to allow scrolling again
            container.ParentNode.Style.Overflow = "auto";

            // Store the new location
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("PanEnd - HammerPositions[" + target.ID + "] = {" + HammerPositions[target.ID][0] + ", " + HammerPositions[target.ID][1] + "}");
#endif*/

            // Get the cursor's X and Y coordinates
            double mouseX = (double)((JSObject)ev["center"])["x"];
            double mouseY = (double)((JSObject)ev["center"])["y"];
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Mouse location is " + mouseX + ", " + mouseY);
#endif
            // The current index of the graph that is closest to the mouse X
            // and Y
            int closestGraphIndex = -1;

            // The current closest distance between a non-dragged graph and the
            // mouse X and Y
            double closestDistance = double.MaxValue;

            //For every network in the container, check its actual distance
            //from the mouse cursor's position
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Determining the closest graph to the drop position");
#endif
            // For each graph in the list of graphs...
            for (int i = 0; i < regulonGraphs.Length; i++) {

                // If the current graph is not one of the dragged graphs...
                if (draggedGraphs.IndexOf(regulonGraphs[i]) == -1) {

                    //Calculate the centre of the current network
                    int graphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientWidth)/2;
                    int graphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientHeight)/2;

                    //Determine the distance between the current centre and the
                    //mouse's position using Pythagoras
                    double currentDistance = Math.Sqrt(Math.Pow(mouseX - graphX, 2) + Math.Pow(mouseY - graphY, 2));
    #if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[i].Regulon.Genome.Name + " centre is " + currentDistance + " away.");
    #endif

                    //If the calculated distance is less than the current closest,
                    //this network is the new closest
                    if (currentDistance < closestDistance) {
                        closestDistance = currentDistance;
                        closestGraphIndex = i;
    #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[i].Regulon.Genome.Name + " is the new closest.");
    #endif
                    }
                } else {
                    if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[i].Regulon.Genome.Name + " is currently being dragged - don't count it!");
                }
            }

            //If we found a closest network, place the dragged network in front
            //of the closest one (in essence, putting it in its place)
            if (closestGraphIndex > -1) {

                //However, if it's:
                //- Closest to the last network in the container (if the container
                //is not empty), and the drop point was to the right, or completely
                //underneath, or
                //- To the right and below the top left corner of the last
                //  network
                //      - put the dropped network last
                //- To the left and below the top left corner of the bottom
                //  left network
                //      - put the dropped network in place of that network

                // Get the X and Y coordinates of the "last" graph in the
                // container
                Element lastGraphElement = container.ChildNodes[container.ChildNodes.Length - 1];
                int lastGraphX = HtmlUtil.GetElementX(lastGraphElement);
                int lastGraphY = HtmlUtil.GetElementY(lastGraphElement);

                //Find the network that is at the bottom left
                int bottomLeftGraphIndex = -1;
                int bottomLeftGraphX = int.MaxValue;

                // For each graph in the list of graphs
                for (int i = 0; i < regulonGraphs.Length; i++) {

                    // Get that graph's X and Y coordinates
                    int currentGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                    int currentGraphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement);

                    // If this graph is on the same Y value as the last graph,
                    // and its X coordinate is less than the X coordinate of
                    // the current "bottom left" network, store it and its
                    // X coordinate
                    if (currentGraphY == lastGraphY && currentGraphX < bottomLeftGraphX) {
                        bottomLeftGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                        bottomLeftGraphIndex = i;
                    }
                }

                // If there is a "last" graph and the mouse X and Y is greater
                // than that graph's X and Y, put the dragged graph at the end
                // of the container
                if (lastGraphX != int.MinValue && ((mouseX > lastGraphX) && (mouseY > lastGraphY))) {
                    container.AppendChild(target);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed at the end of the container");
#endif
                // Else, if there is a "bottom left" graph and the mouse X is
                // less than its X coordinate, and more than its Y cooridnate,
                // put the dragged graph in front of that graph
                } else if (bottomLeftGraphX != int.MaxValue && ((mouseX < bottomLeftGraphX) && (mouseY > lastGraphY))) {
                    container.InsertBefore(target, regulonGraphs[bottomLeftGraphIndex].DomElement);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed in front of " + regulonGraphs[bottomLeftGraphIndex].Regulon.Genome.Name);
#endif
                // Else, put the graph in front of the graph that it is closest
                // to
                } else {
                    container.InsertBefore(target, regulonGraphs[closestGraphIndex].DomElement);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed in front of " + regulonGraphs[closestGraphIndex].Regulon.Genome.Name);
#endif
                }
            }

            //Else append it to the end of the container - this should only
            //occur if the container is empty
            else {
                container.AppendChild(target);
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("A closest graph could not be found, appending the dropped object to the end of the container.");
#endif
            }

            // Reset the initial location of this graph
            if (HammerPositions[target.ID] != null) {
                HammerPositions[target.ID][0] = -1;
                HammerPositions[target.ID][1] = -1;
            } else {
                if (Constants.ShowDebugMessages) Console.Log("HammerPositions[target.ID] was null for some reason");
            }

            // Return the position attribute to "initial" so that the graph
            // uses HTML flow again and clear its left and top attributes
            target.Style.Position = "initial";
            target.Style.Left = string.Empty;
            target.Style.Top = string.Empty;

            // Find the dragged graph in the list of graphs
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.DomElement.ID == target.ID) {

                    // Ensure that the dragged graph is returned to its proper
                    // class
                    SetGraphClass(graph, false);

                    // Return it to the default Z level
                    graph.DomElement.Style.ZIndex = 0;

                    // Remove it from the list of dragged graphs
                    draggedGraphs.Remove(graph);
                    break;
                }
            }

            // For Surface fix
            panStart[0] = -1;
            panStart[1] = -1;

#if DEBUG
            // Note that a pan action is no longer being performed
            panning = false;
            }
#endif
            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();
        }

        /// <summary>
        /// Activates when a tap event is detected on a graph
        /// <para>When a tap event occurs, hide tooltips and select networks if they were tapped on</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnTap(JSObject ev) {
            Script.Literal("ev.preventDefault()");

/*#if DEBUG
            Script.Literal("console.log(ev.type + ' triggered (Graph_OnTap)')");
#endif*/

            // Don't select anything if the user clicked in a tooltip
            if (toolTipNode != null) {
                // Get the cursor's X and Y coordinates
                int mouseX = (int)((JSObject)ev["center"])["x"];
                int mouseY = (int)((JSObject)ev["center"])["y"];

                // If the cursor is within the tooltip, hide it and do nothing
                // else
                if (toolTipNode.IsWithinToolTipExternal(mouseX, mouseY)) {
                    HideCurrentToolTip();
                    return;

                // Else hide it and continue
                } else {
                    HideCurrentToolTip();
                }
            }

/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Tap target is a " + (ev["target"] as Element).TagName);
#endif*/
                // Find the parent span of the target, to use it as the actual target
                Element target = FindParentOfType( (Element)ev["target"], "span" );
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Tap triggered on " + target.ID);
#endif*/
            // If the target is not a circle or path (which would make it a
            // node)...
            if ((String.Compare( (ev["target"] as Element).TagName, "circle", true )) != 0
                && (String.Compare( (ev["target"] as Element).TagName, "path", true )) != 0) {

                // Find the graph that was clicked on from the list of graphs...
                foreach (RegulonGraph graph in regulonGraphs) {
                    if (target == FindParentOfType(graph.DomElement, "span")) {

                        // Toggle the selected status of the graph
                        graph.IsSelected = !graph.IsSelected;
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(graph.Regulon.GenomeName + " has had its selection changed!");
#endif*/
                        // Update the class of the graph
                        SetGraphClass(graph, false);

                        // Do nothing else here
                        break;
                    }
                }

            // Else if the tap happened on a node
            } /*else {
//#if DEBUG
//                if (Constants.ShowDebugMessages) Console.Log("Tap was triggered on a non background element and so we don't want to select the graph.");
//#endif
                // Whether the node that was tapped on was found, to quit out
                // of the loops below
                bool foundNode = false;

                // Go through all the graphs in the list of graphs...
                foreach (RegulonGraph graph in regulonGraphs)
                {
                    // If the graph where the click occured in is found, go
                    // through all of the nodes in that graph
                    if (target == FindParentOfType(graph.DomElement, "span")) {
                        foreach (AbstractNode node in graph.Nodes) {

                            // If the correct node is found...
                            if (node.Node.DomElement == (ev["target"] as Element)) {

                                // Toggle the selected status of the node
                                node.IsSelected = !node.IsSelected;

                                // Process the change in selection status
                                if (node.Gene is GeneInfo) {
                                    GeneSelectionChanged((node.Gene as GeneInfo));
                                }

                                // Inform that the node was found and do
                                // nothing else
                                foundNode = true;
                                break;
                            }
                        }
                    }

                    // Stop looping if the node was found
                    if (foundNode) { break; }
                }
            }*/
        }

        /// <summary>
        /// Activates when a Hammer pinch event is detected on a graph
        /// <para>On pinching in or out, change the zoom level</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnPinch(JSObject ev)
        {
#if DEBUG
            if (!panning) {
#endif
            Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip
            HideCurrentToolTip();

            // Find the parent span of the target, to use it as the actual target
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Pinch triggered on " + target.ID);
#endif
            // Get the scale value that was generated by the pinch
            double scale = (double)ev["scale"];

            // If pinching inwards and not already at the "Icon list", decrease
            // the zoom level
            if (scale < 1 && currentZoomLevel > ZoomLevels.IconList) {
                CurrentZoomLevel = currentZoomLevel - 1;
            }

            // If pinching outwards and not already at the "Detail view",
            // increase the zoom level
            if (scale > 1 && currentZoomLevel < ZoomLevels.DetailView) {
                CurrentZoomLevel = currentZoomLevel + 1;
            }

            // Notify listeners that the zoom level has changed
            NotifyPropertyChanged("ZoomLevel");
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Zoom level changed to " + currentZoomLevel);
#endif
#if DEBUG
            }
#endif
        }

        /// <summary>
        /// Activates when a Hammer pan event starts on a graph
        /// <para>At the start of a panning event, put the graph on the top layer</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnPanStart(JSObject ev) {
#if DEBUG
            // Only proceed if a pinching action is not occuring
            if (!pinching) {
                // Note that a panning event is being performed
                panning = true;
#endif
            Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip
            HideCurrentToolTip();

            // Find the parent span of the target, to use it as the actual target
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("PanStart triggered on " + target.ID);
#endif
            // for now, we block scrolling of the parent container so that we
            // don't get funny results if we accidentally scroll
            // container.ParentNode.Style.Overflow = "hidden";
#if DEBUG
            }
#endif
        }

        /// <summary>
        /// Stores the node that is currently showing a tooltip, so it can be
        /// cleared later on
        /// </summary>
        private AbstractNode toolTipNode;

        /// <summary>
        /// Shows tooltips on clicking on nodes in a network.
        /// <para>Can be on either tap events or press events, depending on the value of ShowTooltipsOnHammerTap (default is true)</para>
        /// <para>This function was  originally "GraphOnPress"</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void GraphTooltipHandler(JSObject ev)
        {
            Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip
            HideCurrentToolTip();

            // Check through all the graphs in the display
            foreach (RegulonGraph graph in regulonGraphs) {

                // Check through all the target gene nodes in the graph
                foreach (AbstractNode node in graph.Nodes) {

                    // If this node's DOM element sent the event, show the
                    // tooltip
                    if (node.Node.DomElement == ev["target"]) {

                        // Get the coordinates of the cursor
                        int mouseX = (int)((JSObject)ev["center"])["x"];
                        int mouseY = (int)((JSObject)ev["center"])["y"];

                        // Show the tooltip at that location and store the
                        // current node so that the tooltip can be cleared
                        // later
                        node.ShowToolTipExternal( mouseX, mouseY );
                        toolTipNode = node;
                        break;
                    }
                }

                // Check through all the regulator nodes in the graph
                foreach (AbstractNode node in graph.RegulatorNodes) {

                    // If this node's DOM element sent the event, show the
                    // tooltip
                    if (node.Node.DomElement == ev["target"]) {

                        // Get the coordinates of the cursor
                        int mouseX = (int)((JSObject)ev["center"])["x"];
                        int mouseY = (int)((JSObject)ev["center"])["y"];

                        // Show the tooltip at that location and store the
                        // current node so that the tooltip can be cleared
                        // later
                        node.ShowToolTipExternal( mouseX, mouseY );
                        toolTipNode = node;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Activates when a tap event is detected on the container
        /// <para>When tapping the container, hide any existing tooltips</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Container_OnTap(JSObject ev)
        {
            Script.Literal("ev.preventDefault()");
/*#if DEBUG
            Script.Literal("console.log(ev.type + ' triggered (Container_OnTap)')");
#endif*/
            // Hide any existing tooltip
            if ((Element)ev["target"] == container) {
            //if (!(ev["target"] is SvgElement)) {
                HideCurrentToolTip();
            }
        }

        /// <summary>
        /// Hides any tooltip currently visible in the display
        /// <para>Can be called from the outside so that tooltips do not persist, for example, when the display manager is changed</para>
        /// </summary>
        public void HideCurrentToolTip()
        {
            if (toolTipNode != null)
            {
                toolTipNode.HideToolTipExternal();
                toolTipNode = null;
            }
        }

#if DEBUG
        // Used to check whether a pinching or panning action is occuring so
        // that the application does not try to do both at the same time
        protected bool pinching = false;
        protected bool panning = false;

        /// <summary>
        /// Listens for when a pinching event starts
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnPinchStart(JSObject ev)
        {
            // Only proceed if a panning action is not occuring
            if (!panning) {
                Script.Literal("ev.preventDefault()");
    //#if DEBUG
    //            Script.Literal("console.log(ev.type + ' triggered (Graph_OnPinchStart)')");
    //#endif
                // Note that a pinch action is being performed
                pinching = true;
            }
        }

        /// <summary>
        /// Listens for when a pinching event ends
        /// </summary>
        /// <param name="ev"></param>
        protected void Graph_OnPinchEnd(JSObject ev)
        {
            // Only proceed if a panning action is not occuring
            if (!panning) {
                Script.Literal("ev.preventDefault()");

    //#if DEBUG
    //            Script.Literal("console.log(ev.type + ' triggered (Graph_OnPinchEnd)')");
    //#endif
                // Note that a pinch action is no longer being performed
                pinching = false;
            }
        }
#endif
        /// <summary>
        /// (De)selects TGs on clicking/tapping nodes in a network.
        /// <para>Can be on either tap or press events, depending on the value of SelectGenesOnHammerTap (default is false)</para>
        /// <para>This was originally handled in "Graph_OnTap"</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void GraphGeneSelectHandler(JSObject ev)
        {
            // Don't select anything if the user clicked in a tooltip
            if (toolTipNode != null) {
                // Get the cursor's X and Y coordinates
                int mouseX = (int)((JSObject)ev["center"])["x"];
                int mouseY = (int)((JSObject)ev["center"])["y"];

                // If the cursor is within the tooltip, do nothing
                // else
                if (toolTipNode.IsWithinToolTipExternal(mouseX, mouseY)) {
                    return;
                }
            }

            // Find the parent span of the target, to use it as the actual target
            Element target = FindParentOfType( (Element)ev["target"], "span" );

            if ((String.Compare( (ev["target"] as Element).TagName, "circle", true )) == 0
                || (String.Compare( (ev["target"] as Element).TagName, "path", true )) == 0) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Tap was triggered on a non background element and so we don't want to select the graph.");
#endif*/
                // Whether the node that was tapped on was found, to quit out
                // of the loops below
                bool foundNode = false;

                // Go through all the graphs in the list of graphs...
                foreach (RegulonGraph graph in regulonGraphs)
                {
                    // If the graph where the click occured in is found, go
                    // through all of the nodes in that graph
                    if (target == FindParentOfType(graph.DomElement, "span")) {
                        foreach (AbstractNode node in graph.Nodes) {

                            // If the correct node is found...
                            if (node.Node.DomElement == (ev["target"] as Element)) {

                                // Toggle the selected status of the node
                                node.IsSelected = !node.IsSelected;

                                // Process the change in selection status
                                if (node.Gene is GeneInfo) {
                                    GeneSelectionChanged((node.Gene as GeneInfo));
                                }

                                // Inform that the node was found and do
                                // nothing else
                                foundNode = true;
                                break;
                            }
                        }
                    }

                    // Stop looping if the node was found
                    if (foundNode) { break; }
                }
            }
        }

        // --------------------------------------------------------------------
        // Things copied from DefaultRegulonDisplay.cs
        // --------------------------------------------------------------------

		/// <summary> The container that holds these graphs.
		/// </summary>
		protected Element container;

		/// <summary> Maintain a list of GUI elements which correspond to the data model.
		/// </summary>
		protected List<RegulonGraph> regulonGraphs = new List<RegulonGraph>();

        // The list of currently selected genes
        // Stores genes as a combination of the regulon index and the gene
        // index in a string, seperated by a comma
        // In the comparison, the regulon *graph* index is used instead
        protected List<string> selectedGenes = new List<string>();

        /// <summary>
        /// Gets or sets the list of selected genes
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        public virtual List<GeneInfo> SelectedGenes
        {
            get {
                // Create a new list to store the gene objects
                List<GeneInfo> geneInfos = new List<GeneInfo>();

                // Go through each string identifier in selected genes and
                // retrieve the gene object that it refers to, adding it to the
                // list of gene objects
                foreach (string index in selectedGenes) {
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Look through the list of graphs
                    foreach ( RegulonGraph graph in regulonGraphs ) {

                        // If this graph is showing the regulon that
                        // corresponds to the index, add the gene object that's
                        // selected to the list (since a gene must exist to be
                        // selected, geneIndexes doesn't need checking for a
                        // "-1")
                        if (graph.Regulon == selectedRegulog.Regulons[regulonIndex]) {
                            geneInfos.Add(graph.Regulon.Genes[geneIndexes[regulonIndex][geneIndex]]);
                            break;
                        }
                    }
                }

                // Return the list of gene objects
                return geneInfos;
            }
// New set behaviour
            set {
                // Clear the current list of selected genes
                selectedGenes.Clear();

                // Look through the list of graphs
                foreach ( RegulonGraph graph in regulonGraphs ) {
                    foreach ( GeneInfo gene in graph.Regulon.Genes ) {

                        // If the current gene is inside the supplied list of
                        // genes
                        if (value.Contains(gene)) {

                            // Get the index of the regulon and the gene, and
                            // combine them into a comma seperated string as the
                            // identifier
                            int regulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                            int geneIndex = geneDictionary[gene.Name];
                            string temp = regulonIndex + "," + geneIndex;

                            // Set the gene object as selected
                            gene.IsSelected = true;

                            // Add the identifier to the list of selected genes
                            selectedGenes.Add(temp);

                        // Otherwise ensure the gene is not selected
                        } else {
                            gene.IsSelected = false;
                        }
                    }
                }
                OnSelectedGenesChanged();
            }
        }

        // The current zoom level of the display
        protected ZoomLevels currentZoomLevel = ZoomLevels.SmallOverview;

        /// <summary>
        /// Gets or sets the current zoom level of the display
        /// <para>The setter was originaly a seperate method called "ChangeZoomLevel"</para>
        /// </summary>
        public ZoomLevels CurrentZoomLevel
        {
            get { return currentZoomLevel; }
            set {
                // Don't do anything if the passed zoom level is the same
                if (currentZoomLevel == value) return;

                // Special filtering if the detail view is intended to only display
                // the selected networks
                if (Constants.DetailedViewSelected) {

                    // If the new zoom level is the detail view, check if the
                    // display needs filtering
                    if (value == ZoomLevels.DetailView) {
                        detailedViewGraphs = new List<RegulonGraph>();

                        // Add all selected networks to a list
                        foreach (RegulonGraph graph in regulonGraphs) {
                            if (graph.Regulon.IsSelected) {
                                detailedViewGraphs.Add(graph);
                            }
                        }

                        // If there are selected networks, filter the display
                        if (detailedViewGraphs.Length > 0) {

                            // Note that the display is not showing all graphs
                            // in the detail view
                            detailViewAllGraphs = false;

                            //DetailView_FilterGraphs();
                            UpdateVisibleRegulons(null, delegate ( RegulonInfo regulon ) { return regulon.IsSelected; } );
                        }

                    // Else if changing out of the detail view, make sure all
                    // networks are visible again
                    } else {
                        if (!detailViewAllGraphs) {
                            //DetailView_RestoreGraphs();

                            // Return this to the default
                            detailViewAllGraphs = true;

                            UpdateVisibleRegulons(null, null);
                        }
                    }
                }

                // Set the zoom level for all graphs in the display
                currentZoomLevel = value;
                for (int i = 0; i < regulonGraphs.Length; i++) {
                    SetGraphZoomLevel(value, i);
                }

                // Clear the status of passing the window size threshold if that's
                // why this method was called
                windowSizeThresholdPassed = false;
            }
        }

        /// <summary>
        /// Stores the number of graphs that are currently visible in the display
        /// <para>This is set manually by some functions</para>
        /// </summary>
        private int numberOfGraphs = -1;

        /// <summary>
        /// Returns the number of graphs in the display
        /// <para>How graphs are counted for this is dependant on the invidiual regulon display classes</para>
        /// </summary>
        public int NumberOfGraphs {
            get {
                if (numberOfGraphs == -1) {
                    return regulonGraphs.Length;
                } else {
                    return numberOfGraphs;
                }
            }
        }

		/// <summary> Updates the visible regulons in the associated container,
		///		applying a filter and sorting function to them
        /// </summary>
        /// <param name="sort">Function for determining the method of sorting regulons</param>
        /// <param name="filter">Function for determining which regulons should be filtered</param>
		public virtual void UpdateVisibleRegulons(
			Func<RegulonInfo, RegulonInfo, int> sort,
			Func<RegulonInfo, bool> filter
		) {
            // If no sorting function was specified, sort using the already
            // stored order
            if (sort == null) {
                sort = delegate( RegulonInfo x, RegulonInfo y ) { return x.Order - y.Order; };
            }

            if (detailedViewGraphs == null) {
                detailedViewGraphs = new List<RegulonGraph>();
            }

            detailedViewGraphs.Clear();

            // The list of regulons that will be displayed
			List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

            // The regulons that will not be visible (to set their order
            // property correctly)
			List<RegulonGraph> invisibleGraphs = new List<RegulonGraph>();

            // Go through all the regulon graphs and check them against the
            // filter
			for ( int i = 0; i < regulonGraphs.Count; i++ ) {
				RegulonGraph graph = regulonGraphs[i];

                // Only add the regulon to the list if it meets the conditions
                // of the filter
				if ( filter == null || filter( graph.Regulon ) ) {
					visibleGraphs.Add( graph );
                    if (!detailViewAllGraphs) detailedViewGraphs.Add( graph );
				} else {
                    invisibleGraphs.Add( graph );
                }
			}

            // Sort the visible regulons based on the sorting function
			visibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return sort( x.Regulon, y.Regulon ); } );

            // Remove the existing regulon graphs from the display
			foreach ( RegulonGraph graph in regulonGraphs ) {
				graph.RemoveElement();
			}

            // Add the regulon graphs that will be visible to the display
			foreach ( RegulonGraph graph in visibleGraphs ) {
				graph.AddElementTo( container );
			}

            // Set the visible number of graphs
            numberOfGraphs = visibleGraphs.Length;

            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();

            // If there are any invisible regulons...
            if (invisibleGraphs.Length > 1) {
                // Sort the invisible regulons based on their already stored order
                invisibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return x.Regulon.Order - y.Regulon.Order; } );

                // Set the invisible regulons' order to be their current order, but
                // to be after the visible regulons
                for (int i = 0; i < invisibleGraphs.Length; i++) {
                    invisibleGraphs[i].Regulon.Order = i + visibleGraphs.Length;
                }
            }
		}

        private List<string> selectedHomologs = new List<string>();

        // These store the locations of genes inside the arrays in each regulon
        // for easier access
        protected int[][] geneIndexes;
        protected Dictionary<string, int> geneDictionary = new Dictionary<string,int>();
        protected RegulogInfo selectedRegulog;

        /// <summary>
        /// Triggered when a gene's selection is changed. Attempts to select homologs of that gene
        /// </summary>
        /// <param name="gene">The gene whose selection was changed</param>
        protected virtual void GeneSelectionChanged(GeneInfo gene) {

            // Don't do anything if a null gene was passed
            if (gene == null) {
                return;
            }

            // Notify listeners that this display is currently
            // processing so the loading overlay can be displayed
            isProcessing = true;
            NotifyPropertyChanged("IsProcessing");

            // Use a timeout delegate so that the loading overlay can appear
            Script.SetTimeout((Action) delegate {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Selecting genes - starting with " + selectedGenes.Length + " in selectedGenes list");
#endif

                // Get the current selected status of the sending target gene
                bool selected = gene.IsSelected;
                string currentGeneName = gene.Name;

                // Get the index of the regulon and the gene, and
                // combine them into a comma seperated string as the
                // identifier
                int regulonIndex = -1;
                foreach (RegulonInfo regulon in selectedRegulog.Regulons) {
                    if (regulon.RegulonId == gene.RegulonId) {
                        regulonIndex = selectedRegulog.Regulons.IndexOf(regulon);
                        break;
                    }
                }
                int geneIndex = geneDictionary[currentGeneName];
                string temp = regulonIndex + "," + geneIndex;

                // Add or remove the gene from the list of selected genes based
                // on its selection status
                if (selected) {
                    if (!selectedGenes.Contains(temp)) {
#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Adding " + currentGeneName + " (from RegulonID " + gene.RegulonId + ") to selected list");
#endif
                        selectedGenes.Add(temp);
                    }
                } else {
                    if (selectedGenes.Contains(temp)) {
#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Removing " + currentGeneName + " (from RegulonID " + gene.RegulonId + ") from selected list");
#endif
                        selectedGenes.Remove(temp);
                    }
                }

                // Don't try to select homologs with the dictionary if it is a
                // comparison (probably not needed as that class overrides it)
                if (!(this is HammerCompareRegulonDisplay)) {

                    // For all the regulons in the display
                    foreach (RegulonGraph graph in regulonGraphs)
                    {

                        // Look through all regulons to find homologs to select
                        // using the dictionary of gene locations
                        int newRegulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                        int newGeneIndex = geneDictionary[currentGeneName];

                        // If a homolog exists for the current regulon, select
                        // or deselect it
                        if (geneIndexes[newRegulonIndex][newGeneIndex] != -1) {
                            GeneInfo homolog = graph.Regulon.Genes[geneIndexes[newRegulonIndex][newGeneIndex]];
                            if (homolog != null && homolog != gene &&
                                homolog.IsSelected != selected) {
                                homolog.IsSelected = selected;

                                // Combine this gene's regulon and gene index to make a
                                // identifier
                                string newTemp = newRegulonIndex + "," + newGeneIndex;

                                // Add or remove from the list as required
                                if (selected) {
                                    if (!selectedGenes.Contains(newTemp)) {
                                        selectedGenes.Add(newTemp);
    #if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log("Adding homolog " + homolog.Name + " (from RegulonID " + homolog.RegulonId + ") to selected list");
    #endif
                                    }
                                } else {
                                    if (selectedGenes.Contains(newTemp)) {
    #if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log("Removing homolog " + homolog.Name + " (from RegulonID " + homolog.RegulonId + ") to selected list");
    #endif
                                        selectedGenes.Remove(newTemp);
                                    }
                                }
#if DEBUG
                                // Use the bug selection handler if required
                                if (requireSelectionBugHandler) {
                                    if (Constants.ShowDebugMessages) Console.Log(String.Format("Activating selection bug handler for gene {0} in RegulonID {1}", homolog.Name, homolog.RegulonId));
                                    SelectionBugHandler(homolog);
                                }
#endif
                            }
                        }
                    }
                }

                // Additional processing on change of gene selections
                OnSelectedGenesChanged();
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Finished - " + selectedGenes.Length + " genes now in selectedGenes list");
#endif
/*#if DEBUG
                // At present, there are currently some issues with homologue
                // selection in DisplayManagers created from
                // RegPreciseRegulonGraphViewer2. This is here to attempt to detect
                // the issue and allow the selection to work properly
                if (checkForSelectionBug && selectedGenes.Length > 1) {

                    foreach (GeneInfo selectedGene in SelectedGenes) {

                        // Keep track of when a desync in gene objects is found
                        bool desyncFound = false;

                        // Go through each graph in the display
                        foreach (RegulonGraph graph in regulonGraphs) {

                            // For each target gene in the graph...
                            foreach (GeneNode node in graph.Nodes) {

                                // If the important identifiers are the same,
                                // assume that these are the same gene and check
                                // if the actual objects are the same
                                if (node.Gene.RegulonId == selectedGene.RegulonId && node.Gene.VimssId == selectedGene.VimssId && node.Gene.Name == selectedGene.Name && node.Gene.LocusTag == selectedGene.LocusTag) {

                                    if (node.Gene != selectedGene) {
                                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Homolog selection is not working properly - activating bug handler (desync on {0} in {1})", node.Gene.Name, graph.GraphLabelText));
                                        requireSelectionBugHandler = true;

                                        // Mark that a desync was found
                                        desyncFound = true;
                                        break;
                                    } //else {
                                        //Console.Log(String.Format("Homolog selection is working properly (first target gene in the data set is the same object as {0} in {1})", node.Gene.Name, graph.GraphLabelText));
                                        //requireSelectionBugHandler = false;
                                    //}
                                }
                            }

                            // Don't look at any more graphs if a desync
                            // was found
                            if (desyncFound) {
                                break;
                            }

                        }

                        // Don't look at any more selected genes if a desync
                        // was found
                        if (desyncFound) {
                            break;
                        }
                    }

                    checkForSelectionBug = false;

                    foreach (GeneInfo selectedGene in SelectedGenes) {
                        SelectionBugHandler(selectedGene);
                    }
                }
#endif*/

                // Notify listeners that this display has finished processing
                // so the loading overlay can be hidden
                isProcessing = false;
                NotifyPropertyChanged("IsProcessing");
            }, 10, null);
        }

        /// <summary>
        /// Runs when a property on a regulon graph is changed. Used for checking if regulon graphs have been selected
        /// - in that case, adds or removes that regulon graph from the list of selected regulons
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="args">Event arguments</param>
        protected virtual void regulonGraph_PropertyChanged(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs args)
        {

            // If a regulon graph's selection has been changed
            if (args.Includes("IsSelected")) {

                RegulonInfo regulon = (sender as RegulonInfo);
                int index = selectedRegulog.Regulons.IndexOf(regulon);

                // Add or remove the regulon graph from the list of selected
                // regulons based on its selection status
                if (regulon.IsSelected) {
                    if (!selectedRegulons.Contains(index)) {
                        selectedRegulons.Add(index);
                    }
                } else {
                    selectedRegulons.Remove(index);
                }
            }
        }

        /// <summary>
        /// Deselects all of the selected genes in the display
        /// </summary>
        /// <param name="allowOverlay">Whether to use a time out to allow the loading overlay to appear</param>
        public virtual void ClearSelectedGenes(bool allowOverlay) {

            if (selectedGenes.Length > 0) {

            if (allowOverlay) {

            // Notify listeners that this display is currently
            // processing so the loading overlay can be displayed
            isProcessing = true;
            NotifyPropertyChanged("IsProcessing");

            // Use a timeout delegate so that the loading overlay can appear
            Script.SetTimeout((Action) delegate {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Clearing all selected genes - starting with " + selectedGenes.Length + " in selectedGenes list");
#endif
                // Clear the selected status of each gene in the selected list
                // and then clear the list
                foreach (string index in selectedGenes) {

                    // Get the index of the regulon and the gene by splitting
                    // the identifier in the selected list
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Set the gene that corresponds to the retrieved indexes
                    // as unselected
                    if (selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]] != null) {
                        selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = false;
                    }
#if DEBUG
                    // Use the bug selection handler if required
                    if (requireSelectionBugHandler) {
                        GeneInfo currentGene = selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]];
                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Activating selection bug handler for gene {0} in RegulonID {1}", currentGene.Name, currentGene.RegulonId));
                        SelectionBugHandler(currentGene);
                    }
#endif
                }

                // Clear the list of selected genes
                selectedGenes.Clear();
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Finished - " + selectedGenes.Length + " genes now in selectedGenes list");
#endif

                // Notify listeners that this display has finished processing
                // so the loading overlay can be hidden
                isProcessing = false;
                NotifyPropertyChanged("IsProcessing");
            }, 10, null);

            } else {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Clearing all selected genes - starting with " + selectedGenes.Length + " in selectedGenes list");
#endif
                // Clear the selected status of each gene in the selected list
                // and then clear the list
                foreach (string index in selectedGenes) {

                    // Get the index of the regulon and the gene by splitting
                    // the identifier in the selected list
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Set the gene that corresponds to the retrieved indexes
                    // as unselected
                    if (selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]] != null) {
                        selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = false;
                    }
#if DEBUG
                    // Use the bug selection handler if required
                    if (requireSelectionBugHandler) {
                        GeneInfo currentGene = selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]];
                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Activating selection bug handler for gene {0} in RegulonID {1}", currentGene.Name, currentGene.RegulonId));
                        SelectionBugHandler(currentGene);
                    }
#endif
                }

                // Clear the list of selected genes
                selectedGenes.Clear();
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Finished - " + selectedGenes.Length + " genes now in selectedGenes list");
#endif
            }

            }
        }

        /// <summary>
        /// Deselects all of the selected regulons in the display
        /// </summary>
        public void ClearSelectedRegulons() {

            // Go through all the regulon graphs and set them as not selected
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.IsSelected) {
                    graph.IsSelected = false;

                    // Ensure the graph has the correct visual class
                    SetGraphClass(graph, false);
                }
            }

            // Clear the list of selected regulons
            selectedRegulons.Clear();
        }

        /// <summary>
        /// Finds a parent of the specified type for the given element
        /// </summary>
        /// <param name="element">The element to find a parent for</param>
        /// <param name="tagName">The tag name to look for</param>
        /// <returns>The parent element that fits the specified type, or null if none was found</returns>
		protected static Element FindParentOfType ( Element element, string tagName ) {

            // While the element is not null, and does not have the same tag
            // name as was requested...
			while ( element != null && String.Compare( element.TagName, tagName, true ) != 0 ) {

                // Set the current element to its parent
				element = element.ParentNode;
			}

            // Return the parent element (or null if no suitable parent was
            // found)
			return element;
		}

        /// <summary>
        /// Ensures that elements of the graphs do not change size when the
        /// display is zoomed in or out
        /// </summary>
        /// <param name="graph">The graph that is changing size</param>
        /// <param name="newWidth">The new width of the graph</param>
        /// <param name="newHeight">The new height of the graph</param>
        protected void SetGraphSize(RegulonGraph graph, double newWidth, double newHeight)
        {

            // Calculate the factor that will be used to change the size
            double factor = newHeight / Double.Parse(graph.GraphSvg.GetAttribute<string>("height"));

            // The window size threshold indicates when graph elements should
            // gain additional size when the display is big enough, or vice versa
            if (windowSizeThresholdPassed) {
                if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                    factor = factor / Constants.LargeScaleNodeScaleFactor;
                } else {
                    factor = Constants.LargeScaleNodeScaleFactor * factor;
                }
            }

            // Set the graph's SVG container to the new width and height
            graph.GraphSvg.SetAttribute("width", newWidth + Constants.RegulonDisplayUnits);
            graph.GraphSvg.SetAttribute("height", newHeight + Constants.RegulonDisplayUnits);

            // For every node in the graph, change their size by the calculated
            // factor
            foreach (AbstractNode node in graph.Nodes) {

                // Each different shape of node requires different handling
                // For circular nodes
                if (node.Node is SvgCircle) {
                    (node.Node as SvgCircle).Radius = (node.Node as SvgCircle).Radius / factor;
                }

                // For rectangular nodes
                if (node.Node is SvgRectangle) {
                    SvgRectangle rectangle = (node.Node as SvgRectangle);
                    rectangle.Height = rectangle.Height / factor;
                    rectangle.Width = rectangle.Width / factor;
                    rectangle.X = rectangle.X / factor;
                    rectangle.Y = rectangle.Y / factor;
                }

                // For polygonal nodes
                if (node.Node is SvgPolygon) {
                    SvgPolygon polygon = (node.Node as SvgPolygon);
                    polygon.Height = polygon.Height / factor;
                    polygon.Width = polygon.Width / factor;
                }

                // Scale the width of lines/spokes for target genes
                if (node is GeneNode) {

                    // See above comment for window size threshold
                    if (windowSizeThresholdPassed) {
                        if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                            (node as GeneNode).Link.StrokeWidth = (node as GeneNode).Link.StrokeWidth / (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                        } else {
                            (node as GeneNode).Link.StrokeWidth = (node as GeneNode).Link.StrokeWidth * (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                        }
                    }
                }

                // See above comment for window size threshold
                if (windowSizeThresholdPassed) {
                    if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                        node.Node.StrokeWidth = node.Node.StrokeWidth / (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                    } else {
                        node.Node.StrokeWidth = node.Node.StrokeWidth * (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                    }
                }

                /*if (!string.IsNullOrEmpty(node.Node.GetAttribute<string>("class"))) {
                    if (currentZoomLevel == ZoomLevels.LargeOverview || currentZoomLevel == ZoomLevels.DetailView) {
                        if (node.Node.GetAttribute<string>("class").IndexOf("GeneDefaultLarge") == -1) {
                            node.Node.SetAttribute("class", node.Node.GetAttribute<string>("class").Replace("GeneDefault", "GeneDefaultLarge"));
                        }
                    } else {
                        if (node.Node.GetAttribute<string>("class").IndexOf("GeneDefaultLarge") > -1) {
                            node.Node.SetAttribute("class", node.Node.GetAttribute<string>("class").Replace("GeneDefaultLarge", "GeneDefault"));
                        }
                    }
                }*/
            }

            // Scale each regulator in the graph as well by the calculated
            // factor
            foreach (RegulatorNode node in graph.RegulatorNodes) {

                // Each different shape of node requires different handling
                // For circular nodes
                if (node.Node is SvgCircle) {
                    (node.Node as SvgCircle).Radius = (node.Node as SvgCircle).Radius / factor;
                }

                // For rectangular nodes
                if (node.Node is SvgRectangle) {
                    SvgRectangle rectangle = (node.Node as SvgRectangle);
                    rectangle.Height = rectangle.Height / factor;
                    rectangle.Width = rectangle.Width / factor;
                    rectangle.X = rectangle.X / factor;
                    rectangle.Y = rectangle.Y / factor;
                }

                // For polygonal nodes
                if (node.Node is SvgPolygon) {
                    SvgPolygon polygon = (node.Node as SvgPolygon);
                    polygon.Height = polygon.Height / factor;
                    polygon.Width = polygon.Width / factor;
                }
            }
        }

        /// <summary>
        /// Whether the detail view should be showing all graphs (true) or a filtered set of graphs based on the selection (false)
        /// </summary>
        private bool detailViewAllGraphs = true;

        /// <summary>
        /// Used to store the current order of all the graphs in the display
        /// while a filtered detail view is visible, so that they can be
        /// restored when switching out of that zoom level
        /// </summary>
        private int[] graphOrder;

        /// <summary>
        /// A list of the graphs that are currently visible in a filtered detail view
        /// </summary>
        private List<RegulonGraph> detailedViewGraphs;

        /// <summary>
        /// Gets the list of graphs that are currently visible in the detail view
        /// <para>This is used to set the comparison when in a filtered detail view and none of the regulons are currently selected</para>
        /// </summary>
        public List<RegulonGraph> DetailedViewGraphs
        {
            get { return detailedViewGraphs; }
        }

        /// <summary>
        /// Filters the display to show only selected graphs, while preserving the original order
        /// </summary>
        /*private void DetailView_FilterGraphs()
        {
            // Used to keep track of the un-filtered graph
            // order
            graphOrder = new int[container.ChildNodes.Length];
            Element currentChild = container.FirstChild;
            int index = 0;

            // Check each graph in the display and store its
            // order
            while (currentChild != null)
            {
                foreach (RegulonGraph graph in regulonGraphs)
                {
                    if (currentChild == graph.DomElement)
                    {
                        graphOrder[index] = regulonGraphs.IndexOf(graph);
                        break;
                    }
                    else
                    {
                        graphOrder[index] = -1;
                    }
                }
                currentChild = currentChild.NextSibling;
                index++;
            }

            // Remove all current regulon graphs from the
            // display
            foreach (RegulonGraph graph in regulonGraphs)
            {
                graph.RemoveElement();
            }

            // Add only the selected graphs back into the display
            foreach (RegulonGraph graph in detailedViewGraphs)
            {
                graph.AddElementTo(container);
            }
        }

        /// <summary>
        /// Undoes selected graph filtering, restoring all graphs to correct columns and order
        /// </summary>
        private void DetailView_RestoreGraphs()
        {
            // Remove all regulon graphs currently in the display
            foreach (RegulonGraph graph in regulonGraphs)
            {
                graph.RemoveElement();
            }

            // Make a list of all the graphs in the display in
            // their previous order
            List<RegulonGraph> orderedGraphs = new List<RegulonGraph>();
            foreach (int index in graphOrder)
            {
                if (index != -1)
                {
                    orderedGraphs.Add(regulonGraphs[index]);
                }
            }

            // Add all the networks in that list to the display
            foreach (RegulonGraph graph in orderedGraphs)
            {
                if (graph != null)
                {
                    graph.AddElementTo(container);
                }
            }

            // Clear the list of graphs in the detail view
            detailedViewGraphs.Clear();
        }*/

        /// <summary>
        /// Calls SetGraphAttributes for a graph when the zoom level is changed, depending on what zoom level was specified
        /// </summary>
        /// <param name="zoomLevel">The zoom level to change to, using the ZoomLevels enum</param>
        /// <param name="graphIndex">The index of the graph in the list of regulon graphs</param>
        virtual protected void SetGraphZoomLevel(ZoomLevels zoomLevel, int graphIndex)
        {
            switch (zoomLevel)
            {
                // Icon list specifies a box 150% the width of the default
                // small overview and 33% the height. The network is also moved
                // to the left of the box and the label to the right
                case ZoomLevels.IconList:
                    SetGraphAttributes(graphIndex, 1.5, (1/3), TextAnchorType.start, 0.75, 0, 100, new SvgRect(190, -100, 200, 200), false);
                    break;

                // Small overview is the default
                case ZoomLevels.SmallOverview:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;

                // Large overview is 150% larger than the default small
                // overview
                case ZoomLevels.LargeOverview:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(graphIndex, 1.5, 1.5, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1.5, 1.5, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;

                // Detail view is 375% larger than the default small
                // overview
                case ZoomLevels.DetailView:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(graphIndex, 3.75, 3.75, TextAnchorType.middle, 0.3, 120, 0, new SvgRect(-125, -108.75, 250, 250), true);
                    } else {
                        SetGraphAttributes(graphIndex, 3.75, 3.75, TextAnchorType.middle, 0.3, 100, 0, new SvgRect(-125, -125, 250, 250), true);
                    }
                    break;

                // If somehow an invalid zoom level is passed, use the default
                // small overview parameters
                default:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;
            }
        }

        /// <summary>
        /// Sets certain attributes of a graph
        /// </summary>
        /// <param name="graphIndex">The index of the graph in the list of regulon graphs</param>
        /// <param name="widthFactor">The multiplier that will be applied to the default graph width</param>
        /// <param name="heightFactor">The multiplier that will be applied to the default graph height</param>
        /// <param name="textAnchor">The alignment of the label text</param>
        /// <param name="textFactor">The multiplier that will be applied to the default font size of the label</param>
        /// <param name="labelY">The Y position of hte label</param>
        /// <param name="labelX">The X position of the label</param>
        /// <param name="newViewBox">The new SVG rectangle that with act as the graph's view box</param>
        /// <param name="nodeLabelVisibility">The visibility of the labels for each target gene node</param>
        protected virtual void SetGraphAttributes(int graphIndex, double widthFactor, double heightFactor, TextAnchorType textAnchor, double textFactor, double labelY, double labelX, SvgRect newViewBox, bool nodeLabelVisibility)
        {
            // Set the size of the graph's elements. This also applies an
            // additional multiplier to the width and height if the window size
            // is larger than a certain value
            if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                SetGraphSize(regulonGraphs[graphIndex], Constants.RegulonDisplayWidth * widthFactor * Constants.LargeScaleGraphScaleFactor, Constants.RegulonDisplayHeight * heightFactor * Constants.LargeScaleGraphScaleFactor);
            } else {
                SetGraphSize(regulonGraphs[graphIndex], Constants.RegulonDisplayWidth * widthFactor, Constants.RegulonDisplayHeight * heightFactor);
            }

            // Set the label's alignment
            regulonGraphs[graphIndex].GraphLabel.TextAnchor = textAnchor;

            // Set the label's font size
            if (Constants.UseGraphLabelWrapping) {

                // If the text in the label is too long, shrink the font size
                // depending on its length so that it is wholly visible
                int textLength = regulonGraphs[graphIndex].GraphLabel.Text.Length;
                regulonGraphs[graphIndex].GraphLabel.FontSize = textLength > 32 ? (Svg.ConvertLength(textFactor / 2.54 / (textLength / 32), Constants.RegulonDisplayHeight, 200)).ToString() : (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();
            } else {
                regulonGraphs[graphIndex].GraphLabel.FontSize = (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();
            }

            // Set the X and Y coordinates of the label
            regulonGraphs[graphIndex].GraphLabel.Y = labelY;
            regulonGraphs[graphIndex].GraphLabel.X = labelX;

            // Do the same for the second line of the label if label text
            // wrapping is enabled
            if (Constants.UseGraphLabelWrapping) {

                // Set the label's alignment
                regulonGraphs[graphIndex].GraphLabelLineTwo.TextAnchor = textAnchor;

                // Set the label's font size
                // If the text in the label is too long, shrink the font size
                // depending on its length so that it is wholly visible
                int textLength2 = regulonGraphs[graphIndex].GraphLabelLineTwo.Text.Length;
                regulonGraphs[graphIndex].GraphLabelLineTwo.FontSize = textLength2 > 32 ? (Svg.ConvertLength(textFactor / 2.54 / (textLength2 / 32), Constants.RegulonDisplayHeight, 200)).ToString() : (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();

                // Set the X and Y coordinates of the label
                regulonGraphs[graphIndex].GraphLabelLineTwo.Y = labelY;
                regulonGraphs[graphIndex].GraphLabelLineTwo.X = labelX;
            }

            // Set the graph's view box rectangle
            regulonGraphs[graphIndex].GraphSvg.ViewBox = newViewBox;

            // Set the graph's node label visibility
            regulonGraphs[graphIndex].SetNodeLabelVisibility(nodeLabelVisibility);

            // Set the position of the label's backgroud and its visibility (it
            // should not appear if the label is not centered - this is used
            // for the icon list)
            /*regulonGraphs[graphIndex].GraphLabelBackground.Y = labelY + (double.Parse(regulonGraphs[graphIndex].GraphLabel.FontSize) * 0.75);
            if (labelX != 0) {
                regulonGraphs[graphIndex].GraphLabelBackground.Visibility = "hidden";
            } else {
                regulonGraphs[graphIndex].GraphLabelBackground.Visibility = "visible";
            }*/
        }

        /// <summary>
        /// Calculates the distances between a given set of regulons
        /// <para>Uses the "Ratio" variable to determine the ratio of Hamming and Euclidean distance to use. 0 = full Euclidean, 1 = full Hamming (default is 0.5)</para>
        /// </summary>
        /// <param name="regulons">The regulons to calculate the distance between</param>
        /// <returns>A matrix (array of arrays) of distances for each regulon to every other regulon in the supplied list</returns>
        private double[][] CalculateDistances(List<RegulonInfo> regulons)
        {
            // Make sure the ratio value is not out of bounds
            if (ratio < 0) {
                ratio = 0;
            } else if (ratio > 1) {
                ratio = 1;
            }
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Alpha value is " + ratio + " (Hamming weight = " + ratio + ", Euclidean weight = " + (1 - ratio) + ")");
#endif*/
            // Get the total number of possible TGs
            int n = geneDictionary.Count;

            //Initialise the distances table
            double[][] distances = new double[regulons.Length][];

            // Set all of the distances to 0 by default
            for (int i = 0; i < regulons.Length; i++) {
                distances[i] = new double[regulons.Length];
                for (int j = 0; j < regulons.Length; j++) {
                    distances[i][j] = 0;
                }
            }

            //Calculate both the Hamming distance and Euclidean distance for
            //each
            for (int i = 0; i < regulons.Length; i++) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Creating Hamming and Euclidean distances for " + regulons[i].GenomeName);
#endif*/
                for (int j = 0; j < regulons.Length; j++) {
                    double iEuclidean = 0;

                    //If we're not comparing this network to itself...
                    if (i != j) {
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Creating Hamming distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName);
#endif*/
                        //Create two vectors to store the presence of genes in
                        //each network
                        double[] iVector = new double[n];
                        double[] jVector = new double[n];
                        int count = 0;

                        //For each gene in the regulog, check if it is present
                        //in both. For Hamming distance, regulation is not
                        //relevant.
                        foreach (KeyValuePair<string, int> geneName in geneDictionary) {
                            iVector[count] = 0;
                            jVector[count] = 0;
                            double iValue = 0;
                            double jValue = 0;

                            //If the gene is in the network, it'll match one of
                            //the names in the list. If so, set the value to 1
                            //for Hamming distance, and for Eucildean distance,
                            //set the value to 1 if there's regulation, or 0.75
                            //if there's not
                            foreach (GeneInfo regulonGene in regulons[i].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (ratio > 0) {
/*#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " - setting the entry in iVector to '1'");
#endif*/
                                        iVector[count] = 1;
                                    }
                                    if (ratio < 1) {
                                        if (regulonGene.Site != null) {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " and is regulated - setting iValue to '1'");
#endif*/
                                            iValue = 1;
                                        } else {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " and is not regulated - setting iValue to '0.75'");
#endif*/
                                            iValue = 0.75;
                                        }
                                    }
                                        break;
                                }
                            }

                            //Do the same with the network that will be
                            //compared with the first
                            foreach (GeneInfo regulonGene in regulons[j].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (ratio > 0) {
/*#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " - setting the entry in jVector to '1'");
#endif*/
                                        jVector[count] = 1;
                                    }
                                    if (ratio < 1) {
                                        if (regulonGene.Site != null) {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " and is regulated - setting jValue to '1'");
#endif*/
                                            jValue = 1;
                                        } else {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " and is not regulated - setting jValue to '0.75'");
#endif*/
                                            jValue = 0.75;
                                        }
                                    }
                                    break;
                                }
                            }

                            //add (xn - yn)^2
                            iEuclidean = iEuclidean + Math.Pow((iValue - jValue), 2);
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("iVector[count] = " + iVector[count] + ", jVector[count] = " + jVector[count] + ", iValue = " + iValue + ", jValue = " + jValue + ", iEuclidean = " + iEuclidean);
#endif*/
                            count = count + 1;
                        }
                        double hammingDistance = 0;
                        for (int k = 0; k < n; k++) {
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("Gene " + k + " value for " + regulons[i].GenomeName + " is " + iVector[k] + " and value for " + regulons[j].GenomeName + " is " + jVector[k]);
#endif*/
                            if (iVector[k] != jVector[k]) {
                                hammingDistance = hammingDistance + Math.Abs(iVector[k] - jVector[k]);
                            }
                        }
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Hamming distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName + " is " + hammingDistance);
#endif*/
                        distances[i][j] = distances[i][j] + hammingDistance * ratio;

                        //Raise all the (xn - yn)^2 terms to the power of 1/2 and
                        //add it to the distance
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Euclidean distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName + " is " + Math.Pow(iEuclidean, 0.5));
#endif*/
                        distances[i][j] = distances[i][j] + (1 - ratio) * (Math.Pow(iEuclidean, 0.5));
                    } else {
                        distances[i][j] = 0;
                    }
                }
            }

            // Return the distances matrix
            return distances;
        }

        /// <summary>
        /// Calculates the centroid of a group
        /// </summary>
        /// <param name="regulons">The list of regulons in the display</param>
        /// <returns>The id of the network that is the centroid</returns>
        private int CalculateCentroid(List<RegulonInfo> regulons)
        {
            //Find out which network out of this group has the smallest
            //total of its distances. I am assuming this is the
            //"average" centroid
            double minDistanceTotal = double.MaxValue;
            int minDistanceTotalIndex = 0;
            for (int i = 0; i < currentDistances.Length; i++)
            {
                double total = 0;
                for (int j = 0; j < currentDistances[i].Length; j++)
                {
                    total = total + currentDistances[i][j];
                }
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Total of all distances for " + regulons[i].GenomeName + " is " + total);
#endif*/
                if (total < minDistanceTotal)
                {
                    minDistanceTotal = total;
                    minDistanceTotalIndex = i;
/*#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("This is the new minimum for this group");
#endif*/
                }
            }

            //Set the new centroid of this group to be the one found
            //above
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("New centroid for group " + groups.IndexOf(group) + " will be " + groupRegulons[minDistanceTotalIndex].GenomeName);
#endif*/
            return regulons.IndexOf(regulons[minDistanceTotalIndex]);
        }

        /// <summary>
        /// Sorts by the distance of each network to the centroid
        /// <para>This needs to be done here rather than passing a function from the RegulonGraphViewer because it needs to access stuff that is not exposed outside</para>
        /// </summary>
        /// <param name="filter"></param>
        public void UpdateVisibleRegulonsSortByCentroidDistance(Func<RegulonInfo, bool> filter)
        {
            // If there are distances calculated for this regulog
            if (currentCentroid != -1) {

                // The list of graphs which will be eventually sorted
                List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

                // The regulons that will not be visible (to set their order
                // property correctly)
			    List<RegulonGraph> invisibleGraphs = new List<RegulonGraph>();

                // The centroid of the current data set
                RegulonGraph groupCentroid = regulonGraphs[currentCentroid];

                // Add all graphs that are not the centroid to the list
                foreach (RegulonGraph graph in regulonGraphs) {
                    if (graph != groupCentroid && (filter == null || filter( graph.Regulon ))) {
                        visibleGraphs.Add(graph);
                    } else {
                        invisibleGraphs.Add( graph );
                    }
                }

                // Sort the list of visible graphs in ascending order of
                // their distance from the centroid
                visibleGraphs.Sort(delegate(RegulonGraph x, RegulonGraph y) {

                    // Get the difference in distance between the two networks
                    int compareResult = (int)((currentDistances[selectedRegulog.Regulons.IndexOf(x.Regulon)][currentCentroid] -
                        currentDistances[selectedRegulog.Regulons.IndexOf(y.Regulon)][currentCentroid]) * 100);

                    // If the result comes out to 0 (the networks have the
                    // same distance) then sort by name instead, otherwise
                    // return the above difference
                    if (compareResult == 0) {
                        return String.Compare(x.Regulon.GenomeName, y.Regulon.GenomeName);
                    } else {
                        return compareResult;
                    }
                });

                // Insert the centroid at the start of the list
                if (groupCentroid != null && (filter == null || filter(groupCentroid.Regulon))) {
                    visibleGraphs.Insert(0, groupCentroid);
                }

                // Remove the existing regulon graphs from the display
			    foreach ( RegulonGraph graph in regulonGraphs ) {
				    graph.RemoveElement();
			    }

                // Add the regulon graphs that will be visible to the display
			    foreach ( RegulonGraph graph in visibleGraphs ) {
				    graph.AddElementTo( container );
			    }

                // Set the visible number of graphs
                numberOfGraphs = visibleGraphs.Length;

                // Ensure the stored order for each visible graph is correct
                RefreshGraphOrder();

                // If there are any invisible regulons...
                if (invisibleGraphs.Length > 1) {
                    // Sort the invisible regulons based on their already stored order
                    invisibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return x.Regulon.Order - y.Regulon.Order; } );

                    // Set the invisible regulons' order to be their current order, but
                    // to be after the visible regulons
                    for (int i = 0; i < invisibleGraphs.Length; i++) {
                        invisibleGraphs[i].Regulon.Order = i + visibleGraphs.Length;
                    }
                }

            // If there is not a centroid, default to sorting by the already stored order
            } else {
                UpdateVisibleRegulons(delegate( RegulonInfo x, RegulonInfo y ) { return x.Order - y.Order; }, filter);
            }
        }

        //Used to check if the window size has passed the threshold for
        //increasing the size of regulon graphs
        protected int currentWindowHeight = 0;
        protected int currentWindowWidth = 0;
        protected bool windowSizeThresholdPassed = false;

        /// <summary>
        /// If the browser window is resized, check if the display has passed
        /// the threshold for "large scale" and adjust the graph sizes if so
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Window_OnResize(ElementEvent e)
        {
            // If the window's height or width has passed one of the thresholds
            if ((Window.OuterWidth > Constants.LargeScaleWidthThreshold && currentWindowWidth <= Constants.LargeScaleWidthThreshold) ||
                (Window.OuterWidth <= Constants.LargeScaleWidthThreshold && currentWindowWidth > Constants.LargeScaleWidthThreshold) ||
                (Window.OuterHeight > Constants.LargeScaleHeightThreshold && currentWindowHeight <= Constants.LargeScaleHeightThreshold) ||
                (Window.OuterHeight <= Constants.LargeScaleHeightThreshold && currentWindowHeight > Constants.LargeScaleHeightThreshold)) {

                // Set the threshold as being passed and then change the size
                // of the graphs
                windowSizeThresholdPassed = true;
                for (int i = 0; i < regulonGraphs.Length; i++) {
                    SetGraphZoomLevel(currentZoomLevel, i);
                }

                // Clear the status of passing the window size threshold
                windowSizeThresholdPassed = false;
            }

            // Store the current window height and width
            currentWindowWidth = Window.OuterWidth;
            currentWindowHeight = Window.OuterHeight;
        }

        /// <summary>
        /// Alerts listeners if the selected genes have changed
        /// </summary>
        protected void OnSelectedGenesChanged() {
            if (SelectedGenesChanged != null) {
                SelectedGenesChanged(this, null);
            }
        }

#if DEBUG
        /// <summary> Returns the contents of a file as a string.
		/// </summary>
		/// <param name="fileName">The path (relative or absolute) of the file.</param>
		/// <param name="process"> A delegate that will be invoked with the content of the file in the event of a successful call, or with null otherwise. </param>
		/// <returns>Returns the content of the file as a string.</returns>

		public static void ReadAllText ( string fileName, Action<string> process ) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.Open( HttpVerb.Get, fileName, false );
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						process( req.ResponseText );
					}
					else {
						if (Constants.ShowDebugMessages) Console.Log( string.Format( "ReadAllText( \"{0}\" )", fileName ) );
						process( null );
					}
				}
			};
            Script.Literal("req.onloadend = function(event) { if (req.status == 404) {console.log('No document retrieved.') } }");
            try {
			    req.Send();
            }
            catch (Exception ex) {
                if (Constants.ShowDebugMessages) Console.Log("req.Send() failed because: " + ex.Message);
            }
		}

        /*private void test(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs args)
        {
            if (Constants.ShowDebugMessages) Console.Log("test fired - args is " + args.ToString());
        }*/
#endif

        // Activated whenether a property changes in the display manager
		public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called whenever a property changes in the display manager. Used to inform listeners when that happens
        /// </summary>
        /// <param name="info">The properties that were changed</param>
        public void NotifyPropertyChanged(String info)
        {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("HammerRegulonDisplay.NotifyPropertyChanged - Sending a notification that " + info + " has changed.");
#endif*/
            // If the property changed event is active
            if (PropertyChanged != null)
            {
                // Trigger a property changed event
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        // Whether this display manager is processing something that should
        // show the loading overlag
        protected bool isProcessing = false;

        /// <summary>
        /// Returns whether the current display manager is processing or not
        /// <para>Used by parent RegulonGraphDisplays to show or hide the loading overlay</para>
        /// </summary>
        public bool IsProcessing
        {
            get { return isProcessing; }
        }

        /// <summary>
        /// Sets a graph's class depending on its current characteristics
        /// </summary>
        /// <param name="graph">The graph to set the class for</param>
        /// <param name="dragged">Whether the graph is currently being dragged</param>
        protected void SetGraphClass (RegulonGraph graph, bool dragged) {

            // Initialise the class string
            string classText = String.Empty;

            // If the graph is a centroid, begin with the centroid class,
            // otherwise begin with the regular class
            if (currentCentroid != -1 && selectedRegulog.Regulons.IndexOf(graph.Regulon) == currentCentroid) {
                classText = "RegulonGraph_Centroid";
            } else {
                classText = "RegulonGraph";
            }

            // If the graph is selected, add the selected class
            if (graph.IsSelected) {
                classText = classText + " RegulonGraph_Selected";
            }

            // If the graph is being dragged, add the dragged class
            if (dragged) {
                classText = classText + " RegulonGraph_Dragged";
            }

            // Apply the class string to the graph
            graph.GraphSvg.SetAttribute("class", classText);
        }

/*#if DEBUG
        /// <summary>
        /// Retrieves the current order of all graphs in the display
        /// <para>Graphs should probably internally store their current position in the display so this doesn't have to be done each time</para>
        /// </summary>
        /// <returns>An array that contains the current position of each network in the display, by their index in the data set's list of regulons</returns>
        public int[] GetGraphOrder() {

            // Initialise a list that is the same length as the amount of
            // networks in the display
            int[] currentGraphOrder = new int[selectedRegulog.Regulons.Length];

            // Set the current graph to check as the first graph in the
            // container, and the index as zero
            Element currentChild = container.FirstChild;
            int index = 0;

            // If in a filtered detail view, the order can be taken directly
            // from the temp order stored from filtering
            if (!detailViewAllGraphs && currentZoomLevel == ZoomLevels.DetailView) {

                // Copy the ordering over
                for (int i = 0; i < graphOrder.Length; i++) {
                    currentGraphOrder[graphOrder[i]] = i;
                }

            // Otherwise, determine the current order
            } else {

                // Make a temporary list of graphs
                // (I'm not sure why this is done)
                List<RegulonGraph> tempGraphs = regulonGraphs;

                // While there is a current graph
                while (currentChild != null) {

                    // For each graph in the temporary list
                    foreach (RegulonGraph graph in tempGraphs) {
    #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Checking if this graph is " + graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ")");
    #endif
                        // If the current graph has been found in the list
                        if (currentChild == graph.DomElement) {
    #if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ") is at " + index);
    #endif
                            // Store the current graph's current position as
                            // the index value, using the position of the
                            // regulon in the data set as the position in the
                            // array
                            currentGraphOrder[selectedRegulog.Regulons.IndexOf(graph.Regulon)] = index;
                            break;
                        } //else {
                            //currentGraphOrder[selectedRegulog.Regulons.IndexOf(graph.Regulon)] = -1;
                        //}
                    }

                    // Go to the next graph in the container
                    currentChild = currentChild.NextSibling;

                    // Increment the index
                    index++;
                }
            }

            // Return the order
            return currentGraphOrder;
        }
#endif*/

        /// <summary>
        /// Refreshes every graph in the display
        /// </summary>
        public void RefreshGraphs() {
            foreach (RegulonGraph graph in regulonGraphs) {
                graph.Refresh();
            }
        }

        /// <summary>
        /// Refreshes the currently stored order for all visible regulon graphs in the display
        /// <para>Graphs not currently in the main container are unaffected</para>
        /// </summary>
        private void RefreshGraphOrder() {
            // Set the current graph to check as the first graph in the
            // container, and the index as zero
            Element currentChild = container.FirstChild;
            int index = 0;

            // While there is a current graph
            while (currentChild != null) {

                // For each graph in the temporary list
                foreach (RegulonGraph graph in regulonGraphs) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Checking if this graph is " + graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ")");
#endif
                    // If the current graph has been found in the list
                    if (currentChild == graph.DomElement) {
#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ") is at " + index);
#endif
                        // Store the current graph's current position as
                        // the index value
                        graph.Regulon.Order = index;
#if DEBUG
                        // Ensure consistency if the selection bug handler is
                        // required
                        if (requireSelectionBugHandler) {
                            foreach (RegulonInfo regulon in selectedRegulog.Regulons) {
                                if (regulon.RegulonId == graph.Regulon.RegulonId &&
                                    regulon.RegulogId == graph.Regulon.RegulogId &&
                                    regulon.GenomeId == graph.Regulon.GenomeId
                                ) {
                                    regulon.Order = index;
                                    break;
                                }
                            }
                        }
#endif
                        break;
                    }
                }

                // Go to the next graph in the container
                currentChild = currentChild.NextSibling;

                // Increment the index
                index++;
            }
        }

#if DEBUG
        // At present, there are currently some issues with homologue
        // selection in DisplayManagers created from
        // RegPreciseRegulonGraphViewer2. This is here to attempt to detect
        // the issue and allow the selection to work properly

        // Status of whether the bug handler needs to be used
        bool requireSelectionBugHandler = false;

        // Status for checking whether the bug handler needs to be used
        //bool checkForSelectionBug = false;

        /// <summary>
        /// While there is a selection issue for DisplayManagers in RegPreciseRegulonGraphViewer2, this will be used to inform
        /// the display manager to use the bug handler when a new regulog is loaded
        /// <para>Automatically sets the check to false as well; this is essentially an override</para>
        /// </summary>
        public bool RequireSelectionBugHandler
        {
            get { return requireSelectionBugHandler; }
            set { requireSelectionBugHandler = value;
                //checkForSelectionBug = false;

                if (value) {
                    if (Constants.ShowDebugMessages) Console.Log("An external source has determined that the bug handler is required");
                } else {
                    if (Constants.ShowDebugMessages) Console.Log("An external source has determined that the bug handler is not required");
                }
            }
        }

        /// <summary>
        /// Handler for homologue selection bugs that occur in a DisplayManager created from RegPreciseRegulonGraphViewer2
        /// </summary>
        /// <param name="gene">The gene to ensure correct (un)selection</param>
        private void SelectionBugHandler(GeneInfo gene) {

            // Go through each graph in the display
            foreach (RegulonGraph graph in regulonGraphs) {

                // Keep track of when the regulon is found, so that extra
                // graphs aren't checked unnecessarily
                bool foundRegulon = false;

                // For each target gene in the graph...
                foreach (GeneNode node in graph.Nodes) {

                    // If the important identifiers are the same, assume the
                    // target gene is the same as the parameter and set it to
                    // the same selection status
                    if (node.Gene.RegulonId == gene.RegulonId && node.Gene.VimssId == gene.VimssId && node.Gene.Name == gene.Name && node.Gene.LocusTag == gene.LocusTag) {
                        node.IsSelected = gene.IsSelected;

                        // Mark that the regulon was found
                        foundRegulon = true;
                        break;
                    }
                }

                // Don't look at any more graphs if the regulon was found
                if (foundRegulon) {
                    break;
                }
            }
        }

        /// <summary>
        /// Activates when a click event is detected on the window
        /// <para>When clicking in the window, hide any existing tooltips</para>
        /// </summary>
        /// <param name="ev">The attributes of the event</param>
        private void Window_OnClick(ElementEvent ev)
        {
            //Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip if the mouse click location is outside
            // the bounds of the container
            int mouseX = ev.PageX;
            int mouseY = ev.PageY;
#if DEBUG
            int containerLeft = HtmlUtil.GetElementX(container);
            int containerRight = HtmlUtil.GetElementX(container) + container.ClientWidth;
            int containerTop = HtmlUtil.GetElementY(container);
            int containerBottom = HtmlUtil.GetElementY(container) + container.ClientHeight;
#endif
            if (mouseX < HtmlUtil.GetElementX(container) ||
                mouseY < HtmlUtil.GetElementY(container) ||
                mouseX > HtmlUtil.GetElementX(container) + container.ClientWidth ||
                mouseY > HtmlUtil.GetElementY(container) + container.ClientHeight) {
                HideCurrentToolTip();
            }
        }
#endif
    }
}
