﻿using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.Common;
using SystemQut;
using SystemQut.Svg;
#if DEBUG
using System.Net;
using SystemQut.IO;
#endif
using SystemQut.ComponentModel;

namespace RegulonExplorer.View.RegPreciseView {

	/// <summary> Default implementation of IRegulonDisplay. Regulon graphs
	///		are added in a flow layout manner.
	/// <para>
	///		Override this to implement alternative display management.
	/// </para>
	/// </summary>

	public class DefaultRegulonDisplay : IRegulonDisplay, INotifyPropertyChanged {

		/// <summary> The container that holds these graphs.
		/// </summary>
		protected Element container;

		/// <summary> Maintain a list of GUI elements which correspond to the data model.
		/// </summary>
		protected List<RegulonGraph> regulonGraphs = new List<RegulonGraph>();

        // The list of currently selected genes
        // Stores genes as a combination of the regulon index and the gene
        // index in a string, seperated by a comma
        // In the comparison, the regulon *graph* index is used instead
        protected List<string> selectedGenes = new List<string>();

        /// <summary>
        /// Gets or sets the list of selected genes
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        public virtual List<GeneInfo> SelectedGenes
        {
            get {
                // Create a new list to store the gene objects
                List<GeneInfo> geneInfos = new List<GeneInfo>();

                // Go through each string identifier in selected genes and
                // retrieve the gene object that it refers to, adding it to the
                // list of gene objects
                foreach (string index in selectedGenes) {
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Look through the list of graphs
                    foreach ( RegulonGraph graph in regulonGraphs ) {

                        // If this graph is showing the regulon that
                        // corresponds to the index, add the gene object that's
                        // selected to the list (since a gene must exist to be
                        // selected, geneIndexes doesn't need checking for a
                        // "-1")
                        if (graph.Regulon == selectedRegulog.Regulons[regulonIndex]) {
                            geneInfos.Add(graph.Regulon.Genes[geneIndexes[regulonIndex][geneIndex]]);
                            break;
                        }
                    }
                }

                // Return the list of gene objects
                return geneInfos;
            }
// New set behaviour
            set {
                // Clear the current list of selected genes
                selectedGenes.Clear();

                // Look through the list of graphs
                foreach ( RegulonGraph graph in regulonGraphs ) {
                    foreach ( GeneInfo gene in graph.Regulon.Genes ) {

                        // If the current gene is inside the supplied list of
                        // genes
                        if (value.Contains(gene)) {

                            // Get the index of the regulon and the gene, and
                            // combine them into a comma seperated string as the
                            // identifier
                            int regulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                            int geneIndex = geneDictionary[gene.Name];
                            string temp = regulonIndex + "," + geneIndex;

                            // Set the gene object as selected
                            gene.IsSelected = true;

                            // Add the identifier to the list of selected genes
                            selectedGenes.Add(temp);

                        // Otherwise ensure the gene is not selected
                        } else {
                            gene.IsSelected = false;
                        }
                    }
                }
                OnSelectedGenesChanged();
            }
        }

        // The current zoom level of the display
        protected ZoomLevels currentZoomLevel = ZoomLevels.SmallOverview;

        /// <summary>
        /// Gets the current zoom level of the display
        /// <para>The setter was originaly a seperate method called "ChangeZoomLevel"</para>
        /// </summary>
        public ZoomLevels CurrentZoomLevel
        {
            get { return currentZoomLevel; }
            set {
                // Don't do anything if the passed zoom level is the same
                if (currentZoomLevel == value) return;

                // Special filtering if the detail view is intended to only display
                // the selected networks
                if (Constants.DetailedViewSelected) {

                    // If the new zoom level is the detail view, check if the
                    // display needs filtering
                    if (value == ZoomLevels.DetailView) {

                        // Add all selected networks to a list
                        detailedViewGraphs = new List<RegulonGraph>();
                        foreach (RegulonGraph graph in regulonGraphs) {
                            if (graph.Regulon.IsSelected) {
                                detailedViewGraphs.Add(graph);
                            }
                        }

                        // If there are selected networks, filter the display
                        if (detailedViewGraphs.Length > 0) {

                            // Note that the display is not showing all graphs
                            // in the detail view
                            detailViewAllGraphs = false;

                            //DetailView_FilterGraphs();
                            UpdateVisibleRegulons(null, delegate ( RegulonInfo regulon ) { return regulon.IsSelected; } );
                        }

                    // Else if changing out of the detail view, make sure all
                    // networks are visible again
                    } else {
                        if (!detailViewAllGraphs) {
                            //DetailView_RestoreGraphs();

                            // Return this to the default
                            detailViewAllGraphs = true;

                            UpdateVisibleRegulons(null, null);
                        }
                    }
                }

                // Set the zoom level for all graphs in the display
                currentZoomLevel = value;
                for (int i = 0; i < regulonGraphs.Length; i++) {
                    SetGraphZoomLevel(value, i);
                }

                // Clear the status of passing the window size threshold if that's
                // why this method was called
                windowSizeThresholdPassed = false;
            }
        }

        // The currently selected regulons in the display, in order of selection
        // if possible
        protected List<int> selectedRegulons = new List<int>();

        /// <summary>
        /// Gets or sets the currently selected regulons in the display
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        public virtual List<RegulonInfo> SelectedRegulons
        {
            get {
                // Create a new list to store the regulon objects
                List<RegulonInfo> regulonInfos = new List<RegulonInfo>();

                // Go through all of the indexes in the list of selected
                // regulons and retrieve the regulon object that it points to
                foreach (int index in selectedRegulons) {

                    // Look through the list of graphs
                    foreach ( RegulonGraph graph in regulonGraphs ) {

                        // If this graph is showing the regulon that
                        // corresponds to the index, add that to the list
                        if ( graph.Regulon == selectedRegulog.Regulons[index] ) {
                            regulonInfos.Add(graph.Regulon);
                            break;
                        }
                    }
                }

                // Return the list of regulon objects
                return regulonInfos;
            }
// New set behaviour
            set {
                // Clear the current list of selected regulons
                selectedRegulons.Clear();

                // Look through the list of graphs
                foreach ( RegulonGraph graph in regulonGraphs ) {

                    // If the current regulon is contained in the supplied list
                    // of regulons, set it as selected
                    if (value.Contains(graph.Regulon)) {
                        graph.Regulon.IsSelected = true;

                        // Add the index of the regulon to the list of
                        // selected regulons
                        selectedRegulons.Add(selectedRegulog.Regulons.IndexOf(graph.Regulon));

                    // Otherwise ensure it is not selected
                    } else {
                        graph.Regulon.IsSelected = false;
                    }

                    // Ensure the graph has the correct class
                    SetGraphClass(graph, false, false);
                }
            }
        }

        // Contains the current distances between regulons and the current
        // centroids for each group
        private double[][] currentDistances;
        private int currentCentroid = -1;

        /// <summary>
        /// Gets or sets a matrix of all the distances between the networks
        /// <para>Allowing setting is primarily to let distances be transferred after changing the display manager</para>
        /// </summary>
        public double[][] CurrentDistances
        {
            get { return currentDistances; }
            set { currentDistances = value; }
        }

        /// <summary>
        /// Gets the network index for the current centroid
        /// </summary>
        public int CurrentCentroid
        {
            get { return currentCentroid; }
            set { currentCentroid = value;

                // Go through all of the graphs to ensure they have the right
                // appearance
                foreach (RegulonGraph graph in regulonGraphs) {

                    // Get the index of that graph's regulon in the list of
                    // regulon
                    int index = selectedRegulog.Regulons.IndexOf(graph.Regulon);

                    // If this graph is one of the current centroids, add it to the
                    // label
                    if (currentCentroid == index) {
                        if (graph.GraphLabelText.IndexOf(" " + Constants.Text_NetworkLabelCentroid) == -1) {
                            graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelCentroid;
                        }

                    // Otherwise, remove that from the label if it was an old
                    // centroid
                    } else {
                        graph.GraphLabelText = graph.GraphLabelText.Replace(" " + Constants.Text_NetworkLabelCentroid, "");
                    }

                    // Ensure this graph has the correct class
                    SetGraphClass(graph, false, false);
                }
            }
        }

        // The ratio of Euclidean and Hamming distances
        private double ratio = -1;

        /// <summary>
        /// Gets or sets the ratio of Euclidean and Hamming distances
        /// <para>0 = full Euclidean, 1 = full Hamming (default is 0.5)</para>
        /// </summary>
        public double Ratio
        {
            get { return ratio; }
            set { if (value == ratio) return;
                ratio = value;
                /*ratioChanged = true;*/ }
        }

        //Used to inform if the above has changed
        private bool ratioChanged = false;

        /// <summary>
        /// Gets or sets the status of the ratio changing
        /// </summary>
        public bool RatioChanged {
            get { return ratioChanged; }
            set { ratioChanged = value; }
        }

        /// <summary>
        /// Used to inform listeners if the selected genes are changed
        /// </summary>
        public event EventHandler SelectedGenesChanged;

        /// <summary>
        /// Stores the number of graphs that are currently visible in the display
        /// <para>This is set manually by some functions</para>
        /// </summary>
        private int numberOfGraphs = -1;

        /// <summary>
        /// Returns the number of graphs in the display
        /// <para>How graphs are counted for this is dependant on the invidiual regulon display classes</para>
        /// </summary>
        public int NumberOfGraphs {
            get {
                if (numberOfGraphs == -1) {
                    return regulonGraphs.Length;
                } else {
                    return numberOfGraphs;
                }
            }
        }

		/// <summary>
        /// Displays the list of regulons in the specified container
		/// </summary>
		/// <param name="regulons">The list of regulons</param>
		/// <param name="container">The DOM object to act as the container</param>
		/// <param name="selectedRegulog">The currently selected regulog</param>
		/// <param name="nodeFactory">The node factory to use to create the nodes</param>
		/// <param name="colourChooser">The attribute factory to use to set the node attributes (mainly colour)</param>
		virtual public void DisplayRegulons(
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser
		) {
            // Store the current regulog
            this.selectedRegulog = selectedRegulog;

            // Make the number of groups "1" if it is another value
            if (selectedRegulog.NumGroups != null && selectedRegulog.NumGroups != 1) {
                selectedRegulog.NumGroups = 1;
            }

            // Remove the resize listener if it exists already
            Window.RemoveEventListener("resize", Window_OnResize, false);

            // Store the current container and set its attributes
            this.container = container;
            this.container.Style.Height = "auto";
            this.container.Style.Width = "auto";

            // Add mouse events to the container
            this.container.AddEventListener(MouseEventType.dragstart, Container_DragStart, false);
            this.container.AddEventListener(MouseEventType.dragenter, Container_DragEnter, false);
            this.container.AddEventListener(MouseEventType.dragleave, Container_DragLeave, false);
            this.container.AddEventListener(MouseEventType.dragover, Container_DragOver, false);
            this.container.AddEventListener(MouseEventType.dragend, Container_DragEnd, false);
            this.container.AddEventListener(MouseEventType.drop, Container_Drop, false);

            // Remove all the children this regulon display has
            while (container.ChildNodes[0] != null)
            {
                container.RemoveChild(container.ChildNodes[0]);
            }

            // Create a list for all the TG names
            List<string> geneNames = new List<string>();

            // Look through all the genes in the *regulog*. This is so
            // homologues can be handled correctly
            foreach (GeneInfo gene in selectedRegulog.TargetGenes)
            {
                // Add the TG's name to the list of names, if it is not already
                // in there
                string name = gene.Name;

                if (!geneNames.Contains(name))
                {
                    geneNames.Add(name);
                }
            }

            // For each name, add it in the dictionary, using the length of the
            // dictionary as its index
            geneDictionary.Clear();
            foreach (string name in geneNames) geneDictionary[name] = geneDictionary.Count;

            // Initialise the matrix of regulons and their gene locations
            geneIndexes = new int[regulons.Count][];
            for (int i = 0; i < regulons.Count; i++) {
                geneIndexes[i] = new int[geneDictionary.Count];
                for (int j = 0; j < geneIndexes[i].Length; j++) {
                    geneIndexes[i][j] = -1;
                }
            }

            // Try to load the distances between networks from a text file, if
            // they do not already exist or the ratio of Hamming to Euclidean
            // distance has changed
            if (currentDistances == null || ratioChanged == true) {
#if DEBUG
                try {
                    ReadAllText( "/RegulonExplorer/Text_Files/regulog" + selectedRegulog.RegulogId + "_distances_" + ratio.ToString() + ".txt", delegate( string text ) {
                        if ( !string.IsNullOrEmpty(text) ) {
                            if (Constants.ShowDebugMessages) Console.Log("loading distances from text file");
                            StringReader reader = new StringReader( text );

                            double[][] tempLoadedDistances = new double[selectedRegulog.Regulons.Length][];

                            for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                                string currentLine = reader.ReadLine();
                                string[] currentRegulonDistances = currentLine.Split(',');
                                tempLoadedDistances[i] = new double[selectedRegulog.Regulons.Length];
                                for (int j = 0; j < selectedRegulog.Regulons.Length; j++) {
                                    tempLoadedDistances[i][j] = double.Parse(currentRegulonDistances[j]);
                                }
                            }

                            this.currentDistances = tempLoadedDistances;
                        } else {
                            if (Constants.ShowDebugMessages) Console.Log("No file retrieved for  " + selectedRegulog.RegulatorName);

                            // If a file does not exist, calculate the distances now
                            this.currentDistances = CalculateDistances(regulons);
                        }
				    });
                } catch (Exception ex) {
                    if (Constants.ShowDebugMessages) Console.Log("Failed to load distances file for " + selectedRegulog.RegulatorName + " because:" + ex.Message);

                    // If a file couldn't be loaded, calculate the distances now
                    this.currentDistances = CalculateDistances(regulons);
				}
#else
                this.currentDistances = CalculateDistances(regulons);
#endif
                ratioChanged = false;
            }

            // Calculate the centroid of the current networks
            currentCentroid = CalculateCentroid(regulons);

            // Create and configure a graph for each regulon that will be
            // displayed
            foreach (RegulonInfo regulon in regulons)
            {
                RegulonGraph graph = new RegulonGraph(
                    selectedRegulog,
                    regulon,
                    nodeFactory,
                    colourChooser,
                    Constants.RegulonDisplayWidth,
                    Constants.RegulonDisplayHeight,
                    Constants.RegulonDisplayUnits
                );

                //regulonGraph.DomElement.SetAttribute("draggable", "true");
                graph.DomElement.ID = regulon.GenomeName;

                // Add mouse events to the regulon graph
                graph.DomElement.AddEventListener(MouseEventType.dragstart, Graph_DragStart, false);
                graph.DomElement.AddEventListener(MouseEventType.dragenter, Graph_DragEnter, false);
                graph.DomElement.AddEventListener(MouseEventType.dragleave, Graph_DragLeave, false);
                graph.DomElement.AddEventListener(MouseEventType.dragover, Graph_DragOver, false);
                graph.DomElement.AddEventListener(MouseEventType.dragend, Graph_DragEnd, false);
                graph.DomElement.AddEventListener(MouseEventType.drop, Graph_Drop, false);
                graph.DomElement.AddEventListener(TouchEventType.touchstart, Graph_TouchStart, false);
                graph.DomElement.AddEventListener(TouchEventType.touchmove, Graph_TouchMove, false);
                graph.DomElement.AddEventListener(TouchEventType.touchend, Graph_TouchEnd, false);
                graph.DomElement.AddEventListener(TouchEventType.touchcancel, Graph_TouchCancel, false);

                // For each gene in the regulon
                foreach (GeneInfo geneInfo in graph.Regulon.TargetGenes)
                {
                    // Add a listerner for the gene's properties changing (used
                    // to detect selection)
                    ((SystemQut.ComponentModel.INotifyPropertyChanged)geneInfo).PropertyChanged += node_PropertyChanged;

                    // Add it to the list of selected genes if it is selected
                    if (geneInfo.IsSelected) {

                        // Get the index of the regulon and the gene, and
                        // combine them into a comma seperated string as the
                        // identifier
                        int regulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                        int geneIndex = geneDictionary[geneInfo.Name];
                        string temp = regulonIndex + "," + geneIndex;
                        selectedGenes.Add(temp);
                    }

                    // Add the index of the current gene into the matrix
                    geneIndexes[selectedRegulog.Regulons.IndexOf(graph.Regulon)][geneDictionary[geneInfo.Name]] = graph.Regulon.Genes.IndexOf(geneInfo);
                }

                // Add the centroid label if the current graph is a centroid
                if (regulons.IndexOf(regulon) == currentCentroid) {
                    graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelCentroid;
                }

                // Set the graph's attributes
                SetGraphClass(graph, false, false);

                // Add the graph to the container as well as the list of graphs
                graph.AddElementTo(container);
                regulonGraphs.Add(graph);

                // Add a listener for the regulon's properties changing (used
                // to detect selection)
                graph.Regulon.PropertyChanged += regulonGraph_PropertyChanged;
            }

            // Add a listener for when the browser window is resized
            Window.AddEventListener("resize", Window_OnResize, false);

            // Store the current window dimensions
            currentWindowWidth = Window.OuterWidth;
            currentWindowHeight = Window.OuterHeight;

            // If the window is larger than the threshold, set the threshold
            // as being passed (to allow size adjustment)
            if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                windowSizeThresholdPassed = true;
            }

            // Process selected genes
            OnSelectedGenesChanged();

            // Ensure the graphs have the right size and position of elements
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphZoomLevel(currentZoomLevel, i);
            }

            // Clear the status of passing the window size threshold
            windowSizeThresholdPassed = false;
		}

        protected List<string> selectedHomologs = new List<string>();

        // These store the locations of genes inside the arrays in each regulon
        // for easier access
        protected int[][] geneIndexes;
        protected Dictionary<string, int> geneDictionary = new Dictionary<string,int>();
        protected RegulogInfo selectedRegulog;

        /// <summary>
        /// Runs when a property on a node is changed. Used for checking if nodes have been selected
        /// - in that case, triggers the selection of homologs
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="args">Event arguments</param>
        protected virtual void node_PropertyChanged(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs args)
        {
            // Don't do anything if in the middle of clearing selected genes
            if (!ClearAllSelectedGenes) {

                // If the object that sent the event is a target gene
                if (sender is GeneInfo) {

                    // If a node's selection has been changed
                    if (args.Includes("IsSelected"))
                    {
                        string currentGeneName = (sender as GeneInfo).Name;

                        // Keep track of how many layers of processing exist
                        // (so that the "isProcessing" variable can be set
                        // correctly)
                        numProcessing = numProcessing + 1;

                        // Notify listeners that this display is currently
                        // processing so the loading overlay can be displayed
                        if (numProcessing > 0 && !isProcessing) {
                            isProcessing = true;
                            NotifyPropertyChanged("IsProcessing");
                        }

                        // Use a timeout delegate so that the loading overlay
                        // can appear
                        Script.SetTimeout((Action) delegate {

                            // Get the current selected status of the sending
                            // target gene
                            bool selected = (sender as ISelectable).IsSelected;

                            // Get the index of the regulon and the gene, and
                            // combine them into a comma seperated string as the
                            // identifier
                            int regulonIndex = -1;
                            foreach (RegulonInfo regulon in selectedRegulog.Regulons) {
                                if (regulon.RegulonId == (sender as GeneInfo).RegulonId) {
                                    regulonIndex = selectedRegulog.Regulons.IndexOf(regulon);
                                    break;
                                }
                            }
                            int geneIndex = geneDictionary[currentGeneName];
                            string temp = regulonIndex + "," + geneIndex;

                            // Add or remove the gene from the list of selected
                            // genes based on its selection status
                            if (selected) {
                                if (!selectedGenes.Contains(temp)) {
                                    selectedGenes.Add(temp);
                                }
                            } else {
                                if (selectedGenes.Contains(temp)) {
                                    selectedGenes.Remove(temp);
                                }
                            }

                            // Add this gene name to the list of selected
                            // homologs if it isn't already
                            if (!selectedHomologs.Contains(currentGeneName)) {
                                selectedHomologs.Add(currentGeneName);
                                foreach (RegulonGraph graph in regulonGraphs)
                                {

                                    // Find homologs to select using the
                                    // dictionary of gene locations
                                    int newRegulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                                    int newGeneIndex = geneDictionary[currentGeneName];
                                    if (geneIndexes[newRegulonIndex][newGeneIndex] != -1 &&
                                        graph.Regulon.Genes[geneIndexes[newRegulonIndex][newGeneIndex]].IsSelected != selected) {
                                        graph.Regulon.Genes[geneIndexes[newRegulonIndex][newGeneIndex]].IsSelected = selected;
                                    }
                                }
                            }

                            // Additional processing on change of gene
                            // selections
                            OnSelectedGenesChanged();

                            // Decrement the number of processing layers, and
                            // change isProcessing to false if there are no
                            // more layers (hiding the loading overlay)
                            if (numProcessing > 0) {
                                numProcessing = numProcessing - 1;
                            }
                            if (numProcessing < 1 && isProcessing) {
                                isProcessing = false;
                                NotifyPropertyChanged("IsProcessing");

                                // Clear the list of homologs
                                selectedHomologs.Clear();
                            }
                        }, 10, null);
                    }
                }
            }
        }

        /// <summary>
        /// Runs when a property on a regulon graph is changed. Used for checking if regulon graphs have been selected
        /// - in that case, adds or removes that regulon graph from the list of selected regulons
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="args">Event arguments</param>
        protected virtual void regulonGraph_PropertyChanged(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs args)
        {

            // If a regulon graph's selection has been changed
            if (args.Includes("IsSelected")) {

                RegulonInfo regulon = (sender as RegulonInfo);
                int index = selectedRegulog.Regulons.IndexOf(regulon);

                // Add or remove the regulon graph from the list of selected
                // regulons based on its selection status
                if (regulon.IsSelected) {
                    if (!selectedRegulons.Contains(index)) {
                        selectedRegulons.Add(index);
                    }
                } else {
                    selectedRegulons.Remove(index);
                }
            }
        }

        /// <summary>
        /// Used to mark genes being deselected so that the display doesn't
        /// attempt to propogate the deslection to homologs
        /// </summary>
        protected bool ClearAllSelectedGenes = false;

        /// <summary>
        /// Deselects all of the selected genes in the display
        /// </summary>
        /// <param name="allowOverlay">Whether to use a time out to allow the loading overlay to appear</param>
        public virtual void ClearSelectedGenes(bool allowOverlay) {
            //if (!(this is CompareRegulonDisplay)) {

            if (selectedGenes.Length > 0) {

            // Mark that selected genes are currently being cleared to prevent
            // homolog selection from taking place
            ClearAllSelectedGenes = true;

            if (allowOverlay) {

            // Notify listeners that this display is currently
            // processing so the loading overlay can be displayed
            isProcessing = true;
            NotifyPropertyChanged("IsProcessing");

            // Use a timeout delegate so that the loading overlay can appear
            Script.SetTimeout((Action) delegate {

                // Clear the selected status of each gene in the selected list
                // and then clear the list
                foreach (string index in selectedGenes) {

                    // Get the index of the regulon and the gene by splitting
                    // the identifier in the selected list
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Set the gene that corresponds to the retrieved indexes
                    // as unselected
                    selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = false;
                }

                // Clear the list of selected genes
                selectedGenes.Clear();

                // Mark that the display is no longer deselecting all genes
                ClearAllSelectedGenes = false;

                // Notify listeners that this display has finished processing
                // so the loading overlay can be hidden
                isProcessing = false;
                NotifyPropertyChanged("IsProcessing");
            }, 10, null);

            } else {

                // Clear the selected status of each gene in the selected list
                // and then clear the list
                foreach (string index in selectedGenes) {

                    // Get the index of the regulon and the gene by splitting
                    // the identifier in the selected list
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Set the gene that corresponds to the retrieved indexes
                    // as unselected
                    selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = false;
                }

                // Clear the list of selected genes
                selectedGenes.Clear();

                // Mark that the display is no longer deselecting all genes
                ClearAllSelectedGenes = false;
            }

            }

            //}
        }

        /// <summary>
        /// Deselects all of the selected regulons in the display
        /// </summary>
        public void ClearSelectedRegulons() {

            // Go through all the regulon graphs and set them as not selected
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.IsSelected) {
                    graph.IsSelected = false;

                    // Ensure the graph has the correct visual class
                    SetGraphClass(graph, false, false);
                }
            }

            // Clear the list of selected regulons
            selectedRegulons.Clear();
        }

		/// <summary> Updates the visible regulons in the associated container,
		///		applying a filter and sorting function to them
        /// </summary>
        /// <param name="sort">Function for determining the method of sorting regulons</param>
        /// <param name="filter">Function for determining which regulons should be filtered</param>
		public virtual void UpdateVisibleRegulons(
			Func<RegulonInfo, RegulonInfo, int> sort,
			Func<RegulonInfo, bool> filter
		) {
            // If no sorting function was specified, sort using the already
            // stored order
            if (sort == null) {
                sort = delegate( RegulonInfo x, RegulonInfo y ) { return x.Order - y.Order; };
            }

            if (detailedViewGraphs == null) {
                detailedViewGraphs = new List<RegulonGraph>();
            }

            detailedViewGraphs.Clear();

            // The list of regulons that will be displayed
			List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

            // The regulons that will not be visible (to set their order
            // property correctly)
			List<RegulonGraph> invisibleGraphs = new List<RegulonGraph>();

            // Go through all the regulon graphs and check them against the
            // filter
			for ( int i = 0; i < regulonGraphs.Count; i++ ) {
				RegulonGraph graph = regulonGraphs[i];

                // Only add the regulon to the list if it meets the conditions
                // of the filter
				if ( filter == null || filter( graph.Regulon ) ) {
					visibleGraphs.Add( graph );
                    if (!detailViewAllGraphs) detailedViewGraphs.Add( graph );
				} else {
                    invisibleGraphs.Add( graph );
                }
			}

            // Sort the visible regulons based on the sorting function
			visibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return sort( x.Regulon, y.Regulon ); } );

            // Remove the existing regulon graphs from the display
			foreach ( RegulonGraph graph in regulonGraphs ) {
				graph.RemoveElement();
			}

            // Add the regulon graphs that will be visible to the display
			foreach ( RegulonGraph graph in visibleGraphs ) {
				graph.AddElementTo( container );
			}

            // Set the visible number of graphs
            numberOfGraphs = visibleGraphs.Length;

            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();

            // If there are any invisible regulons...
            if (invisibleGraphs.Length > 1) {
                // Sort the invisible regulons based on their already stored order
                invisibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return x.Regulon.Order - y.Regulon.Order; } );

                // Set the invisible regulons' order to be their current order, but
                // to be after the visible regulons
                for (int i = 0; i < invisibleGraphs.Length; i++) {
                    invisibleGraphs[i].Regulon.Order = i + visibleGraphs.Length;
                }
            }
		}

		/// <summary> Removes all regulon graphs from the display and disposes them, leaving the
		///		display blank.
		/// </summary>

        virtual public void Clear()
        {

            // Remove the listeners from the main container
            if (this.container != null)
            {
                this.container.RemoveEventListener("dragstart", Container_DragStart, false);
                this.container.RemoveEventListener("dragenter", Container_DragEnter, false);
                this.container.RemoveEventListener("dragleave", Container_DragLeave, false);
                this.container.RemoveEventListener("dragover", Container_DragOver, false);
                this.container.RemoveEventListener("dragend", Container_DragEnd, false);
                this.container.RemoveEventListener("drop", Container_Drop, false);
            }
			foreach ( RegulonGraph graph in regulonGraphs ) {

                // Remove the graph from the DOM
                graph.RemoveElement();

                // Remove the listeners from the graph
                graph.DomElement.RemoveEventListener("dragstart", Graph_DragStart, false);
                graph.DomElement.RemoveEventListener("dragenter", Graph_DragEnter, false);
                graph.DomElement.RemoveEventListener("dragleave", Graph_DragLeave, false);
                graph.DomElement.RemoveEventListener("dragover", Graph_DragOver, false);
                graph.DomElement.RemoveEventListener("dragend", Graph_DragEnd, false);
                graph.DomElement.RemoveEventListener("drop", Graph_Drop, false);
                graph.DomElement.RemoveEventListener("touchstart", Graph_TouchStart, false);
                graph.DomElement.RemoveEventListener("touchmove", Graph_TouchMove, false);
                graph.DomElement.RemoveEventListener("touchend", Graph_TouchEnd, false);
                graph.DomElement.RemoveEventListener("touchcancel", Graph_TouchCancel, false);

                // Properly dispose the graph
				graph.Dispose();
			}

            // Clear all the selected genes and regulons
            ClearSelectedGenes(false);
            ClearSelectedRegulons();

            // Clear the list of regulon graphs
			regulonGraphs.Clear();

            // Clear the list of graphs in the detail view
            if (detailedViewGraphs != null && detailedViewGraphs.Length > 0) {
                detailedViewGraphs.Clear();
            }
		}

        /// <summary>
        /// Finds a parent of the specified type for the given element
        /// </summary>
        /// <param name="element">The element to find a parent for</param>
        /// <param name="tagName">The tag name to look for</param>
        /// <returns>The parent element that fits the specified type, or null if none was found</returns>
		protected static Element FindParentOfType ( Element element, string tagName ) {

            // While the element is not null, and does not have the same tag
            // name as was requested...
			while ( element != null && String.Compare( element.TagName, tagName, true ) != 0 ) {

                // Set the current element to its parent
				element = element.ParentNode;
			}

            // Return the parent element (or null if no suitable parent was
            // found)
			return element;
		}

        // The current dragged object, if any
		protected Element draggedObject = null;

        /// <summary>
        /// Listens for when a drag event starts on a graph
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Graph_DragStart(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "DragStart: " + target.ID );
#endif
			e.DataTransfer.SetData( DataFormat.Text, "Dummy Payload" );

            // Set the current dragged objecct
			draggedObject = target;
            e.StopPropagation();
		}

        /// <summary>
        /// Listens for when a drag event enters a graph
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Graph_DragEnter(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "DragEnter: " + target.ID );
#endif
			e.PreventDefault();
            e.StopPropagation();

            // Find the graph that corresponds to the target
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.DomElement.ID == target.ID) {

                    // Set the graph's class so that it is highlighted (to show
                    // that it is the "nearest" graph)
                    SetGraphClass(graph, false, true);
                    break;
                }
            }
		}

        /// <summary>
        /// Listens for when a drag event is over a graph
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Graph_DragOver(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "DragOver: " + target.ID );
#endif
			e.PreventDefault();
            e.StopPropagation();

            // Find the graph that corresponds to the target
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.DomElement.ID == target.ID) {

                    // Set the graph's class so that it is highlighted (to show
                    // that it is the "nearest" graph)
                    SetGraphClass(graph, false, true);
                    break;
                }
            }
		}

        /// <summary>
        /// Listens for when a drag event leaves a graph
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Graph_DragLeave(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "DragLeave: " + target.ID );
#endif
			e.PreventDefault();
            e.StopPropagation();

            // Find the graph that corresponds to the target
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.GraphSvg.DomElement == target.ChildNodes[0]) {

                    // Set the graph's class so that it is no longer
                    // highlighted
                    SetGraphClass(graph, false, false);
                    break;
                }
            }
		}

        /// <summary>
        /// Listens for when a drag ends on a graph
        /// <para>Currently does nothing</para>
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Graph_DragEnd(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "DragEnd: " + target.ID );
#endif
			e.PreventDefault();
            e.StopPropagation();
		}

        /// <summary>
        /// Listens for when a drag-and-drop action for a graph completes on a graph
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Graph_Drop(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "Drop: " + target.ID );
#endif
			e.PreventDefault();
            e.StopPropagation();

            // Find the graph that corresponds to the target
            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.GraphSvg.DomElement == target.ChildNodes[0]) {

                    // Set the graph's class so that it is no longer
                    // highlighted
                    SetGraphClass(graph, false, false);
                    break;
                }
            }

            // Insert the dragged graph before the target in the container
            target.ParentNode.InsertBefore(draggedObject, target);

            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();
		}

        /// <summary>
        /// Listens for when a drag event starts on the container
        /// <para>Currently does nothing</para>
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Container_DragStart(ElementEvent e)
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("ContainerDragStart: " + e.Target.ID);
#endif
            e.PreventDefault();
        }

        /// <summary>
        /// Listens for when a drag event enters the container
        /// <para>Currently does nothing</para>
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Container_DragEnter(ElementEvent e)
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("ContainerDragEnter: " + e.Target.ID);
#endif
            e.PreventDefault();
        }

        /// <summary>
        /// Listens for when a drag event is over the container
        /// <para>Highlights the graph in the container that would have its place taken by the dragged network</para>
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Container_DragOver(ElementEvent e)
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("ContainerDragOver: " + e.Target.ID);
#endif
            e.PreventDefault();

            // Get the target from the event
            Element target = e.Target;

            //We want to place the dragged network in the position that's
            //closest to the place the user dragged to

            //Get the position of the mouse cursor
            int mouseX = e.ClientX;
            int mouseY = e.ClientY;
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Mouse location is " + mouseX + ", " + mouseY);
#endif

            // The current index of the graph that is closest to the mouse X
            // and Y
            int closestGraphIndex = -1;

            // The current closest distance between a non-dragged graph and the
            // mouse X and Y
            double closestDistance = double.MaxValue;

            //For every network in the container, check its actual distance
            //from the mouse cursor's position
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Determining the closest graph to the drop position");
#endif
            // For each graph in the list of graphs...
            for (int i = 0; i < regulonGraphs.Length; i++) {

                // Reset that graph's class to normal
                SetGraphClass(regulonGraphs[i], false, false);

                //Calculate the centre of the current network
                int graphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientWidth)/2;
                int graphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientHeight)/2;

                //Determine the distance between the current centre and the
                //mouse's position using Pythagoras
                double currentDistance = Math.Sqrt(Math.Pow(mouseX - graphX, 2) + Math.Pow(mouseY - graphY, 2));
#if DEBUG
                //Console.Log(regulonGraphs[i].Regulon.Genome.Name + " centre is " + currentDistance + " away.");
#endif
                //If the calculated distance is less than the current closest,
                //this network is the new closest
                if (currentDistance < closestDistance) {
                    closestDistance = currentDistance;
                    closestGraphIndex = i;
#if DEBUG
                    //Console.Log(regulonGraphs[i].Regulon.Genome.Name + " is the new closest.");
#endif
                }
            }

            //If we found a closest network, highlight it
            if (closestGraphIndex > -1) {

                //But only highlight it if the network would replace it. If it is
                //to the right or below the last graph, and depending on how close
                //it is, it will go at the end and so we won't highlight anything

                //If it's:
                //- Closest to the last network in the container (if the container
                //is not empty), and the drop point was to the right, or completely
                //underneath, or
                //- To the right and below the top left corner of the last
                //  network
                //      - put the dropped network last
                //- To the left and below the top left corner of the bottom
                //  left network
                //      - put the dropped network in place of that network

                // Get the X and Y coordinates of the "last" graph in the
                // container
                Element lastGraphElement = container.ChildNodes[container.ChildNodes.Length - 1];
                int lastGraphX = HtmlUtil.GetElementX(lastGraphElement);
                int lastGraphY = HtmlUtil.GetElementY(lastGraphElement);

                //Find the network that is at the bottom left
                int bottomLeftGraphIndex = -1;
                int bottomLeftGraphX = int.MaxValue;

                // For each graph in the list of graphs
                for (int i = 0; i < regulonGraphs.Length; i++) {

                    // Get that graph's X and Y coordinates
                    int currentGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                    int currentGraphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement);

                    // If this graph is on the same Y value as the last graph,
                    // and its X coordinate is less than the X coordinate of
                    // the current "bottom left" network, store it and its
                    // X coordinate
                    if (currentGraphY == lastGraphY && currentGraphX < bottomLeftGraphX) {
                        bottomLeftGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                        bottomLeftGraphIndex = i;
                    }
                }

                // If there is a "last" graph and the mouse X and Y is greater
                // than that graph's X and Y, show that the dragged graph will
                // go at the end of the container
                if (lastGraphX != int.MinValue && ((mouseX > lastGraphX) && (mouseY > lastGraphY))) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object will go at the end of the container.");
#endif
                // Else, if there is a "bottom left" graph and the mouse X is
                // less than its X coordinate, and more than its Y cooridnate,
                // show that the dragged graph will go in front of that graph
                } else if (bottomLeftGraphX != int.MaxValue && ((mouseX < bottomLeftGraphX) && (mouseY > lastGraphY))) {
                    SetGraphClass(regulonGraphs[bottomLeftGraphIndex], false, true);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object would go in front of " + regulonGraphs[bottomLeftGraphIndex].Regulon.Genome.Name + ".");
#endif
                // Else, show that the dragged graph will go in front of the
                // graph that it is closest to
                } else {
                    SetGraphClass(regulonGraphs[closestGraphIndex], false, false);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[closestGraphIndex].Regulon.Genome.Name + " is currently closest.");
#endif
                }
            }
        }

        /// <summary>
        /// Listens for when a drag event leaves the container
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Container_DragLeave(ElementEvent e)
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("DragLeave: " + e.Target.ID);
#endif
            e.PreventDefault();

            // Go through all of the graphs and make sure they are not
            // highlighted
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphClass(regulonGraphs[i], false, false);
            }
        }

        /// <summary>
        /// Listens for when a drag event ends on the container
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Container_DragEnd(ElementEvent e)
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("DragEnd: " + e.Target.ID);
#endif
            e.PreventDefault();

            // Go through all of the graphs and make sure they are not
            // highlighted
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphClass(regulonGraphs[i], false, false);
            }
        }

        /// <summary>
        /// Listens for when a drop event occurs on the container
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Container_Drop(ElementEvent e)
        {
            // Get the target from the event
            Element target = e.Target;
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Drop: " + target.ID);
#endif
            e.PreventDefault();

            //We want to place the dragged network in the position that's
            //closest to the place the user dragged to

            //Get the position of the mouse cursor
            int mouseX = e.ClientX;
            int mouseY = e.ClientY;
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Mouse location is " + mouseX + ", " + mouseY);
#endif
            // The current index of the graph that is closest to the mouse X
            // and Y
            int closestGraphIndex = -1;

            // The current closest distance between a non-dragged graph and the
            // mouse X and Y
            double closestDistance = double.MaxValue;

            //For every network in the container, check its actual distance
            //from the mouse cursor's position
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Determining the closest graph to the drop position");
#endif
            // For each graph in the list of graphs...
            for (int i = 0; i < regulonGraphs.Length; i++) {

                // Reset that graph's class to normal
                SetGraphClass(regulonGraphs[i], false, false);

                //Calculate the centre of the current network
                int graphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientWidth)/2;
                int graphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientHeight)/2;

                //Determine the distance between the current centre and the
                //mouse's position using Pythagoras
                double currentDistance = Math.Sqrt(Math.Pow(mouseX - graphX, 2) + Math.Pow(mouseY - graphY, 2));
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[i].Regulon.Genome.Name + " centre is " + currentDistance + " away.");
#endif

                //If the calculated distance is less than the current closest,
                //this network is the new closest
                if (currentDistance < closestDistance) {
                    closestDistance = currentDistance;
                    closestGraphIndex = i;
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[i].Regulon.Genome.Name + " is the new closest.");
#endif
                }
            }

            //If we found a closest network, place the dragged network in front
            //of the closest one (in essence, putting it in its place)
            if (closestGraphIndex > -1) {

                //However, if it's:
                //- Closest to the last network in the container (if the container
                //is not empty), and the drop point was to the right, or completely
                //underneath, or
                //- To the right and below the top left corner of the last
                //  network
                //      - put the dropped network last
                //- To the left and below the top left corner of the bottom
                //  left network
                //      - put the dropped network in place of that network

                // Get the X and Y coordinates of the "last" graph in the
                // container
                Element lastGraphElement = container.ChildNodes[container.ChildNodes.Length - 1];
                int lastGraphX = HtmlUtil.GetElementX(lastGraphElement);
                int lastGraphY = HtmlUtil.GetElementY(lastGraphElement);

                //Find the network that is at the bottom left
                int bottomLeftGraphIndex = -1;
                int bottomLeftGraphX = int.MaxValue;

                // For each graph in the list of graphs
                for (int i = 0; i < regulonGraphs.Length; i++) {

                    // Get that graph's X and Y coordinates
                    int currentGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                    int currentGraphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement);

                    // If this graph is on the same Y value as the last graph,
                    // and its X coordinate is less than the X coordinate of
                    // the current "bottom left" network, store it and its
                    // X coordinate
                    if (currentGraphY == lastGraphY && currentGraphX < bottomLeftGraphX) {
                        bottomLeftGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                        bottomLeftGraphIndex = i;
                    }
                }

                // If there is a "last" graph and the mouse X and Y is greater
                // than that graph's X and Y, put the dragged graph at the end
                // of the container
                if (lastGraphX != int.MinValue && ((mouseX > lastGraphX) && (mouseY > lastGraphY))) {
                    container.AppendChild(draggedObject);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed at the end of the container");
#endif
                // Else, if there is a "bottom left" graph and the mouse X is
                // less than its X coordinate, and more than its Y cooridnate,
                // put the dragged graph in front of that graph
                } else if (bottomLeftGraphX != int.MaxValue && ((mouseX < bottomLeftGraphX) && (mouseY > lastGraphY))) {
                    container.InsertBefore(draggedObject, regulonGraphs[bottomLeftGraphIndex].DomElement);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed in front of " + regulonGraphs[bottomLeftGraphIndex].Regulon.Genome.Name);
#endif
                // Else, put the graph in front of the graph that it is closest
                // to
                } else {
                    container.InsertBefore(draggedObject, regulonGraphs[closestGraphIndex].DomElement);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed in front of " + regulonGraphs[closestGraphIndex].Regulon.Genome.Name);
#endif
                }
            }

            //Else append it to the end of the container - this should only
            //occur if the container is empty
            else {
                container.AppendChild(draggedObject);
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("A closest graph could not be found, appending the dropped object to the end of the container.");
#endif
            }

            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();
        }

        /// <summary>
        /// Ensures that elements of the graphs do not change size when the
        /// display is zoomed in or out
        /// </summary>
        /// <param name="graph">The graph that is changing size</param>
        /// <param name="newWidth">The new width of the graph</param>
        /// <param name="newHeight">The new height of the graph</param>
        protected void SetGraphSize(RegulonGraph graph, double newWidth, double newHeight)
        {

            // Calculate the factor that will be used to change the size
            double factor = newHeight / Double.Parse(graph.GraphSvg.GetAttribute<string>("height"));

            // Set the graph's SVG container to the new width and height
            graph.GraphSvg.SetAttribute("width", newWidth + Constants.RegulonDisplayUnits);
            graph.GraphSvg.SetAttribute("height", newHeight + Constants.RegulonDisplayUnits);

            // For every node in the graph, change their size by the calculated
            // factor
            foreach (AbstractNode node in graph.Nodes) {

                // Each different shape of node requires different handling
                // For circular nodes
                if (node.Node is SvgCircle) {
                    (node.Node as SvgCircle).Radius = (node.Node as SvgCircle).Radius / factor;
                }

                // For rectangular nodes
                if (node.Node is SvgRectangle) {
                    SvgRectangle rectangle = (node.Node as SvgRectangle);
                    rectangle.Height = rectangle.Height / factor;
                    rectangle.Width = rectangle.Width / factor;
                    rectangle.X = rectangle.X / factor;
                    rectangle.Y = rectangle.Y / factor;
                }

                // For polygonal nodes
                if (node.Node is SvgPolygon) {
                    SvgPolygon polygon = (node.Node as SvgPolygon);
                    polygon.Height = polygon.Height / factor;
                    polygon.Width = polygon.Width / factor;
                }

                // Scale the width of lines/spokes for target genes
                if (node is GeneNode) {

                    // The window size threshold indicates when graph elements should
                    // gain additional size when the display is big enough, or vice versa
                    if (windowSizeThresholdPassed) {
                        if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                            (node as GeneNode).Link.StrokeWidth = (node as GeneNode).Link.StrokeWidth / (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                        } else {
                            (node as GeneNode).Link.StrokeWidth = (node as GeneNode).Link.StrokeWidth * (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                        }
                    }
                }

                // See above comment for window size threshold
                if (windowSizeThresholdPassed) {
                    if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                        node.Node.StrokeWidth = node.Node.StrokeWidth / (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                    } else {
                        node.Node.StrokeWidth = node.Node.StrokeWidth * (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                    }
                }
            }

            // Scale each regulator in the graph as well by the calculated
            // factor
            foreach (RegulatorNode node in graph.RegulatorNodes) {

                // Each different shape of node requires different handling
                // For circular nodes
                if (node.Node is SvgCircle) {
                    (node.Node as SvgCircle).Radius = (node.Node as SvgCircle).Radius / factor;
                }

                // For rectangular nodes
                if (node.Node is SvgRectangle) {
                    SvgRectangle rectangle = (node.Node as SvgRectangle);
                    rectangle.Height = rectangle.Height / factor;
                    rectangle.Width = rectangle.Width / factor;
                    rectangle.X = rectangle.X / factor;
                    rectangle.Y = rectangle.Y / factor;
                }

                // For polygonal nodes
                if (node.Node is SvgPolygon) {
                    SvgPolygon polygon = (node.Node as SvgPolygon);
                    polygon.Height = polygon.Height / factor;
                    polygon.Width = polygon.Width / factor;
                }
            }
        }

        /// <summary>
        /// Whether the detail view should be showing all graphs (true) or a filtered set of graphs based on the selection (false)
        /// </summary>
        private bool detailViewAllGraphs = true;

        /// <summary>
        /// Used to store the current order of all the graphs in the display
        /// while a filtered detail view is visible, so that they can be
        /// restored when switching out of that zoom level
        /// </summary>
        private int[] graphOrder;
        private List<RegulonGraph> detailedViewGraphs;

        /// <summary>
        /// Gets the list of graphs that are currently visible in the detail view
        /// <para>This is used to set the comparison when in a filtered detail view and none of the regulons are currently selected</para>
        /// </summary>
        public List<RegulonGraph> DetailedViewGraphs
        {
            get { return detailedViewGraphs; }
        }

        /// <summary>
        /// Filters the display to show only selected graphs, while preserving the original order
        /// </summary>
        /*private void DetailView_FilterGraphs()
        {
            // Used to keep track of the un-filtered graph
            // order
            graphOrder = new int[container.ChildNodes.Length];
            Element currentChild = container.FirstChild;
            int index = 0;

            // Check each graph in the display and store its
            // order
            while (currentChild != null)
            {
                foreach (RegulonGraph graph in regulonGraphs)
                {
                    if (currentChild == graph.DomElement)
                    {
                        graphOrder[index] = regulonGraphs.IndexOf(graph);
                        break;
                    }
                    else
                    {
                        graphOrder[index] = -1;
                    }
                }
                currentChild = currentChild.NextSibling;
                index++;
            }

            // Remove all current regulon graphs from the
            // display
            foreach (RegulonGraph graph in regulonGraphs)
            {
                graph.RemoveElement();
            }

            // Add only the selected graphs back into the display
            foreach (RegulonGraph graph in detailedViewGraphs)
            {
                graph.AddElementTo(container);
            }
        }

        /// <summary>
        /// Undoes selected graph filtering, restoring all graphs to correct columns and order
        /// </summary>
        private void DetailView_RestoreGraphs()
        {
            // Remove all regulon graphs currently in the display
            foreach (RegulonGraph graph in regulonGraphs)
            {
                graph.RemoveElement();
            }

            // Make a list of all the graphs in the display in
            // their previous order
            List<RegulonGraph> orderedGraphs = new List<RegulonGraph>();
            foreach (int index in graphOrder)
            {
                if (index != -1)
                {
                    orderedGraphs.Add(regulonGraphs[index]);
                }
            }

            // Add all the networks in that list to the display
            foreach (RegulonGraph graph in orderedGraphs)
            {
                if (graph != null)
                {
                    graph.AddElementTo(container);
                }
            }

            // Clear the list of graphs in the detail view
            detailedViewGraphs.Clear();
        }*/

        /// <summary>
        /// Calls SetGraphAttributes for a graph when the zoom level is changed, depending on what zoom level was specified
        /// </summary>
        /// <param name="zoomLevel">The zoom level to change to, using the ZoomLevels enum</param>
        /// <param name="graphIndex">The index of the graph in the list of regulon graphs</param>
        virtual protected void SetGraphZoomLevel(ZoomLevels zoomLevel, int graphIndex)
        {
            switch (zoomLevel)
            {
                // Icon list specifies a box 150% the width of the default
                // small overview and 33% the height. The network is also moved
                // to the left of the box and the label to the right
                case ZoomLevels.IconList:
                    SetGraphAttributes(graphIndex, 1.5, (1/3), TextAnchorType.start, 0.75, 0, 100, new SvgRect(190, -100, 200, 200), false);
                    break;

                // Small overview is the default
                case ZoomLevels.SmallOverview:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;

                // Large overview is 150% larger than the default small
                // overview
                case ZoomLevels.LargeOverview:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(graphIndex, 1.5, 1.5, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1.5, 1.5, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;

                // Detail view is 375% larger than the default small
                // overview
                case ZoomLevels.DetailView:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(graphIndex, 3.75, 3.75, TextAnchorType.middle, 0.3, 120, 0, new SvgRect(-125, -108.75, 250, 250), true);
                    } else {
                        SetGraphAttributes(graphIndex, 3.75, 3.75, TextAnchorType.middle, 0.3, 100, 0, new SvgRect(-125, -125, 250, 250), true);
                    }
                    break;

                // If somehow an invalid zoom level is passed, use the default
                // small overview parameters
                default:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(graphIndex, 1, 1, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;
            }
        }

        /// <summary>
        /// Sets certain attributes of a graph
        /// </summary>
        /// <param name="graphIndex">The index of the graph in the list of regulon graphs</param>
        /// <param name="widthFactor">The multiplier that will be applied to the default graph width</param>
        /// <param name="heightFactor">The multiplier that will be applied to the default graph height</param>
        /// <param name="textAnchor">The alignment of the label text</param>
        /// <param name="textFactor">The multiplier that will be applied to the default font size of the label</param>
        /// <param name="labelY">The Y position of hte label</param>
        /// <param name="labelX">The X position of the label</param>
        /// <param name="newViewBox">The new SVG rectangle that with act as the graph's view box</param>
        /// <param name="nodeLabelVisibility">The visibility of the labels for each target gene node</param>
        protected virtual void SetGraphAttributes(int graphIndex, double widthFactor, double heightFactor, TextAnchorType textAnchor, double textFactor, double labelY, double labelX, SvgRect newViewBox, bool nodeLabelVisibility)
        {
            // Set the size of the graph's elements. This also applies an
            // additional multiplier to the width and height if the window size
            // is larger than a certain value
            if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                SetGraphSize(regulonGraphs[graphIndex], Constants.RegulonDisplayWidth * widthFactor * Constants.LargeScaleGraphScaleFactor, Constants.RegulonDisplayHeight * heightFactor * Constants.LargeScaleGraphScaleFactor);
            } else {
                SetGraphSize(regulonGraphs[graphIndex], Constants.RegulonDisplayWidth * widthFactor, Constants.RegulonDisplayHeight * heightFactor);
            }

            // Set the label's alignment
            regulonGraphs[graphIndex].GraphLabel.TextAnchor = textAnchor;

            // Set the label's font size
            if (Constants.UseGraphLabelWrapping) {

                // If the text in the label is too long, shrink the font size
                // depending on its length so that it is wholly visible
                int textLength = regulonGraphs[graphIndex].GraphLabel.Text.Length;
                regulonGraphs[graphIndex].GraphLabel.FontSize = textLength > 32 ? (Svg.ConvertLength(textFactor / 2.54 / (textLength / 32), Constants.RegulonDisplayHeight, 200)).ToString() : (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();
            } else {
                regulonGraphs[graphIndex].GraphLabel.FontSize = (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();
            }

            // Set the X and Y coordinates of the label
            regulonGraphs[graphIndex].GraphLabel.Y = labelY;
            regulonGraphs[graphIndex].GraphLabel.X = labelX;

            // Do the same for the second line of the label if label text
            // wrapping is enabled
            if (Constants.UseGraphLabelWrapping) {

                // Set the label's alignment
                regulonGraphs[graphIndex].GraphLabelLineTwo.TextAnchor = textAnchor;

                // Set the label's font size
                // If the text in the label is too long, shrink the font size
                // depending on its length so that it is wholly visible
                int textLength2 = regulonGraphs[graphIndex].GraphLabelLineTwo.Text.Length;
                regulonGraphs[graphIndex].GraphLabelLineTwo.FontSize = textLength2 > 32 ? (Svg.ConvertLength(textFactor / 2.54 / (textLength2 / 32), Constants.RegulonDisplayHeight, 200)).ToString() : (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();

                // Set the X and Y coordinates of the label
                regulonGraphs[graphIndex].GraphLabelLineTwo.Y = labelY;
                regulonGraphs[graphIndex].GraphLabelLineTwo.X = labelX;
            }

            // Set the graph's view box rectangle
            regulonGraphs[graphIndex].GraphSvg.ViewBox = newViewBox;

            // Set the graph's node label visibility
            regulonGraphs[graphIndex].SetNodeLabelVisibility(nodeLabelVisibility);

            // Set the position of the label's backgroud and its visibility (it
            // should not appear if the label is not centered - this is used
            // for the icon list)
            /*regulonGraphs[graphIndex].GraphLabelBackground.Y = labelY + (double.Parse(regulonGraphs[graphIndex].GraphLabel.FontSize) * 0.75);
            if (labelX != 0) {
                regulonGraphs[graphIndex].GraphLabelBackground.Visibility = "hidden";
            } else {
                regulonGraphs[graphIndex].GraphLabelBackground.Visibility = "visible";
            }*/
        }

/*#if DEBUG
        private void DebugCheckSelectedList()
        {
            if (Constants.ShowDebugMessages) Console.Log("Checking the initial selected list for this display");
            if (selectedGenes.Length == 0)
            {
                if (Constants.ShowDebugMessages) Console.Log("Nothing is selected!");
            }
            else
            {
                foreach (GeneInfo gene in selectedGenes)
                {
                    if (Constants.ShowDebugMessages) Console.Log(gene.Name + " in Regulon ID " + gene.RegulonId + " is selected.");
                }
                if (Constants.ShowDebugMessages) Console.Log(selectedGenes.Length + " nodes in total are selected.");
            }
        }
#endif*/

        /// <summary>
        /// Calculates the distances between a given set of regulons
        /// <para>Uses the "Ratio" variable to determine the ratio of Hamming and Euclidean distance to use. 0 = full Euclidean, 1 = full Hamming (default is 0.5)</para>
        /// </summary>
        /// <param name="regulons">The regulons to calculate the distance between</param>
        /// <returns>A matrix (array of arrays) of distances for each regulon to every other regulon in the supplied list</returns>
        private double[][] CalculateDistances(List<RegulonInfo> regulons)
        {
            // Make sure the ratio value is not out of bounds
            if (ratio < 0) {
                ratio = 0;
            } else if (ratio > 1) {
                ratio = 1;
            }
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Alpha value is " + ratio + " (Hamming weight = " + ratio + ", Euclidean weight = " + (1 - ratio) + ")");
#endif*/
            // Get the total number of possible TGs
            int n = geneDictionary.Count;

            //Initialise the distances table
            double[][] distances = new double[regulons.Length][];

            // Set all of the distances to 0 by default
            for (int i = 0; i < regulons.Length; i++) {
                distances[i] = new double[regulons.Length];
                for (int j = 0; j < regulons.Length; j++) {
                    distances[i][j] = 0;
                }
            }

            //Calculate both the Hamming distance and Euclidean distance for
            //each
            for (int i = 0; i < regulons.Length; i++) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Creating Hamming and Euclidean distances for " + regulons[i].GenomeName);
#endif*/
                for (int j = 0; j < regulons.Length; j++) {
                    double iEuclidean = 0;

                    //If we're not comparing this network to itself...
                    if (i != j) {
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Creating Hamming distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName);
#endif*/
                        //Create two vectors to store the presence of genes in
                        //each network
                        double[] iVector = new double[n];
                        double[] jVector = new double[n];
                        int count = 0;

                        //For each gene in the regulog, check if it is present
                        //in both. For Hamming distance, regulation is not
                        //relevant.
                        foreach (KeyValuePair<string, int> geneName in geneDictionary) {
                            iVector[count] = 0;
                            jVector[count] = 0;
                            double iValue = 0;
                            double jValue = 0;

                            //If the gene is in the network, it'll match one of
                            //the names in the list. If so, set the value to 1
                            //for Hamming distance, and for Eucildean distance,
                            //set the value to 1 if there's regulation, or 0.75
                            //if there's not
                            foreach (GeneInfo regulonGene in regulons[i].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (ratio > 0) {
/*#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " - setting the entry in iVector to '1'");
#endif*/
                                        iVector[count] = 1;
                                    }
                                    if (ratio < 1) {
                                        if (regulonGene.Site != null) {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " and is regulated - setting iValue to '1'");
#endif*/
                                            iValue = 1;
                                        } else {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " and is not regulated - setting iValue to '0.75'");
#endif*/
                                            iValue = 0.75;
                                        }
                                    }
                                        break;
                                }
                            }

                            //Do the same with the network that will be
                            //compared with the first
                            foreach (GeneInfo regulonGene in regulons[j].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (ratio > 0) {
/*#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " - setting the entry in jVector to '1'");
#endif*/
                                        jVector[count] = 1;
                                    }
                                    if (ratio < 1) {
                                        if (regulonGene.Site != null) {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " and is regulated - setting jValue to '1'");
#endif*/
                                            jValue = 1;
                                        } else {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " and is not regulated - setting jValue to '0.75'");
#endif*/
                                            jValue = 0.75;
                                        }
                                    }
                                    break;
                                }
                            }

                            //add (xn - yn)^2
                            iEuclidean = iEuclidean + Math.Pow((iValue - jValue), 2);
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("iVector[count] = " + iVector[count] + ", jVector[count] = " + jVector[count] + ", iValue = " + iValue + ", jValue = " + jValue + ", iEuclidean = " + iEuclidean);
#endif*/
                            count = count + 1;
                        }
                        double hammingDistance = 0;
                        for (int k = 0; k < n; k++) {
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("Gene " + k + " value for " + regulons[i].GenomeName + " is " + iVector[k] + " and value for " + regulons[j].GenomeName + " is " + jVector[k]);
#endif*/
                            if (iVector[k] != jVector[k]) {
                                hammingDistance = hammingDistance + Math.Abs(iVector[k] - jVector[k]);
                            }
                        }
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Hamming distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName + " is " + hammingDistance);
#endif*/
                        distances[i][j] = distances[i][j] + hammingDistance * ratio;

                        //Raise all the (xn - yn)^2 terms to the power of 1/2 and
                        //add it to the distance
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Euclidean distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName + " is " + Math.Pow(iEuclidean, 0.5));
#endif*/
                        distances[i][j] = distances[i][j] + (1 - ratio) * (Math.Pow(iEuclidean, 0.5));
                    } else {
                        distances[i][j] = 0;
                    }
                }
            }

            // Return the distances matrix
            return distances;
        }

        /// <summary>
        /// Calculates the centroid of a group
        /// </summary>
        /// <param name="regulons">The list of regulons in the display</param>
        /// <returns>The id of the network that is the centroid</returns>
        private int CalculateCentroid(List<RegulonInfo> regulons)
        {
            //Find out which network out of this group has the smallest
            //total of its distances. I am assuming this is the
            //"average" centroid
            double minDistanceTotal = double.MaxValue;
            int minDistanceTotalIndex = 0;
            for (int i = 0; i < currentDistances.Length; i++)
            {
                double total = 0;
                for (int j = 0; j < currentDistances[i].Length; j++)
                {
                    total = total + currentDistances[i][j];
                }
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Total of all distances for " + groupRegulons[k].GenomeName + " is " + total);
#endif*/
                if (total < minDistanceTotal)
                {
                    minDistanceTotal = total;
                    minDistanceTotalIndex = i;
/*#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("This is the new minimum for this group");
#endif*/
                }
            }

            //Set the new centroid of this group to be the one found
            //above
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("New centroid for group " + groups.IndexOf(group) + " will be " + groupRegulons[minDistanceTotalIndex].GenomeName);
#endif*/
            return regulons.IndexOf(regulons[minDistanceTotalIndex]);
        }

        /// <summary>
        /// Sorts by the distance of each network to the centroid
        /// <para>This needs to be done here rather than passing a function from the RegulonGraphViewer because it needs to access stuff that is not exposed outside</para>
        /// </summary>
        /// <param name="filter"></param>
        public void UpdateVisibleRegulonsSortByCentroidDistance(Func<RegulonInfo, bool> filter)
        {
            // If there are distances calculated for this regulog
            if (currentCentroid != -1) {

                // The list of graphs which will be eventually sorted
                List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

                // The regulons that will not be visible (to set their order
                // property correctly)
			    List<RegulonGraph> invisibleGraphs = new List<RegulonGraph>();

                // The centroid of the current data set
                RegulonGraph groupCentroid = regulonGraphs[currentCentroid];

                // Add all graphs that are not the centroid to the list
                foreach (RegulonGraph graph in regulonGraphs) {
                    if (graph != groupCentroid && (filter == null || filter( graph.Regulon ))) {
                        visibleGraphs.Add(graph);
                    } else {
                        invisibleGraphs.Add( graph );
                    }
                }

                // Sort the list of visible graphs in ascending order of
                // their distance from the centroid
                visibleGraphs.Sort(delegate(RegulonGraph x, RegulonGraph y) {

                    // Get the difference in distance between the two networks
                    int compareResult = (int)((currentDistances[selectedRegulog.Regulons.IndexOf(x.Regulon)][currentCentroid] -
                        currentDistances[selectedRegulog.Regulons.IndexOf(y.Regulon)][currentCentroid]) * 100);

                    // If the result comes out to 0 (the networks have the
                    // same distance) then sort by name instead, otherwise
                    // return the above difference
                    if (compareResult == 0) {
                        return String.Compare(x.Regulon.GenomeName, y.Regulon.GenomeName);
                    } else {
                        return compareResult;
                    }
                });

                // Insert the centroid at the start of the list
                if (groupCentroid != null && (filter == null || filter(groupCentroid.Regulon))) {
                    visibleGraphs.Insert(0, groupCentroid);
                }

                // Remove the existing regulon graphs from the display
			    foreach ( RegulonGraph graph in regulonGraphs ) {
				    graph.RemoveElement();
			    }

                // Add the regulon graphs that will be visible to the display
			    foreach ( RegulonGraph graph in visibleGraphs ) {
				    graph.AddElementTo( container );
			    }

                // Set the visible number of graphs
                numberOfGraphs = visibleGraphs.Length;

                // Ensure the stored order for each visible graph is correct
                RefreshGraphOrder();

                // If there are any invisible regulons...
                if (invisibleGraphs.Length > 1) {
                    // Sort the invisible regulons based on their already stored order
                    invisibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return x.Regulon.Order - y.Regulon.Order; } );

                    // Set the invisible regulons' order to be their current order, but
                    // to be after the visible regulons
                    for (int i = 0; i < invisibleGraphs.Length; i++) {
                        invisibleGraphs[i].Regulon.Order = i + visibleGraphs.Length;
                    }
                }

            // If there is not a centroid, default to sorting by the already stored order
            } else {
                UpdateVisibleRegulons(delegate( RegulonInfo x, RegulonInfo y ) { return x.Order - y.Order; }, filter);
            }
        }

        //Clumsy, but means we don't have to do a loop in the below function
		//protected List<Element> regulonGraphDomElements = new List<Element>();

        private void ContainerTouchMove(ElementEvent e)
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("ContainerTouchMove: " + e.Target.ID);
#endif
            e.PreventDefault();
#if DEBUG
            //Console.Log("default behaviour for touchmove blocked!");
#endif

            Element target = e.Target;

            //We want to place the dragged network in the position that's
            //closest to the place the user dragged to

            //Get the position of the first touch. Scroll is to account for
            //the scrolling of the page
            int touchX = (int)Script.Literal("Math.round(e.changedTouches[0].pageX)") + container.ScrollLeft;
            int touchY = (int)Script.Literal("Math.round(e.changedTouches[0].pageY)") + container.ScrollTop;
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("First touch location is " + touchX + ", " + touchY);
#endif

            //These variables are to keep track of which is closest
            int closestGraphIndex = -1;
            double closestDistance = double.MaxValue;

            //For every network in the container, check its actual distance
            //from the mouse cursor's position
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Determining the closest graph to the drop position");
#endif
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphClass(regulonGraphs[i], false, false);

                //Calculate the centre of the current network
                int graphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientWidth)/2;
                int graphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientHeight)/2;

                //Determine the distance between the current centre and the
                //mouse's position using Pythagoras
                double currentDistance = Math.Sqrt(Math.Pow(touchX - graphX, 2) + Math.Pow(touchY - graphY, 2));
#if DEBUG
                //Console.Log(regulonGraphs[i].Regulon.Genome.Name + " centre is " + currentDistance + " away.");
#endif

                //If the calculated distance is less than the current closest,
                //this network is the new closest
                if (currentDistance < closestDistance) {
                    closestDistance = currentDistance;
                    closestGraphIndex = i;
#if DEBUG
                    //Console.Log(regulonGraphs[i].Regulon.Genome.Name + " is the new closest.");
#endif
                }
            }

            //If we found a closest network, highlight it
            if (closestGraphIndex > -1) {

                //But only highlight it if the network would replace it. If it is
                //to the right or below the last graph, and depending on how close
                //it is, it will go at the end and so we won't highlight anything

                //If it's:
                //- Closest to the last network in the container (if the container
                //is not empty), and the drop point was to the right, or completely
                //underneath, or
                //- Not closest, but to the right of the last network AND at least
                //below the centre of the last network;
                //It will go last, and so nothing needs to be highlighted
                // commented out for the moment

                //- To the right and below the top left corner of the last
                //  network
                //      - put the dropped network last
                //- To the left and below the top left corner of the bottom
                //  left network
                //      - put the dropped network in place of that network
                Element lastGraphElement = container.ChildNodes[container.ChildNodes.Length - 1];
                int lastGraphX = HtmlUtil.GetElementX(lastGraphElement);
                int lastGraphY = HtmlUtil.GetElementY(lastGraphElement);

                //Find the network that is at the bottom left
                int bottomLeftGraphIndex = -1;
                int bottomLeftGraphX = int.MaxValue;
                for (int i = 0; i < regulonGraphs.Length; i++) {
                    int currentGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                    int currentGraphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement);
                    if (currentGraphY == lastGraphY && currentGraphX < bottomLeftGraphX) {
                        bottomLeftGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                        bottomLeftGraphIndex = i;
                    }
                }
                if (lastGraphX != int.MinValue && ((touchX > lastGraphX) || (touchY > lastGraphY))) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object will go at the end of the container.");
#endif
                } else if (bottomLeftGraphX != int.MaxValue && ((touchX < bottomLeftGraphX) && (touchY > lastGraphY))) {
                    SetGraphClass(regulonGraphs[bottomLeftGraphIndex], false, true);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object would go in front of " + regulonGraphs[bottomLeftGraphIndex].Regulon.Genome.Name + ".");
#endif
                } else {
                    SetGraphClass(regulonGraphs[closestGraphIndex], false, true);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[closestGraphIndex].Regulon.Genome.Name + " is currently closest.");
#endif
                }
            }
        }

        /// <summary>
        /// Javascript Touch Start event, when a user touches one of the graphs
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Graph_TouchStart(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "TouchStart: " + target.ID );
#endif
			e.PreventDefault();

            // Set the current dragged objecct
			draggedObject = target;
            e.StopPropagation();

            // Set the dragged object to have a solid border
            target.ChildNodes[0].Style.Border = "1px solid black";
        }

        /// <summary>
        /// Javascript Touch Move event, when a user moves their finger on the screen after touching one of the graphs
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Graph_TouchMove(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "TouchMove: " + target.ID );
#endif
			e.PreventDefault();
            e.StopPropagation();

            //We want to place the dragged network in the position that's
            //closest to the place the user dragged to

            //Get the position of the first touch. Scroll is to account for
            //the scrolling of the page
            int touchX = (int)Script.Literal("Math.round(e.changedTouches[0].pageX)") + container.ScrollLeft;
            int touchY = (int)Script.Literal("Math.round(e.changedTouches[0].pageY)") + container.ScrollTop;
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("First touch location is " + touchX + ", " + touchY);
#endif
            // The current index of the graph that is closest to the mouse X
            // and Y
            int closestGraphIndex = -1;

            // The current closest distance between a non-dragged graph and the
            // mouse X and Y
            double closestDistance = double.MaxValue;

            //For every network in the container, check its actual distance
            //from the mouse cursor's position
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Determining the closest graph to the drop position");
#endif
            // For each graph in the list of graphs...
            for (int i = 0; i < regulonGraphs.Length; i++) {

                // Reset that graph's class to normal
                SetGraphClass(regulonGraphs[i], false, false);

                //Calculate the centre of the current network
                int graphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientWidth)/2;
                int graphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientHeight)/2;

                //Determine the distance between the current centre and the
                //mouse's position using Pythagoras
                double currentDistance = Math.Sqrt(Math.Pow(touchX - graphX, 2) + Math.Pow(touchY - graphY, 2));
#if DEBUG
                //Console.Log(regulonGraphs[i].Regulon.Genome.Name + " centre is " + currentDistance + " away.");
#endif
                //If the calculated distance is less than the current closest,
                //this network is the new closest
                if (currentDistance < closestDistance) {
                    closestDistance = currentDistance;
                    closestGraphIndex = i;
#if DEBUG
                    //Console.Log(regulonGraphs[i].Regulon.Genome.Name + " is the new closest.");
#endif
                }
            }

            //If we found a closest network, highlight it
            if (closestGraphIndex > -1) {

                //But only highlight it if the network would replace it. If it is
                //to the right or below the last graph, and depending on how close
                //it is, it will go at the end and so we won't highlight anything

                //If it's:
                //- Closest to the last network in the container (if the container
                //is not empty), and the drop point was to the right, or completely
                //underneath, or
                //- To the right and below the top left corner of the last
                //  network
                //      - put the dropped network last
                //- To the left and below the top left corner of the bottom
                //  left network
                //      - put the dropped network in place of that network

                // Get the X and Y coordinates of the "last" graph in the
                // container
                Element lastGraphElement = container.ChildNodes[container.ChildNodes.Length - 1];
                int lastGraphX = HtmlUtil.GetElementX(lastGraphElement);
                int lastGraphY = HtmlUtil.GetElementY(lastGraphElement);

                //Find the network that is at the bottom left
                int bottomLeftGraphIndex = -1;
                int bottomLeftGraphX = int.MaxValue;

                // For each graph in the list of graphs
                for (int i = 0; i < regulonGraphs.Length; i++) {

                    // Get that graph's X and Y coordinates
                    int currentGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                    int currentGraphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement);

                    // If this graph is on the same Y value as the last graph,
                    // and its X coordinate is less than the X coordinate of
                    // the current "bottom left" network, store it and its
                    // X coordinate
                    if (currentGraphY == lastGraphY && currentGraphX < bottomLeftGraphX) {
                        bottomLeftGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                        bottomLeftGraphIndex = i;
                    }
                }

                // If there is a "last" graph and the mouse X and Y is greater
                // than that graph's X and Y, show that the dragged graph will
                // go at the end of the container
                if (lastGraphX != int.MinValue && ((touchX > lastGraphX + lastGraphElement.ChildNodes[0].ClientWidth && touchY > lastGraphY)
                    || (touchY > lastGraphY + lastGraphElement.ChildNodes[0].ClientHeight && touchX > lastGraphX))) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object will go at the end of the container.");
#endif
                // Else, if there is a "bottom left" graph and the mouse X is
                // less than its X coordinate, and more than its Y cooridnate,
                // show that the dragged graph will go in front of that graph
                } else if (bottomLeftGraphX != int.MaxValue && ((touchX < bottomLeftGraphX) && (touchY > lastGraphY))) {
                    SetGraphClass(regulonGraphs[bottomLeftGraphIndex], false, true);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object would go in front of " + regulonGraphs[bottomLeftGraphIndex].Regulon.Genome.Name + ".");
#endif
                // Else, show that the dragged graph will go in front of the
                // graph that it is closest to
                } else {
                    SetGraphClass(regulonGraphs[closestGraphIndex], false, true);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[closestGraphIndex].Regulon.Genome.Name + " is currently closest.");
#endif
                }
            }
		}

        /// <summary>
        /// Javascript Touch End event, when a user releases their finger on the screen after touching one of the graphs
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Graph_TouchEnd(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "TouchEnd: " + target.ID );
#endif
			e.PreventDefault();
            e.StopPropagation();

            //Element temp = draggedObject.ParentNode;

            //We want to place the dragged network in the position that's
            //closest to the place the user dragged to

            //Get the position of the first touch. Scroll is to account for
            //the scrolling of the page
            int touchX = (int)Script.Literal("Math.round(e.changedTouches[0].pageX)") + container.ScrollLeft;
            int touchY = (int)Script.Literal("Math.round(e.changedTouches[0].pageY)") + container.ScrollTop;
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("First touch location is " + touchX + ", " + touchY);
#endif
            // The current index of the graph that is closest to the mouse X
            // and Y
            int closestGraphIndex = -1;

            // The current closest distance between a non-dragged graph and the
            // mouse X and Y
            double closestDistance = double.MaxValue;

            //For every network in the container, check its actual distance
            //from the mouse cursor's position
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Determining the closest graph to the drop position");
#endif
            // For each graph in the list of graphs...
            for (int i = 0; i < regulonGraphs.Length; i++) {

                // Reset that graph's class to normal
                SetGraphClass(regulonGraphs[i], false, false);

                //Calculate the centre of the current network
                int graphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientWidth)/2;
                int graphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement) + (regulonGraphs[i].GraphSvg.DomElement.ClientHeight)/2;

                //Determine the distance between the current centre and the
                //mouse's position using Pythagoras
                double currentDistance = Math.Sqrt(Math.Pow(touchX - graphX, 2) + Math.Pow(touchY - graphY, 2));
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[i].Regulon.Genome.Name + " centre is " + currentDistance + " away.");
#endif

                //If the calculated distance is less than the current closest,
                //this network is the new closest
                if (currentDistance < closestDistance) {
                    closestDistance = currentDistance;
                    closestGraphIndex = i;
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[i].Regulon.Genome.Name + " is the new closest.");
#endif
                }
            }

            //If we found a closest network, place the dragged network in front
            //of the closest one (in essence, putting it in its place)
            if (closestGraphIndex > -1) {

                //However, if it's:
                //- Closest to the last network in the container (if the container
                //is not empty), and the drop point was to the right, or completely
                //underneath, or
                //- To the right and below the top left corner of the last
                //  network
                //      - put the dropped network last
                //- To the left and below the top left corner of the bottom
                //  left network
                //      - put the dropped network in place of that network

                // Get the X and Y coordinates of the "last" graph in the
                // container
                Element lastGraphElement = container.ChildNodes[container.ChildNodes.Length - 1];
                int lastGraphX = HtmlUtil.GetElementX(lastGraphElement);
                int lastGraphY = HtmlUtil.GetElementY(lastGraphElement);

                //Find the network that is at the bottom left
                int bottomLeftGraphIndex = -1;
                int bottomLeftGraphX = int.MaxValue;

                // For each graph in the list of graphs
                for (int i = 0; i < regulonGraphs.Length; i++) {

                    // Get that graph's X and Y coordinates
                    int currentGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                    int currentGraphY = HtmlUtil.GetElementY(regulonGraphs[i].DomElement);

                    // If this graph is on the same Y value as the last graph,
                    // and its X coordinate is less than the X coordinate of
                    // the current "bottom left" network, store it and its
                    // X coordinate
                    if (currentGraphY == lastGraphY && currentGraphX < bottomLeftGraphX) {
                        bottomLeftGraphX = HtmlUtil.GetElementX(regulonGraphs[i].DomElement);
                        bottomLeftGraphIndex = i;
                    }
                }

                // If there is a "last" graph and the mouse X and Y is greater
                // than that graph's X and Y, put the dragged graph at the end
                // of the container
                if (lastGraphX != int.MinValue && ((touchX > lastGraphX + lastGraphElement.ChildNodes[0].ClientWidth && touchY > lastGraphY)
                    || (touchY > lastGraphY + lastGraphElement.ChildNodes[0].ClientHeight && touchX > lastGraphX))) {
                    container.AppendChild(draggedObject);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed at the end of the container");
#endif
                // Else, if there is a "bottom left" graph and the mouse X is
                // less than its X coordinate, and more than its Y cooridnate,
                // put the dragged graph in front of that graph
                } else if (bottomLeftGraphX != int.MaxValue && ((touchX < bottomLeftGraphX) && (touchY > lastGraphY))) {
                    container.InsertBefore(draggedObject, regulonGraphs[bottomLeftGraphIndex].DomElement);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed in front of " + regulonGraphs[bottomLeftGraphIndex].Regulon.Genome.Name);
#endif
                // Else, put the graph in front of the graph that it is closest
                // to
                } else {
                    container.InsertBefore(draggedObject, regulonGraphs[closestGraphIndex].DomElement);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed in front of " + regulonGraphs[closestGraphIndex].Regulon.Genome.Name);
#endif
                }
            }

            //Else append it to the end of the container - this should only
            //occur if the container is empty
            else {
                container.AppendChild(draggedObject);
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("A closest graph could not be found, appending the dropped object to the end of the container.");
#endif
            }

            //Make sure everything is the correct colour
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphClass(regulonGraphs[i], false, false);
            }

            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();
        }

        /// <summary>
        ///Javascript Touch Cancel event, when something interrupts a touch initiated from a graph
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Graph_TouchCancel(ElementEvent e)
        {
            // Find the parent span of the target, to use it as the actual target
			Element target = FindParentOfType( e.Target, "span" );
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("TouchCancel: " + e.Target.ID);
#endif
            e.PreventDefault();

            //Make sure everything is the correct colour
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphClass(regulonGraphs[i], false, false);
            }
        }

        //Used to check if the window size has passed the threshold for
        //increasing the size of regulon graphs
        protected int currentWindowHeight = 0;
        protected int currentWindowWidth = 0;
        protected bool windowSizeThresholdPassed = false;

        /// <summary>
        /// If the browser window is resized, check if the display has passed
        /// the threshold for "large scale" and adjust the graph sizes if so
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        protected void Window_OnResize(ElementEvent e)
        {
            // If the window's height or width has passed one of the thresholds
            if ((Window.OuterWidth > Constants.LargeScaleWidthThreshold && currentWindowWidth <= Constants.LargeScaleWidthThreshold) ||
                (Window.OuterWidth <= Constants.LargeScaleWidthThreshold && currentWindowWidth > Constants.LargeScaleWidthThreshold) ||
                (Window.OuterHeight > Constants.LargeScaleHeightThreshold && currentWindowHeight <= Constants.LargeScaleHeightThreshold) ||
                (Window.OuterHeight <= Constants.LargeScaleHeightThreshold && currentWindowHeight > Constants.LargeScaleHeightThreshold)) {

                // Set the threshold as being passed and then change the size
                // of the graphs
                windowSizeThresholdPassed = true;
                for (int i = 0; i < regulonGraphs.Length; i++) {
                    SetGraphZoomLevel(currentZoomLevel, i);
                }

                // Clear the status of passing the window size threshold
                windowSizeThresholdPassed = false;
            }

            // Store the current window height and width
            currentWindowWidth = Window.OuterWidth;
            currentWindowHeight = Window.OuterHeight;
        }

        /// <summary>
        /// Alerts listeners if the selected genes have changed
        /// </summary>
        protected void OnSelectedGenesChanged() {
            if (SelectedGenesChanged != null) {
                SelectedGenesChanged(this, null);
            }
        }

#if DEBUG
        /// <summary> Returns the contents of a file as a string.
		/// </summary>
		/// <param name="fileName">The path (relative or absolute) of the file.</param>
		/// <param name="process"> A delegate that will be invoked with the content of the file in the event of a successful call, or with null otherwise. </param>
		/// <returns>Returns the content of the file as a string.</returns>

		public static void ReadAllText ( string fileName, Action<string> process ) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.Open( HttpVerb.Get, fileName, false );
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						process( req.ResponseText );
					}
					else {
						if (Constants.ShowDebugMessages) Console.Log( string.Format( "ReadAllText( \"{0}\" )", fileName ) );
						process( null );
					}
				}
			};
            Script.Literal("req.onloadend = function(event) { if (req.status == 404) {console.log('No document retrieved.') } }");
            try {
			    req.Send();
            }
            catch (Exception ex) {
                if (Constants.ShowDebugMessages) Console.Log("req.Send() failed because: " + ex.Message);
            }
		}
#endif

        // Activated whenether a property changes in the display manager
		public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called whenever a property changes in the display manager. Used to inform listeners when that happens
        /// </summary>
        /// <param name="info">The properties that were changed</param>
        public void NotifyPropertyChanged(String info)
        {
            // If the property changed event is active
            if (PropertyChanged != null)
            {
                // Trigger a property changed event
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        // Whether this display manager is processing something that should
        // show the loading overlag
        protected bool isProcessing = false;

        // The levels of processing the gene selection function is in, used to
        // tell when processing starts and finishes while selecting homologues
        protected int numProcessing = 0;

        /// <summary>
        /// Returns whether the current display manager is processing or not
        /// <para>Used by parent RegulonGraphDisplays to show or hide the loading overlay</para>
        /// </summary>
        public bool IsProcessing
        {
            get { return isProcessing; }
        }

        /// <summary>
        /// Sets a graph's class depending on its current characteristics
        /// </summary>
        /// <param name="graph">The graph to set the class for</param>
        /// <param name="dragged">Whether the graph is currently being dragged</param>
        /// <param name="highlighted">Whether the graph should be highlighted (used when it is being shown as the graph that a dragged graph will take the place of)</param>
        protected void SetGraphClass (RegulonGraph graph, bool dragged, bool highlighted) {

            // Initialise the class string
            string classText = String.Empty;

            // If the graph is a centroid, begin with the centroid class,
            // otherwise begin with the regular class
            if (currentCentroid != -1 && selectedRegulog.Regulons.IndexOf(graph.Regulon) == currentCentroid) {
                classText = "RegulonGraph_Centroid";
            } else {
                classText = "RegulonGraph";
            }

            // If the graph is selected, add the selected class
            if (graph.IsSelected) {
                classText = classText + " RegulonGraph_Selected";
            }

            // If the graph is being dragged, add the dragged class
            if (dragged) {
                classText = classText + " RegulonGraph_Dragged";
            }

            // If the graph should be highlighted, add the highlighted class
            if (highlighted) {
                classText = classText + " RegulonGraph_Highlighted";
            }

            // Apply the class string to the graph
            graph.GraphSvg.SetAttribute("class", classText);
        }

/*#if DEBUG
        /// <summary>
        /// Retrieves the current order of all graphs in the display
        /// <para>Graphs should probably internally store their current position in the display so this doesn't have to be done each time</para>
        /// </summary>
        /// <returns>An array that contains the current position of each network in the display, by their index in the data set's list of regulons</returns>
        public int[] GetGraphOrder() {

            // Initialise a list that is the same length as the amount of
            // networks in the display
            int[] currentGraphOrder = new int[selectedRegulog.Regulons.Length];

            // Set the current graph to check as the first graph in the
            // container, and the index as zero
            Element currentChild = container.FirstChild;
            int index = 0;

            // If in a filtered detail view, the order can be taken directly
            // from the temp order stored from filtering
            if (!detailViewAllGraphs && currentZoomLevel == ZoomLevels.DetailView) {

                // Copy the ordering over
                for (int i = 0; i < graphOrder.Length; i++) {
                    currentGraphOrder[graphOrder[i]] = i;
                }

            // Otherwise, determine the current order
            } else {

                // Make a temporary list of graphs
                // (I'm not sure why this is done)
                List<RegulonGraph> tempGraphs = regulonGraphs;

                // While there is a current graph
                while (currentChild != null) {

                    // For each graph in the temporary list
                    foreach (RegulonGraph graph in tempGraphs) {
    #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Checking if this graph is " + graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ")");
    #endif
                        // If the current graph has been found in the list
                        if (currentChild == graph.DomElement) {
    #if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ") is at " + index);
    #endif
                            // Store the current graph's current position as
                            // the index value, using the position of the
                            // regulon in the data set as the position in the
                            // array
                            currentGraphOrder[selectedRegulog.Regulons.IndexOf(graph.Regulon)] = index;
                            break;
                        } //else {
                        //    currentGraphOrder[selectedRegulog.Regulons.IndexOf(graph.Regulon)] = -1;
                        //}
                    }

                    // Go to the next graph in the container
                    currentChild = currentChild.NextSibling;

                    // Increment the index
                    index++;
                }
            }

            // Return the order
            return currentGraphOrder;
        }
#endif*/

        /// <summary>
        /// Refreshes every graph in the display
        /// </summary>
        public void RefreshGraphs() {
            foreach (RegulonGraph graph in regulonGraphs) {
                graph.Refresh();
            }
        }

        /// <summary>
        /// Refreshes the currently stored order for all visible regulon graphs in the display
        /// <para>Graphs not currently in the main container are unaffected</para>
        /// </summary>
        private void RefreshGraphOrder() {
            // Set the current graph to check as the first graph in the
            // container, and the index as zero
            Element currentChild = container.FirstChild;
            int index = 0;

            // While there is a current graph
            while (currentChild != null) {

                // For each graph in the temporary list
                foreach (RegulonGraph graph in regulonGraphs) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Checking if this graph is " + graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ")");
#endif
                    // If the current graph has been found in the list
                    if (currentChild == graph.DomElement) {
#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ") is at " + index);
#endif
                        // Store the current graph's current position as
                        // the index value
                        graph.Regulon.Order = index;
                        break;
                    }
                }

                // Go to the next graph in the container
                currentChild = currentChild.NextSibling;

                // Increment the index
                index++;
            }
        }
	}
}