﻿using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.RegPreciseView {

	//TESTING
	public class SamsRegulonDisplay : DefaultRegulonDisplay {

        //private List<Element> otherElements;
        
		override public void DisplayRegulons(
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
            IAttributeFactory colourChooser
		) {
			base.DisplayRegulons( regulons, container, selectedRegulog, nodeFactory, colourChooser );
		}

		/*public void DisplayRegulonsWithExtras(
			IEnumerable<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser,
			List<Element> otherElements
		) {
			List<RegulonGraph> PathogenicGraphs = new List<RegulonGraph>();
			List<RegulonGraph> NormalGraphs = new List<RegulonGraph>();

            this.otherElements = otherElements;

			foreach ( RegulonInfo regulon in regulons ) {
				RegulonGraph regulonGraph;
				if ( regulon.Genome.GenomeStats["isPathogenic"] != null && (string) regulon.Genome.GenomeStats["isPathogenic"] == "true" ) {
					//regulon.GenomeName += " (pathogenic)";
					regulonGraph = CreateGraph( selectedRegulog, nodeFactory, colourChooser, regulon );
					PathogenicGraphs.Add( regulonGraph );
				}
				else {
					regulonGraph = CreateGraph( selectedRegulog, nodeFactory, colourChooser, regulon );
					NormalGraphs.Add( regulonGraph );
				}

				//ImageElement test = new ImageElement();
				//test.Alt = "this is a test image";
				//test.Src = "http://hard-light.net/wiki/images/Gtvacolossus-old.jpg";
				//container.AppendChild(test);
				//otherElements.Add(test);
			}

			foreach ( RegulonGraph regulonGraph in PathogenicGraphs ) {
				regulonGraph.AddElementTo( container );
				regulonGraphs.Add( regulonGraph );
			}

			SystemQut.Svg.SvgLine line = new SystemQut.Svg.SvgLine( null, 0, 0, 0, 300, null );

			foreach ( RegulonGraph regulonGraph in NormalGraphs ) {
				regulonGraph.AddElementTo( container );
				regulonGraphs.Add( regulonGraph );
			}
		}*/

		private static RegulonGraph CreateGraph(
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser,
			RegulonInfo regulon
		) {
			RegulonGraph regulonGraph = new RegulonGraph( selectedRegulog, regulon, nodeFactory, colourChooser,
				Constants.RegulonDisplayWidth,
				Constants.RegulonDisplayHeight,
				Constants.RegulonDisplayUnits
			);
			return regulonGraph;
		}
	}
}
