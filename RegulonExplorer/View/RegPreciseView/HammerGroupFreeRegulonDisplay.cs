﻿using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.Common;
using SystemQut;
using SystemQut.Svg;
using Hammer;

namespace RegulonExplorer.View.RegPreciseView
{

    /// <summary>
    /// A variant of the Hammer regulon display that allows grouping of graphs.
    /// Allows changing of group number without needing to be recreated, and
    /// works fine with only one group to simplify enabling and disabling
    /// groups
    /// </summary>
    class HammerGroupFreeRegulonDisplay : HammerFreeRegulonDisplay
    {

        /// <summary> Maintain a list of the DivElement columns that are used as groups.
        /// </summary>
        
        private List<DivElement> groups = new List<DivElement>();

        /// <summary> Used to colour groups to distinguish them
        /// </summary>
        
		private string[] groupColours = { "#ffdddd", "#ddffdd", "#ddddff", "#ffffdd", "#ffddff", "ddffff" };

        /// <summary> Stores the current number of groups
        /// </summary>

        private int numberOfGroups;

        public int NumberOfGroups
        {
            get { return numberOfGroups; }
        }

        public List<RegulonGraph> RegulonGraphs
        {
            get { return regulonGraphs; }
        }

        // Contains the current distances between regulons and the current
        // centroids for each group
        private List<int> currentCentroids = new List<int>();

        public List<int> CurrentCentroids
        {
            get { return currentCentroids; }
        }

        /// <summary> Displays the list of regulons in the specified container
		/// </summary>
		/// <param name="regulons"></param>
		/// <param name="container"></param>
		/// <param name="selectedRegulog"></param>
		/// <param name="nodeFactory"></param>
		/// <param name="colourChooser"></param>
        /// 
        override public void DisplayRegulons(
            List<RegulonInfo> regulons,
            Element container,
            RegulogInfo selectedRegulog,
            INodeFactory nodeFactory,
            IAttributeFactory colourChooser
        )
        {
            DisplayRegulonsWithExtraGroups(regulons, container, selectedRegulog, nodeFactory, colourChooser, 1, false);
        }

        /// <summary> Displays the list of regulons in the specified container, while specifing the number of groups and whether to automatically group them
        /// </summary>
        /// <param name="regulons"></param>
        /// <param name="container"></param>
        /// <param name="selectedRegulog"></param>
        /// <param name="nodeFactory"></param>
        /// <param name="colourChooser"></param>
        /// <param name="numberOfGroups"></param>
        /// <param name="automatic"></param>

		public void DisplayRegulonsWithExtraGroups(
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser,
            int numberOfGroups,
            bool automatic
		) {
            this.selectedRegulog = selectedRegulog;
            this.container = container;

            if (containerHammer != null) {
                containerHammer.Destroy();
            }
            containerHammer = new Hammer.Hammer(container.ParentNode);
            
            JSObject managerOptions = new JSObject();
            managerOptions["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
            managerOptions["domEvents"] = true; //http://hammerjs.github.io/tips/

            containerHammer.On( "press" , ContainerOnPress );

            this.container.Style.Height = "auto";
            this.container.Style.Width = "auto";
            
            //remove all the children this regulon display has
            while (container.ChildNodes[0] != null)
            {
                container.RemoveChild(container.ChildNodes[0]);
            }

            this.numberOfGroups = numberOfGroups;

            if (numberOfGroups > 1) {
                SetUpGroupDivs(regulons.Length, container);
            }

            foreach (RegulonInfo regulon in regulons)
            {
                RegulonGraph regulonGraph = new RegulonGraph(
                    selectedRegulog,
                    regulon,
                    nodeFactory,
                    colourChooser,
                    Constants.RegulonDisplayWidth,
                    Constants.RegulonDisplayHeight,
                    Constants.RegulonDisplayUnits
                );

                regulonGraph.DomElement.ID = regulon.GenomeName;

                foreach (GeneInfo geneInfo in regulonGraph.Regulon.Genes)
                {
                    ((SystemQut.ComponentModel.INotifyPropertyChanged)geneInfo).PropertyChanged += node_PropertyChanged;
                    if (geneInfo.IsSelected) {
                        selectedGenes.Add(geneInfo);
                    }
                }

                SetGraphClass(regulonGraph, false, false);
                regulonGraph.AddElementTo(container);
                regulonGraphs.Add(regulonGraph);                
   
				HammerListeners[regulonGraph.DomElement.ID] = new Hammer.Hammer( regulonGraph.DomElement );
                
                // let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                HammerListeners[regulonGraph.DomElement.ID].Set(managerOptions);

                HammerListeners[regulonGraph.DomElement.ID].On( "panleft panright panup pandown", GraphOnPan );
                HammerListeners[regulonGraph.DomElement.ID].On( "doubletap", GraphOnTap );
                HammerListeners[regulonGraph.DomElement.ID].On( "panstart", GraphOnPanStart );
                HammerListeners[regulonGraph.DomElement.ID].On( "singletap", GraphOnPress );
                HammerListeners[regulonGraph.DomElement.ID].On( "panend", GraphOnPanEnd );
                HammerListeners[regulonGraph.DomElement.ID].On( "pinch", GraphOnPinch );

                JSObject pinchOptions = new JSObject();
                pinchOptions["enable"] = true;
                pinchOptions["threshold"] = 2;

                HammerListeners[regulonGraph.DomElement.ID].Get( "pinch" ).Set(pinchOptions);

                regulonGraph.Regulon.PropertyChanged += regulonGraph_PropertyChanged;
            }

            Window.AddEventListener("resize", OnResize, false);
        }

		/// <summary> Updates the visible regulons in the associated container,
		///		applying a filter and sorting function to 
		/// </summary>
		/// <param name="sort"></param>
		/// <param name="filter"></param>

		override public void UpdateVisibleRegulons(
			Func<RegulonInfo, RegulonInfo, int> sort,
			Func<RegulonInfo, bool> filter
		) {

            // Single group
            if (numberOfGroups < 2) {			    
                    PositionGraphs(sort, filter);
            } else {

            // Multiple groups
                for (int i = 0; i < numberOfGroups; i++) {
                    PositionGraphsInGroups(sort, filter, i);
                }
            }

            // We want to always sort after this
            newDisplay = false;
        }   

		/// <summary> Removes all regulon graphs from the display and disposes them, leaving the 
		///		display blank.
		/// </summary>

        public override void Clear()
        {			
            foreach ( RegulonGraph graph in regulonGraphs ) {
                HammerListeners[graph.DomElement.ID].Destroy();
                graph.RemoveElement();
				graph.Dispose();
			}
			regulonGraphs.Clear();
            foreach ( DivElement group in groups ) {
                group.ParentNode.RemoveChild( group );
            }
            groups.Clear();
		}
        
        override public void ChangeZoomLevel(ZoomLevels zoomLevel) {
            
            base.ChangeZoomLevel(zoomLevel);

            if (numberOfGroups > 1) {
                for (int i = 0; i < numberOfGroups; i++) {
                    SetGroupDivSize(regulonGraphs.Length, container, groups[i], i);
                }
            }
        }

        // At the end of a panning event, store the location of the graph. This
        // is used since the "isFinal" attribute of the pan event does not
        // always fire - it's mainly for internal HammerJS use
        protected override void GraphOnPanEnd(JSObject ev) {
            Script.Literal("ev.preventDefault()");
            //Script.Literal("ev.stopPropagation()");
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            Console.Log("PanEnd triggered on " + target.ID);
#endif
            
            // Reset the overflow to auto to allow scrolling again
            container.ParentNode.Style.Overflow = "auto";

            // Store the new location
            HammerPositions[target.ID][0] = target.OffsetLeft;
            HammerPositions[target.ID][1] = target.OffsetTop;

            foreach (RegulonGraph graph in regulonGraphs) {
                if (graph.DomElement.ID == target.ID) {
                    graph.Regulon.PositionLeft = HammerPositions[target.ID][0];
                    graph.Regulon.PositionTop = HammerPositions[target.ID][1];
                    SetGraphClass(graph, false, false);

                    if (numberOfGroups > 1) {
                        int newGroup = DetermineGraphGroup(graph);

                        if (graph.Regulon.GroupNumber != newGroup) {
    #if DEBUG
                            Console.Log(target.ID + " was moved to group " + newGroup);
    #endif
                            graph.Regulon.GroupNumber = newGroup;
                            if ((graph.GraphLabelText.IndexOf(" (centroid)") != -1 && Constants.TwoLineGraphLabels) || graph.GraphLabel.Text.IndexOf(" (centroid)") != -1) {
                                if (Constants.TwoLineGraphLabels) {
                                    graph.GraphLabelText = graph.GraphLabelText.Replace(" (centroid)", "");
                                } else {
                                    graph.GraphLabel.Text = graph.GraphLabel.Text.Replace(" (centroid)", "");
                                }
                                SetGraphClass(graph, false, false);
                            }
                        }
                    }

                    break;
                }
            }
/*#if DEBUG
            Console.Log("PanEnd - HammerPositions[" + target.ID + "] = {" + HammerPositions[target.ID][0] + ", " + HammerPositions[target.ID][1] + "}");
#endif*/
        }        

        // Determines which group a graph is in, based on testing its location
        // against the background group elements. This is done by checking the
        // centre position of the graph and whether that's within one of the
        // groups. If it's not within any, it will be assigned to the closest
        // group by pythagorian distance

        private int DetermineGraphGroup (RegulonGraph graph) {
            Element domElement = graph.DomElement;
            int currentGroup = -1;

            // Check against each of the groups
            for (int i = 0; i < numberOfGroups; i++) {

                // If the centre of this graph is inside the current group
                // rectangle, it's in this group
                if (groups[i].OffsetLeft < (domElement.OffsetLeft + domElement.OffsetWidth/2)
                    && (domElement.OffsetLeft + domElement.OffsetWidth/2) < (groups[i].OffsetLeft + groups[i].OffsetWidth)
                    && groups[i].OffsetTop < (domElement.OffsetTop + domElement.OffsetHeight/2)
                    && (domElement.OffsetTop + domElement.OffsetHeight/2) < (groups[i].OffsetTop + groups[i].OffsetHeight)) {
                    currentGroup = i + 1;
#if DEBUG
                    Console.Log(graph.DomElement.ID + " is inside group " + currentGroup);
#endif
                }
            }

            // If the centre is not inside any group, get the closest group
            // by checking the distance from the centre for all groups
            if (currentGroup == -1) {
#if DEBUG
                Console.Log(graph.DomElement.ID + " is not inside any group! Finding the group closest to it...");
#endif
                //To keep track of which is closest
                double closestDistance = double.MaxValue;

                for (int i = 0; i < numberOfGroups; i++) {
                    double currentDistance = Math.Sqrt(Math.Pow((domElement.OffsetLeft + domElement.OffsetWidth/2) - (groups[i].OffsetLeft + groups[i].OffsetWidth/2), 2)
                        + Math.Pow((domElement.OffsetTop + domElement.OffsetHeight/2) - (groups[i].OffsetTop + groups[i].OffsetHeight/2), 2));
                    
                    if (currentDistance < closestDistance) {
                        closestDistance = currentDistance;
                        currentGroup = i + 1;
                    }
                }

                Console.Log(graph.DomElement.ID + " is closest to the centre of group " + currentGroup + "; considering it as a member of that group.");
            }

            return currentGroup;
        }

        // Sets up the group rectangles that appear in the background
        private void SetUpGroupDivs(int numberOfRegulons, Element container)
        {
            if (groups.Length > 0) {     
                foreach ( DivElement group in groups ) {
                    group.ParentNode.RemoveChild( group );
                }
                groups.Clear();
            }

            for (int i = 0; i < numberOfGroups; i++)
            {
                DivElement newDiv = (DivElement)Document.CreateElement("group_" + (i + 1));
                newDiv.ID = "group_" + (i + 1);
                newDiv.Style.BackgroundColor = groupColours[i % groupColours.Length];
                newDiv.Style.Position = "absolute";
                newDiv.Style.Top = "0px";
                SetGroupDivSize(numberOfRegulons, container, newDiv, i);
#if DEBUG
                Console.Log(newDiv.ID + " left = " + newDiv.Style.Left);
#endif
                container.AppendChild(newDiv);
                groups.Add(newDiv);
#if DEBUG
                Console.Log(newDiv.ID + " width = " + newDiv.Style.Width + ", height = " + newDiv.Style.Height + ", backgroundColor = " + newDiv.Style.BackgroundColor);
#endif
            }
        }

        // Sets the size of the group rectangles, based on the current zoom
        // level
        private void SetGroupDivSize(int numberOfRegulons, Element container, DivElement newDiv, int i)
        {
            double widthFactor;
            double heightFactor;
            switch (currentZoomLevel) {                
                case ZoomLevels.IconList:
                    widthFactor = 1.5;
                    heightFactor = (1/3);
                    break;
                case ZoomLevels.SmallOverview:
                    widthFactor = 1;
                    heightFactor = 1;
                    break;
                case ZoomLevels.LargeOverview:
                    widthFactor = 1.5;
                    heightFactor = 1.5;
                    break;
                case ZoomLevels.DetailView:
                    widthFactor = 3.75;
                    heightFactor = 3.75;
                    break;
                default:
                    widthFactor = 1;
                    heightFactor = 1;
                    break;
            }
            double width = Math.Max(container.ClientWidth / numberOfGroups, (Constants.RegulonDisplayWidth * 92 + 30) * widthFactor);
            newDiv.Style.Width = width + "px";
            newDiv.Style.Height = (((Constants.RegulonDisplayHeight * 92 + 30) * heightFactor) * numberOfRegulons) + "px";
            newDiv.Style.Left = (width * i) + "px";
        }

        // Changes the number of groups in the display, and recalculates graph
        // position if automatic grouping is enabled
        public void SetNumGroups(int numberOfGroups, bool automatic, RegulogInfo selectedRegulog) {

            //Remove any centroid labels
            foreach (RegulonGraph graph in regulonGraphs) {
                if ((graph.GraphLabelText.IndexOf(" (centroid)") != -1 && Constants.TwoLineGraphLabels) || graph.GraphLabel.Text.IndexOf(" (centroid)") != -1) {
                    if (Constants.TwoLineGraphLabels) {
                        graph.GraphLabelText = graph.GraphLabelText.Replace(" (centroid)", "");
                    } else {
                        graph.GraphLabel.Text = graph.GraphLabel.Text.Replace(" (centroid)", "");
                    }
                    SetGraphClass(graph, false, false);
                }
            }

            this.numberOfGroups = numberOfGroups;
            if (numberOfGroups < 2) {
                if (groups.Length > 0) {     
                    foreach ( DivElement group in groups ) {
                        group.ParentNode.RemoveChild( group );
                    }
                    groups.Clear();
                }
                foreach (RegulonGraph graph in regulonGraphs) {
                    graph.Regulon.GroupNumber = 0;
                }
            } else {
                SetUpGroupDivs(selectedRegulog.Regulons.Length, container);
                foreach (RegulonGraph graph in regulonGraphs) {
                    graph.Regulon.GroupNumber = DetermineGraphGroup(graph);
                }

                // If automatic is true, get the hamming distances for all networks
                // against all other networks, and then position them in each
                // group rectangle
                if (automatic) {
                    AutomaticGroupingAndPositioning(selectedRegulog);
                }
            }
        }

        public void AutomaticGroupingAndPositioning(RegulogInfo selectedRegulog)
        {
            //Remove any centroid labels
            foreach (RegulonGraph graph in regulonGraphs) {
                if ((graph.GraphLabelText.IndexOf(" (centroid)") != -1 && Constants.TwoLineGraphLabels) || graph.GraphLabel.Text.IndexOf(" (centroid)") != -1) {
                    if (Constants.TwoLineGraphLabels) {
                        graph.GraphLabelText = graph.GraphLabelText.Replace(" (centroid)", "");
                    } else {
                        graph.GraphLabel.Text = graph.GraphLabel.Text.Replace(" (centroid)", "");
                    }
                    SetGraphClass(graph, false, false);
                }
            }

            Dictionary<int, int> assignments = new Dictionary<int, int>();
            currentCentroids = new List<int>();

            currentDistances = CalculateDistances((List<RegulonInfo>)selectedRegulog.Regulons, selectedRegulog, ratio);
            KMeansGroupAssignment((List<RegulonInfo>)selectedRegulog.Regulons, selectedRegulog, currentDistances, assignments, numberOfGroups, ratio);
            foreach (KeyValuePair<int, int> pair in assignments)
            {
                if (!currentCentroids.Contains(pair.Value))
                {
                    currentCentroids.Add(pair.Value);
                }
            }

/*#if DEBUG
            DaviesBoudinTesting(selectedRegulog, distances);
#endif*/

            // Position the graphs in the right group rectangle
            for (int i = 0; i < regulonGraphs.Length; i++)
            {
                int groupIndex = currentCentroids.IndexOf(assignments[i]);
#if DEBUG
                Console.Log(regulonGraphs[i].Regulon.GenomeName + " will be put in group " + (groupIndex + 1));
#endif
                regulonGraphs[i].Regulon.GroupNumber = groupIndex + 1;

                // Temporarily add it to the rectangle, so that it is
                // positioned correctly
                Element domElement = FindParentOfType(regulonGraphs[i].DomElement, "span");
                domElement.Style.Position = "initial";
                domElement.Style.Left = string.Empty;
                domElement.Style.Top = string.Empty;
                regulonGraphs[i].RemoveElement();
                if (currentCentroids.Contains(i)) {
                    if (Constants.TwoLineGraphLabels) {
                        regulonGraphs[i].GraphLabelText = regulonGraphs[i].GraphLabelText + " (centroid)";
                    } else {
                        regulonGraphs[i].GraphLabel.Text = regulonGraphs[i].GraphLabel.Text + " (centroid)";
                    }
                    SetGraphClass(regulonGraphs[i], false, true);

                    // Should position centroids first if there are already
                    // graphs in this group
                    if (groups[groupIndex].ChildNodes.Length > 0) {
                        groups[groupIndex].InsertBefore(regulonGraphs[i].DomElement, groups[groupIndex].FirstChild);
                    } else {
                        regulonGraphs[i].AddElementTo(groups[groupIndex]);
                    }
                } else {
                    regulonGraphs[i].AddElementTo(groups[groupIndex]);
                }
            }

            // Get the new positions of the graphs
            foreach (RegulonGraph graph in regulonGraphs)
            {
                // We need to add the offset of the rectangle, as well
                HammerPositions[graph.DomElement.ID] = new double[] { FindParentOfType(graph.DomElement, "span").OffsetLeft + groups[graph.Regulon.GroupNumber - 1].OffsetLeft, FindParentOfType(graph.DomElement, "span").OffsetTop + groups[graph.Regulon.GroupNumber - 1].OffsetTop};

                graph.Regulon.PositionLeft = HammerPositions[graph.DomElement.ID][0];
                graph.Regulon.PositionTop = HammerPositions[graph.DomElement.ID][1];
            }

            // Move the graphs back to being a child of the main container
            foreach (RegulonGraph graph in regulonGraphs)
            {
                graph.RemoveElement();
                graph.AddElementTo(container);
                FindParentOfType(graph.DomElement, "span").Style.Position = "absolute";
                FindParentOfType(graph.DomElement, "span").Style.Left = HammerPositions[graph.DomElement.ID][0] + "px";
                FindParentOfType(graph.DomElement, "span").Style.Top = HammerPositions[graph.DomElement.ID][1] + "px";
            }
        }

#if DEBUG
        private void DaviesBoudinTesting(RegulogInfo selectedRegulog, double[][] distances)
        {
            // Testing for the Davies-Boudin index calculations. This is based
            // on the definition as I understand it from here:
            // https://en.wikipedia.org/wiki/Davies%E2%80%93Bouldin_index#Definition
            Console.Log("Testing for Davies-Boudin index");

            // Contains all of the assignments for each network in each group
            // across all tests
            List<Dictionary<int, int>> testAssignments = new List<Dictionary<int, int>>();

            // Contains all of the centroids for each group across all tests
            List<List<int>> testCentroids = new List<List<int>>();

            // Contains the actual contents of each group across all tests
            List<List<List<int>>> testGroups = new List<List<List<int>>>();

            // Contains the average cluster scatter of each group across all
            // tests
            List<List<double>> testAvgGroupScatter = new List<List<double>>();

            // Contains all of the calculated Davies-Boudin indexes
            //Dictionary<int, double> testDBIndexes = new Dictionary<int,double>();

            // Since we want to find the "best" number of groups, we need to
            // create groups for all the possible amounts of groups (2 to 5).
            // The check for the amount of graphs is so we don't try to make
            // more groups then there are graphs
            for (int i = 2; i < 6 && i < regulonGraphs.Length; i++)
            {
                Console.Log("Number of groups: " + i);

                // Initialise all the lists for this number of groups
                testAssignments[i - 1] = new Dictionary<int, int>();
                testAssignments[i - 1] = KMeansGroupAssignment((List<RegulonInfo>)selectedRegulog.Regulons, selectedRegulog, distances, testAssignments[i - 1], i, ratio);
                testCentroids[i - 1] = new List<int>();
                testGroups[i - 1] = new List<List<int>>();
                testAvgGroupScatter[i - 1] = new List<double>();

                // Initialise the group contents list for each group
                for (int j = 0; j < i; j++)
                {
                    testGroups[i - 1][j] = new List<int>();
                }

                // Find the centroids for each group and put them in the list
                // of centroids, as well as assigning each network to the
                // correct group list
                foreach (KeyValuePair<int, int> pair in testAssignments[i - 1])
                {
                    if (!testCentroids[i - 1].Contains(pair.Value))
                    {
                        testCentroids[i - 1].Add(pair.Value);
                    }
                    Console.Log(regulonGraphs[pair.Key].Regulon.GenomeName + " is in group " + testCentroids[i - 1].IndexOf(pair.Value));

                    // For some reason it was choking on build so I had to use
                    // these temporary variables :S
                    List<List<int>> currentGroupSet = testGroups[i - 1];

                    //Because JavaScript treats keys in pairs as strings, we
                    //need to have this ugly conversion
                    currentGroupSet[testCentroids[i - 1].IndexOf(pair.Value)].Add(int.Parse(pair.Key.ToString()));
                    //testGroups[i-1][testCentroids[i-1].IndexOf(pair.Value)].Add(int.Parse(pair.Key.ToString()));
                }

                // Calculate the average scatter for all groups. This is used
                // as part of the calculation of Ri,j in the definition (Si and
                // Sj)
                // I'm not actually sure what the "within cluster scatter" is
                // that is mentioned in the definition for the DB index. So I
                // made an assumption that it's the average difference from the
                // centroid for all other elements in the cluster
                foreach (List<int> cluster in testGroups[i - 1])
                {
                    Console.Log("Group " + testGroups[i - 1].IndexOf(cluster) + " has " + cluster.Length + " networks.");
                    double totalScatter = 0;
                    int centroid = -1;

                    // Find the centroid for this group
                    foreach (int currentCentroid in testCentroids[i - 1])
                    {
                        if (cluster.Contains(currentCentroid))
                        {
                            centroid = currentCentroid;
                            break;
                        }
                    }

                    // Add up the distances between the centroid and all of the
                    // other elements in this group
                    for (int j = 0; j < cluster.Length; j++)
                    {
                        if (centroid != cluster[j])
                        {
                            totalScatter = totalScatter + distances[centroid][cluster[j]];
                        }
                    }

                    // Divide by the amount of elements to get the average
                    testAvgGroupScatter[i - 1][testGroups[i - 1].IndexOf(cluster)] = totalScatter / cluster.Length;
                }

                // Find the centroid distance between all groups and all other
                // groups, and calculate Ri,j, Di and DB    
                // We declare the variable for the final DB index here, so we
                // can add up the values of Mi,j and then divide them by the
                // total number of clusters
                double dBIndex = 0;

                // For every group in this set of groups
                foreach (List<int> ithCluster in testGroups[i - 1])
                {

                    // Store the maximum Ri,j calculation for this set of
                    // groups - (Si + Sj) / Mi,j
                    double maxRij = -1;

                    // Compare this group with every other group in the set of
                    // groups
                    foreach (List<int> jthCluster in testGroups[i - 1])
                    {
                        if (ithCluster != jthCluster)
                        {
                            Console.Log("Testing group " + testGroups[i-1].IndexOf(ithCluster) + " against group " + testGroups[i-1].IndexOf(jthCluster));

                            // These store the current smallest distance
                            double centroidDistance = distances[testCentroids[i-1][testGroups[i-1].IndexOf(ithCluster)]][testCentroids[i-1][testGroups[i-1].IndexOf(jthCluster)]];

                            Console.Log("Average cluster scatter for group i (" + testGroups[i-1].IndexOf(ithCluster) + ") is " + testAvgGroupScatter[i-1][testGroups[i-1].IndexOf(ithCluster)]);
                            Console.Log("Average cluster scatter for group j (" + testGroups[i-1].IndexOf(jthCluster) + ") is " + testAvgGroupScatter[i-1][testGroups[i-1].IndexOf(jthCluster)]);
                            Console.Log("Distance between the centroid of group i (" + testGroups[i-1].IndexOf(ithCluster) + ") and group j (" + testGroups[i-1].IndexOf(jthCluster) + ") is " + centroidDistance + " (between " + regulonGraphs[testCentroids[i-1][testGroups[i-1].IndexOf(ithCluster)]].Regulon.GenomeName + " and " + regulonGraphs[testCentroids[i-1][testGroups[i-1].IndexOf(jthCluster)]].Regulon.GenomeName + ")");

                            // Calculate Ri,j for these closest groups
                            double currentRij = (testAvgGroupScatter[i - 1][testGroups[i - 1].IndexOf(ithCluster)] + testAvgGroupScatter[i - 1][testGroups[i - 1].IndexOf(jthCluster)]) / centroidDistance;
                            Console.Log("Ri,j for this combination is " + currentRij);

                            // Di requires the maximum Ri,j, so check if our current one
                            // is it
                            if (currentRij > maxRij)
                            {
                                maxRij = currentRij;
                                Console.Log("This is the new maximum Ri,j for group i");
                            }
                        }
                    }
                    if (maxRij < 0)
                    {
                        Console.Log("Somehow, we did not get a max Ri,j for this group.");
                    }
                    else
                    {

                        // Add the max Ri,j (i.e. Di) to our DB value variable
                        // so we can use it later
                        dBIndex = dBIndex + maxRij;
                        Console.Log("Current sum of all Di is " + dBIndex);
                    }
                }
                // Divide the added up Di values to get the DB index
                dBIndex = 1 / dBIndex;
                Console.Log("Davies-Bouldin index for this set of groups (?) is " + dBIndex);
                //testDBIndexes[i] = dBIndex;
                Console.Log("Finished test with " + i + " groups.");
                Console.Log("------------");
            }
            Console.Log("Done.");
        }
#endif

        // Updates the size of groups when the window is resized
        private void OnResize(ElementEvent e)
        {
            if (numberOfGroups > 1) {
                SetUpGroupDivs(regulonGraphs.Length, container);
            }
        }
       
        private static double[][] CalculateDistances(List<RegulonInfo> regulons, RegulogInfo selectedRegulog, double alpha)
        {
#if DEBUG
            Console.Log("Alpha value is " + alpha + " (Hamming weight = " + alpha + ", Euclidean weight = " + (1 - alpha) + ")");
#endif
            //Again, I'm going to have to make the same, possibly invalid, assumption here.
            List<string> names = new List<string>();

            foreach (GeneInfo gene in selectedRegulog.TargetGenes)
            {
                // TODO: Allow alternate ortholog identification mechanism. One that actually works would be good.

                // We have to assume that orthologs have the same name. Otherwise, there 
                // appears to be no way to connect them in RegPrecise. However, this is
                // probably an invalid assumption. nonetheless...

                string name = gene.Name;

                if (!names.Contains(name))
                {
                    names.Add(name);
                }
            }

            Dictionary<string, int> geneList = new Dictionary<string, int>();

            foreach (string name in names) geneList[name] = geneList.Count;

            int n = geneList.Keys.Length;

            double[][] distances = new double[regulons.Length][];

            //Initialise the distances table
            for (int i = 0; i < regulons.Length; i++) {
                distances[i] = new double[regulons.Length];
                for (int j = 0; j < regulons.Length; j++) {
                    distances[i][j] = 0;
                }
            }

            //Calculate both the Hamming distance and Euclidean distance for
            //each
            for (int i = 0; i < regulons.Length; i++) {
/*#if DEBUG
                Console.Log("Creating Hamming and Euclidean distances for " + regulons[i].GenomeName);
#endif*/
                for (int j = 0; j < regulons.Length; j++) {
                    double iEuclidean = 0;

                    //If we're not comparing this network to itself...
                    if (i != j) {
/*#if DEBUG
                        Console.Log("Creating Hamming distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName);
#endif*/
                        //Create two vectors to store the presence of genes in 
                        //each network
                        double[] iVector = new double[names.Length];
                        double[] jVector = new double[names.Length];
                        int count = 0;

                        //For each gene in the regulog, check if it is present
                        //in both. For Hamming distance, regulation is not
                        //relevant.
                        foreach (KeyValuePair<string, int> geneName in geneList) {
                            iVector[count] = 0;
                            jVector[count] = 0;
                            double iValue = 0;
                            double jValue = 0;

                            //If the gene is in the network, it'll match one of
                            //the names in the list. If so, set the value to 1
                            //for Hamming distance, and for Eucildean distance,
                            //set the value to 1 if there's regulation, or 0.75
                            //if there's not
                            foreach (GeneInfo regulonGene in regulons[i].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (alpha > 0) {
/*#if DEBUG
                                        Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " - setting the entry in iVector to '1'");
#endif*/
                                        iVector[count] = 1;
                                    }
                                    if (alpha < 1) {
                                        if (regulonGene.Site != null) {
/*#if DEBUG
                                            Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " and is regulated - setting iValue to '1'");
#endif*/
                                            iValue = 1;
                                        } else {
/*#if DEBUG
                                            Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " and is not regulated - setting iValue to '0.75'");
#endif*/
                                            iValue = 0.75;
                                        }
                                    }
                                        break;
                                }
                            }

                            //Do the same with the network that will be
                            //compared with the first
                            foreach (GeneInfo regulonGene in regulons[j].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (alpha > 0) {
/*#if DEBUG
                                        Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " - setting the entry in jVector to '1'");
#endif*/
                                        jVector[count] = 1;
                                    }
                                    if (alpha < 1) {
                                        if (regulonGene.Site != null) {
/*#if DEBUG
                                            Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " and is regulated - setting jValue to '1'");
#endif*/
                                            jValue = 1;
                                        } else {
/*#if DEBUG
                                            Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " and is not regulated - setting jValue to '0.75'");
#endif*/
                                            jValue = 0.75;
                                        }
                                    }
                                    break;
                                }
                            }

                            //add (xn - yn)^2
                            iEuclidean = iEuclidean + Math.Pow((iValue - jValue), 2);
/*#if DEBUG
                            Console.Log("iVector[count] = " + iVector[count] + ", jVector[count] = " + jVector[count] + ", iValue = " + iValue + ", jValue = " + jValue + ", iEuclidean = " + iEuclidean);
#endif*/
                            count = count + 1;
                        }
                        double hammingDistance = 0;
                        for (int k = 0; k < names.Length; k++) {
/*#if DEBUG
                            Console.Log("Gene " + k + " value for " + regulons[i].GenomeName + " is " + iVector[k] + " and value for " + regulons[j].GenomeName + " is " + jVector[k]);
#endif*/
                            if (iVector[k] != jVector[k]) {
                                hammingDistance = hammingDistance + Math.Abs(iVector[k] - jVector[k]);
                            }
                        }
/*#if DEBUG
                        Console.Log("Hamming distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName + " is " + hammingDistance);
#endif*/
                        distances[i][j] = distances[i][j] + hammingDistance * alpha;

                        //Raise all the (xn - yn)^2 terms to the power of 1/2 and
                        //add it to the distance
/*#if DEBUG
                        Console.Log("Euclidean distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName + " is " + Math.Pow(iEuclidean, 0.5));
#endif*/
                        distances[i][j] = distances[i][j] + (1 - alpha) * (Math.Pow(iEuclidean, 0.5));
                    } else {
                        distances[i][j] = 0;
                    }
                }
            }

            return distances;
        }

        private static Dictionary<int, int> KMeansGroupAssignment(List<RegulonInfo> regulons, RegulogInfo selectedRegulog, double[][] distances, Dictionary<int, int> assignments, int numberOfGroups, double alpha)
        {
            //Initialise the list of centroids, and the list of the networks
            //and which groups they were in (including the centroids, they go
            //in their own groups)
            List<int> centroids = new List<int>();

            //Pick an amount of centroids based upon the number of groups
            //selected (or all of the networks if there's less networks than
            //groups). Centroids are picked randomly
            int counter = 0;
            while (counter < Math.Min(numberOfGroups, regulons.Length))
            {
                //Pick a random network to act as a centroid
                int centroid = Math.Floor(Math.Random() * regulons.Length);
#if DEBUG
                Console.Log("Attempting to add network " + centroid + " as a centroid");
#endif

                //Check if the same centroid wasn't chosen twice
                if (!centroids.Contains(centroid))
                {
                    centroids.Add(centroid);
#if DEBUG
                    Console.Log("Regulon " + centroid + " (" + regulons[centroid].GenomeName + ") added as a centroid");
#endif

                    //Only advance if we picked a centroid that wasn't already
                    //picked
                    counter++;
                }
            }

            //List<int> previousCentroids = new List<int>();
            Dictionary<int, int> previousAssignments = new Dictionary<int, int>();
            int numberOfTimes = 1;

            //We will keep going until the list of centroids we generate is the
            //same as before
            bool assignmentEquilibrium = false;
            while (!assignmentEquilibrium) {
#if DEBUG
                Console.Log("Number of times processing centroids = " + numberOfTimes);
#endif
                numberOfTimes = numberOfTimes + 1;
#if DEBUG
                //Console.Log("previousCentroids == " + previousCentroids);
                Console.Log("centroids == " + centroids);
#endif

                if (numberOfTimes > 1) {
                    foreach (KeyValuePair<int, int> pair in assignments) {
                        previousAssignments[pair.Key] = pair.Value;
                    }
                }

                //Go through all the networks and determine which group they should
                //belong to
                for (int i = 0; i < regulons.Length; i++)
                {
                    //Centroids go in their own group, obviously
                    if (centroids.Contains(i))
                    {
                        assignments[i] = i;
                    }
                    else
                    {
                        //Initally, put the network in the same group as the first
                        //centroid
                        assignments[i] = centroids[0];

                        //Check the current network to see which centroid it is
                        //closest to
                        foreach (int centroid in centroids)
                        {
                            //For each centroid, see if the current network is
                            //closer to it than to the centroid it is currently
                            //assigned to, and then switch its assignment if it is.
                            //Because of the way this is implemented, a network
                            //will go to the lower number centroid if it is the
                            //same distance from more than one
#if DEBUG
                            //Console.Log("Regulon " + i + " (" + regulons[i].GenomeName + ") is " + distances[i][centroid] + " away from centroid " + centroids.IndexOf(centroid));
#endif
                            if (distances[i][centroid] < distances[i][assignments[i]])
                            {
                                assignments[i] = centroid;
                            }
                        }
#if DEBUG
                        //Console.Log("Regulon " + i + " (" + regulons[i].GenomeName + ") will be in the same group as centroid " + centroids.IndexOf(assignments[i]));
#endif
                    }
                }
                //For each group, recalculate the centroids by calculating the
                //one in the group that has the smallest total distance

                //First create a list of lists of the networks in each centroid
                //group
                List<List<int>> groups = new List<List<int>>();
                for (int i = 0; i < centroids.Length; i++)
                {
                    groups[i] = new List<int>();
                }
                foreach (KeyValuePair<int, int> pair in assignments) {

                    //Because JavaScript treats keys in pairs as strings, we
                    //need to have this ugly conversion
                    groups[centroids.IndexOf(pair.Value)].Add(int.Parse(pair.Key.ToString()));
                }

                // Stores the new centroids
                centroids = new List<int>();

                //For each group...
                foreach (List<int> group in groups)
                {
                    centroids[groups.IndexOf(group)] = CalculateCentroid(regulons, selectedRegulog, distances, alpha, group);
                }

                //If the centroids we just found are not the same as the ones
                //at the start of this loop, set centroidEquilibrium to false
                //which will mean the loop will be processed another time
#if DEBUG
                Console.Log("new centroids == " + centroids);
#endif
                if (numberOfTimes > 1) {
                    assignmentEquilibrium = true;
                    foreach (KeyValuePair<int, int> pair in assignments) {
                        if (previousAssignments[pair.Key] != pair.Value) {
                            assignmentEquilibrium = false;
                            break;
                        }
                    }
                }
            }

            //Return the completed list of group assignments
#if DEBUG
            //Console.Log("Centroid equilibrium has been found!");
            Console.Log("Assignment equilibrium has been found!");
#endif
            return assignments;
        }

        private void ContainerOnPress(JSObject ev)
        {
            Script.Literal("ev.preventDefault()");
            Element target = (Element)ev["target"];
#if DEBUG
            Console.Log("ContainerOnPress triggered on " + target.ID);
#endif
            if (groups.Contains(target)) {
#if DEBUG
                Console.Log("A press occured in the container for group " + (int)(groups.IndexOf((DivElement)target) + 1));
#endif
                if (selectedRegulons.Length < 1) {
                    selectedRegulog.Regulons[currentCentroids[groups.IndexOf((DivElement)target)]].IsSelected = true;
                    selectedRegulons.Add(selectedRegulog.Regulons[currentCentroids[groups.IndexOf((DivElement)target)]]);
                }
                foreach (RegulonGraph graph in regulonGraphs) {
                    if ((graph.Regulon.GroupNumber == groups.IndexOf((DivElement)target) + 1) && (!selectedRegulons.Contains(graph.Regulon))) {
                        graph.Regulon.IsSelected = true;
                        selectedRegulons.Add(graph.Regulon);
                    }
                }
            }
        }

        // If there are multiple groups and centroids present, sort by distance
        // Should only be called if there is more than one group. Assumes one
        // centroid and will not do anything if there are no distances
        // calculated, or there is no centroid
        public void UpdateVisibleRegulonsSortByCentroidDistance(Func<RegulonInfo, bool> filter)
        {

            if (currentDistances != null) {
            
                for (int i = 0; i < numberOfGroups; i++) {
                    List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

                    RegulonGraph centroid = null;

                    // Get all the graphs in this group. If a graph does not
                    // have any group at present, put it in the current one
                    // (in effect it will always go in the first)
                    foreach (RegulonGraph graph in regulonGraphs) {
                        if (graph.Regulon.GroupNumber == i + 1 || graph.Regulon.GroupNumber == 0) {

                            // At present, we identify centroids outside the
                            // grouping function by their label
                            if ((graph.GraphLabelText.IndexOf(" (centroid)") != -1 && Constants.TwoLineGraphLabels) || graph.GraphLabel.Text.IndexOf(" (centroid)") != -1) {
                                centroid = graph;
                            } else {
                                if (filter == null || filter(graph.Regulon)) {
                                    visibleGraphs.Add(graph);
                                }
                            }
                            graph.Regulon.GroupNumber = i + 1;
                        }
                    }

                    if (centroid != null) {
                    
                        visibleGraphs.Sort(delegate(RegulonGraph x, RegulonGraph y) {
                            return (int)((currentDistances[regulonGraphs.IndexOf(x)][regulonGraphs.IndexOf(centroid)] -
                                currentDistances[regulonGraphs.IndexOf(y)][regulonGraphs.IndexOf(centroid)]) * 100);
                        });
                        
                        if (filter == null || filter(centroid.Regulon)) {
                            visibleGraphs.Insert(0, centroid);
                        }

			            foreach ( RegulonGraph graph in visibleGraphs ) {
				            graph.RemoveElement();
                            FindParentOfType( graph.DomElement, "span" ).Style.Position = String.Empty;
                            FindParentOfType( graph.DomElement, "span" ).Style.Left = String.Empty;
                            FindParentOfType( graph.DomElement, "span" ).Style.Top = String.Empty;
                        }

			            foreach ( RegulonGraph graph in visibleGraphs ) {

                            // Temporarily add it to the group rectangle to
                            // position it correctly
				            graph.AddElementTo( groups[i] );
			            }

                        foreach ( RegulonGraph graph in visibleGraphs ) {

                            if (newDisplay && graph.Regulon.PositionLeft != -1 && graph.Regulon.PositionTop != -1) {
                                HammerPositions[graph.DomElement.ID] = new double[] { graph.Regulon.PositionLeft, graph.Regulon.PositionTop };
        #if DEBUG
                            Console.Log(graph.DomElement.ID + " origin loaded from the saved file as " + HammerPositions[graph.DomElement.ID][0] + ", " + HammerPositions[graph.DomElement.ID][1]);
        #endif
                            } else {
                            
                                // We need to add the offset of the rectangle, as
                                // well
                                HammerPositions[graph.DomElement.ID] = new double[] { FindParentOfType( graph.DomElement, "span" ).OffsetLeft + groups[i].OffsetLeft, FindParentOfType( graph.DomElement, "span" ).OffsetTop + groups[i].OffsetTop };
                                graph.Regulon.PositionLeft = HammerPositions[graph.DomElement.ID][0];
                                graph.Regulon.PositionTop = HammerPositions[graph.DomElement.ID][1];
        #if DEBUG
                            Console.Log(graph.DomElement.ID + " origin set as " + HammerPositions[graph.DomElement.ID][0] + ", " + HammerPositions[graph.DomElement.ID][1]);
        #endif
                            }
                        }

                        maxZLevel = 1;
                        foreach ( RegulonGraph graph in visibleGraphs ) {
                            graph.RemoveElement();
                            graph.AddElementTo(container);
                            FindParentOfType( graph.DomElement, "span" ).Style.Position = "absolute";
                            FindParentOfType( graph.DomElement, "span" ).Style.Left = HammerPositions[graph.DomElement.ID][0] + "px";
                            FindParentOfType( graph.DomElement, "span" ).Style.Top = HammerPositions[graph.DomElement.ID][1] + "px";
                            FindParentOfType( graph.DomElement, "span" ).Style.ZIndex = maxZLevel;
                            maxZLevel = (short)(maxZLevel + 1);
                        }
                    }
                    // We want to always sort after this
                    newDisplay = false;
                }

            }
        }        

        private void PositionGraphsInGroups(Func<RegulonInfo, RegulonInfo, int> sort, Func<RegulonInfo, bool> filter, int groupNumber)
        {
            List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

            // Allows us to give special position to centroids
            List<RegulonGraph> centroids = new List<RegulonGraph>();

            // Get all the graphs in this group. If a graph does not
            // have any group at present, put it in the current one
            // (in effect it will always go in the first)
            foreach (RegulonGraph graph in regulonGraphs)
            {
                if (groupNumber > -1) {
                    if (graph.Regulon.GroupNumber == groupNumber + 1 || graph.Regulon.GroupNumber == 0)
                    {

                        // At present, we identify centroids outside the
                        // grouping function by their label
                        if ((graph.GraphLabelText.IndexOf(" (centroid)") != -1 && Constants.TwoLineGraphLabels) || graph.GraphLabel.Text.IndexOf(" (centroid)") != -1) {
                            centroids.Add(graph);
                        }
                        else
                        {
				            if ( filter == null || filter( graph.Regulon ) ) {
                                visibleGraphs.Add(graph);
                            }
                        }
                        graph.Regulon.GroupNumber = groupNumber + 1;
                    }
                } else {
				    if ( filter == null || filter( graph.Regulon ) ) {
                        visibleGraphs.Add(graph);
                    }
                }
            }

            visibleGraphs.Sort(delegate(RegulonGraph x, RegulonGraph y) { return sort(x.Regulon, y.Regulon); });
            if (groupNumber > -1) {
                centroids.Sort(delegate(RegulonGraph x, RegulonGraph y) { return sort(x.Regulon, y.Regulon); });

                // Ensures that any "centroids" are added first
                for (int j = 0; j < centroids.Length; j++)
                {
                    visibleGraphs.Insert(j, centroids[j]);
                }
            }

            foreach (RegulonGraph graph in visibleGraphs)
            {
                graph.RemoveElement();
                FindParentOfType(graph.DomElement, "span").Style.Position = String.Empty;
                FindParentOfType(graph.DomElement, "span").Style.Left = String.Empty;
                FindParentOfType(graph.DomElement, "span").Style.Top = String.Empty;
            }

            foreach (RegulonGraph graph in visibleGraphs)
            {

                // Temporarily add it to the group rectangle to
                // position it correctly
                if (groupNumber > -1) {
                    graph.AddElementTo(groups[groupNumber]);
                } else {
                    graph.AddElementTo(container);
                }
            }

            foreach (RegulonGraph graph in visibleGraphs)
            {

                if (newDisplay && graph.Regulon.PositionLeft != -1 && graph.Regulon.PositionTop != -1)
                {
                    HammerPositions[graph.DomElement.ID] = new double[] { graph.Regulon.PositionLeft, graph.Regulon.PositionTop };
#if DEBUG
                    Console.Log(graph.DomElement.ID + " origin loaded from the saved file as " + HammerPositions[graph.DomElement.ID][0] + ", " + HammerPositions[graph.DomElement.ID][1]);
#endif
                }
                else
                {

                    // We need to add the offset of the rectangle, as
                    // well
                    if (groupNumber > -1) {
                        HammerPositions[graph.DomElement.ID] = new double[] { FindParentOfType(graph.DomElement, "span").OffsetLeft + groups[groupNumber].OffsetLeft, FindParentOfType(graph.DomElement, "span").OffsetTop + groups[groupNumber].OffsetTop };
                    } else {
                        HammerPositions[graph.DomElement.ID] = new double[] { FindParentOfType(graph.DomElement, "span").OffsetLeft, FindParentOfType(graph.DomElement, "span").OffsetTop };
                    }
                    graph.Regulon.PositionLeft = HammerPositions[graph.DomElement.ID][0];
                    graph.Regulon.PositionTop = HammerPositions[graph.DomElement.ID][1];
#if DEBUG
                    Console.Log(graph.DomElement.ID + " origin set as " + HammerPositions[graph.DomElement.ID][0] + ", " + HammerPositions[graph.DomElement.ID][1]);
#endif
                }
            }

            maxZLevel = 1;
            foreach (RegulonGraph graph in visibleGraphs)
            {
                graph.RemoveElement();
                graph.AddElementTo(container);
                FindParentOfType(graph.DomElement, "span").Style.Position = "absolute";
                FindParentOfType(graph.DomElement, "span").Style.Left = HammerPositions[graph.DomElement.ID][0] + "px";
                FindParentOfType(graph.DomElement, "span").Style.Top = HammerPositions[graph.DomElement.ID][1] + "px";
                FindParentOfType(graph.DomElement, "span").Style.ZIndex = maxZLevel;
                maxZLevel = (short)(maxZLevel + 1);
            }
        }

        /// <summary>
        /// Extracts the distances for only the current group from the matrix
        /// of distances between all networks
        /// </summary>
        /// <param name="groupRegulons"></param>
        /// <param name="selectedRegulog"></param>
        /// <param name="alpha"></param>
        /// <param name="distances"></param>
        /// <returns>The matrix of distances for this specific group</returns>
        private static double[][] GetGroupDistances(List<RegulonInfo> groupRegulons, RegulogInfo selectedRegulog, double alpha, double[][] distances)
        {
            double[][] groupDistances = new double[groupRegulons.Length][];
            for (int i = 0; i < groupRegulons.Length; i++) {
                groupDistances[i] = new double[groupRegulons.Length];
                int iIndex = selectedRegulog.Regulons.IndexOf(groupRegulons[i]);
                for (int j = 0; j < groupRegulons.Length; j++) {
                    int jIndex = selectedRegulog.Regulons.IndexOf(groupRegulons[j]);
                    groupDistances[i][j] = distances[iIndex][jIndex];
                }
            }
            return groupDistances;
        }  

        /// <summary>
        /// Calculates the centroid of a group
        /// </summary>
        /// <param name="regulons"></param>
        /// <param name="selectedRegulog"></param>
        /// <param name="distances"></param>
        /// <param name="alpha"></param>
        /// <param name="group"></param>
        /// <returns>The id of the network that is the centroid</returns>
        private static int CalculateCentroid(List<RegulonInfo> regulons, RegulogInfo selectedRegulog, double[][] distances, double alpha, List<int> group)
        {            
            //Get all the reguloninfos for each network in that group
            //and store it in a temporary list
            List<RegulonInfo> groupRegulons = new List<RegulonInfo>();
            for (int i = 0; i < regulons.Length; i++) {
                if (group.Contains(i)) {
                    groupRegulons.Add(regulons[i]);
                }
            }

            //Calculate the distances for this list of networks
            double[][] groupDistances = //CalculateDistances(groupRegulons, selectedRegulog, alpha);
                GetGroupDistances(groupRegulons, selectedRegulog, alpha, distances);

            //Find out which network out of this group has the smallest
            //total of its distances. I am assuming this is the
            //"average" centroid
            double minDistanceTotal = double.MaxValue;
            int minDistanceTotalIndex = 0;
            for (int i = 0; i < groupDistances.Length; i++)
            {
                double total = 0;
                for (int j = 0; j < groupDistances[i].Length; j++)
                {
                    total = total + groupDistances[i][j];
                }
/*#if DEBUG
                Console.Log("Total of all distances for " + groupRegulons[k].GenomeName + " is " + total);
#endif*/
                if (total < minDistanceTotal)
                {
                    minDistanceTotal = total;
                    minDistanceTotalIndex = i;
/*#if DEBUG
                    Console.Log("This is the new minimum for this group");
#endif*/
                }
            }

            //Set the new centroid of this group to be the one found
            //above
/*#if DEBUG
            Console.Log("New centroid for group " + groups.IndexOf(group) + " will be " + groupRegulons[minDistanceTotalIndex].GenomeName);
#endif*/
            return regulons.IndexOf(groupRegulons[minDistanceTotalIndex]);
        }

        protected override void SetGraphClass (RegulonGraph graph, bool dragged, bool flash) {
            string classText = String.Empty;
            if (currentCentroids.Contains(selectedRegulog.Regulons.IndexOf(graph.Regulon))) {
                if (flash) {
                    classText = "RegulonGraph_CentroidFlash";
                } else {
                    classText = "RegulonGraph_Centroid";
                }
            } else {
                classText = "RegulonGraph";
            }

            if (graph.IsSelected) {
                classText = classText + " RegulonGraph_Selected";
            }

            if (dragged) {
                classText = classText + " RegulonGraph_Dragged";
            }
            
            graph.GraphSvg.SetAttribute("class", classText);
        }
    }
}
