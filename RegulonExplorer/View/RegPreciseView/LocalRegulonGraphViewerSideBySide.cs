﻿using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using SystemQut.ComponentModel;
using SystemQut.Controls;
using SystemQut.Linq;
using System.Collections;
using ElementEventHandler = SystemQut.Controls.ElementEventHandler;
using SystemQut;
using System.Html.Data.Files;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.RegPreciseView {

	/// <summary> Host control for the famous TrnDiff "wagon-wheel" display :^)
	/// </summary>

	public class LocalRegulonGraphViewerSideBySide : RegPreciseRegulonGraphViewerBase {

        List<RegulonInfo> currentRegulons;

		/// <summary> Create a new regulon graph viewer bound to a provided DOM element which
		///		should already exist in the application html document.
		///	<para>
		///		The element should contain a nested block element of some sort to hold the view
		///		(data-name="RegulonDisplay", data-codebehind="CodeBehind") and a record navigator
		///		(data-name="Pager", data-codebehind="RecordNavigator").
		/// </para>
		/// </summary>
		/// <param name="element"></param>

		public LocalRegulonGraphViewerSideBySide( Element element )
			: base( element ) {
			Instance = this;

			Connect( LocalRegulonGraphViewer.Instance );

            CreateComparisonLegend();
		}

		private int selectedIndex;
		private RegulogInfo[] regulogs = new RegulogInfo[1];

		//private Select compareChooser;
		private Select select_CompareChooserLeftNetwork;
		private Select select_CompareChooserRightNetwork;
        private Select select_CompareChooserReference;
        private TableRowElement tableRow_CompareChooserReference;
        private TableRowElement tableRow_CompareChooserLeftNetwork;
        private TableRowElement tableRow_CompareChooserRightNetwork;
        private Button button_ResetToTwo;

#if DEBUG
        private Button touchButton_ColourBlind;
#endif

        private Element div_PairComparisonExplanation;

		/// <summary> Currently available items for compare chooser.
		/// </summary>
		//private string[] compareItems = { "Off", "AND", "OR", "XOR" };
		public static LocalRegulonGraphViewerSideBySide Instance;

        /// <summary> Locates the required nested controls, matching by name and type.
		/// <para>
		///		Overriden version to find controls specific to this display
		/// </para>
		/// </summary>
		protected override void FindChildControls() {
			base.FindChildControls();
			select_CompareChooserLeftNetwork = (Select) FindControlByNameAndType( "CompareChooserLeftNetwork", typeof( Select ) );
			select_CompareChooserRightNetwork = (Select) FindControlByNameAndType( "CompareChooserRightNetwork", typeof( Select ) );
            div_SidePanel = (DivElement)Document.GetElementById("SidePanel_Local_SideBySide");
            table_Legend = (TableElement)Document.GetElementById("Legend_Local_SideBySide");
            div_RegulonDisplayHolder = (DivElement)Document.GetElementById("RegulonDisplayHolder_Local_SideBySide");
            touchButton_HighlightFunctionPopup = (DivElement)Document.GetElementById("HighlightFunctionPopup_Local_SideBySide");
            touchButton_HighlightTermPopup = (DivElement)Document.GetElementById("HighlightTermPopup_Local_SideBySide");
            touchButton_SearchTermPopup = (DivElement)Document.GetElementById("SearchTermPopup_Local_SideBySide");

            touchButton_LockComparison = (Button)FindControlByNameAndType("TouchButton_LockComparison", typeof(Button));
            label_LockComparison = (Element)Document.GetElementById("LockComparison_Local");

            select_CompareChooserReference = (Select) FindControlByNameAndType( "CompareChooserReference", typeof( Select ) );
            tableRow_CompareChooserReference = (TableRowElement)Document.GetElementById("CompareChooserReferenceRow_Local");
            tableRow_CompareChooserLeftNetwork = (TableRowElement)Document.GetElementById("CompareChooserLeftNetworkRow_Local");
            tableRow_CompareChooserRightNetwork = (TableRowElement)Document.GetElementById("CompareChooserRightNetworkRow_Local");
            tableRow_CompareChooserReference = (TableRowElement)Document.GetElementById("CompareChooserReferenceRow_Local");
            button_ResetToTwo = (Button)FindControlByNameAndType("CompareChooser_ResetToTwo", typeof(Button));

#if DEBUG
            touchButton_ColourBlind = (Button)FindControlByNameAndType("TouchButton_ColourBlind", typeof(Button));
#endif

            label_SelectedGenes = (Element)Document.GetElementById("Label_SelectedGenes_Local_SideBySide");
            div_PairComparisonExplanation = (Element)Document.GetElementById("Div_PairComparisonExplanation_Local_SideBySide");
		}

        /// <summary>
        /// Binds functions to relevant controls in the display
		/// <para>
		///		Overriden version to bind controls specific to this display
		/// </para>
		/// </summary>
		protected override void BindChildControls() {
			base.BindChildControls();

			select_CompareChooserLeftNetwork.SelectionChanged += new ElementEventHandler( Select_CompareChooserNetwork_SelectionChanged );
			select_CompareChooserRightNetwork.SelectionChanged += new ElementEventHandler( Select_CompareChooserNetwork_SelectionChanged );

            if ( touchButton_LockComparison != null ) {
				touchButton_LockComparison.Clicked += new ElementEventHandler( touchButton_LockComparison_Clicked );
			}

            if ( select_CompareChooserReference != null ) {
				select_CompareChooserReference.SelectionChanged += new ElementEventHandler( Select_CompareChooserReference_SelectionChanged );
			}

            if ( button_ResetToTwo != null ) {
				button_ResetToTwo.Clicked += new ElementEventHandler( button_ResetToTwo_Clicked );
			}

            /* Colour blind toggle testing (colour blind friendly colours not actually implemented yet)
             Button should always be removed if colour blind constant is not enabled */
#if DEBUG
            if (Constants.UseColourBlindToggle) {
                if ( touchButton_ColourBlind != null) {
				    touchButton_ColourBlind.Clicked += new ElementEventHandler( touchButton_ColourBlind_Clicked );
                }
            } else {
#endif
                Element temp = (TableCellElement)Document.GetElementById("ColourBlind_LocalSideBySide");
                temp.ParentNode.RemoveChild(temp);
                temp = (TableCellElement)Document.GetElementById("ColourBlindLabel_LocalSideBySide");
                temp.ParentNode.RemoveChild(temp);
#if DEBUG
            }
#endif
		}

        /// <summary>
        /// Checks for when the activation status of this display is changed
        /// <para>Used to automatically display a selected set of networks in a comparison</para>
        /// </summary>
        /// <param name="activated">The details of the changed activation</param>
		protected override void this_ActivationChanged( IActivated activated ) {
/*#if DEBUG
			if (Constants.ShowDebugMessages) Console.Log( "this_ActivationChanged triggered for LocalRegulonGraphViewerSideBySide" );
#endif*/

            // If this display is visible, and the user did not press on a
            // group in the regular display and the comparison is not locked,
            // display the selected networks in the regular display as a
            // comparison
            if (activated.IsActivated && !groupPressed && !comparisonLocked) {

                // If the regular display is currently in the detail view, is
                // showing a filtered set of networks, but none of them are
                // currently selected, use this as the "selected" networks and
                // compare them
                if (Constants.DetailedViewSelected &&
                    normalViewer.DisplayManager.CurrentZoomLevel == ZoomLevels.DetailView &&
                    normalViewer.DisplayManager.SelectedRegulons.Length == 0) {

                    // Not all display manager types have the functions so
                    // the type it is needs to be confirmed first
                    if (normalViewer.DisplayManager is DefaultRegulonDisplay) {

                        // Go through the list of networks in the detail view
                        // and add them to the list of current regulons to
                        // compare
                        currentRegulons = new List<RegulonInfo>();
                        foreach (RegulonGraph graph in (normalViewer.DisplayManager as DefaultRegulonDisplay).DetailedViewGraphs) {
                            currentRegulons.Add(graph.Regulon);
                        }
                    }
                    if (normalViewer.DisplayManager is GroupRegulonDisplay) {

                        // Go through the list of networks in the detail view
                        // and add them to the list of current regulons to
                        // compare
                        currentRegulons = new List<RegulonInfo>();
                        foreach (List<RegulonGraph> graphList in (normalViewer.DisplayManager as GroupRegulonDisplay).DetailedViewGraphs) {
                            foreach (RegulonGraph graph in graphList) {
                                currentRegulons.Add(graph.Regulon);
                            }
                        }
                    }
                    if (normalViewer.DisplayManager is HammerRegulonDisplay) {

                        // Go through the list of networks in the detail view
                        // and add them to the list of current regulons to
                        // compare
                        currentRegulons = new List<RegulonInfo>();
                        foreach (RegulonGraph graph in (normalViewer.DisplayManager as HammerRegulonDisplay).DetailedViewGraphs) {
                            currentRegulons.Add(graph.Regulon);
                        }
                    }
                    if (normalViewer.DisplayManager is HammerGroupRegulonDisplay) {

                        // Go through the list of networks in the detail view
                        // and add them to the list of current regulons to
                        // compare
                        currentRegulons = new List<RegulonInfo>();
                        foreach (List<RegulonGraph> graphList in (normalViewer.DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs) {
                            foreach (RegulonGraph graph in graphList) {
                                currentRegulons.Add(graph.Regulon);
                            }
                        }
                    }

                // Otherwise, take the list of selected networks from the
                // display manager
                } else {
                    currentRegulons = normalViewer.DisplayManager.SelectedRegulons;
                }

                // If there are more than two selected networks, perform a
                // multiple comparison
                if (currentRegulons.Length > 2) {

                    // Use the first network in the list as the reference
                    DisplayGroupComparison(currentRegulons, 0);

                    // Set the selected items in the drop downs to the first
                    // two networks in the selection
                    // (These should no longer be visible during a multiple
                    // comparison)
                    select_CompareChooserLeftNetwork.SelectedIndex = selectedRegulog.Regulons.IndexOf(currentRegulons[0]);
                    select_CompareChooserRightNetwork.SelectedIndex = selectedRegulog.Regulons.IndexOf(currentRegulons[1]);
                }

                // Otherwise, perform a pair comparison
                if (currentRegulons.Length == 2) {

                    // Set the selected items in the drop downs to the two
                    // networks in the selection
                    select_CompareChooserLeftNetwork.SelectedIndex = selectedRegulog.Regulons.IndexOf(currentRegulons[0]);
                    select_CompareChooserRightNetwork.SelectedIndex = selectedRegulog.Regulons.IndexOf(currentRegulons[1]);

                    // Display the pair comparison
                    DisplayRegulonsInComparisonFromSelectors();

                    // Disable the reference chooser
                    DisableReferenceChooser();
                }
            }

            // Set whether a group was pressed to false
            this.groupPressed = false;

            // Hide the current tooltip if the display manager is using Hammer
            if (DisplayManager is HammerRegulonDisplay) {
                (DisplayManager as HammerRegulonDisplay).HideCurrentToolTip();
            }
		}

		/// <summary> Overrides GetSelectedRegulog to return the current regulog in the RegulogChooser.
		/// <para>
		///		This method invokes the action callback immediately.
		/// </para>
		/// </summary>
		/// <param name="action"></param>

		protected override void GetSelectedRegulog( Action<RegulogInfo> action ) {
			action( selectedRegulog );
		}

		/// <summary> Text formatter for the pager.
		/// </summary>
		/// <param name="format">
		///		The current format string in use by the pager. The argument is ignored
		///		by the current instance.
		/// </param>
		/// <param name="current">
		///		The current index within the associated collection. This may be undefined
		///		if the collection has no "current" element, such as when the no record is
		///		selected in a data grid.
		/// </param>
		/// <param name="min">
		///		The (possibly undefined) minimum value in the range covered by the pager.
		///		See note for <c>current</c>.
		/// </param>
		/// <param name="max">
		///		The (possibly undefined) maximum value in the range covered by the pager.
		///		See note for <c>current</c>.
		/// </param>
		/// <returns>
		///		A string that represents the current state. Returns a NO_SELECTION literal
		///		if any of the arguments are undefined or no regulog is selected. Returns
		///		a formatted string in the form "Regulog: {0} {1} (group {2} of {3})" otherwise.
		/// </returns>

		protected override string FormatterImpl( string format, int current, int min, int max ) {
			const string NO_SELECTION = "No regulog has been selected.";
            const string NO_COMPARISON = "A comparison is not possible with this dataset.";

			if ( Script.IsValue( current ) && Script.IsValue( min ) && Script.IsValue( max ) ) {
				/*return selectedRegulog == null ? NO_SELECTION
					: string.Format( "Regulog: {0} {1} (group {2} of {3})",
						selectedRegulog.RegulatorFamily,
						selectedRegulog.RegulatorName,
						current + 1,
						max + 1
				);*/
                if (selectedRegulog == null) {
                    return NO_SELECTION;
                } else if (selectedRegulog.Regulons.Length < 2) {
                    return NO_COMPARISON;
                } else {
			        RegulonInfo selectedNetworkLeft = (RegulonInfo) Enumerable.FirstOrDefault<object>( select_CompareChooserLeftNetwork.SelectedItems, null );
			        RegulonInfo selectedNetworkRight = (RegulonInfo) Enumerable.FirstOrDefault<object>( select_CompareChooserRightNetwork.SelectedItems, null );
                    return string.Format("Comparison between {0} and {1}", selectedNetworkLeft.GenomeName, selectedNetworkRight.GenomeName);
                }
			}
			else {
				return NO_SELECTION;
			}
		}

        /// <summary>
        /// Gets or sets the selected index in the data grid
        /// <para>Probably overriden because it was in the regular local display</para>
        /// </summary>
		[ScriptName( "selectedIndex" )] // Unmangle generated name to allow databinding.
		protected override int SelectedIndex {
			get {
				return selectedIndex;
			}
			set {
				if ( value == selectedIndex ) return;
				selectedIndex = value;
				SelectedRegulog = ( 0 <= value && value <= MaxCollectionIndex ) ? regulogs[value] : null;
				NotifyPropertyChanged( "SelectedIndex" );
				Redraw();
			}
		}

        /// <summary>
        /// Returns the minimum index in the data grid
        /// <para>Probably overriden because it was in the regular local display</para>
        /// </summary>
		[ScriptName( "minCollectionIndex" )] // Unmangle generated name to allow databinding.
		protected override int MinCollectionIndex {
			get {
				return regulogs != null ? 0 : (int) Script.Undefined;
			}
		}

        /// <summary>
        /// Returns the maximum index in the data grid
        /// <para>Probably overriden because it was in the regular local display</para>
        /// </summary>
		[ScriptName( "maxCollectionIndex" )] // Unmangle generated name to allow databinding.
		protected override int MaxCollectionIndex {
			get {
				return regulogs != null ? regulogs.Length - 1 : (int) Script.Undefined;
			}
		}

        /// <summary>
        /// Returns the label text in the data grid
        /// <para>Probably overriden because it was in the regular local display</para>
        /// </summary>
		[ScriptName( "labelText" )] // Unmangle generated name to allow databinding.
		protected override string LabelText {
			get {
				return Script.IsValue( selectedRegulog )
					? String.Format( "Regulog: {0} {1}", selectedRegulog.TaxonName, selectedRegulog.RegulatorName )
					: "Choose a regulog to browse regulogRecords.";
			}
		}

		/// <summary> Updates the content of the compare network choosers to reflect the current set of networks.
		/// </summary>

		private void RefreshCompareNetworkChoosers() {
			if ( selectedRegulog == null || selectedRegulog.Regulons == null ) return;

			RegulonInfo[] items = new RegulonInfo[selectedRegulog.Regulons.Length];
			for ( int i = 0; i < items.Length; i++ ) {
				items[i] = selectedRegulog.Regulons[i];
			}
			select_CompareChooserLeftNetwork.Items = items;
			select_CompareChooserRightNetwork.Items = items;
			select_CompareChooserRightNetwork.SelectedIndex = base.selectedRegulog.Regulons.Length > 1 ? 1 : 0;
		}

        /// <summary>
        /// Listener for when one of the compare chooser drop downs has its selection changed
        /// <para>Updates the display to show the new comparison</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
		private void Select_CompareChooserNetwork_SelectionChanged( object sender, ElementEventArgs eventArgs ) {
			DisplayRegulonsInComparisonFromSelectors();
		}

        /// <summary>
        /// Displays the comparison specified by the two drop down network choosers
        /// </summary>
		private void DisplayRegulonsInComparisonFromSelectors() {

            // Get the two networks from the drop downs
			RegulonInfo selectedNetworkLeft = (RegulonInfo) Enumerable.FirstOrDefault<object>( select_CompareChooserLeftNetwork.SelectedItems, null );
			RegulonInfo selectedNetworkRight = (RegulonInfo) Enumerable.FirstOrDefault<object>( select_CompareChooserRightNetwork.SelectedItems, null );

            // Display the comparison
			DisplayRegulonsInComparison( selectedNetworkLeft, selectedNetworkRight );
		}

        /// <summary>
        /// Performs a set of logical comparisons between two networks
        /// </summary>
        /// <param name="selectedNetworkLeft">The first network to compare</param>
        /// <param name="selectedNetworkRight">The second network to compare</param>
		private void DisplayRegulonsInComparison( RegulonInfo selectedNetworkLeft, RegulonInfo selectedNetworkRight ) {

            // Don't do anything if one of the given networks does not exist
			if ( selectedNetworkLeft != null && selectedNetworkRight != null ) {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "The selected networks for comparison are " + selectedNetworkLeft.ToString() + " and " + selectedNetworkRight.ToString() + "." );
#endif
                // Make a list of the regulons to compare
				currentRegulons = new List<RegulonInfo>();
				currentRegulons.Add( selectedNetworkLeft );
				currentRegulons.Add( selectedNetworkRight );

                // Handlers for whether the display manager is Hammer or not
				if ( DisplayManager is CompareRegulonDisplay ) {
                    (DisplayManager as CompareRegulonDisplay).DisplayRegulonsAllOperations(currentRegulons, base.regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser);
				}

				if ( DisplayManager is HammerCompareRegulonDisplay ) {
                    (DisplayManager as HammerCompareRegulonDisplay).DisplayRegulonsAllOperations(currentRegulons, base.regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser);
				}

                // Set the zoom level to that which is currently selected
                DisplayManager.CurrentZoomLevel = currentZoomLevel;

                // Update the labels in the legend to indicate a pair
                // comparison
                UpdateLegendLabels();

                // Hide the reference chooser since this is a pair comparison
                DisableReferenceChooser();
			}
			else {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "Something went wrong as a null network was selected..." );
#endif
			}
		}

        /// <summary> Configures and renders the display
        /// <para>This works a bit differently to the others due to handling loaded CSVs rather than a RegPrecise data set</para>
		/// </summary>
		protected override void DisplayFullMode() {
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("LocalRegulonGraphViewerSideBySide - entering DisplayFullMode");
#endif*/

            // Note that DisplayFullMode is active
            processingDisplayFullMode = true;

            // Configuration of colours
            // If GO terms are enabled, use the GO term colour chooser
            if (Constants.UseGoTerms) {

                // Use CSS attributes if they are enabled
                if (Constants.UseCssNodeAttributes) {
                    colourChooser = new GOTermCSSAttributeFactory(selectedRegulog);
                } else {
                    colourChooser = new GOTermAttributeFactory(selectedRegulog);
                }

            // If Wedge mode is enabled, use the wedge colour chooser if CSS
            // attributes are disabled
            } else if ( Constants.UseWedgeWheels ) {

                // Use regular CSS attributes if they are enabled
                if (Constants.UseCssNodeAttributes) {
				    colourChooser = new GeneFunctionCSSAttributeFactory( selectedRegulog );
                // Otherwise use the special wedge colour chooser
                } else {
				    colourChooser = new WedgeAttributeFactory( selectedRegulog );
                }

            // Default case is using the gene function colour factory
            } else {

                // Use CSS attributes if they are enabled
				if (Constants.UseCssNodeAttributes) {
				    colourChooser = new GeneFunctionCSSAttributeFactory( selectedRegulog );
                } else {
				    colourChooser = new GeneFunctionAttributeFactory( selectedRegulog );
                }
            }

            // Configuration of nodes
            // Create wedge nodes if that mode is enabled, otherwise create
            // regular circular nodes
            if ( Constants.UseWedgeWheels ) {
                nodeFactory = new WedgeNodeFactory();
            } else {
                nodeFactory = new CircleNodeFactory();
            }

            // Get the current list of regulons
			List<RegulonInfo> regulons = (List<RegulonInfo>) selectedRegulog.Regulons;

			/*if ( scaleChooser != null ) {
				for ( int i = 0; i < regulons.Length; i++ ) {
					regulons[i].DoScale = scaleItems[scaleChooser.SelectedIndex];
				}
			}*/

            // Don't do anything else if there are no regulons
			if ( Script.IsNullOrUndefined( regulons ) ) {
				return;
			}

            // Refresh the gene function chooser if it exists
            if (select_FunctionChooser != null) {
			    RefreshFunctionChooser();
            }

            // Refresh the drop downs for selecting networks to compare
			RefreshCompareNetworkChoosers();

            // Display the networks selected for comparison in the display
            // manager
			DisplayRegulonsInComparisonFromSelectors();

            // Set the zoom level to that which is currently selected
            DisplayManager.CurrentZoomLevel = currentZoomLevel;

            // Refresh the GO term chooser if GO terms are enabled
            if (Constants.UseGoTerms) {
                RefreshTermChooser();
            }

            // Allow the side panel to be toggled on and off if a data set
            // exists
            if (selectedRegulog != null) {
                button_ToggleSidePanel.DomElement.Disabled = false;
            }

            // If there are any selected genes, use the selected gene
            // highlighter
            if (DisplayManager.SelectedGenes.Length > 0) {
				colourChooser.Highlighter = SelectedGeneHighlighter;
            } else {
                colourChooser.Highlighter = null;
            }

            // Add the listener for if selected genes are changed
            DisplayManager.SelectedGenesChanged += DisplayManager_SelectedGenesChanged;

            // Note that DisplayFullMode has finished
            processingDisplayFullMode = false;
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("LocalRegulonGraphViewerSideBySide - exiting DisplayFullMode");
#endif*/
		}

        // Stores the regular version of this display
        private LocalRegulonGraphViewer normalViewer;

        /// <summary>
        /// Connects the regular version of this display so that it can be informed when the data set changes
        /// </summary>
        /// <param name="localRegulonGraphViewer">The regular version of this display</param>
        internal void Connect( LocalRegulonGraphViewer localRegulonGraphViewer ) {

            // Store the regular version
			normalViewer = localRegulonGraphViewer;

            // If an actual regular version was passed into here...
			if ( normalViewer != null ) {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("LocalRegulonGraphViewer and LocalRegulonGraphViewerSideBySide are connected.");
#endif
                // Add a listener to watch for changes in the regular viewer
				normalViewer.PropertyChanged += Instance_PropertyChanged;
			}
#if DEBUG
			else {
				if (Constants.ShowDebugMessages) Console.Log( "LocalRegulonGraphViewer.Instance is null.\n\nIs this a problem?" );
			}
#endif
		}

        /// <summary>
        /// Listens for when a property is changed in the regular display
        /// <para>Used to update this display when the loaded data set is changed</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
		void Instance_PropertyChanged( object sender, PropertyChangedEventArgs eventArgs ) {

            // If the current dataset changed...
			if ( eventArgs.Includes( "SelectedRegulog" ) ) {

                // If the current data set is actually different and exists...
				if ( this.selectedRegulog != LocalRegulonGraphViewer.Instance.SelectedRegulog
					&& LocalRegulonGraphViewer.Instance.SelectedRegulog != null
				) {

                    // Clear the current display manager
                    DisplayManager.Clear();

                    // Set the current data set to the same that was loaded in
                    // the regular display
					this.selectedRegulog = LocalRegulonGraphViewer.Instance.SelectedRegulog;

                    // Notify any listeners that the selected regulog changed
                    // here
                    this.NotifyPropertyChanged( "SelectedRegulog" );

                    // Display the current data set
					DisplayFullMode();

                    // Enable the drop downs for choosing networks to compare
                    select_CompareChooserLeftNetwork.IsEnabled = true;
                    select_CompareChooserRightNetwork.IsEnabled = true;
				}
			}

/*           if ( args.Includes( "DisplayManager" ) ) {

                if (localViewer.DisplayManager is HammerGroupRegulonDisplay) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("LocalRegulonGraphViewer is using a HammerGroupRegulonDisplay.");
#endif
                }
                else if (localViewer.DisplayManager is HammerGroupFreeRegulonDisplay) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("LocalRegulonGraphViewer is using a HammerGroupFreeRegulonDisplay.");
#endif
                } else {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("LocalRegulonGraphViewer is not using a HammerGroupRegulonDisplay or HammerGroupFreeRegulonDisplay.");
#endif
                }
            }*/
		}

        /// <summary>
        /// Displays a comparison of more than two networks
        /// <para>Used for both multiple comparisons and when the user presses on a group container in the regular display (when it contains more than two networks); the latter is currently disabled</para>
        /// </summary>
        /// <param name="groupRegulons">The networks from that group which will be compared</param>
        /// <param name="referenceIndex">The index of the reference network</param>
        private void DisplayGroupComparison( List<RegulonInfo> groupRegulons, int referenceIndex ) {

            // If a valid network list was sent and there is more than one
            // network in it
			if ( groupRegulons != null && groupRegulons.Length > 2 ) {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "The selected networks for comparison are:" );
                foreach (RegulonInfo regulon in groupRegulons) {
                    if (Constants.ShowDebugMessages) Console.Log(regulon.GenomeName);
                }
#endif
                // Create a temporary list for the networks in the group
                List<RegulonInfo> groupRegulonsTemp = new List<RegulonInfo>();

                // Store the networks in the temporary list
                for ( int i = 0; i < groupRegulons.Length ; i++) {
                    groupRegulonsTemp[i] = groupRegulons[i];
                }

                // If the reference chooser drop down exists, make it visible
                // and enabled
                if ( select_CompareChooserReference != null ) {
                    tableRow_CompareChooserReference.Style.Visibility = "visible";
                    select_CompareChooserReference.DomElement.Disabled = false;

                    // Create a list of items to place inside the drop down
                    RegulonInfo[] referenceItems = new RegulonInfo[groupRegulonsTemp.Length];
			        for ( int i = 0; i < referenceItems.Length; i++ ) {
				        referenceItems[i] = groupRegulonsTemp[i];
			        }

                    // Set the drop down's items to the list and set the
                    // selected index to be the reference index that was
                    // specified
			        select_CompareChooserReference.Items = referenceItems;
			        select_CompareChooserReference.SelectedIndex = referenceIndex;
                }

                // If the reset to two button exists, make it visible and
                // enabled
                if ( button_ResetToTwo != null ) {
                    button_ResetToTwo.DomElement.Style.Visibility = "visible";
                    button_ResetToTwo.DomElement.Disabled = false;
                }

                // If the regulon one drop down exists, make it invisible and
                // disabled
                if ( select_CompareChooserLeftNetwork != null ) {
                    tableRow_CompareChooserLeftNetwork.Style.Visibility = "hidden";
                    select_CompareChooserLeftNetwork.DomElement.Disabled = true;
                }

                // If the regulon two drop down exists, make it invisible and
                // disabled
                if ( select_CompareChooserRightNetwork != null ) {
                    tableRow_CompareChooserRightNetwork.Style.Visibility = "hidden";
                    select_CompareChooserRightNetwork.DomElement.Disabled = true;
                }

                // If the reference is not the first network in the group...
                if (referenceIndex > 0) {

                    // Temporarily store the reference network
                    RegulonInfo selectedNetworkReference = groupRegulonsTemp[referenceIndex];

                    // Remove it from its current position in the list of
                    // networks
                    groupRegulonsTemp.RemoveAt(referenceIndex);

                    // Add it at the start of the list of networks
                    groupRegulonsTemp.Insert(0, selectedNetworkReference);
                }

                // Not all display manager types have the functions so
                // the type it is needs to be confirmed first
				if ( DisplayManager is CompareRegulonDisplay ) {

                    // Clear the current data set in the display manager
                    DisplayManager.Clear();

                    // Create a comparison of the current networks in the
                    // display manager
				    (DisplayManager as CompareRegulonDisplay).DisplayRegulonsWithOperation(groupRegulonsTemp, base.regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser, LogicalOperations.OR);

                    // Set the zoom level to that which is currently selected
                    DisplayManager.CurrentZoomLevel = currentZoomLevel;
				}

				if ( DisplayManager is HammerCompareRegulonDisplay ) {

                    // Clear the current data set in the display manager
                    DisplayManager.Clear();

                    // Create a comparison of the current networks in the
                    // display manager
				    (DisplayManager as HammerCompareRegulonDisplay).DisplayRegulonsWithOperation(groupRegulonsTemp, base.regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser, LogicalOperations.OR);

                    // Set the zoom level to that which is currently selected
                    DisplayManager.CurrentZoomLevel = currentZoomLevel;
				}

                /*if ( DisplayManager is HammerCompareFreeRegulonDisplay ) {
                    DisplayManager.Clear();
				    (DisplayManager as HammerCompareFreeRegulonDisplay).DisplayRegulonsWithOperation(groupRegulonsTemp, base.regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser, LogicalOperations.OR);
                    DisplayManager.CurrentZoomLevel = currentZoomLevel;
				}*/

                // Update the labels in the legend if needed
                UpdateLegendLabels();
			}
			else {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "Something went wrong as a null or empty list of regulons was passed..." );
#endif
			}
		}
        // Used to check if the user pressed on a group container in the
        // regular viewer
        // Not actually used at the moment, but was used to perform a
        // comparison of the whole group
        bool groupPressed = false;

        /*private void LocalRegulonGraphViewerSideBySide_GroupPressed(object sender, GroupPressedEventArgs e)
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Group " + e.Group + " was pressed in LocalRegulonGraphViewer's HammerGroup(Free)RegulonDisplay.");
#endif
            List<RegulonInfo> groupRegulons = new List<RegulonInfo>();

            if (sender is HammerCompareFreeRegulonDisplay) {
                foreach (RegulonGraph graph in (sender as HammerGroupFreeRegulonDisplay).RegulonGraphs) {
                    if (graph.Regulon.GroupNumber == e.Group) {

                        // If this is the centroid of the group, put it at the beginning
                        if (graph.GraphLabel.Text.IndexOf(" (centroid)") != -1) {
                            groupRegulons.Insert(0, graph.Regulon);
                        } else {
                            groupRegulons.Add(graph.Regulon);
                        }
                    }
                }
            }

            if (sender is HammerGroupRegulonDisplay) {

                foreach (RegulonGraph graph in (sender as HammerGroupRegulonDisplay).RegulonGraphs[e.Group]) {

                    // If this is the centroid of the group, put it at the beginning
                    if ((sender as HammerGroupRegulonDisplay).CurrentCentroids.IndexOf(selectedRegulog.Regulons.IndexOf(graph.Regulon)) != -1) {
                        groupRegulons.Insert(0, graph.Regulon);
                    } else {
                        groupRegulons.Add(graph.Regulon);
                    }
                }
            }
//#if DEBUG
//            foreach (RegulonInfo regulon in groupRegulons) {
//                if (Constants.ShowDebugMessages) Console.Log(regulon.GenomeName + " is in this group.");
//            }
//#endif
            DisplayGroupComparison(groupRegulons, 0);
            this.groupPressed = true;
            this.IsActivated = true;
        }*/

        /// <summary>
        /// Updates the labels in the legend depending on how many regulons there are in the comparison
        /// <para>Also updates the visibility of the explanation of AND, OR and XOR</para>
        /// </summary>
        private void UpdateLegendLabels() {

            // If there are more than two networks being compared
            if (currentRegulons.Length > 2) {

                // Set the text of the first three legend captions to the
                // correct ones for this comparison
                this.LegendCaptionBothCell.TextContent = Constants.Text_LegendCaptionBothMultiple;
                this.LegendCaptionLeftCell.TextContent = Constants.Text_LegendCaptionLeftMultiple;
                this.LegendCaptionRightCell.TextContent = Constants.Text_LegendCaptionRightMultiple;

                // Make the "some" row visible (depending on whether the legend
                // itself is currently visible)
                this.LegendRowSome.Style.Visibility = "inherit";

                // Make the logical operation explanation invisible
                div_PairComparisonExplanation.Style.Visibility = "hidden";
            }

            // Otherwise if this is a pair comparison
            if (currentRegulons.Length == 2) {

                // Set the text of the first three legend captions to the
                // correct ones for this comparison
                this.LegendCaptionBothCell.TextContent = Constants.Text_LegendCaptionBoth;
                this.LegendCaptionLeftCell.TextContent = Constants.Text_LegendCaptionLeft;
                this.LegendCaptionRightCell.TextContent = Constants.Text_LegendCaptionRight;

                // Make the "some" row invisible
                this.LegendRowSome.Style.Visibility = "hidden";

                // Make the logical operation explanation visible (depending on
                // whether the legend itself is currently visible)
                div_PairComparisonExplanation.Style.Visibility = "inherit";
            }
        }

        // Stores whether the comparison is currently locked (false = unlocked,
        // true = locked)
        private bool comparisonLocked = false;

        // The touch button and label for the comparison lock
        private Button touchButton_LockComparison;
        private Element label_LockComparison;

        /// <summary>
        /// Listens for when the comparison lock touch button is clicked
        /// <para>Toggles the comparison lock</para>
        /// </summary>
		/// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_LockComparison_Clicked(object sender, ElementEventArgs eventArgs) {

            // Toggle the status of the comparison lock
            comparisonLocked = !comparisonLocked;

            // If the comparison is locked, show that it is locked via the
            // button's image and label, otherwise show that it is unlocked
            if (comparisonLocked) {
                touchButton_LockComparison.DomElement.ClassName = "TouchButton ComparisonLockActive";
                label_LockComparison.TextContent = Constants.Text_ButtonComparisonLockOn;
            } else {
                touchButton_LockComparison.DomElement.ClassName = "TouchButton ComparisonLockInactive";
                label_LockComparison.TextContent = Constants.Text_ButtonComparisonLockOff;
            }
        }

        /// <summary>
        /// Disables the reference chooser when only two networks are selected
        /// </summary>
        private void DisableReferenceChooser() {

            // If the reference chooser drop down exists, make it invisible
            // and disabled, and clear its list of items
            if ( select_CompareChooserReference != null ) {
                tableRow_CompareChooserReference.Style.Visibility = "hidden";
                select_CompareChooserReference.DomElement.Disabled = true;
                select_CompareChooserReference.Items = null;
            }

            // If the reset to two button exists, make it invisible
            // and disabled
            if ( button_ResetToTwo != null ) {
                button_ResetToTwo.DomElement.Style.Visibility = "hidden";
                button_ResetToTwo.DomElement.Disabled = true;
            }

            // If the regulon one drop down exists, make it visible and enabled
            if ( select_CompareChooserLeftNetwork != null ) {
                tableRow_CompareChooserLeftNetwork.Style.Visibility = "visible";
                select_CompareChooserLeftNetwork.DomElement.Disabled = false;
            }

            // If the regulon two drop down exists, make it visible and enabled
            if ( select_CompareChooserRightNetwork != null ) {
                tableRow_CompareChooserRightNetwork.Style.Visibility = "visible";
                select_CompareChooserRightNetwork.DomElement.Disabled = false;
            }
        }

        /// <summary>
        /// Listens for when the selection in the reference drop down is changed
        /// <para>Changes the reference when comparing more than two networks</para>
        /// </summary>
		/// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void Select_CompareChooserReference_SelectionChanged( object sender, ElementEventArgs eventArgs ) {

            // Display a group comparison with the new reference
			DisplayGroupComparison(currentRegulons, select_CompareChooserReference.SelectedIndex);
		}

        /// <summary>
        /// Listens for when the reset to two button is clicked
        /// <paras>Reverts the comparison back to the default pair comparison</paras>
        /// </summary>
		/// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void button_ResetToTwo_Clicked(object sender, ElementEventArgs eventArgs) {

            // Display a pair comparison using the current networks in the two
            // network drop downs
            DisplayRegulonsInComparisonFromSelectors();
        }
	}
}
