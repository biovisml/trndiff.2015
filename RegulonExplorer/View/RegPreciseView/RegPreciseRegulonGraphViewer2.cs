﻿using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using SystemQut.ComponentModel;
using SystemQut.Controls;
using SystemQut.Linq;
using System.Collections;
using ElementEventHandler = SystemQut.Controls.ElementEventHandler;
using SystemQut;
using RegulonExplorer.Common;
#if DEBUG
using SystemQUT.Csv;
using SystemQut.IO;
using System.Html.Data.Files;
#endif

namespace RegulonExplorer.View.RegPreciseView {

	/// <summary> Host control for the famous TrnDiff "wagon-wheel" display :^)
	/// </summary>

    public class RegPreciseRegulonGraphViewer2 : RegPreciseRegulonGraphViewerBase
    {
        public static RegPreciseRegulonGraphViewer2 Instance;

        /// <summary> Allows users to select the number of groups in the display
        /// </summary>
        private Select select_GroupsChooser;

        private int numberOfGroups = 1;

        private Select select_AutoGroupChooser;

        /// <summary> Currently available items for groups chooser.
        /// </summary>
        private string[] groupsItems = { "1", "2", "3", "4", "5" };

        private string[] autoGroupItems = { "YES", "NO" };

        /// <summary> The slider that allows the user to decide on the ratio
        /// between Hamming and Eucildean distance
        /// </summary>
        private InputElement slider_HEDistance;

        /// <summary> The row for the above, so we can hide both the slider and
        /// label when they are not needed
        /// </summary>
        private TableRowElement tableRow_HEDistance;

        /// <summary> The button that allows the user to recalculate the
        /// automatic groups
        /// </summary>
        private Button button_recalculateGroups;

        /// <summary> Touch-friendly button for toggling automatic grouping
        /// </summary>
        private Button touchButton_ToggleAutoGroup;

        /// <summary> Touch-friendly button for recalculating automatic groups
        /// </summary>
        private Button touchButton_RecalculateGroups;

        /// <summary> Label for automatic grouping toggle button
        /// </summary>
        private Element label_AutoGrouping;

        /// <summary> Touch-friendly button for toggling automatic centroid
        /// calculation on drag and drop
        /// </summary>
        private Button touchButton_ToggleAutoCentroid;

        /// <summary> Label for automatic centroid toggle button
        /// </summary>
        private Element label_AutoCentroid;

        /// <summary> Stores the currently selected ratio for this viewer
        /// </summary>
        private double ratio;

        /// <summary> Touch-friendly button for changing the number of groups
        /// </summary>
        private Button touchButton_NumberOfGroups;

        protected DivElement touchButton_NumberOfGroupsPopup;

        /// <summary> Touch-friendly button for sorting regulons
        /// </summary>
        private Button touchButton_Sort;

        protected DivElement touchButton_SortPopup;

#if DEBUG
        private Button touchButton_Slider;
#endif

        protected DivElement touchButton_SliderPopup;

        private TextBox textBox_NumberOfGroups;
        private Button button_NumberOfGroups;

        private ToolButton activatingButton;
        private ToolButton activatingButtonSideBySide;

#if DEBUG
        private Button touchButton_ColourBlind;
#endif
#if DEBUG
        private Button touchButton_SelectCentroids;
#endif

        /// <summary>
        /// The current setting for automatic grouping
        /// <para>true = on (YES), false = off (NO)</para>
        /// </summary>
        bool currentAutoGroupSetting = true;

		// TODO: cook up a better name for this class!

		/// <summary> Create a new regulon graph viewer bound to a provided DOM element which
		///		should already exist in the application html document.
		///	<para>
		///		The element should contain a nested block element of some sort to hold the view
		///		(data-name="RegulonDisplay", data-codebehind="CodeBehind") and a record navigator
		///		(data-name="Pager", data-codebehind="RecordNavigator").
		/// </para>
		/// </summary>
		/// <param name="element"></param>
		public RegPreciseRegulonGraphViewer2 ( Element element )
            : base(element)
        {
            // Set this to be the current instance, so that the side by side
            // version can take data from it
            Instance = this;

#if DEBUG
            // Find the activating buttons for both this and the side by side
            // version
            CodeBehind.ForallControlsOfType( typeof( ToolButton ), Document.Body, delegate( ToolButton toolButton ) {
                if (toolButton.ActivatedId == "RegPreciseGeneRegulonViewer") {
                    activatingButton = toolButton;
                }
                if (toolButton.ActivatedId == "RegPreciseGeneRegulonViewerSideBySide") {
                    activatingButtonSideBySide = toolButton;
                }
            } );
#endif
#if DEBUG
            if (Constants.UseHammerTouchSupport) {
                DataLayer.Instance.RegulogLoaded += DataLayer_RegulogLoaded;
            }
#endif
		}

        /// <summary> Locates the required nested controls, matching by name and type.
		///		Override this to locate additional child controls.
		/// <para>
		///		Remember to call base.FindChildControls!
		/// </para>
		/// </summary>
        protected override void FindChildControls()
        {
            base.FindChildControls();
            select_GroupsChooser = (Select)FindControlByNameAndType("GroupsChooser", typeof(Select));
            select_AutoGroupChooser = (Select)FindControlByNameAndType("AutoGroupChooser", typeof(Select));
            div_SidePanel = (DivElement)Document.GetElementById("SidePanel_RegPrecise2");
            table_Legend = (TableElement)Document.GetElementById("Legend_RegPrecise2");
            div_RegulonDisplayHolder = (DivElement)Document.GetElementById("RegulonDisplayHolder_RegPrecise2");
            slider_HEDistance = (InputElement)Document.GetElementById("GroupSlider2");
            tableRow_HEDistance = (TableRowElement)Document.GetElementById("SliderRow2");
            button_recalculateGroups = (Button)FindControlByNameAndType("RecalculateButton", typeof(Button));
            touchButton_ToggleAutoGroup = (Button)FindControlByNameAndType("TouchButton_ToggleAutoGroup", typeof(Button));
            touchButton_RecalculateGroups = (Button)FindControlByNameAndType("TouchButton_RecalculateGroups", typeof(Button));
            label_AutoGrouping = (Element)Document.GetElementById("AutoGroupingLabel_2");
            touchButton_ToggleAutoCentroid = (Button)FindControlByNameAndType("TouchButton_ToggleAutoCentroid", typeof(Button));
            label_AutoCentroid = (Element)Document.GetElementById("AutoCentroidLabel_2");
            touchButton_HighlightFunctionPopup = (DivElement)Document.GetElementById("HighlightFunctionPopup_RegPrecise2");
            touchButton_HighlightTermPopup = (DivElement)Document.GetElementById("HighlightTermPopup_RegPrecise2");
            touchButton_NumberOfGroups = (Button)FindControlByNameAndType("TouchButton_NumberOfGroups", typeof(Button));
            touchButton_NumberOfGroupsPopup = (DivElement)Document.GetElementById("NumberOfGroupsPopup_RegPrecise2");
            touchButton_Sort = (Button)FindControlByNameAndType("TouchButton_Sort", typeof(Button));
            touchButton_SortPopup = (DivElement)Document.GetElementById("SortPopup_RegPrecise2");
            touchButton_SearchTermPopup = (DivElement)Document.GetElementById("SearchTermPopup_RegPrecise2");
#if DEBUG
            touchButton_Slider = (Button)FindControlByNameAndType("TouchButton_Slider", typeof(Button));
            touchButton_SliderPopup = (DivElement)Document.GetElementById("SliderPopup_RegPrecise2");
#endif
            textBox_NumberOfGroups = (TextBox)FindControlByNameAndType("Group_NumberInputText", typeof(TextBox));
            button_NumberOfGroups = (Button)FindControlByNameAndType("Group_NumberInputButton", typeof(Button));

            div_LoadingOverlay = (DivElement)Document.GetElementById("loadingOverlay");

#if DEBUG
            touchButton_ColourBlind = (Button)FindControlByNameAndType("TouchButton_ColourBlind", typeof(Button));
#endif
#if DEBUG
            touchButton_SelectCentroids = (Button)FindControlByNameAndType("TouchButton_SelectCentroids", typeof(Button));
#endif

            label_SelectedGenes = (Element)Document.GetElementById("Label_SelectedGenes_RegPrecise2");
        }

        // Used to store a reference to the side by side version of this
        // display
        private RegPreciseRegulonGraphViewer2SideBySide sideBySide;

        /// <summary>
        /// Binds functions to relevant controls in the display
		/// <para>
		///		Overriden version to bind controls specific to this display
		/// </para>
		/// </summary>
		protected override void BindChildControls () {
			base.BindChildControls();

            // Connect the side by side display
            sideBySide = RegPreciseRegulonGraphViewer2SideBySide.Instance;
            if (sideBySide != null) {
                sideBySide.Connect(this);
            }

            // Add the options to the groups chooser
            if (select_GroupsChooser != null)
            {
                select_GroupsChooser.Items = groupsItems;
                select_GroupsChooser.SelectionChanged += new ElementEventHandler(groupsChooser_SelectionChanged);
            }

            // Add the options to the automatic grouping chooser
            if (select_AutoGroupChooser != null)
            {
                select_AutoGroupChooser.Items = autoGroupItems;
                select_AutoGroupChooser.SelectionChanged += new ElementEventHandler(autoGroupChooser_SelectionChanged);
            }

            // Add the event to the recalculate button
            if (button_recalculateGroups != null) {
                button_recalculateGroups.Clicked += new ElementEventHandler(recalculateButton_Clicked);
            }

            //Add the action for the auto group toggle button
            if ( touchButton_ToggleAutoGroup != null ) {
				touchButton_ToggleAutoGroup.Clicked += new ElementEventHandler( touchButton_ToggleAutoGroup_clicked );
			}

            // Add the event to the recalculate button (2)
            if ( touchButton_RecalculateGroups != null ) {
				touchButton_RecalculateGroups.Clicked += new ElementEventHandler( recalculateButton_Clicked );
			}

            //Add the action for the auto centroid toggle button
            if ( touchButton_ToggleAutoCentroid != null ) {

                // If the auto centroid button is enabled in the constants, add
                // its event handler, otherwise remove it from the display
                if (Constants.UseAutoCentroidButton) {
				    touchButton_ToggleAutoCentroid.Clicked += new ElementEventHandler( touchButton_ToggleAutoCentroid_clicked );
                } else {
                    Element temp4 = (TableCellElement)Document.GetElementById("AutoCentroidButton_2");
                    temp4.ParentNode.RemoveChild(temp4);
                    temp4 = (TableCellElement)Document.GetElementById("AutoCentroidLabel_2");
                    temp4.ParentNode.RemoveChild(temp4);
                }
			}

            // Get the initial ratio value
            if ( slider_HEDistance != null ) {
                ratio = double.Parse(slider_HEDistance.Value)/100;
            } else {
                ratio = 0.5;
            }

            //Add the action for the change number of groups button
            if ( touchButton_NumberOfGroups != null ) {
				touchButton_NumberOfGroups.Clicked += new ElementEventHandler( touchButton_NumberOfGroups_Clicked );
                Document.Body.AddEventListener(MouseEventType.click, SubBodyOnClick, false);
			}

            // Set up the drop down for the number of groups touch button
            if ( touchButton_NumberOfGroupsPopup != null ) {

                // If there's no group limit, set up the text box and button
                // to allow users to enter a number of groups
                if (Constants.NoGroupLimit) {
                    if ( textBox_NumberOfGroups != null && button_NumberOfGroups != null ) {

                        // Listener for the search button
				        button_NumberOfGroups.Clicked += new ElementEventHandler( group_NumberInputButton_Clicked );

                        // Listener for pressing enter in the text box
                        textBox_NumberOfGroups.DomElement.AddEventListener("keyup", group_NumberInputText_KeyUp, false);
			        }

                // Otherwise, set up the options in the drop down
                } else {

                    // Remove the text box and button if they exist
                    if (textBox_NumberOfGroups != null) {
                        textBox_NumberOfGroups.RemoveElement();
                        textBox_NumberOfGroups.Dispose();
                        textBox_NumberOfGroups = null;
                    }
                    if (button_NumberOfGroups != null) {
                        button_NumberOfGroups.RemoveElement();
                        button_NumberOfGroups.Dispose();
                        button_NumberOfGroups = null;
                    }

                    // Remove any existing labels from the drop down
                    while (touchButton_NumberOfGroupsPopup.ChildNodes.Length > 0)
                    {
                        touchButton_NumberOfGroupsPopup.RemoveChild(touchButton_NumberOfGroupsPopup.ChildNodes[0]);
                    }

                    // Add a clickable label for each of the five options
                    foreach (string item in groupsItems) {

                        // Create the label element
                        Element label = Document.CreateElement("label");

                        // Set the text and attributes
                        label.TextContent = item;
                        label.ClassName = "ChooserPopupLabel";

                        // Add a click listener
                        label.AddEventListener(MouseEventType.click, GroupLabelOnClick, false);

                        // Add the label to the drop down
                        touchButton_NumberOfGroupsPopup.AppendChild(label);
                    }
                }
            }

            //Add the action for the sort button
            if ( touchButton_Sort != null ) {
				touchButton_Sort.Clicked += new ElementEventHandler( touchButton_Sort_Clicked );
                Document.Body.AddEventListener(MouseEventType.click, SubBodyOnClick, false);
			}

            // Add the labels to the touch sort button drop down
            if ( touchButton_SortPopup != null ) {

                // Remove any existing labels from the drop down
                while (touchButton_SortPopup.ChildNodes.Length > 0)
                {
                    touchButton_SortPopup.RemoveChild(touchButton_SortPopup.ChildNodes[0]);
                }

                // Add a clickable label for each of the five options
                foreach (string item in sortItems) {

                    // Create the label element
                    Element label = Document.CreateElement("label");

                    // Set the text and attributes
                    label.TextContent = item;
                    label.ClassName = "ChooserPopupLabel";

                    // Add a click listener
                    label.AddEventListener(MouseEventType.click, SortLabelOnClick, false);

                    // Add the label to the drop down
                    touchButton_SortPopup.AppendChild(label);
                }

                // (TEMP) Adding filter options
                // Add a placeholder label for the divider
                Element placeholderLabel = Document.CreateElement("label");

                // Set the text and attributes
                placeholderLabel.TextContent = Constants.Text_OptionPlaceholder;
                placeholderLabel.ClassName = "ChooserPopupLabel";
                placeholderLabel.Style.Cursor = "default";

                // Add the label to the drop down
                touchButton_SortPopup.AppendChild(placeholderLabel);

                // Add a clickable label for each of the two options
                foreach (string item in filterItems) {

                    // Create the label element
                    Element label = Document.CreateElement("label");

                    // Set the text and attributes
                    label.TextContent = item;
                    label.ClassName = "ChooserPopupLabel";

                    // Add a click listener
                    label.AddEventListener(MouseEventType.click, FilterLabelOnClick, false);

                    // Add the label to the drop down
                    touchButton_SortPopup.AppendChild(label);
                }
            }

            /* Euclidean/Hamming distance slider testing
             Button should always be removed if slider constant is not enabled */
#if DEBUG
            if (Constants.UseVariableHEDistance) {

                // Add the events for clicking on the button and for dismissing
                // the drop down
                if ( touchButton_Slider != null) {
				    touchButton_Slider.Clicked += new ElementEventHandler( touchButton_Slider_Clicked );
                    Document.Body.AddEventListener(MouseEventType.click, SubBodyOnClick, false);
                }

            // Remove the button if the setting is not enabled
            } else {
#endif
                Element temp = (TableCellElement)Document.GetElementById("SliderButton_RegPrecise2");
                temp.ParentNode.RemoveChild(temp);
                temp = (TableCellElement)Document.GetElementById("SliderLabel_RegPrecise2");
                temp.ParentNode.RemoveChild(temp);
#if DEBUG
            }
#endif
            /* Colour blind toggle testing (colour blind friendly colours not actually implemented yet)
             Button should always be removed if colour blind constant is not enabled */
#if DEBUG
            if (Constants.UseColourBlindToggle) {

                // Add the events for clicking on the button
                if ( touchButton_ColourBlind != null) {
				    touchButton_ColourBlind.Clicked += new ElementEventHandler( touchButton_ColourBlind_Clicked );
                }
            } else {
#endif
                // Remove the button if the setting is not enabled
                Element temp2 = (TableCellElement)Document.GetElementById("ColourBlind_RegPrecise2");
                temp2.ParentNode.RemoveChild(temp2);
                temp2 = (TableCellElement)Document.GetElementById("ColourBlindLabel_RegPrecise2");
                temp2.ParentNode.RemoveChild(temp2);
#if DEBUG
            }
#endif
            /* Selected networks as centroids button testing
             Button should always be removed if selected networks as centroids constant is not enabled */
#if DEBUG
            if (Constants.SelectedNetworksAsCentroids) {

                // Add the events for clicking on the button
                if ( touchButton_SelectCentroids != null) {
				    touchButton_SelectCentroids.Clicked += new ElementEventHandler( touchButton_SelectCentroids_Clicked );
                }
            } else {
#endif
                // Remove the button if the setting is not enabled
                Element temp3 = (TableCellElement)Document.GetElementById("SelectCentroids_RegPrecise2");
                temp3.ParentNode.RemoveChild(temp3);
                temp3 = (TableCellElement)Document.GetElementById("SelectCentroidsLabel_RegPrecise2");
                temp3.ParentNode.RemoveChild(temp3);
#if DEBUG
            }
#endif
		}

		/// <summary> Overrides GetSelectedRegulog to return the current regulog in the RegulogChooser.
		/// <para>
		///		This method invokes the action callback immediately.
		/// </para>
		/// </summary>
		/// <param name="action"></param>

		protected override void GetSelectedRegulog ( Action<RegulogInfo> action ) {
			RegulonInfo item = Script.IsValue( sourceDataGrid ) ? (RegulonInfo) sourceDataGrid.SelectedItem : null;

			if ( item == null ) {
				action( null );
			}
			else {
				DataLayer.Instance.GetRegulog( item.RegulogId, action );
			}
		}

		/// <summary> Text formatter for the pager.
		/// </summary>
		/// <param name="format">
		///		The current format string in use by the pager. The argument is ignored
		///		by the current instance.
		/// </param>
		/// <param name="current">
		///		The current index within the associated collection. This may be undefined
		///		if the collection has no "current" element, such as when the no record is
		///		selected in a data grid.
		/// </param>
		/// <param name="min">
		///		The (possibly undefined) minimum value in the range covered by the pager.
		///		See note for <c>current</c>.
		/// </param>
		/// <param name="max">
		///		The (possibly undefined) maximum value in the range covered by the pager.
		///		See note for <c>current</c>.
		/// </param>
		/// <returns>
		///		A string that represents the current state. Returns a NO_SELECTION literal
		///		if any of the arguments are undefined or no regulog is selected. Returns
		///		a formatted string in the form "Regulog: {0} {1} (group {2} of {3})" otherwise.
		/// </returns>

		protected override string FormatterImpl ( string format, int current, int min, int max ) {
			const string NO_SELECTION = "No regulon has been selected.";

			if ( Script.IsValue( current ) && Script.IsValue( min ) && Script.IsValue( max ) ) {
				RegulonInfo selectedRegulon = Script.IsValue( sourceDataGrid )
					? (RegulonInfo) sourceDataGrid.ItemAt( current ) : null;

				return Script.IsNullOrUndefined( selectedRegulon )
					? NO_SELECTION
					: string.Format( "Regulon: {0} {1} (group {2} of {3})",
						selectedRegulon.GenomeName,
						selectedRegulon.RegulatorName,
						current + 1,
						max + 1
				);
			}
			else {
				return NO_SELECTION;
			}
		}

        /// <summary> Changes the number of groups when the selection in the
        /// groups chooser is changed
        /// <para>This is used even if the touch button and drop down are present - it is invisible and used programatically</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void groupsChooser_SelectionChanged(object sender, ElementEventArgs eventArgs)
        {
            // Don't do anything if the number of groups selected is greater
            // than the number of networks in the data set
            if (int.Parse(groupsItems[select_GroupsChooser.SelectedIndex]) > DisplayManager.NumberOfGraphs) {
                Window.Alert(Constants.Text_MessageTooManyGroups);
                return;
            }

            numberOfGroups = int.Parse(groupsItems[select_GroupsChooser.SelectedIndex]);

            if (selectedRegulog != null) {
                SetNumberOfGroups();
            }
        }

        /// <summary>
        /// Sets the number of groups in the data set, and updates the display
        /// manager to show that number
        /// </summary>
        private void SetNumberOfGroups() {

            // Change interface elements in response to the new group setting
            ToggleAutoGroupSettingElements();

            // Set the data set's number of groups
            //SelectedRegulog.NumGroups = numberOfGroups;

            // Simply change the number of groups to the given number if Group
            // type displays are always in use
#if DEBUG
            if (Constants.UseGroupDisplaysGlobally) {
#endif
                if (DisplayManager is GroupRegulonDisplay) {
                    (DisplayManager as GroupRegulonDisplay).SetNumberOfGroups(numberOfGroups, true);

                    // Recalculate groups if auto grouping is currently on
                    if (currentAutoGroupSetting) {
                        (DisplayManager as GroupRegulonDisplay).SetNumberOfGroups(numberOfGroups, true);
                        (DisplayManager as GroupRegulonDisplay).RecalculateGroups();
                        SortLabelOnClick(null);
                    }
                }
                if (DisplayManager is HammerGroupRegulonDisplay) {
                    (DisplayManager as HammerGroupRegulonDisplay).SetNumberOfGroups(numberOfGroups, true);

                    // Recalculate groups if auto grouping is currently on
                    if (currentAutoGroupSetting) {
                        (DisplayManager as HammerGroupRegulonDisplay).SetNumberOfGroups(numberOfGroups, true);
                        (DisplayManager as HammerGroupRegulonDisplay).RecalculateGroups();
                        SortLabelOnClick(null);
                    }
                }

#if DEBUG
            // Otherwise, change display types as required
            } else {
                // If the number of groups is set to one...
                if (numberOfGroups == 1)
                {

                    // If the display manager is a group display manager, change it
                    // back to a regular display manager
                    if (DisplayManager is GroupRegulonDisplay || DisplayManager is HammerGroupRegulonDisplay)
                    {
                        // Store the selected regulons and genes for restoration
                        // later
                        List<RegulonInfo> tempSelectedRegulons = new List<RegulonInfo>();
                        foreach (RegulonInfo regulon in DisplayManager.SelectedRegulons) {
                            tempSelectedRegulons.Add(regulon);
                        }
                        List<GeneInfo> tempSelectedGenes = new List<GeneInfo>();
                        foreach (GeneInfo gene in DisplayManager.SelectedGenes) {
                            tempSelectedGenes.Add(gene);
                        }

                        //DisplayManager.Clear(); //This is superfluous since changing the DisplayManager property already does this (?)

                        // Remove listeners from the display managaer
                        DisplayManager.PropertyChanged -= DisplayManager_PropertyChanged;

                        // Create a new regular display manager
                        if (Constants.UseHammerTouchSupport) {
                            DisplayManager = new HammerRegulonDisplay();
                        } else {
                            DisplayManager = new DefaultRegulonDisplay();
                        }

                        // Restore existing selections
                        // This uses a timeout to ensure that the selection is
                        // restored after the rest of the data set is loaded
                        Script.SetTimeout((Action) delegate {
                            DisplayManager.SelectedRegulons = tempSelectedRegulons;
                            DisplayManager.SelectedGenes = tempSelectedGenes;
                        }, 100, null);

                        // Re-add the listener to the display manager
                        DisplayManager.PropertyChanged += DisplayManager_PropertyChanged;
                    }

                    // Set the number of groups and each regulon's group number to
                    // nothing in the underlying data
                    /*SelectedRegulog.NumGroups = null;
                    foreach (RegulonInfo regulon in SelectedRegulog.Regulons) {
                        regulon.GroupNumber = null;
                    }*/
                }

                // Else if changing to a number of groups greater than one
                else
                {
                    // Simply change the number of groups if the display manager is
                    // already a group display manager
                    if ((DisplayManager is GroupRegulonDisplay || DisplayManager is HammerGroupRegulonDisplay))
                    {
                        if (DisplayManager is GroupRegulonDisplay) {
                            (DisplayManager as GroupRegulonDisplay).SetNumberOfGroups(numberOfGroups, true);

                            // Recalculate groups if auto grouping is currently on
                            if (currentAutoGroupSetting) {
                                (DisplayManager as GroupRegulonDisplay).SetNumberOfGroups(numberOfGroups, true);
                                (DisplayManager as GroupRegulonDisplay).RecalculateGroups();
                                SortLabelOnClick(null);
                            }
                        }
                        if (DisplayManager is HammerGroupRegulonDisplay) {
                            (DisplayManager as HammerGroupRegulonDisplay).SetNumberOfGroups(numberOfGroups, true);

                            // Recalculate groups if auto grouping is currently on
                            if (currentAutoGroupSetting) {
                                (DisplayManager as HammerGroupRegulonDisplay).SetNumberOfGroups(numberOfGroups, true);
                                (DisplayManager as HammerGroupRegulonDisplay).RecalculateGroups();
                                SortLabelOnClick(null);
                            }
                        }
                    }

                    // Else replace the regular display manager with a group
                    // display manager
                    else
                    {
                        // Store the selected regulons and genes for restoration
                        // later
                        List<RegulonInfo> tempSelectedRegulons = new List<RegulonInfo>();
                        foreach (RegulonInfo regulon in DisplayManager.SelectedRegulons) {
                            tempSelectedRegulons.Add(regulon);
                        }
                        List<GeneInfo> tempSelectedGenes = new List<GeneInfo>();
                        foreach (GeneInfo gene in DisplayManager.SelectedGenes) {
                            tempSelectedGenes.Add(gene);
                        }

                        //DisplayManager.Clear(); --This is superfluous since changing the DisplayManager property already does this (?)

                        // Remove listeners from the display managaer
                        DisplayManager.PropertyChanged -= DisplayManager_PropertyChanged;

                        // Create a new group display manager
                        if (Constants.UseHammerTouchSupport) {
                            DisplayManager = new HammerGroupRegulonDisplay();
                            (DisplayManager as HammerGroupRegulonDisplay).AutoCalculateCentroidOnDrag = autoCentroid;
                        } else {
                            DisplayManager = new GroupRegulonDisplay();
                            (DisplayManager as GroupRegulonDisplay).AutoCalculateCentroidOnDrag = autoCentroid;
                        }

                        // Restore existing selections
                        // This uses a timeout to ensure that the selection is
                        // restored after the rest of the data set is loaded
                        Script.SetTimeout((Action) delegate {
                            DisplayManager.SelectedRegulons = tempSelectedRegulons;
                            DisplayManager.SelectedGenes = tempSelectedGenes;
                        }, 100, null);

                        // Re-add the listener to the display manager
                        DisplayManager.PropertyChanged += DisplayManager_PropertyChanged;
                    }
                }
            }
#endif
        }

        /// <summary>
        /// Configures and renders the display
        /// </summary>
        protected override void DisplayFullMode()
        {
            // TODO: push these details up to the calling class.
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("RegPreciseRegulonGraphViewer2 - entering DisplayFullMode");
#endif*/

            // Note that DisplayFullMode is active
            processingDisplayFullMode = true;

            // Reset the current filter option
            currentFilterOption = 0;

            // Configuration of colours
            // If GO terms are enabled, use the GO term colour chooser
            if (Constants.UseGoTerms) {

                // Use CSS attributes if they are enabled
                if (Constants.UseCssNodeAttributes) {
                    colourChooser = new GOTermCSSAttributeFactory(selectedRegulog);
                } else {
                    colourChooser = new GOTermAttributeFactory(selectedRegulog);
                }

            // If Wedge mode is enabled, use the wedge colour chooser if CSS
            // attributes are disabled
            } else if ( Constants.UseWedgeWheels ) {

                // Use regular CSS attributes if they are enabled
                if (Constants.UseCssNodeAttributes) {
				    colourChooser = new GeneFunctionCSSAttributeFactory( selectedRegulog );
                // Otherwise use the special wedge colour chooser
                } else {
				    colourChooser = new WedgeAttributeFactory( selectedRegulog );
                }

            // Default case is using the gene function colour factory
            } else {

                // Use CSS attributes if they are enabled
				if (Constants.UseCssNodeAttributes) {
				    colourChooser = new GeneFunctionCSSAttributeFactory( selectedRegulog );
                } else {
				    colourChooser = new GeneFunctionAttributeFactory( selectedRegulog );
                }
            }

            // Configuration of nodes
            // Create wedge nodes if that mode is enabled, otherwise create
            // regular circular nodes
            if ( Constants.UseWedgeWheels ) {
                nodeFactory = new WedgeNodeFactory();
            } else {
                nodeFactory = new CircleNodeFactory();
            }

            // Get the current list of regulons
            List<RegulonInfo> regulons = (List<RegulonInfo>)selectedRegulog.Regulons;

            /*if (scaleChooser != null) {
                for (int i = 0; i < regulons.Length; i++)
                {
                    regulons[i].DoScale = scaleItems[scaleChooser.SelectedIndex];
                }
            }*/

            // Don't do anything else if there are no regulons
            if (Script.IsNullOrUndefined(regulons))
            {
                return;
            }

            // Pass the current ratio of Hamming and Euclidean distance to the
            // display manager. Defaults to 0.5 if the slider does not exist
            double currentRatio = ratio;

            // Take the value from the slider control if it exists
            if (slider_HEDistance != null) {
                 currentRatio = double.Parse(slider_HEDistance.Value)/100;
            }

            // Set the display manager's ratio
            DisplayManager.Ratio = currentRatio;

            // If the ratio has changed, store it and inform the display
            // manager that it has
            if (currentRatio != ratio) {
                ratio = currentRatio;
                DisplayManager.RatioChanged = true;
            }

            //Please let me know if this is a correct way to do things, Lawrence...
            // If the display manager is a group display, display the networks
            // in groups
            if (DisplayManager is GroupRegulonDisplay)
            {
                // Set the display manager to automatically group networks if
                // that option is enabled
                if (currentAutoGroupSetting) {
                    (DisplayManager as GroupRegulonDisplay).DisplayRegulonsWithExtraGroups(regulons, regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser, numberOfGroups, true);
                } else {
                    (DisplayManager as GroupRegulonDisplay).DisplayRegulonsWithExtraGroups(regulons, regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser, numberOfGroups, false);
                }
                (DisplayManager as GroupRegulonDisplay).AutoCalculateCentroidOnDrag = autoCentroid;
            } else if (DisplayManager is HammerGroupRegulonDisplay)
            {
                // Set the display manager to automatically group networks if
                // that option is enabled
                if (currentAutoGroupSetting) {
                    (DisplayManager as HammerGroupRegulonDisplay).DisplayRegulonsWithExtraGroups(regulons, regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser, numberOfGroups, true);
                } else {
                    (DisplayManager as HammerGroupRegulonDisplay).DisplayRegulonsWithExtraGroups(regulons, regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser, numberOfGroups, false);
                }
                (DisplayManager as HammerGroupRegulonDisplay).AutoCalculateCentroidOnDrag = autoCentroid;
            }
            else
            {
                DisplayManager.DisplayRegulons(regulons, regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser);
            }

#if DEBUG
            // If there is an existing order in the data set, order by this now
//#if DEBUG
//            if (Constants.ShowDebugMessages) Console.Log("The first regulon in the list (" + regulons[0].GenomeName + ") has the order " + regulons[0].Order);
//#endif
            if (regulons[0].Order != 0 && regulons[0].Order != -1 && regulons[0].Order != null) {
                DisplayManager.UpdateVisibleRegulons( RegulogInfo_SavedOrder, null );

                //Clear order from regulons
                foreach (RegulonInfo regulon in regulons) {
                    regulon.Order = -1;
                }
                //regulons[0].Order = null;
            } else
#endif
            // If the networks are to be sorted by their distance to the
            // centroid, do this now
            if (currentSortOption == 4) {
                /*if (DisplayManager is HammerGroupFreeRegulonDisplay
                && (DisplayManager as HammerGroupFreeRegulonDisplay).NumberOfGroups > 1) {
                    (DisplayManager as HammerGroupFreeRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }*/
                if (DisplayManager is HammerGroupRegulonDisplay) {
                    (DisplayManager as HammerGroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }
                if (DisplayManager is GroupRegulonDisplay) {
                    (DisplayManager as GroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }
                if (DisplayManager is DefaultRegulonDisplay) {
                    (DisplayManager as DefaultRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }
                if (DisplayManager is HammerRegulonDisplay) {
                    (DisplayManager as HammerRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }

            // Otherwise use the corresponding sort function to the currently
            // selected sort option
            } else {
			    DisplayManager.UpdateVisibleRegulons( sortFunctions[currentSortOption], null );
            }

            // Set the zoom level to that which is currently selected
            DisplayManager.CurrentZoomLevel = currentZoomLevel;

/*#if DEBUG
            StringWriter writer = new StringWriter();

            for (int i = 0; i < DisplayManager.CurrentDistances.Length; i++) {
                writer.WriteLine(DisplayManager.CurrentDistances[i].ToString(), null);
            }

            string doc = writer.ToString();

            // Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html
            {
                string actualFileName = "regulog" + selectedRegulog.RegulogId + "_distances_" + DisplayManager.Ratio + ".txt";

                Blob blob = new Blob(new string[] { doc }, new Dictionary<string, string>(
                    "type", "text/csv"
                ));

                // Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition.
                Func<Blob, string> getBlobUrl = (Func<Blob, string>)Script.Literal("(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL");
                Action<string> revokeBlobUrl = (Action<string>)Script.Literal("(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL");

                string url = getBlobUrl(blob);

                if ((bool)Script.Literal("!! window.navigator.msSaveBlob"))
                {
                    // Internet Explorer...
                    Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
                }
                else
                {
                    // FireFox, Chrome...
                    AnchorElement save = (AnchorElement)Document.CreateElement("a");
                    save.Href = url;
                    save.Target = "_blank";
                    save.Download = actualFileName;
                    save.Style.Display = "none";
                    save.InnerHTML = "Click to save!";

                    // This is a shim for Firefox.
                    string alternateUrl = String.Format("{0}:{1}:{2}", "text/csv", actualFileName, url);
                    Script.Literal("if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }");

                    Document.Body.AppendChild(save);

                    // This appears to not work in Firefox.
                    // MutableEvent evt = Document.CreateEvent( "Event" );
                    // evt.InitEvent( "click", true, true );
                    // save.DispatchEvent( evt );

                    save.Click();

                    // This causes grief in Firefox.
                    // revokeBlobUrl( url );
                }
            }
#endif*/

            // If there are any selected genes, use the selected gene
            // highlighter
            if (DisplayManager.SelectedGenes.Length > 0) {
				colourChooser.Highlighter = SelectedGeneHighlighter;
            } else {
                colourChooser.Highlighter = null;
            }

            // Notify anyone listening for selected genes being changed
            DisplayManager.SelectedGenesChanged += DisplayManager_SelectedGenesChanged;

            if (div_LoadingOverlay != null && div_LoadingOverlay.Style.Visibility == "visible" ) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Attempting to hide loading overlay");
#endif*/
                div_LoadingOverlay.Style.Visibility = "hidden";
            }

            // Renable the button that displays this view if it is disabled
            // Also makes sure the buttons have actually been stored
            if (activatingButton == null || activatingButtonSideBySide == null) {
                CodeBehind.ForallControlsOfType( typeof( ToolButton ), Document.Body, delegate( ToolButton toolButton ) {
                    if (toolButton.ActivatedId == "RegPreciseGeneRegulonViewer") {
                        activatingButton = toolButton;
                    }
                    if (toolButton.ActivatedId == "RegPreciseGeneRegulonViewerSideBySide") {
                        activatingButtonSideBySide = toolButton;
                    }
                } );
            }

            // Renable the buttons now
            if (activatingButton.IsDisabled) {
                activatingButton.IsDisabled = false;
            }
            if (activatingButtonSideBySide.IsDisabled) {
                activatingButtonSideBySide.IsDisabled = false;
            }

            // Note that DisplayFullMode has finished
            processingDisplayFullMode = false;
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("RegPreciseRegulonGraphViewer2 - exiting DisplayFullMode");
#endif*/
        }

        /// <summary>
        /// Listener for when the value of the auto group chooser drop down changes
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void autoGroupChooser_SelectionChanged(object sender, ElementEventArgs eventArgs)
        {
            // Store the current automatic grouping setting
            if (select_AutoGroupChooser.SelectedIndex == 0) {
                currentAutoGroupSetting = true;
            } else {
                currentAutoGroupSetting = false;
            }

            // Do the processing for when the option changes
            AutoGroupSettingChanged();
        }

        /// <summary>
        /// Process the currently selected data set and display it, as well as configuring drop down menus and other interface elements that rely on the loaded dataset
        /// <para>For this display, the legend showing gene functions or GO terms must also be generated</para>
        /// </summary>
        protected override void ProcessRegulons()
        {
            // Perform the base processing (refreshing drop downs for gene
            // functions/GO terms and displaying the netowrks)
            base.ProcessRegulons();

            // If there is a colour chooser loaded
            if (colourChooser != null) {

                // Generate a legend based on whether GO terms are enabled or
                // not
                if (Constants.UseGoTerms) {
                    CreateGeneGOTermLegend(true);
                } else {
                    CreateGeneFunctionLegend(true);
                }

                // Allow the side panel to be toggled on and off
                button_ToggleSidePanel.DomElement.Disabled = false;
            }
        }

        /// <summary>
        /// Enable or disable the button that recalculates groups, and toggle the appearance and label of the auto grouping button, based on whether automatic grouping is enabled
        /// <para>This also used to toggle the original slider for Hamming/Euclidean distance</para>
        /// </summary>
        private void ToggleAutoGroupSettingElements()
        {
            // Enable the recalculate grouping button if automatic grouping is
            // enabled and the number of groups is greater than 1
            if (/*currentAutoGroupSetting && */numberOfGroups > 1)
            {
                /*if (sliderRow != null) {
                    sliderRow.Style.Visibility = "inherit";
                }*/
                if (button_recalculateGroups != null) {
                    button_recalculateGroups.IsEnabled = true;
                }
                if (touchButton_RecalculateGroups != null) {
                    touchButton_RecalculateGroups.IsEnabled = true;
                }
            }

            // Otherwise, disable these buttons
            else
            {
                /*if (sliderRow != null) {
                    sliderRow.Style.Visibility = "hidden";
                }*/
                if (button_recalculateGroups != null) {
                    button_recalculateGroups.IsEnabled = false;
                }
                if (touchButton_RecalculateGroups != null) {
                    touchButton_RecalculateGroups.IsEnabled = false;
                }
            }

            // If automatic grouping is enabled, show that it is on via the
            // button's image and label, otherwise show that it is off
            if (currentAutoGroupSetting) {
                touchButton_ToggleAutoGroup.DomElement.ClassName = "TouchButton AutogroupActive";
                label_AutoGrouping.TextContent = Constants.Text_ButtonAutoGroupingOn;
            } else {
                touchButton_ToggleAutoGroup.DomElement.ClassName = "TouchButton AutogroupInactive";
                label_AutoGrouping.TextContent = Constants.Text_ButtonAutoGroupingOff;
            }
        }

        /// <summary>
        /// When the recalculate button is clicked, use the automatic grouping function to recalculate the groups in the display
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void recalculateButton_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Show the loading overlay if it exists
            if (div_LoadingOverlay != null) {
                div_LoadingOverlay.Style.Visibility = "visible";
            }

            // Use a timeout delegate so the loading overlay can display
            // properly
            Script.SetTimeout((Action) delegate {

                // Get the current list of regulons
                //List<RegulonInfo> regulons = (List<RegulonInfo>)selectedRegulog.Regulons;

                // Tell the display manager to recalculate the current groups
                if (Constants.UseHammerTouchSupport) {
                    (DisplayManager as HammerGroupRegulonDisplay).RecalculateGroups();

                    // Force the display manager to re sort the networks
                    SortLabelOnClick(null);
                } else {
                    (DisplayManager as GroupRegulonDisplay).RecalculateGroups();

                    // Force the display manager to re sort the networks
                    SortLabelOnClick(null);
                }

    /*#if DEBUG
                Window.Alert("Attempting to generate Xin-Yi's csv...");

                if (Constants.Hammer == 1) {

                    List<string> names = new List<string>();

                    foreach (GeneInfo gene in selectedRegulog.TargetGenes)
                    {
                        // TODO: Allow alternate ortholog identification mechanism. One that actually works would be good.

                        // We have to assume that orthologs have the same name. Otherwise, there
                        // appears to be no way to connect them in RegPrecise. However, this is
                        // probably an invalid assumption. nonetheless...

                        string name = gene.Name;

                        if (!names.Contains(name))
                        {
                            names.Add(name);
                        }
                    }

                    Dictionary<string, int> geneList = new Dictionary<string, int>();

                    foreach (string name in names) geneList[name] = geneList.Count;

                    int n = geneList.Keys.Length;

                    List<List<int>> genePresence = new List<List<int>>();

                    for (int i = 0; i < regulons.Length; i++) {
                        genePresence[i] = new List<int>();
                        foreach (KeyValuePair<string, int> geneName in geneList) {
                            genePresence[i][names.IndexOf(geneName.Key)] = 0;
                            foreach (GeneInfo regulonGene in regulons[i].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (regulonGene.Site != null) {
                                        genePresence[i][names.IndexOf(geneName.Key)] = 2;
                                        break;
                                    } else {
                                        genePresence[i][names.IndexOf(geneName.Key)] = 1;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    List<int> currentCentroids = (DisplayManager as HammerGroupRegulonDisplay).CurrentCentroids;
                    Dictionary<int, int> currentAssignments = (DisplayManager as HammerGroupRegulonDisplay).CurrentAssignments;
                    double[][] currentDistances = (DisplayManager as HammerGroupRegulonDisplay).CurrentDistances;

                    List<List<int>> groups = new List<List<int>>();
                    for (int i = 0; i < regulons.Length; i++) {
                        int groupIndex = currentCentroids.IndexOf(currentAssignments[i]);
                        if (groups[groupIndex] == null) {
                            groups[groupIndex] = new List<int>();
                        }
                        if (currentCentroids.IndexOf(i) > -1) {
                            groups[groupIndex].Insert(0, i);
                        } else {
                            groups[groupIndex].Add(i);
                        }
                    }

                    List<string[]> csvOutput = new List<string[]>();
                    csvOutput[0] = new string[names.Length + 1];
                    csvOutput[0][0] = "Genes";
                    csvOutput[1] = new string[names.Length + 1];
                    for (int i = 0; i < names.Length; i++) {
                        //Console.Log("Adding '" + names[i] + "'...");
                        csvOutput[1][i+1] = names[i];
                    }

                    int currentLine = 2;
                    foreach (List<int> group in groups) {
                        csvOutput[currentLine] = new string[names.Length + 1];
                        //Console.Log("Adding '" + "Group " + groups.IndexOf(group) + "'...");
                        csvOutput[currentLine][0] = "Group " + (groups.IndexOf(group) + 1);
                        currentLine++;
                        foreach (int regulon in group) {
                            csvOutput[currentLine] = new string[names.Length + 1];
                            //Console.Log("Adding '" + regulons[regulon].GenomeName + "'...");
                            csvOutput[currentLine][0] = regulons[regulon].GenomeName;
                            if (group.IndexOf(regulon) == 0) {
                                csvOutput[currentLine][0] = csvOutput[currentLine][0] + " (centroid)";
                            }
                            for (int i = 0; i < names.Length; i++) {
                                csvOutput[currentLine][i+1] = genePresence[regulon][i].ToString();
                            }
                            currentLine++;
                        }
                    }

                    csvOutput[currentLine] = new string[names.Length + 1];
                    currentLine++;
                    csvOutput[currentLine] = new string[names.Length + 1];
                    //Console.Log("Adding '" + "Distances to centroid" + "'...");
                    csvOutput[currentLine][0] = "Distances to centroid";
                    currentLine++;

                    foreach (List<int> group in groups) {
                        csvOutput[currentLine] = new string[names.Length + 1];
                        //Console.Log("Adding '" + "Group " + groups.IndexOf(group) + "'...");
                        csvOutput[currentLine][0] = "Group " + (groups.IndexOf(group) + 1);
                        currentLine++;
                        foreach (int regulon in group) {
                            csvOutput[currentLine] = new string[names.Length + 1];
                            //Console.Log("Adding '" + regulons[regulon].GenomeName + "'...");
                            csvOutput[currentLine][0] = regulons[regulon].GenomeName;
                            if (group.IndexOf(regulon) == 0) {
                                csvOutput[currentLine][0] = csvOutput[currentLine][0] + " (centroid)";
                                //Console.Log("Adding '" + "0" + "'...");
                                csvOutput[currentLine][1] = "0";
                            } else {
                                //Console.Log("Adding '" + currentDistances[regulon][group[0]].ToString() + "'...");
                                Script.Literal("csvOutput[currentLine][1] = String(currentDistances[regulon][group[0]])");
                            }
                            currentLine++;
                        }
                    }

			        Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );

				    CsvIO csvIO = new CsvIO( ',', '"' );

                    StringWriter writer = new StringWriter();

                    csvIO.Write( csvOutput, writer );

				    string doc = writer.ToString();

				    // Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html
				    {
					    string fileName = null;

					    Blob blob = new Blob( new string[] { doc }, new Dictionary<string, string>(
						    "type", "text/csv"
					    ) );

					    // Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition.
					    Func<Blob, string> getBlobUrl = (Func<Blob, string>) Script.Literal( "(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL" );
					    Action<string> revokeBlobUrl = (Action<string>) Script.Literal( "(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL" );

					    string url = getBlobUrl( blob );
					    string actualFileName = fileName ?? "Untitled.csv";

					    if ( (bool) Script.Literal( "!! window.navigator.msSaveBlob" ) ) {
						    // Internet Explorer...
						    Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
					    }
					    else {
						    // FireFox, Chrome...
						    AnchorElement save = (AnchorElement) Document.CreateElement( "a" );
						    save.Href = url;
						    save.Target = "_blank";
						    save.Download = actualFileName;
						    save.Style.Display = "none";
						    save.InnerHTML = "Click to save!";

						    // This is a shim for Firefox.
						    string alternateUrl = String.Format( "{0}:{1}:{2}", "text/csv", actualFileName, url );
						    Script.Literal( "if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }" );

						    contentHolder.AppendChild( save );

						    save.Click();
					    }
				    }

                    Document.Body.RemoveChild(contentHolder);
                }
    #endif*/

                // Hide the loading overlay
                if (div_LoadingOverlay != null) {
                    div_LoadingOverlay.Style.Visibility = "hidden";
                }
            }, 10, null);
        }

        /// <summary>
        /// Listens for when the auto grouping touch button is clicked
        /// <para>Changes the automatic grouping setting and then processes required changes</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_ToggleAutoGroup_clicked(object sender, ElementEventArgs eventArgs)
        {
            // Store the current automatic grouping setting
            currentAutoGroupSetting = !currentAutoGroupSetting;

            // If the old drop down exists, set it to the current automatic
            // grouping setting
            if (select_AutoGroupChooser != null) {
                if (currentAutoGroupSetting) {
                    select_AutoGroupChooser.SelectedIndex = 0;
                } else {
                    select_AutoGroupChooser.SelectedIndex = 1;
                }
            }

            // Do the processing for when the option changes
            AutoGroupSettingChanged();
        }

        // Variable to store the automatic centroid setting's current value
        // (true = on, false = off)
        private bool autoCentroid = true;

        /// <summary>
        /// Listens for when the auto centroid touch button is clicked
        /// <para>Toggles auto centroid calculation on or off and changes the attributes of the button to reflect this</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_ToggleAutoCentroid_clicked(object sender, ElementEventArgs eventArgs) {

            // Toggle the automatic centroid status
            autoCentroid = !autoCentroid;

            // Show whether it is enabled or not through the button's
            // appearance and label
            if (autoCentroid) {
                touchButton_ToggleAutoCentroid.DomElement.ClassName = "TouchButton AutoCentroidActive";
                label_AutoCentroid.TextContent = Constants.Text_ButtonAutoCentroidOn;
            } else {
                touchButton_ToggleAutoCentroid.DomElement.ClassName = "TouchButton AutoCentroidInactive";
                label_AutoCentroid.TextContent = Constants.Text_ButtonAutoCentroidOff;
            }

            // Change the status of automatic centroid calculation in the
            // display manager
            if (DisplayManager is GroupRegulonDisplay) {
                (DisplayManager as GroupRegulonDisplay).AutoCalculateCentroidOnDrag = autoCentroid;
            }
            if (DisplayManager is HammerGroupRegulonDisplay) {
                (DisplayManager as HammerGroupRegulonDisplay).AutoCalculateCentroidOnDrag = autoCentroid;
            }
        }

        // Used to check if the user clicked on the number of groups touch
        // button
        bool touchButton_NumberOfGroups_justClicked = false;

        /// <summary>
        /// Listener for if the number of groups touch button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_NumberOfGroups_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Make the drop down visible
            touchButton_NumberOfGroupsPopup.Style.Visibility = "visible";

            // Set the button as just being clicked
            touchButton_NumberOfGroups_justClicked = true;
        }

        /// <summary>
        /// Does things when the user clicks somewhere on the body of the page
        /// <para>Handles elements that are unique to this viewer</para>
        /// </summary>
        /// <param name="e">The element event that was sent by the body</param>
        private void SubBodyOnClick(ElementEvent e)
        {
            // Get the location of the cursor
            int mouseX = (int)Script.Literal("Math.round(e.clientX)");
            int mouseY = (int)Script.Literal("Math.round(e.clientY)");

            // If the number of groups touch button has just been clicked, set
            // that status to false
            if ( touchButton_NumberOfGroups_justClicked ) {
                touchButton_NumberOfGroups_justClicked = false;

            // Otherwise if the drop down exists, and the mouse is outside of
            // the drop down, hide it
            } else {
                if ( touchButton_NumberOfGroupsPopup != null) {
                    if (mouseX < HtmlUtil.GetElementX(touchButton_NumberOfGroupsPopup) || mouseX > HtmlUtil.GetElementX(touchButton_NumberOfGroupsPopup) + touchButton_NumberOfGroupsPopup.ClientWidth
                        || mouseY < HtmlUtil.GetElementY(touchButton_NumberOfGroupsPopup) || mouseY > HtmlUtil.GetElementY(touchButton_NumberOfGroupsPopup) + touchButton_NumberOfGroupsPopup.ClientHeight ) {
                        touchButton_NumberOfGroupsPopup.Style.Visibility = "hidden";
                    }
                }
            }

            // If the sort networks touch button has just been clicked, set
            // that status to false
            if ( touchButton_Sort_justClicked ) {
                touchButton_Sort_justClicked = false;

            // Otherwise if the drop down exists, and the mouse is outside of
            // the drop down, hide it
            } else {
                if ( touchButton_SortPopup != null) {
                    if (mouseX < HtmlUtil.GetElementX(touchButton_SortPopup) || mouseX > HtmlUtil.GetElementX(touchButton_SortPopup) + touchButton_SortPopup.ClientWidth
                        || mouseY < HtmlUtil.GetElementY(touchButton_SortPopup) || mouseY > HtmlUtil.GetElementY(touchButton_SortPopup) + touchButton_SortPopup.ClientHeight ) {
                        touchButton_SortPopup.Style.Visibility = "hidden";
                    }
                }
            }

#if DEBUG
            // If changing the Hamming/Euclidean ratio is enabled...
            if (Constants.UseVariableHEDistance) {

                // If the button for changing Hamming/Euclidean ratio has just been
                // clicked, set that status to false
                if ( touchButton_Slider_justClicked ) {
                    touchButton_Slider_justClicked = false;

                // Otherwise if the drop down exists, and the mouse is outside of
                // the drop down, hide it
                } else {
                    if ( touchButton_SliderPopup != null) {
                        if (mouseX < HtmlUtil.GetElementX(touchButton_SliderPopup) || mouseX > HtmlUtil.GetElementX(touchButton_SliderPopup) + touchButton_SliderPopup.ClientWidth
                            || mouseY < HtmlUtil.GetElementY(touchButton_SliderPopup) || mouseY > HtmlUtil.GetElementY(touchButton_SliderPopup) + touchButton_SliderPopup.ClientHeight ) {
                            touchButton_SliderPopup.Style.Visibility = "hidden";
                        }
                    }
                }
            }
#endif
        }

        /// <summary>
        /// Listens for when one of the labels in the number of groups touch button drop down is clicked
        /// <para>Changes the display manager's number of groups to the selected value</para>
        /// </summary>
        /// <param name="e">The element event that was sent by the label</param>
        private void GroupLabelOnClick (ElementEvent e) {

            // Don't do anything if the number of groups selected is greater
            // than the number of networks in the data set
            if (int.Parse(e.Target.TextContent) > DisplayManager.NumberOfGraphs) {
                Window.Alert(Constants.Text_MessageTooManyGroups);
                return;
            }

            // Set the number of groups to the value that was selected
            numberOfGroups = int.Parse(e.Target.TextContent);

            // If there is a data set loaded, change the number of groups
            if (selectedRegulog != null) {

                // Show the loading overlay if it exists
                if (div_LoadingOverlay != null) {
                    div_LoadingOverlay.Style.Visibility = "visible";
                }

                // Use a timeout delegate so that the loading overlay displays
                // properly
                Script.SetTimeout((Action) delegate {

                    // Set the display manager's number of groups
                    SetNumberOfGroups();

                    // Hide the loading overlay
                    if (div_LoadingOverlay != null) {
                        div_LoadingOverlay.Style.Visibility = "hidden";
                    }
                }, 10, null);
            }

            // Hide the drop down for the number of groups touch button
            if ( touchButton_NumberOfGroupsPopup != null ) {
                touchButton_NumberOfGroupsPopup.Style.Visibility = "hidden";
            }

            // If the original groups chooser drop down exists, set its
            // selected item to the current number of groups
            if ( select_GroupsChooser != null ) {
                select_GroupsChooser.SelectedIndex = groupsItems.IndexOf(e.Target.TextContent);
            }
        }

        // Used to check if the user clicked on the sort touch button
        bool touchButton_Sort_justClicked = false;

        /// <summary>
        /// Listener for if the sort touch button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_Sort_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Make the drop down visible
            touchButton_SortPopup.Style.Visibility = "visible";

            // Set the button as just being clicked
            touchButton_Sort_justClicked = true;
        }

        /// <summary>
        /// Listens for when one of the labels in the sort touch button drop down is clicked
        /// <para>Tells the display manager to sort the networks by the selected method</para>
        /// </summary>
        /// <param name="e">The element event that was sent by the label</param>
        private void SortLabelOnClick(ElementEvent e)
        {
            // Check if a filtered detail view is being used. If so, the
            // update requests sent to the DisplayManager will be forced to use
            // the selected network filter
            bool filteredDetailView = false;
            if (((DisplayManager is HammerGroupRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is GroupRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as GroupRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is DefaultRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as DefaultRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is HammerRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as HammerRegulonDisplay).DetailedViewGraphs.Length > 0))
            {
                filteredDetailView = true;
            }

            // If this was triggered by an actual event and not forced by
            // another function, set the internal sort option
            if (e != null) {
                currentSortOption = sortItems.IndexOf(e.Target.TextContent);
            }

            // Sorting by centroid distance requires special handling as it
            // uses a specific function in the display manager
            if (currentSortOption == 4) {
                /*if (DisplayManager is HammerGroupFreeRegulonDisplay
                && (DisplayManager as HammerGroupFreeRegulonDisplay).NumberOfGroups > 1) {
                    (DisplayManager as HammerGroupFreeRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }*/
                if (DisplayManager is HammerGroupRegulonDisplay) {
                    // If it's a filtered detail view, force filter by selected
                    // networks
                    if (filteredDetailView) {
                        (DisplayManager as HammerGroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(RegulogInfo_FilterBySelection);
                    // Otherwise use the currently selected filter
                    } else {
                        (DisplayManager as HammerGroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(filterFunctions[currentFilterOption]);
                    }
                }
                if (DisplayManager is GroupRegulonDisplay) {
                    // If it's a filtered detail view, force filter by selected
                    // networks
                    if (filteredDetailView) {
                        (DisplayManager as GroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(RegulogInfo_FilterBySelection);
                    // Otherwise use the currently selected filter
                    } else {
                        (DisplayManager as GroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(filterFunctions[currentFilterOption]);
                    }
                }
                if (DisplayManager is DefaultRegulonDisplay) {
                    // If it's a filtered detail view, force filter by selected
                    // networks
                    if (filteredDetailView) {
                        (DisplayManager as DefaultRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(RegulogInfo_FilterBySelection);
                    // Otherwise use the currently selected filter
                    } else {
                        (DisplayManager as DefaultRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(filterFunctions[currentFilterOption]);
                    }
                }
                if (DisplayManager is HammerRegulonDisplay) {
                    // If it's a filtered detail view, force filter by selected
                    // networks
                    if (filteredDetailView) {
                        (DisplayManager as HammerRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(RegulogInfo_FilterBySelection);
                    // Otherwise use the currently selected filter
                    } else {
                        (DisplayManager as HammerRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(filterFunctions[currentFilterOption]);
                    }
                }

            // Otherwise send the corresponding function in the list of sort
            // functions to the display manager
            } else {
                // If it's a filtered detail view, force filter by selected
                // networks
                if (filteredDetailView) {
			        DisplayManager.UpdateVisibleRegulons( sortFunctions[currentSortOption], RegulogInfo_FilterBySelection );
                // Otherwise use the currently selected filter
                } else {
			        DisplayManager.UpdateVisibleRegulons( sortFunctions[currentSortOption], filterFunctions[currentFilterOption] );
                }
            }

            // Hide the drop down for the sort touch button
            if ( touchButton_SortPopup != null ) {
                touchButton_SortPopup.Style.Visibility = "hidden";
            }

            // If the original sort drop down exists, set its selected item to
            // the current sort option
            if ( select_SortChooser != null ) {
                select_SortChooser.SelectedIndex = currentSortOption;
            }
        }

#if DEBUG
        // Used to check if the user clicked on the Hamming/Euclidean ratio
        // touch button
        bool touchButton_Slider_justClicked = false;

        /// <summary>
        /// Listener for if the Hamming/Euclidean ratio touch button is clicked
        /// <para>Used only if the variableHEDistanceEnabled constant is enabled</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_Slider_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Make the drop down visible
            touchButton_SliderPopup.Style.Visibility = "visible";

            // Set the button as just being clicked
            touchButton_Slider_justClicked = true;
        }
#endif

        /// <summary>
        /// Allows the group number text box to set the number of groups on pressing enter
        /// <para>Used only if the noGroupLimit constant is enabled</para>
        /// </summary>
        /// <param name="e">The element event that was sent by the text box</param>
        private void group_NumberInputText_KeyUp(ElementEvent e)
        {
            // If the enter key was pressed...
            if ((bool)Script.Literal("e.key == 'Enter'")) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("User pressed enter in the number of groups text box");
#endif*/
                // Force the function that activates when the associated button is clicked
                group_NumberInputButton_Clicked(textBox_NumberOfGroups, null);
            }
        }

        /// <summary>
        /// Listens for when the button in the number of groups touch down is clicked
        /// <para>Sets the number of groups to the value inputted into the associated text box</para>
        /// <para>Used only if the noGroupLimit constant is enabled</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void group_NumberInputButton_Clicked(object sender, ElementEventArgs eventArgs)
        {

            // If there is text in the text box
            if (textBox_NumberOfGroups.Text.Length > 0) {

                //As the input is number type, it should always be a number
                int number = int.Parse(textBox_NumberOfGroups.Text);

                // If it is less than one, treat it as being 1
                if (number < 1) {
                    number = 1;
                }

                // Don't do anything if the number of groups selected is greater
                // than the number of networks in the data set
                if (number > DisplayManager.NumberOfGraphs) {
                    Window.Alert(Constants.Text_MessageTooManyGroups);
                    return;
                }

                // If the inputted number is different to the current number
                // of groups...
                if (number != numberOfGroups) {

                    // Set the current number of groups to the inputted number
                    numberOfGroups = number;

                    // If there is a data set loaded, change the number of groups
                    if (selectedRegulog != null) {

                        // Show the loading overlay if it exists
                        if (div_LoadingOverlay != null) {
                            div_LoadingOverlay.Style.Visibility = "visible";
                        }

                        // Use a timeout delegate so that the loading overlay displays
                        // properly
                        Script.SetTimeout((Action) delegate {

                            // Set the display manager's number of groups
                            SetNumberOfGroups();

                            // Hide the loading overlay
                            if (div_LoadingOverlay != null) {
                                div_LoadingOverlay.Style.Visibility = "hidden";
                            }
                        }, 10, null);
                    }

                    // Hide the drop down for the number of groups touch button
                    if ( touchButton_NumberOfGroupsPopup != null ) {
                        touchButton_NumberOfGroupsPopup.Style.Visibility = "hidden";
                    }
                }
            }
        }

        /// <summary>
        /// Performs actions when the automatic grouping setting is changed
        /// </summary>
        private void AutoGroupSettingChanged()
        {
            // Change interface elements in response to the new group setting
            ToggleAutoGroupSettingElements();

            // Do not do anything if there is no selected data set
            if (selectedRegulog != null)
            {

                // If there is more than one group and the setting is currently
                // on, recalculate the groups
                if (currentAutoGroupSetting && numberOfGroups > 1)
                {
                    // Display the loading overlay if  it exists
                    if (div_LoadingOverlay != null)
                    {
                        div_LoadingOverlay.Style.Visibility = "visible";
                    }

                    // Use a timeout delegate so the loading overlay is displayed
                    Script.SetTimeout((Action)delegate
                    {
                        // Recalculate the groups in the display manager
                        // Handling for Hammer and non-Hammer display managers
                        if (Constants.UseHammerTouchSupport)
                        {
                            (DisplayManager as HammerGroupRegulonDisplay).RecalculateGroups();

                            // Sort the networks
                            SortLabelOnClick(null);
                        }
                        else
                        {
                            (DisplayManager as GroupRegulonDisplay).RecalculateGroups();

                            // Sort the networks
                            SortLabelOnClick(null);
                        }

                        // Hide the loading overlay
                        if (div_LoadingOverlay != null)
                        {
                            div_LoadingOverlay.Style.Visibility = "hidden";
                        }
                    }, 10, null);
                }
            }
        }

#if DEBUG
        /// <summary>
        /// Listens for when the select centroids button is clicked
        /// <para>Takes the current selection and tells the display to create a grouping using all of the selected networks as centroids</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_SelectCentroids_Clicked(object sender, ElementEventArgs eventArgs) {

            // Don't do anything if no networks are selected
            if (DisplayManager.SelectedRegulons.Length < 1) return;

            // Get the selected networks from the display
            List<RegulonInfo> tempSelectedRegulons = DisplayManager.SelectedRegulons;

            // Get the number of new centroids
            int numberOfNewCentroids = tempSelectedRegulons.Length;

#if DEBUG
            // If group type displays are always used, the same method can be
            // used at all times
            if (Constants.UseGroupDisplaysGlobally) {
#endif
                // Initialise a new list of centroids
                List<int> newCentroids = new List<int>();

                // Go through all of the regulons in the current data set and
                // add their index to the list of centroids if they are
                // selected
                for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                    if (tempSelectedRegulons.Contains(selectedRegulog.Regulons[i])) {
                        newCentroids.Add(i);
                    }
                }

                // Send the new centroids to the display
                if (DisplayManager is HammerGroupRegulonDisplay) {
                    (DisplayManager as HammerGroupRegulonDisplay).CurrentCentroids = newCentroids;
                } else {
                    (DisplayManager as GroupRegulonDisplay).CurrentCentroids = newCentroids;
                }

                // Store the new number of groups
                numberOfGroups = numberOfNewCentroids;

                // Set the data set's number of groups
                //SelectedRegulog.NumGroups = numberOfGroups;
#if DEBUG
            }
            // Otherwise, what needs to be done depends on how many groups
            // exist and how many will be present after the centroids are
            // changed
            else
            {
                // If there were more than one centroid before and there will
                // be more than one afterwards, proceed as before
                if (numberOfNewCentroids > 1 && numberOfGroups > 1) {
                    // Initialise a new list of centroids
                    List<int> newCentroids = new List<int>();

                    // Go through all of the regulons in the current data set and
                    // add their index to the list of centroids if they are
                    // selected
                    for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                        if (tempSelectedRegulons.Contains(selectedRegulog.Regulons[i])) {
                            newCentroids.Add(i);
                        }
                    }

                    // Send the new centroids to the display
                    if (DisplayManager is HammerGroupRegulonDisplay) {
                        (DisplayManager as HammerGroupRegulonDisplay).CurrentCentroids = newCentroids;
                    } else {
                        (DisplayManager as GroupRegulonDisplay).CurrentCentroids = newCentroids;
                    }

                    // Store the new number of groups
                    numberOfGroups = numberOfNewCentroids;

                    // Set the data set's number of groups
                    //SelectedRegulog.NumGroups = numberOfGroups;

                // If there was one centroid before and there will be one
                // afterwards, change the centroid
                } else if (numberOfNewCentroids == 1 && numberOfGroups == 1) {
                    // Initialise a new centroid
                    int newCentroid = 0;

                    // Go through all of the regulons in the current data set
                    // to find the index of the new centroid
                    for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                        if (tempSelectedRegulons.Contains(selectedRegulog.Regulons[i])) {
                            newCentroid = i;
                            break;
                        }
                    }

                    // Send the new centroid to the display
                    if (DisplayManager is HammerRegulonDisplay) {
                        (DisplayManager as HammerRegulonDisplay).CurrentCentroid = newCentroid;
                    } else {
                        (DisplayManager as DefaultRegulonDisplay).CurrentCentroid = newCentroid;
                    }

                // In other cases, the type of display must be changed
                } else {
                    // If moving from one to many groups...
                    if (numberOfGroups == 1) {
                        // Initialise a new list of centroids
                        List<int> newCentroids = new List<int>();

                        // Go through all of the regulons in the current data set and
                        // add their index to the list of centroids if they are
                        // selected
                        for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                            if (tempSelectedRegulons.Contains(selectedRegulog.Regulons[i])) {
                                newCentroids.Add(i);
                            }
                        }

                        // Store the selected regulons and genes for restoration
                        // later
                        // We've already got the selected regulons from before
                        List<GeneInfo> tempSelectedGenes = new List<GeneInfo>();
                        foreach (GeneInfo gene in DisplayManager.SelectedGenes) {
                            tempSelectedGenes.Add(gene);
                        }

                        //DisplayManager.Clear(); --This is superfluous since changing the DisplayManager property already does this (?)

                        // Remove listeners from the display managaer
                        DisplayManager.PropertyChanged -= DisplayManager_PropertyChanged;

                        // Create a new group display manager
                        if (Constants.UseHammerTouchSupport) {
                            DisplayManager = new HammerGroupRegulonDisplay();
                            (DisplayManager as HammerGroupRegulonDisplay).AutoCalculateCentroidOnDrag = autoCentroid;
                        } else {
                            DisplayManager = new GroupRegulonDisplay();
                            (DisplayManager as GroupRegulonDisplay).AutoCalculateCentroidOnDrag = autoCentroid;
                        }

                        // Restore existing selections
                        // This uses a timeout to ensure that the selection is
                        // restored after the rest of the data set is loaded
                        Script.SetTimeout((Action) delegate {
                            DisplayManager.SelectedRegulons = tempSelectedRegulons;
                            DisplayManager.SelectedGenes = tempSelectedGenes;
                        }, 100, null);

                        // Re-add the listener to the display manager
                        DisplayManager.PropertyChanged += DisplayManager_PropertyChanged;

                        // Store the new number of groups
                        numberOfGroups = numberOfNewCentroids;

                        // Set the data set's number of groups
                        //SelectedRegulog.NumGroups = numberOfGroups;

                        // Send the new centroids to the display
                        if (DisplayManager is HammerGroupRegulonDisplay) {
                            (DisplayManager as HammerGroupRegulonDisplay).CurrentCentroids = newCentroids;
                        } else {
                            (DisplayManager as GroupRegulonDisplay).CurrentCentroids = newCentroids;
                        }
                    }
                    // Otherwise, if moving from many to one group...
                    else {
                        // Initialise a new centroid
                        int newCentroid = 0;

                        // Go through all of the regulons in the current data set
                        // to find the index of the new centroid
                        for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                            if (tempSelectedRegulons.Contains(selectedRegulog.Regulons[i])) {
                                newCentroid = i;
                                break;
                            }
                        }

                        // If the display manager is a group display manager, change it
                        // back to a regular display manager
                        if (DisplayManager is GroupRegulonDisplay || DisplayManager is HammerGroupRegulonDisplay)
                        {
                            // Store the selected regulons and genes for restoration
                            // later
                            // We've already got the selected regulons from
                            // before
                            List<GeneInfo> tempSelectedGenes = new List<GeneInfo>();
                            foreach (GeneInfo gene in DisplayManager.SelectedGenes) {
                                tempSelectedGenes.Add(gene);
                            }

                            //DisplayManager.Clear(); //This is superfluous since changing the DisplayManager property already does this (?)

                            // Remove listeners from the display managaer
                            DisplayManager.PropertyChanged -= DisplayManager_PropertyChanged;

                            // Create a new regular display manager
                            if (Constants.UseHammerTouchSupport) {
                                DisplayManager = new HammerRegulonDisplay();
                            } else {
                                DisplayManager = new DefaultRegulonDisplay();
                            }

                            // Restore existing selections
                            // This uses a timeout to ensure that the selection is
                            // restored after the rest of the data set is loaded
                            Script.SetTimeout((Action) delegate {
                                DisplayManager.SelectedRegulons = tempSelectedRegulons;
                                DisplayManager.SelectedGenes = tempSelectedGenes;
                            }, 100, null);

                            // Re-add the listener to the display manager
                            DisplayManager.PropertyChanged += DisplayManager_PropertyChanged;
                        }

                        // Store the new number of groups
                        numberOfGroups = numberOfNewCentroids;

                        // Set the number of groups and each regulon's group number to
                        // nothing in the underlying data
                        /*SelectedRegulog.NumGroups = null;
                        foreach (RegulonInfo regulon in SelectedRegulog.Regulons) {
                            regulon.GroupNumber = null;
                        }*/

                        // Send the new centroid to the display
                        if (DisplayManager is HammerRegulonDisplay) {
                            (DisplayManager as HammerRegulonDisplay).CurrentCentroid = newCentroid;
                        } else {
                            (DisplayManager as DefaultRegulonDisplay).CurrentCentroid = newCentroid;
                        }
                    }
                }
            }
#endif
        }
#endif
#if DEBUG
        /// <summary>
        /// Listens for when a regulog is loaded in the DataLayer and informs the DisplayManager to use the bug handler
        /// <para>At present, homologue selection does not work properly when a new regulog is loaded from RegPrecise.
        /// This is a hacky way of attempting to make sure that it does</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="args">The parameters of the event</param>
        private void DataLayer_RegulogLoaded(object sender, RegulogLoadedEventArgs args)
        {
            if (args.DownloadRequired) {
                if (DisplayManager is HammerRegulonDisplay) {
                    (DisplayManager as HammerRegulonDisplay).RequireSelectionBugHandler = true;
                }

                if (DisplayManager is HammerGroupRegulonDisplay) {
                    (DisplayManager as HammerGroupRegulonDisplay).RequireSelectionBugHandler = true;
                }
            } else {
                if (DisplayManager is HammerRegulonDisplay) {
                    (DisplayManager as HammerRegulonDisplay).RequireSelectionBugHandler = false;
                }

                if (DisplayManager is HammerGroupRegulonDisplay) {
                    (DisplayManager as HammerGroupRegulonDisplay).RequireSelectionBugHandler = false;
                }
            }
        }
#endif

        /// <summary>
        /// Listens for when one of the labels in the filter touch button drop down is clicked
        /// <para>Tells the display manager to filter the networks by the selected method</para>
        /// </summary>
        /// <param name="e">The element event that was sent by the label</param>
        private void FilterLabelOnClick(ElementEvent e)
        {
            // Don't do anything if there is a filtered detail view already
            if (((DisplayManager is HammerGroupRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is GroupRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as GroupRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is DefaultRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as DefaultRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is HammerRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as HammerRegulonDisplay).DetailedViewGraphs.Length > 0))
            {
                // Hide the drop down for the sort touch button
                if ( touchButton_SortPopup != null ) {
                    touchButton_SortPopup.Style.Visibility = "hidden";
                }
                return;
            }

            // Store the current filter option
            currentFilterOption = filterItems.IndexOf(e.Target.TextContent);

            // Send the corresponding function in the list of filter
            // functions to the display manager
		    DisplayManager.UpdateVisibleRegulons( null, filterFunctions[currentFilterOption] );

            // Hide the drop down for the sort touch button
            if ( touchButton_SortPopup != null ) {
                touchButton_SortPopup.Style.Visibility = "hidden";
            }
        }
	}
}
