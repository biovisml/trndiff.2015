﻿using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.Common;
using SystemQut;
using SystemQut.Svg;
using Hammer;

namespace RegulonExplorer.View.RegPreciseView
{
    class HammerCompareRegulonDisplay : HammerRegulonDisplay
    {

        public HammerCompareRegulonDisplay() {
			// Inserted to allow superclass constructor to be called.
		}

		/// <summary> Displays the list of regulons in the specified container with a default of logical operation OR
		/// <para>Included because it is a required method in the interface, though it only calls the "DisplayRegulonsWithOperation" function with a dummy operation of OR</para>
		/// </summary>
		/// <param name="regulons">The list of regulons</param>
		/// <param name="container">The DOM object to act as the container</param>
		/// <param name="selectedRegulog">The currently selected regulog</param>
		/// <param name="nodeFactory">The node factory to use to create the nodes</param>
		/// <param name="colourChooser">The attribute factory to use to set the node attributes (mainly colour)</param>

		override public void DisplayRegulons(
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser
		) {
            DisplayRegulonsWithOperation(regulons, container, selectedRegulog, nodeFactory, colourChooser, LogicalOperations.OR);
		}

		/// <summary> Displays the list of regulons in the specified container, compared using the specified logical operation
		/// </summary>
		/// <param name="regulons">The list of regulons</param>
		/// <param name="container">The DOM object to act as the container</param>
		/// <param name="selectedRegulog">The currently selected regulog</param>
		/// <param name="nodeFactory">The node factory to use to create the nodes</param>
		/// <param name="colourChooser">The attribute factory to use to set the node attributes (mainly colour)</param>
		/// <param name="operation">The logical operation to display the networks with</param>

		public void DisplayRegulonsWithOperation(
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser,
			LogicalOperations operation
		) {
            // Store the current regulog
            this.selectedRegulog = selectedRegulog;

            // Store the current container
			this.container = container;

            // Clear any existing graphs
            if (regulonGraphs.Length > 0) {
                Clear();
            }

            // If there is already a Hammer manager for this container, destroy
            // it
            if (containerHammer != null) {
                containerHammer.Destroy();
            }

            // Create a new Hammer manager for this container
            containerHammer = new Hammer.Hammer(container.ParentNode);

            // Sets the characteristics of the container's manager
            // let the pan gesture support all directions.
            // this will block the vertical scrolling on a touch-device while on the element
            JSObject managerOptions = new JSObject();
            managerOptions["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
            managerOptions["domEvents"] = true; //http://hammerjs.github.io/tips/
            containerHammer.Set(managerOptions);

            // Add a tap listener for the container's manager
            containerHammer.On( "tap" , Container_OnTap );

            // Set the attributes of the container
            this.container.Style.Height = "auto";
            this.container.Style.Width = "auto";

			//remove all the children this regulon display has
			while ( container.ChildNodes[0] != null ) {
				container.RemoveChild( container.ChildNodes[0] );
			}

            // Generate the list of special networks that will display the
            // logical comparison
			List<RegulonInfo> comparedRegulons = GenerateComparisonRegulonList( regulons, operation );

            // Initialise the matrix of regulons and their gene locations
            geneIndexes = new int[regulons.Count][];
            for (int i = 0; i < regulons.Count; i++) {
                geneIndexes[i] = new int[selectedRegulog.Genes.Length];
                for (int j = 0; j < geneIndexes[i].Length; j++) {
                    geneIndexes[i][j] = -1;
                }
            }

            // Store the index of each gene in the matrix above
            /*foreach (GeneInfo gene in selectedRegulog.Genes) {
                geneDictionary[gene.Name] = selectedRegulog.Genes.IndexOf(gene);
            }*/

            // Create a list for all the TG names
            List<string> geneNames = new List<string>();

            // Look through all the genes in the *regulog*. This is so
            // homologues can be handled correctly
            foreach (GeneInfo gene in selectedRegulog.TargetGenes)
            {
                // Add the TG's name to the list of names, if it is not already
                // in there
                string name = gene.Name;

                if (!geneNames.Contains(name))
                {
                    geneNames.Add(name);
                }
            }

            // For each name, add it in the dictionary, using the length of the
            // dictionary as its index
            geneDictionary.Clear();
            foreach (string name in geneNames) geneDictionary[name] = geneDictionary.Count;

            // Create and configure a graph for each regulon that will be
            // displayed
			foreach ( RegulonInfo regulon in comparedRegulons ) {
				RegulonGraph graph = new RegulonGraph(
					selectedRegulog,
					regulon,
					nodeFactory,
					colourChooser,
					Constants.RegulonDisplayWidth,
					Constants.RegulonDisplayHeight,
					Constants.RegulonDisplayUnits
				);

				// If there are more than two regulons, mark the first regulon
				// as the "reference"
				if ( comparedRegulons.Length > 2 && comparedRegulons.IndexOf( regulon ) == 0 ) {
					graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelReference;
				}

/* #if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Checking genes for " + regulon.GenomeName);
                foreach (GeneInfo gene in regulon.TargetGenes) {
                    if (gene is GeneCompareInfo) {
                        if (Constants.ShowDebugMessages) Console.Log(gene.Name + " in " + regulon.GenomeName + " has GenePresentIn " + (gene as GeneCompareInfo).GenePresentIn + " and RegulationPresentIn " + (gene as GeneCompareInfo).RegulationPresentIn);
                    }
                }
#endif */

                // Set the graph's attributes
                SetGraphClass(graph, false);

                // Add the graph to the container as well as the list of graphs
				graph.AddElementTo( container );
				regulonGraphs.Add( graph );

                // Set up a Hammer manager for this graph
				HammerListeners[graph.DomElement.ID] = new Hammer.Hammer( graph.DomElement );

                // Sets the characteristics of the graph's manager
                // let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                HammerListeners[graph.DomElement.ID].Set(managerOptions);

                // Add all the listeners to the graph's manager
                HammerListeners[graph.DomElement.ID].On( "tap", Graph_OnTap );
                HammerListeners[graph.DomElement.ID].On( "pinch", Graph_OnPinch );

                // Set whether tooltips appear on a tap or a press
                if (Constants.ShowTooltipsOnHammerTap) {
                    HammerListeners[graph.DomElement.ID].On( "tap", GraphTooltipHandler );
                } else {
                    HammerListeners[graph.DomElement.ID].On( "press", GraphTooltipHandler );
                }

                // Set whether TGs are selected on a tap or a press
                if (Constants.SelectGenesOnHammerTap) {
                    HammerListeners[graph.DomElement.ID].On( "tap", GraphGeneSelectHandler );
                } else {
                    HammerListeners[graph.DomElement.ID].On( "press", GraphGeneSelectHandler );
                }

                // Options for pinch for the graph's manager
                JSObject pinchOptions = new JSObject();
                pinchOptions["enable"] = true;
                pinchOptions["threshold"] = 2;

                HammerListeners[graph.DomElement.ID].Get( "pinch" ).Set(pinchOptions);

                foreach (GeneInfo geneInfo in graph.Regulon.Genes)
                {
                    // Add the index of the current gene into the matrix
                    geneIndexes[comparedRegulons.IndexOf(regulon)][geneDictionary[geneInfo.Name]] = graph.Regulon.Genes.IndexOf(geneInfo);
                }
			}

            // Add a listener for when the browser window is resized
            Window.AddEventListener("resize", Window_OnResize, false);

            // Store the current window dimensions
            currentWindowWidth = Window.OuterWidth;
            currentWindowHeight = Window.OuterHeight;

            // If the window is larger than the threshold, set the threshold
            // as being passed (to allow size adjustment)
            if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                windowSizeThresholdPassed = true;
            }

            // Process selected genes
            OnSelectedGenesChanged();

            // Ensure the graphs have the right size and position of elements
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphZoomLevel(currentZoomLevel, i);
            }

            // Clear the status of passing the window size threshold
            windowSizeThresholdPassed = false;
		}

		/// <summary> Displays the list of regulons in the specified container, with each logical operation on the same display
		/// </summary>
		/// <param name="regulons">The list of regulons</param>
		/// <param name="container">The DOM object to act as the container</param>
		/// <param name="selectedRegulog">The currently selected regulog</param>
		/// <param name="nodeFactory">The node factory to use to create the nodes</param>
		/// <param name="colourChooser">The attribute factory to use to set the node attributes (mainly colour)</param>

		public void DisplayRegulonsAllOperations(
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser
		) {
            // Store the current regulog
            this.selectedRegulog = selectedRegulog;

            // Store the current container
            this.container = container;

            // Clear any existing graphs
            if (regulonGraphs.Length > 0) {
                Clear();
            }

            // If there is already a Hammer manager for this container, destroy
            // it
            if (containerHammer != null) {
                containerHammer.Destroy();
            }

            // Create a new Hammer manager for this container
            containerHammer = new Hammer.Hammer(container.ParentNode);

            // Sets the characteristics of the container's manager
            // let the pan gesture support all directions.
            // this will block the vertical scrolling on a touch-device while on the element
            JSObject managerOptions = new JSObject();
            managerOptions["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
            managerOptions["domEvents"] = true; //http://hammerjs.github.io/tips/
            containerHammer.Set(managerOptions);

            // Add a tap listener for the container's manager
            containerHammer.On( "tap" , Container_OnTap );

            // Set the attributes of the container
            this.container.Style.Height = "auto";
            this.container.Style.Width = "auto";

			//remove all the children this regulon display has
			while ( container.ChildNodes[0] != null ) {
				container.RemoveChild( container.ChildNodes[0] );
			}

            // Initialise the matrix of regulons and their gene locations
            geneIndexes = new int[(int)LogicalOperations.XOR][];
            for (int i = 0; i < (int)LogicalOperations.XOR; i++) {
                geneIndexes[i] = new int[selectedRegulog.Genes.Length];
                for (int j = 0; j < geneIndexes[i].Length; j++) {
                    geneIndexes[i][j] = -1;
                }
            }

            // Store the index of each gene in the matrix above
            /*foreach (GeneInfo gene in selectedRegulog.Genes) {
                geneDictionary[gene.Name] = selectedRegulog.Genes.IndexOf(gene);
            }*/

            // Create a list for all the TG names
            List<string> geneNames = new List<string>();

            // Look through all the genes in the *regulog*. This is so
            // homologues can be handled correctly
            foreach (GeneInfo gene in selectedRegulog.TargetGenes)
            {
                // Add the TG's name to the list of names, if it is not already
                // in there
                string name = gene.Name;

                if (!geneNames.Contains(name))
                {
                    geneNames.Add(name);
                }
            }

            // For each name, add it in the dictionary, using the length of the
            // dictionary as its index
            geneDictionary.Clear();
            foreach (string name in geneNames) geneDictionary[name] = geneDictionary.Count;

            // List that will store the special regulons to display
			List<RegulonInfo> comparedRegulons = new List<RegulonInfo>();

            // Generate the list of genes for these networks with the correct
            // PresentInStates
            GeneCompareInfo[] combinedTemp = GenerateGenesCombined(regulons);

			// Create a combined regulon for each logical operation
			for ( int i = (int) LogicalOperations.AND; i <= (int) LogicalOperations.XOR; i++ ) {
				comparedRegulons.Add( GenerateComparisonCombined( regulons, (LogicalOperations) i, combinedTemp) );
			}

            // Create and configure a graph for each regulon that will be
            // displayed
            foreach ( RegulonInfo regulon in comparedRegulons ) {

				RegulonGraph graph = new RegulonGraph(
					selectedRegulog,
					regulon,
					nodeFactory,
					colourChooser,
					Constants.RegulonDisplayWidth,
					Constants.RegulonDisplayHeight,
					Constants.RegulonDisplayUnits
				);

				// Set the title of the graph based on the operation
				/*if ( regulon.Genes[0] is GeneCompareInfo ) {
					switch ( ( regulon.Genes[0] as GeneCompareInfo ).CurrentOperation ) {

                        // AND operation
						case LogicalOperations.AND:
					        graph.GraphLabelText = Constants.Text_NetworkLabelAnd;
							break;

                        // OR operation
						case LogicalOperations.OR:
					        graph.GraphLabelText = Constants.Text_NetworkLabelOr;
							break;

                        // XOR operation
						case LogicalOperations.XOR:
					        graph.GraphLabelText = Constants.Text_NetworkLabelXor;
							break;

                        // Don't do anything if no operation is assigned
						default:
#if DEBUG
							if (Constants.ShowDebugMessages) Console.Log( graph.DomElement.ID + " has no operation type assigned to it for some reason!" );
#endif
							break;
					}
				}*/

                switch ( (LogicalOperations)regulon.Regulators[0].Regulator["CurrentOperation"] ) {

                    // AND operation
					case LogicalOperations.AND:
					    graph.GraphLabelText = Constants.Text_NetworkLabelAnd;
						break;

                    // OR operation
					case LogicalOperations.OR:
					    graph.GraphLabelText = Constants.Text_NetworkLabelOr;
						break;

                    // XOR operation
					case LogicalOperations.XOR:
					    graph.GraphLabelText = Constants.Text_NetworkLabelXor;
						break;

                    // Don't do anything if no operation is assigned
					default:
#if DEBUG
						if (Constants.ShowDebugMessages) Console.Log( graph.DomElement.ID + " has no operation type assigned to it for some reason!" );
#endif
						break;
				}

                // Set the graph's attributes
                SetGraphClass(graph, false);

                // Add the graph to the container as well as the list of graphs
                graph.AddElementTo( container );
				regulonGraphs.Add( graph );

                // Set up a Hammer manager for this graph
			    HammerListeners[graph.DomElement.ID] = new Hammer.Hammer( graph.DomElement );

                // Sets the characteristics of the graph's manager
                // let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                HammerListeners[graph.DomElement.ID].Set(managerOptions);

                // Add all the listeners to the graph's manager
                HammerListeners[graph.DomElement.ID].On( "tap", Graph_OnTap );
                HammerListeners[graph.DomElement.ID].On( "pinch", Graph_OnPinch );

                // Set whether tooltips appear on a tap or a press
                if (Constants.ShowTooltipsOnHammerTap) {
                    HammerListeners[graph.DomElement.ID].On( "tap", GraphTooltipHandler );
                } else {
                    HammerListeners[graph.DomElement.ID].On( "press", GraphTooltipHandler );
                }

                // Set whether TGs are selected on a tap or a press
                if (Constants.SelectGenesOnHammerTap) {
                    HammerListeners[graph.DomElement.ID].On( "tap", GraphGeneSelectHandler );
                } else {
                    HammerListeners[graph.DomElement.ID].On( "press", GraphGeneSelectHandler );
                }

                // Options for pinch for the graph's manager
                JSObject pinchOptions = new JSObject();
                pinchOptions["enable"] = true;
                pinchOptions["threshold"] = 2;

                HammerListeners[graph.DomElement.ID].Get( "pinch" ).Set(pinchOptions);

                foreach (GeneInfo geneInfo in graph.Regulon.Genes)
                {
                    // Add the index of the current gene into the matrix
                    geneIndexes[comparedRegulons.IndexOf(regulon)][geneDictionary[geneInfo.Name]] = graph.Regulon.Genes.IndexOf(geneInfo);
                }
			}

            // Add a listener for when the browser window is resized
            Window.AddEventListener("resize", Window_OnResize, false);

            // Store the current window dimensions
            currentWindowWidth = Window.OuterWidth;
            currentWindowHeight = Window.OuterHeight;

            // If the window is larger than the threshold, set the threshold
            // as being passed (to allow size adjustment)
            if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                windowSizeThresholdPassed = true;
            }

            // Process selected genes
            OnSelectedGenesChanged();

            // Ensure the graphs have the right size and position of elements
            for (int i = 0; i < regulonGraphs.Length; i++) {
                SetGraphZoomLevel(currentZoomLevel, i);
            }

            // Clear the status of passing the window size threshold
            windowSizeThresholdPassed = false;
		}

        /// <summary>
        /// Creates a list of special regulons that display in a comparison
        /// <para>Only shows one operation. Used mainly for comparing more than two networks (and OR is generally used in this case)</para>
        /// </summary>
        /// <param name="regulons">The regulons to create a comparison list for</param>
        /// <param name="operation">The logical operation to use (AND, OR or XOR)</param>
        /// <returns></returns>
		private static List<RegulonInfo> GenerateComparisonRegulonList( List<RegulonInfo> regulons, LogicalOperations operation ) {
			List<RegulonInfo> comparedRegulons = new List<RegulonInfo>();

			//If there are more than two regulons, the first regulon will be
			//treated as a "reference"
			if ( regulons.Length > 1 ) {

#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "Setting up comparisons..." );
				if ( regulons.Length > 2 ) {
					if (Constants.ShowDebugMessages) Console.Log( "Network 1 (reference network): " + regulons[0].GenomeName );
				}
#endif
                // Generate the first network in the comparison. This is always
                // treated seperately since either it is the left/first network
                // in a pair comparison, or the reference network in a multiple
                // comparison
                // A new list of genes is created for the network that stores
                // a copy of the TGs that also say what logical comparison is
                // being used and which networks they are present in
				GeneCompareInfo[] temp1 = new GeneCompareInfo[regulons[0].Genes.Length];
				for ( int i = 0; i < regulons[0].Genes.Length; i++ ) {
					temp1[i] = new GeneCompareInfo( regulons[0].Genes[i].Gene );

                    // Copy the gene's information over
					temp1[i].Regulator = regulons[0].Genes[i].Regulator;
					temp1[i].Site = regulons[0].Genes[i].Site;

                    // By default, genes in the first network are given the
                    // "left" state
					temp1[i].GenePresentIn = PresentInStates.left;
#if DEBUG
					//Console.Log(temp1[i].Gene.Name + " is present in " + regulons[0].GenomeName);
#endif
                    // Initialise the number of homologues for this gene as 1
                    numberOfHomologGenes[temp1[i].Name] = 1;

                    // Store the regulation's presence as well, using "left"
                    // as the default if regulation is there
					if ( temp1[i].Site != null ) {
						temp1[i].RegulationPresentIn = PresentInStates.left;
#if DEBUG
						//Console.Log(temp1[i].Gene.Name + " is also regulated in " + regulons[0].GenomeName);
#endif
                        // Initialise the number of regulations for this
                        // homologue at 1 as well
                        numberOfHomologRegulations[temp1[i].Name] = 1;
					}

                    // Store the current operation
					temp1[i].CurrentOperation = operation;
				}

				//For the rest of the networks, compare them to the first and
				//colour them accordingly
				for ( int i = 1; i < regulons.Length; i++ ) {
#if DEBUG
					//Console.Log("Network " + (i + 1) + ": " + regulons[i].GenomeName);
#endif
                    // A new list of genes is created for the network that stores
                    // a copy of the TGs that also say what logical comparison is
                    // being used and which networks they are present in
					GeneCompareInfo[] temp2 = new GeneCompareInfo[regulons[i].Genes.Length];
					for ( int j = 0; j < regulons[i].Genes.Length; j++ ) {
						temp2[j] = new GeneCompareInfo( regulons[i].Genes[j].Gene );

                        // Copy the gene's information over
						temp2[j].Regulator = regulons[i].Genes[j].Regulator;
						temp2[j].Site = regulons[i].Genes[j].Site;

                        // By default, genes in other networks are given the
                        // "right" state
						temp2[j].GenePresentIn = PresentInStates.right;
#if DEBUG
						//Console.Log(temp2[j].Gene.Name + " is present in " + regulons[i].GenomeName);
#endif
                        // The same is true for the regulation if it exists
						if ( temp2[j].Site != null ) {
							temp2[j].RegulationPresentIn = PresentInStates.right;
#if DEBUG
							//Console.Log(temp2[j].Gene.Name + " is also regulated in " + regulons[i].GenomeName);
#endif
						}

                        // Store the current operation
						temp2[j].CurrentOperation = operation;
					}

#if DEBUG
					if (Constants.ShowDebugMessages) Console.Log( "Determining which genes and regulations are in the reference and other networks..." );
#endif
                    // Every time another network is created for the comparison
                    // check whether its genes and regulation are present in
                    // the reference as well
					foreach ( GeneCompareInfo gene1 in temp1 ) {
						foreach ( GeneCompareInfo gene2 in temp2 ) {

                            // If genes with the same name in both networks are
                            // found, they are assumed to be homologues (due to
                            // the same positioning)
							if ( gene1.Name == gene2.Name ) {
#if DEBUG
								//Console.Log(gene1.Gene.Name + " is present in both " + regulons[0].GenomeName + " and " + regulons[i].GenomeName);
#endif
                                // Set their states to "all" to show that the
                                // genes occur in all networks (this is not
                                // entirely correct at this stage; "some" is
                                // checked for later for multiple comparisons)
								gene1.GenePresentIn = PresentInStates.all;
								gene2.GenePresentIn = PresentInStates.all;

                                // Add one to the number of homologues of this
                                // gene
                                numberOfHomologGenes[gene1.Name] = numberOfHomologGenes[gene1.Name] + 1;

                                // If the genes both have sites (i.e. are
                                // regulated), mark that as well
								if ( gene1.Site != null && gene2.Site != null ) {
#if DEBUG
									//Console.Log(gene1.Gene.Name + " is also regulated in both " + regulons[0].GenomeName + " and " + regulons[i].GenomeName);
#endif
                                    // Set their states to "all" to show that the
                                    // regulation occur in all networks (this is not
                                    // entirely correct at this stage; "some" is
                                    // checked for later for multiple comparisons)
									gene1.RegulationPresentIn = PresentInStates.all;
									gene2.RegulationPresentIn = PresentInStates.all;

                                    // Add one to the number of homologues of this
                                    // gene's regulation
                                    numberOfHomologRegulations[gene1.Name] = numberOfHomologRegulations[gene1.Name] + 1;
								}

                                // Break the loop for the current gene in
                                // network one, if there was no name for this
                                // gene - this is to prevent counting too many
                                // homologues if there are multiple genes with
                                // no name
                                if (!( gene1.Name == "undefined" ) && !( gene1.Name == null )) {
                                    break;
                                }
							}
						}
					}

                    // Create a new regulon for this secondary network
					RegulonInfo regulon2 = new RegulonInfo( regulons[i].Regulon );

                    // Set the regulon's genes, genome name and regulators,
                    // taking the latter two from existing information
					regulon2.Genes = temp2;
					regulon2.Genome = regulons[i].Genome;
					regulon2.Regulators = regulons[i].Regulators;

                    // Make sure it is not selected
                    if (regulon2.IsSelected) {
                        regulon2.IsSelected = false;
                    }

                    // Add the regulon to the list of compared regulons
					comparedRegulons.Add( regulon2 );
				}

                // Create a new regulon for this secondary network
				RegulonInfo regulon1 = new RegulonInfo( regulons[0].Regulon );

                // Set the regulon's genes, genome name and regulators, taking
                // the latter two from existing information
				regulon1.Genes = temp1;
				regulon1.Genome = regulons[0].Genome;
				regulon1.Regulators = regulons[0].Regulators;

                // Make sure it is not selected
                if (regulon1.IsSelected) {
                    regulon1.IsSelected = false;
                }

                // Insert the regulon at the start of the list of compared
                // networks
				comparedRegulons.Insert( 0, regulon1 );

                // In the case of multiple comparisons, check if the number of
                // homologues for a certain TG is the same as the number of
                // regulons in the comparison. If not, set their state to
                // "some" rather than "all"

                // For each possible TG in the compared networks
                foreach (KeyValuePair<string, int> pair in numberOfHomologGenes) {

                    // For each regulon in the list of compared networks
                    foreach (RegulonInfo regulon in comparedRegulons) {

                        // For each TG in that regulon
                        foreach (GeneCompareInfo gene in regulon.Genes) {

                            // When the current gene is reached...
                            if (gene.Name == pair.Key) {

                                // Compare the number of times that TG occurs
                                // in networks. If it is less than the total
                                // number of compared networks, and this TG is
                                // set to "all", set it to "some" instead
                                if (pair.Value < comparedRegulons.Length
                                    && gene.GenePresentIn == PresentInStates.all) {
                                    gene.GenePresentIn = PresentInStates.some;
/*#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(gene.Name + " gene in " + regulon.GenomeName + " set as a 'some'");
                                    if (Constants.ShowDebugMessages) Console.Log(gene.GenePresentIn.ToString());
#endif*/
                                }

                                // Do the same thing for this TG's regulation
                                if (numberOfHomologRegulations.ContainsKey(pair.Key)
                                    && numberOfHomologRegulations[pair.Key] < comparedRegulons.Length
                                    && gene.RegulationPresentIn == PresentInStates.all) {
                                    gene.RegulationPresentIn = PresentInStates.some;
/*#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(gene.Name + " regulation in " + regulon.GenomeName + " set as a 'some'");
                                    if (Constants.ShowDebugMessages) Console.Log(gene.RegulationPresentIn.ToString());
#endif*/
                                }
                                //break;
                            }
                        }
                    }
                }
			}

            // Don't do anything if a list of one regulon was used (this
            // shouldn't happen normally)
			else if (regulons.Length == 0) {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "We tried to compare a collection with only one regulon. We probably want a special case for this." );
#endif
                // Set the combined regulon to the single regulon that was
                // passed
				comparedRegulons.Add( regulons[0] );

			// Passing a list of no regulons should not happen either, but it
            // should be handled anyway
			} else {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "We tried to compare a collection of no regulons. This shouldn't happen." );
#endif
            }

            // Return the list of compared regulons
			return comparedRegulons;
		}

        // Stores the number of times a homologue and its regulation occur in
        // the current compared networks
        private static Dictionary<string, int> numberOfHomologGenes = new Dictionary<string,int>();
        private static Dictionary<string, int> numberOfHomologRegulations = new Dictionary<string,int>();

        /// <summary>
        /// Creates a "combined" network out of multiple regulons to show which genes are present, based on a given logical comparison
        /// <para>This does not currently support creating a network out of more than two regulons properly. Genes will not be assigned "some" status</para>
        /// </summary>
        /// <param name="regulons">The regulons to combine into one network</param>
        /// <param name="operation">The logical operation to use (AND, OR or XOR)</param>
        /// <param name="genes">A list of GeneCompareInfo to (potentially) place in this network</param>
        /// <returns></returns>
		private RegulonInfo GenerateComparisonCombined( List<RegulonInfo> regulons, LogicalOperations operation, GeneCompareInfo[] genes) {

            // Create an empty regulon to insert the combined data into
			RegulonInfo combinedRegulon;

			//If there are more than two regulons, the first regulon will be
			//treated as a "reference"
			if ( regulons.Length > 1 ) {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "Setting up comparisons..." );
#endif
                // Create the new regulon using the first regulon as a base
				combinedRegulon = new RegulonInfo( regulons[0].Regulon );

                // Put all the required genes into a new list
                List<GeneCompareInfo> combinedTemp = new List<GeneCompareInfo>();

                // Create a new gene object for all the genes that will appear
                // in this graph. This is necessary so that the gene objects
                // are different for each graph; taking the objects directly
                // from the supplied list would make the operation applied
                // below apply to *all* the graphs that share the gene
                foreach (GeneCompareInfo gene in genes) {

                    // Whether a gene will be included depends on the current
                    // operation and the PresentInState
                    // AND: only TGs that have the gene or regulation in all
                    //      networks
                    // OR:  TGs that have the gene or regulation in any network
                    // XOR: only TGs that have the gene or regulation in one
                    //      and only one of the networks
                    if ( (operation == LogicalOperations.AND && (gene.GenePresentIn == PresentInStates.all || gene.RegulationPresentIn == PresentInStates.all))
                        || (operation == LogicalOperations.OR && !(gene.GenePresentIn == PresentInStates.none && gene.RegulationPresentIn == PresentInStates.none))
                        || (operation == LogicalOperations.XOR && ((gene.GenePresentIn == PresentInStates.left || gene.GenePresentIn == PresentInStates.right) || (gene.RegulationPresentIn == PresentInStates.left || gene.RegulationPresentIn == PresentInStates.right)))
                        ) {

                        // Create a new gene object - this will differ from the
                        // one in the list soley through the operation
                        GeneCompareInfo newGene = new GeneCompareInfo(gene.Gene);

                        // Transfer the information over to the new object
                        newGene.Regulator = gene.Regulator;
                        newGene.Site = gene.Site;
                        newGene.GenePresentIn = gene.GenePresentIn;
                        newGene.RegulationPresentIn = gene.RegulationPresentIn;

                        // Apply the correct operation
                        newGene.CurrentOperation = operation;

                        // Add the gene to the list
                        combinedTemp.Add(newGene);
                    }
                }

                // Set the genes to the created list
				combinedRegulon.Genes = (GeneCompareInfo[])combinedTemp;

                // Copy the genome over
				combinedRegulon.Genome = regulons[0].Genome;

                // Create a unique regulator object from this gene (based on
                // the first regulator in the first supplied regulon (or a
                // dummy regulator if there is no regulators in the regulon)
                RegulatorInfo tempRegulator = null;
                if (regulons[0].Regulators != null && regulons[0].Regulators.Length > 0) {

                    // Create a new regulator object to avoid altering
                    // regulator data elsewhere
                    RegPrecise.Regulator tempRegulatorStats = new RegPrecise.Regulator(
                        regulons[0].Regulators[0].Regulator.RegulonId,
                        regulons[0].Regulators[0].Regulator.Name,
                        regulons[0].Regulators[0].Regulator.LocusTag,
                        regulons[0].Regulators[0].Regulator.VimssId,
                        regulons[0].Regulators[0].Regulator.RegulatorFamily
                        );
                    tempRegulator = new RegulatorInfo(tempRegulatorStats);
                    tempRegulator.Gene = regulons[0].Regulators[0].Gene;
                } else {
                    tempRegulator = new RegulatorInfo(new RegPrecise.Regulator());
                }
                tempRegulator.Regulator["CurrentOperation"] = operation;

                // Set the number of TGs
                tempRegulator.NumberOfTargetGenes = combinedTemp.Length;

                // Set the new regulator
                combinedRegulon.Regulators = new RegulatorInfo[1];
                combinedRegulon.Regulators[0] = tempRegulator;
			}

            // Don't do anything if a list of one regulon was used (this
            // shouldn't happen normally)
			else if (regulons.Length == 0) {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "We tried to compare a collection with only one regulon. We probably want a special case for this." );
#endif
                // Set the combined regulon to the single regulon that was
                // passed
				combinedRegulon = regulons[0];

            // Passing a list of no regulons should not happen either, but it
            // should be handled anyway
			} else {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "We tried to compare a collection of no regulons. This shouldn't happen." );
#endif
                // Don't send anything
                combinedRegulon = null;
            }

            // Return the special regulon
			return combinedRegulon;
		}

        /// <summary>
        /// Generates the base list of genes for a combined comparison graph(s)
        /// </summary>
        /// <param name="regulons">The regulons that will be used to make the graph</param>
        /// <returns>A list of gene objects with data identifying which of the regulons they occur in</returns>
        private GeneCompareInfo[] GenerateGenesCombined(List<RegulonInfo> regulons)
        {
            // Create a list and dictionary for all the TGs that occur at
            // least once in the given list of regulons
            List<string> names = new List<string>();
            Dictionary<string, int> geneList = new Dictionary<string, int>();

            foreach (GeneInfo gene in selectedRegulog.TargetGenes)
            {
                // TODO: Allow alternate ortholog identification mechanism. One that actually works would be good.

                // We have to assume that orthologs have the same name. Otherwise, there
                // appears to be no way to connect them in RegPrecise. However, this is
                // probably an invalid assumption. nonetheless...

                string name = gene.Name;

                // Add the TG's name to the list of names
                if (!names.Contains(name))
                {
                    names.Add(name);

                    // Store the location of the TG in the dictionary
                    geneList[name] = selectedRegulog.TargetGenes.IndexOf(gene);
                }
            }

            // Create a list of special TGs for the combined network
            GeneCompareInfo[] combinedGenes = new GeneCompareInfo[names.Length];

            // For every possible TG in the networks...
            for (int i = 0; i < names.Length; i++)
            {

                // Get the normal TG's information from the selected regulog
                GeneInfo geneInfo = selectedRegulog.Genes[geneList[names[i]]];

                // Initialise the special TG by using the normal TG's gene
                // data
                combinedGenes[i] = new GeneCompareInfo(geneInfo.Gene);

                // Use the same regulator as the original TG
                // This is not technically correct since the regulator may
                // be slightly different between regulons but this should
                // not be a huge issue at the moment
                combinedGenes[i].Regulator = geneInfo.Regulator;

                // It does not make sense to include the site since that
                // will differ between regulons
                combinedGenes[i].Site = null;

                // As a placeholder, assign "none" to both the presence of
                // the gene and the regulation
                combinedGenes[i].GenePresentIn = PresentInStates.none;
                combinedGenes[i].RegulationPresentIn = PresentInStates.none;

/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Network 1 (reference network): " + regulons[0].GenomeName);
#endif*/
                // Check if the TG appears in the first network
                // For all the TGs in the first network...
                for (int j = 0; j < regulons[0].Genes.Length; j++)
                {

                    // If a TG with the same name as the current special TG
                    // is found...
                    if (regulons[0].Genes[j].Name == combinedGenes[i].Name)
                    {

                        // Set the state to "left" to begin with
                        combinedGenes[i].GenePresentIn = PresentInStates.left;
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(regulons[0].Genes[j].Name + " is present in " + regulons[0].GenomeName);
#endif*/
                        // If the normal TG is regulated...
                        if (regulons[0].Genes[j].Site != null)
                        {

                            // Copy the site information over
                            // (I'm not sure why considering it doesn't make sense
                            // to... perhaps to have "some" information there)
                            combinedGenes[i].Gene.VimssId = regulons[0].Genes[j].Site.GeneVimssid;
                            combinedGenes[i].Site = regulons[0].Genes[j].Site;

                            // Set the state to "left" to begin with
                            combinedGenes[i].RegulationPresentIn = PresentInStates.left;
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(regulons[0].Genes[j].Name + " is also regulated in " + regulons[0].GenomeName);
#endif*/
                        }

                        // Stop checking the current TG for this network
                        break;
                    }
                }

                // Check if the TG appears in the other networks as well
                // For all of the other networks...
                for (int j = 1; j < regulons.Length; j++)
                {
/*#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Network " + (j + 1) + ": " + regulons[j].GenomeName);
#endif*/
                    // For all the TGs in the current network...
                    for (int k = 0; k < regulons[j].Genes.Length; k++)
                    {

                        // If a TG with the same name as the current special TG
                        // is found...
                        if (regulons[j].Genes[k].Name == combinedGenes[i].Name)
                        {

                            // If the special TG already had a "left" state, set
                            // it to "all", otherwise set it to "right"
                            // This is not correct for multiple comparisons
                            combinedGenes[i].GenePresentIn = combinedGenes[i].GenePresentIn == PresentInStates.left ? PresentInStates.all : PresentInStates.right;
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(regulons[j].Genes[k].Name + " is present in " + regulons[i].GenomeName);
#endif*/
                            // If the normal TG is regulated...
                            if (regulons[j].Genes[k].Site != null)
                            {

                                // Copy the site information over if it wasn't
                                // already there
                                // (I'm not sure why considering it doesn't make sense
                                // to... perhaps to have "some" information there)
                                if (combinedGenes[i].Site == null)
                                {
                                    combinedGenes[i].Gene.VimssId = regulons[j].Genes[k].Site.GeneVimssid;
                                    combinedGenes[i].Site = regulons[j].Genes[k].Site;
                                }

                                // If the special TG already had a "left" state, set
                                // it to "all", otherwise set it to "right"
                                // This is not correct for multiple comparisons
                                combinedGenes[i].RegulationPresentIn = combinedGenes[i].RegulationPresentIn == PresentInStates.left ? PresentInStates.all : PresentInStates.right;
/*#if DEBUG
                                if (Constants.ShowDebugMessages) Console.Log(regulons[j].Genes[k].Name + " is also regulated in " + regulons[i].GenomeName);
#endif*/
                            }

                            // Stop checking the current TG for this network
                            break;
                        }
                    }
                }
            }
            return combinedGenes;
        }

        /// <summary>
        /// Triggered when a gene's selection is changed. Attempts to select homologs of that gene
        /// HammerCompareRegulonDisplay gets its own version of this because
        /// dictionary handling is a bit different in the comparison
        /// </summary>
        /// <param name="gene">The gene whose selection was changed</param>
        protected override void GeneSelectionChanged(GeneInfo gene) {

            // Notify listeners that this display is currently
            // processing so the loading overlay can be displayed
            isProcessing = true;
            NotifyPropertyChanged("IsProcessing");

            // Use a timeout delegate so that the loading overlay can appear
            Script.SetTimeout((Action) delegate {

                // Get the current selected status of the sending target gene
                bool selected = gene.IsSelected;
                string currentGeneName = gene.Name;

                // Get the index of the regulon graph and the gene, and
                // combine them into a comma seperated string as the
                // identifier
                int regulonIndex = -1;
                foreach ( RegulonGraph graph in regulonGraphs ) {
                    foreach ( GeneInfo otherGene in graph.Regulon.Genes ) {
                        if (otherGene == gene) {
                            regulonIndex = regulonGraphs.IndexOf(graph);
                            break;
                        }
                    }
                }
                int geneIndex = geneDictionary[currentGeneName];
                string temp = regulonIndex + "," + geneIndex;

                // Add or remove the gene from the list of selected genes based
                // on its selection status
                if (selected) {
                    if (!selectedGenes.Contains(temp)) {
                        selectedGenes.Add(temp);
                    }
                } else {
                    if (selectedGenes.Contains(temp)) {
                        selectedGenes.Remove(temp);
                    }
                }

                // For all the regulons in the display
                for (int newRegulonIndex = 0; newRegulonIndex < regulonGraphs.Length; newRegulonIndex++)
                {

                    // Look through all regulons to find homologs to select
                    // using the dictionary of gene locations
                    RegulonGraph graph = regulonGraphs[newRegulonIndex];
                    int newGeneIndex = geneDictionary[currentGeneName];

                    // If a homolog exists for the current regulon, select
                    // or deselect it
                    if (geneIndexes[newRegulonIndex][newGeneIndex] != -1 &&
                        graph.Regulon.Genes[geneIndexes[newRegulonIndex][newGeneIndex]].IsSelected != selected) {
                        graph.Regulon.Genes[geneIndexes[newRegulonIndex][newGeneIndex]].IsSelected = selected;

                        // Combine this gene's regulon and gene index to make a
                        // identifier
                        string newTemp = newRegulonIndex + "," + newGeneIndex;

                        // Add or remove from the list as required
                        if (selected) {
                            if (!selectedGenes.Contains(newTemp)) {
                                selectedGenes.Add(newTemp);
                            }
                        } else {
                            if (selectedGenes.Contains(newTemp)) {
                                selectedGenes.Remove(newTemp);
                            }
                        }
                    }
                }

                // Additional processing on change of gene selections
                OnSelectedGenesChanged();

                // Notify listeners that this display has finished processing
                // so the loading overlay can be hidden
                isProcessing = false;
                NotifyPropertyChanged("IsProcessing");
            }, 10, null);
        }

        /// <summary>
        /// Gets or sets the list of selected genes
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        public override List<GeneInfo> SelectedGenes
        {
            get {
                // Create a new list to store the gene objects
                List<GeneInfo> geneInfos = new List<GeneInfo>();

                // Go through each string identifier in selected genes and
                // retrieve the gene object that it refers to, adding it to the
                // list of gene objects
                foreach (string index in selectedGenes) {
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);
                    geneInfos.Add(regulonGraphs[0].Regulon.Genes[1]);
                }

                // Return the list of gene objects
                return geneInfos;
            }
// New set behaviour
            set {
                // Clear the current list of selected genes
                selectedGenes.Clear();

                // Look through the list of graphs
                foreach ( RegulonGraph graph in regulonGraphs ) {
                    foreach ( GeneInfo gene in graph.Regulon.Genes ) {

                        // If the current gene is inside the supplied list of
                        // genes
                        if (value.Contains(gene)) {

                            // Get the index of the regulon graph and the gene, and
                            // combine them into a comma seperated string as the
                            // identifier
                            int regulonIndex = regulonGraphs.IndexOf(graph);
                            int geneIndex = geneDictionary[gene.Name];
                            string temp = regulonIndex + "," + geneIndex;

                            // Set the gene object as selected
                            gene.IsSelected = true;

                            // Add the identifier to the list of selected genes
                            selectedGenes.Add(temp);

                        // Otherwise ensure the gene is not selected
                        } else {
                            gene.IsSelected = false;
                        }
                    }
                }
                OnSelectedGenesChanged();
            }
        }

        /// <summary>
        /// Deselects all of the selected genes in the display
        /// </summary>
        /// <param name="allowOverlay">Whether to use a time out to allow the loading overlay to appear</param>
        public override void ClearSelectedGenes(bool allowOverlay) {

            if (selectedGenes.Length > 0) {

            if (allowOverlay) {

            // Notify listeners that this display is currently
            // processing so the loading overlay can be displayed
            isProcessing = true;
            NotifyPropertyChanged("IsProcessing");

            // Use a timeout delegate so that the loading overlay can appear
            Script.SetTimeout((Action) delegate {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Clearing all selected genes - starting with " + selectedGenes.Length + " in selectedGenes list");
#endif
                // Clear the selected status of each gene in the selected list
                // and then clear the list
                foreach (string index in selectedGenes) {

                    // Get the index of the regulon graph and the gene by
                    // splitting the identifier in the selected list
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Set the gene that corresponds to the retrieved indexes
                    // as unselected
                    regulonGraphs[regulonIndex].Regulon.Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = false;
                }

                // Clear the list of selected genes
                selectedGenes.Clear();
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Finished - " + selectedGenes.Length + " genes now in selectedGenes list");
#endif

                // Notify listeners that this display has finished processing
                // so the loading overlay can be hidden
                isProcessing = false;
                NotifyPropertyChanged("IsProcessing");
            }, 10, null);

            } else {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Clearing all selected genes - starting with " + selectedGenes.Length + " in selectedGenes list");
#endif
                // Clear the selected status of each gene in the selected list
                // and then clear the list
                foreach (string index in selectedGenes) {

                    // Get the index of the regulon graph and the gene by
                    // splitting the identifier in the selected list
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Set the gene that corresponds to the retrieved indexes
                    // as unselected
                    regulonGraphs[regulonIndex].Regulon.Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = false;
                }

                // Clear the list of selected genes
                selectedGenes.Clear();
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Finished - " + selectedGenes.Length + " genes now in selectedGenes list");
#endif
            }

            }
        }

        /// <summary>
        /// Gets or sets the currently selected regulons in the display
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        public override List<RegulonInfo> SelectedRegulons
        {
            get {
                // Create a new list to store the regulon objects
                List<RegulonInfo> regulonInfos = new List<RegulonInfo>();

                // Go through all of the indexes in the list of selected
                // regulons and retrieve the regulon object that it points to
                foreach (int index in selectedRegulons) {

                    // Add the regulon that is in the graph with the given
                    // index
                    regulonInfos.Add(regulonGraphs[index].Regulon);
                }

                // Return the list of regulon objects
                return regulonInfos;
            }
// New set behaviour
            set {
                // Clear the current list of selected regulons
                selectedRegulons.Clear();

                // Look through the list of graphs
                foreach ( RegulonGraph graph in regulonGraphs ) {

                    // If the current regulon is contained in the supplied list
                    // of regulons, set it as selected
                    if (value.Contains(graph.Regulon)) {
                        graph.Regulon.IsSelected = true;

                        // Add the index of the regulon graph to the list of
                        // selected regulons
                        selectedRegulons.Add(regulonGraphs.IndexOf(graph));

                    // Otherwise ensure it is not selected
                    } else {
                        graph.Regulon.IsSelected = false;
                    }

                    // Ensure the graph has the correct class
                    SetGraphClass(graph, false);
                }
            }
        }

        /// <summary>
        /// Runs when a property on a regulon graph is changed. Used for checking if regulon graphs have been selected
        /// - in that case, adds or removes that regulon graph from the list of selected regulons
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="args">Event arguments</param>
        protected override void regulonGraph_PropertyChanged(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs args)
        {

            // If a regulon graph's selection has been changed
            if (args.Includes("IsSelected")) {

                RegulonInfo regulon = (sender as RegulonInfo);
                int index = -1;

                // Find the regulon graph that this regulon belongs to
                for (index = 0; index < regulonGraphs.Length; index++) {
                    if (regulonGraphs[index].Regulon == regulon) {
                        break;
                    }
                }

                // Add or remove the regulon graph from the list of selected
                // regulons based on its selection status
                if (regulon.IsSelected) {
                    if (!selectedRegulons.Contains(index)) {
                        selectedRegulons.Add(index);
                    }
                } else {
                    selectedRegulons.Remove(index);
                }
            }
        }
	}
}
