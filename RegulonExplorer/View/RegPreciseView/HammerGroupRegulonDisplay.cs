﻿using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.Common;
using SystemQut;
using SystemQut.Svg;
using Hammer;
#if DEBUG
    using System.Net;
    using SystemQut.IO;
#endif
using SystemQut.ComponentModel;

namespace RegulonExplorer.View.RegPreciseView
{
    class HammerGroupRegulonDisplay : IRegulonDisplay, INotifyPropertyChanged
    {

        // A list of the current centre positions of each graph, as well as
        // their listeners.
        // This setup is based on that used in the HammerJS homepage to store
        // the current location of the square in the example
        protected Dictionary<string, double[]> HammerPositions = new Dictionary<string, double[]>();
        protected Dictionary<string, Hammer.Hammer> HammerListeners = new Dictionary<string, Hammer.Hammer>();

        // Contains the listener for the primary container
        protected Hammer.Hammer containerHammer;

        // Contains the current distances between regulons and the current
        // centroids for each group
        private double[][] currentDistances;
        private List<int> currentCentroids = new List<int>();

        /// <summary>
        /// Gets or sets a matrix of all the distances between the networks
        /// <para>Allowing setting is primarily to let distances be transferred after changing the display manager</para>
        /// </summary>
        public double[][] CurrentDistances
        {
            get { return currentDistances; }
            set { currentDistances = value; }
        }

        /// <summary>
        /// Gets the network indexes for the current centroids
        /// </summary>
        public List<int> CurrentCentroids
        {
            get { return currentCentroids; }
#if DEBUG
            // If centroids are set manually, recalculate groups and reposition
            // networks based on the new ones
            set { currentCentroids = value;

                // If this is a filtered detail view, restore everything first
                if (currentZoomLevel == ZoomLevels.DetailView && !detailViewAllGraphs) {
                    //DetailView_RestoreGraphs();

                    // Return this to the default
                    detailViewAllGraphs = true;

                    UpdateVisibleRegulons(null, null);
                }

                // Set the number of groups to the number of new centroids
                SetNumberOfGroups(currentCentroids.Length, false);

                // Create a new dictionary for the current assignments
                currentAssignments = AssignRegulonsToCentroids((List<RegulonInfo>)selectedRegulog.Regulons, currentCentroids);

                // Create a temproary list of graphs to store the current graphs
                // while the display is cleared
                List<RegulonGraph> tempRegulonGraphs = new List<RegulonGraph>();

                // Take all of the graphs out of the columns and put them in
                // the temporary list, clearing the columns as necessary
                foreach (List<RegulonGraph> column in regulonGraphs) {
                    foreach (RegulonGraph graph in column) {

                        // If the palette is present and this is the first column,
                        // reset the graph's zoom level
                        if (Constants.UseGroupPalette && regulonGraphs.IndexOf(column) == 0) {
                            SetGraphZoomLevel(currentZoomLevel, 0, column.IndexOf(graph));
                        }

                        tempRegulonGraphs.Add(graph);
                    }
                    column.Clear();
                }

                // For every graph in the temporary list
                foreach (RegulonGraph graph in tempRegulonGraphs) {

                    // Remove that graph from its column in the display
                    graph.DomElement.ParentNode.RemoveChild(graph.DomElement);

                    // Get the index of that graph's regulon in the list of
                    // regulon
                    int index = selectedRegulog.Regulons.IndexOf(graph.Regulon);

                    // If this graph is one of the current centroids, add it to the
                    // label
                    if (currentCentroids.Contains(index)) {
                        if (graph.GraphLabelText.IndexOf(" " + Constants.Text_NetworkLabelCentroid) == -1) {
                            graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelCentroid;
                        }

                    // Otherwise, remove that from the label if it was an old
                    // centroid
                    } else {
                        graph.GraphLabelText = graph.GraphLabelText.Replace(" " + Constants.Text_NetworkLabelCentroid, "");
                    }

                    // Ensure this graph has the correct class
                    SetGraphClass(graph, false, false);

                    // Add the graph to the correct column and list of graphs
                    graph.AddElementTo(columns[currentCentroids.IndexOf(currentAssignments[index])]);
                    regulonGraphs[currentCentroids.IndexOf(currentAssignments[index])+1].Add(graph);

                    // Set the regulon's current group number
                    graph.Regulon.GroupNumber = currentAssignments[index];
                }
            }
#endif
        }

        // Stores the current regulog for this display
        private RegulogInfo selectedRegulog;

        // Stores the current group assignments for each network (via which
        // centroid each network is closest to)
        private Dictionary<int, int> currentAssignments;

        /// <summary>
        /// Gets the current group assignments for each network
        /// </summary>
        public Dictionary<int, int> CurrentAssignments
        {
            get { return currentAssignments; }
        }

        // Whether centroids should be recalculated when networks are dragged
        // between groups
        private bool autoCalculateCentroidOnDrag = false;

        /// <summary>
        /// Gets or sets the setting for recalculating centroids on drag
        /// </summary>
        public bool AutoCalculateCentroidOnDrag
        {
            get { return autoCalculateCentroidOnDrag; }
            set { autoCalculateCentroidOnDrag = value; }
        }

        // The currently selected regulons in the display, in order of selection
        // if possible
        protected List<int> selectedRegulons = new List<int>();

        /// <summary>
        /// Gets or sets the currently selected regulons in the display
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        public List<RegulonInfo> SelectedRegulons
        {
            get {
                // Create a new list to store the regulon objects
                List<RegulonInfo> regulonInfos = new List<RegulonInfo>();

                // Go through all of the indexes in the list of selected
                // regulons and retrieve the regulon object that it points to
                foreach (int index in selectedRegulons) {

                    // Look through the list of graphs
                    foreach ( List<RegulonGraph> column in regulonGraphs ) {

                        // Keep track of when the regulon is found, so that
                        // extra columns aren't checked unnecessarily
                        bool foundRegulon = false;
                        foreach ( RegulonGraph graph in column ) {

                            // If this graph is showing the regulon that
                            // corresponds to the index, add that to the list
                            if ( graph.Regulon == selectedRegulog.Regulons[index] ) {
                                regulonInfos.Add(graph.Regulon);

                                // Mark that the regulon was found
                                foundRegulon = true;
                                break;
                            }
                        }

                        // Don't look at any more columns if the regulon was
                        // found
                        if (foundRegulon) {
                            break;
                        }
                    }
                }

                // Return the list of regulon objects
                return regulonInfos;
            }
// New set behaviour
            set {
                // Clear the current list of selected regulons
                selectedRegulons.Clear();

                // Look through the list of graphs
                foreach ( List<RegulonGraph> column in regulonGraphs ) {
                    foreach ( RegulonGraph graph in column ) {

                        // If the current regulon is contained in the supplied list
                        // of regulons, set it as selected
                        if (value.Contains(graph.Regulon)) {
                            graph.Regulon.IsSelected = true;

                            // Add the index of the regulon to the list of
                            // selected regulons
                            selectedRegulons.Add(selectedRegulog.Regulons.IndexOf(graph.Regulon));

                        // Otherwise ensure it is not selected
                        } else {
                            graph.Regulon.IsSelected = false;
                        }

                        // Ensure the graph has the correct class
                        SetGraphClass(graph, false, false);
                    }
                }
            }
        }

        /// <summary>
        /// Displays the list of regulons in the specified container with a default of one group
        /// </summary>
        /// <param name="regulons">The list of regulons</param>
		/// <param name="container">The DOM object to act as the container</param>
		/// <param name="selectedRegulog">The currently selected regulog</param>
		/// <param name="nodeFactory">The node factory to use to create the nodes</param>
		/// <param name="colourChooser">The attribute factory to use to set the node attributes (mainly colour)</param>
        virtual public void DisplayRegulons(
            List<RegulonInfo> regulons,
            Element container,
            RegulogInfo selectedRegulog,
            INodeFactory nodeFactory,
            IAttributeFactory colourChooser
        )
        {
            DisplayRegulonsWithExtraGroups(regulons, container, selectedRegulog, nodeFactory, colourChooser, 1, false);
        }

        /// <summary>
        /// Displays the list of regulons in the specified container, while specifing the number of groups and whether to automatically group them
        /// </summary>
        /// <param name="regulons">The list of regulons</param>
		/// <param name="container">The DOM object to act as the container</param>
		/// <param name="selectedRegulog">The currently selected regulog</param>
		/// <param name="nodeFactory">The node factory to use to create the nodes</param>
		/// <param name="colourChooser">The attribute factory to use to set the node attributes (mainly colour)</param>
        /// <param name="numberOfGroups">The number of groups to divide the display into</param>
        /// <param name="automatic">Whether the networks will be grouped automatically</param>
        public void DisplayRegulonsWithExtraGroups(
            List<RegulonInfo> regulons,
            Element container,
            RegulogInfo selectedRegulog,
            INodeFactory nodeFactory,
            IAttributeFactory colourChooser,
            int numberOfGroups,
            bool automatic
        )
        {
            // Store the current regulog
            this.selectedRegulog = selectedRegulog;

            // Remove the resize listener if it exists already
            Window.RemoveEventListener("resize", Window_OnResize, false);

            // Remove the window click listener if it exists already
            Window.RemoveEventListener("click", Window_OnClick, false);

/*#if DEBUG
            Hammer.Hammer testHammer = new Hammer.Hammer(Document.Body);
            testHammer.On( "panleft panright panup pandown", OnPanTest);
#endif*/

            // Loaded CSVs can make the number of groups appear as a string, so
            // this should ensure it is an integer
            numberOfGroups = int.Parse(numberOfGroups.ToString());

            // Store the number of groups
            // If the number of groups given is less than one, set it to one
            this.numberOfGroups = numberOfGroups < 1 ? 1 : numberOfGroups;

            // Store the number of groups in the regulog
            selectedRegulog.NumGroups = this.numberOfGroups;

            // Store the current container
            this.container = container;

            // If there is already a Hammer manager for this container, destroy
            // it
            if (containerHammer != null) {
                containerHammer.Destroy();
            }

            // Create a new Hammer manager for this container
            containerHammer = new Hammer.Hammer(container.ParentNode);

            // Sets the characteristics of the container's manager
            // let the pan gesture support all directions.
            // this will block the vertical scrolling on a touch-device while on the element
            JSObject managerOptions = new JSObject();
            managerOptions["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
            managerOptions["domEvents"] = true; //http://hammerjs.github.io/tips/

            // Add tap and press listeners for the container's manager
            //containerHammer.On( "press" , Container_OnPress );
            containerHammer.On( "tap" , Container_OnTap );

            //remove all the children this regulon display has
            while (container.ChildNodes[0] != null)
            {
                container.RemoveChild(container.ChildNodes[0]);
            }

            // If the palette setting is active, create the palette container
            // Don't create one if there is only one group
            if (Constants.UseGroupPalette && numberOfGroups > 1) {
                InitialisePalette(container);
            } else {

                // If there's no palette, add a list of regulon graphs for it
                // anyway (allows the same lists to be referenced regardless of
                // the palette's presence
                regulonGraphs.Add(new List<RegulonGraph>());
            }

            // Create a container for each group in the display
            for (int i = 0; i < numberOfGroups; i++)
            {
                DivElement newDiv = (DivElement)Document.CreateElement("div");

                // Set the attributes
                newDiv.ID = "column_" + (i + 1);
                /*//newDiv.Style.CssFloat = "left";
                newDiv.Style.BackgroundColor = "white";
                newDiv.Style.Margin = "0px";
                newDiv.Style.Border = "1px dashed black";*/
                newDiv.Style.MinWidth = Constants.RegulonDisplayWidth + Constants.RegulonDisplayUnits;
                /*newDiv.Style.Display = "table-cell";*/
                newDiv.ClassName = "GraphGroupColumn";

                // Add the group container to the main container
                container.AppendChild(newDiv);
                columns.Add(newDiv);

                // Add a new list for the graphs in this group
                regulonGraphs.Add(new List<RegulonGraph>());
            }

            // Create a list for all the TG names
            List<string> geneNames = new List<string>();

            // Look through all the genes in the *regulog*. This is so
            // homologues can be handled correctly
            foreach (GeneInfo gene in selectedRegulog.TargetGenes)
            {
                // Add the TG's name to the list of names, if it is not already
                // in there
                string name = gene.Name;

                if (!geneNames.Contains(name))
                {
                    geneNames.Add(name);
                }
            }

            // For each name, add it in the dictionary, using the length of the
            // dictionary as its index
            geneDictionary.Clear();
            foreach (string name in geneNames) geneDictionary[name] = geneDictionary.Count;

            // Initialise the matrix of regulons and their gene locations
            geneIndexes = new int[regulons.Count][];
            for (int i = 0; i < regulons.Count; i++) {
                geneIndexes[i] = new int[geneDictionary.Count];
                for (int j = 0; j < geneIndexes[i].Length; j++) {
                    geneIndexes[i][j] = -1;
                }
            }

            // Try to load the distances between networks from a text file, if
            // they do not already exist or the ratio of Hamming to Euclidean
            // distance has changed
            if (currentDistances == null || ratioChanged == true) {
#if DEBUG
                try {
                    ReadAllText( "/RegulonExplorer/Text_Files/regulog" + selectedRegulog.RegulogId + "_distances_" + ratio.ToString() + ".txt", delegate( string text ) {
                        if ( !string.IsNullOrEmpty(text) ) {
                            if (Constants.ShowDebugMessages) Console.Log("loading distances from text file");
                            StringReader reader = new StringReader( text );

                            double[][] tempLoadedDistances = new double[selectedRegulog.Regulons.Length][];

                            for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                                string currentLine = reader.ReadLine();
                                string[] currentRegulonDistances = currentLine.Split(',');
                                tempLoadedDistances[i] = new double[selectedRegulog.Regulons.Length];
                                for (int j = 0; j < selectedRegulog.Regulons.Length; j++) {
                                    tempLoadedDistances[i][j] = double.Parse(currentRegulonDistances[j]);
                                }
                            }

                            this.currentDistances = tempLoadedDistances;
                        } else {
                            if (Constants.ShowDebugMessages) Console.Log("No file retrieved for  " + selectedRegulog.RegulatorName);

                            // If a file does not exist, calculate the distances now
                            this.currentDistances = CalculateDistances(regulons);
                        }
				    });
                } catch (Exception ex) {
                    if (Constants.ShowDebugMessages) Console.Log("Failed to load distances file for " + selectedRegulog.RegulatorName + " because:" + ex.Message);

                    // If a file couldn't be loaded, calculate the distances now
                    this.currentDistances = CalculateDistances(regulons);
				}
#else
                this.currentDistances = CalculateDistances(regulons);
#endif
                ratioChanged = false;
            }

            // Stores the current assigned groups for all networks
            currentAssignments = new Dictionary<int, int>();

            // If automatic grouping is enabled, perform K-means on this
            // dataset
            if (automatic) {

                // Only do K-means if there is more than one group defined.
                // If there's one group, make the first network the centroid
                // temporarily and assign everything to it
                if (numberOfGroups > 1) {
                    currentAssignments = KMeansGroupAssignment(regulons);

                    // Store which centroid each network has
                    foreach (KeyValuePair<int, int> pair in currentAssignments) {
                        if (!currentCentroids.Contains(pair.Value))
                        {
                            currentCentroids.Add(pair.Value);
                        }
                    }
                } else {
                    currentCentroids.Add(0);
                    for (int i = 0; i <  regulons.Length; i++) {
                        currentAssignments[i] = 0;
                    }
                }

/*#if DEBUG
                DaviesBoudinTesting(selectedRegulog, currentDistances);
#endif*/
            }

            // Create and configure a graph for each regulon that will be
            // displayed
            for (int i = 0; i < regulons.Length; i++)
            {
                RegulonInfo regulon = regulons[i];
                RegulonGraph graph = new RegulonGraph(
                    selectedRegulog,
                    regulon,
                    nodeFactory,
                    colourChooser,
                    Constants.RegulonDisplayWidth,
                    Constants.RegulonDisplayHeight,
                    Constants.RegulonDisplayUnits
                );

                graph.DomElement.ID = regulon.GenomeName;

                // For each gene in the regulon
                foreach (GeneInfo geneInfo in graph.Regulon.TargetGenes)
                {
                    // Add it to the list of selected genes if it is selected
                    if (geneInfo.IsSelected) {

                        // Get the index of the regulon and the gene, and
                        // combine them into a comma seperated string as the
                        // identifier
                        int regulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                        int geneIndex = geneDictionary[geneInfo.Name];
                        string temp = regulonIndex + "," + geneIndex;
                        selectedGenes.Add(temp);
                    }

                    // Add the index of the current gene into the matrix
                    geneIndexes[selectedRegulog.Regulons.IndexOf(graph.Regulon)][geneDictionary[geneInfo.Name]] = graph.Regulon.Genes.IndexOf(geneInfo);
                }

                // Set the graph's attributes
                SetGraphClass(graph, false, false);

                // If automatic grouping is enabled, place the graph in the
                // group container it was assigned to
                if (automatic) {

                    // Add the centroid label if the current graph is a centroid
                    if (currentCentroids.Contains(i)) {
                        graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelCentroid;
                    }

                    // Add the graph to the correct group container
                    graph.AddElementTo(columns[currentCentroids.IndexOf(currentAssignments[i])]);

                    // Add the graph to the correct graph list
                    regulonGraphs[currentCentroids.IndexOf(currentAssignments[i])+1].Add(graph);

                    // Set the regulon's group number
                    regulon.GroupNumber = currentCentroids.IndexOf(currentAssignments[i])+1;
                }

                // If automatic grouping is not enabled, but the regulon has a
                // group number, place it in the correct group container
                else if (regulon.GroupNumber != null && regulon.GroupNumber > 0 && regulon.GroupNumber <= numberOfGroups) {
                    graph.AddElementTo(columns[regulon.GroupNumber-1]);
                    regulonGraphs[regulon.GroupNumber].Add(graph);

                // Else, if the palette is enabled, place the graph in that,
                // otherwise place it in the first group
                } else {
                    if (Constants.UseGroupPalette) {
                        graph.AddElementTo(palette);
                        regulonGraphs[0].Add(graph);
                        regulon.GroupNumber = 0;
                    } else {
                        graph.AddElementTo(columns[0]);
                        regulonGraphs[1].Add(graph);
                        regulon.GroupNumber = 1;
                    }
                }

                // Set up a Hammer manager for this graph
				HammerListeners[graph.DomElement.ID] = new Hammer.Hammer( graph.DomElement );

                // Sets the characteristics of the graph's manager
                // let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                HammerListeners[graph.DomElement.ID].Set(managerOptions);

                // Add all the listeners to the graph's manager
                HammerListeners[graph.DomElement.ID].On( "panleft panright panup pandown", Graph_OnPan );
                HammerListeners[graph.DomElement.ID].On( "tap", Graph_OnTap );
                HammerListeners[graph.DomElement.ID].On( "panstart", Graph_OnPanStart );
                HammerListeners[graph.DomElement.ID].On( "panend", Graph_OnPanEnd );
                HammerListeners[graph.DomElement.ID].On( "pinch", Graph_OnPinch );

                // Set whether tooltips appear on a tap or a press
                if (Constants.ShowTooltipsOnHammerTap) {
                    HammerListeners[graph.DomElement.ID].On( "tap", GraphTooltipHandler );
                } else {
                    HammerListeners[graph.DomElement.ID].On( "press", GraphTooltipHandler );
                }
                HammerListeners[graph.DomElement.ID].On( "pancancel", Graph_OnPanEnd );
#if DEBUG
                HammerListeners[graph.DomElement.ID].On( "pinchstart", Graph_OnPinchStart );
                HammerListeners[graph.DomElement.ID].On( "pinchend", Graph_OnPinchEnd );
                HammerListeners[graph.DomElement.ID].On( "pinchcancel", Graph_OnPinchEnd );
#endif
                // Set whether TGs are selected on a tap or a press
                if (Constants.SelectGenesOnHammerTap) {
                    HammerListeners[graph.DomElement.ID].On( "tap", GraphGeneSelectHandler );
                } else {
                    HammerListeners[graph.DomElement.ID].On( "press", GraphGeneSelectHandler );
                }

                // Options for pinch for the graph's manager
                JSObject pinchOptions = new JSObject();
                pinchOptions["enable"] = true;
                pinchOptions["threshold"] = 2;

                HammerListeners[graph.DomElement.ID].Get( "pinch" ).Set(pinchOptions);

                // Add a listener for the regulon's properties changing (used
                // to detect selection)
                graph.Regulon.PropertyChanged += regulonGraph_PropertyChanged;
            }

            // Change the zoom level of all graphs in the palette to the icon
            // list
            if (Constants.UseGroupPalette) {
                foreach (RegulonGraph graph in regulonGraphs[0]) {
                    SetGraphZoomLevel(ZoomLevels.IconList, 0, regulonGraphs[0].IndexOf(graph));
                }
            }

            //Make sure the columns have the correct dimensions
            UpdateDimensionsOfColumns();

            /*foreach (DivElement div in columns) {
                int index = columns.IndexOf(div) + 1;
                Element newDivLabel = Document.CreateElement("label");
                newDivLabel.TextContent = "Group " + (index);
                newDivLabel.ClassName = "GraphGroupLabel";
                if (index == 1 && !Constants.GroupPalette) {
                    newDivLabel.Style.Left = 40 + (div.OffsetLeft - div.ScrollLeft + div.ClientLeft) + "px";
                } else {
                    newDivLabel.Style.Left = 5 + (div.OffsetLeft - div.ScrollLeft + div.ClientLeft) + "px";
                }
                newDivLabel.Style.Top = 5 + (div.OffsetTop - div.ScrollTop + div.ClientTop) + "px";
                div.AppendChild(newDivLabel);
                columnLabels.Add(newDivLabel);
            }*/

            // Add a listener for when the browser window is resized
            Window.AddEventListener("resize", Window_OnResize, false);

            // Add a listener for when a click occurs in the browser window
            Window.AddEventListener("click", Window_OnClick, false);

            // Store the current window dimensions
            currentWindowWidth = Window.OuterWidth;
            currentWindowHeight = Window.OuterHeight;

            // If the window is larger than the threshold, set the threshold
            // as being passed (to allow size adjustment)
            if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                windowSizeThresholdPassed = true;
            }

            // Process selected genes
            OnSelectedGenesChanged();

            // If we haven't already calculated the centroids because
            // automatic grouping wasn't used, or only one group was requested,
            // do it now
            if (!automatic || numberOfGroups == 1) {
                CalculateAllCentroids(regulons);
            }

            // Remove the dashed border if there is only one group
            if (numberOfGroups == 1) {
                columns[0].ClassName = String.Empty;
            }

            // Ensure the graphs have the right size and position of elements
            for (int i = 1; i < regulonGraphs.Length; i++) {
                for (int j = 0; j < regulonGraphs[i].Length; j++) {
                    SetGraphZoomLevel(currentZoomLevel, i, j);
                }
            }

            // Clear the status of passing the window size threshold
            windowSizeThresholdPassed = false;
/*#if DEBUG
            requireSelectionBugHandler = false;
            //checkForSelectionBug = true;
#endif*/
        }

        /// <summary> Removes all regulon graphs from the display and disposes them, leaving the
        ///		display blank.
        /// </summary>

        public void Clear()
        {
            // Hide the tooltip if it is present, and clear all selected
            // genes and regulons
            HideCurrentToolTip();
            ClearSelectedGenes(false);
            ClearSelectedRegulons();

            /*if (this.container != null)
            {
            }*/

            // For all columns...
            for (int i = 0; i < regulonGraphs.Length; i++ ) {

                // Remove all the graphs in that column from the display
                foreach (RegulonGraph graph in regulonGraphs[i])
                {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Removing graph " + graph.Regulon.GenomeName + " from regulonGraphs[" + i + "]");
#endif
                    // Destroy the Hammer manager that was watching this graph
                    HammerListeners[graph.DomElement.ID].Destroy();

                    // Remove the graph from the DOM
                    graph.RemoveElement();

                    // Properly dispose the graph
				    graph.Dispose();
                }
            }

            // Clear the list of lists of regulon graphs
            regulonGraphs.Clear();

            // Clear the list of columns
            columns.Clear();

            // Clear the list of current centroids
            currentCentroids.Clear();

            // Clear the list of graphs in the detail view
            if (detailedViewGraphs != null && detailedViewGraphs.Length > 0) {
                detailedViewGraphs.Clear();
            }
        }

        // The list of all the graphs currently being dragged
        private List<RegulonGraph> draggedGraphs = new List<RegulonGraph>();

        // The starting location of the latest pan event
        private double[] panStart = { -1, -1 };

        /// <summary>
        /// Activates when a pan event is occuring on a graph
        /// <para>On a pan event, move the graph along with the pan by altering its absolute positioning in the container</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnPan(JSObject ev) {
#if DEBUG
            // Only proceed if a pinching action is not occuring
            if (!pinching) {
#endif
            // Do not do the following if this is in the detail view when it is
            // not showing all the graphs
            //if (!Constants.DetailedViewSelected || detailViewAllGraphs) {
                Script.Literal("ev.preventDefault()");

                // On the Microsoft surface, we have some issues with weird delta
                // values. This bit will attempt to prevent odd results from that
                if (panStart[0] == -1) {
                    panStart[0] = (double)(ev["center"] as JSObject)["x"];
                    panStart[1] = (double)(ev["center"] as JSObject)["y"];
                }

                // If a drag goes outside the bounds of the window, stop the drag
                if (panStart[0] + (double)ev["deltaX"] < 0
                    || panStart[0] + (double)ev["deltaY"] < 0
                    || panStart[1] + (double)ev["deltaX"] > Window.OuterWidth
                    || panStart[1] + (double)ev["deltaY"] > Window.OuterHeight) {
    #if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Pan would go outside the bounds of the page; cancelling the pan function.");
    #endif

                    // Force a drop if anything was being dragged
                    if (draggedGraphs.Length > 0) {
    #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Attempting to drop all dragged graphs.");
    #endif
                        Graph_OnPanEnd(ev);
                    }
                    return;
                }

                // Find the parent span of the dragged graph, to use it as the
                // target
                Element target = FindParentOfType( (Element)ev["target"], "span" );

                // for now, we block scrolling of the parent container so that we
                // don't get funny results if we accidentally scroll
                container.ParentNode.Style.Overflow = "hidden";

                // Create a new position storage for the target span if one does
                // not yet exist
                // This is used to keep track of the span's original position
                if (HammerPositions[target.ID] == null) {
                    HammerPositions[target.ID] = new double[] { -1, -1 };
                }

                // Find the group that this graph is in to help find the actual
                // graph object in the lists of networks
                Element temp = target.ParentNode;
                int tempIndex = 0;
                if (columns.IndexOf(temp) > -1) {
                    tempIndex = columns.IndexOf(temp) + 1;
                }

                // Find the graph that is being dragged in the correct list of
                // networks
                foreach (RegulonGraph graph in regulonGraphs[tempIndex]) {
                    if (graph.DomElement.ID == target.ID) {

                        // Variables to store the current position of the graph as well as
                        // its height
                        double leftPosition = 0;
                        double topPosition = 0;
                        double graphHeight = graph.GraphSvg.DomElement.ClientHeight;

                        // Complete hack solution for Firefox since it always reports 0 for
                        // ClientHeight of SVG objects
                        // If the graph height is zero, find the *actual* graph height
                        if (graphHeight == 0) {

                            // Determine the default graph height (based on whether the
                            // window size is past the threshold)
                            int defaultGraphHeight = (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) ? 200 * Constants.LargeScaleNodeScaleFactor : 200;

                            // If the graph was in the palette, treat it as if the current
                            // zoom level was "Icon list"
                            if (tempIndex == 0) {
                                graphHeight = defaultGraphHeight * (1/3);

                            // Else get the actual height depending on the current zoom
                            // level
                            } else {
                                switch (currentZoomLevel)
                                {
                                case ZoomLevels.IconList:
                                    graphHeight = defaultGraphHeight * (1/3);
                                    break;
                                case ZoomLevels.SmallOverview:
                                    graphHeight = defaultGraphHeight;
                                    break;
                                case ZoomLevels.LargeOverview:
                                    graphHeight = defaultGraphHeight * 1.5;
                                    break;
                                case ZoomLevels.DetailView:
                                    graphHeight = defaultGraphHeight * 3.75;
                                    break;
                                default:
                                    graphHeight = defaultGraphHeight;
                                    break;
                                }
                            }
                        }
                        // End Firefox hack

                        // Script literals are used here since getBoundingClientRect()
                        // cannot be called here, only in JavaScript
                        // Get the bounding box of the graph and the container
                        Script.Literal("var boundingBoxGraph = target.getBoundingClientRect();");
                        Script.Literal("var boundingBoxContainer = this._container.getBoundingClientRect();");

                        // Use the two bounding boxes to determine the exact X and Y
                        // coordinates of the graph in the container
                        Script.Literal("leftPosition = boundingBoxGraph.left - boundingBoxContainer.left;");
                        Script.Literal("topPosition = (boundingBoxGraph.bottom - graphHeight) - boundingBoxContainer.top;");
                        Script.Literal("console.log('graphHeight = ' + graphHeight + '\\n boundingBoxGraph.left = ' + boundingBoxGraph.left + '\\n boundingBoxGraph.bottom = ' + boundingBoxGraph.bottom + '\\n boundingBoxContainer.left = ' + boundingBoxContainer.left + '\\n boundingBoxContainer.top = ' + boundingBoxContainer.top + '\\n leftPosition = ' + leftPosition + '\\n topPosition = ' + topPosition);");

                        // Round the X and Y values
                        leftPosition = Math.Round(leftPosition);
                        topPosition = Math.Round(topPosition);

                        // If the current location hasn't been stored yet, store the X and
                        // Y position from the above, as well as applying it as attributes
                        // to the graph, and making it absolute positioned
                        if (HammerPositions[target.ID][0] == -1) {
                            target.Style.Left = leftPosition + "px";
                            HammerPositions[target.ID][0] = leftPosition;
                            target.Style.Top = topPosition + "px";
                            HammerPositions[target.ID][1] = topPosition;
                            target.Style.Position = "absolute";
                        }

                        // Place the graph in the new location, based on adding the current
                        // distance panned to the stored distance. The distance is counted
                        // from the initial touch position.
                        // It is limited to a minimum of 0, 0, to prevent odd results by
                        // having a negative coordinate
                        target.Style.Left = Math.Max(HammerPositions[target.ID][0] + (double)ev["deltaX"], 0) + "px";
                        target.Style.Top = Math.Max(HammerPositions[target.ID][1] + (double)ev["deltaY"], 0) + "px";

                        // Set the graph's attributes
                        SetGraphClass(graph, true, false);

                        // Increase the graph's Z index so that appears above other
                        // graphs in the container
                        graph.GraphSvg.DomElement.Style.ZIndex = 5;

                        // If it is not already, put the graph in the list of
                        // dragged graphs
                        if (draggedGraphs.IndexOf(graph) == -1) {
                            draggedGraphs.Add(graph);
                        }
                        break;
                    }
                }
            //}
#if DEBUG
            }
#endif
        }

/*#if DEBUG
        protected void OnPanTest(JSObject ev) {
            if (Constants.ShowDebugMessages) Console.Log("Current pan centre is " + (double)(ev["center"] as JSObject)["x"] + ", " + (double)(ev["center"] as JSObject)["y"]);
            if (Constants.ShowDebugMessages) Console.Log("Current pan delta is " + (double)ev["deltaX"] + ", " + (double)ev["deltaY"]);
        }
#endif*/


        /// <summary>
        /// Activates when a pan event ends on a graph
        /// <para>At the end of a panning event, place a dragged graph</para>
        /// <para>This is used since the "isFinal" attribute of the pan event does not always fire - it's mainly for internal HammerJS use</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected virtual void Graph_OnPanEnd(JSObject ev) {
#if DEBUG
            // Only proceed if a pinching action is not occuring
            if (!pinching) {
#endif
            // Do not do the following if this is in the detail view when it is
            // not showing all the graphs
            //if (!Constants.DetailedViewSelected || detailViewAllGraphs) {
                Script.Literal("ev.preventDefault()");

                // Find the parent span of the dragged graph, to use it as the
                // target
                Element target = FindParentOfType( (Element)ev["target"], "span" );
    #if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("PanEnd triggered on " + target.ID);
    #endif

                // Reset the overflow to auto to allow scrolling again
                container.ParentNode.Style.Overflow = "auto";

                // Determine which group this graph was in
                Element temp = target.ParentNode;
                int tempIndex = 0;
                if (columns.IndexOf(temp) > -1) {
                    tempIndex = columns.IndexOf(temp) + 1;
                }
    /*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("PanEnd - HammerPositions[" + target.ID + "] = {" + HammerPositions[target.ID][0] + ", " + HammerPositions[target.ID][1] + "}");
    #endif*/

                // Get the cursor's X and Y coordinates
                double mouseX = (double)((JSObject)ev["center"])["x"];
                double mouseY = (double)((JSObject)ev["center"])["y"];
    #if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Mouse location is " + mouseX + ", " + mouseY);
    #endif

                //We want to place the dragged network in the position that's
                //closest to the place the user dragged to
                int newIndex = Constants.UseGroupPalette ? 0 : 1;

                //First, we need to see which column the touch position is. If it
                //is not in a column, it defaults to the palette
                // Or group 1 if the palette is disabled
                for (int i = 0; i < columns.Length; i++) {
                    int columnLeft = HtmlUtil.GetElementX(columns[i]);
                    int columnRight = columnLeft + columns[i].ClientWidth;
                    int columnTop = HtmlUtil.GetElementY(columns[i]);
                    int columnBottom = columnTop + columns[i].ClientHeight;

                    if ((columnLeft <= mouseX) && (mouseX <= columnRight) && (columnTop <= mouseY) && (mouseY <= columnBottom)) {
                        newIndex = i + 1;
                        break;
                    }
                }
    #if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("newIndex = " + newIndex);
    #endif
                // The current index of the graph that is closest to the mouse X
                // and Y
                int closestGraphIndex = -1;

                // The current closest distance between a non-dragged graph and the
                // mouse X and Y
                double closestDistance = double.MaxValue;

                //For every network in the container, check its actual distance
                //from the mouse cursor's position
    #if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Determining the closest graph to the drop position");
    #endif
                // For each graph in the list of graphs in the target group...
                for (int i = 0; i < regulonGraphs[newIndex].Length; i++) {

                    // If the current graph is not one of the dragged graphs...
                    if (draggedGraphs.IndexOf(regulonGraphs[newIndex][i]) == -1) {
        #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[newIndex][i].Regulon.Genome.Name + " GetElementX = " + HtmlUtil.GetElementX(regulonGraphs[newIndex][i].DomElement) +
                            ", GetElementY = " + HtmlUtil.GetElementY(regulonGraphs[newIndex][i].DomElement) +
                            ", ClientWidth = " + regulonGraphs[newIndex][i].GraphSvg.DomElement.ClientWidth +
                            ", ClientHeight = " + regulonGraphs[newIndex][i].GraphSvg.DomElement.ClientHeight);
        #endif
                        //Calculate the centre of the current network
                        int graphX = HtmlUtil.GetElementX(regulonGraphs[newIndex][i].DomElement) + (regulonGraphs[newIndex][i].GraphSvg.DomElement.ClientWidth)/2;
                        int graphY = HtmlUtil.GetElementY(regulonGraphs[newIndex][i].DomElement) + (regulonGraphs[newIndex][i].GraphSvg.DomElement.ClientHeight)/2;
        #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[newIndex][i].Regulon.Genome.Name + " centre = " + graphX + ", " + graphY);
        #endif

                        //Determine the distance between the current centre and the
                        //touch's position using Pythagoras
                        double currentDistance = Math.Sqrt(Math.Pow(mouseX - graphX, 2) + Math.Pow(mouseY - graphY, 2));
        #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[newIndex][i].Regulon.Genome.Name + " centre is " + currentDistance + " away.");
        #endif

                        //If the calculated distance is less than the current closest,
                        //this network is the new closest
                        if (currentDistance < closestDistance) {
                            closestDistance = currentDistance;
                            closestGraphIndex = i;
        #if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[newIndex][i].Regulon.Genome.Name + " is the new closest.");
        #endif
                        }
                    } else {
                        if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[newIndex][i].Regulon.Genome.Name + " is currently being dragged - don't count it!");
                    }
                }

                //If we found a closest network, place the dragged network in front
                //of the closest one (in essence, putting it in its place)
                if (closestGraphIndex > -1) {

                    //However, if it's:
                    //- Closest to the last network in the container (if the container
                    //is not empty), and the drop point was to the right, or completely
                    //underneath, or
                    //- To the right and below the top left corner of the last
                    //  network
                    //      - put the dropped network last
                    //- To the left and below the top left corner of the bottom
                    //  left network
                    //      - put the dropped network in place of that network

                    // Get the X and Y coordinates of the "last" graph in the
                    // container
                    Element lastGraphElement;

                    // Look in the palette if necesary, otherwise look in the
                    // corresponding group
                    if (newIndex == 0) {
                        lastGraphElement = palette.ChildNodes[palette.ChildNodes.Length - 1];
                    } else {
                        lastGraphElement = columns[newIndex-1].ChildNodes[columns[newIndex-1].ChildNodes.Length - 1];
                    }
                    int lastGraphX = HtmlUtil.GetElementX(lastGraphElement);
                    int lastGraphY = HtmlUtil.GetElementY(lastGraphElement);

                    //Find the network that is at the bottom left
                    int bottomLeftGraphIndex = -1;
                    int bottomLeftGraphX = int.MaxValue;

                    // For each graph in the list of graphs in the target group
                    for (int i = 0; i < regulonGraphs[newIndex].Length; i++) {

                        // Get that graph's X and Y coordinates
                        int currentGraphX = HtmlUtil.GetElementX(regulonGraphs[newIndex][i].DomElement);
                        int currentGraphY = HtmlUtil.GetElementY(regulonGraphs[newIndex][i].DomElement);

                        // If this graph is on the same Y value as the last graph,
                        // and its X coordinate is less than the X coordinate of
                        // the current "bottom left" network, store it and its
                        // X coordinate
                        if (currentGraphY == lastGraphY && currentGraphX < bottomLeftGraphX) {
                            bottomLeftGraphX = HtmlUtil.GetElementX(regulonGraphs[newIndex][i].DomElement);
                            bottomLeftGraphIndex = i;
                        }
                    }

                    // If there is a "last" graph and the mouse X and Y is greater
                    // than that graph's X and Y, put the dragged graph at the end
                    // of the group
                    if (lastGraphX != int.MinValue && ((mouseX > lastGraphX + lastGraphElement.ChildNodes[0].ClientWidth && mouseY > lastGraphY)
                        || (mouseY > lastGraphY + lastGraphElement.ChildNodes[0].ClientHeight && mouseX > lastGraphX))) {
                        if (newIndex > 0) {
                            columns[newIndex-1].AppendChild(target);
                        } else {
                            palette.AppendChild(target);
                        }
    #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed at the end of the container");
    #endif
                    // Else, if there is a "bottom left" graph and the mouse X is
                    // less than its X coordinate, and more than its Y cooridnate,
                    // put the dragged graph in front of that graph
                    } else if (bottomLeftGraphX != int.MaxValue && ((mouseX < bottomLeftGraphX) && (mouseY > lastGraphY))) {
                        if (newIndex > 0) {
                            columns[newIndex-1].InsertBefore(target, regulonGraphs[newIndex][bottomLeftGraphIndex].DomElement);
                        } else {
                            palette.InsertBefore(target, regulonGraphs[newIndex][bottomLeftGraphIndex].DomElement);
                        }
    #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed in front of " + regulonGraphs[newIndex][bottomLeftGraphIndex].Regulon.Genome.Name);
    #endif
                    // Else, put the graph in front of the graph that it is closest
                    // to
                    } else {
                        if (newIndex > 0) {
                            columns[newIndex-1].InsertBefore(target, regulonGraphs[newIndex][closestGraphIndex].DomElement);
                        } else {
                            palette.InsertBefore(target, regulonGraphs[newIndex][closestGraphIndex].DomElement);
                        }
    #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("The dropped object was placed in front of " + regulonGraphs[newIndex][closestGraphIndex].Regulon.Genome.Name);
    #endif
                    }
                }

                //Else append it to the end of the group - this should only
                //occur if the container is empty
                else {
                    if (newIndex > 0) {
                        columns[newIndex-1].AppendChild(target);
                    } else {
                        if (Constants.UseGroupPalette) {
                            palette.AppendChild(target);
                        } else {
                            columns[0].AppendChild(target);
                        }
                    }
    #if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("A closest graph could not be found, appending the dropped object to the end of the container.");
    #endif
                }

                //If the item was dropped onto a new column, we need to change
                //which regulonGraph list it is in
                if ((newIndex > 0 && columns[newIndex-1] != temp) || (newIndex == 0 && palette != temp))
                {
                    // For recalculating the centroid
                    List<RegulonInfo> regulons = (List<RegulonInfo>)selectedRegulog.Regulons;

    #if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Current centroids are " + currentCentroids.ToString());
    #endif
                    if (newIndex > 0 && columns.Contains(temp))
                    {
                        // Find the original group the graph was in
                        int oldIndex = columns.IndexOf(temp) + 1;

                        // Find the dragged graph in the list of graphs for old
                        // group
                        for (int i = 0; i < regulonGraphs[oldIndex].Length; i++)
                        {
                            if (regulonGraphs[oldIndex][i].DomElement == target)
                            {
                                // Set the graph's regulon's group number to the
                                // new group's index
                                regulonGraphs[oldIndex][i].Regulon.GroupNumber = newIndex;

                                //Remove the centroid label if this was a centroid
                                if (currentCentroids.IndexOf(selectedRegulog.Regulons.IndexOf(regulonGraphs[oldIndex][i].Regulon)) != -1) {
                                    regulonGraphs[oldIndex][i].GraphLabelText = regulonGraphs[oldIndex][i].GraphLabelText.Replace(" " + Constants.Text_NetworkLabelCentroid, "");

                                    // Also reset the graph's class
                                    SetGraphClass(regulonGraphs[oldIndex][i], false, false);
    /*s#if DEBUG
                                    if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[oldIndex][i].DomElement.ID + " was a centroid - removing that status.");
    #endif*/
                                }

                                // Place the graph in the new list and remove it
                                // from the old one
                                regulonGraphs[newIndex].Add(regulonGraphs[oldIndex][i]);
                                regulonGraphs[oldIndex].RemoveAt(i);
                                break;
                            }
                        }
                    }
                    // from palette to column
                    if (newIndex > 0 && temp == palette)
                    {
                        //int newIndex = columns.IndexOf(target) + 1;

                        // Find the dragged graph in the list of graphs for the
                        // palette
                        for (int i = 0; i < regulonGraphs[0].Length; i++)
                        {
                            if (regulonGraphs[0][i].DomElement == target)
                            {
                                // Set the graph's regulon's group number to the
                                // new group's index
                                regulonGraphs[0][i].Regulon.GroupNumber = newIndex;

                                // Place the graph in the new list and remove it
                                // from the old one
                                regulonGraphs[newIndex].Add(regulonGraphs[0][i]);
                                regulonGraphs[0].RemoveAt(i);
                                break;
                            }
                        }

                        // Ensure that this graph has the correct zoom level
                        SetGraphZoomLevel(currentZoomLevel, newIndex, regulonGraphs[newIndex].Length-1);
                    }
                    // from column to palette
                    if (newIndex == 0 && columns.Contains(temp))
                    {
                        // Find the original group the graph was in
                        int oldIndex = columns.IndexOf(temp) + 1;

                        // Find the dragged graph in the list of graphs for old
                        // group
                        for (int i = 0; i < regulonGraphs[oldIndex].Length; i++)
                        {
                            if (regulonGraphs[oldIndex][i].DomElement == target)
                            {
                                // Set the graph's regulon's group number to the
                                // new group's index
                                regulonGraphs[oldIndex][i].Regulon.GroupNumber = 0;

                                //Remove the centroid label if it has it
                                if (AutoCalculateCentroidOnDrag && currentCentroids.IndexOf(selectedRegulog.Regulons.IndexOf(regulonGraphs[oldIndex][i].Regulon)) != -1) {
                                    regulonGraphs[oldIndex][i].GraphLabelText = regulonGraphs[oldIndex][i].GraphLabelText.Replace(" " + Constants.Text_NetworkLabelCentroid, "");

                                    // Also reset the graph's class
                                    SetGraphClass(regulonGraphs[oldIndex][i], false, false);
    //#if DEBUG
    //                                if (Constants.ShowDebugMessages) Console.Log(regulonGraphs[oldIndex][i].DomElement.ID + " was a centroid - removing that status.");
    //#endif
                                }

                                // Place the graph in the new list and remove it
                                // from the old one
                                if (Constants.UseGroupPalette) {
                                    regulonGraphs[0].Add(regulonGraphs[oldIndex][i]);
                                } else {
                                    regulonGraphs[1].Add(regulonGraphs[oldIndex][i]);
                                }
                                regulonGraphs[oldIndex].RemoveAt(i);
                                break;
                            }
                        }

                        // If put in the palette, force the graph's zoom level
                        // to "Icon list"
                        if (Constants.UseGroupPalette) {
                            SetGraphZoomLevel(ZoomLevels.IconList, newIndex, regulonGraphs[newIndex].Length-1);
                        }
                    }

                    // Add centroid labels
                    // Recalculate centroids
                    if (autoCalculateCentroidOnDrag) {
                        CalculateAllCentroids(regulons);
    /*#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("New centroids are " + currentCentroids.ToString());
    #endif*/
                    }

                }

                // Reset the initial location of this graph
                if (HammerPositions[target.ID] != null) {
                    HammerPositions[target.ID][0] = -1;
                    HammerPositions[target.ID][1] = -1;
                } else {
                    if (Constants.ShowDebugMessages) Console.Log("HammerPositions[target.ID] was null for some reason");
                }

                // Return the position attribute to "initial" so that the graph
                // uses HTML flow again and clear its left and top attributes
                target.Style.Position = "initial";
                target.Style.Left = string.Empty;
                target.Style.Top = string.Empty;

                // Find the dragged graph in the list of graphs
                foreach (RegulonGraph graph in regulonGraphs[newIndex]) {
                    if (graph.DomElement.ID == target.ID) {

                        // Ensure that the dragged graph is returned to its proper
                        // class
                        SetGraphClass(graph, false, false);

                        // Return it to the default Z level
                        graph.DomElement.Style.ZIndex = 0;

                        // Remove it from the list of dragged graphs
                        draggedGraphs.Remove(graph);
                        break;
                    }
                }

                //Make sure the columns have the correct dimensions
                UpdateDimensionsOfColumns();

                // For Surface fix
                panStart[0] = -1;
                panStart[1] = -1;
                //}
#if DEBUG
            // Note that a pan action is no longer being performed
            panning = false;
            }
#endif
            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();
        }

        /// <summary>
        /// Calculates the centroids for all of the current groups
        /// </summary>
        /// <param name="regulons">The list of regulons to calculate centroids for</param>
        private void CalculateAllCentroids(List<RegulonInfo> regulons)
        {
            // Clear the list of current centroids
            currentCentroids.Clear();

            // Go through all non-palette groups
            for (int i = 1; i < RegulonGraphs.Length; i++) {

                // Add all the regulons from that group to a new list
                List<int> group = new List<int>();
                foreach (RegulonGraph graph in regulonGraphs[i]) {
                    group.Add(regulons.IndexOf(graph.Regulon));
                }

                // Calculate the centroid for that group
                currentCentroids[i-1] = CalculateCentroid(regulons, group);
            }

            // Go through all non-palette groups (again)
            for (int i = 1; i < RegulonGraphs.Length; i++) {

                // For each graph in that group...
                foreach (RegulonGraph graph in regulonGraphs[i]) {

                    // If the current graph is a centroid...
                    if (currentCentroids.Contains(regulons.IndexOf(graph.Regulon))) {

                        // Add " (centroid)" to its label if it is not already there
                        if (graph.GraphLabelText.IndexOf(" " + Constants.Text_NetworkLabelCentroid) == -1) {
                            graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelCentroid;

                            // Set the graph to flash to indicate to the user
                            // that it is a new centroid
                            SetGraphClass(graph, false, true);
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(graph.DomElement.ID + " in group " + i + " was marked as a centroid");
#endif*/
                        } else {
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(graph.DomElement.ID + " in group " + i + " is already a centroid");
#endif*/

                            // Even if it was already a centroid, flash it
                            // anyway to inform the user
                            SetGraphClass(graph, false, false);

                            if (numberOfGroups > 1) {
                                SetGraphClass(graph, false, true);
                            }
                        }

                    // If it is not a centroid, ensure that " " + Constants.Text_NetworkLabelCentroid is
                    // removed from its label if it is there
                    } else {
                        graph.GraphLabelText = graph.GraphLabelText.Replace(" " + Constants.Text_NetworkLabelCentroid, "");

                        // Return it to its regular class
                        SetGraphClass(graph, false, false);
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(graph.DomElement.ID + " in group " + i + " is not a centroid - removing label if it has it");
#endif*/
                    }
                }
            }
        }

        /// <summary>
        /// Activates when a tap event is detected on a graph
        /// <para>When a tap event occurs, hide tooltips and select networks if they were tapped on</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnTap(JSObject ev) {
            Script.Literal("ev.preventDefault()");


            // Don't select anything if the user clicked in a tooltip
            if (toolTipNode != null) {
                // Get the cursor's X and Y coordinates
                int mouseX = (int)((JSObject)ev["center"])["x"];
                int mouseY = (int)((JSObject)ev["center"])["y"];

                // If the cursor is within the tooltip, hide it and do nothing
                // else
                if (toolTipNode.IsWithinToolTipExternal(mouseX, mouseY)) {
                    HideCurrentToolTip();
                    return;

                // Else hide it and continue
                } else {
                    HideCurrentToolTip();
                }
            }

/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Tap target is a " + (ev["target"] as Element).TagName);
#endif*/

            // Find the parent span of the target, to use it as the actual target
            Element target = FindParentOfType( (Element)ev["target"], "span" );
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Tap triggered on " + target.ID);
#endif*/

            // Get the parent of the target to determine if it is a group and
            // store the group number if it is
            Element temp = target.ParentNode;
            int tempIndex = 0;
            if (columns.IndexOf(temp) > -1) {
                tempIndex = columns.IndexOf(temp) + 1;
            }

            // If the target is not a circle or path (which would make it a
            // node)...
            if ((String.Compare( (ev["target"] as Element).TagName, "circle", true )) != 0
                && (String.Compare( (ev["target"] as Element).TagName, "path", true )) != 0) {

                // Find the graph that was clicked on from the list of graphs
                // in the current group...
                foreach (RegulonGraph graph in regulonGraphs[tempIndex]) {
                    if (target == FindParentOfType(graph.DomElement, "span")) {

                        // Toggle the selected status of the graph
                        graph.IsSelected = !graph.IsSelected;
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log(graph.Regulon.GenomeName + " has had its selection changed!");
#endif*/
                        // Update the class of the graph
                        SetGraphClass(graph, false, false);

                        // Do nothing else here
                        break;
                    }
                }

            // Else if the tap happened on a node
            } /*else {
//#if DEBUG
//                if (Constants.ShowDebugMessages) Console.Log("Tap was triggered on a non background element and so we don't want to select the graph.");
//#endif
                // Whether the node that was tapped on was found, to quit out
                // of the loops below
                bool foundNode = false;

                // Determine which group of graphs to check, depending on
                // whether this is a filtered detail view
                List<RegulonGraph> graphs = regulonGraphs[tempIndex];
                if (!detailViewAllGraphs) {
                    graphs.Clear();
                    foreach (List<RegulonGraph> graphList in detailedViewGraphs) {
                        foreach (RegulonGraph graph in graphList) {
                            graphs.Add(graph);
                        }
                    }
                }

                // Check through the graphs to determine which one the tap
                // occured in
                foreach (RegulonGraph graph in graphs)
                {
                    // If the graph where the click occured in is found, go
                    // through all of the nodes in that graph
                    if (target == FindParentOfType(graph.DomElement, "span")) {
                        foreach (AbstractNode node in graph.Nodes) {

                            // If the correct node is found...
                            if (node.Node.DomElement == (ev["target"] as Element)) {

                                // Toggle the selected status of the node
                                node.IsSelected = !node.IsSelected;

                                // Process the change in selection status
                                if (node.Gene is GeneInfo) {
                                    GeneSelectionChanged((node.Gene as GeneInfo));
                                }

                                // Inform that the node was found and do
                                // nothing else
                                foundNode = true;
                                break;
                            }
                        }
                    }

                    // Stop looping if the node was found
                    if (foundNode) { break; }
                }
            }*/
        }

        /// <summary>
        /// Activates when a Hammer pinch event is detected on a graph
        /// <para>On pinching in or out, change the zoom level</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnPinch(JSObject ev)
        {
#if DEBUG
            if (!panning) {
#endif
            Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip
            HideCurrentToolTip();

            // Find the parent span of the target, to use it as the actual target
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Pinch triggered on " + target.ID);
#endif
            // Get the scale value that was generated by the pinch
            double scale = (double)ev["scale"];

            // If pinching inwards and not already at the "Icon list", decrease
            // the zoom level
            if (scale < 1 && currentZoomLevel > ZoomLevels.IconList) {
                CurrentZoomLevel = currentZoomLevel - 1;
            }

            // If pinching outwards and not already at the "Detail view",
            // increase the zoom level
            if (scale > 1 && currentZoomLevel < ZoomLevels.DetailView) {
                CurrentZoomLevel = currentZoomLevel + 1;
            }

            // Notify listeners that the zoom level has changed
            NotifyPropertyChanged("ZoomLevel");
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Zoom level changed to " + currentZoomLevel);
#endif
#if DEBUG
            }
#endif
        }

        /// <summary>
        /// Activates when a Hammer pan event starts on a graph
        /// <para>At the start of a panning event, put the graph on the top layer</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnPanStart(JSObject ev) {
#if DEBUG
            // Only proceed if a pinching action is not occuring
            if (!pinching) {
                // Note that a panning event is being performed
                panning = true;
#endif
            //if (!Constants.DetailedViewSelected || detailViewAllGraphs) {
            Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip
            HideCurrentToolTip();

            // Find the parent span of the target, to use it as the actual target
            Element target = FindParentOfType( (Element)ev["target"], "span" );
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("PanStart triggered on " + target.ID);
#endif
            // for now, we block scrolling of the parent container so that we
            // don't get funny results if we accidentally scroll
            // container.ParentNode.Style.Overflow = "hidden";
#if DEBUG
            }
#endif
            //}
        }

        /// <summary>
        /// Stores the node that is currently showing a tooltip, so it can be
        /// cleared later on
        /// </summary>
        private AbstractNode toolTipNode;

        /// <summary>
        /// Shows tooltips on clicking on nodes in a network.
        /// <para>Can be on either tap or press events, depending on the value of ShowTooltipsOnHammerTap (default is true)</para>
        /// <para>This function was  originally "GraphOnPress"</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        private void GraphTooltipHandler(JSObject ev)
        {
            Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip
            HideCurrentToolTip();

            // Check through all the lists of graphs in the display
            foreach (RegulonGraph[] graphs in regulonGraphs) {

                // Check through all the graphs in the current list
                foreach (RegulonGraph graph in graphs) {

                    // Check through all the target gene nodes in the graph
                    foreach (AbstractNode node in graph.Nodes) {

                        // If this node's DOM element sent the event, show the
                        // tooltip
                        if (node.Node.DomElement == ev["target"]) {

                            // Get the coordinates of the cursor
                            int mouseX = (int)((JSObject)ev["center"])["x"];
                            int mouseY = (int)((JSObject)ev["center"])["y"];

                            // Show the tooltip at that location and store the
                            // current node so that the tooltip can be cleared
                            // later
                            node.ShowToolTipExternal( mouseX, mouseY );
                            toolTipNode = node;
                            break;
                        }
                    }


                    // Check through all the regulator nodes in the graph
                    foreach (AbstractNode node in graph.RegulatorNodes) {

                        // If this node's DOM element sent the event, show the
                        // tooltip
                        if (node.Node.DomElement == ev["target"]) {

                            // Get the coordinates of the cursor
                            int mouseX = (int)((JSObject)ev["center"])["x"];
                            int mouseY = (int)((JSObject)ev["center"])["y"];

                            // Show the tooltip at that location and store the
                            // current node so that the tooltip can be cleared
                            // later
                            node.ShowToolTipExternal( mouseX, mouseY );
                            toolTipNode = node;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Activates when a tap event is detected on the container
        /// <para>When tapping the container, hide any existing tooltips</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        private void Container_OnTap(JSObject ev)
        {
            Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip
            Element target = (Element)ev["target"];
            if (target == container || target == palette || columns.Contains(target)) {
            //if (!(ev["target"] is SvgElement)) {
                HideCurrentToolTip();
            }
        }

        /// <summary>
        /// Hides any tooltip currently visible in the display
        /// <para>Can be called from the outside so that tooltips do not persist, for example, when the display manager is changed</para>
        /// </summary>
        public void HideCurrentToolTip()
        {
            if (toolTipNode != null)
            {
                toolTipNode.HideToolTipExternal();
                toolTipNode = null;
            }
        }

#if DEBUG
        // Used to check whether a pinching or panning action is occuring so
        // that the application does not try to do both at the same time
        protected bool pinching = false;
        protected bool panning = false;

        /// <summary>
        /// Listens for when a pinching event starts
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        protected void Graph_OnPinchStart(JSObject ev)
        {
            // Only proceed if a panning action is not occuring
            if (!panning) {
                Script.Literal("ev.preventDefault()");
    //#if DEBUG
    //            Script.Literal("console.log(ev.type + ' triggered (Graph_OnPinchStart)')");
    //#endif
                // Note that a pinch action is being performed
                pinching = true;
            }
        }

        /// <summary>
        /// Listens for when a pinching event ends
        /// </summary>
        /// <param name="ev"></param>
        protected void Graph_OnPinchEnd(JSObject ev)
        {
            // Only proceed if a panning action is not occuring
            if (!panning) {
                Script.Literal("ev.preventDefault()");

    //#if DEBUG
    //            Script.Literal("console.log(ev.type + ' triggered (Graph_OnPinchEnd)')");
    //#endif
                // Note that a pinch action is no longer being performed
                pinching = false;
            }
        }
#endif
        /// <summary>
        /// (De)selects TGs on clicking/tapping nodes in a network.
        /// <para>Can be on either tap or press events, depending on the value of SelectGenesOnHammerTap (default is false)</para>
        /// <para>This was originally handled in "Graph_OnTap"</para>
        /// </summary>
        /// <param name="ev">The attributes of the Hammer event</param>
        private void GraphGeneSelectHandler(JSObject ev)
        {
            // Don't select anything if the user clicked in a tooltip
            if (toolTipNode != null) {
                // Get the cursor's X and Y coordinates
                int mouseX = (int)((JSObject)ev["center"])["x"];
                int mouseY = (int)((JSObject)ev["center"])["y"];

                // If the cursor is within the tooltip, do nothing
                // else
                if (toolTipNode.IsWithinToolTipExternal(mouseX, mouseY)) {
                    return;
                }
            }

            // Find the parent span of the target, to use it as the actual target
            Element target = FindParentOfType( (Element)ev["target"], "span" );

            if ((String.Compare( (ev["target"] as Element).TagName, "circle", true )) == 0
                || (String.Compare( (ev["target"] as Element).TagName, "path", true )) == 0) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Tap was triggered on a non background element and so we don't want to select the graph.");
#endif*/
                // Whether the node that was tapped on was found, to quit out
                // of the loops below
                bool foundNode = false;

                // Get the parent of the target to determine if it is a group and
                // store the group number if it is
                Element temp = target.ParentNode;
                int tempIndex = 0;
                if (columns.IndexOf(temp) > -1) {
                    tempIndex = columns.IndexOf(temp) + 1;
                }

                // Determine which group of graphs to check, depending on
                // whether this is a filtered detail view
                List<RegulonGraph> graphs = regulonGraphs[tempIndex];
                if (!detailViewAllGraphs) {
                    graphs.Clear();
                    foreach (List<RegulonGraph> graphList in detailedViewGraphs) {
                        foreach (RegulonGraph graph in graphList) {
                            graphs.Add(graph);
                        }
                    }
                }

                // Check through the graphs to determine which one the tap
                // occured in
                foreach (RegulonGraph graph in graphs)
                {
                    // If the graph where the click occured in is found, go
                    // through all of the nodes in that graph
                    if (target == FindParentOfType(graph.DomElement, "span")) {
                        foreach (AbstractNode node in graph.Nodes) {

                            // If the correct node is found...
                            if (node.Node.DomElement == (ev["target"] as Element)) {

                                // Toggle the selected status of the node
                                node.IsSelected = !node.IsSelected;

                                // Process the change in selection status
                                if (node.Gene is GeneInfo) {
                                    GeneSelectionChanged((node.Gene as GeneInfo));
                                }

                                // Inform that the node was found and do
                                // nothing else
                                foundNode = true;
                                break;
                            }
                        }
                    }

                    // Stop looping if the node was found
                    if (foundNode) { break; }
                }
            }
        }

        // --------------------------------------------------------------------
        // Things copied from GroupRegulonDisplay.cs
        // --------------------------------------------------------------------

        /// <summary> The container that holds these graphs.
        /// </summary>
        private Element container;

        /// <summary> Maintain a list of GUI elements which correspond to the data model.
        /// </summary>
        private List<List<RegulonGraph>> regulonGraphs = new List<List<RegulonGraph>>();

        /// <summary> Maintain a list of the DivElement columns that are used as groups.
        /// </summary>
        private List<Element> columns = new List<Element>();

        /// <summary>
        /// The labels of the groups
        /// </summary>
        private List<Element> columnLabels = new List<Element>();

        // Used when calculating the required column height for all columns
        //private int currentMaxColumnHeight;

        /// <summary>
        /// The special group container for the palette, if it is used
        /// </summary>
        private Element palette;

        // The number of groups currently being used in the display
        private int numberOfGroups;

        /// <summary>
        /// Gets the current list of graphs in the display
        /// </summary>
        public List<List<RegulonGraph>> RegulonGraphs
        {
            get { return regulonGraphs; }
        }

        /// <summary>
        /// Gets the number of groups in the display
        /// </summary>
        public int NumberOfGroups
        {
            get { return numberOfGroups; }
        }

        // The list of currently selected genes
        // Stores genes as a combination of the regulon index and the gene
        // index in a string, seperated by a comma
        private List<string> selectedGenes = new List<string>();

        /// <summary>
        /// Gets or sets the list of selected genes
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        public List<GeneInfo> SelectedGenes
        {
            get {
                // Create a new list to store the gene objects
                List<GeneInfo> geneInfos = new List<GeneInfo>();

                // Go through each string identifier in selected genes and
                // retrieve the gene object that it refers to, adding it to the
                // list of gene objects
                foreach (string index in selectedGenes) {
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Look through the list of graphs
                    foreach ( List<RegulonGraph> column in regulonGraphs ) {

                        // Keep track of when the gene is found, so that extra
                        // columns aren't checked unnecessarily
                        bool foundGene = false;
                        foreach ( RegulonGraph graph in column ) {

                            // If this graph is showing the regulon that
                            // corresponds to the index, add the gene object that's
                            // selected to the list (since a gene must exist to be
                            // selected, geneIndexes doesn't need checking for a
                            // "-1")
                            if (graph.Regulon == selectedRegulog.Regulons[regulonIndex]) {
                                geneInfos.Add(graph.Regulon.Genes[geneIndexes[regulonIndex][geneIndex]]);

                                // Mark that the gene was found
                                foundGene = true;
                                break;
                            }
                        }

                        // Don't look at any more columns if the gene was found
                        if (foundGene) {
                            break;
                        }
                    }
                }

                // Return the list of gene objects
                return geneInfos;
            }
// New set behaviour
            set {
                // Clear the current list of selected genes
                selectedGenes.Clear();

                // Look through the list of graphs
                foreach ( List<RegulonGraph> column in regulonGraphs ) {
                    foreach ( RegulonGraph graph in column ) {
                        foreach ( GeneInfo gene in graph.Regulon.Genes ) {

                            // If the current gene is inside the supplied list of
                            // genes
                            if (value.Contains(gene)) {

                                // Get the index of the regulon and the gene, and
                                // combine them into a comma seperated string as the
                                // identifier
                                int regulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                                int geneIndex = geneDictionary[gene.Name];
                                string temp = regulonIndex + "," + geneIndex;

                                // Set the gene object as selected
                                gene.IsSelected = true;

                                // Add the identifier to the list of selected genes
                                selectedGenes.Add(temp);

                            // Otherwise ensure the gene is not selected
                            } else {
                                gene.IsSelected = false;
                            }
                        }
                    }
                }
                OnSelectedGenesChanged();
            }
        }

        // The current zoom level of the display
        protected ZoomLevels currentZoomLevel = ZoomLevels.SmallOverview;

        /// <summary>
        /// Gets the current zoom level of the display
        /// <para>The setter was originaly a seperate method called "ChangeZoomLevel"</para>
        /// </summary>
        public ZoomLevels CurrentZoomLevel
        {
            get { return currentZoomLevel; }
            set {
                // Don't do anything if the passed zoom level is the same
                if (currentZoomLevel == value) return;

                // Special filtering if the detail view is intended to only display
                // the selected networks
                if (Constants.DetailedViewSelected) {

                    // If the new zoom level is the detail view, check if the
                    // display needs filtering
                    if (value == ZoomLevels.DetailView) {

                        // Check if there are any selected networks
                        for (int i = 0; i < regulonGraphs.Length; i++) {
                            foreach (RegulonGraph graph in regulonGraphs[i]) {
                                if (graph.Regulon.IsSelected) {

                                    // Note that the display is not showing all graphs
                                    // in the detail view
                                    detailViewAllGraphs = false;
                                    break;
                                }
                            }
                        }

                        // If there are selected networks, filter the display
                        if (!detailViewAllGraphs) {
                            //DetailView_FilterGraphs();
                            UpdateVisibleRegulons(null, delegate ( RegulonInfo regulon ) { return regulon.IsSelected; } );
                        }

                    // Else if changing out of the detail view, make sure all
                    // networks are visible again
                    } else {
                        if (!detailViewAllGraphs) {
                            //DetailView_RestoreGraphs();

                            // Return this to the default
                            detailViewAllGraphs = true;

                            UpdateVisibleRegulons(null, null);
                        }
                    }
                }

                // Set the zoom level for all graphs in the display
                currentZoomLevel = value;
                for (int i = 1; i < regulonGraphs.Length; i++) {
                    for (int j = 0; j < regulonGraphs[i].Length; j++) {
                        SetGraphZoomLevel(value, i, j);
                    }
                }

                // Clear the status of passing the window size threshold if that's
                // why this method was called, as well as update the height of the
                // columns
                UpdateDimensionsOfColumns();
                windowSizeThresholdPassed = false;
            }
        }

        // The ratio of Euclidean and Hamming distances
        private double ratio = -1;

        /// <summary>
        /// Gets or sets the ratio of Euclidean and Hamming distances
        /// <para>0 = full Euclidean, 1 = full Hamming (default is 0.5)</para>
        /// </summary>
        public double Ratio
        {
            get { return ratio; }
            set { if (value == ratio) return;
                ratio = value;
                /*ratioChanged = true;*/ }
        }

        //Used to inform if the above has changed
        private bool ratioChanged = false;

        /// <summary>
        /// Gets or sets the status of the ratio changing
        /// </summary>
        public bool RatioChanged {
            get { return ratioChanged; }
            set { ratioChanged = value; }
        }

        /// <summary>
        /// Stores the number of graphs that are currently visible in the display
        /// <para>This is set manually by some functions</para>
        /// </summary>
        private int numberOfGraphs = -1;

        /// <summary>
        /// Returns the number of graphs in the display
        /// <para>How graphs are counted for this is dependant on the invidiual regulon display classes</para>
        /// </summary>
        public int NumberOfGraphs {
            get {
                if (numberOfGraphs == -1) {
                    int tempNumberOfGraphs = 0;
                    foreach (List<RegulonGraph> column in regulonGraphs) {
                        tempNumberOfGraphs += column.Length;
                    }
                    return tempNumberOfGraphs;
                } else {
                    return numberOfGraphs;
                }
            }
        }

        // Used to inform listeners if the selected genes are changed
        public event EventHandler SelectedGenesChanged;

        /// <summary>
        /// Assigns all the regulons in the data set to groups using a K-means clustering algorithim
        /// </summary>
        /// <param name="regulons">The list of regulons</param>
        /// <returns></returns>
        private Dictionary<int, int> KMeansGroupAssignment(List<RegulonInfo> regulons)
        {
            //Initialise the list of centroids, and the list of the networks
            //and which groups they were in (including the centroids, they go
            //in their own groups)
            List<int> centroids = new List<int>();

            //Initalise the list of assignments
            Dictionary<int, int> assignments = new Dictionary<int,int>();

            //Pick an amount of centroids based upon the number of groups
            //selected (or all of the networks if there's less networks than
            //groups). Centroids are picked randomly
            int counter = 0;
            while (counter < Math.Min(numberOfGroups, regulons.Length))
            {
                //Pick a random network to act as a centroid
                int centroid = Math.Floor(Math.Random() * regulons.Length);
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Attempting to add network " + centroid + " as a centroid");
#endif

                //Check if the same centroid wasn't chosen twice
                if (!centroids.Contains(centroid))
                {
                    centroids.Add(centroid);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Regulon " + centroid + " (" + regulons[centroid].GenomeName + ") added as a centroid");
#endif

                    //Only advance if we picked a centroid that wasn't already
                    //picked
                    counter++;
                }
            }

            //List<int> previousCentroids = new List<int>();
            Dictionary<int, int> previousAssignments = new Dictionary<int, int>();
            int numberOfTimes = 1;

            //We will keep going until the list of centroids we generate is the
            //same as before
            bool assignmentEquilibrium = false;
            while (!assignmentEquilibrium) {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Number of times processing centroids = " + numberOfTimes);
#endif
                numberOfTimes = numberOfTimes + 1;
#if DEBUG
                //Console.Log("previousCentroids == " + previousCentroids);
                if (Constants.ShowDebugMessages) Console.Log("centroids == " + centroids);
#endif
                if (numberOfTimes > 1) {
                    foreach (KeyValuePair<int, int> pair in assignments) {
                        previousAssignments[pair.Key] = pair.Value;
                    }
                }

                // Assign the networks to the centroid they are closest to
                assignments = AssignRegulonsToCentroids(regulons, centroids);

                //For each group, recalculate the centroids by calculating the
                //one in the group that has the smallest total distance

                //First create a list of lists of the networks in each centroid
                //group
                List<List<int>> groups = new List<List<int>>();
                for (int i = 0; i < centroids.Length; i++)
                {
                    groups[i] = new List<int>();
                }
                foreach (KeyValuePair<int, int> pair in assignments) {

                    //Because JavaScript treats keys in pairs as strings, we
                    //need to have this ugly conversion
                    groups[centroids.IndexOf(pair.Value)].Add(int.Parse(pair.Key.ToString()));
                }

                // Stores the new centroids
                centroids = new List<int>();

                //For each group...
                foreach (List<int> group in groups)
                {
                    centroids[groups.IndexOf(group)] = CalculateCentroid(regulons, group);
                }

                //If the centroids we just found are not the same as the ones
                //at the start of this loop, set centroidEquilibrium to false
                //which will mean the loop will be processed another time
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("new centroids == " + centroids);
#endif
                if (numberOfTimes > 1) {
                    assignmentEquilibrium = true;
                    foreach (KeyValuePair<int, int> pair in assignments) {
                        if (previousAssignments[pair.Key] != pair.Value) {
                            assignmentEquilibrium = false;
                            break;
                        }
                    }
                }
            }

            //Return the completed list of group assignments
#if DEBUG
            //Console.Log("Centroid equilibrium has been found!");
            if (Constants.ShowDebugMessages) Console.Log("Assignment equilibrium has been found!");
#endif
            return assignments;
        }

        /// <summary>
        /// Assigns networks to a supplied list of centroids based upon which centroid they are closest to
        /// </summary>
        /// <param name="regulons">The networks to assign to groups</param>
        /// <param name="centroids">The centroids of each group</param>
        /// <returns></returns>
        private Dictionary<int, int> AssignRegulonsToCentroids(List<RegulonInfo> regulons, List<int> centroids)
        {
            //Initalise the list of assignments
            Dictionary<int, int> assignments = new Dictionary<int,int>();

            //Go through all the networks and determine which group they should
            //belong to
            for (int i = 0; i < regulons.Length; i++)
            {
                //Centroids go in their own group, obviously
                if (centroids.Contains(i))
                {
                    assignments[i] = i;
                }
                else
                {
                    //Initally, put the network in the same group as the first
                    //centroid
                    assignments[i] = centroids[0];

                    //Check the current network to see which centroid it is
                    //closest to
                    foreach (int centroid in centroids)
                    {
                        //For each centroid, see if the current network is
                        //closer to it than to the centroid it is currently
                        //assigned to, and then switch its assignment if it is.
                        //Because of the way this is implemented, a network
                        //will go to the lower number centroid if it is the
                        //same distance from more than one
#if DEBUG
                        //Console.Log("Regulon " + i + " (" + regulons[i].GenomeName + ") is " + currentDistances[i][centroid] + " away from centroid " + centroids.IndexOf(centroid));
#endif
                        if (currentDistances[i][centroid] < currentDistances[i][assignments[i]])
                        {
                            assignments[i] = centroid;
                        }
                    }
#if DEBUG
                    //Console.Log("Regulon " + i + " (" + regulons[i].GenomeName + ") will be in the same group as centroid " + centroids.IndexOf(assignments[i]));
#endif
                }
            }
            return assignments;
        }

        /// <summary>
        /// Adds a palette to the main container to store ungrouped networks
        /// <para>Not used if the constant GroupPalette is false</para>
        /// </summary>
        /// <param name="container">The container to insert the palette into</param>
        private void InitialisePalette(Element container)
        {
            palette = Document.CreateElement("div");

            // Set the attributes
            palette.ID = "palette";
            /*palette.Style.CssFloat = "left";
            palette.Style.BackgroundColor = "white";
            palette.Style.Margin = "0px";
            palette.Style.Border = "1px dashed black";
            palette.Style.Height = "auto"; //minGraphSize + 10 + "px";*/
            palette.Style.MinHeight = (200 / 3) + 10 + "px";
            //palette.Style.Width = "99%";
            /*palette.Style.Width = "100%";*/
            palette.ClassName = "GraphGroupPalette";

            // Add the palette container to the main container
            if (container.ChildNodes.Length > 0) {
                container.InsertBefore(palette, container.FirstChild);
            } else {
                container.AppendChild(palette);
            }

            // Add a new list for the graphs in this group
            regulonGraphs.Add(new List<RegulonGraph>());

            // Create the label for the palette, set its attributes, and add it
            // to the palette container
            Element paletteLabel = Document.CreateElement("label");
            paletteLabel.TextContent = "Palette";
            paletteLabel.ClassName = "GraphGroupLabelPalette";
            palette.AppendChild(paletteLabel);
        }

        /// <summary>
        /// Calculates the distances between a given set of regulons
        /// <para>Uses the "Ratio" variable to determine the ratio of Hamming and Euclidean distance to use. 0 = full Euclidean, 1 = full Hamming (default is 0.5)</para>
        /// </summary>
        /// <param name="regulons">The regulons to calculate the distance between</param>
        /// <returns>A matrix (array of arrays) of distances for each regulon to every other regulon in the supplied list</returns>
        private double[][] CalculateDistances(List<RegulonInfo> regulons)
        {
            // Make sure the ratio value is not out of bounds
            if (ratio < 0) {
                ratio = 0;
            } else if (ratio > 1) {
                ratio = 1;
            }
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Alpha value is " + ratio + " (Hamming weight = " + ratio + ", Euclidean weight = " + (1 - ratio) + ")");
#endif*/
            // Get the total number of possible TGs
            int n = geneDictionary.Count;

            //Initialise the distances table
            double[][] distances = new double[regulons.Length][];

            // Set all of the distances to 0 by default
            for (int i = 0; i < regulons.Length; i++) {
                distances[i] = new double[regulons.Length];
                for (int j = 0; j < regulons.Length; j++) {
                    distances[i][j] = 0;
                }
            }

            //Calculate both the Hamming distance and Euclidean distance for
            //each
            for (int i = 0; i < regulons.Length; i++) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Creating Hamming and Euclidean distances for " + regulons[i].GenomeName);
#endif*/
                for (int j = 0; j < regulons.Length; j++) {
                    double iEuclidean = 0;

                    //If we're not comparing this network to itself...
                    if (i != j) {
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Creating Hamming distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName);
#endif*/
                        //Create two vectors to store the presence of genes in
                        //each network
                        double[] iVector = new double[n];
                        double[] jVector = new double[n];
                        int count = 0;

                        //For each gene in the regulog, check if it is present
                        //in both. For Hamming distance, regulation is not
                        //relevant.
                        foreach (KeyValuePair<string, int> geneName in geneDictionary) {
                            iVector[count] = 0;
                            jVector[count] = 0;
                            double iValue = 0;
                            double jValue = 0;

                            //If the gene is in the network, it'll match one of
                            //the names in the list. If so, set the value to 1
                            //for Hamming distance, and for Eucildean distance,
                            //set the value to 1 if there's regulation, or 0.75
                            //if there's not
                            foreach (GeneInfo regulonGene in regulons[i].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (ratio > 0) {
/*#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " - setting the entry in iVector to '1'");
#endif*/
                                        iVector[count] = 1;
                                    }
                                    if (ratio < 1) {
                                        if (regulonGene.Site != null) {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " and is regulated - setting iValue to '1'");
#endif*/
                                            iValue = 1;
                                        } else {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[i].GenomeName + " and is not regulated - setting iValue to '0.75'");
#endif*/
                                            iValue = 0.75;
                                        }
                                    }
                                        break;
                                }
                            }

                            //Do the same with the network that will be
                            //compared with the first
                            foreach (GeneInfo regulonGene in regulons[j].TargetGenes) {
                                if (regulonGene.Name == geneName.Key) {
                                    if (ratio > 0) {
/*#if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " - setting the entry in jVector to '1'");
#endif*/
                                        jVector[count] = 1;
                                    }
                                    if (ratio < 1) {
                                        if (regulonGene.Site != null) {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " and is regulated - setting jValue to '1'");
#endif*/
                                            jValue = 1;
                                        } else {
/*#if DEBUG
                                            if (Constants.ShowDebugMessages) Console.Log(regulonGene.Name + " is in " + regulons[j].GenomeName + " and is not regulated - setting jValue to '0.75'");
#endif*/
                                            jValue = 0.75;
                                        }
                                    }
                                    break;
                                }
                            }

                            //add (xn - yn)^2
                            iEuclidean = iEuclidean + Math.Pow((iValue - jValue), 2);
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("iVector[count] = " + iVector[count] + ", jVector[count] = " + jVector[count] + ", iValue = " + iValue + ", jValue = " + jValue + ", iEuclidean = " + iEuclidean);
#endif*/
                            count = count + 1;
                        }
                        double hammingDistance = 0;
                        for (int k = 0; k < n; k++) {
/*#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log("Gene " + k + " value for " + regulons[i].GenomeName + " is " + iVector[k] + " and value for " + regulons[j].GenomeName + " is " + jVector[k]);
#endif*/
                            if (iVector[k] != jVector[k]) {
                                hammingDistance = hammingDistance + Math.Abs(iVector[k] - jVector[k]);
                            }
                        }
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Hamming distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName + " is " + hammingDistance);
#endif*/
                        distances[i][j] = distances[i][j] + hammingDistance * ratio;

                        //Raise all the (xn - yn)^2 terms to the power of 1/2 and
                        //add it to the distance
/*#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Euclidean distance for " + regulons[i].GenomeName + " and " + regulons[j].GenomeName + " is " + Math.Pow(iEuclidean, 0.5));
#endif*/
                        distances[i][j] = distances[i][j] + (1 - ratio) * (Math.Pow(iEuclidean, 0.5));
                    } else {
                        distances[i][j] = 0;
                    }
                }
            }

            // Return the distances matrix
            return distances;
        }

        private List<string> selectedHomologs = new List<string>();

        // These store the locations of genes inside the arrays in each regulon
        // for easier access
        private int[][] geneIndexes;
        private Dictionary<string, int> geneDictionary = new Dictionary<string,int>();

        /// <summary>
        /// Triggered when a gene's selection is changed. Attempts to select homologs of that gene
        /// </summary>
        /// <param name="gene">The gene whose selection was changed</param>
        protected void GeneSelectionChanged(GeneInfo gene) {

            // Don't do anything if a null gene was passed
            if (gene == null) {
                return;
            }

            // Notify listeners that this display is currently
            // processing so the loading overlay can be displayed
            isProcessing = true;
            NotifyPropertyChanged("IsProcessing");

            // Use a timeout delegate so that the loading overlay can appear
            Script.SetTimeout((Action) delegate {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Selecting genes - starting with " + selectedGenes.Length + " in selectedGenes list");
#endif

                // Get the current selected status of the sending target gene
                bool selected = gene.IsSelected;
                string currentGeneName = gene.Name;

                // Get the index of the regulon and the gene, and
                // combine them into a comma seperated string as the
                // identifier
                int regulonIndex = -1;
                foreach (RegulonInfo regulon in selectedRegulog.Regulons) {
                    if (regulon.RegulonId == gene.RegulonId) {
                        regulonIndex = selectedRegulog.Regulons.IndexOf(regulon);
                        break;
                    }
                }
                int geneIndex = geneDictionary[currentGeneName];
                string temp = regulonIndex + "," + geneIndex;

                // Add or remove the gene from the list of selected genes based
                // on its selection status
                if (selected) {
                    if (!selectedGenes.Contains(temp)) {
#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Adding " + currentGeneName + " (from RegulonID " + gene.RegulonId + ") to selected list");
#endif
                        selectedGenes.Add(temp);
                    }
                } else {
                    if (selectedGenes.Contains(temp)) {
#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Removing " + currentGeneName + " (from RegulonID " + gene.RegulonId + ") from selected list");
#endif
                        selectedGenes.Remove(temp);
                    }
                }

                // For all columns of regulons in the display
                foreach (List<RegulonGraph> column in regulonGraphs)
                {

                    // For all regulons in that column
                    foreach (RegulonGraph graph in column)
                    {
                        // Look through all regulons to find homologs to select
                        // using the dictionary of gene locations
                        int newRegulonIndex = selectedRegulog.Regulons.IndexOf(graph.Regulon);
                        int newGeneIndex = geneDictionary[currentGeneName];

                        // If a homolog exists for the current regulon, select
                        // or deselect it
                        if (geneIndexes[newRegulonIndex][newGeneIndex] != -1) {
                            GeneInfo homolog = graph.Regulon.Genes[geneIndexes[newRegulonIndex][newGeneIndex]];
                            if (homolog != null && homolog != gene &&
                                homolog.IsSelected != selected) {
                                homolog.IsSelected = selected;

                                // Combine this gene's regulon and gene index to make a
                                // identifier
                                string newTemp = newRegulonIndex + "," + newGeneIndex;

                                // Add or remove from the list as required
                                if (selected) {
                                    if (!selectedGenes.Contains(newTemp)) {
                                        selectedGenes.Add(newTemp);
    #if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log("Adding homolog " + homolog.Name + " (from RegulonID " + homolog.RegulonId + ") to selected list");
    #endif
                                    }
                                } else {
                                    if (selectedGenes.Contains(newTemp)) {
    #if DEBUG
                                        if (Constants.ShowDebugMessages) Console.Log("Removing homolog " + homolog.Name + " (from RegulonID " + homolog.RegulonId + ") to selected list");
    #endif
                                        selectedGenes.Remove(newTemp);
                                    }
                                }
#if DEBUG
                                // Use the bug selection handler if required
                                if (requireSelectionBugHandler) {
                                    if (Constants.ShowDebugMessages) Console.Log(String.Format("Activating selection bug handler for gene {0} in RegulonID {1}", homolog.Name, homolog.RegulonId));
                                    SelectionBugHandler(homolog);
                                }
#endif
                            }
                        }
                    }
                }

                // Additional processing on change of gene selections
                OnSelectedGenesChanged();
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Finished - " + selectedGenes.Length + " genes now in selectedGenes list");
#endif
/*#if DEBUG
                // At present, there are currently some issues with homologue
                // selection in DisplayManagers created from
                // RegPreciseRegulonGraphViewer2. This is here to attempt to detect
                // the issue and allow the selection to work properly
                if (checkForSelectionBug && selectedGenes.Length > 1) {

                    foreach (GeneInfo selectedGene in SelectedGenes) {

                        // Keep track of when a desync in gene objects is found
                        bool desyncFound = false;

                        // Go through each graph in the display
                        foreach (List<RegulonGraph> column in regulonGraphs) {

                            foreach (RegulonGraph graph in column) {

                                // For each target gene in the graph...
                                foreach (GeneNode node in graph.Nodes) {

                                    // If the important identifiers are the same,
                                    // assume that these are the same gene and check
                                    // if the actual objects are the same
                                    if (node.Gene.RegulonId == selectedGene.RegulonId && node.Gene.VimssId == selectedGene.VimssId && node.Gene.Name == selectedGene.Name && node.Gene.LocusTag == selectedGene.LocusTag) {

                                        if (node.Gene != selectedGene) {
                                            if (Constants.ShowDebugMessages) Console.Log(String.Format("Homolog selection is not working properly - activating bug handler (desync on {0} in {1})", node.Gene.Name, graph.GraphLabelText));
                                            requireSelectionBugHandler = true;

                                            // Mark that a desync was found
                                            desyncFound = true;
                                            break;
                                        } //else {
                                            //Console.Log(String.Format("Homolog selection is working properly (first target gene in the data set is the same object as {0} in {1})", node.Gene.Name, graph.GraphLabelText));
                                            //requireSelectionBugHandler = false;
                                        //}
                                    }
                                }

                                // Don't look at any more graphs if a desync
                                // was found
                                if (desyncFound) {
                                    break;
                                }

                            }

                            // Don't look at any more columns if a desync was
                            // found
                            if (desyncFound) {
                                break;
                            }
                        }

                        // Don't look at any more selected genes if a desync
                        // was found
                        if (desyncFound) {
                            break;
                        }
                    }

                    checkForSelectionBug = false;

                    foreach (GeneInfo selectedGene in SelectedGenes) {
                        SelectionBugHandler(selectedGene);
                    }
                }
#endif*/

                // Notify listeners that this display has finished processing
                // so the loading overlay can be hidden
                isProcessing = false;
                NotifyPropertyChanged("IsProcessing");
            }, 10, null);
        }

        /// <summary>
        /// Runs when a property on a regulon graph is changed. Used for checking if regulon graphs have been selected
        /// - in that case, adds or removes that regulon graph from the list of selected regulons
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="args">Event arguments</param>
        private void regulonGraph_PropertyChanged(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs args)
        {

            // If a regulon graph's selection has been changed
            if (args.Includes("IsSelected")) {

                RegulonInfo regulon = (sender as RegulonInfo);
                int index = selectedRegulog.Regulons.IndexOf(regulon);

                // Add or remove the regulon graph from the list of selected
                // regulons based on its selection status
                if (regulon.IsSelected) {
                    if (!selectedRegulons.Contains(index)) {
                        selectedRegulons.Add(index);
                    }
                } else {
                    selectedRegulons.Remove(index);
                }
            }
        }

        /// <summary> Updates the visible regulons in the associated container,
        ///		applying a filter and sorting function to them
        /// </summary>
        /// <param name="sort">Function for determining the method of sorting regulons</param>
        /// <param name="filter">Function for determining which regulons should be filtered</param>
        public void UpdateVisibleRegulons(
            Func<RegulonInfo, RegulonInfo, int> sort,
            Func<RegulonInfo, bool> filter
        )
        {
            // If no sorting function was specified, sort using the already
            // stored order
            if (sort == null) {
                sort = delegate( RegulonInfo x, RegulonInfo y ) { return x.Order - y.Order; };
            }

            if (detailedViewGraphs == null) {
                detailedViewGraphs = new List<List<RegulonGraph>>();
            } else {
                detailedViewGraphs.Clear();
            }

            // Keeping track of how many graphs are visible
            int tempNumberOfGraphs = 0;

            // Go through each column in the display one by one
            // The comparison operator is <= because it's possible that this
            // display includes a palette area
            for (int i = 0; i <= columns.Length; i++)
            {
                if (!detailViewAllGraphs) {
                    if (detailedViewGraphs[i] == null) {
                        detailedViewGraphs[i] = new List<RegulonGraph>();
                    }
                }

                // The list of regulons that will be displayed
                List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

                // The regulons that will not be visible (to set their order
                // property correctly)
			    List<RegulonGraph> invisibleGraphs = new List<RegulonGraph>();

                // Go through all the regulon graphs in the current column and
                // check them against the filter
                for (int j = 0; j < regulonGraphs[i].Count; j++)
                {
                    RegulonGraph graph = regulonGraphs[i][j];

                    // Only add the regulon to the list if it meets the conditions
                    // of the filter
                    if (filter == null || filter(graph.Regulon))
                    {
                        visibleGraphs.Add(graph);
                        if (!detailViewAllGraphs) detailedViewGraphs[i].Add( graph );
                    } else {
                        invisibleGraphs.Add( graph );
                    }
                }

                // Sort the visible regulons based on the sorting function
                visibleGraphs.Sort(delegate(RegulonGraph x, RegulonGraph y) { return sort(x.Regulon, y.Regulon); });

                // Remove the existing regulon graphs from the display
                foreach (RegulonGraph graph in regulonGraphs[i])
                {
                    graph.RemoveElement();
                }

                // Add the regulon graphs that will be visible to the display
                foreach (RegulonGraph graph in visibleGraphs)
                {
                    // If in the first sublist of regulon graphs, place them in
                    // the palette area if it is enabled, else place them in
                    // the first column
                    if (i == 0) {
                        if (Constants.UseGroupPalette) {
                            graph.AddElementTo(palette);
                        } else {
                            graph.AddElementTo(columns[0]);
                        }

                    // Place the other regulon graphs in their columns as
                    // normal
                    } else {
                        graph.AddElementTo(columns[i-1]);
                    }
                }

                // Keep track of the number of visible graphs
                tempNumberOfGraphs += visibleGraphs.Length;

                // If there are any invisible regulons...
                if (invisibleGraphs.Length > 1) {
                    // Sort the invisible regulons based on their already stored order
                    invisibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return x.Regulon.Order - y.Regulon.Order; } );

                    // Set the invisible regulons' order to be their current order, but
                    // to be after the visible regulons
                    for (int j = 0; j < invisibleGraphs.Length; j++) {
                        invisibleGraphs[j].Regulon.Order = j + visibleGraphs.Length;
                    }
                }
            }

            // Set the visible number of graphs
            numberOfGraphs = tempNumberOfGraphs;

            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();
        }

        /// <summary>
        /// Finds a parent of the specified type for the given element
        /// </summary>
        /// <param name="element">The element to find a parent for</param>
        /// <param name="tagName">The tag name to look for</param>
        /// <returns>The parent element that fits the specified type, or null if none was found</returns>
        private static Element FindParentOfType(Element element, string tagName)
        {
            // While the element is not null, and does not have the same tag
            // name as was requested...
            while (element != null && String.Compare(element.TagName, tagName, true) != 0)
            {
                // Set the current element to its parent
                element = element.ParentNode;
            }

            // Return the parent element (or null if no suitable parent was
            // found)
            return element;
        }

        /// <summary> When the user selects a new number of groups, add or
        /// remove extra columns as required.
        /// </summary>
        /// <param name="numberOfGroups">The number of groups to change to</param>
        /// <param name="recalculateCentroids">Whether the centroids should be recalculated (usually true)</param>
        public void SetNumberOfGroups(int numberOfGroups, bool recalculateCentroids)
        {
            //Do nothing if the requested number of groups is the same
            if (columns.Length == numberOfGroups) return;

            // If this is a filtered detail view, restore everything first
            if (currentZoomLevel == ZoomLevels.DetailView && !detailViewAllGraphs) {
                //DetailView_RestoreGraphs();

                // Return this to the default
                detailViewAllGraphs = true;

                UpdateVisibleRegulons(null, null);
            }

            //If the requested number of groups is greater than the already
            //existing number of groups, add extra columns
            if (columns.Length < numberOfGroups)
            {
                // Add the palette if it is enabled in the constants and the
                // original number of groups was one
                if (Constants.UseGroupPalette && columns.Length == 1) {
                    InitialisePalette(container);
                }

                for (int i = columns.Length; i < numberOfGroups; i++)
                {
                    DivElement newDiv = (DivElement)Document.CreateElement("div");

                    // Set the attributes
                    newDiv.ID = "column_" + (i + 1);
                    /*//newDiv.Style.CssFloat = "left";
                    newDiv.Style.BackgroundColor = "white";
                    newDiv.Style.Margin = "0px";
                    newDiv.Style.Border = "1px dashed black";*/
                    newDiv.Style.MinWidth = Constants.RegulonDisplayWidth + Constants.RegulonDisplayUnits;
                    /*newDiv.Style.Display = "table-cell";*/
                    newDiv.ClassName = "GraphGroupColumn";

                    // Add the group container to the main container
                    container.AppendChild(newDiv);
                    columns.Add(newDiv);

                    // Add a new list for the graphs in this group
                    regulonGraphs.Add(new List<RegulonGraph>());
                }
            }

            //If the requested number of groups is less than the already
            //existing number of groups, move the graphs in those groups to the
            //palette and then remove the extra columns
            if (columns.Length > numberOfGroups)
            {
                //For each column that is greater than the requested number of
                //groups
                for (int i = numberOfGroups+1; i <= columns.Length; i++)
                {
                    //For each graph in that column
                    for (int j = 0; j < regulonGraphs[i].Count; j++)
                    {
                        RegulonGraph currentRegulonGraph = regulonGraphs[i][j];

                        //Remove it from its current column and add it to the
                        //palette (and first list of graphs)
                        currentRegulonGraph.RemoveElement();
                        if (Constants.UseGroupPalette) {
                            currentRegulonGraph.AddElementTo(palette);
                            regulonGraphs[0].Add(currentRegulonGraph);
                            SetGraphZoomLevel(ZoomLevels.IconList, 0, regulonGraphs[0].Length-1);

                            // Set the regulon's current group number
                            currentRegulonGraph.Regulon.GroupNumber = 0;
                        } else {
                            currentRegulonGraph.AddElementTo(columns[0]);
                            regulonGraphs[1].Add(currentRegulonGraph);

                            // Set the regulon's current group number
                            currentRegulonGraph.Regulon.GroupNumber = 1;
                        }
                    }

                    //Remove all of the leftover graphs from the current list
                    //of graphs
                    while (regulonGraphs[i][0] != null)
                    {
                        regulonGraphs[i].RemoveAt(0);
                    }

                    //Remove that column
                    container.RemoveChild(columns[i-1]);
                }

                //Remove the extra columns and lists of graphs
                columns.RemoveRange(numberOfGroups, columns.Length - numberOfGroups);
                regulonGraphs.RemoveRange(numberOfGroups + 1, regulonGraphs.Length - numberOfGroups +1);

                // Also remove the palette if moving to one group
                if (Constants.UseGroupPalette && numberOfGroups == 1) {

                    // For each graph in the palette, move it back to the only
                    // group
                    for (int j = 0; j < regulonGraphs[0].Count; j++)
                    {
                        RegulonGraph currentRegulonGraph = regulonGraphs[0][j];

                        //Remove it from its current column and add it to the
                        //palette (and first list of graphs)
                        currentRegulonGraph.RemoveElement();
                        currentRegulonGraph.AddElementTo(columns[0]);
                        regulonGraphs[1].Add(currentRegulonGraph);

                        // Set the regulon's current group number
                        currentRegulonGraph.Regulon.GroupNumber = 1;

                        // Reset the zoom
                        SetGraphZoomLevel(currentZoomLevel, 1, regulonGraphs[1].Length-1);
                    }

                    //Remove all of the leftover graphs from the current list
                    //of graphs
                    while (regulonGraphs[0][0] != null)
                    {
                        regulonGraphs[0].RemoveAt(0);
                    }

                    container.RemoveChild(palette);
                    palette = null;
                }
            }

            //Make sure the columns have the correct dimensions
            UpdateDimensionsOfColumns();

            //Actually update the number of groups for later reference
            this.numberOfGroups = numberOfGroups;

            // Store the number of groups in the regulog
            selectedRegulog.NumGroups = this.numberOfGroups;

            // Make sure the first group has the right border
            if (numberOfGroups == 1) {
                columns[0].ClassName = String.Empty;
            } else {
                columns[0].ClassName = "GraphGroupColumn";
            }

            // Recalculate the centroids if specified
            if (recalculateCentroids && autoCalculateCentroidOnDrag) {
                CalculateAllCentroids((List<RegulonInfo>)selectedRegulog.Regulons);
            }

            // Ensure the stored order for each visible graph is correct
            RefreshGraphOrder();
        }

        /// <summary>
        /// Updates the height and width of the column containers so that they
        /// are a suitable size
        /// </summary>
        private void UpdateDimensionsOfColumns()
        {

            //Reset the heights to auto first so we can find the column which
            //has the largest height - i.e. the column with the most boxes in
            //it. Also, set the correct percentage of width depending on how
            //many columns there are
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i].Style.Height = "auto";
                columns[i].Style.Width = 100 / columns.Length + "%";
            }

            //Find the one with the largest height
            /*for (int i = 0; i < columns.Length; i++)
            {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("currentMaxColumnHeight == " + currentMaxColumnHeight + ", columns[" + i + "].ClientHeight == " + columns[i].ClientHeight);
#endif
                currentMaxColumnHeight = (Math.Max(columns[i].ClientHeight, currentMaxColumnHeight));

                //If the columns are empty and have disappeared, we need to use
                //a special value to indicate this
                if (currentMaxColumnHeight == 0)
                {
                    currentMaxColumnHeight = -1;
                }
            }

            //Set the new heights
            for (int i = 0; i < columns.Length; i++)
            {
                if (currentMaxColumnHeight == -1)
                {
                    columns[i].Style.Height = Math.Max(minGraphSize, container.ParentNode.ClientHeight) + "px";
                }
                else
                {
                    columns[i].Style.Height = Math.Max(currentMaxColumnHeight, container.ParentNode.ClientHeight) + "px";
                }
            }*/
        }

        //// <summary> For zooming, to ensure we don't get odd results
        //// </summary>

        //private double minGraphSize = 125;

        /// <summary>
        /// Ensures that elements of the graphs do not change size when the
        /// display is zoomed in or out
        /// </summary>
        /// <param name="graph">The graph that is changing size</param>
        /// <param name="newWidth">The new width of the graph</param>
        /// <param name="newHeight">The new height of the graph</param>
        private void SetGraphSize(RegulonGraph graph, double newWidth, double newHeight)
        {

            // Calculate the factor that will be used to change the size
            double factor = newHeight / Double.Parse(graph.GraphSvg.GetAttribute<string>("height"));

            // The window size threshold indicates when graph elements should
            // gain additional size when the display is big enough, or vice versa
            if (windowSizeThresholdPassed) {
                if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                    factor = factor / Constants.LargeScaleNodeScaleFactor;
                } else {
                    factor = Constants.LargeScaleNodeScaleFactor * factor;
                }
            }

            // Set the graph's SVG container to the new width and height
            graph.GraphSvg.SetAttribute("width", newWidth + Constants.RegulonDisplayUnits);
            graph.GraphSvg.SetAttribute("height", newHeight + Constants.RegulonDisplayUnits);

            // For every node in the graph, change their size by the calculated
            // factor
            foreach (AbstractNode node in graph.Nodes) {

                // Each different shape of node requires different handling
                // For circular nodes
                if (node.Node is SvgCircle) {
                    (node.Node as SvgCircle).Radius = (node.Node as SvgCircle).Radius / factor;
                }

                // For rectangular nodes
                if (node.Node is SvgRectangle) {
                    SvgRectangle rectangle = (node.Node as SvgRectangle);
                    rectangle.Height = rectangle.Height / factor;
                    rectangle.Width = rectangle.Width / factor;
                    rectangle.X = rectangle.X / factor;
                    rectangle.Y = rectangle.Y / factor;
                }

                // For polygonal nodes
                if (node.Node is SvgPolygon) {
                    SvgPolygon polygon = (node.Node as SvgPolygon);
                    polygon.Height = polygon.Height / factor;
                    polygon.Width = polygon.Width / factor;
                }

                // Scale the width of lines/spokes for target genes
                if (node is GeneNode) {

                    // See above comment for window size threshold
                    if (windowSizeThresholdPassed) {
                        if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                            (node as GeneNode).Link.StrokeWidth = (node as GeneNode).Link.StrokeWidth / (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                        } else {
                            (node as GeneNode).Link.StrokeWidth = (node as GeneNode).Link.StrokeWidth * (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                        }
                    }
                }
                //node.Node.StrokeWidth = node.Node.StrokeWidth / factor;

                // See above comment for window size threshold
                if (windowSizeThresholdPassed) {
                    if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                        node.Node.StrokeWidth = node.Node.StrokeWidth / (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                    } else {
                        node.Node.StrokeWidth = node.Node.StrokeWidth * (Constants.LargeScaleNodeScaleFactor / Constants.LargeScaleGraphScaleFactor);
                    }
                }
            }

            // Scale each regulator in the graph as well by the calculated
            // factor
            foreach (RegulatorNode node in graph.RegulatorNodes) {

                // Each different shape of node requires different handling
                // For circular nodes
                if (node.Node is SvgCircle) {
                    (node.Node as SvgCircle).Radius = (node.Node as SvgCircle).Radius / factor;
                }

                // For rectangular nodes
                if (node.Node is SvgRectangle) {
                    SvgRectangle rectangle = (node.Node as SvgRectangle);
                    rectangle.Height = rectangle.Height / factor;
                    rectangle.Width = rectangle.Width / factor;
                    rectangle.X = rectangle.X / factor;
                    rectangle.Y = rectangle.Y / factor;
                }

                // For polygonal nodes
                if (node.Node is SvgPolygon) {
                    SvgPolygon polygon = (node.Node as SvgPolygon);
                    polygon.Height = polygon.Height / factor;
                    polygon.Width = polygon.Width / factor;
                }
            }
        }

        /// <summary>
        /// Deselects all of the selected genes in the display
        /// </summary>
        /// <param name="allowOverlay">Whether to use a time out to allow the loading overlay to appear</param>
        public void ClearSelectedGenes(bool allowOverlay) {

            if (selectedGenes.Length > 0) {

            if (allowOverlay) {

            // Notify listeners that this display is currently
            // processing so the loading overlay can be displayed
            isProcessing = true;
            NotifyPropertyChanged("IsProcessing");

            // Use a timeout delegate so that the loading overlay can appear
            Script.SetTimeout((Action) delegate {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Clearing all selected genes - starting with " + selectedGenes.Length + " in selectedGenes list");
#endif
                // Clear the selected status of each gene in the selected list
                // and then clear the list
                foreach (string index in selectedGenes) {

                    // Get the index of the regulon and the gene by splitting
                    // the identifier in the selected list
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Set the gene that corresponds to the retrieved indexes
                    // as unselected
                    if (selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]] != null) {
                        selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = false;
                    }
/*#if DEBUG
                    if (selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].Gene["annotation"] != null) {
                        if (Constants.ShowDebugMessages) Console.Log("Currently setting gene " + selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].Name + " (regulon index "  + regulonIndex + ", gene index " + geneIndex + ")  with annotation '" + selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].Gene["annotation"] + "' as unselected");
                    } else {
                        if (Constants.ShowDebugMessages) Console.Log("Currently setting gene " + selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].Name + " (regulon index "  + regulonIndex + ", gene index " + geneIndex + ") which has no annotation as unselected");
                    }
#endif*/
#if DEBUG
                    // Use the bug selection handler if required
                    if (requireSelectionBugHandler) {
                        GeneInfo currentGene = selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]];
                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Activating selection bug handler for gene {0} in RegulonID {1}", currentGene.Name, currentGene.RegulonId));
                        SelectionBugHandler(currentGene);
                    }
#endif
                }

                // Clear the list of selected genes
                selectedGenes.Clear();
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Finished - " + selectedGenes.Length + " genes now in selectedGenes list");
#endif
                // Notify listeners that this display has finished processing
                // so the loading overlay can be hidden
                isProcessing = false;
                NotifyPropertyChanged("IsProcessing");
            }, 10, null);

            } else {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Clearing all selected genes - starting with " + selectedGenes.Length + " in selectedGenes list");
#endif
                // Clear the selected status of each gene in the selected list
                // and then clear the list
                foreach (string index in selectedGenes) {

                    // Get the index of the regulon and the gene by splitting
                    // the identifier in the selected list
                    string[] temp = index.Split(",");
                    int regulonIndex = int.Parse(temp[0]);
                    int geneIndex = int.Parse(temp[1]);

                    // Set the gene that corresponds to the retrieved indexes
                    // as unselected
                    if (selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]] != null) {
                        selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].IsSelected = false;
                    }
/*#if DEBUG
                    if (selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].Gene["annotation"] != null) {
                        if (Constants.ShowDebugMessages) Console.Log("Currently setting gene " + selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].Name + " (regulon index "  + regulonIndex + ", gene index " + geneIndex + ")  with annotation '" + selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].Gene["annotation"] + "' as unselected");
                    } else {
                        if (Constants.ShowDebugMessages) Console.Log("Currently setting gene " + selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]].Name + " (regulon index "  + regulonIndex + ", gene index " + geneIndex + ") which has no annotation as unselected");
                    }
#endif*/
#if DEBUG
                    // Use the bug selection handler if required
                    if (requireSelectionBugHandler) {
                        GeneInfo currentGene = selectedRegulog.Regulons[regulonIndex].Genes[geneIndexes[regulonIndex][geneIndex]];
                        if (Constants.ShowDebugMessages) Console.Log(String.Format("Activating selection bug handler for gene {0} in RegulonID {1}", currentGene.Name, currentGene.RegulonId));
                        SelectionBugHandler(currentGene);
                    }
#endif
                }

                // Clear the list of selected genes
                selectedGenes.Clear();
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Finished - " + selectedGenes.Length + " genes now in selectedGenes list");
#endif
            }

            }
        }

        /// <summary>
        /// Deselects all of the selected regulons in the display
        /// </summary>
        public void ClearSelectedRegulons() {

            // Go through each column of regulon graphs and set them as not
            // selected
            foreach (List<RegulonGraph> graphs in regulonGraphs) {
                foreach (RegulonGraph graph in graphs) {
                    if (graph.IsSelected) {
                        graph.IsSelected = false;

                        // Ensure the graph has the correct visual class
                        SetGraphClass(graph, false, false);
                    }
                }
            }

            // Clear the list of selected regulons
            selectedRegulons.Clear();
        }

        /// <summary>
        /// Whether the detail view should be showing all graphs (true) or a filtered set of graphs based on the selection (false)
        /// </summary>
        private bool detailViewAllGraphs = true;

        /// <summary>
        /// Used to store the current order of all the graphs in the display
        /// while a filtered detail view is visible, so that they can be
        /// restored when switching out of that zoom level
        /// </summary>
        private int[][] graphOrder;

        /// <summary>
        /// A list of the graphs that are currently visible in a filtered detail view
        /// </summary>
        private List<List<RegulonGraph>> detailedViewGraphs;

        /// <summary>
        /// Gets the list of graphs that are currently visible in the detail view
        /// <para>This is used to set the comparison when in a filtered detail view and none of the regulons are currently selected</para>
        /// </summary>
        public List<List<RegulonGraph>> DetailedViewGraphs
        {
            get { return detailedViewGraphs; }
        }

        /// <summary>
        /// Filters the display to show only selected graphs, while preserving the original order
        /// </summary>
        /*private void DetailView_FilterGraphs()
        {
            // Used to keep track of the un-filtered graph
            // order
            graphOrder = new int[regulonGraphs.Length][];
            detailedViewGraphs = new List<List<RegulonGraph>>();
            int i = Constants.UseGroupPalette ? 0 : 1;

            // For all the columns in the display...
            for (; i < regulonGraphs.Length; i++)
            {
                graphOrder[i] = i == 0 ? new int[palette.ChildNodes.Length] : new int[columns[i - 1].ChildNodes.Length];

                Element currentChild = i == 0 ? palette.FirstChild : columns[i - 1].FirstChild;
                int index = 0;

                // Check each graph in the display and store its
                // order
                while (currentChild != null)
                {
                    foreach (RegulonGraph graph in regulonGraphs[i])
                    {
                        if (currentChild == graph.DomElement)
                        {
                            graphOrder[i][index] = regulonGraphs[i].IndexOf(graph);
                            break;
                        }
                        else
                        {
                            graphOrder[i][index] = -1;
                        }
                    }
                    currentChild = currentChild.NextSibling;
                    index++;
                }

                // Add all selected networks to a list
                // (This is divided into columns as originally
                // filtering kept the graphs in their columns)
                detailedViewGraphs[i] = new List<RegulonGraph>();
                foreach (RegulonGraph graph in regulonGraphs[i])
                {
                    if (graph.Regulon.IsSelected)
                    {
                        detailedViewGraphs[i].Add(graph);
                    }
                }

                // Remove all current regulon graphs from the
                // display
                foreach (RegulonGraph graph in regulonGraphs[i])
                {
                    graph.RemoveElement();
                }

                // Add only the selected graphs back into the display
                // This ignores columns and goes straight to
                // the main container
                foreach (RegulonGraph graph in detailedViewGraphs[i])
                {
                    graph.AddElementTo(container);
                }

                // Remove all the column containers from the display
                if (i == 0)
                {
                    container.RemoveChild(palette);
                }
                else
                {
                    container.RemoveChild(columns[i - 1]);
                }
            }
        }

        /// <summary>
        /// Undoes selected graph filtering, restoring all graphs to correct columns and order
        /// </summary>
        private void DetailView_RestoreGraphs()
        {
            int i = Constants.UseGroupPalette ? 0 : 1;
            for (; i < regulonGraphs.Length; i++)
            {

                // Remove all regulon graphs currently in the display
                // that were in the current column
                foreach (RegulonGraph graph in regulonGraphs[i])
                {
                    graph.RemoveElement();
                }

                // Make a list of all the graphs in the display in
                // their previous order for this column
                List<RegulonGraph> orderedGraphs = new List<RegulonGraph>();
                for (int j = 0; j < graphOrder[i].Length; j++)
                {
                    if (graphOrder[i][j] != -1)
                    {
                        orderedGraphs.Add(regulonGraphs[i][graphOrder[i][j]]);
                    }
                }

                // Add all the networks in that list to the display
                // in the appropriate column
                foreach (RegulonGraph graph in orderedGraphs)
                {

                    // This is required in case there was an empty column
                    if (graph != null)
                    {
                        if (i == 0)
                        {
                            graph.AddElementTo(palette);
                        }
                        else
                        {
                            graph.AddElementTo(columns[i - 1]);
                        }
                    }
                }

                // Add all the columns back to the display
                if (i == 0)
                {
                    container.AppendChild(palette);
                }
                else
                {
                    container.AppendChild(columns[i - 1]);
                }
            }

            // Clear the list of graphs in the detail view
            detailedViewGraphs.Clear();
        }*/

        /// <summary>
        /// Calls SetGraphAttributes for a graph when the zoom level is changed, depending on what zoom level was specified
        /// </summary>
        /// <param name="zoomLevel">The zoom level to change to, using the ZoomLevels enum</param>
        /// <param name="containerIndex">The index of the column the graph is in</param>
        /// <param name="graphIndex">The index of the graph in the list of regulon graphs</param>
        private void SetGraphZoomLevel(ZoomLevels zoomLevel, int containerIndex, int graphIndex)
        {
            switch (zoomLevel)
            {
                // Icon list specifies a box 150% the width of the default
                // small overview and 33% the height. The network is also moved
                // to the left of the box and the label to the right
                case ZoomLevels.IconList:
                    SetGraphAttributes(containerIndex, graphIndex, 1.5, (1/3), TextAnchorType.start, 0.75, 0, 100, new SvgRect(190, -100, 200, 200), false);
                    break;

                // Small overview is the default
                case ZoomLevels.SmallOverview:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(containerIndex, graphIndex, 1, 1, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(containerIndex, graphIndex, 1, 1, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;

                // Large overview is 150% larger than the default small
                // overview
                case ZoomLevels.LargeOverview:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(containerIndex, graphIndex, 1.5, 1.5, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(containerIndex, graphIndex, 1.5, 1.5, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;

                // Detail view is 375% larger than the default small
                // overview
                case ZoomLevels.DetailView:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(containerIndex, graphIndex, 3.75, 3.75, TextAnchorType.middle, 0.3, 120, 0, new SvgRect(-125, -108.75, 250, 250), true);
                    } else {
                        SetGraphAttributes(containerIndex, graphIndex, 3.75, 3.75, TextAnchorType.middle, 0.3, 100, 0, new SvgRect(-125, -125, 250, 250), true);
                    }
                    break;

                // If somehow an invalid zoom level is passed, use the default
                // small overview parameters
                default:
                    if (Constants.UseGraphLabelWrapping) {
                        SetGraphAttributes(containerIndex, graphIndex, 1, 1, TextAnchorType.middle, 0.3, 102, 0, new SvgRect(-100, -87, 200, 200), false);
                    } else {
                        SetGraphAttributes(containerIndex, graphIndex, 1, 1, TextAnchorType.middle, 0.3, 85, 0, new SvgRect(-100, -100, 200, 200), false);
                    }
                    break;
            }
        }

        /// <summary>
        /// Sets certain attributes of a graph
        /// </summary>
        /// <param name="containerIndex">The index of the column the graph is in</param>
        /// <param name="graphIndex">The index of the graph in the list of regulon graphs</param>
        /// <param name="widthFactor">The multiplier that will be applied to the default graph width</param>
        /// <param name="heightFactor">The multiplier that will be applied to the default graph height</param>
        /// <param name="textAnchor">The alignment of the label text</param>
        /// <param name="textFactor">The multiplier that will be applied to the default font size of the label</param>
        /// <param name="labelY">The Y position of hte label</param>
        /// <param name="labelX">The X position of the label</param>
        /// <param name="newViewBox">The new SVG rectangle that with act as the graph's view box</param>
        /// <param name="nodeLabelVisibility">The visibility of the labels for each target gene node</param>
        private void SetGraphAttributes(int containerIndex, int graphIndex, double widthFactor, double heightFactor, TextAnchorType textAnchor, double textFactor, double labelY, double labelX, SvgRect newViewBox, bool nodeLabelVisibility)
        {
            // Set the size of the graph's elements. This also applies an
            // additional multiplier to the width and height if the window size
            // is larger than a certain value
            if (Window.OuterWidth > Constants.LargeScaleWidthThreshold || Window.OuterHeight > Constants.LargeScaleHeightThreshold) {
                SetGraphSize(regulonGraphs[containerIndex][graphIndex], Constants.RegulonDisplayWidth * widthFactor * Constants.LargeScaleGraphScaleFactor, Constants.RegulonDisplayHeight * heightFactor * Constants.LargeScaleGraphScaleFactor);
            } else {
                SetGraphSize(regulonGraphs[containerIndex][graphIndex], Constants.RegulonDisplayWidth * widthFactor, Constants.RegulonDisplayHeight * heightFactor);
            }

            // Set the label's alignment
            regulonGraphs[containerIndex][graphIndex].GraphLabel.TextAnchor = textAnchor;

            // Set the label's font size
            if (Constants.UseGraphLabelWrapping) {

                // If the text in the label is too long, shrink the font size
                // depending on its length so that it is wholly visible
                int textLength = regulonGraphs[containerIndex][graphIndex].GraphLabel.Text.Length;
                regulonGraphs[containerIndex][graphIndex].GraphLabel.FontSize = textLength > 32 ? (Svg.ConvertLength(textFactor / 2.54 / (textLength / 32), Constants.RegulonDisplayHeight, 200)).ToString() : (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();
            } else {
                regulonGraphs[containerIndex][graphIndex].GraphLabel.FontSize = (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();
            }

            // Set the X and Y coordinates of the label
            regulonGraphs[containerIndex][graphIndex].GraphLabel.Y = labelY;
            regulonGraphs[containerIndex][graphIndex].GraphLabel.X = labelX;

            // Do the same for the second line of the label if label text
            // wrapping is enabled
            if (Constants.UseGraphLabelWrapping) {

                // Set the label's alignment
                regulonGraphs[containerIndex][graphIndex].GraphLabelLineTwo.TextAnchor = textAnchor;

                // Set the label's font size
                // If the text in the label is too long, shrink the font size
                // depending on its length so that it is wholly visible
                int textLength2 = regulonGraphs[containerIndex][graphIndex].GraphLabelLineTwo.Text.Length;
                regulonGraphs[containerIndex][graphIndex].GraphLabelLineTwo.FontSize = textLength2 > 32 ? (Svg.ConvertLength(textFactor / 2.54 / (textLength2 / 32), Constants.RegulonDisplayHeight, 200)).ToString() : (Svg.ConvertLength(textFactor / 2.54, Constants.RegulonDisplayHeight, 200)).ToString();

                // Set the X and Y coordinates of the label
                regulonGraphs[containerIndex][graphIndex].GraphLabelLineTwo.Y = labelY;
                regulonGraphs[containerIndex][graphIndex].GraphLabelLineTwo.X = labelX;
            }

            // Set the graph's view box rectangle
            regulonGraphs[containerIndex][graphIndex].GraphSvg.ViewBox = newViewBox;

            // Set the graph's node label visibility
            regulonGraphs[containerIndex][graphIndex].SetNodeLabelVisibility(nodeLabelVisibility);

            // Set the position of the label's backgroud and its visibility (it
            // should not appear if the label is not centered - this is used
            // for the icon list)
            /*regulonGraphs[containerIndex][graphIndex].GraphLabelBackground.Y = labelY + (double.Parse(regulonGraphs[containerIndex][graphIndex].GraphLabel.FontSize) * 0.75);
            if (labelX != 0) {
                regulonGraphs[containerIndex][graphIndex].GraphLabelBackground.Visibility = "hidden";
            } else {
                regulonGraphs[containerIndex][graphIndex].GraphLabelBackground.Visibility = "visible";
            }*/
        }

/*#if DEBUG
        private void DebugCheckSelectedList()
        {
            if (Constants.ShowDebugMessages) Console.Log("Checking the initial selected list for this display");
            if (selectedGenes.Length == 0)
            {
                if (Constants.ShowDebugMessages) Console.Log("Nothing is selected!");
            }
            else
            {
                foreach (GeneInfo gene in selectedGenes)
                {
                    if (Constants.ShowDebugMessages) Console.Log(gene.Name + " in Regulon ID " + gene.RegulonId + " is selected.");
                }
                if (Constants.ShowDebugMessages) Console.Log(selectedGenes.Length + " nodes in total are selected.");
            }
        }
#endif*/

/*#if DEBUG
        private void DaviesBoudinTesting(RegulogInfo selectedRegulog, double[][] distances)
        {
            // Testing for the Davies-Boudin index calculations. This is based
            // on the definition as I understand it from here:
            // https://en.wikipedia.org/wiki/Davies%E2%80%93Bouldin_index#Definition
            if (Constants.ShowDebugMessages) Console.Log("Testing for Davies-Boudin index");

            // Contains all of the assignments for each network in each group
            // across all tests
            List<Dictionary<int, int>> testAssignments = new List<Dictionary<int, int>>();

            // Contains all of the centroids for each group across all tests
            List<List<int>> testCentroids = new List<List<int>>();

            // Contains the actual contents of each group across all tests
            List<List<List<int>>> testGroups = new List<List<List<int>>>();

            // Contains the average cluster scatter of each group across all
            // tests
            List<List<double>> testAvgGroupScatter = new List<List<double>>();

            // Contains all of the calculated Davies-Boudin indexes
            //Dictionary<int, double> testDBIndexes = new Dictionary<int,double>();

            // Since we want to find the "best" number of groups, we need to
            // create groups for all the possible amounts of groups (2 to 5).
            // The check for the amount of graphs is so we don't try to make
            // more groups then there are graphs
            for (int i = 2; i < 6 && i < selectedRegulog.Regulons.Length; i++)
            {
                if (Constants.ShowDebugMessages) Console.Log("Number of groups: " + i);

                // Initialise all the lists for this number of groups
                testAssignments[i - 1] = new Dictionary<int, int>();
                testAssignments[i - 1] = KMeansGroupAssignment((List<RegulonInfo>)selectedRegulog.Regulons, distances, testAssignments[i - 1], i, ratio);
                testCentroids[i - 1] = new List<int>();
                testGroups[i - 1] = new List<List<int>>();
                testAvgGroupScatter[i - 1] = new List<double>();

                // Initialise the group contents list for each group
                for (int j = 0; j < i; j++)
                {
                    testGroups[i - 1][j] = new List<int>();
                }

                // Find the centroids for each group and put them in the list
                // of centroids, as well as assigning each network to the
                // correct group list
                foreach (KeyValuePair<int, int> pair in testAssignments[i - 1])
                {
                    if (!testCentroids[i - 1].Contains(pair.Value))
                    {
                        testCentroids[i - 1].Add(pair.Value);
                    }
                    if (Constants.ShowDebugMessages) Console.Log(selectedRegulog.Regulons[pair.Key].GenomeName + " is in group " + testCentroids[i - 1].IndexOf(pair.Value));

                    // For some reason it was choking on build so I had to use
                    // these temporary variables :S
                    List<List<int>> currentGroupSet = testGroups[i - 1];

                    //Because JavaScript treats keys in pairs as strings, we
                    //need to have this ugly conversion
                    currentGroupSet[testCentroids[i - 1].IndexOf(pair.Value)].Add(int.Parse(pair.Key.ToString()));
                    //testGroups[i-1][testCentroids[i-1].IndexOf(pair.Value)].Add(int.Parse(pair.Key.ToString()));
                }

                // Calculate the average scatter for all groups. This is used
                // as part of the calculation of Ri,j in the definition (Si and
                // Sj)
                // I'm not actually sure what the "within cluster scatter" is
                // that is mentioned in the definition for the DB index. So I
                // made an assumption that it's the average difference from the
                // centroid for all other elements in the cluster
                foreach (List<int> cluster in testGroups[i - 1])
                {
                    if (Constants.ShowDebugMessages) Console.Log("Group " + testGroups[i - 1].IndexOf(cluster) + " has " + cluster.Length + " networks.");
                    double totalScatter = 0;
                    int centroid = -1;

                    // Find the centroid for this group
                    foreach (int currentCentroid in testCentroids[i - 1])
                    {
                        if (cluster.Contains(currentCentroid))
                        {
                            centroid = currentCentroid;
                            break;
                        }
                    }

                    // Add up the distances between the centroid and all of the
                    // other elements in this group
                    for (int j = 0; j < cluster.Length; j++)
                    {
                        if (centroid != cluster[j])
                        {
                            totalScatter = totalScatter + distances[centroid][cluster[j]];
                        }
                    }

                    // Divide by the amount of elements to get the average
                    testAvgGroupScatter[i - 1][testGroups[i - 1].IndexOf(cluster)] = totalScatter / cluster.Length;
                }

                // Find the centroid distance between all groups and all other
                // groups, and calculate Ri,j, Di and DB
                // We declare the variable for the final DB index here, so we
                // can add up the values of Mi,j and then divide them by the
                // total number of clusters
                double dBIndex = 0;

                // For every group in this set of groups
                foreach (List<int> ithCluster in testGroups[i - 1])
                {

                    // Store the maximum Ri,j calculation for this set of
                    // groups - (Si + Sj) / Mi,j
                    double maxRij = -1;

                    // Compare this group with every other group in the set of
                    // groups
                    foreach (List<int> jthCluster in testGroups[i - 1])
                    {
                        if (ithCluster != jthCluster)
                        {
                            if (Constants.ShowDebugMessages) Console.Log("Testing group " + testGroups[i-1].IndexOf(ithCluster) + " against group " + testGroups[i-1].IndexOf(jthCluster));

                            // These store the current smallest distance
                            double centroidDistance = distances[testCentroids[i-1][testGroups[i-1].IndexOf(ithCluster)]][testCentroids[i-1][testGroups[i-1].IndexOf(jthCluster)]];

                            if (Constants.ShowDebugMessages) Console.Log("Average cluster scatter for group i (" + testGroups[i-1].IndexOf(ithCluster) + ") is " + testAvgGroupScatter[i-1][testGroups[i-1].IndexOf(ithCluster)]);
                            if (Constants.ShowDebugMessages) Console.Log("Average cluster scatter for group j (" + testGroups[i-1].IndexOf(jthCluster) + ") is " + testAvgGroupScatter[i-1][testGroups[i-1].IndexOf(jthCluster)]);
                            if (Constants.ShowDebugMessages) Console.Log("Distance between the centroid of group i (" + testGroups[i-1].IndexOf(ithCluster) + ") and group j (" + testGroups[i-1].IndexOf(jthCluster) + ") is " + centroidDistance + " (between " + selectedRegulog.Regulons[testCentroids[i-1][testGroups[i-1].IndexOf(ithCluster)]].GenomeName + " and " + selectedRegulog.Regulons[testCentroids[i-1][testGroups[i-1].IndexOf(jthCluster)]].GenomeName + ")");

                            // Calculate Ri,j for these closest groups
                            double currentRij = (testAvgGroupScatter[i - 1][testGroups[i - 1].IndexOf(ithCluster)] + testAvgGroupScatter[i - 1][testGroups[i - 1].IndexOf(jthCluster)]) / centroidDistance;
                            if (Constants.ShowDebugMessages) Console.Log("Ri,j for this combination is " + currentRij);

                            // Di requires the maximum Ri,j, so check if our current one
                            // is it
                            if (currentRij > maxRij)
                            {
                                maxRij = currentRij;
                                if (Constants.ShowDebugMessages) Console.Log("This is the new maximum Ri,j for group i");
                            }
                        }
                    }
                    if (maxRij < 0)
                    {
                        if (Constants.ShowDebugMessages) Console.Log("Somehow, we did not get a max Ri,j for this group.");
                    }
                    else
                    {

                        // Add the max Ri,j (i.e. Di) to our DB value variable
                        // so we can use it later
                        dBIndex = dBIndex + maxRij;
                        if (Constants.ShowDebugMessages) Console.Log("Current sum of all Di is " + dBIndex);
                    }
                }
                // Divide the added up Di values to get the DB index
                dBIndex = 1 / dBIndex;
                if (Constants.ShowDebugMessages) Console.Log("Davies-Bouldin index for this set of groups (?) is " + dBIndex);
                //testDBIndexes[i] = dBIndex;
                if (Constants.ShowDebugMessages) Console.Log("Finished test with " + i + " groups.");
                if (Constants.ShowDebugMessages) Console.Log("------------");
            }
            if (Constants.ShowDebugMessages) Console.Log("Done.");
        }
#endif*/

        /// <summary>
        /// Extracts the distances for only the current group from the matrix
        /// of distances between all networks
        /// </summary>
        /// <param name="groupRegulons">The regulons contained in the current group</param>
        /// <returns>The matrix of distances for this specific group</returns>
        private double[][] GetGroupDistances(List<RegulonInfo> groupRegulons)
        {
            double[][] groupDistances = new double[groupRegulons.Length][];
            for (int i = 0; i < groupRegulons.Length; i++) {
                groupDistances[i] = new double[groupRegulons.Length];
                int iIndex = selectedRegulog.Regulons.IndexOf(groupRegulons[i]);
                for (int j = 0; j < groupRegulons.Length; j++) {
                    int jIndex = selectedRegulog.Regulons.IndexOf(groupRegulons[j]);
                    groupDistances[i][j] = currentDistances[iIndex][jIndex];
                }
            }
            return groupDistances;
        }

        /// <summary>
        /// Calculates the centroid of a group
        /// </summary>
        /// <param name="regulons">The list of regulons in the display</param>
        /// <param name="group">The indexes of the regulons in the current group</param>
        /// <returns>The id of the network that is the centroid</returns>
        private int CalculateCentroid(List<RegulonInfo> regulons, List<int> group)
        {
            //Get all the reguloninfos for each network in that group
            //and store it in a temporary list
            List<RegulonInfo> groupRegulons = new List<RegulonInfo>();
            for (int i = 0; i < regulons.Length; i++) {
                if (group.Contains(i)) {
                    groupRegulons.Add(regulons[i]);
                }
            }

            //Calculate the distances for this list of networks
            double[][] groupDistances = GetGroupDistances(groupRegulons);

            //Find out which network out of this group has the smallest
            //total of its distances. I am assuming this is the
            //"average" centroid
            double minDistanceTotal = double.MaxValue;
            int minDistanceTotalIndex = 0;
            for (int i = 0; i < groupDistances.Length; i++)
            {
                double total = 0;
                for (int j = 0; j < groupDistances[i].Length; j++)
                {
                    total = total + groupDistances[i][j];
                }
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Total of all distances for " + groupRegulons[k].GenomeName + " is " + total);
#endif*/
                if (total < minDistanceTotal)
                {
                    minDistanceTotal = total;
                    minDistanceTotalIndex = i;
/*#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("This is the new minimum for this group");
#endif*/
                }
            }

            //Set the new centroid of this group to be the one found
            //above
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("New centroid for group " + groups.IndexOf(group) + " will be " + groupRegulons[minDistanceTotalIndex].GenomeName);
#endif*/
            return regulons.IndexOf(groupRegulons[minDistanceTotalIndex]);
        }

        /// <summary>
        /// Sorts by the distance of each network to the centroid
        /// <para>This needs to be done here rather than passing a function from the RegulonGraphViewer because it needs to access stuff that is not exposed outside</para>
        /// </summary>
        /// <param name="filter"></param>
        public void UpdateVisibleRegulonsSortByCentroidDistance(Func<RegulonInfo, bool> filter)
        {
            // Keeping track of how many graphs are visible
            int tempNumberOfGraphs = 0;

            // If there are distances calculated for this regulog
            if (currentDistances != null) {

                // For each column in the display
                for (int i = 1; i < regulonGraphs.Length; i++)
                {
                    // The list of graphs which will be eventually sorted
                    List<RegulonGraph> visibleGraphs = new List<RegulonGraph>();

                    // The regulons that will not be visible (to set their order
                    // property correctly)
			        List<RegulonGraph> invisibleGraphs = new List<RegulonGraph>();

                    // The centroid of the current column
                    RegulonGraph groupCentroid = null;

                    // For each graph in the current column
                    foreach (RegulonGraph graph in regulonGraphs[i])
                    {

                        // Check if the current graph is the centroid and store
                        // it if it is
                        if (currentCentroids.Contains(selectedRegulog.Regulons.IndexOf(graph.Regulon))) {
                            groupCentroid = graph;
                        } else {

                            // Otherwise, add it to the list of visible graphs
                            // if it meets the conditions of the filter
                            if (filter == null || filter(graph.Regulon)) {
                                visibleGraphs.Add(graph);
                            } else {
                                invisibleGraphs.Add( graph );
                            }
                        }
                    }

                    // Sort the list of visible graphs in ascending order of
                    // their distance from the centroid
                    // If the distances between two networks are the same,
                    // order them by their name instead
                    visibleGraphs.Sort(delegate(RegulonGraph x, RegulonGraph y) {

                        // Get the difference in distance between the two networks
                        int compareResult = (int)((currentDistances[selectedRegulog.Regulons.IndexOf(x.Regulon)][selectedRegulog.Regulons.IndexOf(groupCentroid.Regulon)] -
                            currentDistances[selectedRegulog.Regulons.IndexOf(y.Regulon)][selectedRegulog.Regulons.IndexOf(groupCentroid.Regulon)]) * 100);

                        // If the result comes out to 0 (the networks have the
                        // same distance) then sort by name instead, otherwise
                        // return the above difference
                        if (compareResult == 0) {
                            return String.Compare(x.Regulon.GenomeName, y.Regulon.GenomeName);
                        } else {
                            return compareResult;
                        }
                    });

                    // Insert the centroid at the start of the list
                    if (groupCentroid != null && (filter == null || filter(groupCentroid.Regulon))) {
                        visibleGraphs.Insert(0, groupCentroid);
                    }

                    // Remove the existing regulon graphs from the column
                    foreach (RegulonGraph graph in regulonGraphs[i])
                    {
                        graph.RemoveElement();
                    }

                    // Add the regulon graphs that will be visible to the column
                    foreach (RegulonGraph graph in visibleGraphs)
                    {
                        if (i == 0) {
                            graph.AddElementTo(palette);
                        } else {
                            graph.AddElementTo(columns[i-1]);
                        }
                    }

                    // Keep track of the number of visible graphs
                    tempNumberOfGraphs += visibleGraphs.Length;

                    // If there are any invisible regulons...
                    if (invisibleGraphs.Length > 1) {
                        // Sort the invisible regulons based on their already stored order
                        invisibleGraphs.Sort( delegate( RegulonGraph x, RegulonGraph y ) { return x.Regulon.Order - y.Regulon.Order; } );

                        // Set the invisible regulons' order to be their current order, but
                        // to be after the visible regulons
                        for (int j = 0; j < invisibleGraphs.Length; j++) {
                            invisibleGraphs[j].Regulon.Order = j + visibleGraphs.Length;
                        }
                    }
                }

                // Set the visible number of graphs
                numberOfGraphs = tempNumberOfGraphs;

                // Ensure the stored order for each visible graph is correct
                RefreshGraphOrder();

            // If there are no distances, default to sorting by the already stored order
            } else {
                UpdateVisibleRegulons(delegate( RegulonInfo x, RegulonInfo y ) { return x.Order - y.Order; }, filter);
            }
        }

        /*private void Container_OnPress(JSObject ev)
        {
            Script.Literal("ev.preventDefault()");
            Element target = (Element)ev["target"];
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Container_OnPress triggered on " + target.ID);
#endif
            if (columns.Contains(target)) {
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("A press occured in the container for group " + (int)(columns.IndexOf((DivElement)target) + 1));
#endif
                //OnGroupPressed(new GroupPressedEventArgs(columns.IndexOf((DivElement)target) + 1));
                if (selectedRegulons.Length < 1) {
                    selectedRegulog.Regulons[currentCentroids[columns.IndexOf(target)]].IsSelected = true;
                    selectedRegulons.Add(selectedRegulog.Regulons[currentCentroids[columns.IndexOf(target)]]);
                }
                foreach (RegulonGraph graph in regulonGraphs[columns.IndexOf(target) + 1]) {
                    if (!selectedRegulons.Contains(graph.Regulon)) {
                        graph.Regulon.IsSelected = true;
                        selectedRegulons.Add(graph.Regulon);
                    }
                }
            }
        }*/

        //Used to check if the window size has passed the threshold for
        //increasing the size of regulon graphs
        private int currentWindowHeight = 0;
        private int currentWindowWidth = 0;
        private bool windowSizeThresholdPassed = false;

        /// <summary>
        /// If the browser window is resized, check if the display has passed
        /// the threshold for "large scale" and adjust the graph sizes if so
        /// </summary>
        /// <param name="e">The event that triggered this function</param>
        private void Window_OnResize(ElementEvent e)
        {
            // If the window's height or width has passed one of the thresholds
            if ((Window.OuterWidth > Constants.LargeScaleWidthThreshold && currentWindowWidth <= Constants.LargeScaleWidthThreshold) ||
                (Window.OuterWidth <= Constants.LargeScaleWidthThreshold && currentWindowWidth > Constants.LargeScaleWidthThreshold) ||
                (Window.OuterHeight > Constants.LargeScaleHeightThreshold && currentWindowHeight <= Constants.LargeScaleHeightThreshold) ||
                (Window.OuterHeight <= Constants.LargeScaleHeightThreshold && currentWindowHeight > Constants.LargeScaleHeightThreshold)) {

                // Set the threshold as being passed and then change the size
                // of the graphs
                windowSizeThresholdPassed = true;
                for (int i = 1; i < regulonGraphs.Length; i++) {
                    for (int j = 0; j < regulonGraphs[i].Length; j++) {
                        SetGraphZoomLevel(currentZoomLevel, i, j);
                    }
                }

                // Clear the status of passing the window size threshold
                windowSizeThresholdPassed = false;

                for (int i = 0; i < columns.Length; i++) {
                    columnLabels[i].Style.Left = 5 + (columns[i].OffsetLeft - columns[i].ScrollLeft + columns[i].ClientLeft) + "px";
                    columnLabels[i].Style.Top = 5 + (columns[i].OffsetTop - columns[i].ScrollTop + columns[i].ClientTop) + "px";
                }
            }

            // Store the current window height and width
            currentWindowWidth = Window.OuterWidth;
            currentWindowHeight = Window.OuterHeight;
        }

        /// <summary>
        /// Alerts listeners if the selected genes have changed
        /// </summary>
        private void OnSelectedGenesChanged() {
            if (SelectedGenesChanged != null) {
                SelectedGenesChanged(this, null);
            }
        }

#if DEBUG
        /// <summary> Returns the contents of a file as a string.
		/// </summary>
		/// <param name="fileName">The path (relative or absolute) of the file.</param>
		/// <param name="process"> A delegate that will be invoked with the content of the file in the event of a successful call, or with null otherwise. </param>
		/// <returns>Returns the content of the file as a string.</returns>

		public static void ReadAllText ( string fileName, Action<string> process ) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.Open( HttpVerb.Get, fileName, false );
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						process( req.ResponseText );
					}
					else {
						if (Constants.ShowDebugMessages) Console.Log( string.Format( "ReadAllText( \"{0}\" )", fileName ) );
						process( null );
					}
				}
			};
            Script.Literal("req.onloadend = function(event) { if (req.status == 404) {console.log('No document retrieved.') } }");
            try {
			    req.Send();
            }
            catch (Exception ex) {
                if (Constants.ShowDebugMessages) Console.Log("req.Send() failed because: " + ex.Message);
            }
		}
#endif

        // Activated whenether a property changes in the display manager
		public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called whenever a property changes in the display manager. Used to inform listeners when that happens
        /// </summary>
        /// <param name="info">The properties that were changed</param>
        public void NotifyPropertyChanged(String info)
        {
            // If the property changed event is active
            if (PropertyChanged != null)
            {
                // Trigger a property changed event
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        // Whether this display manager is processing something that should
        // show the loading overlag
        protected bool isProcessing = false;

        /// <summary>
        /// Returns whether the current display manager is processing or not
        /// <para>Used by parent RegulonGraphDisplays to show or hide the loading overlay</para>
        /// </summary>
        public bool IsProcessing
        {
            get { return isProcessing; }
        }

        /// <summary>
        /// Sets a graph's class depending on its current characteristics
        /// </summary>
        /// <param name="graph">The graph to set the class for</param>
        /// <param name="dragged">Whether the graph is currently being dragged</param>
        /// <param name="flash">Whether the graph should flash - only takes effect if it is a centroid</param>
        private void SetGraphClass (RegulonGraph graph, bool dragged, bool flash) {

            // Initialise the class string
            string classText = String.Empty;

            // If the graph is a centroid, begin with the centroid class,
            // otherwise begin with the regular class
            if (currentCentroids.Contains(selectedRegulog.Regulons.IndexOf(graph.Regulon))) {

                // If the graph should flash, start with the centroid flash
                // class
                if (flash) {
                    classText = "RegulonGraph_CentroidFlash";
                } else {
                    classText = "RegulonGraph_Centroid";
                }
            } else {
                classText = "RegulonGraph";
            }

            // If the graph is selected, add the selected class
            if (graph.IsSelected) {
                classText = classText + " RegulonGraph_Selected";
            }

            // If the graph is being dragged, add the dragged class
            if (dragged) {
                classText = classText + " RegulonGraph_Dragged";
            }

            // Apply the class string to the graph
            graph.GraphSvg.SetAttribute("class", classText);
        }

/*#if DEBUG
        /// <summary>
        /// Retrieves the current order of all graphs in the display
        /// <para>Graphs should probably internally store their current position in the display so this doesn't have to be done each time</para>
        /// </summary>
        /// <returns>An array that contains the current position of each network in the display in their current group, by their index in the data set's list of regulons
        /// <para>It doesn't matter if multiple networks get the same position number since they will be in different groups</para></returns>
        public int[] GetGraphOrder() {

            // Initialise a list that is the same length as the amount of
            // networks in the display
            int[] currentGraphOrder = new int[selectedRegulog.Regulons.Length];

            // Depending on whether the palette is present, the list of groups
            // is initially the 0th if it is, or the 1st if it isn't
            int i = Constants.UseGroupPalette ? 0 : 1;

            // If in a filtered detail view, the order can be taken directly
            // from the temp order stored from filtering
            if (!detailViewAllGraphs && currentZoomLevel == ZoomLevels.DetailView) {

                // Start from list 1 if the palette is not present
                int j = Constants.UseGroupPalette ? 0 : 1;

                // For each list of graphs...
                for (; j < regulonGraphs.Length; j++) {

                    // Copy the ordering over
			        for (int k = 1; k < graphOrder[j].Length; k++) {

                        // Unfortunately, a direct copying cannot be done since
                        // the temp order is a matrix rather than an array.
                        // Therefore the right regulon needs to be found first,
                        // so that its index in the data set's list of regulons
                        // can be used
                        currentGraphOrder[selectedRegulog.Regulons.IndexOf(regulonGraphs[j][graphOrder[j][k]].Regulon)] = k;
                    }
                }

            // Otherwise, determine the current order
            } else {

                // Make a temporary list of graphs
                // (I'm not sure why this is done)
                List<List<RegulonGraph>> tempGraphs = regulonGraphs;

                // For each list of graphs in the temporary list
                for (; i < tempGraphs.Length; i++) {

                    // Set the current graph to check as the first graph in the
                    // group, and the index as zero
                    Element currentChild = i == 0 ? palette.FirstChild : columns[i-1].FirstChild;
                    int index = 0;

                    // While there is a current graph
                    while (currentChild != null) {

                        // For each graph in the current list
                        foreach (RegulonGraph graph in tempGraphs[i]) {
    #if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Checking if this graph is " + graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ")");
    #endif
                            // If the current graph has been found in the list
                            if (currentChild == graph.DomElement) {
    #if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ") is at " + index);
    #endif
                                // Store the current graph's current position as
                                // the index value, using the position of the
                                // regulon in the data set as the position in the
                                // array
                                currentGraphOrder[selectedRegulog.Regulons.IndexOf(graph.Regulon)] = index;
                                break;
                            } //else {
                            //    currentGraphOrder[selectedRegulog.Regulons.IndexOf(graph.Regulon)] = -1;
                            //}
                        }

                        // Go to the next graph in the container
                        currentChild = currentChild.NextSibling;

                        // Increment the index
                        index++;
                    }
                }
            }

            // Return the order
            return currentGraphOrder;
        }
#endif*/

        /// <summary>
        /// Refreshes every graph in the display
        /// </summary>
        public void RefreshGraphs() {
            foreach (List<RegulonGraph> group in regulonGraphs) {
                foreach (RegulonGraph graph in group) {
                    graph.Refresh();
                }
            }
        }


        /// <summary>
        /// Recalcuate groups without having to recreate the whole display
        /// </summary>
        public void RecalculateGroups() {

            // If this is a filtered detail view, restore everything first
            if (currentZoomLevel == ZoomLevels.DetailView && !detailViewAllGraphs) {
                //DetailView_RestoreGraphs();

                // Return this to the default
                detailViewAllGraphs = true;

                UpdateVisibleRegulons(null, null);
            }

            // Create a new dictionary for the current assignments
            currentAssignments = new Dictionary<int,int>();

            // Get the current regulons in the display
            List<RegulonInfo> regulons = (List<RegulonInfo>)selectedRegulog.Regulons;

            // Clear the display's current centroids
            currentCentroids.Clear();

            // Perform K-means on the current regulons
            currentAssignments = KMeansGroupAssignment(regulons);

            // Add the new centroids to the current centroids
            foreach (KeyValuePair<int, int> pair in currentAssignments) {
                if (!currentCentroids.Contains(pair.Value))
                {
                    currentCentroids.Add(pair.Value);
                }
            }

            // Create a temproary list of graphs to store the current graphs
            // while the display is cleared
            List<RegulonGraph> tempRegulonGraphs = new List<RegulonGraph>();

            // Take all of the graphs out of the columns and put them in
            // the temporary list, clearing the columns as necessary
            foreach (List<RegulonGraph> column in regulonGraphs) {
                foreach (RegulonGraph graph in column) {

                    // If the palette is present and this is the first column,
                    // reset the graph's zoom level
                    if (Constants.UseGroupPalette && regulonGraphs.IndexOf(column) == 0) {
                        SetGraphZoomLevel(currentZoomLevel, 0, column.IndexOf(graph));
                    }

                    tempRegulonGraphs.Add(graph);
                }
                column.Clear();
            }

            // For every graph in the temporary list
            foreach (RegulonGraph graph in tempRegulonGraphs) {

                // Remove that graph from its column in the display
                graph.DomElement.ParentNode.RemoveChild(graph.DomElement);

                // Get the index of that graph's regulon in the list of
                // regulon
                int index = regulons.IndexOf(graph.Regulon);

                // If this graph is one of the current centroids, add it to the
                // label
                if (currentCentroids.Contains(index)) {
                    if (graph.GraphLabelText.IndexOf(" " + Constants.Text_NetworkLabelCentroid) == -1) {
                        graph.GraphLabelText = graph.GraphLabelText + " " + Constants.Text_NetworkLabelCentroid;
                    }

                // Otherwise, remove that from the label if it was an old
                // centroid
                } else {
                    graph.GraphLabelText = graph.GraphLabelText.Replace(" " + Constants.Text_NetworkLabelCentroid, "");
                }

                // Ensure this graph has the correct class
                SetGraphClass(graph, false, false);

                // Add the graph to the correct column and list of graphs
                graph.AddElementTo(columns[currentCentroids.IndexOf(currentAssignments[index])]);
                regulonGraphs[currentCentroids.IndexOf(currentAssignments[index])+1].Add(graph);

                // Set the regulon's current group number
                graph.Regulon.GroupNumber = currentAssignments[index];
            }
        }

        /// <summary>
        /// Refreshes the currently stored order for all visible regulon graphs in the display
        /// <para>Graphs not currently in any of the groups in the main container are unaffected</para>
        /// </summary>
        private void RefreshGraphOrder() {
            // Depending on whether the palette is present, the list of groups
            // is initially the 0th if it is, or the 1st if it isn't
            int i = Constants.UseGroupPalette ? 0 : 1;

            // For each list of graphs
            for (; i < regulonGraphs.Length; i++) {

                // Set the current graph to check as the first graph in the
                // group, and the index as zero
                Element currentChild = i == 0 ? palette.FirstChild : columns[i-1].FirstChild;
                int index = 0;

                // While there is a current graph
                while (currentChild != null) {

                    // For each graph in the current list
                    foreach (RegulonGraph graph in regulonGraphs[i]) {
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Checking if this graph is " + graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ")");
#endif
                        // If the current graph has been found in the list
                        if (currentChild == graph.DomElement) {
#if DEBUG
                            if (Constants.ShowDebugMessages) Console.Log(graph.Regulon.GenomeName + " (regulon index " + selectedRegulog.Regulons.IndexOf(graph.Regulon) + ") is at " + index);
#endif
                            // Store the current graph's current position as
                            // the index value
                            graph.Regulon.Order = index;
#if DEBUG
                            // Ensure consistency if the selection bug handler is
                            // required
                            if (requireSelectionBugHandler) {
                                foreach (RegulonInfo regulon in selectedRegulog.Regulons) {
                                    if (regulon.RegulonId == graph.Regulon.RegulonId &&
                                        regulon.RegulogId == graph.Regulon.RegulogId &&
                                        regulon.GenomeId == graph.Regulon.GenomeId
                                    ) {
                                        regulon.Order = index;
                                        break;
                                    }
                                }
                            }
#endif
                            break;
                        }
                    }

                    // Go to the next graph in the container
                    currentChild = currentChild.NextSibling;

                    // Increment the index
                    index++;
                }
            }
        }

#if DEBUG
        // At present, there are currently some issues with homologue
        // selection in DisplayManagers created from
        // RegPreciseRegulonGraphViewer2. This is here to attempt to detect
        // the issue and allow the selection to work properly

        // Status of whether the bug handler needs to be used
        bool requireSelectionBugHandler = false;

        // Status for checking whether the bug handler needs to be used
        //bool checkForSelectionBug = false;

        /// <summary>
        /// While there is a selection issue for DisplayManagers in RegPreciseRegulonGraphViewer2, this will be used to inform
        /// the display manager to use the bug handler when a new regulog is loaded
        /// <para>Automatically sets the check to false as well; this is essentially an override</para>
        /// </summary>
        public bool RequireSelectionBugHandler
        {
            get { return requireSelectionBugHandler; }
            set { requireSelectionBugHandler = value;
                //checkForSelectionBug = false;

                if (value) {
                    if (Constants.ShowDebugMessages) Console.Log("An external source has determined that the bug handler is required");
                } else {
                    if (Constants.ShowDebugMessages) Console.Log("An external source has determined that the bug handler is not required");
                }
            }
        }

        /// <summary>
        /// Handler for homologue selection bugs that occur in a DisplayManager created from RegPreciseRegulonGraphViewer2
        /// </summary>
        /// <param name="gene">The gene to ensure correct (un)selection</param>
        private void SelectionBugHandler(GeneInfo gene) {

            // Go through each graph in the display
            foreach (List<RegulonGraph> column in regulonGraphs) {

                // Keep track of when the regulon is found, so that extra
                // graphs/columns aren't checked unnecessarily
                bool foundRegulon = false;

                foreach (RegulonGraph graph in column) {

                    // For each target gene in the graph...
                    foreach (GeneNode node in graph.Nodes) {

                        // If the important identifiers are the same, assume the
                        // target gene is the same as the parameter and set it to
                        // the same selection status
                        if (node.Gene.RegulonId == gene.RegulonId && node.Gene.VimssId == gene.VimssId && node.Gene.Name == gene.Name && node.Gene.LocusTag == gene.LocusTag) {
                            node.IsSelected = gene.IsSelected;

                            // Mark that the regulon was found
                            foundRegulon = true;
                            break;
                        }
                    }

                    // Don't look at any more graphs mns if the regulon was found
                    if (foundRegulon) {
                        break;
                    }
                }

                // Don't look at any more columns if the regulon was found
                if (foundRegulon) {
                    break;
                }
            }
        }

        /// <summary>
        /// Activates when a click event is detected on the window
        /// <para>When clicking in the window, hide any existing tooltips</para>
        /// </summary>
        /// <param name="ev">The attributes of the event</param>
        private void Window_OnClick(ElementEvent ev)
        {
            //Script.Literal("ev.preventDefault()");

            // Hide any existing tooltip if the mouse click location is outside
            // the bounds of the container
            int mouseX = ev.PageX;
            int mouseY = ev.PageY;
#if DEBUG
            int containerLeft = HtmlUtil.GetElementX(container);
            int containerRight = HtmlUtil.GetElementX(container) + container.ClientWidth;
            int containerTop = HtmlUtil.GetElementY(container);
            int containerBottom = HtmlUtil.GetElementY(container) + container.ClientHeight;
#endif
            if (mouseX < HtmlUtil.GetElementX(container) ||
                mouseY < HtmlUtil.GetElementY(container) ||
                mouseX > HtmlUtil.GetElementX(container) + container.ClientWidth ||
                mouseY > HtmlUtil.GetElementY(container) + container.ClientHeight) {
                HideCurrentToolTip();
            }
        }
#endif
    }
}
