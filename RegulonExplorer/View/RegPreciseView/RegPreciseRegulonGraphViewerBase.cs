﻿using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.View.TrnDiff;
using RegulonExplorer.ViewModel;
using SystemQut.ComponentModel;
using SystemQut.Controls;
using SystemQut.Linq;
using System.Collections;
using ElementEventHandler = SystemQut.Controls.ElementEventHandler;
using SystemQut;
using RegulonExplorer.Common;

/// <summary> A list of all the logical operations that can be used
/// </summary>
public enum ZoomLevels { IconList = 0, SmallOverview = 1, LargeOverview = 2, DetailView = 3 };

namespace RegulonExplorer.View.RegPreciseView {

	/// <summary> Host control for the famous TrnDiff "wagon-wheel" display :^)
	/// </summary>

	public abstract class RegPreciseRegulonGraphViewerBase : AppContent {

		/// <summary> The container that will hold the graph display.
		/// </summary>
		protected CodeBehind regulogDisplay;

		/// <summary> A record navigator that allows user to click through the regulogs
		/// </summary>
		protected RecordNavigator pager;

		/// <summary> Combo box for gene functions.
		/// </summary>
		protected Select select_FunctionChooser;

		/// <summary> Allows user to save the current regulog as CSV.
		/// </summary>
		private Button button_SaveCsv;

		/// <summary> Allows the user to enter the file name.
		/// </summary>
		private TextBox textBox_FileName;

		#region Bindings for record navigator.
		private Binding pagerMinBinding;
		private Binding pagerMaxBinding;
		private Binding pagerCurrentBinding;
		#endregion

		/// <summary> Source of current regulog. This is actually too-tight a coupling, because we
		///		interact directly with the dataGrid embedded in the sibling control.
		/// </summary>
		protected DataGrid sourceDataGrid = null;

		/// <summary> The regulog that is currently being displayed.
		/// </summary>
		protected RegulogInfo selectedRegulog;

		/// <summary> Maintain a list of regulogs for which Regulon info has been requested.
		///		Regulon info is saved in the RegulogInfo object when it is received, and the
		///		view is updated as a side effect as soon as it arrives.
		/// </summary>
		private readonly List<RegulogInfo> regulonsRequested = new List<RegulogInfo>();

        protected List<RegulogInfo> RegulonsRequested
        {
            get { return regulonsRequested; }
        }

		/// <summary> An attribute factory which is used to decorate the regulon graphs.
		/// <para>
		///		This is updated each time we select a new regulon.
		/// </para>
		/// </summary>
		protected IAttributeFactory colourChooser;

		//// <summary> The constant that represents "all gene functions".
		//// </summary>
		//private readonly string allFunctions = "All functions";

		/// <summary>
		/// </summary>
		private IRegulonDisplay displayManager;

		/// <summary> A node factory which will be used to create glyphs.
		/// </summary>
		protected  INodeFactory nodeFactory;

		/// <summary> This will be used to let users sort the wagon-wheels
		/// </summary>
		protected Select select_SortChooser;

        /// <summary>
        /// The currently selected sorting option
        /// </summary>
        protected int currentSortOption = 4;

        /// <summary>
        /// The currently selected filtering option
        /// </summary>
        protected int currentFilterOption = 0;

        //// <summary> Toggles whether the spokes are scaled
        //// </summary>
        //protected Select scaleChooser;

		/// <summary> Currently available items for sort chooser.
		/// </summary>
		protected string[] sortItems = {
			Constants.Text_DropDownSortAlpha,
			Constants.Text_DropDownSortReverseAlpha,
			Constants.Text_DropDownSortNoOfGenes,
			Constants.Text_DropDownSortReverseNoOfGenes,
            Constants.Text_DropDownSortDistToCentroid,
		};

		/// <summary> Currently available items for filter chooser
		/// </summary>
		protected string[] filterItems = {
			Constants.Text_DropDownFilterAll,
            Constants.Text_DropDownFilterSelected
		};

        /// <summary> Currently available items for spoke scaling chooser.
        /// </summary>
        protected bool[] scaleItems = { false, true };

        /// <summary> Clears the current selection
        /// </summary>
        private Button button_ClearSelection;

        /// <summary> Allows the user to choose the zoom level of the display
        /// </summary>
        private Select select_SetZoomLevel;

        public Select Select_SetZoomLevel
        {
            get { return select_SetZoomLevel; }
            //set { zoomChooser = value; }
        }

        /// <summary> Items for the zoom chooser (until a slider or similar is
        /// implemented)
        /// </summary>
		private string[] zoomItems = {
			"Icon list",
			"Small overview",
			"Large overview",
            "Detail view"
		};

        /// <summary>
        /// Array of sort functions that is referenced by the sort drop down
        /// menu. The fifth option should not normally be used but exists as
        /// a fall back
        /// </summary>
		protected Func<RegulonInfo,RegulonInfo,int> [] sortFunctions = {
			RegulogInfo_OrderByName,
			RegulogInfo_OrderByNameReverse,
			RegulogInfo_OrderByGeneCountAsc,
			RegulogInfo_OrderByGeneCountDesc,
			RegulogInfo_OrderByName, // Fall back
		};

        /// <summary>
        /// Array of filter functions that is referenced by the filter drop down
        /// menu
        /// </summary>
        protected Func<RegulonInfo, bool> [] filterFunctions = {
            null,
            RegulogInfo_FilterBySelection
        };

        //private bool refresh = true;

        /// <summary>
        /// The container that holds the side panel
        /// </summary>
        protected DivElement div_SidePanel;

        /// <summary>
        /// The button that toggles the side panel on and off
        /// </summary>
        protected Button button_ToggleSidePanel;

        /// <summary>
        /// The container that holds the regulon display (container)
        /// </summary>
        protected DivElement div_RegulonDisplayHolder;

        /// <summary>
        /// The table element that holds the legend
        /// </summary>
        protected TableElement table_Legend;

        /// <summary> Touch-friendly button for icon list zoom level
        /// </summary>
        private Button touchButton_ZoomLevel1;

        /// <summary> Touch-friendly button for small overview zoom level
        /// </summary>
        private Button touchButton_ZoomLevel2;

        /// <summary> Touch-friendly button for large overview zoom level
        /// </summary>
        private Button touchButton_ZoomLevel3;

        /// <summary> Touch-friendly button for detail view zoom level
        /// </summary>
        private Button touchButton_ZoomLevel4;

        /// <summary> Touch-friendly button for clearing selection
        /// </summary>
        private Button touchButton_ClearSelection;

		/// <summary> Combo box for GO terms.
		/// </summary>
		protected Select select_TermChooser;

		//// <summary> The constant that represents "all GO terms".
		//// </summary>
		//private readonly string allTerms = "All terms";

        /// <summary> Touch-friendly button for highlighting gene functions
        /// </summary>
        private Button touchButton_HighlightFunction;

        /// <summary>
        /// Popup that appears when using the gene function button
        /// </summary>
        protected DivElement touchButton_HighlightFunctionPopup;

        /// <summary> Touch-friendly button for highlighting GO terms
        /// </summary>
        private Button touchButton_HighlightTerm;

        protected DivElement touchButton_HighlightTermPopup;

        //// <summary> Used in the pop up to allow the user to highlight terms
        //// with a text query
        //// </summary>
        //private TextBox touchButton_HighlightTermPopup_TextBox;

        /// <summary>
        /// Touch-friendly button for searching for strings in gene data
        /// </summary>
        private Button touchButton_SearchTerm;

        /// <summary>
        /// Popup that appears when using the search for strings button
        /// </summary>
        protected DivElement touchButton_SearchTermPopup;

        /// <summary>
        /// Text box in the above popup where the user can enter a string
        /// </summary>
        private TextBox textBox_GeneFunctionSearch;

        /// <summary>
        /// Button in the above popup that performs the search
        /// </summary>
        private Button button_GeneFunctionSearch;

        /// <summary> Div element that is shown when something is processing
        /// </summary>
        protected DivElement div_LoadingOverlay;

        /// <summary>
        /// The currently selected zoom level
        /// </summary>
        protected ZoomLevels currentZoomLevel = ZoomLevels.SmallOverview;

        /// <summary>
        ///  The label that shows the currently selected genes
        /// </summary>
        protected Element label_SelectedGenes;

		/// <summary> Create a new regulon graph viewer bound to a provided DOM element which
		///		should already exist in the application html document.
		///	<para>
		///		The element should contain a nested block element of some sort to hold the view
		///		(data-name="RegulonDisplay", data-codebehind="CodeBehind") and a record navigator
		///		(data-name="Pager", data-codebehind="RecordNavigator").
		/// </para>
		/// </summary>
		/// <param name="element"></param>
		public RegPreciseRegulonGraphViewerBase(
			Element element
		)
			: base( element ) {

			// TODO: this seems to be in the wrong place.
            // Clear any existing display manager
            if (this.displayManager != null) {
                this.displayManager.PropertyChanged -= DisplayManager_PropertyChanged;
                this.displayManager.Clear();
            }

            // If this is a comparison, create a comparison display manager
            if (this.GetType() == typeof(RegPreciseRegulonGraphViewerSideBySide) || this.GetType() == typeof(RegPreciseRegulonGraphViewer2SideBySide) || this.GetType() == typeof(LocalRegulonGraphViewerSideBySide))
            {
                if (Constants.UseHammerTouchSupport) {
                    this.displayManager = new HammerCompareRegulonDisplay();
                } else {
                    this.displayManager = new CompareRegulonDisplay();
                }
            }

            // Else create a regular display manager
            else
            {
                if (Constants.UseHammerTouchSupport) {
#if DEBUG
                    if (!Constants.UseGroupDisplaysGlobally) {
                        this.displayManager = new HammerRegulonDisplay();
                    } else {
#endif
                        this.displayManager = new HammerGroupRegulonDisplay();
#if DEBUG
                    }
#endif
                } else {
#if DEBUG
                    if (!Constants.UseGroupDisplaysGlobally) {
                        this.displayManager = new DefaultRegulonDisplay();
                    } else {
#endif
                        this.displayManager = new GroupRegulonDisplay();
#if DEBUG
                    }
#endif
                }
            }

            // Add a listener to check for properties (e.g. zoom) changing in
            // the display manager
            this.displayManager.PropertyChanged += DisplayManager_PropertyChanged;

            // Find all the child controls and then bind functions to them
			FindChildControls();
            BindChildControls();

            // Watch for when this graph viewer is switched to or away from
			ActivationChanged += this_ActivationChanged;

			DataContext = this;

			// for development/debug purposes... in case this control has been hard-coded to "activated" in the HTML document.
			if ( IsActivated ) RefreshRegulons();
		}

		/// <summary>
        /// Binds functions to relevant controls in the display
		/// <para>
		///		Override this to perform additional custom data binding.
        ///		Remember to call base.BindChildControls!
		/// </para>
		/// </summary>

		protected virtual void BindChildControls() {
            if (pager != null) {
			    pagerMinBinding = new Binding(
				    this, "MinCollectionIndex",
				    pager, "Min",
				    DataBindingMode.OneWay
			    );

			    pagerMaxBinding = new Binding(
				    this, "MaxCollectionIndex",
				    pager, "Max",
				    DataBindingMode.OneWay
			    );

			    pagerCurrentBinding = new Binding(
				    this, "SelectedIndex",
				    pager, "Current",
				    DataBindingMode.TwoWay
			    );

			    pager.Formatter = Formatter;
            }

            // Add a listener for the save button when it's clicked
			if ( button_SaveCsv != null ) {
				button_SaveCsv.Clicked += new ElementEventHandler( saveButton_Clicked );
			}

            /*if (bVButton != null)
            {
                bVButton.Clicked += new ElementEventHandler(bVButton_Clicked);
            }*/

            // Add a listener for the clear selection button when it's clicked
            if ( button_ClearSelection != null ) {
				button_ClearSelection.Clicked += new ElementEventHandler( button_Clear_Clicked );
			}

            // Add a listener to the gene function chooser
            if (select_FunctionChooser != null) {
                select_FunctionChooser.SelectionChanged += new ElementEventHandler(functionChooser_SelectionChanged);
            }

			// Add the default option to the sort chooser
			if ( select_SortChooser != null ) {
				select_SortChooser.Items = sortItems;
                select_SortChooser.SelectedIndex = currentSortOption;
				select_SortChooser.SelectionChanged += new ElementEventHandler( sortChooser_SelectionChanged );
			}

            // Add the options to the scale chooser
            /*if (scaleChooser != null)
            {
                scaleChooser.Items = scaleItems;
                scaleChooser.SelectionChanged += new ElementEventHandler(scaleChooser_SelectionChanged);
            }*/

            // Add the options to the scale chooser
            if (select_SetZoomLevel != null)
            {
                select_SetZoomLevel.Items = zoomItems;
                select_SetZoomLevel.SelectedIndex = (int)ZoomLevels.SmallOverview;
                select_SetZoomLevel.SelectionChanged += new ElementEventHandler(zoomChooser_SelectionChanged);
            }

            //Add the action for the legend toggle button
            if ( button_ToggleSidePanel != null ) {
				button_ToggleSidePanel.Clicked += new ElementEventHandler( toggleSidePanel_clicked );
			}

            // Update the size of the side panel if necessary
            if ( div_SidePanel != null ) {
                UpdateSidePanelClass();
                Window.AddEventListener("resize", Window_OnResize, false);
            }

            //Add the action for the icon list zoom level button
            if ( touchButton_ZoomLevel1 != null ) {
				touchButton_ZoomLevel1.Clicked += new ElementEventHandler( touchButton_ZoomLevel1_clicked );
			}

            //Add the action for the small overview zoom level button
            if ( touchButton_ZoomLevel2 != null ) {
				touchButton_ZoomLevel2.Clicked += new ElementEventHandler( touchButton_ZoomLevel2_clicked );
			}

            //Add the action for the large overview zoom level button
            if ( touchButton_ZoomLevel3 != null ) {
				touchButton_ZoomLevel3.Clicked += new ElementEventHandler( touchButton_ZoomLevel3_clicked );
			}

            //Add the action for the detail view zoom level button
            if ( touchButton_ZoomLevel4 != null ) {
				touchButton_ZoomLevel4.Clicked += new ElementEventHandler( touchButton_ZoomLevel4_clicked );
			}

            //Add the action for the clear selection button
            if ( touchButton_ClearSelection != null ) {
				touchButton_ClearSelection.Clicked += new ElementEventHandler( button_Clear_Clicked );
			}

            //Make sure the zoom buttons have the right image
            if ( touchButton_ZoomLevel1 != null && touchButton_ZoomLevel2 != null && touchButton_ZoomLevel3 != null && touchButton_ZoomLevel4 != null ) {
                SetZoomLevelButtonImages();
            }

            //Add the action for the GO term chooser
            if ( select_TermChooser != null ) {
                select_TermChooser.SelectionChanged += new ElementEventHandler(termChooser_SelectionChanged);
            }

            //Add the action for the highlight gene function button
            if ( touchButton_HighlightFunction != null ) {
				touchButton_HighlightFunction.Clicked += new ElementEventHandler( touchButton_HighlightFunction_Clicked );
			}

            //Add the action for the highlight GO term button
            if ( touchButton_HighlightTerm != null ) {
				touchButton_HighlightTerm.Clicked += new ElementEventHandler( touchButton_HighlightTerm_Clicked );
			}

            //Touch event that hides the popups for the above two
            if ( touchButton_HighlightFunction != null || touchButton_HighlightTerm != null ) {
                Document.Body.AddEventListener(MouseEventType.click, BodyOnClick, false);
            }

            //Event for when things are typed in the popup text box
            if ( textBox_GeneFunctionSearch != null && button_GeneFunctionSearch != null ) {
				button_GeneFunctionSearch.Clicked += new ElementEventHandler( geneFunction_SearchButton_Clicked );
                textBox_GeneFunctionSearch.DomElement.AddEventListener("keyup", geneFunction_SearchText_KeyUp, false);
			}

            // Event for when the user clicks on the button to search for a
            // string
            if ( touchButton_SearchTerm != null ) {
				touchButton_SearchTerm.Clicked += new ElementEventHandler( touchButton_SearchTerm_Clicked );
			}

#if DEBUG
            // Used to allow the loading overlay to be cleared in case an error
            // disrupts processing
            if ( div_LoadingOverlay != null ) {
                div_LoadingOverlay.AddEventListener(MouseEventType.click, LoadingOverlayClicked, false);
            }
#endif

            // Update the list of selected homologues
            if (label_SelectedGenes != null) {
                UpdateSelectedGenesLabel();
            }
		}

        /// <summary>
        /// Is true if DisplayFullMode is already active
        /// </summary>
        protected bool processingDisplayFullMode = false;

        /// <summary>
        /// Gets and sets the display manager
        /// </summary>
		public IRegulonDisplay DisplayManager {
			get {
				return this.displayManager;
			}
			set {

                // Don't do anything if the display manager is not different
				if ( value == displayManager ) return;

                // Store the current distances between networks
                double[][] tempCurrentDistances = displayManager.CurrentDistances;

                // Clear the current display manager if one exists
				if ( displayManager != null ) {
					displayManager.Clear();
				}

                // Set the current display manager to the new one
				displayManager = value;

                // Set the display manager's distances to the stored distances
                displayManager.CurrentDistances = tempCurrentDistances;

                // Make sure the list of selected homologues is correct
                if (label_SelectedGenes != null) {
                    UpdateSelectedGenesLabel();
                }

                // If not already in DisplayFullMode, set up the display
                // manager
                if (!processingDisplayFullMode) {
				    DisplayFullMode();
                }

                // Notify listeners that the display manager has changed
				NotifyPropertyChanged( "DisplayManager" );
			}
		}

		/// <summary>
        /// When the save button is clicked, attempt to save the current data
        /// set
		/// </summary>
		/// <param name="sender">Object that sent the event</param>
		/// <param name="eventArgs">Arguments for the event</param>
		void saveButton_Clicked( object sender, ElementEventArgs eventArgs ) {

            // Don't do anything if no data is loaded yet
			if ( RequiredDataNotAvailable ) {
				Window.Alert( Constants.Text_MessageErrorDataLoading );
				return;
			}

            //Store which genes are selected for saving
            // This should be changed to happen automatically when things are
            // selected in the display
            /*for (int i = 0; i < selectedRegulog.Genes.Length; i++) {

                //If the selected genes can be found by checking whether the
                //selected genes list contains them, do it that way
                if (displayManager.SelectedGenes.Contains(selectedRegulog.Genes[i])) {
                    selectedRegulog.Genes[i].Gene.Selected = true;
                } else {

                    //Otherwise, go through all the selected genes for each
                    //gene in the regulog, and set them to selected if the gene
                    //and regulon IDs match
                    for (int j = 0; j < displayManager.SelectedGenes.Length; j++) {
                        if (selectedRegulog.Genes[i].RegulonId == displayManager.SelectedGenes[j].RegulonId &&
                            selectedRegulog.Genes[i].VimssId == displayManager.SelectedGenes[j].VimssId) {
                            selectedRegulog.Genes[i].Gene.Selected = true;
                            break;
                        } else {
                            selectedRegulog.Genes[i].Gene.Selected = false;
                        }
                    }
                }
            }*/

/*#if DEBUG
            //Save the order of networks
            // This should be changed to happen automatically when things are
            // dragged around the display
            int[] graphOrder = new int[selectedRegulog.Regulons.Length];
            if (DisplayManager is GroupRegulonDisplay) {
                graphOrder = (DisplayManager as GroupRegulonDisplay).GetGraphOrder();
            }
            if (DisplayManager is HammerGroupRegulonDisplay) {
                graphOrder = (DisplayManager as HammerGroupRegulonDisplay).GetGraphOrder();
            }
            if (DisplayManager is DefaultRegulonDisplay) {
                graphOrder = (DisplayManager as DefaultRegulonDisplay).GetGraphOrder();
            }
            if (DisplayManager is HammerRegulonDisplay) {
                graphOrder = (DisplayManager as HammerRegulonDisplay).GetGraphOrder();
            }
            for (int i = 0; i < selectedRegulog.Regulons.Length; i++) {
                selectedRegulog.Regulons[i].Order = graphOrder[i];
            }
#endif*/

            // Save the regulog, using the entered filename if it exists,
            // otherwise using an autogenerated name based on the regulog ID
            if ( textBox_FileName != null ) {
			    DataLayer.SaveRegulogAsCsv( textBox_FileName.Text, selectedRegulog );
            } else {
			    DataLayer.SaveRegulogAsCsv( "regulog" + selectedRegulog.RegulogId + ".csv", selectedRegulog );
            }
		}

		/// <summary> Text formatter for the pager.
		/// </summary>
		/// <param name="format">
		///		The current format string in use by the pager. The argument is ignored
		///		by the current instance.
		/// </param>
		/// <param name="current">
		///		The current index within the associated collection. This may be undefined
		///		if the collection has no "current" element, such as when the no record is
		///		selected in a data grid.
		/// </param>
		/// <param name="min">
		///		The (possibly undefined) minimum value in the range covered by the pager.
		///		See note for <c>current</c>.
		/// </param>
		/// <param name="max">
		///		The (possibly undefined) maximum value in the range covered by the pager.
		///		See note for <c>current</c>.
		/// </param>
		/// <returns>
		///		A string that represents the current state. Returns a NO_SELECTION literal
		///		if any of the arguments are undefined or no regulog is selected. Returns
		///		a formatted string in the form "Regulog: {0} {1} (group {2} of {3})" otherwise.
		/// </returns>

		protected string Formatter( string format, int current, int min, int max ) {
			return FormatterImpl( format, current, min, max );
		}

		/// <summary> Text formatter for the pager.
		/// </summary>
		/// <param name="format">
		///		The current format string in use by the pager. The argument is ignored
		///		by the current instance.
		/// </param>
		/// <param name="current">
		///		The current index within the associated collection. This may be undefined
		///		if the collection has no "current" element, such as when the no record is
		///		selected in a data grid.
		/// </param>
		/// <param name="min">
		///		The (possibly undefined) minimum value in the range covered by the pager.
		///		See note for <c>current</c>.
		/// </param>
		/// <param name="max">
		///		The (possibly undefined) maximum value in the range covered by the pager.
		///		See note for <c>current</c>.
		/// </param>
		/// <returns>
		///		A string that represents the current state. Returns a NO_SELECTION literal
		///		if any of the arguments are undefined or no regulog is selected. Returns
		///		a formatted string in the form "Regulog: {0} {1} (group {2} of {3})" otherwise.
		/// </returns>

		protected abstract string FormatterImpl( string format, int current, int min, int max );

		/// <summary> Locates the required nested controls, matching by name and type.
		///		Override this to locate additional child controls.
		/// <para>
		///		Remember to call base.FindChildControls!
		/// </para>
		/// </summary>
		/// <exception cref="Exception">
		///		Exception is thrown if required controls do no exist.
		/// </exception>

		protected virtual void FindChildControls() {
			regulogDisplay = (CodeBehind) FindControlByNameAndType( "RegulonDisplay", typeof( CodeBehind ) );
			pager = (RecordNavigator) FindControlByNameAndType( "Pager", typeof( RecordNavigator ) );
			select_FunctionChooser = (Select) FindControlByNameAndType( "FunctionChooser", typeof( Select ) );
			button_SaveCsv = (Button) FindControlByNameAndType( "SaveButton", typeof( Button ) );
			textBox_FileName = (TextBox) FindControlByNameAndType( "FileName", typeof( TextBox ) );
            select_SortChooser = (Select)FindControlByNameAndType( "SortChooser", typeof(Select) );
            //scaleChooser = (Select)FindControlByNameAndType("ScaleChooser", typeof(Select));
            //bVButton = (Button)FindControlByNameAndType("BVButton", typeof(Button));
			button_ClearSelection = (Button) FindControlByNameAndType( "ClearButton", typeof( Button ) );
            select_SetZoomLevel = (Select)FindControlByNameAndType("ZoomChooser", typeof(Select));
            button_ToggleSidePanel = (Button)FindControlByNameAndType("ToggleSidePanel", typeof(Button));
            touchButton_ZoomLevel1 = (Button)FindControlByNameAndType("TouchButton_ZoomLevel1", typeof(Button));
            touchButton_ZoomLevel2 = (Button)FindControlByNameAndType("TouchButton_ZoomLevel2", typeof(Button));
            touchButton_ZoomLevel3 = (Button)FindControlByNameAndType("TouchButton_ZoomLevel3", typeof(Button));
            touchButton_ZoomLevel4 = (Button)FindControlByNameAndType("TouchButton_ZoomLevel4", typeof(Button));
            touchButton_ClearSelection = (Button)FindControlByNameAndType("TouchButton_ClearSelection", typeof(Button));
			select_TermChooser = (Select) FindControlByNameAndType( "TermChooser", typeof( Select ) );
            touchButton_HighlightFunction = (Button)FindControlByNameAndType("TouchButton_HighlightFunction", typeof(Button));
            touchButton_HighlightTerm = (Button)FindControlByNameAndType("TouchButton_HighlightTerm", typeof(Button));
            textBox_GeneFunctionSearch = (TextBox)FindControlByNameAndType("GeneFunction_SearchText", typeof(TextBox));
            button_GeneFunctionSearch = (Button)FindControlByNameAndType("GeneFunction_SearchButton", typeof(Button));
            touchButton_SearchTerm = (Button)FindControlByNameAndType("TouchButton_SearchTerm", typeof(Button));

            div_LoadingOverlay = (DivElement)Document.GetElementById("loadingOverlay");
		}

		/// <summary> Event handler for this.ActivationChanged.
		/// <para>
		///		Refreshes the regulon table.
		/// </para>
		/// </summary>
		/// <param name="activated"></param>

        protected virtual void this_ActivationChanged(IActivated activated)
        {
            // Hide the current tooltip in Hammer display managers
            if (DisplayManager is HammerRegulonDisplay) {
                (DisplayManager as HammerRegulonDisplay).HideCurrentToolTip();
            }

            if (DisplayManager is HammerGroupRegulonDisplay) {
                (DisplayManager as HammerGroupRegulonDisplay).HideCurrentToolTip();
            }
		}

		/// <summary> Refreshes the visual display.
		/// </summary>

		protected virtual void RefreshRegulons() {

            // Clear the display manager
			displayManager.Clear();

            // Make sure the list of selected homologues is correct
            if (label_SelectedGenes != null) {
                UpdateSelectedGenesLabel();
            }

            // Don't do anything else if there is no data set
			if ( selectedRegulog == null ) return;

            // If the current dataset is still wanted, redraw the display
			if ( regulonsRequested.Contains( selectedRegulog ) ) {
				Redraw();
			}

            // Otherwise get the data for the selected regulog
			else {
				regulonsRequested.Add( selectedRegulog );
				DataLayer.Instance.RequestRegulonsInRegulog( selectedRegulog, false );
				DataLayer.Instance.RequestGenesInRegulog( selectedRegulog, false );
				DataLayer.Instance.RequestRegulatorsInRegulog( selectedRegulog, false );
				DataLayer.Instance.RequestSitesInRegulog( selectedRegulog, false );
			}
		}

        /// <summary>
        /// Redraw the display
        /// </summary>
		protected void Redraw() {

            // Clear the current display first, then recreate it with the
            // selected regulons
			displayManager.Clear();
			ProcessRegulons();
		}

		/// <summary> Renders a regulon graph as appropriate for the current selected options.
		/// <para>
		///		In "full" mode, a wagon-wheel is displayed for each regulon, flowing in the display
		///		area as required.
		/// </para>
		/// <para>
		///		In "two-up comparison" mode, two wagon-wheels are fitted to fill the display.
		///		Only the two selected regulons are displayed.
		/// </para>
		/// <para>
		///		In "logical" mode (and|or|xor) three wheels are fitted horizontally to fill the display.
		///		The two selected regulons are displayed left and right, with the derived regulon in the middle.
		/// </para>
		/// <para>
		///		In "consensus" mode, a single wheel is fitted to the display.
		/// </para>
		/// </summary>

		protected virtual void ProcessRegulons() {
			// do not update until we get required information.
			if ( RequiredDataNotAvailable ) return;

/*#if DEBUG
            // Ensure everything in the data set is connected up properly
            selectedRegulog.UpdateReferences();
#endif*/

            // Refresh the options in the gene function chooser if it exists
			if (select_FunctionChooser != null) {
			    RefreshFunctionChooser();
            }

            // Redraw the display
			DisplayFullMode();

            // Refresh the options in the GO term chooser if it exists
            if (Constants.UseGoTerms) {
                RefreshTermChooser();
            }
		}

		/// <summary> Returns true iff the required data is available.
		/// </summary>

		private bool RequiredDataNotAvailable {
			get {
				return selectedRegulog == null || selectedRegulog.Sites == null || selectedRegulog.Genes == null || selectedRegulog.Regulons == null || selectedRegulog.Regulators == null;
			}
		}

		/// <summary> Configures and renders the display
		/// </summary>
		protected virtual void DisplayFullMode() {
			// TODO: push these details up to the calling class.
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("RegPreciseRegulonGraphViewerBase - entering DisplayFullMode");
#endif*/

            // Note that DisplayFullMode is active
            processingDisplayFullMode = true;

            // Configuration of colours
            // If GO terms are enabled, use the GO term colour chooser
            if (Constants.UseGoTerms) {

                // Use CSS attributes if they are enabled
                if (Constants.UseCssNodeAttributes) {
                    colourChooser = new GOTermCSSAttributeFactory(selectedRegulog);
                } else {
                    colourChooser = new GOTermAttributeFactory(selectedRegulog);
                }

            // If Wedge mode is enabled, use the wedge colour chooser if CSS
            // attributes are disabled
            } else if ( Constants.UseWedgeWheels ) {

                // Use regular CSS attributes if they are enabled
                if (Constants.UseCssNodeAttributes) {
				    colourChooser = new GeneFunctionCSSAttributeFactory( selectedRegulog );
                // Otherwise use the special wedge colour chooser
                } else {
				    colourChooser = new WedgeAttributeFactory( selectedRegulog );
                }

            // Default case is using the gene function colour factory
            } else {

                // Use CSS attributes if they are enabled
				if (Constants.UseCssNodeAttributes) {
				    colourChooser = new GeneFunctionCSSAttributeFactory( selectedRegulog );
                } else {
				    colourChooser = new GeneFunctionAttributeFactory( selectedRegulog );
                }
            }

            // Configuration of nodes
            // Create wedge nodes if that mode is enabled, otherwise create
            // regular circular nodes
            if ( Constants.UseWedgeWheels ) {
                nodeFactory = new WedgeNodeFactory();
            } else {
                nodeFactory = new CircleNodeFactory();
            }

            // Get the current list of regulons
			List<RegulonInfo> regulons = (List<RegulonInfo>) selectedRegulog.Regulons;

            /*if (scaleChooser != null) {
                for (int i = 0; i < regulons.Length; i++)
                {
                    regulons[i].DoScale = scaleItems[scaleChooser.SelectedIndex];
                }
            }*/

            // Don't do anything else if there are no regulons
			if ( Script.IsNullOrUndefined( regulons ) ) {
				return;
			}

            // Pass the data set and configuration to the display manager to
            // create the networks
            displayManager.DisplayRegulons(regulons, regulogDisplay.DomElement, selectedRegulog, nodeFactory, colourChooser);

#if DEBUG
//#if DEBUG
//            if (Constants.ShowDebugMessages) Console.Log("The first regulon in the list (" + regulons[0].GenomeName + ") has the order " + regulons[0].Order);
//#endif

            // If there was a saved order, ensure the regulons are sorted in
            // that order
            if (regulons[0].Order != 0 && regulons[0].Order != -1 && regulons[0].Order != null) {
                DisplayManager.UpdateVisibleRegulons( RegulogInfo_SavedOrder, null );

                //Clear order from regulons
                /*foreach (RegulonInfo regulon in regulons) {
                    regulon.Order = null;
                }*/
                //regulons[0].Order = null;
            } else
#endif

            // If the networks are to be sorted by their distance to the
            // centroid, do this now
			if (currentSortOption == 4) {
                /*if (DisplayManager is HammerGroupFreeRegulonDisplay
                && (DisplayManager as HammerGroupFreeRegulonDisplay).NumberOfGroups > 1) {
                    (DisplayManager as HammerGroupFreeRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }*/
                if (DisplayManager is HammerGroupRegulonDisplay) {
                    (DisplayManager as HammerGroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }

                if (DisplayManager is GroupRegulonDisplay) {
                    (DisplayManager as GroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }
                if (DisplayManager is DefaultRegulonDisplay) {
                    (DisplayManager as DefaultRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }
                if (DisplayManager is HammerRegulonDisplay) {
                    (DisplayManager as HammerRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }

            // Otherwise use the corresponding sort function to the currently
            // selected sort option
            } else {
			    DisplayManager.UpdateVisibleRegulons( sortFunctions[currentSortOption], null );
            }

            // Set the zoom level to that which is currently selected
            DisplayManager.CurrentZoomLevel = currentZoomLevel;

            // If there are any selected genes, use the selected gene
            // highlighter
			if (DisplayManager.SelectedGenes.Length > 0) {
				colourChooser.Highlighter = SelectedGeneHighlighter;
            } else {
                colourChooser.Highlighter = null;
            }

            // Notify anyone listening for selected genes being changed
            DisplayManager.SelectedGenesChanged += DisplayManager_SelectedGenesChanged;

            // Note that DisplayFullMode has finished
            processingDisplayFullMode = false;
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("RegPreciseRegulonGraphViewerBasey - exiting DisplayFullMode");
#endif*/
		}

		/// <summary> Get or set a reference to the regulogChooser.
		/// </summary>
		public DataGrid SourceDataGrid {
			get {
				return this.sourceDataGrid;
			}
			set {
                // Don't do anything if the data grid has not changed
				if ( sourceDataGrid == value ) return;

                // Set the new data grid
				sourceDataGrid = value;

                // Add a listener to the data grid
				sourceDataGrid.PropertyChanged += regulogDataGrid_PropertyChanged;

                // Notify listeners that details from the data grid have
                // changed
				NotifyPropertyChanged( "LabelText,CollectionChooser,MinCollectionIndex,MaxCollectionIndex,SelectedIndex" );
			}
		}

		/// <summary> When the selected item in the regulog table changes, we need to make ourselves visible
		///		if not already so and then redisplay.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="eventArgs"></param>

		void regulogDataGrid_PropertyChanged( object sender, PropertyChangedEventArgs eventArgs ) {
#if DEBUG
            //Console.Log("regulogDataGrid_PropertyChanged triggered in RegPreciseRegulonGraphViewerBase");
#endif
			if ( eventArgs.Includes( "ItemsSource" ) ) {
				NotifyPropertyChanged( "LabelText,MinCollectionIndex,MaxCollectionIndex" );
			}
			if ( eventArgs.Includes( "SelectedItem" ) ) {
                GetSelectedRegulog(RegulogReceived);
                //refresh = true;
            }
		}

		/// <summary> Callback which is invoked when the regulog to be displayed becomes available.
		/// <para>
		///		This _may_ be invoked immediately, as is the case when the control is connected to
		///		a RegulogChooser. however it may be invoked asynchronously, as is the case with a
		///		RegulonChooser, because the regulog has to be pulled down from the web in that case.
		/// </para>
		/// </summary>
		/// <param name="regulog"></param>

		private void RegulogReceived( RegulogInfo regulog ) {
#if DEBUG
            //Console.Log("RegulonReceived triggered in RegPreciseRegulonGraphViewerBase");
#endif

            // Set the new regulog as the currently selected one
			SelectedRegulog = regulog;

            // Clear the current distances in the display manager
            if (DisplayManager != null) {
                DisplayManager.CurrentDistances = null;
            }

            // Update the display with the new regulog
			UpdateDisplay();
            //refresh = true;
		}

        /// <summary>
        /// Updates the display based on new data
        /// </summary>
		protected virtual void UpdateDisplay() {
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("UpdateDisplay triggered for RegPreciseRegulonGraphViewerBase");
#endif*/

            // If there is a selected regulog
            if (selectedRegulog != null)
            {
                // Show the loading overlay if it exists
                if (div_LoadingOverlay != null) {
                    if (Constants.ShowDebugMessages) Console.Log("Attempting to display loading overlay");
                    div_LoadingOverlay.Style.Visibility = "visible";
                }

                // Set this regulon viewer as being active (i.e. visible)
                IsActivated = true;

                // Refresh the current regulons
                RefreshRegulons();

                // Refresh the gene function chooser if it exists
                if (select_FunctionChooser != null) {
			        RefreshFunctionChooser();
                }

                // Refresh the GO term function chooser if GO terms are enabled
                // and if it exists
                if (Constants.UseGoTerms && select_TermChooser != null) {
                    RefreshTermChooser();
                }
            }
		}

        /// <summary>
        /// Gets or sets the selected index in the data grid
        /// </summary>
		[ScriptName( "selectedIndex" )] // Unmangle generated name to allow databinding.
		protected virtual int SelectedIndex {
			get {
				return sourceDataGrid == null ? (int) Script.Undefined : sourceDataGrid.SelectedIndex;
			}
			set {
                // Don't do anything if the value is the same or there is no
                // data grid
				if ( value == SelectedIndex ) return;
				if ( sourceDataGrid == null ) return;

                // Set the selected index
				sourceDataGrid.SelectedIndex = value;

                // Notify listeners that the selected index has changed
				NotifyPropertyChanged( "SelectedIndex" );
			}
		}

        /// <summary>
        /// Returns the minimum index in the data grid
        /// </summary>
		[ScriptName( "minCollectionIndex" )] // Unmangle generated name to allow databinding.
		protected virtual int MinCollectionIndex {
			get {
				ObservableList items = Script.IsValue( sourceDataGrid ) ? sourceDataGrid.ItemsSource : null;
				return Script.IsValue( items ) ? 0 : (int) Script.Undefined;
			}
		}

        /// <summary>
        /// Returns the maximum index in the data grid
        /// </summary>
		[ScriptName( "maxCollectionIndex" )] // Unmangle generated name to allow databinding.
		protected virtual int MaxCollectionIndex {
			get {
				ObservableList items = Script.IsValue( sourceDataGrid ) ? sourceDataGrid.ItemsSource : null;
				return Script.IsValue( items ) ? items.Length - 1 : (int) Script.Undefined;
			}
		}

        /// <summary>
        /// Returns the label text in the data grid
        /// </summary>
		[ScriptName( "labelText" )] // Unmangle generated name to allow databinding.
		protected virtual string LabelText {
			get {
				return Script.IsValue( selectedRegulog )
					? String.Format( "Regulog: {0} {1}", selectedRegulog.TaxonName, selectedRegulog.RegulatorName )
					: "Choose a regulog to browse regulogRecords.";
			}
		}

		/// <summary> Override this method to provide the currently selected regulog from the source datagrid.
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>

		protected abstract void GetSelectedRegulog( Action<RegulogInfo> action );

		/// <summary> Get or set the selected regulog.
		/// </summary>

		public RegulogInfo SelectedRegulog {
			get { return selectedRegulog; }
			set {

                // Don't do anything if the selected regulog hasn't changed
				if ( selectedRegulog == value ) return;

                // Remove the listeners to the previous selected regulog if one
                // was present
				if ( selectedRegulog != null ) {
					selectedRegulog.PropertyChanged -= selectedRegulog_PropertyChanged;
				}

                // Set the new selected regulog
				selectedRegulog = value;

                // If there actually is a selected regulog...
				if ( selectedRegulog != null ) {

                    // Set the filename in the text box, if it exists, to the
                    // default derived from the regulog's ID
					if ( textBox_FileName != null ) {
						textBox_FileName.Text = "regulog" + selectedRegulog.RegulogId + ".csv";
					}

                    // Add a listener to the selected regulog
					selectedRegulog.PropertyChanged += selectedRegulog_PropertyChanged;
				}

                // Notify listeners that the selected regulog has changed
				NotifyPropertyChanged( "SelectedRegulog,LabelText,SelectedIndex" );

                // Clear the display manager's current distances
                if ( DisplayManager != null ) {
                    DisplayManager.CurrentDistances = null;
                }
			}
		}

		/// <summary> Observer for preoprty changes in the selected regulog. This will fire when
		///		data arrives from the server.
		/// </summary>
		/// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>

        protected virtual void selectedRegulog_PropertyChanged(object sender, PropertyChangedEventArgs eventArgs)
        {
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("selectedRegulog_PropertyChanged triggered for RegPreciseRegulonGraphViewerBase");
#endif*/

            // If the sender isn't the current data set, don't do anything
			if ( sender != selectedRegulog ) return;

            // Refresh the regulons if they, the regulators or the sites have
            // changed
			if ( eventArgs.Includes( "Regulons" ) || eventArgs.Includes( "Regulators" ) || eventArgs.Includes( "Sites" ) ) {
				RefreshRegulons();
                //refresh = true;
			}

            // Also refresh the regulons if the genes have changed, but also
            // do other things
			if ( eventArgs.Includes( "Genes" ) ) {

                // Refresh the options in the gene function chooser if it exists
				if (select_FunctionChooser != null) {
			        RefreshFunctionChooser();
                }

                RefreshRegulons();
                //refresh = true;

                // Refresh the options in the GO term chooser if it exists
                if (Constants.UseGoTerms && select_TermChooser != null) {
                    RefreshTermChooser();
                }
            }
		}

		/// <summary> Updates the content of the function chooser to reflect the current set of gene functions.
		/// </summary>

		protected void RefreshFunctionChooser() {

            // Don't do anything if there isn't a current dataset
			if ( selectedRegulog == null ) return;

            // Get the list of gene functions from the current dataset
			string[] items = selectedRegulog.GeneFunctions;
			items.Unshift( Constants.Text_OptionAllFunctions );

            // Set the items in the gene function chooser's list
			select_FunctionChooser.Items = items;
		}

        /// <summary>
        /// Highlight TGs with the selected gene function when the selected option in the gene function drop down is changed
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
		private void functionChooser_SelectionChanged( object sender, ElementEventArgs eventArgs ) {

            // Get the currently selected function as a string
			string selectedFunction = (string) Enumerable.FirstOrDefault<object>( select_FunctionChooser.SelectedItems, null );

            // If there is no actual selected function, or it is the "all
            // functions" option, set the highlighter to show TGs as normal
			if ( selectedFunction == Constants.Text_OptionAllFunctions || selectedFunction == null ) {
                UpdateHighlights(null, null, null, PresentInStates.none);
			}

            // Otherwise set the highlighter to highlight TGs with that gene
            // function
			else {
                UpdateHighlights(selectedFunction, null, null, PresentInStates.none);
			}

            // If using the popup for the gene function touch button, hide it
            // now
            if ( touchButton_HighlightFunctionPopup != null) {
                touchButton_HighlightFunctionPopup.Style.Visibility = "hidden";
            }
		}

		/// <summary> Allows the user to sort the networks based on certain factors
		/// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
		private void sortChooser_SelectionChanged( object sender, ElementEventArgs eventArgs ) {

            // Don't do anything if there is a filtered detail view
            // Will be removed once the filtering mechanics are changed to
            // UpdateVisibleRegulons
            /*if (((DisplayManager is HammerGroupRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is GroupRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as GroupRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is DefaultRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as DefaultRegulonDisplay).DetailedViewGraphs.Length > 0)
                || ((DisplayManager is HammerRegulonDisplay) && (DisplayManager as HammerGroupRegulonDisplay).DetailedViewGraphs != null && (DisplayManager as HammerRegulonDisplay).DetailedViewGraphs.Length > 0))
            { return; }*/

            // Get the current sort option
            currentSortOption = select_SortChooser.SelectedIndex;

            // Option 5 (internally 4), sorting by centroid distance, needs to
            // be specially handled by a function in the display manager
            if (currentSortOption == 4) {
                /*if (DisplayManager is HammerGroupFreeRegulonDisplay
                && (DisplayManager as HammerGroupFreeRegulonDisplay).NumberOfGroups > 1) {
                    (DisplayManager as HammerGroupFreeRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }*/
                if (DisplayManager is HammerGroupRegulonDisplay) {
                    (DisplayManager as HammerGroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }
                if (DisplayManager is GroupRegulonDisplay) {
                    (DisplayManager as GroupRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }
                if (DisplayManager is DefaultRegulonDisplay) {
                    (DisplayManager as DefaultRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }
                if (DisplayManager is HammerRegulonDisplay) {
                    (DisplayManager as HammerRegulonDisplay).UpdateVisibleRegulonsSortByCentroidDistance(null);
                }

            // For all other options, use the function in the list of sort
            // functions that corresponds with that option
            } else {
			    DisplayManager.UpdateVisibleRegulons( sortFunctions[currentSortOption], null );
            }
		}

		/// <summary> Compares two regulons by name.
        /// <para> One of the sorting functions </para>
		/// </summary>
		/// <param name="r1">Regulon 1 to compare</param>
		/// <param name="r2">Regulon 2 to compare</param>
		/// <returns>Whether regulon 1 is to be ordered before or after regulon 2</returns>

		private static int RegulogInfo_OrderByName( RegulonInfo r1, RegulonInfo r2 ) {
			return string.Compare( r1.GenomeName, r2.GenomeName, true );
		}

        /// <summary> Compares two regulons by name in reverse order.
        /// <para> One of the sorting functions </para>
		/// </summary>
		/// <param name="r1">Regulon 1 to compare</param>
		/// <param name="r2">Regulon 2 to compare</param>
		/// <returns>Whether regulon 1 is to be ordered before or after regulon 2</returns>

		private static int RegulogInfo_OrderByNameReverse( RegulonInfo r1, RegulonInfo r2 ) {
			return -string.Compare( r1.GenomeName, r2.GenomeName, true );
		}

		/// <summary> Compares two regulons by ascending gene count.
        /// <para> One of the sorting functions </para>
		/// </summary>
		/// <param name="r1">Regulon 1 to compare</param>
		/// <param name="r2">Regulon 2 to compare</param>
		/// <returns>Whether regulon 1 is to be ordered before or after regulon 2</returns>

		private static int RegulogInfo_OrderByGeneCountAsc( RegulonInfo r1, RegulonInfo r2 ) {
			return r1.Genes.Length - r2.Genes.Length;
		}

		/// <summary> Compares two regulons by descending gene count.
        /// <para> One of the sorting functions </para>
		/// </summary>
		/// <param name="r1">Regulon 1 to compare</param>
		/// <param name="r2">Regulon 2 to compare</param>
		/// <returns>Whether regulon 1 is to be ordered before or after regulon 2</returns>

		private static int RegulogInfo_OrderByGeneCountDesc( RegulonInfo r1, RegulonInfo r2 ) {
			return r2.Genes.Length - r1.Genes.Length;
		}

        /// <summary> Used to order networks in the same way they were saved
        /// <para> This is only used when loading a saved dataset, and is not one of the default sorting options in the drop down </para>
		/// </summary>
		/// <param name="r1">Regulon 1 to compare</param>
		/// <param name="r2">Regulon 2 to compare</param>
		/// <returns>Whether regulon 1 is to be ordered before or after regulon 2</returns>

		protected static int RegulogInfo_SavedOrder( RegulonInfo r1, RegulonInfo r2 ) {
			return r1.Order - r2.Order;
		}

        //// <summary> Toggles whether the spokes are scaled
        //// Hopefully changing the property here propogates it to each regulon graph...
        //// </summary>
        //// <param name="sender">The sender of the event</param>
		//// <param name="eventArgs">The parameters of the event</param>

		/*private void scaleChooser_SelectionChanged(object sender, ElementEventArgs eventArgs)
        {
            for (int i = 0; i < SelectedRegulog.Regulons.Length; i++)
            {
                SelectedRegulog.Regulons[i].DoScale = scaleItems[scaleChooser.SelectedIndex];
            }
        }*/



        //// <summary>
        //// Saves the current dataset as a binary vector (old function used for
        //// experimenting with something)
        //// </summary>
        //// <param name="sender">The sender of the event</param>
		//// <param name="eventArgs">The parameters of the event</param>

        /*void bVButton_Clicked(object sender, ElementEventArgs eventArgs)
        {
            if (RequiredDataNotAvailable)
            {
                Window.Alert( Constants.MessageErrorDataLoading );
                return;
            }

            DataLayer.SaveRegulogAsBinaryVector("regulog" + selectedRegulog.RegulogId + ".out", selectedRegulog);
        }*/

        /// <summary>
        /// Clears the current selection when the clear button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void button_Clear_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Clear all the selected genes and regulons
            displayManager.ClearSelectedGenes(true);
            displayManager.ClearSelectedRegulons();

            // Update highlights to show TGs normally
            UpdateHighlights(null, null, null, PresentInStates.none);

            // Update the list of selected homologues
            Script.SetTimeout( (Action) delegate {
            if (label_SelectedGenes != null) {
                UpdateSelectedGenesLabel();
            }
            }, 100);
        }

        /// <summary>
        /// Changes the zoom level of the display when the zoom chooser's
        /// selection is changed
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void zoomChooser_SelectionChanged(object sender, ElementEventArgs eventArgs)
        {
            currentZoomLevel = (ZoomLevels)select_SetZoomLevel.SelectedIndex;
            displayManager.CurrentZoomLevel = currentZoomLevel;
            SetZoomLevelButtonImages();
        }

        /// <summary>
        /// Creates a row for the legend
        /// </summary>
        /// <param name="tableBodyElement">The table to insert the row into</param>
        /// <param name="colour">The colour of the dot, either as a CSS class or an actual colour string</param>
        /// <param name="caption">The label of that entry in the legend</param>
        /// <param name="clickable">Whether the label should be clickable (which highlights all the corresponding TGs in the display)</param>
        /// <returns></returns>
        protected TableRowElement CreateLegendTableRow(Element tableBodyElement, string colour, string caption, bool clickable)
        {
            // Set the cell colour
            TableCellElement cellColour = (TableCellElement)Document.CreateElement("td");

            // Add a bold dot as the content of the "colour" cell
            cellColour.TextContent = "●";
            cellColour.Style.FontWeight = "bold";

            // Set the colour of the dot
            // If using CSS attributes, set the class of the cell to that which
            // was passed, otherwise set the colour style to the colour that
            // was passed
            if (Constants.UseCssNodeAttributes) {
#if DEBUG
                // Append the necessary characters if in "colour blind" mode
                if (AppMain.ColourBlind) {
                    colour = "CB_" + colour;
                }
#endif
                cellColour.ClassName = colour;
            } else {
                cellColour.Style.Color = colour;
            }

            // Set the width of the "colour" cell
            cellColour.Style.Width = "12px";

            // Create the cell for containing the label, and configure it
            TableCellElement cellText = (TableCellElement)Document.CreateElement("td");
            cellText.TextContent = caption;
            cellText.Style.TextAlign = "left";
            cellText.ClassName = "LegendText";

            // If this is a clickable label, set the cursor to appear as a
            // pointer when it hovers over
            if (clickable) {
                cellText.Style.Cursor = "pointer";
            }

            // Store the cell in the appropriate variable if it is part of the
            // legend for a comparison display (to allow for text changing if
            // switched between pair and multiple comparisons)
            // TGs in both/mutiple networks
            if (caption == Constants.Text_LegendCaptionBoth || caption == Constants.Text_LegendCaptionBothMultiple) {
                LegendCaptionBothCell = cellText;
            }

            // TGs only in the left/reference network
            if (caption == Constants.Text_LegendCaptionLeft || caption == Constants.Text_LegendCaptionLeftMultiple) {
                LegendCaptionLeftCell = cellText;
            }

            // TGs only in the right/non-reference network(s)
            if (caption == Constants.Text_LegendCaptionRight || caption == Constants.Text_LegendCaptionRightMultiple) {
                LegendCaptionRightCell = cellText;
            }

            // Create the actual row element and add the two cells
            TableRowElement row = (TableRowElement)Document.CreateElement("tr");
            row.AppendChild(cellColour);
            row.AppendChild(cellText);

            // Add an event listener to the label cell
            if (clickable) {
                cellText.AddEventListener(MouseEventType.click, LegendTextOnClick, false);
            }

            // Add the row to the table and return it
            tableBodyElement.AppendChild(row);
            return row;
        }

        /// <summary>
        /// Creates a legend from the gene functions in the data set
        /// </summary>
        /// <param name="includeExtras">Whether to include extra functions in the caption if multiple functions have the same colour
        /// <para>If false, the caption will instead have "(plus X more)"</para>
        /// </param>
        protected void CreateGeneFunctionLegend(bool includeExtras)
        {
            // Create a dictionary to store all functions in the dataset
            Dictionary<string, int> legendFunctions = new Dictionary<string, int>();

            // Store the index of each function in the dictionary
            for (int i = 0; i < selectedRegulog.GeneFunctions.Length; i++)
            {
                legendFunctions[selectedRegulog.GeneFunctions[i]] = i;
            }

            // Create a list of captions plus a list of "extra functions" for
            // if there are multiple functions for a specific colour (only used
            // if includeExtras is false)
            // The number of captions is determined by getting the length of the
            // colour palette from the current colour chooser
            string[] legendCaptions = new string[colourChooser.ColourPalette.Length];
            int[] numExtraFunctions = new int[colourChooser.ColourPalette.Length];

            // Show a warning if there are more gene functions than colours
            if (legendFunctions.Count > colourChooser.ColourPalette.Length) {
                if (Constants.ShowDebugMessages) Console.Log(String.Format(Constants.Text_MessageNotEnoughColoursGF, legendFunctions.Count, colourChooser.ColourPalette.Length));
#if DEBUG
                Window.Alert(String.Format(Constants.Text_MessageNotEnoughColoursGF, legendFunctions.Count, colourChooser.ColourPalette.Length));
#endif
            }

            // Go through each function in the dictionary
            foreach (KeyValuePair<string, int> pair in legendFunctions)
            {

                // The index for the caption is determined with the modulus of
                // the function's index with the length of the list of captions.
                // This is similar to how TG colour is determined in the colour
                // chooser, and works because the captions list is the same
                // length as the palette

                // If there is not already a caption for this colour...
                if (legendCaptions[pair.Value % legendCaptions.Length] == null)
                {
                    // Set the caption for this colour to the current function
                    legendCaptions[pair.Value % legendCaptions.Length] = pair.Key;

                    // Set the number of extras to zero initially, if extra
                    // gene functions are set to not appear
                    if (!includeExtras) {
                        numExtraFunctions[pair.Value % legendCaptions.Length] = 0;
                    }
                }

                // Otherwise, add it to the existing caption in one of two ways
                else
                {
                    // If includeExtras is true, add the new function to the
                    // caption including a divider between it and the previous
                    // ones
                    if (includeExtras) {
                        legendCaptions[pair.Value % legendCaptions.Length] += " | " + pair.Key;

                    // Else add one to the number of extra functions
                    } else {
                        numExtraFunctions[pair.Value % legendCaptions.Length] += 1;
                    }
                }
            }

            // If the legend already has entries in it, remove them all
            while (table_Legend.ChildNodes.Length > 0)
            {
                table_Legend.RemoveChild(table_Legend.ChildNodes[0]);
            }

            // Create a body for the new legend
            Element legendBody = Document.CreateElement("tbody");

            // Include a row for the regulator
            // If there are no regulators for this dataset, display a different
            // colour dot (corresponding to the colour used in the actual
            // network)
            if (selectedRegulog.Regulators != null && selectedRegulog.Regulators.Length > 0 && selectedRegulog.Regulators[0].VimssId > 0) {
                CreateLegendTableRow(legendBody, colourChooser.RegulatorColour, Constants.Text_LegendCaptionRegulator, false);
            } else {
                CreateLegendTableRow(legendBody, colourChooser.NoRegulatorColour, Constants.Text_LegendCaptionNoRegulator, false);
            }

            // Start creating the rows for all the captions
            for (int i = 0; i < legendCaptions.Length; i++)
            {
                // Don't create rows for empty captions
                if (!String.IsNullOrEmpty(legendCaptions[i])) {

                    // If includeExtras is false, and there are extra
                    // functions for this colour add "(plus X more)" to the end
                    // of this caption, with X as the number of extras
                    if (!includeExtras && numExtraFunctions[i] > 0)
                    {
                        legendCaptions[i] += (" (plus " + numExtraFunctions[i] + " more)");
                    }

                    // Create the row using the correct colour from the colour
                    // chooser
                    CreateLegendTableRow(legendBody, colourChooser.ColourPalette[i], legendCaptions[i], true);
                }
            }

            // Add the body element to the table
            table_Legend.AppendChild(legendBody);
        }

        /// <summary>
        /// Creates a legend from the GO terms in the data set
        /// </summary>
        /// <param name="includeExtras">Whether to include extra terms in the caption if multiple terms have the same colour
        /// <para>If false, the caption will instead have "(plus X more)"</para>
        /// </param>
        protected void CreateGeneGOTermLegend(bool includeExtras)
        {
            // Create a dictionary to store all terms in the dataset
            Dictionary<string, int> legendTerms = new Dictionary<string, int>();

            // As there is not a list of terms like there are functions in the
            // dataset, one must be made here
			List<string> terms = new List<string>();

            // Add all the GO terms to the list, as well as "unknown" for any
            // target genes that have no GO term
            foreach (GeneInfo gene in selectedRegulog.Genes) {
                string term = (gene.GoParentTerm == string.Empty) ? Constants.Text_LegendUnknown : gene.GoParentTerm.ToLowerCase();

                // Only add new terms
                if (!terms.Contains(term)) {
                    terms.Add(term);
                }
            }

            // Create the dictionary of terms by going through the list and
            // storing the index for each as the value
            for (int i = 0; i < terms.Length; i++)
            {
                legendTerms[terms[i]] = i;
            }

            // Create a list of captions plus a list of "extra terms" for if
            // there are multiple terms for a specific colour (only used if
            // includeExtras is false)
            // The number of captions is determined by getting the length of the
            // colour palette from the current colour chooser
            string[] legendCaptions = new string[colourChooser.ColourPalette.Length];
            int[] numExtraTerms = new int[colourChooser.ColourPalette.Length];

            // Show a warning if there are more GO terms than colours
            if (legendTerms.Count > colourChooser.ColourPalette.Length) {
                if (Constants.ShowDebugMessages) Console.Log(String.Format(Constants.Text_MessageNotEnoughColoursGO, legendTerms.Count, colourChooser.ColourPalette.Length));
#if DEBUG
                Window.Alert(String.Format(Constants.Text_MessageNotEnoughColoursGO, legendTerms.Count, colourChooser.ColourPalette.Length));
#endif
            }

            // Go through each function in the dictionary
            foreach (KeyValuePair<string, int> pair in legendTerms)
            {

                // The index for the caption is determined with the modulus of
                // the terms's index with the length of the list of captions.
                // This is similar to how TG colour is determined in the colour
                // chooser, and works because the captions list is the same
                // length as the palette

                // If there is not already a caption for this colour...
                if (legendCaptions[pair.Value % legendCaptions.Length] == null)
                {
                    // Set the caption for this colour to the current function
                    legendCaptions[pair.Value % legendCaptions.Length] = pair.Key;

                    // Set the number of extras to zero initially, if extra
                    // gene functions are set to not appear
                    if (!includeExtras) {
                        numExtraTerms[pair.Value % legendCaptions.Length] = 0;
                    }
                }

                // Otherwise, add it to the existing caption in one of two ways
                else
                {
                    // If includeExtras is true, add the new function to the
                    // caption including a divider between it and the previous
                    // ones
                    if (includeExtras) {
                        legendCaptions[pair.Value % legendCaptions.Length] += " | " + pair.Key;

                    // Else add one to the number of extra functions
                    } else {
                        numExtraTerms[pair.Value % legendCaptions.Length] += 1;
                    }
                }
            }

            // If the legend already has entries in it, remove them all
            while (table_Legend.ChildNodes.Length > 0)
            {
                table_Legend.RemoveChild(table_Legend.ChildNodes[0]);
            }

            // Create a body for the new legend
            Element legendBody = Document.CreateElement("tbody");

            // Include a row for the regulator
            // If there are no regulators for this dataset, display a different
            // colour dot (corresponding to the colour used in the actual
            // network)
            if (selectedRegulog.Regulators != null && selectedRegulog.Regulators.Length > 0 && selectedRegulog.Regulators[0].VimssId > 0) {
                CreateLegendTableRow(legendBody, colourChooser.RegulatorColour, Constants.Text_LegendCaptionRegulator, false);
            } else {
                CreateLegendTableRow(legendBody, colourChooser.NoRegulatorColour, Constants.Text_LegendCaptionNoRegulator, false);
            }

            // Start creating the rows for all the captions
            for (int i = 0; i < legendCaptions.Length; i++)
            {
                // Don't create rows for empty captions
                if (!String.IsNullOrEmpty(legendCaptions[i])) {

                    // If includeExtras is false, and there are extra
                    // terms for this colour add "(plus X more)" to the end of
                    // this caption, with X as the number of extras
                    if (!includeExtras && numExtraTerms[i] > 0)
                    {
                        legendCaptions[i] += (" (plus " + numExtraTerms[i] + " more)");
                    }

                    // Create the row using the correct colour from the colour
                    // chooser
                    CreateLegendTableRow(legendBody, colourChooser.ColourPalette[i], legendCaptions[i], true);
                }
            }

            // Add the body element to the table
            table_Legend.AppendChild(legendBody);
        }

        protected TableCellElement LegendCaptionBothCell;
        protected TableCellElement LegendCaptionLeftCell;
        protected TableCellElement LegendCaptionRightCell;
        protected TableRowElement LegendRowSome;

        /// <summary>
        /// Creates a legend for the comparison displays
        /// </summary>
        protected void CreateComparisonLegend()
        {
            // Create a body for the new legend
            Element legendBody = Document.CreateElement("tbody");

            // Create each row of the legend. The "some" row is created even if
            // the current comparison is only a pair - it is hidden until
            // required

            // If CSS node attributes are enabled, the class name for each row
            // is passed for the colour
            if (Constants.UseCssNodeAttributes) {
                //Both row
                CreateLegendTableRow(legendBody, "CompareColourLeft", Constants.Text_LegendCaptionLeft, true);

                //Left row
                CreateLegendTableRow(legendBody, "CompareColourRight", Constants.Text_LegendCaptionRight, true);

                //Right row
                CreateLegendTableRow(legendBody, "CompareColourAll", Constants.Text_LegendCaptionBoth, true);

                //Some row
                LegendRowSome = CreateLegendTableRow(legendBody, "CompareColourSome", Constants.Text_LegendCaptionSomeMultiple, true);

            // Otherwise, the colour is used directly
            // (This should probably be a reference to a constant or the colour
            // chooser rather than hard coded)
            } else {
            //Both row
            CreateLegendTableRow(legendBody, "#007F00", Constants.Text_LegendCaptionBoth, true);

            //Left row
            CreateLegendTableRow(legendBody, "#FF0000", Constants.Text_LegendCaptionLeft, true);

            //Right row
            CreateLegendTableRow(legendBody, "#0000FF", Constants.Text_LegendCaptionRight, true);

            //Some row
            LegendRowSome = CreateLegendTableRow(legendBody, "#7F3300", Constants.Text_LegendCaptionSomeMultiple, true);
            }

            // Add the body element to the table
            table_Legend.AppendChild(legendBody);
        }

        /// <summary>
        /// Listener for when the toggle side panel button is clicked. Shows or hides the side panel, which contains the legend
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void toggleSidePanel_clicked(object sender, ElementEventArgs eventArgs)
        {
            // Do not do anything if there is no data set currently loaded
            if (selectedRegulog != null) {

                // If the side panel is currently hidden
                if (div_SidePanel.Style.Visibility == "hidden") {

                    // Make it visible
                    div_SidePanel.Style.Visibility = "visible";

                    // Change the class of the toggle button to what it should
                    // be when the side panel is visible
                    button_ToggleSidePanel.DomElement.ClassName = "TouchButtonSmall SidePanelHide";

                    // Set the container for the display to a new class which
                    // allows room for the legend
                    div_RegulonDisplayHolder.ClassName = "RegulonDisplayHolderPlusLegend";

                // If the side pannel is currently visible
                } else {

                    // Make it hidden
                    div_SidePanel.Style.Visibility = "hidden";

                    // Change the class of the toggle button to what it should
                    // be when the side panel is hidden
                    button_ToggleSidePanel.DomElement.ClassName = "TouchButtonSmall SidePanelShow";

                    // Set the container for the display back to the original
                    // class
                    div_RegulonDisplayHolder.ClassName = "RegulonDisplayHolder";
                }
            }
        }

        // Changes the size of elements in the side panel if the screen is
        // large enough
        private void UpdateSidePanelClass() {
            if (div_SidePanel.Style.Visibility == "hidden") {
                button_ToggleSidePanel.DomElement.ClassName = "TouchButtonSmall SidePanelShow";
            } else {
                button_ToggleSidePanel.DomElement.ClassName = "TouchButtonSmall SidePanelHide";
            }
        }

        /// <summary>
        /// Updates the class of the side panel when the size of the window is changed
        /// </summary>
        /// <param name="e">The element event that was sent by the window</param>
        private void Window_OnResize(ElementEvent e)
        {
            UpdateSidePanelClass();
        }

        /// <summary>
        /// Changes the zoom level to "Icon list" when the first zoom level button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_ZoomLevel1_clicked(object sender, ElementEventArgs eventArgs)
        {
            // Set the current zoom level
            currentZoomLevel = ZoomLevels.IconList;

            // Set the index of the zoom drop down to the "Icon list" entry
            // if it exists
            if (select_SetZoomLevel != null) {
                select_SetZoomLevel.SelectedIndex = (int)currentZoomLevel;
            }

            // Change the zoom level in the display manager
            displayManager.CurrentZoomLevel = currentZoomLevel;

            // Update the button images
            SetZoomLevelButtonImages();
        }

        /// <summary>
        /// Changes the zoom level to "Small overview" when the second zoom level button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_ZoomLevel2_clicked(object sender, ElementEventArgs eventArgs)
        {
            // Set the current zoom level
            currentZoomLevel = ZoomLevels.SmallOverview;

            // Set the index of the zoom drop down to the "Small overview"
            // entry if it exists
            if (select_SetZoomLevel != null) {
                select_SetZoomLevel.SelectedIndex = (int)currentZoomLevel;
            }

            // Change the zoom level in the display manager
            displayManager.CurrentZoomLevel = currentZoomLevel;

            // Update the button images
            SetZoomLevelButtonImages();
        }

        /// <summary>
        /// Changes the zoom level to "Large overview" when the third zoom level button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_ZoomLevel3_clicked(object sender, ElementEventArgs eventArgs)
        {
            // Set the current zoom level
            currentZoomLevel = ZoomLevels.LargeOverview;

            // Set the index of the zoom drop down to the "Large overview"
            // entry if it exists
            if (select_SetZoomLevel != null) {
                select_SetZoomLevel.SelectedIndex = (int)currentZoomLevel;
            }

            // Change the zoom level in the display manager
            displayManager.CurrentZoomLevel = currentZoomLevel;

            // Update the button images
            SetZoomLevelButtonImages();
        }

        /// <summary>
        /// Changes the zoom level to "Detail view" when the fourth zoom level button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_ZoomLevel4_clicked(object sender, ElementEventArgs eventArgs)
        {
            // Set the current zoom level
            currentZoomLevel = ZoomLevels.DetailView;

            // Set the index of the zoom drop down to the "Detail view" entry
            // if it exists
            if (select_SetZoomLevel != null) {
                select_SetZoomLevel.SelectedIndex = (int)currentZoomLevel;
            }

            // Change the zoom level in the display manager
            displayManager.CurrentZoomLevel = currentZoomLevel;

            // Update the button images
            SetZoomLevelButtonImages();
        }

        /// <summary>
        /// Sets the images for the zoom level buttons after one is clicked
        /// </summary>
        private void SetZoomLevelButtonImages()
        {
            // Depending on the current zoom level...
            switch (currentZoomLevel)
            {
                // Set the "Icon list" button as active and all the rest as
                // inactive
                case ZoomLevels.IconList:
                    touchButton_ZoomLevel1.DomElement.ClassName = "TouchButton ZoomLevel1Active";
                    touchButton_ZoomLevel2.DomElement.ClassName = "TouchButton ZoomLevel2Inactive";
                    touchButton_ZoomLevel3.DomElement.ClassName = "TouchButton ZoomLevel3Inactive";
                    touchButton_ZoomLevel4.DomElement.ClassName = "TouchButton ZoomLevel4Inactive";
                    break;

                // Set the "Small overview" button as active and all the rest
                // as inactive
                case ZoomLevels.SmallOverview:
                    touchButton_ZoomLevel1.DomElement.ClassName = "TouchButton ZoomLevel1Inactive";
                    touchButton_ZoomLevel2.DomElement.ClassName = "TouchButton ZoomLevel2Active";
                    touchButton_ZoomLevel3.DomElement.ClassName = "TouchButton ZoomLevel3Inactive";
                    touchButton_ZoomLevel4.DomElement.ClassName = "TouchButton ZoomLevel4Inactive";
                    break;

                // Set the "Large overview" button as active and all the rest
                // as inactive
                case ZoomLevels.LargeOverview:
                    touchButton_ZoomLevel1.DomElement.ClassName = "TouchButton ZoomLevel1Inactive";
                    touchButton_ZoomLevel2.DomElement.ClassName = "TouchButton ZoomLevel2Inactive";
                    touchButton_ZoomLevel3.DomElement.ClassName = "TouchButton ZoomLevel3Active";
                    touchButton_ZoomLevel4.DomElement.ClassName = "TouchButton ZoomLevel4Inactive";
                    break;

                // Set the "Detail view" button as active and all the rest as
                // inactive
                case ZoomLevels.DetailView:
                    touchButton_ZoomLevel1.DomElement.ClassName = "TouchButton ZoomLevel1Inactive";
                    touchButton_ZoomLevel2.DomElement.ClassName = "TouchButton ZoomLevel2Inactive";
                    touchButton_ZoomLevel3.DomElement.ClassName = "TouchButton ZoomLevel3Inactive";
                    touchButton_ZoomLevel4.DomElement.ClassName = "TouchButton ZoomLevel4Active";
                    break;
            }
        }

        /// <summary>
        /// Listener for when the selected GO term in the term drop down box is changed
        /// <para>Changes the highlighter so that TGs with that GO term are highilghted (or back to the normal highlighter if "all terms" is selected)</para>
        /// <para>The drop down is not used directly - it is hidden and is changed programmatically by the new touch button drop down</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void termChooser_SelectionChanged(object sender, ElementEventArgs eventArgs)
        {
            // Get the selected term from the drop down
			string selectedTerm = (string) Enumerable.FirstOrDefault<object>( select_TermChooser.SelectedItems, null );

            // If "all terms" (or, somehow, no option at all) is selected,
            // return the highlighter to normal
			if ( selectedTerm == Constants.Text_OptionAllTerms || selectedTerm == null ) {
                UpdateHighlights(null, null, null, PresentInStates.none);
			}

            // Otherwise, set the highlighter to highlight those TGs with the
            // selected GO term
			else {
                UpdateHighlights(null, selectedTerm, null, PresentInStates.none);
			}

            // Hide the touch button's popup if it is still visible
            if ( touchButton_HighlightTermPopup != null) {
                touchButton_HighlightTermPopup.Style.Visibility = "hidden";
            }
        }

		/// <summary> Updates the content of the GO term chooser to reflect the current set of GO terms.
		/// </summary>

        protected void RefreshTermChooser() {

            // Don't do anything if there is no data set
			if ( selectedRegulog == null ) return;

            // Create the list of terms
            List<string> termItems = new List<string>();

            // Add "all terms" to the start of the list
            termItems.Add( Constants.Text_OptionAllTerms );

            // Go through each gene in the data set and store their GO term in
            // the list - "unknown" is used for TGs that have no GO term
            foreach (GeneInfo gene in selectedRegulog.Genes) {
                string term = (gene.GoParentTerm == string.Empty) ? Constants.Text_LegendUnknown : gene.GoParentTerm.ToLowerCase();

                // Don't add duplicates
                if (!termItems.Contains(term)) {
                    termItems.Add(term);
                }
            }

            // If the termChooser exists...
            if (select_TermChooser != null) {

                // Create an array of items for it and then add all of the
                // GO terms in the list to the array
                string[] termItemsArray = new string[termItems.Length];
                for (int i = 0; i < termItems.Length; i++) {
                    termItemsArray[i] = termItems[i];
                }

                // Set the termChooser's options as the array
                select_TermChooser.Items = termItemsArray;
            }

            // If the GO term touch button exists...
            if (touchButton_HighlightTerm != null) {

                // Clear all options that were previously in the drop down
                while (touchButton_HighlightTermPopup.ChildNodes.Length > 0)
                {
                    touchButton_HighlightTermPopup.RemoveChild(touchButton_HighlightTermPopup.ChildNodes[0]);
                }

                // Add a new option for every term in the list
                foreach (string item in termItems) {

                    // Each option uses a clickable label element
                    Element label = Document.CreateElement("label");

                    // Set the label text and attributes
                    label.TextContent = item;
                    label.ClassName = "ChooserPopupLabel";

                    // Add a listener to the label
                    label.AddEventListener(MouseEventType.click, TermLabelOnClick, false);

                    // Add the label to the drop down
                    touchButton_HighlightTermPopup.AppendChild(label);
                }
            }
        }

        // Used to check if the user clicked on the highlight gene function
        // touch button
        bool touchButton_HighlightFunction_justClicked = false;

        /// <summary>
        /// Listener for if the highlight gene function touch button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_HighlightFunction_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Make the drop down visible
            touchButton_HighlightFunctionPopup.Style.Visibility = "visible";

            // Set the button as just being clicked
            touchButton_HighlightFunction_justClicked = true;
        }

        // Used to check if the user clicked on the highlight GO term touch
        // button
        bool touchButton_HighlightTerm_justClicked = false;

        /// <summary>
        /// Listener for if the highlight GO term touch button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_HighlightTerm_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Make the drop down visible
            touchButton_HighlightTermPopup.Style.Visibility = "visible";

            // Set the button as just being clicked
            touchButton_HighlightTerm_justClicked = true;
        }

        /// <summary>
        /// Does things when the user clicks somewhere on the body of the page
        /// </summary>
        /// <param name="e">The element event that was sent by the body</param>
        private void BodyOnClick(ElementEvent e)
        {
            // Get the location of the cursor
            int mouseX = (int)Script.Literal("Math.round(e.clientX)");
            int mouseY = (int)Script.Literal("Math.round(e.clientY)");

            // If the highlight gene function touch button has just been
            // clicked, set that status to false
            if ( touchButton_HighlightFunction_justClicked ) {
                touchButton_HighlightFunction_justClicked = false;

            // Otherwise if the drop down exists, and the mouse is outside of
            // the drop down, hide it
            } else {
                if ( touchButton_HighlightFunctionPopup != null) {
                    if (mouseX < HtmlUtil.GetElementX(touchButton_HighlightFunctionPopup) || mouseX > HtmlUtil.GetElementX(touchButton_HighlightFunctionPopup) + touchButton_HighlightFunctionPopup.ClientWidth
                        || mouseY < HtmlUtil.GetElementY(touchButton_HighlightFunctionPopup) || mouseY > HtmlUtil.GetElementY(touchButton_HighlightFunctionPopup) + touchButton_HighlightFunctionPopup.ClientHeight ) {
                        touchButton_HighlightFunctionPopup.Style.Visibility = "hidden";
                    }
                }
            }

            // If the highlight GO term touch button has just been clicked, set
            // that status to false
            if ( touchButton_HighlightTerm_justClicked ) {
                touchButton_HighlightTerm_justClicked = false;

            // Otherwise if the drop down exists, and the mouse is outside of
            // the drop down, hide it
            } else {
                if ( touchButton_HighlightTermPopup != null) {
                    if (mouseX < HtmlUtil.GetElementX(touchButton_HighlightTermPopup) || mouseX > HtmlUtil.GetElementX(touchButton_HighlightTermPopup) + touchButton_HighlightTermPopup.ClientWidth
                        || mouseY < HtmlUtil.GetElementY(touchButton_HighlightTermPopup) || mouseY > HtmlUtil.GetElementY(touchButton_HighlightTermPopup) + touchButton_HighlightTermPopup.ClientHeight ) {
                        touchButton_HighlightTermPopup.Style.Visibility = "hidden";
                    }
                }
            }

            // If the search term touch button has just been clicked, set that
            // status to false
            if ( touchButton_SearchTerm_justClicked ) {
                touchButton_SearchTerm_justClicked = false;

            // Otherwise if the drop down exists, and the mouse is outside of
            // the drop down, hide it
            } else {
                if ( touchButton_SearchTermPopup != null) {
                    if (mouseX < HtmlUtil.GetElementX(touchButton_SearchTermPopup) || mouseX > HtmlUtil.GetElementX(touchButton_SearchTermPopup) + touchButton_SearchTermPopup.ClientWidth
                        || mouseY < HtmlUtil.GetElementY(touchButton_SearchTermPopup) || mouseY > HtmlUtil.GetElementY(touchButton_SearchTermPopup) + touchButton_SearchTermPopup.ClientHeight ) {
                        touchButton_SearchTermPopup.Style.Visibility = "hidden";
                    }
                }
            }

        }

        /// <summary>
        /// Listener triggered when the user clicks on an option (label) in the highlight GO term touch button drop down
        /// <para>Highlights all TGs in the display with the selected GO term, or returns the highlighter to normal if it was "all terms"</para>
        /// </summary>
        /// <param name="e">The element event that was sent by the label</param>
        private void TermLabelOnClick (ElementEvent e) {

            // Get the selected term from the drop down
            string selectedTerm = e.Target.TextContent;

            // If "all terms" (or, somehow, no option at all) is selected,
            // return the highlighter to normal
            if ( selectedTerm == Constants.Text_OptionAllTerms || selectedTerm == null ) {
                UpdateHighlights(null, null, null, PresentInStates.none);
			}

            // Otherwise, set the highlighter to highlight those TGs with the
            // selected GO term
			else {
                UpdateHighlights(null, selectedTerm, null, PresentInStates.none);
			}

            // Hide the touch button's popup if it is still visible
            if ( touchButton_HighlightTermPopup != null) {
                touchButton_HighlightTermPopup.Style.Visibility = "hidden";
            }
        }

        /// <summary>
        /// Listener triggered when the user clicks on an caption (label) in the legend
        /// <para>Highlights all TGs in the display with the corresponding colour</para>
        /// <para>Note: this will currently not work when a caption is showing more than one function/term, and not all of them are shown (i.e. includeExtras was false when creating the legend)</para>
        /// </summary>
        /// <param name="e">The element event that was sent by the label</param>
        private void LegendTextOnClick(ElementEvent e)
        {
            // Get the label that triggered the event
            Element target = e.Target;

            // If it was from the comparison legend and for "both/all" TGs,
            // highlight those
            if (target.TextContent == Constants.Text_LegendCaptionBoth || target.TextContent == Constants.Text_LegendCaptionBothMultiple) {
                UpdateHighlights(null, null, null, PresentInStates.all);
            }

            // If it was from the comparison legend and for "left/reference"
            // TGs, highlight those
            else if (target.TextContent == Constants.Text_LegendCaptionLeft || target.TextContent == Constants.Text_LegendCaptionLeftMultiple) {
                UpdateHighlights(null, null, null, PresentInStates.left);
            }

            // If it was from the comparison legend and for "right/non-reference"
            // TGs, highlight those
            else if (target.TextContent == Constants.Text_LegendCaptionRight || target.TextContent == Constants.Text_LegendCaptionRightMultiple) {
                UpdateHighlights(null, null, null, PresentInStates.right);
            }

            // If it was from the comparison legend and for "some" TGs in a
            // multiple comparison, highlight those
            else if (target.TextContent == Constants.Text_LegendCaptionSomeMultiple) {
                UpdateHighlights(null, null, null, PresentInStates.some);

            // Else if it was from a regular view's legend
            } else {

                // Get the string from the label
                string text = target.TextContent;

                // If GO terms are being used, highlight TGs with that GO term
                if (Constants.UseGoTerms) {
                    UpdateHighlights(null, text, null, PresentInStates.none);

                // Otherwise highlight TGs with that gene function
                } else {
                    UpdateHighlights(text, null, null, PresentInStates.none);
                }
            }
        }

        /// <summary>
        /// An alternate highlighter that highlights selected genes if there are any
        /// </summary>
        /// <param name="gene">The TG to set the highlighting for</param>
        /// <returns>Returns the highlight mode to use on this TG</returns>
        protected HighlightMode SelectedGeneHighlighter ( IGeneInfo gene ) {
            if ( gene is GeneInfo ) {
/*#if DEBUG
                //Console.Log("Is gene " + gene.Name + " of regulon " + gene.RegulonId + " selected? " + (gene as GeneInfo).IsSelected + ". What about the underlying gene object? " + (gene as GeneInfo).Gene["selected"]);
                if ((gene as GeneInfo).Gene["annotation"] != null) {
                    if (Constants.ShowDebugMessages) Console.Log("Currently checking if gene " + gene.Name + " with annotation '" + (gene as GeneInfo).Gene["annotation"] + "' is selected.");
                    if (Constants.ShowDebugMessages) Console.Log("gene.IsSelected = " + (gene as GeneInfo).IsSelected + ", gene.Gene[\"selected\"] = " + (gene as GeneInfo).Gene["selected"]);
                }
#endif*/
				return ((gene as GeneInfo).IsSelected || (bool)(gene as GeneInfo).Gene["selected"]) ? HighlightMode.Selected : HighlightMode.Normal;
            } else {
                return HighlightMode.Normal;
            }
        }

        /// <summary>
        /// Listener for when selected genes change in the display manager
        /// <para>Updates the highlighting so that selected genes, if any, are highlighted</para>
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The event that was sent</param>
        protected void DisplayManager_SelectedGenesChanged(object sender, EventArgs e)
        {
            UpdateHighlights(currentSelectedFunction, currentSelectedTerm, currentSearchQuery, currentPresentInStates);

            // Update the list of selected homologues
            if (label_SelectedGenes != null) {
                UpdateSelectedGenesLabel();
            }
        }

        /// <summary>
        /// Listener for when the button in the highlight search term popup is clicked
        /// <para>Highilghts genes that contain the specified query string somewhere in their data</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void geneFunction_SearchButton_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // If there are contents in the text box, search through the genes
            // using that string to see if they should be highlighted
            if (textBox_GeneFunctionSearch.Text.Length > 0) {
                UpdateHighlights(null, null, textBox_GeneFunctionSearch.Text, PresentInStates.none);

            // If the text box is empty, clear the highlighting
            } else {
                UpdateHighlights(null, null, null, PresentInStates.none);
            }
        }

        // Used to check if the user clicked on the highlight search term touch
        // button
        bool touchButton_SearchTerm_justClicked = false;

        /// <summary>
        /// Listener for if the highlight search term touch button is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        private void touchButton_SearchTerm_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Make the drop down visible
            touchButton_SearchTermPopup.Style.Visibility = "visible";

            // Set the button as just being clicked
            touchButton_SearchTerm_justClicked = true;
        }

        /// <summary>
        /// Listener for when properties in the display manager are changed
        /// <para>Currently checks for the zoom level or processing status changing</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        protected void DisplayManager_PropertyChanged(object sender, SystemQut.ComponentModel.PropertyChangedEventArgs eventArgs)
        {
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("DisplayManager_PropertyChanged triggered in RegPreciseGraphViewerBase");
#endif*/

            // If the zoom level is changed...
            if (eventArgs.Includes("ZoomLevel"))
            {
                // Store the new zoom level
                currentZoomLevel = DisplayManager.CurrentZoomLevel;

                // Change the zoomChooser to the correct item
                if (select_SetZoomLevel != null) {
                    select_SetZoomLevel.SelectedIndex = (int)currentZoomLevel;
                }

                // Update the images for the zoom touch buttons
                SetZoomLevelButtonImages();
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("ZoomLevel changed by the display manager internally");
#endif*/
            }

            // If the processing status is changed, show the loading overlay if
            // it is processing, and hide it if it is not
            if (eventArgs.Includes("IsProcessing"))
            {
                if (DisplayManager.IsProcessing) {
/*#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Attempting to display loading overlay");
#endif*/
                    div_LoadingOverlay.Style.Visibility = "visible";
                } else {
/*#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Attempting to hide loading overlay");
#endif*/
                    div_LoadingOverlay.Style.Visibility = "hidden";
                }
            }
        }

#if DEBUG
        /// <summary>
        /// Allows the user to clear the overlay if it gets stuck on the screen.
        /// They should not be able to remove it while the application is
        /// loading something as the page does not respond
        /// </summary>
        /// <param name="e">The element event that was sent by the overlay</param>
        private void LoadingOverlayClicked(ElementEvent e)
        {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("LoadingOverlayClicked");
            if (Constants.ShowDebugMessages) Console.Log("Attempting to hide loading overlay");
#endif
            if (!DisplayManager.IsProcessing) {
                div_LoadingOverlay.Style.Visibility = "hidden";
            }
        }
#endif
        /// <summary>
        /// Allows the highlight query text box to search on pressing enter
        /// </summary>
        /// <param name="e">The element event that was sent by the text box</param>
        private void geneFunction_SearchText_KeyUp(ElementEvent e)
        {
            // If the enter key was pressed...
            if ((bool)Script.Literal("e.key == 'Enter'")) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("User pressed enter in the highlight query text box");
#endif*/
                // Force the function that activates when the search button is clicked
                geneFunction_SearchButton_Clicked(textBox_GeneFunctionSearch, null);
            }
        }

        // New Highligter that highlights both selected genes and GO Terms/gene
        // functions at the same time
        // For it to work properly, the current thing that should be
        // highlighted needs to be stored
        protected string currentSelectedFunction = null;
        protected string currentSelectedTerm = null;
        protected string currentSearchQuery = null;
        protected PresentInStates currentPresentInStates = PresentInStates.none;

        /// <summary>
        /// The new highlighter that is able to show selected TGs and highlighted TGs simultaneously
        /// </summary>
        /// <param name="gene">The TG to set the highlighting for</param>
        /// <returns>Returns the highlight mode to use on this TG</returns>
        protected HighlightMode NewGeneHighlighter ( IGeneInfo gene ) {

            if ( gene is GeneInfo ) {
                // Selected genes take precedence other all others, and appear
                // differently to highlighted genes
                if ( (gene as GeneInfo).IsSelected ) {
                    return HighlightMode.Selected;
                }
                // If there is a search query, highlight all TGs that have the
                // string in their GO term, gene function, annotation, name, or
                // site
                if ( currentSearchQuery != null &&
                    ((gene as GeneInfo).GoParentTerm.ToLowerCase().IndexOf(currentSearchQuery.ToLowerCase()) > -1
                            || (gene as GeneInfo).GeneFunction.ToLowerCase().IndexOf(currentSearchQuery.ToLowerCase()) > -1
                            || ((gene as GeneInfo).Annotation != null && (gene as GeneInfo).Annotation.ToLowerCase().IndexOf(currentSearchQuery.ToLowerCase()) > -1)
                            || (gene as GeneInfo).Name.ToLowerCase().IndexOf(currentSearchQuery.ToLowerCase()) > -1
                            || ((gene as GeneInfo).Site != null && (gene as GeneInfo).Site.Sequence.ToLowerCase().IndexOf(currentSearchQuery.ToLowerCase()) > -1))) {
                    return HighlightMode.HighLight;
                }

                // If there is a selected GO Term, highlight all TGs that have
                // that GO term. If there were multiple GO terms in the passed
                // string (i.e. it came from a legend that showed the extra
                // terms), split the string and highlight TGs with any of the
                // supplied terms
                if ( currentSelectedTerm != null ) {

                    string[] terms = currentSelectedTerm.Split(" | ");
                    foreach (string term in terms) {
                        if (String.Compare( (gene as GeneInfo).GoParentTerm, term, true ) == 0
                            // Also have a special case for if the supplied
                            // query text is "unknown" - check for an empty GO
                            // term as well
                            || (term == Constants.Text_LegendUnknown && String.IsNullOrEmpty((gene as GeneInfo).GoParentTerm))
                            ) {
                            return HighlightMode.HighLight;
                        }
                    }
                }
                // If there is a selected gene function, highlight all TGs that
                // have that gene function. If there were multiple gene
                // functions in the passed string (i.e. it came from a legend
                // that showed the extra functions), split the string and
                // highlight TGs with any of the supplied functions
                if ( currentSelectedFunction != null ) {

                    string[] functions = currentSelectedFunction.Split(" | ");
                    foreach (string functionPart in functions) {
                        if (String.Compare( gene.GeneFunction, functionPart, true ) == 0
                            // Also have a special case for if the supplied
                            // query text is "unknown" - check for an empty gene
                            // function as well
                            || (functionPart == Constants.Text_LegendUnknown && String.IsNullOrEmpty(gene.GeneFunction))
                            ) {
                            return HighlightMode.HighLight;
                        }
                    }
                }

                // If highlighting one of the comparison colours, highilght all
                // the TGs that have the corresponding "present in state"
                // property
                if ( currentPresentInStates != PresentInStates.none &&
                     gene is GeneCompareInfo ) {
                    if ((gene as GeneCompareInfo).GenePresentIn == currentPresentInStates
                         || (gene as GeneCompareInfo).RegulationPresentIn == currentPresentInStates)
                    {
                        return HighlightMode.HighLight;
                    }
                }
            }

            // Default case
            return HighlightMode.Normal;
        }

        /// <summary>
        /// Determines what highlighter should be used and what should be highlighted
        /// </summary>
        /// <param name="geneFunction">The gene function to highlight, if any</param>
        /// <param name="goTerm">The GO term to highlight, if any</param>
        /// <param name="searchQuery">The search query to highlight, if any</param>
        /// <param name="presentInStates">The "present in state" to highlight, if any (comparison only)</param>
        protected void UpdateHighlights(string geneFunction, string goTerm, string searchQuery, PresentInStates presentInStates) {

            Script.SetTimeout((Action) delegate {

            // Store the passed values for later reference
            currentSelectedFunction = geneFunction;
            currentSelectedTerm = goTerm;
            currentSearchQuery = searchQuery;
            currentPresentInStates = presentInStates;

            // If any of the above was given a value, use the "new" gene
            // highlighter
            if (currentSelectedFunction != null || currentSelectedTerm != null || currentSearchQuery != null || currentPresentInStates != PresentInStates.none) {
                colourChooser.Highlighter = NewGeneHighlighter;

            // If none of the above has a value, but there are selected genes,
            // use the selected gene highlighter
            } else if (DisplayManager.SelectedGenes.Length > 0) {
				colourChooser.Highlighter = SelectedGeneHighlighter;

            // Otherwise, do no highlighting
            } else {
                colourChooser.Highlighter = null;
            }
            }, 100, null);
        }

#if DEBUG

        /// <summary>
        /// Used for the "colour blind" testing - allows the TGs and legend to
        /// change colours when the "colour blind" button is pressed
        /// <para>"Colour blind" is in quotes because actual colour blind friendly colours are not used - this is to test if changing colours on user command was possible (it was)</para>
        /// </summary>
        protected void RefreshColours () {

            // Don't do anything if not using CSS node attributes and if the
            // "colour blind" toggle is not enabled
            if (Constants.UseCssNodeAttributes && Constants.UseColourBlindToggle) {

                // Update the colours in the legend by changing the class of
                // the "colour" cells of the table - these should always be the
                // first child of each row
                for (int i = 0; i < table_Legend.Rows.Length; i++) {
                    TableCellElement tableCell = (TableCellElement)table_Legend.Rows[i].ChildNodes[0];
                    if (AppMain.ColourBlind && tableCell.ClassName.IndexOf("CB_") == -1) {
                            tableCell.ClassName = tableCell.ClassName.Insert(0, "CB_");
                    }
                    if (!AppMain.ColourBlind && tableCell.ClassName.IndexOf("CB_") == 0) {
                        tableCell.ClassName = tableCell.ClassName.Remove(0,3);
                    }
                }

                // Update the display manager to change the colours
                DisplayManager.RefreshGraphs();
            }
        }

        /// <summary>
        /// Listener for when the "colour blind" toggle button is pressed
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
        protected void touchButton_ColourBlind_Clicked(object sender, ElementEventArgs eventArgs)
        {
            // Change the "colour blind" status
            AppMain.ColourBlind = !AppMain.ColourBlind;

            // Update the colours
            RefreshColours();
        }
#endif
        /// <summary>
        /// Updates the list of selected homologues in the legend panel
        /// <para>If there are no selected homologues, "none" is displayed. Otherwise, the name of each homologue is listed, followed by the number of that homologue that is selected</para>
        /// <para>Note that this is designed under the assumption that a selected target gene will always have its homologues selected as well</para>
        /// </summary>
        private void UpdateSelectedGenesLabel()
        {
            // Show "none" if a data set has not been loaded yet
            if (RequiredDataNotAvailable || DisplayManager.SelectedGenes == null) {
                label_SelectedGenes.TextContent = Constants.Text_LabelSelectedGenes_None;
                return;
            }

            // Get the currently selected genes from the display manager
            List<GeneInfo> tempSelectedGenes = DisplayManager.SelectedGenes;

            // Show "none" if there are no selected target genes
            if (tempSelectedGenes.Length < 1) {
                label_SelectedGenes.TextContent = Constants.Text_LabelSelectedGenes_None;

            // Otherwise, start listing the selected homologues
            } else {

                // Create a dictionary to store all of the selected homologues
                // and how many of each are present
                Dictionary<string, int> numberOfHomologues = new Dictionary<string,int>();

                // String to store the text that will be passed to the label
                // element
                string labelText = string.Empty;

                // For each gene in the list of selected genes...
                foreach (GeneInfo gene in tempSelectedGenes) {

                    // If this gene's name is already in the dictionary, add
                    // one to the number of genes, otherwise initialise the
                    // value for the name as 1
                    if (gene != null) {
                        if (numberOfHomologues.ContainsKey(gene.Name)) {
                            numberOfHomologues[gene.Name] += 1;
                        } else {
                            numberOfHomologues[gene.Name] = 1;
                        }
                    }
                }

                // For each entry in the dictionary...
                foreach (KeyValuePair<string, int> homologue in numberOfHomologues) {

                    // If there is more than one selected gene for this name,
                    // use the label for multiple regulons, otherwise use the
                    // label for a single regulon
                    if (homologue.Value > 1) {
                        labelText += String.Format(Constants.Text_LabelSelectedGenes_EntryMultiple, homologue.Key, homologue.Value);
                    } else {
                        labelText += String.Format(Constants.Text_LabelSelectedGenes_EntrySingle, homologue.Key, homologue.Value);
                    }
                }

                // As the text constant used above includes a trailing comma
                // and space, truncate the last two characters so that these
                // do not appear after the last entry
                labelText = labelText.Remove(labelText.Length - 2, 2);

                // Set the label element's content to the generated text
                label_SelectedGenes.TextContent = labelText;
            }
        }

        /// <summary> Returns if a network is selected
        /// <para> One of the filtering functions </para>
        /// </summary>
        /// <param name="regulon">The regulon to check</param>
        /// <returns>Whether the regulon is selected</returns>
        protected static bool RegulogInfo_FilterBySelection ( RegulonInfo regulon ) {
            return regulon.IsSelected;
        }
	}
}
