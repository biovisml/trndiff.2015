// RegPreciseRegulogPanel.cs
//

using System;
using System.Collections.Generic;
using System.Html;

namespace RegulonExplorer.View.RegPreciseView {

	public class RegPreciseGenomePanel : AppContentHolder {
		private RegPreciseGenomeChooser genomeChooser;
		private RegPreciseRegulonChooser regulonChooser;
        private RegPreciseRegulonGraphViewer2 regulogViewer;
        private RegPreciseRegulonGraphViewer2SideBySide regulogViewerCompare;

		public RegPreciseGenomePanel ( Element domElement )
			: base( domElement ) {

			FindChildControls();
			BindControls();
		}

		private void FindChildControls()
		{
			genomeChooser = (RegPreciseGenomeChooser) FindControlByType( typeof( RegPreciseGenomeChooser ) );
			regulonChooser = (RegPreciseRegulonChooser) FindControlByType( typeof( RegPreciseRegulonChooser ) );
			regulogViewer = (RegPreciseRegulonGraphViewer2) FindControlByType( typeof( RegPreciseRegulonGraphViewer2 ) );
			regulogViewerCompare = (RegPreciseRegulonGraphViewer2SideBySide) FindControlByType( typeof( RegPreciseRegulonGraphViewer2SideBySide ) );
		}

		private void BindControls()
		{
 			regulonChooser.GenomeChooser = genomeChooser;
            regulogViewer.SourceDataGrid = regulonChooser.DataGrid;
            regulogViewerCompare.SourceDataGrid = regulonChooser.DataGrid;
		}
	}
}
