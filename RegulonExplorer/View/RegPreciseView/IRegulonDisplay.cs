﻿using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using SystemQut.Controls;
using System.Html;
using RegulonExplorer.View.TrnDiff;
using SystemQut.ComponentModel;

namespace RegulonExplorer.View.RegPreciseView
{
    /// <summary>
	/// An interface for defining new layouts for regulons
    /// </summary>
    public interface IRegulonDisplay
    {
        event EventHandler SelectedGenesChanged;

		/// <summary> Displays the list of regulons in the specified container
		/// </summary>
		/// <param name="regulons"></param>
		/// <param name="container"></param>
		/// <param name="selectedRegulog"></param>
		/// <param name="nodeFactory"></param>
		/// <param name="colourChooser"></param>
        void DisplayRegulons(
			List<RegulonInfo> regulons,
			Element container,
			RegulogInfo selectedRegulog,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser
		);

		/// <summary> Updates the visible regulons in the associated container,
		///		applying a filter and sorting function to
		/// </summary>
		/// <param name="sort"></param>
		/// <param name="filter"></param>
		void UpdateVisibleRegulons (
			Func<RegulonInfo,RegulonInfo,int> sort,
			Func<RegulonInfo,bool> filter
		);

		/// <summary> Removes all visible graphs from the display and disposes them.
		/// </summary>
		void Clear();

        /// <summary> Goes through all of the selected genes in the display and
        /// deselects them
		/// </summary>
        /// <param name="allowOverlay">Whether to use a time out to allow the loading overlay to appear</param>
        void ClearSelectedGenes(bool allowOverlay);

        /// <summary>
        /// Gets or sets the list of selected genes
        /// <para>Allowing setting is primarily to let selections be transferred after changing the display manager</para>
        /// </summary>
        List<GeneInfo> SelectedGenes {
            get;
// New set behaviour
            set;
        }

        /// <summary> Goes through all of the selected regulons in the display
        /// and deselects them
		/// </summary>
        void ClearSelectedRegulons();

        /// <summary> Returns the list of selected regulons in the order they
        /// were selected
        /// </summary>
        List<RegulonInfo> SelectedRegulons {
            get;
// New set behaviour
            set;
        }

        /// <summary>
        /// Gets or sets a matrix of all the distances between the networks
        /// <para>Allowing setting is primarily to let distances be transferred after changing the display manager</para>
        /// </summary>
        double[][] CurrentDistances {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ratio of Euclidean and Hamming distances
        /// <para>0 = full Euclidean, 1 = full Hamming (default is 0.5)</para>
        /// </summary>
        double Ratio {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the status of the ratio changing
        /// </summary>
        bool RatioChanged {
            get;
            set;
        }

        /// <summary>
        /// Gets the current zoom level of the display
        /// <para>The setter was originaly a seperate method called "ChangeZoomLevel"</para>
        /// </summary>
        ZoomLevels CurrentZoomLevel {
            get;
            set;
        }

        /// <summary>
        /// Used when a property is changed in the display
        /// </summary>
        event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called whenever a property changes in the display manager. Used to inform listeners when that happens
        /// </summary>
        /// <param name="info">The properties that were changed</param>
        void NotifyPropertyChanged(String info);

        /// <summary>
        /// Returns whether the current display manager is processing or not
        /// <para>Used by parent RegulonGraphDisplays to show or hide the loading overlay</para>
        /// </summary>
        bool IsProcessing {
            get;
        }

        /// <summary>
        /// Refreshes every graph in the display
        /// </summary>
        void RefreshGraphs();

        /// <summary>
        /// Returns the number of graphs in the display
        /// <para>How graphs are counted for this is dependant on the invidiual regulon display classes</para>
        /// </summary>
        int NumberOfGraphs {
            get;
        }
    }
}
