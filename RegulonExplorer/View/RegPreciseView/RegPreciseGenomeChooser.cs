// HomeAbout.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.ViewModel;
using SystemQut.Controls;
using RegPrecise;
using SystemQut.ComponentModel;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.RegPreciseView {

	/// <summary> Code behind for the "Browse RegPrecise -> Genomes" content frame.
	/// <para>
	///		This codeBehind is not intended to be used dynamically. It must have a non-null domElement
	///		with the required fields present.
	/// </para>
	/// </summary>

	public class RegPreciseGenomeChooser : AppContent {
		private TextBox searchField;
		private Button searchButton;
		private DataGrid dataGrid;

		private GenomeInfo[] genomeSummaries;
		private bool genomesAlreadyRequested;

		public RegPreciseGenomeChooser ( Element domElement )
			: base( domElement ) {

			if ( domElement == null ) {
				throw new Exception( Constants.DOM_ELEMENT_MAY_NOT_BE_NULL );
			}

			FindChildControls();

			searchButton.Clicked += searchButton_Clicked;
			ActivationChanged += RegPreciseGenomeChooser_ActivationChanged;

			DataLayer.Instance.PropertyChanged += new PropertyChangedEventHandler( DataLayer_PropertyChanged );

			// for development/debug purposes.
			if ( IsActivated ) Refresh();

            searchField.DomElement.AddEventListener("keyup", searchField_KeyUp, false);
		}

		private void FindChildControls () {
			searchField = (TextBox) FindControlByNameAndType( "SearchText", typeof( TextBox ) );
			searchButton = (Button) FindControlByNameAndType( "SearchButton", typeof( Button ) );
			dataGrid = (DataGrid) FindControlByNameAndType( "DataGrid", typeof( DataGrid ) );
		}

		/// <summary> Event handler for this.ActivationChanged.
		/// <para>
		///		When this control becomes active for the first time it will request
		///		the genome summaries from the server.
		/// </para>
		/// <para>
		///		Other than that, no further actions are taken.
		/// </para>
		/// </summary>
		/// <param name="activated"></param>

		private void RegPreciseGenomeChooser_ActivationChanged ( IActivated activated ) {
			if ( activated.IsActivated ) {
				Refresh();
			}
		}

		/// <summary> Listen for the arrival of genomes at the data layer.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>

		void DataLayer_PropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			if ( args.Includes( "Genomes" ) ) {
				this.genomeSummaries = DataLayer.Instance.Genomes.GetGenomes( DataLayer.RegPreciseId );
				dataGrid.ItemsSource = new ObservableList( genomeSummaries );
			}
		}

		/// <summary> Fetch Genomes redisplay genome table.
		/// </summary>

		private void Refresh () {
			if ( genomesAlreadyRequested )
				return;

			genomesAlreadyRequested = true;

			DataLayer.Instance.RequestGenomes();

		}

		/// <summary> When the search button is cliecked we apply the curernt filter
		///		(as a plain regexp) to the datagrid to reduce the number of displayed rows.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="eventArgs"></param>

		void searchButton_Clicked ( object sender, ElementEventArgs eventArgs ) {
			// TODO: replace this with an onchange listener.

			string searchText = searchField.Text;

			if ( string.IsNullOrWhiteSpace( searchText ) ) {
				dataGrid.Filter = null;
			}
			else {
				try {
					RegExp regexp = new RegExp( searchText, "i" );
					dataGrid.Filter = delegate( object item ) {
						GenomeInfo genomeSummary = (GenomeInfo) item;
						return genomeSummary.Name.Match( regexp ) != null;
					};
				}
				catch ( Exception ex ) {
					Window.Alert( string.Format( "searchButton_Clicked: error {0}: {1}", ex.Message, ex.StackTrace ) );
				}
			}
		}

		public DataGrid DataGrid {
			get { return dataGrid; }
		}

        /// <summary>
        /// Allows the filter text box to search on pressing enter
        /// </summary>
        /// <param name="e"></param>
        private void searchField_KeyUp(ElementEvent e)
        {
            if ((bool)Script.Literal("e.key == 'Enter'")) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("User pressed enter in the filter text box");
#endif*/
                searchButton_Clicked(searchField, null);
            }
        }
	}
}
