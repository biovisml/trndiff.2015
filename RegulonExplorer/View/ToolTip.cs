// ToolTip.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut;
using SystemQut.Controls;

namespace RegulonExplorer.View {

    /// <summary>
    /// Tooltips for nodes in graphs. Only one tooltip can be displayed at a time, hence this is a singleton class
    /// </summary>
	public class ToolTip : CodeBehind {

        // The tooltip instance
		private static ToolTip instance;

        // Table that comprises the tooltip
		private TableElement table;

        // The text contents of the tooltip
		private string[] content;

        // X and Y offsets
		private double H_OFFSET = 20;
		private double Y_OFFSET = 0;

        /// <summary>
        /// Creates the tooltip element
        /// </summary>
		private ToolTip ()

            // Create a HTML element for the tooltip
			: base( HtmlUtil.CreateElement( "div", Document.Body, null ) ) {
			Element domElement = DomElement;

            // Apply the correct class
			domElement.ClassName = "tooltip";

            // Create the table
			table = (TableElement) HtmlUtil.CreateElement( "table", domElement, new Dictionary<string, string>(
				"border", "0",
				"cellpadding", "0",
				"cellspacing", "3"
			) );

            // Apply a super large ZIndex so it will always appear above other
            // elements
            // (The high value was probably a leftover from when the free
            // Hammer regulon displays existed)
            domElement.Style.ZIndex = 10000;
		}

        /// <summary>
        /// Gets the tooltip instance
        /// </summary>
		public static ToolTip Instance {
			get {

                // Create a new tooltip if there is no instance
				if ( instance == null ) {
					instance = new ToolTip();
				}

				return instance;
			}
		}

        /// <summary>
        /// Gets or sets the content of the tooltip
        /// </summary>
		public string[] Content {
			get { return content; }
			set {
                // Clear the existing contents of the tooltip
				Clear();

                // Store the specified value as the content
				this.content = value;

                // Go through all of the rows of the new content and create
                // table rows out of them
				int i = 0;
				while ( i < value.Length ) {
					string label = value[i++];
					string text = value[i++];
					Element tr = HtmlUtil.CreateElement( "tr", table, null );

                    // Create the cells containing the label and data for the
                    // current row
					HtmlUtil.CreateElement( "td", tr, null ).TextContent = label;
					HtmlUtil.CreateElement( "td", tr, null ).TextContent = text;
				}
			}
		}

        /// <summary>
        /// Clears the contents of the tooltip
        /// </summary>
		private void Clear () {
			Element row;

            // While there are rows in the table, delete them
			while ( null != ( row = table.FirstChild ) ) {
				table.RemoveChild( row );
			}
		}

        /// <summary>
        /// Sets the tooltip's position (through its top left coordinate)
        /// </summary>
        /// <param name="x">The target X coordinate</param>
        /// <param name="y">The target Y coordinate</param>
		public void GotoXY ( double x, double y ) {
			// TODO: this needs to test to ensure that it will be visible.
            //object temp1 = DomElement.OwnerDocument;
            //object temp2 = DomElement.OwnerDocument.ParentWindow;
            //object temp3 = DomElement.OwnerDocument.ParentWindow.InnerWidth;

            // Get the bounds of the document body
            Element bounds = (Element)Script.Literal("this.get_domElement().ownerDocument.childNodes[1]");

            //Horizontal position
            // If the given X coordinate would put some of the tooltip out of
            // bounds, set the X coordinate to be the maximum it can be without
            // overflowing by setting it to the current width of the window
            // minus the tooltip's width
            if ((x + H_OFFSET + DomElement.ClientWidth) > bounds.ClientWidth)
            {
                DomElement.Style.Left = (bounds.ClientWidth - (DomElement.ClientWidth)) + "px";
            }

            // Else simply apply the X coordinate plus the offset
            else
            {
                DomElement.Style.Left = (x + H_OFFSET) + "px";
            }

            //Vertical position
            // If the given Y coordinate would put some of the tooltip out of
            // bounds, set the Y coordinate to be the maximum it can be without
            // overflowing by setting it to the current height of the window
            // minus the tooltip's height
            if ((y + Y_OFFSET + DomElement.ClientHeight) > bounds.ClientHeight)
            {
                DomElement.Style.Top = (bounds.ClientHeight - (DomElement.ClientHeight)) + "px";
            }

            // Else simply apply the Y coordinate plus the offset
            else {
                DomElement.Style.Top = (y + Y_OFFSET) + "px";
            }

		}

        /// <summary>
        /// Shows the tooltip
        /// </summary>
		public void Show () {
			DomElement.Style.Opacity = "1";
		}

        /// <summary>
        /// Hideds the tooltip
        /// </summary>
		public void Hide () {
			DomElement.Style.Opacity = "0";
		}

        /// <summary>
        /// Checks whether a given X and Y coordinate is within the tooltip
        /// </summary>
        /// <param name="x">The target X coordinate</param>
        /// <param name="y">The target Y coordinate</param>
        /// <returns>True if the coordinates are in the tooltip, false otherwise</returns>
        public bool IsWithinToolTip(int x, int y) {
            if (HtmlUtil.GetElementX(DomElement) <= x && HtmlUtil.GetElementY(DomElement) <= y
                && HtmlUtil.GetElementX(DomElement) + DomElement.ClientWidth >= x
                && HtmlUtil.GetElementY(DomElement) + DomElement.ClientHeight >= y) {
                return true;
            }
            return false;
        }
	}
}
