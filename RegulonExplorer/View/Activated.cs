﻿using System;
using System.Collections.Generic;

using System.Html;
using SystemQut.Controls;
using RegulonExplorer.Common;

namespace RegulonExplorer.View {
	public class Activated : CodeBehind, IActivated {
		private string visibleStyleValue = "inline";
		private const string CSS_CLASS_ACTIVE = "Active";
		private const string CSS_CLASS_SELECTED = "Selected";
		public event Action<IActivated> ActivationChanged;
		private IActivated nominalActivator;

		public Activated ( Element domElement )
			: base( domElement ) {
		}

		/// <summary> Get or set the IsActivated state.
		/// <para>
		///		When setting to true, the DOM peer of this codeBehind is made visible and all other
		///		elements in the same immediate parent as this codeBehind are made invisible.
		/// </para>
		/// </summary>

		virtual public bool IsActivated {
			get {
				return DomElement.ClassList.Contains( CSS_CLASS_ACTIVE );
			}
			set {
				if ( value == IsActivated ) return;

				ToggleMutexClass( value, CSS_CLASS_ACTIVE, delegate( IActivated codeBehind ) {
					codeBehind.IsActivated = false;
				} );

				if ( ActivationChanged != null ) ActivationChanged( this );
			}
		}

		/// <summary> Sets or unsets membership of the specified CSS class for the DOM element.
		/// <para>
		///		If the new state is set "on", it asks all siblings in its parent container to 
		///		set themselves "off".
		/// </para>
		/// <para>
		///		If the new state is "off", it adjusts itself but leaves its siblings alone.
		/// </para>
		/// </summary>
		/// <param name="value"></param>
		/// <param name="className"></param>
		/// <param name="toggleOff"></param>

		private void ToggleMutexClass ( bool value, string className, Action<IActivated> toggleOff ) {
			if ( value ) {
				DomElement.ClassList.Add( className );
			}
			else {
				DomElement.ClassList.Remove( className );
				return;
			}

			// The active control should only be visible when it's nominal activator is selected.
			if ( Script.IsValue( nominalActivator ) ) nominalActivator.IsSelected = true;

			// The active codeBehind deactivates its siblings.
			ElementCollection parentContent = Script.IsValue( DomElement.ParentNode ) ? DomElement.ParentNode.ChildNodes : null;

			if ( parentContent == null ) return;

			for ( int i = 0; i < parentContent.Length; i++ ) {
				//	Stretching the type system here. We don't really know if the codeBehind is IActivated, 
				//	but it _should_ be, due to the containment model. We check later anyway to be safe.
				Control siblingElement = (Control) parentContent[i];
				IActivated sibling = (IActivated) siblingElement.CodeBehind;

				if ( Script.IsValue( sibling ) && sibling != this && sibling is IActivated ) {
					toggleOff( sibling );
				}
			}
		}

		/// <summary> Get or set the CSS value used to make the codeBehind visible.
		/// </summary>

		public string VisibleStyleValue {
			get {
				return visibleStyleValue;
			}
			set {
				visibleStyleValue = value;
			}
		}

		/// <summary> Get or set the IsSelected state.
		/// <para>
		///		When setting to true, the DOM peer of this codeBehind is made visible and all other
		///		elements in the same immediate parent as this codeBehind are made invisible.
		/// </para>
		/// </summary>

		virtual public bool IsSelected {
			get {
				return DomElement.ClassList.Contains( CSS_CLASS_SELECTED );
			}
			set {
				if ( value == IsSelected ) return;

				ToggleMutexClass( value, CSS_CLASS_SELECTED, delegate( IActivated control ) {
					control.IsSelected = false;
				} );

				NotifyPropertyChanged("IsSelected");
			}
		}


		public IActivated NominalActivator {
			get {
				return this.nominalActivator;
			}
			set {
				if ( nominalActivator == value ) return;
				nominalActivator = value;
				NotifyPropertyChanged("NominalActivator");
			}
		}

		/// <summary> Throws an exception indicating that a child control has the wrong Codebehind type. 
		/// </summary>
		/// <param name="type"></param>

		protected static void WrongControlType ( Type type ) {
			throw new Exception( string.Format( "{0}: {1}", Constants.CHILD_CONTROLS_WRONG_TYPE, type.Name ) );
		}

		/// <summary> Enforce the constraint that a control must be an instance of a specified type.
		/// </summary>
		/// <param name="codeBehind"></param>
		/// <param name="type"></param>

		protected static void VerifyControlType ( ICodeBehind codeBehind, Type type ) {
			if ( !Script.IsValue( codeBehind ) || !( type.IsAssignableFrom( codeBehind.GetType() ) ) ) {
				WrongControlType( typeof( DataGrid ) );
			}
		}

		/// <summary> Lcoates a child control having the specified name and type.
		/// </summary>
		/// <param name="controlName"></param>
		/// <param name="type"></param>
		/// <returns></returns>
		/// <exception cref="Exception"> 
		///		If the child control located does not have the specified control type, or none was found.
		/// </exception>

		protected ICodeBehind FindControlByNameAndType ( string controlName, Type type ) {
			ICodeBehind childControl = base.FindControl( controlName );
			if (Constants.ShowDebugMessages) Console.Log( controlName + ( childControl == null ? " not" : "" ) + " found" );

			if ( childControl != null ) {
				VerifyControlType( childControl, type );
			}

			return childControl;
		}
	}
}
