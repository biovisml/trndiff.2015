﻿using System;
using System.Collections.Generic;


namespace RegulonExplorer.View {

	/// <summary> Interface satisfied by members of mutually exclusive 
	///		controls which occupy the same display space.
	///	<para>
	///		Examples include the secondary tool bars and the main content items, which can be made "active" or "inactive",
	///		and the tool buttons, which can be made "selected" or "not selected".
	/// </para>
	/// </summary>

	public interface IActivated {

		/// <summary> Get or set the "activated" state of the associated codeBehind.
		/// <para>
		///		This normally amounts to the simple act of making the codeBehind visible, 
		///		while simultaneously toggling off the other controls that occupy the 
		///		parent container.
		/// </para>
		/// </summary>

		bool IsActivated { get; set; }

		/// <summary> Get or set the "selected" state of the associated codeBehind.
		/// <para>
		///		This normally amounts to the simple act of making the codeBehind visible, 
		///		while simultaneously toggling off the other controls that occupy the 
		///		parent container.
		/// </para>
		/// </summary>

		bool IsSelected { get; set; }

		/// <summary> Get or set the CSS value used to make the codeBehind visible.
		/// </summary>

		string VisibleStyleValue { get; set; }

		/// <summary>
		///  Notify observers that the activation state of this control has changed.
		/// </summary>

		event Action<IActivated> ActivationChanged;

		/// <summary> Although an IActivated may be activated by any object, many have
		///		a distinct or special activator which should always be selected when
		///		the current control is active. ToolButton for example.
		/// </summary>

		IActivated NominalActivator { get; set; }
	}
}
