// Toolbar.cs
//

using System;
using System.Collections.Generic;
using SystemQut.Controls;
using System.Html;
using SystemQut;

namespace RegulonExplorer.View {

	public class ToolBar : Activated {
		public static string PRIMARY_CSS_CLASS { get { return "PrimaryToolbar"; } }
		public static string SECONDARY_CSS_CLASS { get { return "SecondaryToolbar"; } }

		/// <summary> Initialises the toolbar, binding it to the supplied DOM element
		///		to form a Control, if supplied.
		/// </summary>
		/// <param name="domElement"></param>

		public ToolBar ( Element domElement )
			: base( domElement ) {
		}
	}
}
