// GeneNode.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.ViewModel;
using SystemQut;
using SystemQut.ComponentModel;
using SystemQut.Svg;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.TrnDiff {

	public abstract class AbstractNode : SvgGroup, ISelectable {

        // Events on nodes to listen for. Starts empty as the contents depend
        // on whether Hammer is being used or not
		protected static string[] elementEvents /*= {
			"click",
			"mouseover",
			"mouseout",
			"mousemove",
		}*/;

        // Actions to perform on the above events
		protected ElementEventListener[] elementActions;

        // The node's SVG element
        private SvgElement node;

        // The gene information the node uses
		protected IGeneInfo gene;

        // The colour chooser for this node
		protected IAttributeFactory colourChooser;

        /// <summary>
        /// Gets or sets the node's SVG element
        /// </summary>
        public SvgElement Node
        {
            get { return node; }
            set { node = value; }
        }

        /// <summary>
        /// Gets the node's gene information
        /// </summary>
        public IGeneInfo Gene
        {
            get { return gene; }
        }

		/// <summary> Create a new abstract gene node, equipping it with a generic image (a circle, placed initially at the origin).
		/// </summary>
		/// <param name="parent">
		///		An SVG group (or some other SvgContainer) in which this node will be displayed.
		/// </param>
		/// <param name="geneInfo">
		///		The gene or regulator to which this applies.
		/// </param>
		/// <param name="colourChooser">
		///		An Attribute factory that will be used to decorate the node.
		/// </param>
		/// <param name="nodeFactory">
		///		A Node Factory that will be used to construct the glyphs for each node.
		/// </param>

		public AbstractNode (
			SvgContainer parent,
			IGeneInfo geneInfo,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser
		)
            // Call the base SVG element constructor
			: base( parent, null ) {

            // Set the node's gene object and colour chooser
			this.gene = geneInfo;
			this.colourChooser = colourChooser;

			// node = new SvgCircle( this, 0, 0, nodeRadius, null );

            // Create a node using the specified node factory
			node = nodeFactory.CreateNode( this, geneInfo );

            // The list of events and actions depends on whether Hammer.JS is
            // being used. The "click" event and its action is only present if
            // Hammer is not
            if (Constants.UseHammerTouchSupport) {

                // Set the events to look for when Hammer is active
                elementEvents = new string[] {
			        "mouseover",
			        "mouseout",
			        "mousemove",
		        };

                // Set the associated actions
			    elementActions = new ElementEventListener[] {
				    circle_MouseOver,
				    circle_MouseOut,
				    circle_MouseMove,
			    };
            } else {

                // Set the events to look for when Hammer is not active
                elementEvents = new string[] {
			        "click",
			        "mouseover",
			        "mouseout",
			        "mousemove",
		        };

                // Set the associated actions
			    elementActions = new ElementEventListener[] {
				    circle_Clicked,
				    circle_MouseOver,
				    circle_MouseOut,
				    circle_MouseMove,
			    };
            }

            // Add the events to the node
			AddEvents( node.DomElement, elementEvents, elementActions );

            // Add listeners to the gene and colour chooser to check if they
            // change
			( (INotifyPropertyChanged) geneInfo ).PropertyChanged += gene_PropertyChanged;
			( (INotifyPropertyChanged) colourChooser ).PropertyChanged += colourChooser_PropertyChanged;
		}

		/// <summary>
        /// Calls the colour chooser to set the attributes of this node
        /// <para>    This needs to be called immediately after the object is created to
		///		set the attributes.</para>
		/// </summary>

		public void SetAttributes () {
			ApplyStyles( colourChooser.GetAttributes( gene ) );
		}

		/// <summary> Applies the attributes returned by a IAttributeFactory
		///		to the node elements.
		/// </summary>
		/// <param name="attrs">
		///		A JavaScript object containing a set of attribute name-value pairs
		///		in the form of a dictionary.
		/// </param>

		protected virtual void ApplyStyles ( JSObject attrs ) {
			foreach ( KeyValuePair<string, object> kvp in attrs ) {
				string key = kvp.Key;
				object value = kvp.Value;
				string[] parts = key.Split( ':' );

                // For the moment, the odd conditions with stroke width are to
                // avoid the size changing when the display is zoomed and nodes
                // and spokes are clicked on
				if ( parts.Length == 1 && !(parts[0] == "stroke-width" && node.StrokeWidth > 0)) {
					node.SetAttribute( key, value );
				}
				else if ( parts.Length == 2 ) {
					if ( parts[0] == "node" ) node.SetAttribute( parts[1], value );
				}
			}
		}

        /// <summary>
        /// Properly deletes this node from the graph
        /// </summary>
		public override void Dispose () {
            // Remove the listeners on the gene and colour chooser
			( (INotifyPropertyChanged) gene ).PropertyChanged -= gene_PropertyChanged;
			( (INotifyPropertyChanged) colourChooser ).PropertyChanged -= colourChooser_PropertyChanged;

            // Remove all events associated with this node link
			RemoveEvents( node.DomElement, elementEvents, elementActions );

            // Dispose of this node
			node.Dispose();

            // Dispose of the base SVG element
			base.Dispose();
		}

        /// <summary>
        /// Listener for the properties of the gene changing
        /// <para>If the gene's selected status changes, update the attributes of this node</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
		protected virtual void gene_PropertyChanged ( object sender, PropertyChangedEventArgs eventArgs ) {

            // If the changed property is the gene's selected status...
			if ( eventArgs.Includes( "IsSelected" ) ) {
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("gene_PropertyChanged triggered for " + gene.Name);
#endif*/
                // Update the node's attributes
				ApplyStyles( colourChooser.GetAttributes( gene ) );
			}
		}

        /// <summary>
        /// Listener for the properties of the colour chooser changing
        /// <para>If the highlighter in the colour chooser changes, update the attributes of this node</para>
        /// </summary>
        /// <param name="sender">The sender of the event</param>
		/// <param name="eventArgs">The parameters of the event</param>
		void colourChooser_PropertyChanged ( object sender, PropertyChangedEventArgs eventArgs ) {

            // If the changed property is the colour chooser's highlighter...
			if ( eventArgs.Includes( "Highlighter" ) ) {
                // Update the node's attributes
				ApplyStyles( colourChooser.GetAttributes( gene ) );
			}
		}

        /// <summary>
        /// Adds the specified events and actions to the specified element
        /// </summary>
        /// <param name="element">The element to add events to</param>
        /// <param name="events">The events to listen for</param>
        /// <param name="actions">The functions that will act as listeners</param>
		protected void AddEvents ( Element element, string[] events, ElementEventListener[] actions ) {
			for ( int i = 0; i < events.Length; i++ ) {
				element.AddEventListener( events[i], actions[i], false );
			}
		}

        /// <summary>
        /// Removes the specified events and actions from the specified element
        /// </summary>
        /// <param name="element">The element to remove events to</param>
        /// <param name="events">The events to stop listening for</param>
        /// <param name="actions">The functions to remove as listeners</param>
		protected void RemoveEvents ( Element element, string[] events, ElementEventListener[] actions ) {
			for ( int i = 0; i < events.Length; i++ ) {
				element.RemoveEventListener( events[i], actions[i], false );
			}
		}

        /// <summary>
        /// Listens for when the node is clicked on
        /// <para>Shows the tooltip for and sets the selection of this node</para>
        /// </summary>
        /// <param name="eventArgs">The parameters of the event</param>
		protected void circle_Clicked ( ElementEvent eventArgs ) {

            // Show the tool tip for this node
		    ShowToolTip( eventArgs );

            // Toggle the selection status of this node/gene
			IsSelected = ! IsSelected;
		}

        /// <summary>
        /// Listens for when the cursor moves while over the node
        /// <para>Currently does nothing</para>
        /// </summary>
        /// <param name="eventArgs">The parameters of the event</param>
		protected void circle_MouseMove ( ElementEvent eventArgs ) {
		}

        /// <summary>
        /// Listens for when the cursor moves onto the node
        /// <para>Shows the tooltip if Hammer.JS is not being used</para>
        /// </summary>
        /// <param name="eventArgs">The parameters of the event</param>
		protected void circle_MouseOver ( ElementEvent eventArgs ) {

            // If Hammer.JS is not active...
            if (!Constants.UseHammerTouchSupport) {

                // Display the tooltip
			    ShowToolTip( eventArgs );
            }
		}

        /// <summary>
        /// Listens for when the cursor moves out of the node
        /// </summary>
        /// <param name="eventArgs">The parameters of the event</param>
		protected void circle_MouseOut ( ElementEvent eventArgs ) {

            // If Hammer.JS is not active...
            if (!Constants.UseHammerTouchSupport) {

                // Get the tooltip's instance
			    ToolTip tooltip = ToolTip.Instance;

                // Hide the tooltip
			    tooltip.Hide();
            }
		}

        /// <summary>
        /// Displays the tooltip of this node
        /// </summary>
        /// <param name="eventArgs">The parameters of the event that called this function - used to get the X and Y coordinates</param>
		private void ShowToolTip ( ElementEvent eventArgs ) {

            // Get the tooltip's instance
			ToolTip tooltip = ToolTip.Instance;

            // Set the content as the node's tooltip content property
			tooltip.Content = TooltipContent;

            // Set the tooltip's X and Y position
			tooltip.GotoXY( eventArgs.PageX, eventArgs.PageY );

            // Display the tooltip
			tooltip.Show();
		}

        /// <summary>
        /// Gets or sets the selected status of this node
        /// <para>Redirects this to the gene's selected status</para>
        /// </summary>
		public bool IsSelected {
			get {
                // Return the node's gene's selected status
				return ( (ISelectable) gene ).IsSelected;
			}
			set {
                // Don't do anything if the selected status has not changed
				if ( IsSelected == value )
					return;

                // Otherwise set the node's gene's selected status
				( (ISelectable) gene ).IsSelected = value;
			}
		}

        /// <summary>
        /// Gets the content for a tooltip as a list of strings
        /// <para>Defined by other classes that implement this class</para>
        /// </summary>
		abstract public string[] TooltipContent {
			get;
		}

        /*public IGeneInfo Gene
        {
            get { return gene; }
        }*/

        /// <summary>
        /// Displays the tooltip of this node. This allows classes outside of this one to show it
        /// </summary>
        /// <param name="x">The X coordinate where the tooltip should appear</param>
        /// <param name="y">The Y coordinate where the tooltip should appear</param>
        public void ShowToolTipExternal ( int x, int y ) {

            // Get the tooltip's instance
			ToolTip tooltip = ToolTip.Instance;

            // Set the content as the node's tooltip content property
			tooltip.Content = TooltipContent;

            // Set the tooltip's X and Y position
			tooltip.GotoXY( x, y );

            // Display the tooltip
			tooltip.Show();
		}

        /// <summary>
        /// Hides the tooltip of this node. This allows classes outside of this one to show it
        /// </summary>
        public void HideToolTipExternal () {

            // Get the tooltip's instance
			ToolTip tooltip = ToolTip.Instance;

            // Hide the tooltip
			tooltip.Hide();
        }

        /// <summary>
        /// Checks whether the specified coordinates are inside the node's tooltip. This allows classes outside of this one to check for that
        /// </summary>
        /// <param name="x">The X coordinate to check with</param>
        /// <param name="y">The Y coordinate to check with</param>
        /// <returns>True if the coordinates are inside it, or false if they are outside it</returns>
        public bool IsWithinToolTipExternal(int x, int y) {

            // Get the tooltip's instance
			ToolTip tooltip = ToolTip.Instance;

            // Check using the tooltip's function
			return tooltip.IsWithinToolTip(x, y);
        }
	}
}
