﻿using System;
using System.Collections.Generic;
using RegulonExplorer.ViewModel;
using SystemQut;
using SystemQut.ComponentModel;

namespace RegulonExplorer.View.TrnDiff {

	/// <summary> Assigns colours to regulators based on their regulator function.
	/// <para>
	///		Colour assignment is arbitrary, based on the position of regulator function in an alpha-ordered
	///		list of functions encountered in the containing regulog.
	/// </para>
	/// </summary>

    // Originally SamsAttributeFactory
	public class GeneFunctionAttributeFactory : AbstractAttributeFactory {

		private const string UNKNOWN_COLOUR = "#dddddd";
		private const string REGULATOR_COLOUR = "black";
		private const string SELECTED_FILL_COLOUR = "gold";
		private const string SELECTED_STROKE_COLOUR = "#00ff00";
		private object HIGHLIGHT_STROKE_COLOUR = "#302833";
		private string NO_REGULATOR_COLOUR = "gray";

		private readonly RegulogInfo regulog;
	    private readonly Dictionary<string, int> geneIndex = new Dictionary<string, int>();

        // Samuel Smith 19-02-2014
        // Colours from ColorBrewer2
        //private string[] thisPalette = { "#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00", "#ffff33", "#a65628" };
        //private string[] thisPalette = {"#8dd3c7","#ffffb3","#bebada","#fb8072","#80b1d3","#fdb462","#b3de69","#fccde5","#d9d9d9","#bc80bd","#ccebc5","#ffed6f"};
        private string[] thisPalette = {"#8dd3c7","#bebada","#fb8072","#80b1d3","#b3de69","#fccde5","#d9d9d9","#bc80bd","#ccebc5","#ffed6f","#ffffb3","#fdb462"};

        //private string[] biofunctionCategories = { "transport", "receptor", "enzyme" , "membrane", "protein", "regulator", "miscellanious" };

		/// <summary> Initialises the colour chooser, creating a lokkuptable
		///		which maps each distinct lower-case function to a non-negative integer.
		/// </summary>
		/// <param name="regulog">
		///		The regulog which contains the referenced target regulators.
		/// </param>

		public GeneFunctionAttributeFactory( RegulogInfo regulog ) {
			this.regulog = regulog;

			string [] functions = regulog.GeneFunctions;

			for ( int i = 0; i < functions.Length; i++ ) {
				geneIndex[functions[i]] = i;
			}

            colourPalette = thisPalette;
            regulatorColour = REGULATOR_COLOUR;
            noRegulatorColour = NO_REGULATOR_COLOUR;
		}

		/// <summary> Assigns a colour to the regulator by lookup in the alphabetically ordered list of
		///		distinct gene functions.
		/// </summary>
		/// <param name="gene"></param>
		/// <returns></returns>

		private JSObject GetGeneAttributes ( GeneInfo gene ) {
			JSObject result = new JSObject();

			int idx = geneIndex[gene.GeneFunction.ToLowerCase()];

			result["opacity"] = "1";
			result["stroke-width"] = "1.5";

			string colour = Script.IsValue( idx ) ? ColourPalette[idx % ColourPalette.Length] : UNKNOWN_COLOUR;

			result["node:fill"] = colour;
			result["node:stroke"] = gene.IsSelected ? SELECTED_STROKE_COLOUR: colour;

            result["link:fill"] = colour;
            result["link:stroke"] = gene.IsSelected ? SELECTED_STROKE_COLOUR : gene.Site == null ? "#eeeeee" : colour;
            result["link:stroke-dasharray"] = gene.Site == null ? "5,5" : "none";

			if ( highlighter != null ) {
				HighlightMode highlight = highlighter( gene );

				if ( highlight == HighlightMode.Hidden ) {
					result["opacity"] = "0";
				}
				else if ( highlight == HighlightMode.Normal ) {
					result["opacity"] = "0.25";
				}
                else if ( highlight == HighlightMode.Selected ) {
					result["node:stroke"] = "blue";
					result["link:stroke"] = "blue";
                }
				else {
					result["node:stroke"] = HIGHLIGHT_STROKE_COLOUR;
					result["link:stroke"] = HIGHLIGHT_STROKE_COLOUR;
				}
			}

			return result;
		}

		/// <summary> Assigns a colour to the regulator by lookup in the alphabetically ordered list of
		///		distinct regulator functions.
		/// </summary>
		/// <param name="regulator"></param>
		/// <returns></returns>

		private JSObject GetRegulatorAttributes ( RegulatorInfo regulator ) {
			string colour = regulator.VimssId > 0 ? regulatorColour : noRegulatorColour;

			JSObject result = new JSObject();
			result["node:fill"] = colour;
			result["node:stroke"] = colour;
			return result;
		}

        /// <summary> Assigns a colour to a gene in the CompareRegulonDisplay
        /// by whether it is present in one or both/all of the compared networks
		/// </summary>
		/// <param name="gene"></param>
		/// <returns></returns>

		private JSObject GetGeneCompareAttributes ( GeneCompareInfo gene ) {
			JSObject result = new JSObject();

#if DEBUG
            //Console.Log(gene.Name + " is a GeneCompareInfo...");
#endif

			result["opacity"] = "1";
			result["stroke-width"] = "1.5";

			string colour = String.Empty;

            // If this gene is on the left and we're not doing an AND
            if (gene.GenePresentIn == PresentInStates.left && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND)
            {
                colour = "#FF0000";
#if DEBUG
                //Console.Log("GenePresentIn is left and CurrentOperation is not AND, therefore setting colour to red.");
#endif
            }

            // If this gene is on the right and we're not doing an AND
            else if (gene.GenePresentIn == PresentInStates.right && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND)
            {
                colour = "#0000FF";
#if DEBUG
                //Console.Log("GenePresentIn is right and CurrentOperation is not AND, therefore setting colour to blue.");
#endif
            }

            // If this gene is on both sides and we're not doing an XOR
            else if (gene.GenePresentIn == PresentInStates.all && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR)
            {
                colour = /*"#FF00FF"*/ "#007F00";
#if DEBUG
                //Console.Log("GenePresentIn is both and CurrentOperation is not XOR, therefore setting colour to purple.");
#endif
            }

            // If this is a multiple comparison and this gene is in some of
            // them, and we're not doing an XOR
            else if (gene.GenePresentIn == PresentInStates.some && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR)
            {
                colour = /*"#FF00FF"*/ "#7F3300";
#if DEBUG
                //Console.Log("Insert some message here.");
#endif
            }

            // Else don't show this gene
            else
            {
                colour = "#FFFFFF";
                result["node:visibility"] = "hidden";
#if DEBUG
                //Console.Log("This gene will not be shown in this logical comparison.");
#endif
            }

			result["node:fill"] = colour;
			result["node:stroke"] = gene.IsSelected ? SELECTED_STROKE_COLOUR: colour;

            // If we're doing a logical comparison (we don't need to check for null since it'll only have a RegulationPresentIn if it's there

#if DEBUG
            //Console.Log("Checking if " + gene.Name + "'s regulation will be shown...");
#endif

            // If this regulation is on the left and we're not doing an AND
            if ((gene as GeneCompareInfo).RegulationPresentIn == PresentInStates.left && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND)
            {
                colour = "#FF0000";
#if DEBUG
                //Console.Log("RegulationPresentIn is left and CurrentOperation is not AND, therefore setting colour to red.");
#endif
            }

            // If this regulation is on the right and we're not doing an AND
            else if ((gene as GeneCompareInfo).RegulationPresentIn == PresentInStates.right && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND)
            {
                colour = "#0000FF";
#if DEBUG
                //Console.Log("RegulationPresentIn is right and CurrentOperation is not AND, therefore setting colour to blue.");
#endif
            }

            // If this regulation is on both sides and we're not doing an XOR
            else if ((gene as GeneCompareInfo).RegulationPresentIn == PresentInStates.all && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR)
            {
                colour = /*"#FF00FF"*/ "#007F00";
#if DEBUG
                //Console.Log("RegulationPresentIn is both and CurrentOperation is not XOR, therefore setting colour to purple.");
#endif
            }

            // If this is a multiple comparison and this gene is in some of
            // them, and we're not doing an XOR
            else if ((gene as GeneCompareInfo).RegulationPresentIn == PresentInStates.some && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR)
            {
                colour = /*"#FF00FF"*/ "#7F3300";
#if DEBUG
                //Console.Log("Insert some message here.");
#endif
            }

            // Else if it's not regulated
            else
            {
                // If the gene is in this comparison, show a dashed, faded spoke
                if ((((gene as GeneCompareInfo).GenePresentIn == PresentInStates.left || (gene as GeneCompareInfo).GenePresentIn == PresentInStates.right) && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND) ||
                    ((gene as GeneCompareInfo).GenePresentIn == PresentInStates.all && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR))
                {
                    colour = "#eeeeee";
#if DEBUG
                    //Console.Log("We would show the regulation if it was there.");
#endif
                }
                // Else don't show the dashed spoke
                else
                {
                    colour = "#FFFFFF";
                    result["link:visibility"] = "hidden";
#if DEBUG
                    //Console.Log("This regulation would not be shown in this logical comparison if it was there.");
#endif
                }
            }

            result["link:fill"] = colour;
            result["link:stroke"] = gene.IsSelected ? SELECTED_STROKE_COLOUR : colour;
            result["link:stroke-dasharray"] = gene.Site == null ? "5,5" : "none";

			if ( highlighter != null ) {
				HighlightMode highlight = highlighter( gene );

				if ( highlight == HighlightMode.Hidden ) {
					result["opacity"] = "0";
				}
				else if ( highlight == HighlightMode.Normal ) {
					result["opacity"] = "0.25";
				}
                else if ( highlight == HighlightMode.Selected ) {
					result["node:stroke"] = "blue";
					result["link:stroke"] = "blue";
                }
				else {
					result["node:stroke"] = HIGHLIGHT_STROKE_COLOUR;
					result["link:stroke"] = HIGHLIGHT_STROKE_COLOUR;
				}
			}

			return result;
		}

		/// <summary> Assigns a colour to the gene or regulator by lookup in the alphabetically ordered list of
		///		distinct gene functions.
		/// </summary>
		/// <param name="gene">
		///		A gene or regulator info object to be decorated.
		/// </param>
		/// <returns></returns>

		override public JSObject GetAttributes ( IGeneInfo gene ) {
            if (gene is GeneCompareInfo) return GetGeneCompareAttributes((GeneCompareInfo)gene);
			if ( gene is GeneInfo ) return GetGeneAttributes( (GeneInfo) gene );
			if ( gene is RegulatorInfo ) return GetRegulatorAttributes( (RegulatorInfo) gene );
			throw new Exception( "Invalid node type." );
		}

        /*public void GenerateRandomColourPalette (int numColours) {
#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Generating random colour palette of " + numColours + " colours");
#endif
            string[] newPalette = new string[numColours];

            for (int i = 0; i < numColours; i++) {
                string newColour = "#";
                do {
                    // 0 = any, 1 = red, 2 = green, 3 = blue
                    int hue = Math.Floor(Math.Random() * 4);
#if DEBUG
                    if (Constants.ShowDebugMessages) Console.Log("Generating colour type " + hue);
#endif
                    for (int j = 0; j < 6; j++) {
                        if ((j > 1 && hue == 1) || ((j < 2 || j > 3) && hue == 2) || (j < 4 && hue == 3)) {
                            newColour = AddHexColourDigit(newColour, 0, 8);
                        } else {
                            newColour = AddHexColourDigit(newColour, 8, 16);
                        }
                    }
                } while (newColour == "#000000" || newColour == "#FFFFFF" || newPalette.Contains(newColour));
#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("Adding " + newColour + " to colour palette");
#endif
                newPalette[i] = newColour;
            }

            ColourPalette = newPalette;
        }

        private static string AddHexColourDigit(string newColour, int min, int max)
        {
            if (min > max || max < 0) {
                min = 0;
                max = 16;
            }

            int randomNumber;
            do {
                randomNumber = Math.Floor((Math.Random() * max));
            } while (randomNumber < min);
            if (randomNumber == 10)
            {
                newColour += "A";
            }
            else if (randomNumber == 11)
            {
                newColour += "B";
            }
            else if (randomNumber == 12)
            {
                newColour += "C";
            }
            else if (randomNumber == 13)
            {
                newColour += "D";
            }
            else if (randomNumber == 14)
            {
                newColour += "E";
            }
            else if (randomNumber == 15)
            {
                newColour += "F";
            }
            else
            {
                newColour += randomNumber;
            }
            return newColour;
        }*/
	}
}
