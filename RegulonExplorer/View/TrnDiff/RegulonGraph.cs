// RegulonGraph.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using RegPrecise;
using RegulonExplorer.ViewModel;
using SystemQut.Controls;
using SystemQut.Svg;
using SystemQut.ComponentModel;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.TrnDiff {

	public class RegulonGraph : CodeBehind {

		// TODO: add "nominal size" for use when determining aspect ratio in the event of document or element resizesizing events.
		// TODO: convert these literal values to parameters/properties.

        // The radius of the whole graph
		private double radius = 85;

        // The distance from the centre to the start of a TG's link
		private double epsilon = 10;

        // The graph's SVG container
        private SvgSvg svg;

        // The SVG group that contains the other SVG elements
		private SvgGroup group;

        // The regulon supplying the data for this graph
        private RegulonInfo regulon = null;

        // The list of TG nodes in the graph
        private readonly List<AbstractNode> nodes = new List<AbstractNode>();

        // The label of the graph (usually will be the organism's name)
		private SvgText label;

        // The regulog that the regulon belongs to
		private RegulogInfo regulog;

        // The node factory used to generate glyphs for nodes
		private INodeFactory nodeFactory;

        // The colour chooser used to define attributes for the nodes
		private IAttributeFactory colourChooser;

        // The list of regulator nodes in the graph
        private readonly List<RegulatorNode> regulatorNodes = new List<RegulatorNode>();

        // Used to give the label a white background
        //private SvgRectangle labelBackground;

        /// <summary>
        /// Gets or sets the graph's SVG element
        /// </summary>
        public SvgSvg GraphSvg
        {
            get { return svg; }
            set { svg = value; }
        }

        /// <summary>
        /// Gets or sets the graph's (first) label
        /// </summary>
        public SvgText GraphLabel
        {
            get { return label; }
            set { label = value; }
        }

        // The second line of the label of the graph. This is used because SVG
        // does not allow text wrapping, so text has to be divided up into
        // multiple labels to allow it to wrap
		private SvgText labelLineTwo;

        /// <summary>
        /// Gets or sets the label element acting as the second line of the label
        /// <para>This is used because SVG does not allow text wrapping, so text has to be divided up into multiple labels to allow it to wrap</para>
        /// </summary>
        public SvgText GraphLabelLineTwo
        {
            get { return labelLineTwo; }
            set { labelLineTwo = value; }
        }

        /// <summary>
        /// Gets or sets the contents of the graph's label
        /// <para>If only using a single line (i.e. GraphLabelWrapping is false), this simply gets or sets the content of the first line</para>
        /// <para>If using two lines (i.e. GraphLabelWrapping is true) then to get the correct string, the contents of both label elements are concatenated with a space character between them.</para>
        /// <para>To set the string in this situation, the text is first divided up into parts using space characters. The first part is added to the first line, and subsequent parts are added (with leading spaces) to the first line until they would make its length longer than 32. Any subsequent parts are added to the second line</para>
        /// </summary>
        public string GraphLabelText {

            // Return the label text, concatinating if necessary
            get { return (Constants.UseGraphLabelWrapping && labelLineTwo.Text.Length > 1) ? label.Text + " " + labelLineTwo.Text : label.Text; }
            set {

                // If GraphLabelWrapping is enabled...
                if (Constants.UseGraphLabelWrapping) {

                    // Split the give text over space characters
                    string[] labelTextSplit = value.Split(' ');

                    // If there is only one part, set the first label element's
                    // text to this part and make the second line empty
                    if (labelTextSplit.Length < 2) {
                        label.Text = labelTextSplit[0];
                        labelLineTwo.Text = string.Empty;

                    // Otherwise, place the parts over the two lines
                    } else {
                        int i = 1;

                        // Set the first label element's text to the first part
                        label.Text = labelTextSplit[0];

                        // For every other part...
                        for (; i < labelTextSplit.Length; i++) {

                            // If adding this part to the first label element
                            // (with a leading space) would make the length of
                            // the text greater than 32, stop adding parts
                            if ((label.Text + " " + labelTextSplit[i]).Length > 32) {
                                break;
                            }

                            // Otherwise, add the current part to the first
                            // label element
                            label.Text = label.Text + " " + labelTextSplit[i];
                        }

                        // If there are no more parts to add, set the second
                        // label element to empty
                        if (i == labelTextSplit.Length) {
                            labelLineTwo.Text = string.Empty;

                        // Otherwise...
                        } else {
                            // Set the second label element's text to the next
                            // part
                            labelLineTwo.Text = labelTextSplit[i];

                            // Increment the conter by one
                            i++;

                            // Add the rest of the parts (with leading spaces)
                            // to the second label element
                            for (; i < labelTextSplit.Length; i++) {
                                labelLineTwo.Text = labelLineTwo.Text + " " + labelTextSplit[i];
                            }
                        }
                    }

                // Otherwise simply set the value of the first label element
                } else {
                    label.Text = value;
                }
            }
        }

        /// <summary>
        /// Gets the list of TG nodes in the graph
        /// </summary>
        public List<AbstractNode> Nodes
        {
            get { return nodes; }
        }

        /// <summary>
        /// Gets the list of regulator nodes in the graph
        /// </summary>
        public List<RegulatorNode> RegulatorNodes
        {
            get { return regulatorNodes; }
        }

        // The size used for the graph's view box. The View box is used as the
        // base of the graph's coordinate system (apparently)
		const double ViewBoxRelativeSize = 200;

        /// <summary>
        /// Creates a new regulon graph
        /// </summary>
        /// <param name="regulog">The data set that the data comes from</param>
        /// <param name="regulon">The regulon this graph is representing</param>
        /// <param name="nodeFactory">A Node Factory that will be used to construct the glyphs for each node.</param>
        /// <param name="colourChooser">An Attribute factory that will be used to decorate the nodes</param>
        /// <param name="width">The width of the graph</param>
        /// <param name="height">The height of the graph</param>
        /// <param name="units">The units the previous two values are in</param>
		public RegulonGraph (
			RegulogInfo regulog,
			RegulonInfo regulon,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser,
			double width,
			double height,
			string units
		)
            // Call the base constructor for a span element
			: base( Document.CreateElement("span") ) {
			// : base( null, null ) {

            // Create a new base SVG element
			this.svg = new SvgSvg( null, null );

            // Add the SVG element to this graph's DOM element
			DomElement.AppendChild( svg.DomElement );

            // Set the regulon, data set, node factory and colour chooser for
            // this graph
			this.regulon = regulon;
			this.regulog = regulog;
			this.nodeFactory = nodeFactory;
			this.colourChooser = colourChooser;

            // Create a view box for the graph
			svg.ViewBox = new SvgRect( -ViewBoxRelativeSize / 2, -ViewBoxRelativeSize / 2, ViewBoxRelativeSize, ViewBoxRelativeSize );

            // Set the graph's SVG element's class, height and width
			svg.SetAttribute( "class", "TrnDiff_RegulonGraph" );
			svg.SetAttribute( "width", width + units );
			svg.SetAttribute( "height", height + units );

            // Set the graph's DOM element's margin
			svg.DomElement.Style.Margin = "3px";

            // Set the graph's DOM element to have no action when touched
            svg.DomElement.SetAttribute( "touch-action", "none");

            // Create the graph's SVG group
			group = new SvgGroup( svg, null );

#if false
			new SvgCircle( group, 0, 0, radius, new Dictionary<string, string>(
				"fill", "#eeeeee",
				"stroke-width", "0",
				"stroke", "transparent"
			) );
#endif
            //regulon.PropertyChanged += regulon_PropertyChanged;

            // Refreshes the graph's contents
			Refresh();

            // Dictionary for the different sizes of fonts depending on the
            // requested unit
			Dictionary<string,double> desiredSizes = new Dictionary<string,double>(
				"px", 92 * 0.3 / 2.54, // assuming 92 dpi
				"cm", 0.3,
				"pt", 72 * 0.3 / 2.54,
				"in", 0.3 / 2.54
			);

            // Sets the correct font size
			double desiredFontSize = desiredSizes[units];

            // Create the label's background rectangle
            /*labelBackground = new SvgRectangle(group, -100, radius + (Svg.ConvertLength( desiredFontSize, height, ViewBoxRelativeSize ) * 0.75), 200, Svg.ConvertLength( desiredFontSize, height, ViewBoxRelativeSize ) * 1.25, new Dictionary<string,string>(
                "fill", "white"
                ));*/

            // Determine the label element's Y position
            double labelY = radius;

            // Increase it a bit if both label elements are being used
            if (Constants.UseGraphLabelWrapping) {
                labelY = radius * 1.1;
            }

            // Create the first label element
            label = new SvgText( group, new Dictionary<string, string>(
				"x", "0",
				"y", labelY.ToString(),
				"font-size", Svg.ConvertLength( desiredFontSize, height, ViewBoxRelativeSize ).ToString(),
                "text-anchor", "middle"
			) );

            // Set the attributes of the first label element depending on
            // whether both are being used
            if (!Constants.UseGraphLabelWrapping) {

                // Add some more vertical distance to the first label element
			    double desiredDescent = desiredFontSize * 12.0 / 7.5;
                label.SetAttribute("dy", Svg.ConvertLength( desiredDescent, height, ViewBoxRelativeSize ).ToString());

                // Set the first label element's class
                label.SetAttribute("class", "RegulonGraphLabel");
            } else {
                // Set the first label element's class
                label.SetAttribute("class", "RegulonGraphLabelTwoLine");
            }

            // Create the second label element if it is being used
            if (Constants.UseGraphLabelWrapping) {
                labelLineTwo = new SvgText( group, new Dictionary<string, string>(
				    "x", "0",
				    "y", labelY.ToString(),
				    "font-size", Svg.ConvertLength( desiredFontSize, height, ViewBoxRelativeSize ).ToString(),
                    "dy", "1em",
                    "class", "RegulonGraphLabelTwoLine",
                    "text-anchor", "middle"
			    ) );
            }

            // Set the graph's text and attributes - the way in which this
            // occurs depends on the GraphLabelWrapping setting
            // If it is on
            if (Constants.UseGraphLabelWrapping) {
                /*if (regulon.Genome.GenomeStats["isPathogenic"] != null && (string) regulon.Genome.GenomeStats["isPathogenic"] == "true")
                {
                    GraphLabelText = regulon.Genome.Name + " - pathogenic";
                }
                else
                {*/
                    // Set the text to the regulon's organism's name
                    GraphLabelText = regulon.Genome.Name;
                /*}*/

                // If the first line is longer than 32 characters, shrink the
                // font size so that it is wholly visible
                if (label.Text.Length > 32) {
                    label.SetAttribute("font-size", Svg.ConvertLength( desiredFontSize / (label.Text.Length / 32), height, ViewBoxRelativeSize ).ToString());
                }

                // If the second line is longer than 32 characters, shrink the
                // font size so that it is wholly visible
                if (labelLineTwo.Text.Length > 32) {
                    labelLineTwo.SetAttribute("font-size", Svg.ConvertLength( desiredFontSize / (labelLineTwo.Text.Length / 32), height, ViewBoxRelativeSize ).ToString());
                }

            // If it is off
            } else {
                /*if (regulon.Genome.GenomeStats["isPathogenic"] != null && (string) regulon.Genome.GenomeStats["isPathogenic"] == "true")
                {
                    label.Text = regulon.Genome.Name + " - pathogenic";
                }
                else
                {*/
                    // Set the text to the regulon's organism's name
                    label.Text = regulon.Genome.Name;
                /*}*/
            }

		}

        /// <summary>
        /// Clears the old graph's nodes and redisplays it
        /// </summary>
        /*private*/ public void Refresh () {

            // Clear the existing graph
			Clear();

            // Create a list for all the TG names
			List<string> names = new List<string>();

            // Look through all the genes in the *regulog*. This is so the TGs
            // can be positioned correctly as homologues
			foreach ( GeneInfo gene in regulog.TargetGenes ) {
				// TODO: Allow alternate ortholog identification mechanism. One that actually works would be good.

				// We have to assume that orthologs have the same name. Otherwise, there
				// appears to be no way to connect them in RegPrecise. However, this is
				// probably an invalid assumption. nonetheless...

                // Add the TG's name to the list of names, if it is not already
                // in there
				string name = gene.Name;

				if ( !names.Contains( name ) ) {
					names.Add( name );
				}
			}

            // Sort the list of names in alphabetical order
			names.Sort();

            // Create a dictionary of TG names and indexes
			Dictionary<string, int> index = new Dictionary<string, int>();

            // For each name, add it in the dictionary, using the length of the
            // dictionary as its index
			foreach ( string name in names ) index[name] = index.Count;

            // Create the nodes for the graph
			ProcessRegulon( regulon, index );
		}

        /// <summary>
        /// Creates nodes in the graph for a given regulon
        /// </summary>
        /// <param name="regulon">The regulon used for this graph</param>
        /// <param name="index">A list of gene names and their index</param>
		private void ProcessRegulon ( RegulonInfo regulon, Dictionary<string, int> index ) {

            // Get the total number of possible TGs
			int n = index.Keys.Length;

            //Calculate the largest distance
/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log("Current regulon is " + regulon.GenomeName);
#endif*/
            /*double max = 0;
            if (regulon.DoScale)
            {
                foreach (GeneInfo gene in regulon.TargetGenes)
                {
                    if (gene.Site != null)
                    {
#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Current max is " + max + ", position for " + gene.Name + " is " + Math.Abs(gene.Site.Position));
#endif
                        max = Math.Max(max, Math.Abs(gene.Site.Position));
#if DEBUG
                        if (Constants.ShowDebugMessages) Console.Log("Max is now " + max);
#endif
                    }
                }
            }*/

            // If the node factory is creating wedges, give it the required
            // angle and graph radius
            if (nodeFactory is WedgeNodeFactory) {
                (nodeFactory as WedgeNodeFactory).Angle = Math.PI/n;
                (nodeFactory as WedgeNodeFactory).GraphRadius = radius;
            }

            // For each TG in the regulon
            foreach (GeneInfo gene in regulon.TargetGenes)
            {
                // Get the index of the current TG name from the list of
                // homologues
                int idx = index[gene.Name];

                // Create a new node for this TG
                GeneNode node = new GeneNode(group, gene, idx, n, radius, epsilon, nodeFactory, colourChooser/*, max*/);

                // Set the attributes of this node
                node.SetAttributes();

//#if DEBUG
                //gene.Gene["annotation"] = "belongs to RegulonGraph with label " + regulon.Genome.Name;
//#endif

                // Add the node to the graph's list of nodes
                nodes.Add(node);
            }

#if RegulatorCallsWorkQuicklyEnough
			RegPreciseModel.RequestRegulatorsInRegulon ( regulon, delegate( RegulonInfo returned ) {
				if ( regulon != returned ) return;
				ProcessRegulators( regulon );
			} );
#endif

            // Get the list of regulators for this regulon
			RegulatorInfo[] regulators = regulon.Regulators;

            // If there are no regulators, create a special regulator to use
            // in the centre of the graph
			if ( regulators == null || regulators.Length == 0 ) {
				// For some reason no regulator exists in many regulons.
				if ( regulog.Regulog.RegulationType == RegulationTypes.RNA ) {
					GenerateRnaRegulator();
				}
				else {
					GenerateUnknownTfRegulator();
				}
			}

            // Otherwise, add the regulon's regulators to the graph
			else {
				RenderRegulators( regulators );
			}
		}

        /// <summary>
        /// Creates nodes for the given list of regulators and places them in the graph
        /// <para>Currently only adds the *first* regulator in the list</para>
        /// </summary>
        /// <param name="regulators">The list of regulators to add</param>
		private void RenderRegulators ( RegulatorInfo[] regulators ) {
			// TODO: see if any regulons actually have more than 1 regulator.

            // Don't do anything if an empty list of regulators was passed
			if ( regulators.Length == 0 ) return;

            // Store the number of target genes for this regulator
            // Uses the length of the list of nodes, in case the number of
            // genes in the RegulonInfo is different to that of all the nodes
            // that were generated for this graph
            regulators[0].NumberOfTargetGenes = nodes.Length;

            // Create a node for the first regulator in the list
			RegulatorNode node = new RegulatorNode( group, regulators[0], nodeFactory, colourChooser );

            // Set the attributes of this node
			node.SetAttributes();

            // Add the node to the graph's list of nodes
			regulatorNodes.Add( node );
		}

		/// <summary> Creates a regulator to represent an RNA regulator.
		/// </summary>
		private void GenerateRnaRegulator () {

            // Create the regulator data that will be used for the node
			Regulator rnaRegulator = new Regulator( regulon.RegulonId, regulog.RegulatorName + "-tRNA", "", -1, regulog.RegulatorFamily );
			RegulatorInfo rnaInfo = new RegulatorInfo( rnaRegulator );

            // Store the number of target genes for this regulator
            // Uses the length of the list of nodes, in case the number of
            // genes in the RegulonInfo is different to that of all the nodes
            // that were generated for this graph
            rnaInfo.NumberOfTargetGenes = nodes.Length;

            // Create a node for the regulator
			RegulatorNode node = new RegulatorNode( group, rnaInfo, nodeFactory, colourChooser );

            // Set the attributes of this node
			node.SetAttributes();

            // Add the node to the graph's list of nodes
			nodes.Add( node );
		}

		/// <summary> Creates a regulator to represent an unknown regulator.
		/// </summary>
		private void GenerateUnknownTfRegulator () {

            // Create the regulator data that will be used for the node
			Regulator unknownRegulator = new Regulator( regulon.RegulonId, regulog.RegulatorName, "", -1, regulog.RegulatorFamily );
			RegulatorInfo unknownInfo = new RegulatorInfo( unknownRegulator );

            // Store the number of target genes for this regulator
            // Uses the length of the list of nodes, in case the number of
            // genes in the RegulonInfo is different to that of all the nodes
            // that were generated for this graph
            unknownInfo.NumberOfTargetGenes = nodes.Length;

            // Create a node for the regulator
			RegulatorNode node = new RegulatorNode( group, unknownInfo, nodeFactory, colourChooser );

            // Set the attributes of this node
			node.SetAttributes();

            // Add the node to the graph's list of nodes
			nodes.Add( node );
		}

        /// <summary>
        /// Clears all of the nodes in the graph
        /// </summary>
		private void Clear () {

            // For each of the gene nodes...
			foreach ( GeneNode node in nodes ) {

                // Dispose of it using the proper method
				node.Dispose();
			}

            // Clear the graph's list of nodes
			nodes.Clear();
		}

        /// <summary>
        /// Properly disposes of the graph
        /// </summary>
		public override void Dispose () {

            // Clear (and dispose) all of the nodes in the graph first
			Clear();

            // Call the base object's disposal method
			base.Dispose();
		}

		/// <summary> Gets a reference to the underlying RegulonInfo object.
		/// </summary>

		public RegulonInfo Regulon {
			get {
				return regulon;
			}
		}

        /*private void regulon_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyNames.Contains("DoScale"))
            {
                Refresh();
            }
        }*/

        /// <summary>
        /// Gets or sets the selected status of this node
        /// <para>Redirects this to the regulon's selected status</para>
        /// </summary>
        public bool IsSelected {
			get {
                // Return the graph's regulon's selected status
				return ( (ISelectable) regulon ).IsSelected;
			}
			set {
                // Don't do anything if the selected status has not changed
				if ( IsSelected == value )
					return;

                // Otherwise set the graph's regulon's selected status
				( (ISelectable) regulon ).IsSelected = value;
			}
		}

        /// <summary>
        /// Toggles the graph's selection when it is clicked
        /// <para>Currently unused</para>
        /// </summary>
        /// <param name="e">The element event that was sent by the element</param>
        private void graph_Clicked(ElementEvent e)
        {
            IsSelected = ! IsSelected;
        }

        /// <summary>
        /// Changes the visibility of all the node labels in the graph
        /// </summary>
        /// <param name="visibility">Whether the nodes should be visible or not (true or false, respectively)</param>
        public void SetNodeLabelVisibility(bool visibility) {

            // For every node in the graph's list of nodes...
            foreach (AbstractNode node in Nodes) {

                // If it is a gene node, set its visibility to the specified
                // value
                if (node is GeneNode) {
                    (node as GeneNode).LabelVisibility = visibility;
                }
            }
        }

        //// <summary>
        //// Gets or sets the rectangle acting as the graph's label's background
        //// </summary>
        /*public SvgRectangle GraphLabelBackground
        {
            get { return labelBackground; }
            set { labelBackground = value; }
        }*/

        /// <summary>
        /// Gets the radius of the graph
        /// </summary>
        public double GraphRadius {
            get { return radius; }
        }
	}
}
