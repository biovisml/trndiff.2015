﻿using System;
using System.Collections.Generic;
using RegulonExplorer.ViewModel;
using SystemQut.Svg;
using SystemQut;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.TrnDiff {

	public class RegulatorNode : AbstractNode {

        // The regulator information this node uses
		private RegulatorInfo regulator;

        /// <summary>
        /// Creates a new regulator node, using a circle glyph to represent it
        /// </summary>
        /// <param name="group">An SVG group (or some other SvgContainer) in which this node will be displayed.</param>
        /// <param name="regulator">The regulator to which this applies</param>
        /// <param name="nodeFactory">A Node Factory that will be used to construct the glyphs for each node.</param>
        /// <param name="colourChooser">An Attribute factory that will be used to decorate the node.</param>
		public RegulatorNode ( SvgGroup group, RegulatorInfo regulator, INodeFactory nodeFactory, IAttributeFactory colourChooser )

            // Call the base AbstractNode constructor
			: base( group, regulator, nodeFactory, colourChooser ) {

            // Set the node's regulator object
			this.regulator = regulator;

            // Place the node at the centre of the graph
			nodeFactory.PlaceNode( Node, 0, 0 );
		}

        /// <summary>
        /// Properly deletes this node from the graph
        /// </summary>
		public override void Dispose () {

            // Dispose of the base node
			base.Dispose();
		}

        /// <summary>
        /// Gets the content for a tooltip as a list of strings
        /// <para>Provides for all types of node:
        /// <list type="bullet">
        /// <item>Locus tag</item>
        /// <item>Name</item>
        /// <item>Gene function</item>
        /// <item>Regulator family</item>
        /// <item>Gene ID</item>
        /// <item>Number of target genes (new)</item>
        /// </list></para>
        /// <para>Also provides an annotation and a GO term if that data is available</para>
        /// </summary>
		public override string[] TooltipContent {
			get {
				List<string> result = new List<string>(
					Constants.Text_TooltipLocusTag,             Objects.ToString( regulator.LocusTag, Constants.Text_TooltipNA ),
					Constants.Text_TooltipName,                 regulator.Name,
					Constants.Text_TooltipGeneFunction,         regulator.GeneFunction,
					Constants.Text_TooltipRegulatorFamily,      regulator.RegulatorFamily,
					Constants.Text_TooltipGeneId,               Objects.ToString( regulator.VimssId, Constants.Text_TooltipNA ),
                    Constants.Text_TooltipNumberOfTGs,          Objects.ToString( regulator.NumberOfTargetGenes, Constants.Text_TooltipNA )
				);

                // Add annotation if one is present
                if (!string.IsNullOrEmpty(regulator.Annotation)) {
					result.AddRange( Constants.Text_TooltipAnnotation, regulator.Annotation );
                }

                // Add GO term if one is present
                if (!string.IsNullOrEmpty(regulator.GoTerm)) {
                    result.AddRange(Constants.Text_TooltipGoTerm, regulator.GoTerm);
                }

                // Add parent GO term if one is present
                if (!string.IsNullOrEmpty(regulator.GoParentTerm)) {
                    result.AddRange(Constants.Text_TooltipGoParentTerm, regulator.GoParentTerm);
                }

				return (string[]) result;
			}
		}

        /// <summary>
        /// Gets or sets the node's regulator information
        /// </summary>
		public RegulatorInfo Regulator {

            // Return the regulator
			get { return regulator; }
			set {
#if DEBUG
				if (Constants.ShowDebugMessages) Console.Log( "Regulator changing." );
#endif
                // Set the regulator to the new one
				regulator = value;

                // Update the styles of the node
				ApplyStyles( colourChooser.GetAttributes( regulator ) );
			}
		}
	}
}
