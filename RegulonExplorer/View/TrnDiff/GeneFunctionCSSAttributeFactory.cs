﻿using System;
using System.Collections.Generic;
using RegulonExplorer.ViewModel;
using SystemQut;
using SystemQut.ComponentModel;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.TrnDiff {

	/// <summary> Assigns colours to regulators based on their regulator function.
	/// <para>
	///		Colour assignment is arbitrary, based on the position of regulator function in an alpha-ordered
	///		list of functions encountered in the containing regulog.
	/// </para>
	/// </summary>

    public class GeneFunctionCSSAttributeFactory : AbstractAttributeFactory {

		private const string REGULATOR_COLOUR = "GeneRegulator";
		private const string NO_REGULATOR_COLOUR = "GeneNoRegulator";

		private readonly RegulogInfo regulog;
	    private readonly Dictionary<string, int> geneIndex = new Dictionary<string, int>();

        // Colours from ColorBrewer2
        //private string[] thisPalette = {"TargetGeneColour1",/*"TargetGeneColour2"*/,"TargetGeneColour3","TargetGeneColour4","TargetGeneColour5","TargetGeneColour6","TargetGeneColour7","TargetGeneColour8","TargetGeneColour9","TargetGeneColour10","TargetGeneColour11",/*"TargetGeneColour12"*/,"TargetGeneColour13","TargetGeneColour14","TargetGeneColour15","TargetGeneColour16","TargetGeneColour17","TargetGeneColour18","TargetGeneColour19","TargetGeneColour20","TargetGeneColour21","TargetGeneColour22","TargetGeneColour23","TargetGeneColour24"};
        //private string[] thisPalette = {"TargetGeneColour1","TargetGeneColour2","TargetGeneColour3","TargetGeneColour4","TargetGeneColour5","TargetGeneColour6","TargetGeneColour7","TargetGeneColour8","TargetGeneColour9","TargetGeneColour10","TargetGeneColour11","TargetGeneColour12"};
        private string[] thisPalette = {"TargetGeneColour1","TargetGeneColour3","TargetGeneColour4","TargetGeneColour6","TargetGeneColour7","TargetGeneColour8","TargetGeneColour9","TargetGeneColour10","TargetGeneColour11","TargetGeneColour12","TargetGeneColour2","TargetGeneColour5"};

        /// <summary> Initialises the colour chooser, creating a lokkuptable
		///		which maps each distinct lower-case function to a non-negative integer.
		/// </summary>
		/// <param name="regulog">
		///		The regulog which contains the referenced target regulators.
		/// </param>

		public GeneFunctionCSSAttributeFactory( RegulogInfo regulog ) {
			this.regulog = regulog;

			string [] functions = regulog.GeneFunctions;

			for ( int i = 0; i < functions.Length; i++ ) {
				geneIndex[functions[i]] = i;
			}

            colourPalette = thisPalette;
            regulatorColour = REGULATOR_COLOUR;
            noRegulatorColour = NO_REGULATOR_COLOUR;
		}

		/// <summary> Assigns a colour to the regulator by lookup in the alphabetically ordered list of
		///		distinct gene functions.
		/// </summary>
		/// <param name="gene"></param>
		/// <returns></returns>

		private JSObject GetGeneAttributes ( GeneInfo gene ) {
			JSObject result = new JSObject();

			int idx = geneIndex[gene.GeneFunction.ToLowerCase()];

			string nodeClass = "GeneDefault";
            string linkClass = nodeClass;

			string colour = Script.IsValue( idx ) ? " " + ColourPalette[idx % ColourPalette.Length] : String.Empty;

			nodeClass += colour;
            linkClass += colour;

            if (gene.Site == null) {
                linkClass += " GeneNoSite";
            }

			if ( highlighter != null ) {
				HighlightMode highlight = highlighter( gene );

				if ( highlight == HighlightMode.Hidden ) {
					nodeClass += " GeneHidden";
                    linkClass += " GeneHidden";
				}
				else if ( highlight == HighlightMode.Normal ) {
					nodeClass += " GeneNotSelected";
                    linkClass += " GeneNotSelected";
				}
                else if ( highlight == HighlightMode.Selected ) {
					nodeClass += " GeneSelected";
                    linkClass += " GeneSelected";
                }
				else {
					nodeClass += " GeneHighlighted";
                    linkClass += " GeneHighlighted";
				}
			}

            result["node:class"] = nodeClass;
            result["link:class"] = linkClass;

			return result;
		}

		/// <summary> Assigns a colour to the regulator by lookup in the alphabetically ordered list of
		///		distinct regulator functions.
		/// </summary>
		/// <param name="regulator"></param>
		/// <returns></returns>

		private JSObject GetRegulatorAttributes ( RegulatorInfo regulator ) {

			JSObject result = new JSObject();
			if (regulator.VimssId > 0) {
		        result["node:class"] = Constants.UseWedgeWheels ? "GraphCentreNumber GeneDefault " + regulatorColour : "GeneDefault " + regulatorColour;
                result["link:class"] = "GeneDefault " + regulatorColour;
            } else {
		        result["node:class"] = Constants.UseWedgeWheels ? "GraphCentreNumber GeneDefault " + noRegulatorColour : "GeneDefault " + noRegulatorColour;
                result["link:class"] = "GeneDefault " + noRegulatorColour;
            }
			return result;
		}

        /// <summary> Assigns a colour to a gene in the CompareRegulonDisplay
        /// by whether it is present in one or both/all of the compared networks
		/// </summary>
		/// <param name="gene"></param>
		/// <returns></returns>

		private JSObject GetGeneCompareAttributes ( GeneCompareInfo gene ) {
			JSObject result = new JSObject();

/*#if DEBUG
            if (Constants.ShowDebugMessages) Console.Log(gene.Name + " is a GeneCompareInfo...");
#endif*/

			string nodeClass = "GeneDefault";
            string linkClass = nodeClass;

			string colour = String.Empty;

            // If this gene is on the left and we're not doing an AND
            if (gene.GenePresentIn == PresentInStates.left && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND)
            {
                colour = " CompareColourLeft";
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("GenePresentIn is left and CurrentOperation is not AND, therefore setting colour to red.");
#endif*/
            }

            // If this gene is on the right and we're not doing an AND
            else if (gene.GenePresentIn == PresentInStates.right && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND)
            {
                colour = " CompareColourRight";
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("GenePresentIn is right and CurrentOperation is not AND, therefore setting colour to blue.");
#endif*/
            }

            // If this gene is on both sides and we're not doing an XOR
            else if (gene.GenePresentIn == PresentInStates.all && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR)
            {
                colour = " CompareColourAll";
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("GenePresentIn is both and CurrentOperation is not XOR, therefore setting colour to green.");
#endif*/
            }

            // If this is a multiple comparison and this gene is in some of
            // them, and we're not doing an XOR
            else if (gene.GenePresentIn == PresentInStates.some && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR)
            {
                colour = " CompareColourSome";
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("GenePresentIn is some and CurrentOperation is not XOR, therefore setting colour to brown.");
#endif*/
            }

            // Else don't show this gene
            else
            {
                colour = " CompareHidden";
/*#if DEBUG
                if (Constants.ShowDebugMessages) Console.Log("This gene will not be shown in this logical comparison.");
#endif*/
            }

            nodeClass += colour;

            // If we're doing a logical comparison (we don't need to check for null since it'll only have a RegulationPresentIn if it's there

#if DEBUG
            //Console.Log("Checking if " + gene.Name + "'s regulation will be shown...");
#endif

            // If this regulation is on the left and we're not doing an AND
            if ((gene as GeneCompareInfo).RegulationPresentIn == PresentInStates.left && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND)
            {
                colour = " CompareColourLeft";
#if DEBUG
                //Console.Log("RegulationPresentIn is left and CurrentOperation is not AND, therefore setting colour to red.");
#endif
            }

            // If this regulation is on the right and we're not doing an AND
            else if ((gene as GeneCompareInfo).RegulationPresentIn == PresentInStates.right && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND)
            {
                colour = " CompareColourRight";
#if DEBUG
                //Console.Log("RegulationPresentIn is right and CurrentOperation is not AND, therefore setting colour to blue.");
#endif
            }

            // If this regulation is on both sides and we're not doing an XOR
            else if ((gene as GeneCompareInfo).RegulationPresentIn == PresentInStates.all && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR)
            {
                colour = " CompareColourAll";
#if DEBUG
                //Console.Log("RegulationPresentIn is both and CurrentOperation is not XOR, therefore setting colour to purple.");
#endif
            }

            // If this is a multiple comparison and this gene is in some of
            // them, and we're not doing an XOR
            else if ((gene as GeneCompareInfo).RegulationPresentIn == PresentInStates.some && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR)
            {
                colour = " CompareColourSome";
#if DEBUG
                //Console.Log("Insert some message here.");
#endif
            }

            // Else if it's not regulated
            else
            {
                // If the gene is in this comparison, show a dashed, faded spoke
                if ((((gene as GeneCompareInfo).GenePresentIn == PresentInStates.left || (gene as GeneCompareInfo).GenePresentIn == PresentInStates.right) && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.AND) ||
                    ((gene as GeneCompareInfo).GenePresentIn == PresentInStates.all && (gene as GeneCompareInfo).CurrentOperation != LogicalOperations.XOR))
                {
                    colour = " GeneNoSite";
#if DEBUG
                    //Console.Log("We would show the regulation if it was there.");
#endif
                }
                // Else don't show the dashed spoke
                else
                {
                    colour = " CompareHidden";
#if DEBUG
                    //Console.Log("This regulation would not be shown in this logical comparison if it was there.");
#endif
                }
            }

            linkClass += colour;

			if ( highlighter != null ) {
				HighlightMode highlight = highlighter( gene );

				if ( highlight == HighlightMode.Hidden ) {
					nodeClass += " GeneHidden";
                    linkClass += " GeneHidden";
				}
				else if ( highlight == HighlightMode.Normal ) {
					nodeClass += " GeneNotSelected";
                    linkClass += " GeneNotSelected";
				}
                else if ( highlight == HighlightMode.Selected ) {
					nodeClass += " GeneSelected";
                    linkClass += " GeneSelected";
                }
				else {
					nodeClass += " GeneHighlighted";
                    linkClass += " GeneHighlighted";
				}
			}

            result["node:class"] = nodeClass;
            result["link:class"] = linkClass;

            return result;
		}

		/// <summary> Assigns a colour to the gene or regulator by lookup in the alphabetically ordered list of
		///		distinct gene functions.
		/// </summary>
		/// <param name="gene">
		///		A gene or regulator info object to be decorated.
		/// </param>
		/// <returns></returns>

		override public JSObject GetAttributes ( IGeneInfo gene ) {
            if (gene is GeneCompareInfo) return GetGeneCompareAttributes((GeneCompareInfo)gene);
			if ( gene is GeneInfo ) return GetGeneAttributes( (GeneInfo) gene );
			if ( gene is RegulatorInfo ) return GetRegulatorAttributes( (RegulatorInfo) gene );
			throw new Exception( "Invalid node type." );
		}
	}
}
