﻿using System;
using System.Collections.Generic;

namespace RegulonExplorer.View.TrnDiff {

    /// <summary>
    /// Enum used to handle the highlight status of TG nodes
    /// <list type="bullet">
    /// <item>0, Normal - node has normal status</item>
    /// <item>1, HighLight - node is highlighted by one of the highlight functions</item>
    /// <item>2, Hidden - node should not be displayed</item>
    /// <item>3, Selected - node has been selected by clicking on it</item>
    /// </list>
    /// </summary>
	public enum HighlightMode {
		Normal = 0,
		HighLight = 1,
		Hidden = 2,
        Selected = 3
	}
}
