// GeneNode.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using RegulonExplorer.ViewModel;
using SystemQut;
using SystemQut.ComponentModel;
using SystemQut.Svg;
using RegulonExplorer.Common;

namespace RegulonExplorer.View.TrnDiff {

	public class GeneNode : AbstractNode {

        // The node's link's SVG element
        private SvgElement link;

        /// <summary>
        /// Gets or sets the node's link's SVG element
        /// </summary>
        public SvgElement Link
        {
            get { return link; }
            set { link = value; }
        }

        // The SVG element for the node's label
        private SvgText label;

        /*public SvgText Label
        {
            get { return label; }
            set { label = value; }
        }*/

        /// <summary>
        /// Creates a new gene node, generating it with either a circle glyph or a wedge glyph depending on whether wedge mode is enabled
        /// </summary>
        /// <param name="parent">An SVG group (or some other SvgContainer) in which this node will be displayed.</param>
        /// <param name="gene">The gene to which this applies.</param>
        /// <param name="i">The index of the gene/node (used to calculate angle)</param>
        /// <param name="n">The total number of genes/nodes in the graph (used to calculate angle)</param>
        /// <param name="radius">The radius of the graph in which the gylph will be placed</param>
        /// <param name="epsilon">The distance from the centre of the graph where the node's link should start</param>
        /// <param name="nodeFactory">A Node Factory that will be used to construct the glyphs for each node.</param>
        /// <param name="colourChooser">An Attribute factory that will be used to decorate the node.</param>
		public GeneNode (
			SvgContainer parent,
			IGeneInfo gene,
			int i,
			int n,
			double radius,
			double epsilon,
			INodeFactory nodeFactory,
			IAttributeFactory colourChooser/*,
            double maxPosition*/
		)
            // Call the base AbstractNode constructor
			: base( parent, gene, nodeFactory, colourChooser ) {

            // Set the node's gene object and colour chooser
			this.gene = gene;
			this.colourChooser = colourChooser;

            // Determine the angle on the circumference of the wheel where the
            // node will be placed
			double theta = Math.PI * ( 2.0 * i / n - 0.5 );

            // Determine sine and cosine of the angle
			double c = Math.Cos( theta );
			double s = Math.Sin( theta );

            // Determine the starting coordinates of the node's link
			double x0 = epsilon * c;
			double y0 = epsilon * s;

            // Get the total radius of the graph
			double r = radius;

            /*if ( maxPosition > 0 && gene is GeneInfo ) {
				SiteInfo site = ( (GeneInfo) gene ).Site;

				if ( site != null ) {
					r = (r - epsilon) * (Math.Abs(site.Position) / maxPosition) + epsilon;
                }
			}*/

            // Determine the coordinates for the centre of the node
			double x1 = r * c;
			double y1 = r * s;

            // Determine the ending coordinates of the node's link
			double x2 = r * c;
			double y2 = r * s;

            // Get the attributes for the node
			JSObject attrs = colourChooser.GetAttributes( gene );

            // If the node should be a wedge...
            if (nodeFactory is WedgeNodeFactory && gene is GeneInfo && (gene as GeneInfo).Site != null) {

                // Create a link wedge for the node
                link = (nodeFactory as WedgeNodeFactory).CreateLink(this, gene);

                // Determine the position of the link on the graph's canvas
                nodeFactory.PlaceNode( link, x1, y1 );

                // Get the original transform data so it is not overwritten
				string oldTransform = Link.TransformProperty ?? string.Empty;

                // Rotate the link wedge
                // Rotates around the centre of the node, using the theta angle
                // calculated for the node previously. Adds to the original
                // transform data
                link.TransformProperty = Transform.RotateXY(theta * (180/Math.PI) - 90, x1, y1) + oldTransform;

            // Otherwise
            } else {

                // Create a link line for the node
			    link = new SvgLine( null, x0, y0, x2, y2, null );

                // Add it before the node's dom element so it appears
                // underneath
			    DomElement.InsertBefore( link.DomElement, Node.DomElement );
            }

            // Determine the position of the node on the graph's canvas
			nodeFactory.PlaceNode( Node, x1, y1 );

            // If the node is a path (i.e. wedge) then rotate it
            if (Node is SvgPath) {
                // Get the original transform data so it is not overwritten
				string oldTransform = Node.TransformProperty ?? string.Empty;
#if DEBUG
                //Console.Log("Angle for " + gene.Name + " = " + theta.ToString());
#endif
                // Rotates around the centre of the node, using the theta angle
                // calculated for the node previously. Adds to the original
                // transform data
                Node.TransformProperty = Transform.RotateXY(theta * (180/Math.PI) - 90, x1, y1) + oldTransform;
            }

            // Determine the coordinates for the node's label
			double x3 = (3 + r) * c;
			double y3 = (3 + r) * s;

            // Create a label for the node
            label = new SvgText( this, new Dictionary<string, string>(
				"x", x3.ToString(),
				"y", y3.ToString(),
				//"text-anchor", "start",
				//"font-size", "10.5",
                //"font-size", "15",
                //"font-size", 7,
                "visibility", "hidden",
                "class", "GeneNodeLabel"
			) );

            // Rotate the label so it is in line with the node's spoke
            // If it is in the right half of the graph, rotate it normally
            if (theta < Math.PI/2) {
                label.TransformProperty = Transform.RotateXY(theta * (180/Math.PI), x3, y3);

            // Otherwise if it is in the left half, rotate it an extra 180
            // degress so it is not upside down
            } else {
                label.TransformProperty = Transform.RotateXY(theta * (180/Math.PI) - 180, x3, y3);
                label.SetAttribute("class", "GeneNodeLabelReverse");
            }

            // Set the text of the label to the gene's name
            label.Text = gene.Name;

            // Add the events to the node's link
			AddEvents( link.DomElement, elementEvents, elementActions );
		}

		/// <summary> Applies the attrbiutes returned by a IAttributeFactory
		///		to the node and link elements.
		/// </summary>
		/// <param name="attrs">
		///		A JavaScript object containing a set of attribute name-value pairs
		///		in the form of a dictionary.
		/// </param>

		protected override void ApplyStyles ( JSObject attrs ) {
			base.ApplyStyles( attrs );

			foreach ( KeyValuePair<string, object> kvp in attrs ) {
				string key = kvp.Key;
				object value = kvp.Value;
				string[] parts = key.Split( ':' );

                // For the moment, the odd conditions with stroke width are to
                // avoid the size changing when the display is zoomed and nodes
                // and spokes are clicked on
				if ( parts.Length == 1  && !(parts[0] == "stroke-width" && link.StrokeWidth > 0)) {
					link.SetAttribute( key, value );
                    if (parts[0] == "stroke-width") {
                        Node.SetAttribute( key, value );
                    }
				}
				else if ( parts.Length == 2 ) {
					if ( parts[0] == "link") link.SetAttribute( parts[1], value );
				}
			}
		}

        /// <summary>
        /// Properly deletes this node and its link from the graph
        /// </summary>
		public override void Dispose () {
            // Remove all events associated with this node's link
			RemoveEvents( link.DomElement, elementEvents, elementActions );

            // Dispose of the link element
			link.Dispose();

            // Dispose of the base node
			base.Dispose();
		}

        /// <summary>
        /// Gets the content for a tooltip as a list of strings
        /// <para>Provides for all types of node:
        /// <list type="bullet">
        /// <item>Locus tag</item>
        /// <item>Name</item>
        /// <item>Function</item>
        /// <item>Regulon ID</item>
        /// <item>Gene ID</item>
        /// </list></para>
        /// <para>Provides for nodes with a valid site:
        /// <list type="bullet">
        /// <item>Site position</item>
        /// <item>Site score</item>
        /// <item>Site sequence</item>
        /// </list></para>
        /// <para>Also provides an annotation and a GO term if that data is available</para>
        /// </summary>
		override public string[] TooltipContent {
			get {

                // Default contents for all genes
				List<string> result = new List<string>(
					Constants.Text_TooltipLocusTag, gene.LocusTag,
					Constants.Text_TooltipName, gene.Name,
					Constants.Text_TooltipGeneFunction, gene.GeneFunction,
					Constants.Text_TooltipRegulonId, Objects.ToString( gene.RegulonId, Constants.Text_TooltipNotFound ),
					Constants.Text_TooltipGeneId, Objects.ToString( gene.VimssId, Constants.Text_TooltipNotFound )
				);

                // If the gene is a regular gene
				if ( gene is GeneInfo ) {
					SiteInfo site = ( (GeneInfo) gene ).Site;

                    // If it has a site, add position, score and sequence
					if ( site != null ) {
						result.AddRange(
							Constants.Text_TooltipSitePosition, Objects.ToString( site.Position, Constants.Text_TooltipNotFound ),
							Constants.Text_TooltipSiteScore, Objects.ToString( site.Score, Constants.Text_TooltipNotFound ),
							Constants.Text_TooltipSiteSequence, Objects.ToString( site.Sequence, Constants.Text_TooltipNotFound )
						);
					}

                    // Otherwise show the site as "not found"
					else {
						result.AddRange(
							Constants.Text_TooltipSite, Constants.Text_TooltipNotFound
						);
					}

                    // Add annotation if one is present
                    if (!string.IsNullOrEmpty(((GeneInfo) gene ).Annotation)) {
					    result.AddRange( Constants.Text_TooltipAnnotation, ( (GeneInfo) gene ).Annotation );
                    }

                    // Add GO term if one is present
                    if (!string.IsNullOrEmpty((gene as GeneInfo).GoTerm)) {
                        result.AddRange(Constants.Text_TooltipGoTerm, (gene as GeneInfo).GoTerm);
                    }

                    // Add parent GO term if one is present
                    if (!string.IsNullOrEmpty((gene as GeneInfo).GoParentTerm)) {
                        result.AddRange(Constants.Text_TooltipGoParentTerm, (gene as GeneInfo).GoParentTerm);
                    }
				}

				return (string[]) result;
			}
		}

        // Stores whether the label for this node should be visible
        private bool labelVisibility = false;

        /// <summary>
        /// Gets or sets the current visibility of the node's label
        /// <para>Automatically sets the label to visible or hidden when the value is set</para>
        /// </summary>
        public bool LabelVisibility {
            get { return labelVisibility; }
            set { labelVisibility = value;

                // Override for when the node and link are invisible, as the
                // label should not be visible as well in this case
                if (Node.Visibility == "hidden" && Link.Visibility == "hidden") {
                    label.Visibility = "hidden";

                // Set the label element's visibility as appropiate to the
                // given value
                } else {
                    label.Visibility = (value /*|| IsSelected*/) ? "visible" : "hidden";
                }
            }
        }

        /*protected override void gene_PropertyChanged(object sender, PropertyChangedEventArgs args) {
            base.gene_PropertyChanged(sender, args);
            if ( args.Includes( "IsSelected" ) ) {
                if (!labelVisibility && !(Node.Visibility == "hidden" && Link.Visibility == "hidden")) {
                    label.Visibility = IsSelected ? "visible" : "hidden" ;
                }
            }
        }*/

        /// <summary>
        /// Gets or sets the font size of the label
        /// </summary>
        public string LabelFontSize {
            get { return label.FontSize; }
            set { label.FontSize = value; }
        }
	}
}
