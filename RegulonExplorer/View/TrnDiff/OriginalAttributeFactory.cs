﻿using System;
using System.Collections.Generic;
using RegulonExplorer.ViewModel;
using SystemQut;
using SystemQut.ComponentModel;
#if DEBUG
using System.Html;
using RegulonExplorer.Common;
#endif

namespace RegulonExplorer.View.TrnDiff {

	/// <summary> Assigns colours to regulators based on their regulator function.
	/// <para>
	///		Colour assignment is arbitrary, based on the position of regulator function in an alpha-ordered
	///		list of functions encountered in the containing regulog.
	/// </para>
	/// </summary>

	public class OriginalAttributeFactory : AbstractAttributeFactory {

		private const string UNKNOWN_COLOUR = "#dddddd";
		private const string REGULATOR_COLOUR = "#e2041b";
		private const string SELECTED_FILL_COLOUR = "gold";
		private const string SELECTED_STROKE_COLOUR = "black";
		private object HIGHLIGHT_STROKE_COLOUR = "#302833";
		private string NO_REGULATOR_COLOUR = "gray";

		private readonly RegulogInfo regulog;
		private readonly Dictionary<string, int> geneIndex = new Dictionary<string, int>();

		// Colour scheme originating in d3.v3.js: d3.scale.category20()
		private string[] thisPalette = { "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f", "#c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5" };

		/// <summary> Initialises the colour chooser, creating a lokkuptable 
		///		which maps each distinct lower-case function to a non-negative integer.
		/// </summary>
		/// <param name="regulog">
		///		The regulog which contains the referenced target regulators.
		/// </param>

		public OriginalAttributeFactory ( RegulogInfo regulog ) {
			this.regulog = regulog;

			string [] functions = regulog.GeneFunctions;

			for ( int i = 0; i < functions.Length; i++ ) {
				geneIndex[functions[i]] = i;
			}

            ColourPalette = thisPalette;
		}

		/// <summary> Assigns a colour to the regulator by lookup in the alphabetically ordered list of 
		///		distinct gene functions.
		/// </summary>
		/// <param name="gene"></param>
		/// <returns></returns>

		private JSObject GetGeneAttributes ( GeneInfo gene ) {
			JSObject result = new JSObject();

			int idx = geneIndex[gene.GeneFunction.ToLowerCase()];

			result["opacity"] = "1";
/*#if DEBUG
            if (Window.OuterWidth > Constants.LargeScaleWidth || Window.OuterHeight > Constants.LargeScaleHeight) {
                result["stroke-width"] = "4.5";
            } else {
#endif*/
			result["stroke-width"] = "1.5";
/*#if DEBUG
            }
#endif*/

			string colour = Script.IsValue( idx ) ? ColourPalette[idx % ColourPalette.Length] : UNKNOWN_COLOUR;

			result["node:fill"] = colour;
			result["node:stroke"] = gene.IsSelected ? SELECTED_STROKE_COLOUR: colour;

			result["link:fill"] = colour;
			result["link:stroke"] = gene.IsSelected ? SELECTED_STROKE_COLOUR : colour;
			result["link:stroke-dasharray"] = gene.Site == null ? "5,5" : "none";

			if ( highlighter != null ) {
				HighlightMode highlight = highlighter( gene );

				if ( highlight == HighlightMode.Hidden ) {
					result["opacity"] = "0";
				}
				else if ( highlight == HighlightMode.Normal ) {
					result["opacity"] = "0.25";
				}
				else {
					result["node:stroke"] = HIGHLIGHT_STROKE_COLOUR;
					result["link:stroke"] = HIGHLIGHT_STROKE_COLOUR;
				}
			}

			return result;
		}

		/// <summary> Assigns a colour to the regulator by lookup in the alphabetically ordered list of 
		///		distinct regulator functions.
		/// </summary>
		/// <param name="regulator"></param>
		/// <returns></returns>

		private JSObject GetRegulatorAttributes ( RegulatorInfo regulator ) {
			string colour = regulator.VimssId > 0 ? REGULATOR_COLOUR : NO_REGULATOR_COLOUR;

			JSObject result = new JSObject();
			result["node:fill"] = colour;
			result["node:stroke"] = colour;
			return result;
		}

		/// <summary> Assigns a colour to the gene or regulator by lookup in the alphabetically ordered list of 
		///		distinct gene functions.
		/// </summary>
		/// <param name="gene">
		///		A gene or regulator info object to be decorated.
		/// </param>
		/// <returns></returns>

		override public JSObject GetAttributes ( IGeneInfo gene ) {
			if ( gene is GeneInfo ) return GetGeneAttributes( (GeneInfo) gene );
			if ( gene is RegulatorInfo ) return GetRegulatorAttributes( (RegulatorInfo) gene );
			throw new Exception( "Invalid node type." );
		}
	}
}
