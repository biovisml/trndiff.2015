﻿using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using SystemQut.Svg;

namespace RegulonExplorer.View.TrnDiff {

    /// <summary>
    /// Creates wedge nodes for target genes. Transcription factors remain as circles
    /// </summary>
	public class WedgeNodeFactory: INodeFactory {
		public double Radius = 3.5;
        private double angle;

        /// <summary>
        /// Gets or sets the angle that each wedge will cover
        /// </summary>
        public double Angle
        {
            get { return angle; }
            set { angle = value; }
        }

        private double graphRadius;

        /// <summary>
        /// Gets or sets the radius of the graph
        /// </summary>
        public double GraphRadius
        {
            get { return graphRadius; }
            set { graphRadius = value; }
        }

        //private double thicknessRegulated = 15;
        //private double thicknessNonRegulated = 5;

        private double nodeThickness = 0.15;
        private double linkThickness = 0.7;
        private double linkStartThickness = 0.15;

		/// <summary> Creates a node of the desired type.
		/// </summary>
		/// <param name="container">
		///		the SVG Container to which the node will be added.
		/// </param>
		/// <param name="contentObject">
		///		The object to which the node is to be attached. The factory may query this object to
		///		determine appropriate action to take, for example by using reflection to check what type
		///		of object it is.
		/// </param>
		/// <returns></returns>

		public SvgElement CreateNode ( SvgContainer container, object contentObject ) {
			SvgElement shape = null;

            // If the current gene is a TG, create a wedge at the edge of the
            // wheel
			if ( contentObject is GeneInfo ) {
                //double thickness = (contentObject as GeneInfo).Site != null ? GraphRadius * 0.85 : GraphRadius * 0.15;

                // Set the thickness of the wedge
                // This will be measured from the outside of the wheel
                double wedgeThickness = GraphRadius * nodeThickness;

                // If the angle is equal to pi, then the wedge should be a
                // whole circle
                if (Angle == Math.PI) {

                    // Create a new SVG path
                    shape = new SvgPath( container, null );

                    // Draw the circle
                    (shape as SvgPath).MoveTo(0, 0, true)
                        .EllipticalArcTo(GraphRadius, GraphRadius, 0, false, false, 0, -(GraphRadius*2), true)
                        .EllipticalArcTo(GraphRadius, GraphRadius, 0, false, false, 0, 0, true)
                        .VerticalLineTo(-wedgeThickness, true)
                        .EllipticalArcTo((GraphRadius - wedgeThickness), (GraphRadius - wedgeThickness), 0, false, true, 0, -(GraphRadius*2 - wedgeThickness), true)
                        .EllipticalArcTo((GraphRadius - wedgeThickness), (GraphRadius - wedgeThickness), 0, false, true, 0, -wedgeThickness, true)
                        .ClosePath();

                // Otherwise, create a regular wedge
                } else {

                    // Calculate the opposite and adjacent coordinates so the
                    // wedge can be drawn correctly
                    // "wedge" refers to the inner coordinates of the wedge
                    // "total" refers to the coordinates at the edge of the
                    // wheel, and the outside of the wedge
                    double wedgeOpposite = wedgeThickness * Math.Sin(Angle);
                    double wedgeAdjacent = wedgeThickness * Math.Cos(Angle);
                    double totalOpposite = GraphRadius * Math.Sin(Angle);
                    double totalAdjacent = GraphRadius * Math.Cos(Angle);

                    // Create a new SVG path
                    shape = new SvgPath( container, null );

                    // Draw the wedge
                    (shape as SvgPath).MoveTo(-totalOpposite, -(GraphRadius - totalAdjacent), true)
                        .EllipticalArcTo(GraphRadius, GraphRadius, 0, false, false, totalOpposite, -(GraphRadius - totalAdjacent), true)
                        .LineTo((totalOpposite - wedgeOpposite), -(GraphRadius - totalAdjacent + wedgeAdjacent), true)
                        .EllipticalArcTo((GraphRadius - wedgeThickness), (GraphRadius - wedgeThickness), 0, false, true, -(totalOpposite - wedgeOpposite), -(GraphRadius - totalAdjacent + wedgeAdjacent), true)
                        .ClosePath();
                }
            }

            // A regulator should appear as a regular circle at the centre,
            // unless there is a number of target genes specified, in which
            // case it should appear as text showing that number
			else if ( contentObject is RegulatorInfo ) {
                if (( contentObject as RegulatorInfo ).NumberOfTargetGenes != -1) {
                    shape = new SvgText( container, null );
                    (shape as SvgText).Text = ( contentObject as RegulatorInfo ).NumberOfTargetGenes.ToString();
                } else {
                    shape = new SvgCircle( container, 0, 0, Radius, null );
                }
            }

            // Return the created shape
			return shape;
		}

		/// <summary> Positions the node centrally at the specified coordinates.
		/// </summary>
		/// <param name="node">
		///		The node to be positioned.
		/// </param>
		/// <param name="x">
		///		The horizontal location relative to the containing viewport.
		/// </param>
		/// <param name="y">
		///		The vertical location relative to the containing viewport.
		/// </param>

		public void PlaceNode ( SvgElement node, double x, double y ) {

            // If the node is a circle, place it normally
			if ( node is SvgCircle ) {
				SvgCircle circle = node as SvgCircle;

                // Set the x and y cooridnates of the circle
				circle.CX = x;
				circle.CY = y;
			}

            // If the node is a SVG path (i.e. a wedge), a translation
            // transform property must be added to the path's existing
            // properties
			else if ( node is SvgPath ) {
				SvgPath path = node as SvgPath;
				string oldTransform = path.TransformProperty ?? string.Empty;
                path.TransformProperty = Transform.TranslateXY( x, y ) + oldTransform;
			}

            // If the node is text, place it normally
            if ( node is SvgText ) {
                SvgText text = node as SvgText;

                // Set the x and y coordinates of the text
                text.X = x;
                text.Y = y;

                // Add a bit of vertical distance so that the centre is the
                // centre of the text
                text.DY = 6;

                // Set the text anchor to "middle" so that the text appears
                // centred
                text.TextAnchor = TextAnchorType.middle;
            }
		}

        /// <summary> Creates a link of the desired type.
		/// </summary>
		/// <param name="container">
		///		the SVG Container to which the node will be added.
		/// </param>
		/// <param name="contentObject">
		///		The object to which the node is to be attached. The factory may query this object to
		///		determine appropriate action to take, for example by using reflection to check what type
		///		of object it is.
		/// </param>
		/// <returns></returns>

		public SvgElement CreateLink ( SvgContainer container, object contentObject ) {
			SvgElement shape = null;

            // In a wedge display, the links are *also* wedges, which fill in
            // the space between the outer wedge node and the regulator

            // Set the thickness of the wedge
            // The first is how far the wedge extends outward, while the second
            // is how far the wedge starts from the inside
            double wedgeThickness = GraphRadius * linkThickness;
            double gapThickness = GraphRadius * linkStartThickness;

            // If the angle is equal to pi, then the wedge should be a
            // whole circle
            if (Angle == Math.PI) {

                // Create a new SVG path
                shape = new SvgPath( container, null );

                // Draw the circle
                (shape as SvgPath).MoveTo(0, -gapThickness  , true)
                    .EllipticalArcTo(GraphRadius - gapThickness, GraphRadius - gapThickness, 0, false, false, 0, -(GraphRadius*2 - gapThickness), true)
                    .EllipticalArcTo(GraphRadius - gapThickness, GraphRadius - gapThickness, 0, false, false, 0, -gapThickness, true)
                    .VerticalLineTo(-(wedgeThickness + gapThickness), true)
                    .EllipticalArcTo((GraphRadius - wedgeThickness - gapThickness), (GraphRadius - wedgeThickness - gapThickness), 0, false, true, 0, -(GraphRadius*2 - (gapThickness + wedgeThickness)), true)
                    .EllipticalArcTo((GraphRadius - wedgeThickness - gapThickness), (GraphRadius - wedgeThickness - gapThickness), 0, false, true, 0, -(wedgeThickness + gapThickness), true)
                    .ClosePath();
            } else {

                // Calculate the opposite and adjacent coordinates so the
                // wedge can be drawn correctly
                // "wedge" refers to the coordinates that meet the wedge node
                // "gap" refers to the coordinates that are near the regulator
                // "total" refers to the coordinates at the edge of the wheel
                double wedgeOpposite = wedgeThickness * Math.Sin(Angle);
                double wedgeAdjacent = wedgeThickness * Math.Cos(Angle);
                double gapOpposite = gapThickness * Math.Sin(Angle);
                double gapAdjacent = gapThickness * Math.Cos(Angle);
                double totalOpposite = GraphRadius * Math.Sin(Angle);
                double totalAdjacent = GraphRadius * Math.Cos(Angle);

                // Create a new SVG path
                shape = new SvgPath( container, null );

                // Draw the wedge
                (shape as SvgPath).MoveTo(-(totalOpposite - gapOpposite), -(GraphRadius - totalAdjacent + gapAdjacent), true)
                    .EllipticalArcTo((GraphRadius - gapThickness), (GraphRadius - gapThickness), 0, false, false, totalOpposite - gapOpposite, -(GraphRadius - totalAdjacent + gapAdjacent), true)
                    .LineTo((totalOpposite - wedgeOpposite - gapOpposite), -(GraphRadius - totalAdjacent + gapAdjacent + wedgeAdjacent), true)
                    .EllipticalArcTo((GraphRadius - wedgeThickness - gapThickness), (GraphRadius - wedgeThickness - gapThickness), 0, false, true, -(totalOpposite - wedgeOpposite - gapOpposite), -(GraphRadius - totalAdjacent + gapAdjacent + wedgeAdjacent), true)
                    .ClosePath();
            }

            // Return the created shape
			return shape;
		}
	}
}
