﻿using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using SystemQut.Svg;
#if DEBUG
using System.Html;
using RegulonExplorer.Common;
#endif

namespace RegulonExplorer.View.TrnDiff {

    /// <summary>
    /// Creates circle nodes for target genes and transcription factors
    /// </summary>
	public class CircleNodeFactory: INodeFactory {
		public double Radius = 3.5;

		/// <summary> Creates a node of the desired type.
		/// </summary>
		/// <param name="container">
		///		the SVG Container to which the node will be added.
		/// </param>
		/// <param name="contentObject">
		///		The object to which the node is to be attached. The factory may query this object to
		///		determine appropriate action to take, for example by using reflection to check what type
		///		of object it is.
		/// </param>
		/// <returns></returns>

		public SvgElement CreateNode ( SvgContainer container, object contentObject ) {
			//if ( contentObject is GeneInfo ) { }
			//else if ( contentObject is RegulatorInfo ) { }
/*#if DEBUG
            if (Window.OuterWidth > Constants.LargeScaleWidth || Window.OuterHeight > Constants.LargeScaleHeight) {
                return new SvgCircle( container, 0, 0, Radius*3, null );
            } else {
#endif*/
            // Create a new svg circle with the specified radius
			return new SvgCircle( container, 0, 0, Radius, null );
/*#if DEBUG
            }
#endif*/
		}

		/// <summary> Positions the node centrally at the specified coordinates.
		/// </summary>
		/// <param name="node">
		///		The node to be positioned.
		/// </param>
		/// <param name="x">
		///		The horizontal location relative to the containing viewport.
		/// </param>
		/// <param name="y">
		///		The vertical location relative to the containing viewport.
		/// </param>

		public void PlaceNode ( SvgElement node, double x, double y ) {
			SvgCircle circle = node as SvgCircle;

            // Set the x and y cooridnates of the circle
			circle.CX = x;
			circle.CY = y;
		}
	}
}
