﻿using System;
using System.Collections.Generic;

using RegulonExplorer.ViewModel;
using SystemQut;

namespace RegulonExplorer.View.TrnDiff {
	/// <summary> A ColourChooser is used to obtain SVG attribute-value pairs for the
	///		various drawing elements that compose the visual representation of a regulator
	///		or regulator.
	/// </summary>

	public interface IAttributeFactory {

		/// <summary> Get the colour associated with a regulator which will be laid out
		///		in the context of a collection where it notionally has a sequential
		///		location <c>index</c> ranging from <c>0</c> to <c>(collectionSize-1)</c>.
		/// </summary>
		/// <param name="gene">
		///		Gene summary information.
		/// </param>
		///	<returns>
		///		A JavaScript object containing a set of attribute-value pairs to be applied to the
		///		visual element representing the regulator.
		/// </returns>

		JSObject GetAttributes ( IGeneInfo gene );

		/// <summary> Get or set the delegate which maps genes to highlight-modes.
		/// </summary>

		Func<IGeneInfo,HighlightMode> Highlighter { get; set; }

        /// <summary> Get or set the main colour palette for this attribute factory
        /// </summary>

        string[] ColourPalette { get; set; }

        /// <summary> Get the regulator colour for this attribute factory
        /// </summary>
        string RegulatorColour { get; }

        /// <summary> Get the colour of the center node when it is not a regulator from the attribute factory
        /// </summary>
        string NoRegulatorColour { get; }
	}
}
