﻿using System;
using System.Collections.Generic;
using RegulonExplorer.ViewModel;
using SystemQut;
using SystemQut.ComponentModel;

namespace RegulonExplorer.View.TrnDiff {
	public abstract class AbstractAttributeFactory: IAttributeFactory, INotifyPropertyChanged {
		abstract public JSObject GetAttributes ( IGeneInfo gene );

		/// <summary> PropertyChanged event handler.
		/// </summary>

		public event PropertyChangedEventHandler PropertyChanged;

		protected Func<IGeneInfo,HighlightMode> highlighter;

        protected string[] colourPalette;

		protected string regulatorColour = "pink";
        protected string noRegulatorColour = "yellow";

		/// <summary> Fire PropertyChanged events.
		/// </summary>

		protected void NotifyPropertyChanged ( string propertyName ) {
			if ( PropertyChanged != null ) {
				PropertyChanged( this, new PropertyChangedEventArgs( propertyName ) );
			}
		}

		/// <summary> Get or set the delegate which maps genes to highlight-modes.
		/// </summary>

		public Func<IGeneInfo, HighlightMode> Highlighter {
			get {
				return highlighter;
			}
			set {
				highlighter = value;
				NotifyPropertyChanged("Highlighter");
			}
		}

        /// <summary> Get or set the main colour palette for this attribute factory
        /// </summary>

        public string[] ColourPalette {
            get {
                return colourPalette;
            }
            set {
                colourPalette = value;
            }
        }

        /// <summary> Get the regulator colour for this attribute factory
        /// </summary>
        public string RegulatorColour {
            get { return regulatorColour; }
        }

        /// <summary> Get the colour of the center node when it is not a regulator from the attribute factory
        /// </summary>
        public string NoRegulatorColour {
            get { return noRegulatorColour; }
        }
	}
}
