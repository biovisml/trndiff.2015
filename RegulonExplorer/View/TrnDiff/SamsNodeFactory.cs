﻿using RegulonExplorer.ViewModel;
using System;
using System.Collections.Generic;
using SystemQut.Svg;

namespace RegulonExplorer.View.TrnDiff {
	public class SamsNodeFactory : INodeFactory {
		public double Radius = 3.5;

		/// <summary> Creates a node of the desired type, centered on the origin.
		/// </summary>
		/// <param name="container">
		///		the SVG Container to which the node will be added.
		/// </param>
		/// <param name="contentObject">
		///		The object to which the node is to be attached. The factory may query this object to 
		///		determine appropriate action to take, for example by using reflection to check what type
		///		of object it is.
		/// </param>
		/// <returns></returns>

		public SvgElement CreateNode ( SvgContainer container, object contentObject ) {
			SvgElement shape = null;
	
			if ( contentObject is GeneInfo ) {
				if ( String.IsNullOrEmpty( ( contentObject as GeneInfo ).Name ) || ( contentObject as GeneInfo ).Name == "undefined" ) {
					/*SystemQut.Controls.Point[] points = new SystemQut.Controls.Point[4];
					points[0] = new SystemQut.Controls.Point(0, 0);
					points[1] = new SystemQut.Controls.Point(0, Radius*2);
					points[2] = new SystemQut.Controls.Point(Radius * 2, Radius * 2);
					points[3] = new SystemQut.Controls.Point(Radius * 2, 0);
					return new SvgPolygon(container, points, null);*/

/*<<<<<<< .mine
                    SvgRectangle rectangle = new SvgRectangle(container, 0, 0, Radius * 2, Radius * 2, null);
                    //rectangle.Transform = "rotate(45 0 0)";
                    return rectangle;
                }
                return new SvgCircle(container, 0, 0, Radius, null);
            }
=======*/
					shape = new SvgRectangle( container, -Radius, -Radius, Radius * 2, Radius * 2, null );
					shape.TransformProperty = Transform.Rotate( 45 );
				}
				else {
					//shape = new SvgCircle( container, 0, 0, Radius, null );
                    shape = new SvgPath( container, null );
                    (shape as SvgPath).MoveTo(0, Math.Round(Radius), true)
					    .LineTo(Math.Round(Radius), -Math.Round(Radius), true)
					    .LineTo(-Math.Round(Radius), -Math.Round(Radius), true)
                        .ClosePath();
				}
			}
//>>>>>>> .r86
			else if ( contentObject is RegulatorInfo ) {
				shape = new SvgRectangle( container, -Radius, -Radius, Radius * 2, Radius * 2, null );
                //shape = new SvgPath( container, null );
                //(shape as SvgPath).MoveTo(0, 3, true)
				//	.LineTo(3, -3, true)
				//	.LineTo(-3, -3, true)
                //    .ClosePath();
			}
			else {
				shape = new SvgCircle( container, 0, 0, Radius, null );
			}
			
			return shape;
		}

		/// <summary> Positions the node centrally at the specified coordinates.
		/// </summary>
		/// <param name="node">
		///		The node to be positioned.
		/// </param>
		/// <param name="x">
		///		The horizontal location relative to the containing viewport.
		/// </param>
		/// <param name="y">
		///		The vertical location relative to the containing viewport.
		/// </param>

		public void PlaceNode ( SvgElement node, double x, double y ) {
/*<<<<<<< .mine
            if (node is SvgCircle) {
                SvgCircle circle = node as SvgCircle;
			    circle.CX = x;
			    circle.CY = y;
            }
            else if (node is SvgRectangle)
            {
                SvgRectangle rectangle = node as SvgRectangle;
                rectangle.X = x - rectangle.Width / 2;
                rectangle.Y = y - rectangle.Width / 2;
                rectangle.Transform = "rotate(45 0 0)";
            }
            else if (node is SvgPolygon)
            {
                SvgPolygon polygon = node as SvgPolygon;
                for (int i = 0; i < polygon.Points.Length; i++)
                {
                    polygon.Points[i].X += x;
                    polygon.Points[i].Y += y;
                }
            }
=======*/
			if ( node is SvgCircle ) {
				SvgCircle circle = node as SvgCircle;
				circle.CX = x;
				circle.CY = y;
			}
			else if ( node is SvgRectangle ) {
				// NB: TRansforms are applied from right to left, not left to right.
				// IMO this is very unclearly expressed in the spec.
				SvgRectangle rectangle = node as SvgRectangle;
				string oldTransform = rectangle.TransformProperty ?? string.Empty;
				rectangle.TransformProperty = Transform.TranslateXY( x, y ) + oldTransform;
			}
			else if ( node is SvgPolygon ) {
				SvgPolygon polygon = node as SvgPolygon;
				for ( int i = 0; i < polygon.Points.Length; i++ ) {
					polygon.Points[i].X += x;
					polygon.Points[i].Y += y;
				}
			}
			else if ( node is SvgPath ) {
				SvgPath path = node as SvgPath;
				//TO DO: positioning
				string oldTransform = path.TransformProperty ?? string.Empty;
                path.TransformProperty = Transform.TranslateXY( x, y ) + oldTransform;
			}
//>>>>>>> .r86
		}
	}
}
