﻿using System;
using System.Collections.Generic;
using SystemQut.Svg;

namespace RegulonExplorer.View.TrnDiff {
	public interface INodeFactory {

		/// <summary> Creates a node of the desired type.
		/// </summary>
		/// <param name="container">
		///		the SVG Container to which the node will be added.
		/// </param>
		/// <param name="contentObject">
		///		The object to which the node is to be attached. The factory may query this object to 
		///		determine appropriate action to take, for example by using reflection to check what type
		///		of object it is.
		/// </param>
		/// <returns></returns>

		SvgElement CreateNode ( SvgContainer container, object contentObject );

		/// <summary> Positions the node centrally at the specified coordinates.
		/// </summary>
		/// <param name="node">
		///		The node to be positioned.
		/// </param>
		/// <param name="x">
		///		The horizontal location relative to the containing viewport.
		/// </param>
		/// <param name="y">
		///		The vertical location relative to the containing viewport.
		/// </param>

		void PlaceNode ( SvgElement node, double x, double y );
	}
}
