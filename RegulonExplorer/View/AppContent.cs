﻿using System;
using System.Collections.Generic;

using System.Html;
using SystemQut.Controls;
using SystemQut;

namespace RegulonExplorer.View {

	/// <summary> Generaic code behind which allows dom elements to be treated 
	///		as members of mutex groups.
	/// </summary>

	public class AppContent : Activated {
		public static string CSS_CLASS { get { return "AppContent"; } }

		/// <summary> Initialise a ContentClass and bind the supplied 
		/// </summary>
		/// <param name="domElement">
		///		A non-null DOM element to which this code behind will be bound.
		/// </param>
		/// <exception cref="Exception">
		///		Excception is thrown if domeElement is not a value.
		/// </exception>

		public AppContent ( Element domElement )
			: base( domElement != null ? domElement : HtmlUtil.CreateElement( "div", null, new Dictionary<string, string>( "class", AppContent.CSS_CLASS ) ) ) {
		}
	}
}
