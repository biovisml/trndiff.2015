﻿using System;
using System.Collections.Generic;

using SystemQut.Controls;
using System.Html;
using SystemQut;

namespace RegulonExplorer.View {

	/// <summary> Tool buttons are used to activate content areas in the application.
	/// </summary>

	public class ToolButton : Activated {
		private const string TAG_NAME = "span";
		private const char CSS_ID_SPECIFIER = '#';
		private const char CSS_CLASS_SPECIFIER = '.';

		public static string CSS_CLASS {
			get {
				return "ToolButton";
			}
		}

		private const string ACTIVATES_ATTR = "data-activates";

		private IActivated activatedControl;
		private  ToolButton[] buttonGroup;
		private  ToolButton parentButton;

		/// <summary> Creates a new ToolButton and either binds it to a pre-existing
		///		DOM element (if one is supplied) or to a newly created DOM element
		///		(if none is supplied.)
		/// </summary>
		/// <param name="domElement">
		///		The (possibly null or indefined) DOM element to which this ToolButton
		///		code-behind is to be attached.
		/// </param>

		public ToolButton ( Element domElement )
			: base( ValidateDomElement( domElement )
		) {
			domElement.AddEventListener( "click", new ElementEventListener( ButtonClicked ), false );

            // New tag that allows buttons to be disabled
            IsDisabled = (bool) DomElement.GetAttribute( "data-disabled" );
		}

		/// <summary> Checks the DOM element supplied, and if it is not a value replaces it with
		///		a reference to a newly created element.
		/// </summary>
		/// <param name="domElement">
		///		If not null, this is a reference to a DOM element defined in markup which is to be
		///		attached to this code behind.
		/// </param>
		/// <returns>
		///		If
		/// </returns>

		private static Element ValidateDomElement ( Element domElement ) {
			if ( !Script.IsValue( domElement ) ) {
				domElement = Document.CreateElement( TAG_NAME );
				domElement.ClassName = CSS_CLASS;
			}
			return domElement;
		}

		/// <summary> Executes the action performed by
		/// </summary>
		/// <param name="eventArgs"></param>

		public void ButtonClicked ( ElementEvent eventArgs ) {

            // Only do something if the button is not disabled
            if (!IsDisabled) {
			    activatedControl.IsActivated = true;
			    IsSelected = true;
            }
		}

        /// <summary>
        /// Gets or sets whether this button is selected
        /// </summary>
		public override bool IsSelected {
			get {
				return base.IsSelected;
			}
			set {
				base.IsSelected = value;

                // If it was set to being selected
				if ( value ) {

                    // If this button is in a group
					if ( buttonGroup != null ) {

                        // For each other button in the group, ensure they are
                        // not selected
						foreach ( ToolButton sibling in buttonGroup ) {
							if ( sibling != this )
								sibling.IsSelected = false;
						}
					}

                    // Also make the parent button of this button selected
					if ( parentButton != null ) {
						parentButton.IsSelected = true;
					}
				}
			}
		}

		/// <summary> Get or set the CSS selector that identifies the object activated by this
		/// </summary>

		public string ActivatedId {
			get {
				return (string) DomElement.GetAttribute( ACTIVATES_ATTR );
			}
			set {
				DomElement.SetAttribute( ACTIVATES_ATTR, value );
			}
		}

		/// <summary> Get ior set the parent button. This is selected automatically when the
		///		current button is selected.
		/// </summary>

		public ToolButton ParentButton {
			get {
				return parentButton;
			}
			set {
				parentButton = value;
			}
		}

		/// <summary> Get or set the ButtonGroup to which this button belongs.
		/// <para>
		///		The other members of this group are automatically deselected when this button is selected.
		/// </para>
		/// </summary>

		public ToolButton[] ButtonGroup {
			get {
				return buttonGroup;
			}
			set {
				buttonGroup = value;
			}
		}

		/// <summary> Get or set the current activated control.
		/// </summary>

		public IActivated ActivatedControl {
			get { return activatedControl; }
			set { activatedControl = value; }
		}
	}
}
