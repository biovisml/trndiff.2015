﻿using SystemQut;
using System.Collections.Generic;

namespace Test
{
    //This stores all of the genes in a genome from a JSON data file - for
    //Yersinia_Fur
    class GenomeYersinia
    {
        private string genome;
        private string tf;
        private GeneYersinia[] tgs;

        //The name of this genome
        public string Genome
        {
            get { return genome; }
            set { genome = value; }
        }

        //The transcription factor of this genome
        public string TF
        {
            get { return tf; }
            set { tf = value; }
        }

        //All the genes in this genome
        public GeneYersinia[] TGs
        {
            get { return tgs; }
            set
            {
                tgs = value;
            }
        }

        //Constructor - does not add genes immediately
        public GenomeYersinia(string genome, string tf)
        {
            this.genome = genome;
            this.tf = tf;
            tgs = new GeneYersinia[0];
        }

    }
}
