﻿using SystemQut;

namespace Test
{
    class GeneYersinia //: IJSONGene
    {
        string tg;
        int weight_;
        private string bioFunction_;
        private string operon_;
        private string group_;
        private string product_;
        private string locus_;

        //The name of the gene
        public string TG
        {
            get { return tg; }
            set { tg = value; }
        }

        //The weight of the gene (i.e. whether it is present or not)
        public int weight
        {
            get { return weight_; }
            set { weight_ = value; }
        }

        public string bioFunction
        {
            get { return bioFunction_; }
            set { bioFunction_ = value; }
        }

        public string operon
        {
            get { return operon_; }
            set { operon_ = value; }
        }

        public string group
        {
            get { return group_; }
            set { group_ = value; }
        }

        public string product
        {
            get { return product_; }
            set { product_ = value; }
        }

        public string locus
        {
            get { return locus_; }
            set { locus_ = value; }
        }

        //Constructor - for the most basic gene possible
        //public GeneYersinia(string tg, int weight)
        //{
        //    this.tg = tg;
        //    this.weight = weight;
        //    this.bioFunction = null;
        //    this.operon = null;
        //    this.group = null;
        //}

        //Constructor for an empty gene
        //public GeneYersinia()
        //{
        //    this.tg = null;
        //    this.weight = 0;
        //    this.bioFunction = null;
        //    this.operon = null;
        //    this.group = null;
        //}

        //Constructor for a gene with full data
        public GeneYersinia(string tg, int weight, string bioFunction, string operon, string group)
        {
            this.tg = tg;
            this.weight = weight;
            this.bioFunction = bioFunction;
            this.operon = operon;
            this.group = group;
        }

        //Constructor for a gene without a group
        //public GeneYersinia(string tg, int weight, string bioFunction, string operon)
        //{
        //    this.tg = tg;
        //    this.weight = weight;
        //    this.bioFunction = bioFunction;
        //    this.operon = operon;
        //    this.group = null;
        //}


    }
}
