// TestControls.cs
//

using System;
using System.Collections.Generic;
using Test.Client.Controls;
using System.Html;
using SystemQut;
using SystemQut.Controls;
using SystemQut.ComponentModel;

using ElementEventHandler = SystemQut.Controls.ElementEventHandler;
using System.Collections;

namespace Test.Client {

	public class TestControls {

		public readonly Action[] Tests = null;

		public TestControls () {
			Tests = new Action[] {
				this.LoadControlTemplate_DisplayFullDataGrid_VerifyRowSelection,
				this.RecordNavigator,
				this.DrawButtons,
				this.CreateSelectElement,
				this.LoadControlTemplate_DisplayFullDataGrid_VerifyDataBinding,
				this.LoadControlTemplate_DisplayEmptyDataGrid,
				this.LoadControlTemplate_BindDataGrid,
				this.LoadControlTemplate_VerifyChildElements,
				this.Select_14_GettingStartedExample01f,
				this.Select_13_GettingStartedExample01e,
			};
		}

		void LoadControlTemplate_DisplayFullDataGrid_VerifyRowSelection () {
			const string T = "LoadControlTemplate_DisplayFullDataGrid_VerifyRowSelection";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				const string templateUri = "/UnitTests/Templates/ChemicalElementTable.html";
				TextControl display = new TextControl( null, "p" );
				display.AddElementTo( contentHolder );

				display.Text = @"This test succeeds iff a DataGrid exhibits correct databinding behaviour when rows are clicked.";

				Action<DocumentFragment> onSuccess = delegate( DocumentFragment fragment ) {
					List<Control> controls = new List<Control>();

					Util.ForEachElement( fragment.ChildNodes, delegate( Element e ) {
						if ( Control.HasCodeBehind( e ) )
							controls.Add( (Control) e );
					} );

					Assert.IsTrue( controls.Length == 1, T + ": " + "controls.Length == 1" );
					Assert.IsTrue( controls[0].CodeBehind is DataGrid, T + ": " + "controls[0].CodeBehind is DataGrid" );

					DataGrid dataGrid = (DataGrid) controls[0].CodeBehind;
					dataGrid.AddElementTo( contentHolder );

					ObservableList items = new ObservableList( null );

					foreach ( ChemicalElement element in ChemicalElement.Elements ) {
						items.Add( element.Clone() );
					}

					dataGrid.ItemsSource = items;

					RecordNavigator navigator = new SystemQut.Controls.RecordNavigator(null);
					navigator.Formatter = delegate( string format, int current, int min, int max ) { 
						return Script.IsValue( current ) && Script.IsValue( min ) && Script.IsValue(max) ? string.Format( format, current + 1, min + 1, max + 1 ) : String.Empty;
					};
					navigator.Min = 0;
					navigator.Max = items.Count - 1;
					navigator.AddElementTo( contentHolder );

					Binding currentbinding = new Binding( dataGrid, "SelectedIndex", navigator, "Current", DataBindingMode.TwoWay );

					TextControl display2 = new TextControl( null, "p" );
					display2.AddElementTo( contentHolder );

					PropertyChangedEventHandler propertyChanged = delegate( object sender, PropertyChangedEventArgs args ) {
						StringBuilder sb = new StringBuilder();

						sb.AppendLine( string.Format( "Properties changed: {0}", args.PropertyNames.Join( ", " ) ) );
						 
						foreach ( string propertyName in args.PropertyNames ) {
							if ( propertyName == "SelectedIndex" ) sb.AppendLine( string.Format( "Selected Index = {0}", dataGrid.SelectedIndex ) );
							if ( propertyName == "SelectedItem" ) sb.AppendLine( string.Format( "Selected Item = {0}", dataGrid.SelectedItems ) );
							if ( propertyName == "SelectedItems" ) {
								sb.AppendLine( string.Format( "Selected Items = {0}", 
									dataGrid.SelectedItems.Map( delegate( object item ) { return item.ToString(); } ).Join( "; " )
								) );
							}
						}

						display2.Text = sb.ToString();
					};

					dataGrid.PropertyChanged += propertyChanged;

					resultDisplay.Passed = true;
				};

				Action<Exception> onError = delegate( Exception ex ) {
					resultDisplay.Text = T + ": " + ex.StackTrace;
					resultDisplay.Passed = false;
				};

				Control.LoadControlTemplate( templateUri, onSuccess, onError );
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void RecordNavigator () {
			const string T = "RecordNavigator";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				TextControl display = new TextControl( null, "p" );
				display.AddElementTo( contentHolder );

				display.Text = @"This test succeeds iff all 4 are rendered correctly and their click events are reported faithfully.";

				TextControl display2 = new TextControl( null, "p" );
				display2.AddElementTo( contentHolder );
				display2.Bind( "Current", "Text", DataBindingMode.OneWay );

				RecordNavigator navigator = new RecordNavigator( null );
				navigator.AddElementTo(contentHolder);
				navigator.Max = 100;
				navigator.Min = 1;
				navigator.Current = navigator.Min;

				display2.DataContext = navigator;

				CodeBehind div = new CodeBehind( null );
				div.DomElement.Style.TextAlign = "center";
				div.AddElementTo( contentHolder );

				Button b2 = new Button(null);
				b2.DomElement.TextContent = "Click to set Current to 17";
				b2.Clicked += delegate( object sender, ElementEventArgs args ) { navigator.Current = 17; };
				b2.AddElementTo( div.DomElement );

				resultDisplay.Passed = true;
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void DrawButtons () {
			const string T = "DrawButtons";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				TextControl display = new TextControl( null, "p" );
				display.AddElementTo( contentHolder );

				display.Text = @"This test succeeds iff all 4 are rendered correctly and their click events are reported faithfully.";

				Element div = Document.CreateElement( "div" );
				contentHolder.AppendChild( div );

				TextControl display2 = new TextControl( null, "p" );
				display2.AddElementTo( contentHolder );

				Func<Button, Button> setUp = delegate( Button b ) {
					Style style = b.DomElement.Style;

					style.Width = "48px";
					style.Height = "48px";
					style.Padding = "3px";
					b.AddElementTo( contentHolder );

					b.Clicked += delegate( object sender, ElementEventArgs args ) {
						display2.Text = b.GetType().Name + " clicked.";
					};
					return b;
				};

				setUp( new GotoFirstRecordButton( null ) );
				setUp( new GotoPreviousRecordButton( null ) );
				setUp( new GotoNextRecordButton( null ) );
				setUp( new GotoLastRecordButton( null ) );

				resultDisplay.Passed = true;
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void CreateSelectElement () {
			const string T = "CreateSelectElement";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				TextControl display = new TextControl( null, "p" );
				display.AddElementTo( contentHolder );

				display.Text = @"This test succeeds iff a Select object always correctly states that the specified object has been selected.";

				Select select = new Select( null );
				select.AddElementTo( contentHolder );
				select.Items = ChemicalElement.Elements;
				select.SelectElement.Style.Display = "inline-block";
				select.SelectElement.Style.VerticalAlign = "top";

				Select select2 = new Select( null );
				select2.AddElementTo( contentHolder );
				select2.Items = ChemicalElement.Elements;
				select2.SelectElement.Style.Display = "inline-block";
				select2.SelectElement.Size = 5;
				select2.SelectElement.Multiple = true;
				select2.SelectElement.Style.VerticalAlign = "top";

				TextControl display2 = new TextControl( null, "p" );
				display2.AddElementTo( contentHolder );

				ElementEventHandler select_SelectionChanged = delegate( object sender, ElementEventArgs eventArgs ) {
					Select selectControl = (Select) sender;

					display2.Text = string.Format( "selected Items = {0}", Util.Join( selectControl.SelectedItems, ", " ) );
				};

				select.SelectionChanged += select_SelectionChanged;
				select2.SelectionChanged += select_SelectionChanged;

				PropertyChangedEventHandler propertyChanged = delegate( object sender, PropertyChangedEventArgs args ) {
					display2.Text = string.Format( "Property {0} changed on object {1}", args.PropertyNames.Join(","), sender );
				};

				resultDisplay.Passed = true;
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void LoadControlTemplate_DisplayFullDataGrid_VerifyDataBinding () {
			const string T = "LoadControlTemplate_DisplayFullDataGrid";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				const string templateUri = "/UnitTests/Templates/ChemicalElementTable.html";
				TextControl display = new TextControl( null, "p" );
				display.AddElementTo( contentHolder );

				display.Text = @"This test succeeds iff a DataGrid exhibits correct databinding behaviour on the first and last elements. Although the test is enforced by assertions, you should see a a table with 10 rows and some very unusual looking chemical elements in some of the rows.";

				Action<DocumentFragment> onSuccess = delegate( DocumentFragment fragment ) {
					List<Control> controls = new List<Control>();

					Util.ForEachElement( fragment.ChildNodes, delegate( Element e ) {
						if ( Control.HasCodeBehind( e ) )
							controls.Add( (Control) e );
					} );

					Assert.IsTrue( controls.Length == 1, T + ": " + "controls.Length == 1" );
					Assert.IsTrue( controls[0].CodeBehind is DataGrid, T + ": " + "controls[0].CodeBehind is DataGrid" );

					DataGrid dataGrid = (DataGrid) controls[0].CodeBehind;
					dataGrid.AddElementTo( contentHolder );

					ObservableList items = new ObservableList( null );

					foreach ( ChemicalElement element in ChemicalElement.Elements ) {
						items.Add( element.Clone() );
					}

					dataGrid.ItemsSource = items;

					TextControl display2 = new TextControl( null, "p" );
					display2.AddElementTo( contentHolder );

					PropertyChangedEventHandler propertyChanged = delegate( object sender, PropertyChangedEventArgs args ) {
						display2.Text = string.Format( "Property {0} changed on object {1}", args.PropertyNames.Join(","), sender );
					};

					foreach ( ChemicalElement element in items ) {
						element.PropertyChanged += propertyChanged;
					}

					IArrayLike<ChemicalElement> elements = items.AsArray<ChemicalElement>();

					elements[0].Name = "Unobtanium";
					elements[0].Symbol = "Uo";
					elements[0].Weight = 299792;

					elements[9].Name = "Disturbium";
					elements[9].Symbol = "Dm";
					elements[9].Weight = 5198985;

					ElementCollection rows = dataGrid.DomElement.GetElementsByTagName( "tr" );

					ChemicalElement e0 = new ChemicalElement(
									  rows[1].ChildNodes[1].TextContent,
									  rows[1].ChildNodes[2].TextContent,
						double.Parse( rows[1].ChildNodes[3].TextContent ) );

					ChemicalElement e9 = new ChemicalElement(
									  rows[10].ChildNodes[1].TextContent,
									  rows[10].ChildNodes[2].TextContent,
						double.Parse( rows[10].ChildNodes[3].TextContent ) );

					Assert.IsTrue( e0.Equals( elements[0] ), T + ": " + "e0.Equals( elements[0] )" );
					Assert.IsTrue( e9.Equals( elements[9] ), T + ": " + "e9.Equals( elements[9] )" );

					resultDisplay.Passed = true;
				};

				Action<Exception> onError = delegate( Exception ex ) {
					resultDisplay.Text = T + ": " + ex.StackTrace;
					resultDisplay.Passed = false;
				};

				Control.LoadControlTemplate( templateUri, onSuccess, onError );
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void LoadControlTemplate_DisplayFullDataGrid () {
			const string T = "LoadControlTemplate_DisplayFullDataGrid";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				const string templateUri = "/UnitTests/Templates/ChemicalElementTable.html";
				TextControl display = new TextControl( null, "p" );
				contentHolder.AppendChild( display.DomElement );
				display.Text = @"This test succeeds iff an empty DataGrid loaded from a URL and dynamically bound to its code behind is then able to be added to the DOM and displayed. Although the test is enforced by assertions, you should see a fully populated table with 10 content rows.";

				Action<DocumentFragment> onSuccess = delegate( DocumentFragment fragment ) {
					List<Control> controls = new List<Control>();

					Util.ForEachElement( fragment.ChildNodes, delegate( Element e ) {
						if ( Control.HasCodeBehind( e ) )
							controls.Add( (Control) e );
					} );

					Assert.IsTrue( controls.Length == 1, T + ": " + "controls.Length == 1" );
					Assert.IsTrue( controls[0].CodeBehind is DataGrid, T + ": " + "controls[0].CodeBehind is DataGrid" );

					DataGrid dataGrid = (DataGrid) controls[0].CodeBehind;

					contentHolder.AppendChild( controls[0] );

					Assert.IsTrue( contentHolder.GetElementsByTagName( "tr" ).Length == 1, T + ": " + "contentHolder.GetElementsByTagName(\"tr\").Length == 1" );
					Assert.IsTrue( contentHolder.GetElementsByTagName( "td" ).Length == 4, T + ": " + "contentHolder.GetElementsByTagName(\"td\").Length == 4" );

					resultDisplay.Passed = true;
				};

				Action<Exception> onError = delegate( Exception ex ) {
					resultDisplay.Text = T + ": " + ex.StackTrace;
					resultDisplay.Passed = false;
				};

				Control.LoadControlTemplate( templateUri, onSuccess, onError );
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void LoadControlTemplate_DisplayEmptyDataGrid () {
			const string T = "LoadControlTemplate_DisplayEmptyDataGrid";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				const string templateUri = "/UnitTests/Templates/ChemicalElementTable.html";
				TextControl display = new TextControl( null, "p" );
				contentHolder.AppendChild( display.DomElement );
				display.Text = @"This test succeeds iff an empty DataGrid loaded from a URL and dynamically bound to its code behind is then able to be added to the DOM, displayed and then populated with a enumerator of elements. Although the test is enforced by assertions, you should see a single row containing the column headings.";

				Action<DocumentFragment> onSuccess = delegate( DocumentFragment fragment ) {
					List<Control> controls = new List<Control>();

					Util.ForEachElement( fragment.ChildNodes, delegate( Element e ) {
						if ( Control.HasCodeBehind( e ) )
							controls.Add( (Control) e );
					} );

					Assert.IsTrue( controls.Length == 1, T + ": " + "controls.Length == 1" );
					Assert.IsTrue( controls[0].CodeBehind is DataGrid, T + ": " + "controls[0].CodeBehind is DataGrid" );

					DataGrid dataGrid = (DataGrid) controls[0].CodeBehind;

					contentHolder.AppendChild( controls[0] );

					Assert.IsTrue( contentHolder.GetElementsByTagName( "tr" ).Length == 1, T + ": " + "contentHolder.GetElementsByTagName(\"tr\").Length == 1" );
					Assert.IsTrue( contentHolder.GetElementsByTagName( "td" ).Length == 4, T + ": " + "contentHolder.GetElementsByTagName(\"td\").Length == 4" );

					dataGrid.ItemsSource = new ObservableList( ChemicalElement.Elements );

					Assert.IsTrue( contentHolder.GetElementsByTagName( "tr" ).Length == 1 + ChemicalElement.Elements.Length, T + ": " + "contentHolder.GetElementsByTagName(\"tr\").Length == 1 + ChemicalElement.Elements.Length" );

					resultDisplay.Passed = true;
				};

				Action<Exception> onError = delegate( Exception ex ) {
					resultDisplay.Text = T + ": " + ex.StackTrace;
					resultDisplay.Passed = false;
				};

				Control.LoadControlTemplate( templateUri, onSuccess, onError );
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void LoadControlTemplate_BindDataGrid () {
			const string T = "LoadControlTemplate_BindDataGrid";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				const string templateUri = "/UnitTests/Templates/ChemicalElementTable.html";
				TextControl display = new TextControl( null, "div" );
				contentHolder.AppendChild( display.DomElement );

				Action<DocumentFragment> onSuccess = delegate( DocumentFragment fragment ) {
					List<Control> controls = new List<Control>();

					Util.ForEachElement( fragment.ChildNodes, delegate( Element e ) {
						if ( Control.HasCodeBehind( e ) )
							controls.Add( (Control) e );
					} );

					Assert.IsTrue( controls.Length == 1, T + ": " + "controls.Length == 1" );
					Assert.IsTrue( controls[0].CodeBehind is DataGrid, T + ": " + "controls[0].CodeBehind is DataGrid" );

					DataGrid dataGrid = (DataGrid) controls[0].CodeBehind;
					Assert.IsTrue( dataGrid.Columns.Length == 4, T + ": " + "dataGrid.Columns.Length == 4" );
					Assert.IsTrue( dataGrid.Columns[0] is DataGridCheckBoxColumn, T + ": " + "dataGrid.Columns[0] is DataGridCheckBoxColumn" );
					Assert.IsTrue( dataGrid.Columns[1] is DataGridTextColumn, T + ": " + "dataGrid.Columns[1] is DataGridTextColumn" );
					Assert.IsTrue( dataGrid.Columns[2] is DataGridTextColumn, T + ": " + "dataGrid.Columns[2] is DataGridTextColumn" );
					Assert.IsTrue( dataGrid.Columns[3] is DataGridTextColumn, T + ": " + "dataGrid.Columns[3] is DataGridTextColumn" );

					resultDisplay.Passed = true;
				};

				Action<Exception> onError = delegate( Exception ex ) {
					resultDisplay.Text = T + ": " + ex.StackTrace;
					resultDisplay.Passed = false;
				};

				Control.LoadControlTemplate( templateUri, onSuccess, onError );
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		/// <summary> Loads a control template from a URL and performs superficial verification of 
		/// </summary>

		void LoadControlTemplate_VerifyChildElements () {
			const string T = "LoadControlTemplate_VerifyChildElements";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				const string templateUri = "/UnitTests/Templates/ChemicalElementTable.html";
				TextControl display = new TextControl( null, "div" );
				contentHolder.AppendChild( display.DomElement );

				Action<DocumentFragment> onSuccess = delegate( DocumentFragment fragment ) {
					List<Element> nonEmptyChildren = new List<Element>();

					for ( Element child = fragment.FirstChild; child != null; child = child.NextSibling ) {
						if ( child.NodeType != ElementType.Text )
							nonEmptyChildren.Add( child );
					}

					resultDisplay.Passed = string.Compare( nonEmptyChildren[0].TagName, "div", true ) == 0 &&
						string.Compare( nonEmptyChildren[1].TagName, "table", true ) == 0;
				};

				Action<Exception> onError = delegate( Exception ex ) {
					resultDisplay.Text = T + ": " + ex.StackTrace;
					resultDisplay.Passed = false;
				};

				Control.LoadControlTemplate( templateUri, onSuccess, onError );
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void Select_13_GettingStartedExample01e () {
			const string T = "Select_13_GettingStartedExample01e";
			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				SimpleStatusDisplay status = new SimpleStatusDisplay();
				ChemicalElementTable table = new ChemicalElementTable();


				table.Data = ChemicalElement.Elements;

				table.Rows.ForEach( delegate( ChemicalElementRow r ) {
					r.Clicked += delegate( ChemicalElementRow r_ ) {
						status.Text = r_.Data.Name + " clicked. Item is" + ( r_.Data.IsSelected ? "" : " not" ) + " selected.";
					};
				} );

				contentHolder.AppendChild( status.DomElement );
				contentHolder.AppendChild( table.DomElement );

				HtmlUtil.CreateElement( "p", contentHolder, null ).TextContent = "This test passes iff all rows are selected.";
				HtmlUtil.CreateElement( "p", contentHolder, new Dictionary<string, string>( "style", "margin-left: 12mm;" ) );

				Script.SetTimeout( delegate() {
					table.Data.ForEach( delegate( ChemicalElement chemical ) {
						chemical.IsSelected = true;
					} );

					int expected = ChemicalElement.Elements.Length;
					int actual = 0;

					table.Rows.ForEach( delegate( ChemicalElementRow row ) {
						if ( ( (CheckBoxElement) row.DomElement.ChildNodes[0].ChildNodes[0] ).Checked )
							actual++;
					} );

					resultDisplay.Passed = expected == actual;
				}, 1500 );
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		void Select_14_GettingStartedExample01f () {
			const string T = "Select_14_GettingStartedExample01f";

			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				SimpleStatusDisplay status = new SimpleStatusDisplay();
				ChemicalElementTable table = new ChemicalElementTable();


				ChemicalElement[] d1 = {
					ChemicalElement.Elements[0],
					ChemicalElement.Elements[2],
					ChemicalElement.Elements[4],
					ChemicalElement.Elements[6],
				};

				ChemicalElement[] d2 = {
					ChemicalElement.Elements[0],
					ChemicalElement.Elements[1],
					ChemicalElement.Elements[2],
					ChemicalElement.Elements[3],
					ChemicalElement.Elements[6],
					ChemicalElement.Elements[7],
				};

				table.Data = d1;

				table.Rows.ForEach( delegate( ChemicalElementRow r ) {
					r.Clicked += delegate( ChemicalElementRow r_ ) {
						status.Text = r_.Data.Name + " clicked. Item is" + ( r_.Data.IsSelected ? "" : " not" ) + " selected.";
					};
				} );

				contentHolder.AppendChild( status.DomElement );
				contentHolder.AppendChild( table.DomElement );

				HtmlUtil.CreateElement( "p", contentHolder, null ).TextContent = "This test passes iff (H, Li, B, N) is replaced by (H, He, Li, Be, N, O).";
				HtmlUtil.CreateElement( "p", contentHolder, new Dictionary<string, string>( "style", "margin-left: 12mm;" ) );

				string expected1 = "H, Li, B, N";
				string actual1 = table.GetAllSymbols();

				Assert.StringsEqual( expected1, actual1, "Initial table setup." );

				Script.SetTimeout( delegate() {
					table.Data = d2;

					string expected2 = "H, He, Li, Be, N, O";
					string actual2 = table.GetAllSymbols();

					status.Text = string.Format( "expected = ({0}), actual = ({1})", expected2, actual2 );

					resultDisplay.Passed = expected2 == actual2;
				}, 1500 );
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}


	}
}
