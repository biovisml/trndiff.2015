﻿using System;
using System.Collections.Generic;

using SystemQut.Xml;
using SystemQut.IO;
using System.Html;
using System.Net;

namespace Test.Client {
	public class TestXmlReader {
		internal static void RunAllTests () {
			Action [] tests = {
				TestEmptyXmlDocument,
				TestDocumentWithOnlyXmlDeclDquote,
				TestBlastResultDocumentTopLevelNode,
				TestBlastOutputParameters,
				TestHitCount,
			};

			for ( int i = 0; i < tests.Length; i++ ) {
				Action test = tests[i];
				
				try { 
					test();
				}
				catch ( Exception ex ) {
					Window.Alert( "Unhandled eror:\n" + ex.Message + "\n" + ex.StackTrace );
				}
			}
		}

		public static void TestEmptyXmlDocument () {
			string document = "";
			XmlReader reader = XmlReader.Create( new StringReader ( document ) );
			bool result = reader.Read();
			Util.AppendResult( "TestEmptyXmlDocument", ! result ); 
		}

		public static void TestDocumentWithOnlyXmlDeclDquote () {
			string document = "<?xml version=\"1.0\"?>";
			XmlReader reader = XmlReader.Create( new StringReader ( document ) );
			bool readOk = reader.Read();

			Util.AppendResult( "TestDocumentWithOnlyXmlDeclDquote (a)", readOk ); 

			XmlNodeType expected = XmlNodeType.XmlDeclaration;
			XmlNodeType actual = reader.NodeType;
			Util.AppendResult( "TestDocumentWithOnlyXmlDeclDquote (b)", expected == actual ); 
		}

		public static void TestDocumentWithOnlyXmlDeclSquote () {
			string document = "<?xml version='1.0'?>";
			XmlReader reader = XmlReader.Create( new StringReader ( document ) );
			bool readOk = reader.Read();
			
			Util.AppendResult( "TestDocumentWithOnlyXmlDeclSquote (a)", readOk ); 

			XmlNodeType expected = XmlNodeType.XmlDeclaration;
			XmlNodeType actual = reader.NodeType;
			Util.AppendResult( "TestDocumentWithOnlyXmlDeclSquote (b)", expected == actual );
		}

		public static void TestBlastResultDocumentTopLevelNode() {
			const string T = "TestBlastResultDocumentTopLevelNode";
			XmlHttpRequest req = new XmlHttpRequest();
			req.Open(HttpVerb.Get, "/UnitTests/Resources/ZKDS0C9D01R-Alignment.xml");
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						TestBlastResultDocumentTopLevelNode_aux( req.ResponseText );
					}
					else {
						Util.AppendResult( T + ": request status != 200", false );
					}
				}
			};
			req.Send();
		}

		private static void TestBlastResultDocumentTopLevelNode_aux ( string document ) {
			XmlReader reader = XmlReader.Create( new StringReader ( document ) );
			reader.Read();
			XmlNodeType expectedNodeType = XmlNodeType.XmlDeclaration;
			XmlNodeType actualNodeType = reader.NodeType;
			Util.AppendResult( "TestBlastResultDocumentTopLevelNode - Xml Declaration OK", expectedNodeType == actualNodeType );
			
			reader.Read();
			reader.Read();
			expectedNodeType = XmlNodeType.DocumentType;
			actualNodeType = reader.NodeType;
			Util.AppendResult( "TestBlastResultDocumentTopLevelNode - DOCTYPE recognised OK", expectedNodeType == actualNodeType );

			string expectedName = "DOCTYPE";
			string actualName = reader.Name;
			Util.AppendResult( "TestBlastResultDocumentTopLevelNode - DOCTYPE name OK", expectedName == actualName );

			string expectedValue = " BlastOutput PUBLIC \"-//NCBI//NCBI BlastOutput/EN\" \"http://www.ncbi.nlm.nih.gov/dtd/NCBI_BlastOutput.dtd\"";
			string actualValue = reader.Value;
			Util.AppendResult( "TestBlastResultDocumentTopLevelNode - DOCTYPE name OK", expectedValue == actualValue );
			
			reader.Read();
			reader.Read();
			expectedNodeType = XmlNodeType.Element;
			actualNodeType = reader.NodeType;
			Util.AppendResult( "TestBlastResultDocumentTopLevelNode - Top element type OK", expectedNodeType == actualNodeType );

			expectedName = "BlastOutput";
			actualName = reader.Name;
			Util.AppendResult( "TestBlastResultDocumentTopLevelNode - Element name OK", expectedName == actualName );
		}

		public static void TestBlastOutputParameters() {
			const string T = "TestBlastOutputParameters";
			XmlHttpRequest req = new XmlHttpRequest();
			req.Open( HttpVerb.Get, "/UnitTests/Resources/ZKDS0C9D01R-Alignment.xml" );
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						TestBlastOutputParameters_aux( req.ResponseText );
					}
					else {
						Util.AppendResult( T + ": request status == 200", false );
					}
				}
			};
			req.Send();
		}

		private static void TestBlastOutputParameters_aux ( string document ) {
			XmlReader reader = XmlReader.Create( new StringReader( document ) );

			Dictionary<string,string> expected = new Dictionary<string,string>();
			expected["BlastOutput_program"] = "blastp";
			expected["BlastOutput_version"] = "BLASTP 2.2.28+";
			expected["BlastOutput_reference"] = "Stephen F. Altschul, Thomas L. Madden, Alejandro A. Sch&amp;auml;ffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J. Lipman (1997), &quot;Gapped BLAST and PSI-BLAST: a new generation of protein database search programs&quot;, Nucleic Acids Res. 25:3389-3402.";
			expected["BlastOutput_db"] = "nr";
			expected["BlastOutput_query-ID"] = "gi|16130963|ref|NP_417539.1|";
			expected["BlastOutput_query-def"] = "RNA polymerase, sigma 70 (sigma D) factor [Escherichia coli str. K-12 substr. MG1655]";
			expected["BlastOutput_query-len"] = "613";
			expected["Parameters_matrix"] = "BLOSUM62";
			expected["Parameters_expect"] = "10";
			expected["Parameters_gap-open"] = "11";
			expected["Parameters_gap-extend"] = "1";
			expected["Parameters_filter"] = "F";
  
			string currentElement = null;

			for(;;) {
				reader.Read();

				XmlNodeType nodeType = reader.NodeType;
				string name = reader.Name;

				if ( nodeType == XmlNodeType.EndElement ) {
					if ( name == "BlastOutput_param" ) break;
				
					currentElement = null;
				}

				if ( nodeType == XmlNodeType.Element && expected.ContainsKey(name) ) currentElement = name;

				if ( nodeType == XmlNodeType.Text && currentElement != null ) {
					string expectedValue = expected[currentElement];
					string actualValue = reader.Value;

					if ( expectedValue == actualValue ) {
						Util.AppendResult( string.Format( "TestBlastOutputParameters - {0} value", currentElement ), true );
					}
					else {
						Util.AppendError( string.Format( "TestBlastOutputParameters - {0} value", currentElement ), expectedValue, actualValue );
					}
				}
			}

		}

		public static void TestHitCount() {
			const string T = "TestHitCount";
			XmlHttpRequest req = new XmlHttpRequest();
			req.Open( HttpVerb.Get, "/UnitTests/Resources/ZKDS0C9D01R-Alignment.xml" );
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						TestHitCount_aux( req.ResponseText );
					}
					else {
						Util.AppendResult( T + ": request status == 200", false );
					}
				}
			};
			req.Send();
		}

		private static void TestHitCount_aux ( string document ) {
			XmlReader reader = XmlReader.Create( new StringReader( document ) );

			int expectedCount = 10000;
			int actualCount = 0;

			Date start = Date.Now;

			while( reader.Read() ) {
				XmlNodeType nodeType = reader.NodeType;
				string name = reader.Name;

				if ( nodeType == XmlNodeType.Element && name == "Hit" ) actualCount ++;
			}

			Date end = Date.Now;

			Util.AppendResult( "TestHitCount - expecting 10000 hits", expectedCount == actualCount );
			Util.AppendText( String.Format( "Elapsed time = {0}", ( end.GetTime() - start.GetTime() ) / 1000 ) );
		}
	}
}
