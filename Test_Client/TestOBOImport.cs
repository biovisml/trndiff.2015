﻿using System;
using System.Collections.Generic;
using SystemQut.IO;

namespace Test.Client
{
    internal class TestOBOImport
    {
        //private static Dictionary<string, List<KeyValuePair<string,string>>> terms;
        private static Dictionary<string, GeneOntologyTerm> terms;

        static void TestImportOBOFile() {
            try {
                Util.ReadAllText( "/UnitTests/Resources/" + "go-basic.obo.txt", delegate( string text ) {
                    StringReader reader = new StringReader( text );

                    //We will create a dictionary of lists - the key is the ID,
                    //and the sub lists are all the attributes of the current
                    //term
                    //terms = new Dictionary<string,List<KeyValuePair<string,string>>>();
                    terms = new Dictionary<string, GeneOntologyTerm>();

                    //So we can see what all the attributes are
                    //Dictionary<string, bool> attributeNames = new Dictionary<string, bool>();

                    string currentLine = reader.ReadLine();

                    do {
                        
                        //If we find the beginning of a term, get all the
                        //attributes
                        if (currentLine == "[Term]") {
                            currentLine = reader.ReadLine();
                            //First attribute is always the ID, which we use
                            //as an identifier
                            string id = currentLine.Split(": ")[1];
                            //List<KeyValuePair<string,string>> attributes = new  List<KeyValuePair<string,string>>();
                            GeneOntologyTerm currentTerm = new GeneOntologyTerm(id);
                            currentLine = reader.ReadLine();
                            
                            //The following lines will be all the attributes of
                            //the term, until we reach a blank line
                            do {
                                string attributeName = currentLine.Split(": ")[0];

                                //Remove any comments
                                string attributeValue = currentLine.Split(": ")[1].Split(" !")[0];
                                /*KeyValuePair<string, string> attribute = new KeyValuePair<string,string>();
                                attribute.Key = attributeName;
                                attribute.Value = attributeValue;
                                attributes.Add(attribute);*/
                                switch (attributeName) {
                                    case "name":
                                        currentTerm.Name = attributeValue;
                                        break;
                                    case "namespace":
                                        currentTerm.GONamespace = attributeValue;
                                        break;
                                    case "def":
                                        currentTerm.Definition = attributeValue;
                                        break;
                                    case "synonym":
                                        currentTerm.Synonyms.Add(attributeValue);
                                        break;
                                    case "is_a":
                                        currentTerm.Parents.Add(attributeValue);
                                        break;
                                    case "alt_id":
                                        currentTerm.AlternativeIds.Add(attributeValue);
                                        break;
                                    case "subset":
                                        currentTerm.Subsets.Add(attributeValue);
                                        break;
                                    case "xref":
                                        currentTerm.Xrefs.Add(attributeValue);
                                        break;
                                    case "comment":
                                        currentTerm.Comment = attributeValue;
                                        break;
                                    case "is_obsolete":
                                        currentTerm.IsObsolete = bool.Parse(attributeValue);
                                        break;
                                    case "consider":
                                        currentTerm.IdsToConsider.Add(attributeValue);
                                        break;
                                    case "relationships":
                                        currentTerm.Relationships.Add(attributeValue);
                                        break;
                                    case "replaced_by":
                                        currentTerm.ReplacementId = attributeValue;
                                        break;
                                }
                                currentLine = reader.ReadLine();
                            } while (!string.IsNullOrEmpty(currentLine));
                            
                            //So we can see what all the attributes are.
                            //This checks whether there can be more than
                            //one of this attribute per term
                            /*string lastAttribute = string.Empty;
                            for (int i = 0; i < attributes.Length; i++) {
                                KeyValuePair<string, string> attribute = attributes[i];
                                if (!attributeNames.ContainsKey(attribute.Key)) {
                                    attributeNames[attribute.Key] = false;
                                }
                                if (lastAttribute == attribute.Key) {
                                    attributeNames[lastAttribute] = true;
                                }
                                lastAttribute = attribute.Key;
                            }*/

                            //terms[id] = attributes;
                            terms[id] = currentTerm;
                            //Console.Log("Parsed term " + id + " with " + attributes.Count + " attributes");
                        }
                        currentLine = reader.ReadLine();
                    } while (currentLine != null);

                    Console.Log("Parsed OBO flat database without errors! " + terms.Count + " gene ontology terms in go-basic.obo.txt");

                    //So we can see what all the attributes are
                    /*Console.Log("List of all attributes:");
                    foreach (KeyValuePair<string, bool> pair in attributeNames) {
                        Console.Log(pair.Key + " (can have multiples? " + pair.Value + ")");
                    }*/

                    TestTreeTraverse();
                });
            } catch (Exception ex) {
                Console.Log("Importing an OBO flat database failed for the following reason: " + ex.Message);
            }
        }
        
        //Tests if we can use the list to traverse to the topmost parents
        static void TestTreeTraverse() {
            //List<KeyValuePair<string,string>> term = terms["GO:0000001"];
            GeneOntologyTerm term = terms["GO:0000001"];
            Console.Log("Finding all parents for GO:0000001");
            //FindAllParents("GO:0000001", term);
            FindAllParents(term);
            term = terms["GO:0000006"];
            Console.Log("Finding all parents for GO:0000006");
            //FindAllParents("GO:0000006", term);
            FindAllParents(term);
            term = terms["GO:0000015"];
            Console.Log("Finding all parents for GO:0000015");
            //FindAllParents("GO:0000015", term);
            FindAllParents(term);
        }

        //private static void FindAllParents(string id, List<KeyValuePair<string, string>> term)
        private static void FindAllParents(GeneOntologyTerm term)
        {
            /*bool hasParent = false;
            string name = string.Empty;
            for (int i = 0; i < term.Length; i++) {
                KeyValuePair<string, string> attribute = term[i];
                if (attribute.Key == "name") {
                    name = attribute.Value;
                }
            }
            for (int i = 0; i < term.Length; i++) {
                KeyValuePair<string, string> attribute = term[i];
                if (attribute.Key == "is_a") {
                    hasParent = true;
                    Console.Log(attribute.Value + " is a parent of " + id + " (" + name + ")");
                    FindAllParents(attribute.Value, terms[attribute.Value]);
                }
            }

            if (!hasParent) {
                Console.Log(id + " (" + name + ") has no parents!");
            }*/
            for (int i = 0; i < term.Parents.Length; i++) {
                Console.Log(term.Parents[i] + " is a parent of " + term.Id + " (" + term.Name + ")");
                FindAllParents(terms[term.Parents[i]]);
            }

            if (term.Parents.Length < 1) {
                Console.Log(term.Id + " (" + term.Name + ") has no parents!");
            }
        }

        static public void RunAllTests() {
			Action [] tests = {
				TestImportOBOFile,
                //TestTreeTraverse,
			};

			for ( int i = 0; i < tests.Length; i++ ) {
				Action test = tests[i];
				test();
			}
		}
    }

    internal class GeneOntologyTerm {
        private string id;
        private string name;
        private string go_namespace;
        private string definition;
        private List<string> synonyms;
        private List<string> parents;
        private List<string> alternativeIds;
        private List<string> subsets;
        private List<string> xrefs;
        private string comment;
        private bool isObsolete;
        private List<string> idsToConsider;
        private List<string> relationships;
        private string replacementId;

        public GeneOntologyTerm(string id) {
            this.id = id;
            synonyms = new List<string>();
            parents = new List<string>();
            alternativeIds = new List<string>();
            subsets = new List<string>();
            xrefs = new List<string>();
            idsToConsider = new List<string>();
            relationships = new List<string>();
        }

        public string Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string GONamespace
        {
            get { return go_namespace; }
            set { go_namespace = value; }
        }

        public string Definition
        {
            get { return definition; }
            set { definition = value; }
        }

        public List<string> Synonyms
        {
            get { return synonyms; }
            set { synonyms = value; }
        }

        public List<string> Parents
        {
            get { return parents; }
            set { parents = value; }
        }

        public List<string> AlternativeIds
        {
            get { return alternativeIds; }
            set { alternativeIds = value; }
        }

        public List<string> Subsets
        {
            get { return subsets; }
            set { subsets = value; }
        }

        public List<string> Xrefs
        {
            get { return xrefs; }
            set { xrefs = value; }
        }

        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        public bool IsObsolete
        {
            get { return isObsolete; }
            set { isObsolete = value; }
        }

        public List<string> IdsToConsider
        {
            get { return idsToConsider; }
            set { idsToConsider = value; }
        }

        public List<string> Relationships
        {
            get { return relationships; }
            set { relationships = value; }
        }

        public string ReplacementId
        {
            get { return replacementId; }
            set { replacementId = value; }
        }
    }
}
