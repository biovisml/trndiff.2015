﻿using System;
using System.Collections.Generic;

using System.Html;
using System.Net;
using SystemQut;
using Test.Client.Controls;
using System.Collections;

namespace Test.Client {
	static public class Util {

		/// <summary> Creates a concatenated string representation of the contents of a list.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="separator"></param>
		/// <returns></returns>

		public static string Join<T> ( IEnumerable<T> list, string separator ) {
			StringBuilder b = new StringBuilder();

			foreach ( object item in list ) {
				if ( ! b.IsEmpty ) b.Append( separator );
				b.Append( item );
			}
			
			return b.ToString();
		}

		/// <summary> Returns the contents of a file as a string.
		/// </summary>
		/// <param name="fileName">The path (relative or absolute) of the file.</param>
		/// <param name="process"> A delegate that will be invoked with the content of the file in the event of a successful call, or with null otherwise. </param>
		/// <returns>Returns the content of the file as a string.</returns>

		public static void ReadAllText ( string fileName, Action<string> process ) {
			XmlHttpRequest req = new XmlHttpRequest();
			req.Open( HttpVerb.Get, fileName );
			req.OnReadyStateChange = delegate() {
				if ( req.ReadyState == ReadyState.Loaded ) {
					if ( req.Status == 200 ) {
						process( req.ResponseText );
					}
					else {
						Util.AppendResult( string.Format( "ReadAllText( \"{0}\" )", fileName ), false );
					}
				}
			};
			req.Send();
		}

		/// <summary> Adds a success-fail indicator to the test log.
		/// </summary>
		/// <param name="methodName"></param>
		/// <param name="condition"></param>

		public static ResultDisplay AppendResult ( string methodName, bool condition ) {
			ResultDisplay res = new ResultDisplay( methodName, condition );
			res.AddElementTo( Document.Body );
			return res;
		}

		/// <summary> Adds a div containing the supplied text to the test log.
		/// </summary>
		/// <param name="text"></param>

		public static void AppendText ( object text ) {
			Element div = Document.CreateElement( "div" );
			div.TextContent = text.ToString();
			Document.Body.AppendChild( div );
		}

		/// <summary> Appends a failure message to the test log.
		/// </summary>
		/// <param name="location"></param>
		/// <param name="expectedValue"></param>
		/// <param name="actualValue"></param>

		internal static void AppendError ( string location, object expectedValue, object actualValue ) {
			Element div = Document.CreateElement( "div" );
			div.TextContent = location + " failed: expected = '" + expectedValue + "' but actual = '" + actualValue + "'";
			Document.Body.AppendChild( div );
		}

		/// <summary> Iterates over the supplied collection and invokes the process callback once for each element.
		/// </summary>
		/// <param name="elementCollection"> 
		///		A list of elements to be processed. 
		///	</param>
		/// <param name="process"> 
		///		A delegate that will be invoked with each element of the collection as its argument. 
		/// </param>

		internal static void ForEachElement ( ElementCollection elementCollection, Action<Element> process ) {
			for ( int i = 0; i < elementCollection.Length; i++ ) {
				process( elementCollection[i] );
			}
		}
	}
}
