﻿using System;
using System.Collections.Generic;

using System.Reflection;

using SystemQut;
using SystemQUT.Csv;
using SystemQut.IO;
using System.Html;
using Test.Client.Controls;
using System.Html.Data.Files;

namespace Test.Client {
	public class TestValidCsvFiles {
		public Action[] AllTests {
			get {
				Action[] allTests = {
					_01_Valid_CSV_File_One_Line_Basic, 
					_02_Valid_CSV_File_One_Line_SpacesAndEmbeddedQuotes_NoTrailingLineFeed,
					_03_Valid_CSV_File_Two_Lines_SpacesAndEmbeddedQuotes_TrailingLineFeed,
					_04_Valid_CSV_File_One_Line_EmptyFieldAndFieldWithEmbeddedLineFeeds,
					_05_Valid_CSV_File_One_Line_FieldWithOneLineFeed_FieldWithQuotedString,
					_99_Valid_CSV_File_ComprehensiveTest,
					_07_Plain_Text_With_Unicode_via_ReadLine,
					_08_Round_Trip_CSV_For_Equality,
					_09_Save_Csv_File,
				};
				return allTests;
			}
		}

		/// <summary> Enforces the constraint that two ragged array's of string are identical in content.
		/// </summary>
		/// <param name="expected">
		///		The expected value.
		/// </param>
		/// <param name="actual">
		///		The actual value.
		/// </param>
		/// <param name="testName">
		///		The name of the test from which this method was called.
		/// </param>

		public static void AssertEqual ( string[][] expected, string[][] actual, string testName ) {
			Assert.NumbersEqual( expected.Length, actual.Length, testName + ": Table length wrong" );

			for ( int i = 0; i < expected.Length; i++ ) {
				Assert.NumbersEqual( expected[i].Length, actual[i].Length, testName + ": Field length wrong" );

				for ( int j = 0; j < expected[i].Length; j++ ) {
					Assert.StringsEqual( expected[i][j], actual[i][j], testName + ": field wrong" );
				}
			}
		}

		public void _01_Valid_CSV_File_One_Line_Basic () {
			const string T = "_01_Valid_CSV_File_One_Line_Basic";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				Util.ReadAllText( "/UnitTests/Resources/Csv/" + "Test01.csv",
					delegate( string text ) {
						StringReader reader = new StringReader( text );
						CsvIO csvIO = new CsvIO( ',', '"' );
						string[][] records = csvIO.Read( reader );

						string[][] expected = {
							new string [] { "Column1", "Column2", "Column3" }
						};

						AssertEqual( expected, records, T );
						resultDisplay.Passed = true;
					}
				);
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		public void _02_Valid_CSV_File_One_Line_SpacesAndEmbeddedQuotes_NoTrailingLineFeed () {
			const string T = "_02_Valid_CSV_File_One_Line_SpacesAndEmbeddedQuotes_NoTrailingLineFeed";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				Util.ReadAllText( "/UnitTests/Resources/Csv/" + "Test02.csv",
					delegate( string text ) {
						StringReader reader = new StringReader( text );
						CsvIO csvIO = new CsvIO( ',', '"' );
						string[][] records = csvIO.Read( reader );

						string[][] expected = {
							new string [] { "1.2300755E-14", "text with spaces", "text which contains an embedded \" character" }
						};

						AssertEqual( expected, records, T );
						resultDisplay.Passed = true;
					}
				);
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		public void _03_Valid_CSV_File_Two_Lines_SpacesAndEmbeddedQuotes_TrailingLineFeed () {
			const string T = "_03_Valid_CSV_File_Two_Lines_SpacesAndEmbeddedQuotes_TrailingLineFeed";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				Util.ReadAllText( "/UnitTests/Resources/Csv/" + "Test03.csv",
					delegate( string text ) {
						StringReader reader = new StringReader( text );
						CsvIO csvIO = new CsvIO( ',', '"' );
						string[][] records = csvIO.Read( reader );

						string[][] expected = {
							new string [] { "Column1", "Column2", "Column3" },
							new string [] { "1.2300755E-14", "text with spaces", "text which contains an embedded \" character" }
						};

						AssertEqual( expected, records, T );
						resultDisplay.Passed = true;
					}
				);
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		public void _04_Valid_CSV_File_One_Line_EmptyFieldAndFieldWithEmbeddedLineFeeds () {
			const string T = "_04_Valid_CSV_File_One_Line_EmptyFieldAndFieldWithEmbeddedLineFeeds";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				Util.ReadAllText( "/UnitTests/Resources/Csv/" + "Test04.csv",
					delegate( string text ) {
						StringReader reader = new StringReader( text );
						CsvIO csvIO = new CsvIO( ',', '"' );
						string[][] records = csvIO.Read( reader );

						string[][] expected = {
							new string [] { "The next cell is empty.", "", "This cell spans multiple lines,\nor it will if I can figure out how to get the\nlines to break. The key is to use the \nAlt-Enter key combination." }
						};

						AssertEqual( expected, records, T );
						resultDisplay.Passed = true;
					}
				);
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		public void _05_Valid_CSV_File_One_Line_FieldWithOneLineFeed_FieldWithQuotedString () {
			const string T = "_05_Valid_CSV_File_One_Line_FieldWithOneLineFeed_FieldWithQuotedString";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				Util.ReadAllText( "/UnitTests/Resources/Csv/" + "Test05.csv",
					delegate( string text ) {
						StringReader reader = new StringReader( text );
						CsvIO csvIO = new CsvIO( ',', '"' );
						string[][] records = csvIO.Read( reader );

						string[][] expected = {
							new string [] { "The next two cells contain a single new line character and a short phrase surrounded by double quotes.", "\n", "\"The rain in Spain falls mainly in the plain.\"" }
						};

						AssertEqual( expected, records, T );
						resultDisplay.Passed = true;
					}
				);
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		public void _99_Valid_CSV_File_ComprehensiveTest () {
			const string T = "_99_Valid_CSV_File_ComprehensiveTest";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				Util.ReadAllText( "/UnitTests/Resources/Csv/" + "Test99.csv",
					delegate( string text ) {
						StringReader reader = new StringReader( text );
						CsvIO csvIO = new CsvIO( ',', '"' );
						string[][] records = csvIO.Read( reader );

						string[][] expected = {
new string [] { "Column1","Column2","Column3" },
new string [] { "1.2300755E-14","text with spaces","text which contains an embedded \" character" },
new string [] { "2147483647","text with an embedded comma, or perhaps two, but not three.","text with trailing spaces... 5 to be exact:     " },
new string [] { "The next cell is empty.","","This cell spans multiple lines,\nor it will if I can figure out how to get the\nlines to break. The key is to use the \nAlt-Enter key combination." },
new string [] { "The next two cells contain a single new line character and a short phrase surrounded by double quotes.","\n","\"The rain in Spain falls mainly in the plain.\"" },
new string [] { "A cell containing a single comma followed by a cell containing three commas",",",",,," },
new string [] { "A cell containing a number with embedded commas followed by a cell containing a number with mulitple embedded commas","1,250","12,345,678.90" },
new string [] { "A cell containing a single quote then one containing two quotes","\"","\"\"" },
new string [] { "A cell containing leading blanks, then a cell containing leading tabs. ","     Text with leading spaces.","     Text with leading tabs." },
new string [] { "A cell containing trailing blanks, then a cell containing trailing tabs.","Text with trailing spaces.     ","Text with trailing tabs.     " },
					};

						AssertEqual( expected, records, T );
						resultDisplay.Passed = true;
					}
				);
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		public void _07_Plain_Text_With_Unicode_via_ReadLine () {
			const string T = "_07_Plain_Text_With_Unicode_via_ReadLine";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				Util.ReadAllText( "/UnitTests/Resources/Csv/" + "Test07.txt",
					delegate( string text ) {
						StringReader reader = new StringReader( text );
						CsvIO csvIO = new CsvIO( ',', '"' );
						string expected = "a";
						string actual = reader.ReadLine();
						Assert.StringsEqual( expected, actual, T + ": string do not match." );
						resultDisplay.Passed = true;
					}
				);
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		public void _08_Round_Trip_CSV_For_Equality () {
			const string T = "_08_Round_Trip_CSV_For_Equality";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				CsvIO csvIO = new CsvIO( ',', '"' );

				string[][] expected = {
					new string [] { "Column1","Column2","Column3" },
					new string [] { "1.2300755E-14","text with spaces","text which contains an embedded \" character" },
					new string [] { "2147483647","text with an embedded comma, or perhaps two, but not three.","text with trailing spaces … 5 to be exact:     " },
					new string [] { "The next cell is empty.","","This cell spans multiple lines,\nor it will if I can figure out how to get the\nlines to break. The key is to use the \nAlt-Enter key combination." },
					new string [] { "The next two cells contain a single new line character and a short phrase surrounded by double quotes.","\n","\"The rain in Spain falls mainly in the plain.\"" },
					new string [] { "A cell containing a single comma followed by a cell containing three commas",",",",,," },
					new string [] { "A cell containing a number with embedded commas followed by a cell containing a number with mulitple embedded commas","1,250","12,345,678.90" },
					new string [] { "A cell containing a single quote then one containing two quotes","\"","\"\"" },
					new string [] { "A cell containing leading blanks, then a cell containing leading tabs. ","     Text with leading spaces.","     Text with leading tabs." },
					new string [] { "A cell containing trailing blanks, then a cell containing trailing tabs.","Text with trailing spaces.     ","Text with trailing tabs.     " },
				};

				StringWriter writer = new StringWriter();

				csvIO.Write( expected, writer );

				string textDocument = writer.ToString();

				StringReader reader = new StringReader( textDocument );

				string[][] actual = csvIO.Read( reader );

				AssertEqual( expected, actual, "T" );
				resultDisplay.Passed = true;
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

		public void _09_Save_Csv_File () {
			const string T = "_09_Save_Csv_File";

			Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			ResultDisplay resultDisplay = new ResultDisplay( T, false );
			contentHolder.AppendChild( resultDisplay.DomElement );

			try {
				CsvIO csvIO = new CsvIO( ',', '"' );

				StringWriter writer = new StringWriter();

				string[][] expected = {
					new string [] { "Column1","Column2","Column3" },
					new string [] { "1.2300755E-14","text with spaces","text which contains an embedded \" character" },
					new string [] { "2147483647","text with an embedded comma, or perhaps two, but not three.","text with trailing spaces … 5 to be exact:     " },
					new string [] { "The next cell is empty.","","This cell spans multiple lines,\nor it will if I can figure out how to get the\nlines to break. The key is to use the \nAlt-Enter key combination." },
					new string [] { "The next two cells contain a single new line character and a short phrase surrounded by double quotes.","\n","\"The rain in Spain falls mainly in the plain.\"" },
					new string [] { "A cell containing a single comma followed by a cell containing three commas",",",",,," },
					new string [] { "A cell containing a number with embedded commas followed by a cell containing a number with mulitple embedded commas","1,250","12,345,678.90" },
					new string [] { "A cell containing a single quote then one containing two quotes","\"","\"\"" },
					new string [] { "A cell containing leading blanks, then a cell containing leading tabs. ","     Text with leading spaces.","     Text with leading tabs." },
					new string [] { "A cell containing trailing blanks, then a cell containing trailing tabs.","Text with trailing spaces.     ","Text with trailing tabs.     " },
				};

				csvIO.Write( expected, writer );

				string doc = writer.ToString();

				/* Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html */
				{
					string fileName = null;

					Blob blob = new Blob( new string[] { doc }, new Dictionary<string, string>(
						"type", "text/csv"
					) );

					// Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition. 
					Func<Blob, string> getBlobUrl = (Func<Blob, string>) Script.Literal( "(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL" );
					Action<string> revokeBlobUrl = (Action<string>) Script.Literal( "(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL" );

					string url = getBlobUrl( blob );
					string actualFileName = fileName ?? "Untitled.csv";

					if ( (bool) Script.Literal( "!! window.navigator.msSaveBlob" ) ) {
						// Internet Explorer...
						Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
					}
					else {
						// FireFox, Chrome...
						AnchorElement save = (AnchorElement) Document.CreateElement( "a" );
						save.Href = url;
						save.Target = "_blank";
						save.Download = actualFileName;
						save.Style.Display = "none";
						save.InnerHTML = "Click to save!";
						
						// This is a shim for Firefox.
						string alternateUrl = String.Format( "{0}:{1}:{2}", "text/csv", actualFileName, url );
						Script.Literal( "if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }" );
						
						contentHolder.AppendChild( save );

						// This appears to not work in Firefox.
						// MutableEvent evt = Document.CreateEvent( "Event" );
						// evt.InitEvent( "click", true, true );
						// save.DispatchEvent( evt );

						save.Click();

						// This causes grief in Firefox.
						// revokeBlobUrl( url ); 
					}
				}

				resultDisplay.Passed = true;
			}
			catch ( Exception ex ) {
				resultDisplay.Text = T + ": " + ex.StackTrace;
				resultDisplay.Passed = false;
			}
		}

	}
}
