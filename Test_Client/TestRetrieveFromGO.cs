﻿using RegPrecise;
using System;
using System.Collections.Generic;
using System.Html;
using System.Net;
using System.Serialization;
using SystemQut.ServiceModel;
using SystemQut;

namespace Test.Client
{
    class TestRetrieveFromGO
    {
        private static ILocalMySQLService service = new LocalMySQLService( "/LocalMySQL.svc", false );
        private static Dictionary<string, List<string>> Tests = new Dictionary<string,List<string>>();
        private static List<GOTerm> terms = null;
        private static List<string> topMostMolecularFunctionTerms = new List<string>( "transcription factor activity, protein binding",
"nucleic acid binding transcription factor activity",
"catalytic activity",
"signal transducer activity",
"nutrient reservoir activity",
"structural molecule activity",
"transporter activity",
"binding",
"protein tag",
"morphogen activity",
"antioxidant activity",
"metallochaperone activity",
"D-alanyl carrier activity",
"chemoattractant activity",
"translation regulator activity",
"chemorepellent activity",
"molecular transducer activity",
"molecular function regulator" );

        public static void RunTests () {
            Tests["Shewanella oneidensis MR-1"] = new List<string>( "dinB", "dinG", "xerD", "yebR", "yebG", "topB", "recX", "recA", "recN", "recG", "lexA", "sulA", "umuC", "umuD" );
            Tests["Shewanella amazonensis SB2B"] = new List<string>( "recN", "recA", "recX", "dinG", "xerD", "yebG", "yebR", "dnaE2", "imuB", "imuA", "topB", "dinB", "recG", "lexA", "sulA" );
            Tests["Escherichia coli K-12"] = new List<string>("fhuA", "fhuC", "fhuD", "fhuB", "entD", "fepA", "fepC", "fepG", "fepD", "ybdA", "fepB", "entC", "entE", "entB", "entA", "ybdB", "fur", "gpmA", "ybiX", "fiu", "oppA", "oppB", "oppC", "oppD", "oppF", "yncD", "yncE", "yddB", "yddA", "ydhT", "ydhU", "ydhX", "ydhW", "ydhV", "ydhY", "sufE", "sufS", "sufD", "sufC", "sufB", "sufA", "hemP", "ftnA", "sdiA", "cirA", "mntH", "exbD", "exbB", "yqjH", "bfr", "bfd", "feoA", "feoB", "yhgG", "yhhY", "sodA", "fecE", "fecD", "fecC", "fecB", "fecA", "fecR", "fecI", "fhuF", "yjjZ", "uof");
            Tests["Yersinia pestis"] = new List<string>("hemV", "hemU", "hemT", "hemS", "hemR", "hemP", "hugX", "hemN1", "fhuC", "fhuD", "fhuB", "cfrA", "mntH", "sfuC", "sfuB", "sfuA", "sitD", "sitC", "sitB", "sitA", "sufA", "sufB", "sufC", "sufD", "sufS", "sufE", "adhE", "yijD1", "ybtS", "ybtX", "ybtQ", "ybtP", "ybtA", "irp2", "nrpS", "nrpU", "nrpT", "ybtE", "fyuA", "ycdB", "ycdO", "yijD", "ftnA", "fhuF", "yiuC", "yiuB", "yiuA", "gpmA", "iucA1", "iucA2", "iucB", "iucC", "iucD", "iutA", "exbD", "exbB", "yjjZ", "hmsT", "feoC", "feoB", "feoA", "bfr", "fatB", "fatD", "fatC", "fecE", "sodA");

            GetTerms();

            Script.SetTimeout((Action) delegate {
                foreach (KeyValuePair<string, List<string>> entry in Tests) {
                    foreach (string gene in entry.Value) {
                        RetrieveFromGO(gene, entry.Key);
                    }
                }
            }, 5000);
        }

        private static void GetTerms()
        {
            //Script.SetTimeout((Action)delegate
            //{
                service.GO_ListTermsWithParents(delegate(ServiceResponse<GOTerm[]> response)
                {

                    if (response.Error != null)
                    {
                        Console.Log("GO term retrieval testing: Error in retriving list of GO terms from MySQL service: " + response.Error);
                        Util.AppendResult("GO term retrieval testing: Error in retriving list of GO terms from MySQL service: " + response.Error, false);
                        return;
                    }

                    try
                    {
                        terms = (List<GOTerm>)response.Content;
                        //Console.Log("GO term retrieval testing: Loaded list of GO terms from MySQL service");
                        //Util.AppendResult("GO term retrieval testing: Loaded list of GO terms from MySQL service" , true);
                    }
                    catch (Exception ex)
                    {
                        Util.AppendResult("GO term retrieval testing: Error in retriving list of GO terms from MySQL service: " + ex.StackTrace, false);
                        Console.Log("GO term retrieval testing: Error in retriving list of GO terms from MySQL service: " + ex.StackTrace);
                    }
                });
            //}, 100);
        }

        private static void RetrieveFromGO(string gene, string organism)
        {
            XmlHttpRequest req = new XmlHttpRequest();
            req.OnReadyStateChange = delegate()
            {
                if (req.ReadyState == ReadyState.Loaded)
                {
                    if (req.Status == 200)
                    {
                        Console.Log("GO term retrieval testing (" + gene + " in " + organism + "): Response received.");
                        JSObject data = (JSObject)Json.Parse(req.ResponseText);
                        Util.AppendResult("GO term retrieval testing (" + gene + " in " + organism + "): Response received.", true);
                        if (((data["response"] as JSObject)["docs"] as Dictionary<string, object>).Count > 0) {
                            foreach (KeyValuePair<string, object> doc in (JSObject)(data["response"] as JSObject)["docs"])
                            {
                                try
                                {
                                    string currentContents = doc.Key + " - " +
                                        "Searchable bioentity label: " + (doc.Value as JSObject)["bioentity_label_searchable"] + ", " +
                                        "Searchable taxon label: " + (doc.Value as JSObject)["taxon_label_searchable"] + ", " +
                                        "Annotation class: " + (doc.Value as JSObject)["annotation_class"];

                                    if (terms != null) {
                                        foreach (GOTerm term in terms) {
                                            if (term.UniqueIdentifier == (doc.Value as JSObject)["annotation_class"]) {
                                                currentContents = currentContents + ". Also found a GO term match (" + term.Name + ") in the MySQL database";
                                                if (topMostMolecularFunctionTerms.Contains(term.Name)) {
                                                    currentContents = currentContents + ", and is also a top level molecular function";
                                                } else if (term.Relationships.Length > 0) {
                                                    currentContents = currentContents + ", which has the top most parents";
                                                    List<GOTerm> topMostParents = FindTopMostCategoryTerms(new List<GOTerm>(), term);
                                                    foreach (GOTerm parentTerm in topMostParents) {
                                                        currentContents = currentContents + ", " + parentTerm.Name + " (" + parentTerm.Namespace + ")";
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    } else {
                                        currentContents = currentContents + ". No terms loaded to look for a match";
                                    }
                                    Console.Log(currentContents);
                                    Util.AppendResult(currentContents, true);
                                }
                                catch (Exception ex)
                                {
                                    Util.AppendResult("Error: " + ex.Message, false);
                                    Console.Log("Error: " + ex.Message);
                                }
                            }
                        } else {
                            Util.AppendResult("No results.", true);
                            Console.Log("No results.");
                        }
                    }
                    else
                    {
                        Console.Log("GO term retrieval testing (" + gene + " in " + organism + "): Error received: " + req.Status);
                        Util.AppendResult("GO term retrieval testing (" + gene + " in " + organism + "): Error received: " + req.Status, false);
                    }
                }
            };
            req.Open(HttpVerb.Post, "http://golr.berkeleybop.org/select?qt=standard&fl=*%2Cscore&version=2.2&wt=json&indent=on&q=bioentity_label:" + gene.Replace(" ", "*") + "&fq=taxon_label:*" + organism.Replace(" ", "*") + "*&start=0");
            req.Send();
        }

        // Finds the top most parents of this term that's not one of the actual
        // ontologies
        private static List<GOTerm> FindTopMostCategoryTerms(List<GOTerm> parentTermList, GOTerm term)
        {
            if (term.Relationships != null && term.Relationships.Length > 0) {
                foreach (JSObject relationship in term.Relationships) {
                    GOTermRelationship temp = GOTermRelationship.Parse(relationship);
                    if (temp.RelationshipDatabaseId == 1) {
                        if (terms[temp.ParentDatabaseId - 1].Name == terms[temp.ParentDatabaseId - 1].Namespace && !parentTermList.Contains(term)) {
                            parentTermList.Add(term);
                        } else {
                            parentTermList = FindTopMostCategoryTerms(parentTermList, terms[temp.ParentDatabaseId - 1]);
                        }
                    }
                }
            }
            return parentTermList;
        }
    }
}