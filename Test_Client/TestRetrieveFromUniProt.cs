﻿using RegPrecise;
using System;
using System.Collections.Generic;
using System.Html;
using System.Net;
using System.Serialization;
using SystemQut.ServiceModel;
using SystemQut;
using SystemQut.Xml;
using SystemQut.IO;

namespace Test.Client
{
    class TestRetrieveFromUniProt
    {
		// RegPrecise service to retrieve regulogs
        private static IRegPreciseService service = new RegPreciseService( "/RegPrecise.svc", false );

        /// <summary>
        /// Runs the tests
        /// </summary>
        public static void RunTests () {

            // The regulog of FUR containing E coli will be used
			int regulogId = 1112;

            // To store the regulog, regulons and genes retrieved from RegPrecise
            Regulog regulog;
            Dictionary<int, Regulon> regulons = new Dictionary<int, Regulon>();
            Gene[] genes = new Gene[0];

            // Get the regulog first
            service.GetRegulog( regulogId, delegate( ServiceResponse<Regulog> response1 ) {

                // If there was an error connecting to RegPrecise
                if (response1.Error != null)
                {
                    Console.Log("UniProt: Error in retriving regulog " + regulogId + " from RegPreciseService: " + response1.Error);
                    Util.AppendResult("UniProt: Error in retriving regulog " + regulogId + " from RegPreciseService: " + response1.Error, false);
                    return;
                }
				try {

                    // Store the retrieved regulog
                    regulog = response1.Content;

                    Console.Log("UniProt: Loaded regulog " + regulogId + " from RegPreciseService");
                    Util.AppendResult("UniProt: Loaded regulog " + regulogId + " from RegPreciseService" , true);

                    // Get the regulons in the retrieved regulog
                    service.ListRegulonsInRegulog( regulog.RegulogId, delegate( ServiceResponse<Regulon[]> response2 ) {

                        // If there was an error connecting to RegPrecise
					    if ( response2.Error != null ) {
                            Console.Log("UniProt: Error in retriving regulons for regulog " + regulogId + " from RegPreciseService: " + response2.Error);
                            Util.AppendResult("UniProt: Error in retriving regulons for regulog " + regulogId + " from RegPreciseService: " + response2.Error, false);
                            return;
					    }

                        try {

                            // Store the regulons in the dictionary, using their
                            // ID as the key
					        for (int i = 0; i < response2.Content.Length; i++) {
                                Regulon currentRegulon = response2.Content[i];
                                regulons[currentRegulon.RegulonId] = currentRegulon;
                            }

                            Console.Log("UniProt: Loaded regulons for regulog " + regulogId + " from RegPreciseService");
                            Util.AppendResult("UniProt: Loaded regulons for regulog " + regulogId + " from RegPreciseService" , true);

                        } catch (Exception ex) {
                            Console.Log("UniProt: Error in retriving regulons for regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace);
                            Util.AppendResult("UniProt: Error in retriving regulons for regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace, false);
                        }
				    } );

                    // Get the genes in the retrieved regulog
                    service.ListGenesInRegulog( regulog.RegulogId, delegate( ServiceResponse<Gene[]> response3 ) {

                        // If there was an error connecting to RegPrecise
					    if ( response3.Error != null ) {
                            Console.Log("UniProt: Error in retriving genes for regulog " + regulogId + " from RegPreciseService: " + response3.Error);
                            Util.AppendResult("UniProt: Error in retriving genes for regulog " + regulogId + " from RegPreciseService: " + response3.Error, false);
                            return;
					    }

                        try {

                            // Store the retrieved genes
					        genes = response3.Content;

                            Console.Log("UniProt: Loaded genes for regulog " + regulogId + " from RegPreciseService");
                            Util.AppendResult("UniProt: Loaded genes for regulog " + regulogId + " from RegPreciseService" , true);

                        } catch (Exception ex) {
                            Console.Log("UniProt: Error in retriving genes for regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace);
                            Util.AppendResult("UniProt: Error in retriving genes for regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace, false);
                        }
                    } );

                } catch (Exception ex) {
                    Console.Log("UniProt: Error in retriving regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace);
                    Util.AppendResult("UniProt: Error in retriving regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace, false);
                }
            });

            // Wait for five seconds so that the above is complete before
            // attempting to retrieve IDs from Entrez
            Script.SetTimeout((Action) delegate {

                // For each retrieved gene...
                for (int i = 0; i < genes.Length; i++) {
                    Gene currentGene = genes[i];

                    // Get its associated regulon using the regulon ID
                    Regulon currentRegulon = regulons[currentGene.RegulonId];

                    // Retrieve all records for the combination of organism and
                    // gene
                    RetrieveFromUniProtSearch(currentRegulon.GenomeName, currentGene.Name, true);
                }

            }, 5000);
        }

        /// <summary>
        /// Queries UniProt for all entries that contain both the specified organism and gene
        /// </summary>
        /// <param name="organism">The organism to search for</param>
        /// <param name="gene">The gene to search for</param>
        /// <param name="show">Whether to add the log to the test page</param>
        private static void RetrieveFromUniProtSearch(string organism, string gene, bool show) {

            // Create a new request
            XmlHttpRequest req = new XmlHttpRequest();
            req.OnReadyStateChange = delegate()
            {
                if (req.ReadyState == ReadyState.Loaded)
                {
                    // If an OK status is returned
                    if (req.Status == 200)
                    {
                        Console.Log("Entrez (" + gene + " of " + organism + "): Able to connect with 'https://www.uniprot.org/uniprot/?query=gene%3A" + gene + "+organism%3A\"" + organism.Replace(" ", "+") + "\"&format=tab' - Status: " + req.Status);
                        if (show) Util.AppendResult("Entrez (" + gene + " of " + organism + "): Able to connect with 'https://www.uniprot.org/uniprot/?query=gene%3A" + gene + "+organism%3A\"" + organism.Replace(" ", "+") + "\"&format=tab' - Status: " + req.Status, true);

                        if (String.IsNullOrEmpty(req.ResponseText)) {
                            Console.Log("Nothing received for this query");
                            if (show) Util.AppendResult("Nothing received for this query", false);
                        } else {
                            // Start reading the retrieved text
                            /*StringReader reader = new StringReader(req.ResponseText);

                            // While there is text to be read...
                            while (reader.Peek() != null) {

                                // Get the next line of text
                                string currentLine = reader.ReadLine();

                                // Display it in the console (and optionally on the page)
                                Console.Log(currentLine);
                                if (show) Util.AppendResult(currentLine, true);
                            }*/
                            Console.Log(req.ResponseText);
                            if (show) Util.AppendResult(req.ResponseText, true);
                        }
                    }

                    // Otherwise if there's an error
                    else
                    {
                        Console.Log("UniProt (" + gene + " of " + organism + "): Error received: " + req.Status);
                        Util.AppendResult("UniProt (" + gene + " of " + organism + "): Error received: " + req.Status, false);
                    }
                }
            };

            // Set the URL to search for
            req.Open(HttpVerb.Post, "https://www.uniprot.org/uniprot/?query=gene%3A" + gene + "+organism%3A\"" + organism.Replace(" ", "+") + "\"&format=tab");

            // Send the request
            req.Send();
        }
    }
}
