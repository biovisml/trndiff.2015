﻿using System;
using System.Collections.Generic;
using SystemQut.Controls;
using System.Runtime.CompilerServices;
using System.Html;
using SystemQut;

namespace Test.Client.Controls {

	/// <summary> The first ever user control! Encapsulates a place to display 
	///		unit test results.
	/// </summary>

	public class SimpleStatusDisplay : CodeBehind {
		private Element text;
		private Element display;



		/// <summary> Get or set the text which is displayed.
		/// </summary>

		public string Text {
			get { return display.TextContent; }
			set { display.TextContent = value; }
		}

		/// <summary> Create a new ResultDisplay.
		/// </summary>

		public SimpleStatusDisplay ()
			: base( HtmlUtil.CreateElement( "p", null, null ) ) {
			this.text = HtmlUtil.CreateElement( "span", DomElement, null );
			this.display = HtmlUtil.CreateElement( "span", DomElement, null );

			this.text.TextContent = "Status: ";
		}
	}
}
