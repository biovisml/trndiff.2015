// ChemicalElementTable.cs
//

using System;
using System.Collections.Generic;
using SystemQut.Controls;
using System.Html;
using SystemQut;

namespace Test.Client.Controls {

	public class ChemicalElementTable : CodeBehind {
		private TableElement table;
		private Element heading;
		private ChemicalElement[] data;
		private List<ChemicalElementRow> rows = new List<ChemicalElementRow>();

		public ChemicalElementTable ()
			: base( HtmlUtil.CreateElement( "table", null, new Dictionary<string, string>(
				"cellpadding", "3",
				"cellspacing", "0",
				"border", "1"
				) ) ) {
			table = ( TableElement ) (object) DomElement;
			heading = HtmlUtil.CreateElement( "tr", table, null );

			HtmlUtil.CreateElement( "td", heading, null );

			HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
				"style", "font-weight:bold"
			) ).TextContent = "Name";

			HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
				"style", "font-weight:bold"
			) ).TextContent = "Symbol";

			HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
				"style", "font-weight:bold"
			) ).TextContent = "Atomic Weight";

		}

		private static string KeyFunction ( ChemicalElement datum, int index ) {
			return datum.Symbol;
		}

		public ChemicalElement[] Data {
			get {
				return this.data;
			}
			set {
				this.data = value;

				rows.ForEach( delegate( ChemicalElementRow row ) {
					table.RemoveChild( row.DomElement );
				} );

				rows.Clear();

				if ( value != null ) {
					value.ForEach( delegate( ChemicalElement datum ) {
						ChemicalElementRow row = new ChemicalElementRow( datum );
						rows.Add( row );
						row.AddElementTo( table );
					} );
				}
			}
		}

		public List<ChemicalElementRow> Rows {
			get { return rows; }
		}


		/// <summary> Helper method which gets a comma-separated list of symbols from a 
		///		ChemicalElementTable, for use in unit tests.
		/// </summary>
		/// <returns></returns>

		public string GetAllSymbols () {
			List<String> s = new List<string>();

			Rows.ForEach( delegate( ChemicalElementRow row ) {
				s.Add( row.Data.Symbol );
			} );

			return s.Join( ", " );
		}

		/// <summary> Helper method which gets a comma-separated list of selected symbols from a 
		///		ChemicalElementTable, for use in unit tests.
		/// </summary>
		/// <param name="status"></param>
		/// <returns></returns>

		public string GetSelectedSymbols (
			SimpleStatusDisplay status
		) {
			List<String> s = new List<string>();

			Rows.ForEach( delegate( ChemicalElementRow row ) {
				if ( row.IsSelected ) s.Add( row.Data.Symbol );
			} );

			return s.Join( ", " );
		}


	}
}
