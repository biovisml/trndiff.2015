// ElementRow.cs
//

using System;
using System.Collections.Generic;
using SystemQut.Controls;
using System.Html;
using SystemQut;
using SystemQut.ComponentModel;

namespace Test.Client.Controls {

	public class ChemicalElementRow : CodeBehind {
		private ChemicalElement data;
		private CheckBox checkBox;

		public ChemicalElementRow (
			ChemicalElement data
		)
			: base( Document.CreateElement( "tr" ) ) {
			this.data = data;

			Element domElement = DomElement;
			Element checkBoxCell = HtmlUtil.CreateElement( "td", domElement, null );
			TextControl nameCell = new TextControl( null, "td" ); 
			TextControl symbolCell = new TextControl( null, "td" );
			TextControl weightCell = new TextControl( null, "td" ); 

			checkBox = new CheckBox( null );
			checkBox.AddElementTo( checkBoxCell );
			nameCell.AddElementTo( domElement );
			symbolCell.AddElementTo( domElement );
			weightCell.AddElementTo( domElement );

			// needs to be set after all controls added to the parent, but before binding.
			DataContext = data;

			checkBox.Bind( "IsSelected", "IsChecked", DataBindingMode.TwoWay );
			nameCell.Bind( "Name", "Text", DataBindingMode.OneWay );
			symbolCell.Bind( "Symbol", "Text", DataBindingMode.OneWay );
			weightCell.Bind( "Weight", "Text", DataBindingMode.OneWay );

			checkBox.PropertyChanged += checkbox_PropertyChanged;
		}

		void checkbox_PropertyChanged ( object sender, PropertyChangedEventArgs args ) {
			if ( Clicked != null ) Clicked( this );
		}

		public event Action<ChemicalElementRow> Clicked;

		public ChemicalElement Data {
			get { return data; }
		}

		public bool IsSelected {
			get { return checkBox.IsChecked; }
			set { checkBox.IsChecked = value; }
		}
	}
}
