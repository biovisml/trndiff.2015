﻿using System;
using System.Collections.Generic;
using SystemQut.Controls;
using System.Runtime.CompilerServices;
using System.Html;
using SystemQut;

namespace Test.Client.Controls {

	/// <summary> The first ever user control! Encapsulates a place to display 
	///		unit test results.
	/// </summary>

	public class ResultDisplay : CodeBehind {
		private bool passed;
		private Element text;
		private Element display;

		/// <summary> Get or set the result status.
		/// <para>
		///		Setting updates the visual state of the indicator.
		/// </para>
		/// </summary>

		public bool Passed {
			get {
				return this.passed;
			}
			set {
				passed = value;

				HtmlUtil.SetAttributes( display, new Dictionary<string, string>(
					"style", passed ? "background-color: green" : "background-color: red"
				) ).TextContent = passed ? "Passed" : "Failed";			
			}
		}

		/// <summary> Get or set the text which is displayed.
		/// </summary>

		public string Text {
			get { return text.TextContent; }
			set { text.TextContent = value; }
		}

		/// <summary> Create a new ResultDisplay.
		/// </summary>
		/// <param name="methodName"></param>
		/// <param name="passed"></param>

		public ResultDisplay (
			string methodName,
			bool passed
		) : base( HtmlUtil.CreateElement( "p", null, new Dictionary<string, string>(
				"style", "border-style: solid; border-width: 1px; border-color: lightGray;"
			) )) {
			
			this.display = HtmlUtil.CreateElement( "span", DomElement, null );
			this.text = HtmlUtil.CreateElement( "span", DomElement, null );

			Text = methodName + ": ";
			Passed = passed;
		}
	}
}
