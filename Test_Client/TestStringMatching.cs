﻿using RegPrecise;
using System;
using System.Collections.Generic;
using System.Html;
using System.Html.Data.Files;
using SystemQut;
using SystemQut.IO;
using SystemQut.ServiceModel;
using SystemQUT.Csv;
using Test.Client.Controls;

namespace Test.Client
{
    class TestStringMatching
    {

		static ILocalMySQLService GOservice = new LocalMySQLService( "/LocalMySQL.svc", false );

        static public void GeneFunctionMatching_Contains_Regulon( int regulonId ) {
		    IRegPreciseService RPservice = new RegPreciseService( "/RegPrecise.svc", false );

            string T = "GeneFunctionMatching_Contains_Regulon";
			RPservice.ListGenesInRegulon( regulonId, delegate( ServiceResponse<Gene[]> response_1 ) {
				Assert.IsTrue( response_1.Error == null, T + ": " + "response_1.Error == null" );				
				try {
					Gene[] actualGenes = response_1.Content;
					actualGenes.Sort<Gene>( Gene.OrderById );

                    Console.Log("Checking whether any gene functions in Regulon ID " + regulonId + " match any Gene Ontology terms or synonyms.");

                    GOservice.GO_ListTermsWithSynonyms( delegate( ServiceResponse<GOTerm[]> response_2 ) {
				        Assert.IsTrue( response_2.Error == null, T + ": " + "response_2.Error == null" );

				        try {
					        GOTerm[] actualTerms = response_2.Content;

                            Assert.IsTrue(actualTerms != null, T + ": " + "actual != null" );

					        actualTerms.Sort<GOTerm>( GOTerm.OrderByDatabaseId );

                            foreach (Gene gene in actualGenes) {
					            foreach (GOTerm term in actualTerms) {
                                    if (term.Name.ToLowerCase().IndexOf(gene.GeneFunction.ToLowerCase()) > -1) {
                                        Console.Log("Gene function '" + gene.GeneFunction + "' of gene '" + gene.Name + "' is in GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                    } else if (gene.GeneFunction.ToLowerCase().IndexOf(term.Name.ToLowerCase()) > -1) {
                                        Console.Log("GO term '" + term.Name + "' (" + term.UniqueIdentifier + ") is in gene function '" + gene.GeneFunction + "' of gene '" + gene.Name + "'");
                                    } else {
                                        if (term.Synonyms != null) {
                                            if (term.Synonyms.Length > 0) {
                                                foreach (JSObject synonym in term.Synonyms) {
                                                    GOTermSynonym temp = GOTermSynonym.Parse(synonym);
                                                    if (temp.Name.ToLowerCase().IndexOf(gene.GeneFunction.ToLowerCase()) > -1) {
                                                        Console.Log("Gene function '" + gene.GeneFunction + "' of gene '" + gene.Name + "' is in synonym '" + temp.Name + "' of GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                                    } else if (gene.GeneFunction.ToLowerCase().IndexOf(temp.Name.ToLowerCase()) > -1) {
                                                        Console.Log("Synonym '" + temp.Name + "' of GO term '" + term.Name + "' (" + term.UniqueIdentifier + ") is in gene function '" + gene.GeneFunction + "' of gene '" + gene.Name + "'");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch ( Exception ex ) {
					        Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					        Console.Log( ex.StackTrace );
				        }
                    } );

					Util.AppendResult( T, true );
				}
				catch ( Exception ex ) {
					Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					Console.Log( ex.StackTrace );
				}
			} );
        }
        
        static public void GeneFunctionMatching_Contains(int maxToCompare) {
            string T = "GeneFunctionMatching_Contains";
            try {
                Util.ReadAllText( "/UnitTests/Resources/Csv/" + "geneData.csv", delegate( string text ) {
                    StringReader reader = new StringReader( text );
				    CsvIO csvIO = new CsvIO( ',', '"' );
				    string[][] records = csvIO.Read( reader );

                    Console.Log("Checking whether any gene functions in RegPrecise's list of genes match any Gene Ontology terms or synonyms.");

                    GOservice.GO_ListTermsWithSynonyms( delegate( ServiceResponse<GOTerm[]> response_2 ) {
				        Assert.IsTrue( response_2.Error == null, T + ": " + "response_2.Error == null" );

				        try {
					        GOTerm[] actualTerms = response_2.Content;

                            Assert.IsTrue(actualTerms != null, T + ": " + "actual != null" );

					        actualTerms.Sort<GOTerm>( GOTerm.OrderByDatabaseId );

                            List<string[]> matches = new List<string[]>();
                            matches[0] = new string[] { "RP_genomeId",
                                "RP_taxonomyId",
                                "RP_vimssId",
                                "RP_regulonId",
                                "RP_geneFunction",
                                "RP_geneName",
                                "RP_genomeName",
                                "RP_locusTag",
                                "GO_id",
                                "GO_name",
                                "GO_term_type",
                                "GO_acc",
                                "GO_is_obsolete",
                                "GO_is_root",
                                "GO_is_relation",
                                //"GO_term_id",
                                "GO_term_synonym",
                                "GO_acc_synonym",
                                "GO_synonym_type_id",
                                "GO_synonym_category_id",
                            };

                            if (maxToCompare < 2) {
                                maxToCompare = 2;
                            }
                            for (int i = 1; i < Math.Min(maxToCompare, records.Length); i++) {
                                string geneName = records[i][records[0].IndexOf("geneName")];
                                string geneFunction = records[i][records[0].IndexOf("geneFunction")];
                                string genomeName = records[i][records[0].IndexOf("genomeName")];
					            foreach (GOTerm term in actualTerms) {
                                    if (term.Name.IndexOf(geneFunction) > -1) {
                                        //Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ") is in GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            term.DatabaseId.ToString(),
                                            term.Name,
                                            term.Namespace,
                                            term.UniqueIdentifier,
                                            term.Obsolete.ToString(),
                                            term.Root.ToString(),
                                            term.Relation.ToString(),
                                            //String.Empty,
                                            null,
                                            null,
                                            null,
                                            null
                                        });
                                    } else if (geneFunction.IndexOf(term.Name) > -1) {
                                        //Console.Log("- GO term '" + term.Name + "' (" + term.UniqueIdentifier + ") is in gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ")");
                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            term.DatabaseId.ToString(),
                                            term.Name,
                                            term.Namespace,
                                            term.UniqueIdentifier,
                                            term.Obsolete.ToString(),
                                            term.Root.ToString(),
                                            term.Relation.ToString(),
                                            //null,
                                            null,
                                            null,
                                            null,
                                            null
                                        });
                                    } else {
                                        if (term.Synonyms != null) {
                                            if (term.Synonyms.Length > 0) {
                                                foreach (JSObject synonym in term.Synonyms) {
                                                    GOTermSynonym temp = GOTermSynonym.Parse(synonym);
                                                    if (temp.Name.IndexOf(geneFunction) > -1) {
                                                        //Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ") is in synonym '" + temp.Name + "' of GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                                            records[i][records[0].IndexOf("taxonomyId")],
                                                            records[i][records[0].IndexOf("vimssId")],
                                                            records[i][records[0].IndexOf("regulonId")],
                                                            records[i][records[0].IndexOf("geneFunction")],
                                                            records[i][records[0].IndexOf("geneName")],
                                                            records[i][records[0].IndexOf("genomeName")],
                                                            records[i][records[0].IndexOf("locusTag")],
                                                            term.DatabaseId.ToString(),
                                                            term.Name,
                                                            term.Namespace,
                                                            term.UniqueIdentifier,
                                                            term.Obsolete.ToString(),
                                                            term.Root.ToString(),
                                                            term.Relation.ToString(),
                                                            //temp.TermDatabaseId,
                                                            temp.Name,
                                                            temp.UniqueIdentifier,
                                                            temp.TypeId.ToString(),
                                                            temp.CategoryId != 0 ? temp.CategoryId.ToString() : null
                                                        });
                                                    } else if (geneFunction.IndexOf(temp.Name) > -1) {
                                                        //Console.Log("- Synonym '" + temp.Name + "' of GO term '" + term.Name + "' (" + term.UniqueIdentifier + ") is in gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ")");
                                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                                            records[i][records[0].IndexOf("taxonomyId")],
                                                            records[i][records[0].IndexOf("vimssId")],
                                                            records[i][records[0].IndexOf("regulonId")],
                                                            records[i][records[0].IndexOf("geneFunction")],
                                                            records[i][records[0].IndexOf("geneName")],
                                                            records[i][records[0].IndexOf("genomeName")],
                                                            records[i][records[0].IndexOf("locusTag")],
                                                            term.DatabaseId.ToString(),
                                                            term.Name,
                                                            term.Namespace,
                                                            term.UniqueIdentifier,
                                                            term.Obsolete.ToString(),
                                                            term.Root.ToString(),
                                                            term.Relation.ToString(),
                                                            //temp.TermDatabaseId,
                                                            temp.Name,
                                                            temp.UniqueIdentifier,
                                                            temp.TypeId.ToString(),
                                                            temp.CategoryId != 0 ? temp.CategoryId.ToString() : null
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

			                Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			                ResultDisplay resultDisplay = new ResultDisplay( T, false );
			                contentHolder.AppendChild( resultDisplay.DomElement );

				            StringWriter writer = new StringWriter();

                            csvIO.Write( matches, writer );

				            string doc = writer.ToString();

				            /* Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html */
				            {
					            string fileName = null;

					            Blob blob = new Blob( new string[] { doc }, new Dictionary<string, string>(
						            "type", "text/csv"
					            ) );

					            // Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition. 
					            Func<Blob, string> getBlobUrl = (Func<Blob, string>) Script.Literal( "(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL" );
					            Action<string> revokeBlobUrl = (Action<string>) Script.Literal( "(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL" );

					            string url = getBlobUrl( blob );
					            string actualFileName = fileName ?? "Untitled.csv";

					            if ( (bool) Script.Literal( "!! window.navigator.msSaveBlob" ) ) {
						            // Internet Explorer...
						            Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
					            }
					            else {
						            // FireFox, Chrome...
						            AnchorElement save = (AnchorElement) Document.CreateElement( "a" );
						            save.Href = url;
						            save.Target = "_blank";
						            save.Download = actualFileName;
						            save.Style.Display = "none";
						            save.InnerHTML = "Click to save!";
						
						            // This is a shim for Firefox.
						            string alternateUrl = String.Format( "{0}:{1}:{2}", "text/csv", actualFileName, url );
						            Script.Literal( "if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }" );
						
						            contentHolder.AppendChild( save );

						            // This appears to not work in Firefox.
						            // MutableEvent evt = Document.CreateEvent( "Event" );
						            // evt.InitEvent( "click", true, true );
						            // save.DispatchEvent( evt );

						            save.Click();

						            // This causes grief in Firefox.
						            // revokeBlobUrl( url ); 
					            }
				            }
                        }
                        catch ( Exception ex ) {
					        Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					        Console.Log( ex.StackTrace );
				        }
                    } );

					Util.AppendResult( T, true );
				} );
            }
			catch ( Exception ex ) {
				Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				Console.Log( ex.StackTrace );
			}
        }

        // Tests Porter Stemming on a selection of random words
        // (from here: http://snowball.tartarus.org/algorithms/porter/stemmer.html)
        static public void StemmerBasicTest() {            
            string T = "StemmerBasicTest";
            try {
                Stemmer stemmer = new Stemmer();

				Assert.IsTrue( stemmer != null, T + ": " + "stemmer != null" );
                string[] testStrings = { "connection", "caresses", "ponies", "ties", "caress", "cats", "feed", "agreed", "plastered", "bled", "motoring",
                                       "sing", "conflated", "troubled", "sized", "hopping", "tanned", "falling", "hissing", "fizzed", "failing", "filing",
                                       "happy", "sky", "relational", "conditional", "rational", "digitizer", "vietnamization", "predication", "operator",
                                       "feudalism", "decisiveness", "hopefulness", "callousness", "triplicate", "formative", "formalize", "electical",
                                       "hopeful", "goodness", "revival", "allowance", "inference", "airliner", "gyroscopic", "adjustable", "defensible",
                                       "irritant", "replacement", "adjustment", "dependant", "adoption", "homologous", "communism", "activate", "effective",
                                       "bowdlerize", "probate", "rate", "cease", "control", "roll" };
                foreach (string testString in testStrings) {
                    for (int i = 0; i < testString.Length; i++) {
                        stemmer.add(testString[i]);
                    }
                    stemmer.stem();
				    Assert.IsTrue( stemmer.ToString() != null, T + ": " + "stemmer.ToString() != null" );
                    Console.Log("Stemmer produced " + stemmer.ToString() + " from " + testString);
                };
				Util.AppendResult( T, true );
            } catch ( Exception ex ) {
				Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				Console.Log( ex.StackTrace );
			}
        }

        // Tests Porter Stemming on all of the genes in geneData.csv
        static public void StemmerGeneDataTest(int maxToCompare) {
            string T = "StemmerGeneDataTest";
            try {
                Util.ReadAllText( "/UnitTests/Resources/Csv/" + "geneData.csv", delegate( string text ) {
                    StringReader reader = new StringReader( text );
				    CsvIO csvIO = new CsvIO( ',', '"' );
				    string[][] records = csvIO.Read( reader );

                    Console.Log("Testing Stemmer on geneData.csv");

                    if (maxToCompare < 0) {
                        maxToCompare = records.Length;
                    }

                    if (maxToCompare < 2) {
                        maxToCompare = 2;
                    }

                    for (int i = 1; i < Math.Min(maxToCompare, records.Length); i++) {
                        string geneName = records[i][records[0].IndexOf("geneName")];
                        string geneFunction = records[i][records[0].IndexOf("geneFunction")];
                        string genomeName = records[i][records[0].IndexOf("genomeName")];
                        string[] porterFunctionParts = geneFunction.Split(' ');
                        for (int j = 0; j < porterFunctionParts.Length; j++) {
                            Stemmer tempStemmer = new Stemmer();
                            //foreach (char character in porterFunctionParts[j]) {
                            //    tempStemmer.add(character);
                            for (int k = 0; k < porterFunctionParts[j].Length; k++) {
                                tempStemmer.add(porterFunctionParts[j][k]);
                            }
                            tempStemmer.stem();
                            porterFunctionParts[j] = tempStemmer.ToString();
                        }
                        //string stemmedGeneFunction = new string(porterFunctionParts);
                        //Console.Log(geneName + " - geneFunction = " + geneFunction + "; stemmed form = " + stemmedGeneFunction);
                        Console.Log(geneName + " - geneFunction = " + geneFunction + "; stemmed parts:");
                        foreach (string part in porterFunctionParts) {
                            Console.Log(part);
                        }
                    }
                    
					Util.AppendResult( T, true );
                });
            }
			catch ( Exception ex ) {
				Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				Console.Log( ex.StackTrace );
			}
        }

        private static char[] forbiddenCharacters = { '(', ')', '[', ']', '_', ',', '.', '-', '/', '\\' };
        private static string[] forbiddenWords = { "for", "of", "and", "not", "\\", "\\(", "\\)", ",", ".", "to", "a", "in" };

        // Uses Porter Stemming and Levenshtein distances to try to match
        // RegPrecise gene functions to Gene Ontology terms/synonyms
        static public void GeneFunctionMatching_LevenshteinPorterStemming_1(int maxToCompare) {
            string T = "GeneFunctionMatching_LevenshteinPorterStemming_1";
            try {

                // Loads all of the RegPrecise genes from the provided data file
                Util.ReadAllText( "/UnitTests/Resources/Csv/" + "geneData.csv", delegate( string text ) {
                    StringReader reader = new StringReader( text );
				    CsvIO csvIO = new CsvIO( ',', '"' );
				    string[][] records = csvIO.Read( reader );

                    Console.Log("Checking the closest match for gene functions in RegPrecise's list of genes to Gene Ontology terms or synonyms, based on Levenshtein distance.");

                    // Retreives all of the terms, with any synonyms they might
                    // have, from the (local) MySQL database
                    GOservice.GO_ListTermsWithSynonyms( delegate( ServiceResponse<GOTerm[]> response_2 ) {
				        Assert.IsTrue( response_2.Error == null, T + ": " + "response_2.Error == null" );

				        try {
					        GOTerm[] actualTerms = response_2.Content;

                            Assert.IsTrue(actualTerms != null, T + ": " + "actual != null" );

					        actualTerms.Sort<GOTerm>( GOTerm.OrderByDatabaseId );

                            // Creates the first row of the output file, with
                            // the column headers
                            List<string[]> matches = new List<string[]>();
                            matches[0] = new string[] { "RP_genomeId",
                                "RP_taxonomyId",
                                "RP_vimssId",
                                "RP_regulonId",
                                "RP_geneFunction",
                                "RP_geneName",
                                "RP_genomeName",
                                "RP_locusTag",
                                "GO_id",
                                "GO_name",
                                "GO_term_type",
                                "GO_acc",
                                "GO_is_obsolete",
                                "GO_is_root",
                                "GO_is_relation",
                                //"GO_term_id",
                                "GO_term_synonym",
                                "GO_acc_synonym",
                                "GO_synonym_type_id",
                                "GO_synonym_category_id",
                            };

                            // If the parameter is a negative value, check the
                            // whole list of RegPrecise genes
                            if (maxToCompare < 0) {
                                maxToCompare = records.Length;
                            }

                            // If the parameter is 0, make it one
                            if (maxToCompare < 1) {
                                maxToCompare = 1;
                            }

                            // For each gene in the loaded file
                            for (int i = 1; i < Math.Min(maxToCompare, records.Length); i++) {

                                // Get the gene name, function and genome name for the current gene
                                string geneName = records[i][records[0].IndexOf("geneName")];
                                string geneFunction = records[i][records[0].IndexOf("geneFunction")];
                                string genomeName = records[i][records[0].IndexOf("genomeName")];

                                // Split the function name into parts, with the space character
                                // as the cut off point
                                string[] porterFunctionParts = geneFunction.Split(' ');
                                for (int j = 0; j < porterFunctionParts.Length; j++) {

                                    // Create a new stemmer object, and push each character one by
                                    // one
                                    Stemmer tempStemmer = new Stemmer();
                                    //foreach (char character in porterFunctionParts[j]) {
                                    //    tempStemmer.add(character);
                                    for (int k = 0; k < porterFunctionParts[j].Length; k++) {

                                        // Don't push punctuation to the stemmer
                                        if (forbiddenCharacters.IndexOf(porterFunctionParts[j][k]) == -1) {
                                            tempStemmer.add(porterFunctionParts[j][k]);
                                        }
                                    }

                                    // Perform the stemming and then put the stemmed part back in
                                    // the list
                                    tempStemmer.stem();
                                    porterFunctionParts[j] = tempStemmer.ToString();
                                }
                                //string stemmedGeneFunction = new string(porterFunctionParts);
                                //Console.Log(geneName + " - geneFunction = " + geneFunction + "; stemmed form = " + stemmedGeneFunction);

                                // Concatenate the stemmed parts
                                string stemmedGeneFunction = string.Empty;
                                for (int j = 0; j < porterFunctionParts.Length; j++) {
                                    if (j == 0) {
                                        stemmedGeneFunction = stemmedGeneFunction + porterFunctionParts[j];
                                    } else {
                                        stemmedGeneFunction = stemmedGeneFunction + " " + porterFunctionParts[j];
                                    }
                                }

                                // Stores the current closest term and the associated levenshtein
                                // distance for this gene function
                                int minLevenshtein = int.MaxValue;
                                int[] closestTerm = {-1, -1};

                                // So we don't have to restem each time, put the stemmed terms
                                // and synonyms into a dictionary
                                Dictionary<int, string> stemmedTerms = new Dictionary<int,string>();
                                Dictionary<int, Dictionary<int, string>> stemmedSynonyms = new Dictionary<int,Dictionary<int,string>>();

					            foreach (GOTerm term in actualTerms) {
                                    string stemmedTerm = string.Empty;
                                    int currentTerm = actualTerms.IndexOf(term);

                                    // If this term hasn't already had its parts stemmed...
                                    if (stemmedTerms[currentTerm] == null) {

                                        // Split the term into parts, with the space character as
                                        // the cut off point
                                        string[] porterTermParts = term.Name.Split(' ');
                                        for (int j = 0; j < porterTermParts.Length; j++) {
                                            Stemmer tempStemmer = new Stemmer();
                                            //foreach (char character in porterTermParts[j]) {
                                                //tempStemmer.add(character);
                                            for (int k = 0; k < porterTermParts[j].Length; k++) {

                                                // Don't push punctuation to the stemmer
                                                if (forbiddenCharacters.IndexOf(porterTermParts[j][k]) == -1) {
                                                    tempStemmer.add(porterTermParts[j][k]);
                                                }
                                            }

                                            // Perform the stemming and then put the stemmed part
                                            // back in the list
                                            tempStemmer.stem();
                                            porterTermParts[j] = tempStemmer.ToString();
                                        }
                                        //string stemmedTerm = new string(porterTermParts);
                                        //Console.Log(term.UniqueIdentifier + " - name = " + term.Name + "; stemmed form = " + stemmedTerm);

                                        // Concatenate the stemmed parts
                                        for (int j = 0; j < porterTermParts.Length; j++) {
                                            if (j == 0) {
                                                stemmedTerm = stemmedTerm + porterTermParts[j];
                                            } else {
                                                stemmedTerm = stemmedTerm + " " + porterTermParts[j];
                                            }
                                        }

                                    // Else, use the existing stemmed term
                                    } else {
                                        stemmedTerm = stemmedTerms[currentTerm];
                                    }

                                    // Calculate the Levenshtein difference between the stemmed
                                    // function and the stemmed term, and store the distance and
                                    // the location of the term if it's the current minimum
                                    int currentLevenshteinTerm = LevenshteinDistance(stemmedGeneFunction, stemmedTerm);
                                    if (currentLevenshteinTerm < minLevenshtein) {
                                        minLevenshtein = currentLevenshteinTerm;
                                        Console.Log("New minimum: Levenshtein distance between " + stemmedGeneFunction + " and " + stemmedTerm + " is " + minLevenshtein);
                                        closestTerm[0] = currentTerm;
                                        closestTerm[1] = -1;
                                    }
                                    
                                    // Not all terms have a list of synonyms
                                    if (term.Synonyms != null) {
                                        if (term.Synonyms.Length > 0) {

                                            // If the term does have a list of synonyms, create a
                                            // subdictionary for it
                                            stemmedSynonyms[currentTerm] = new Dictionary<int,string>();
                                            foreach (JSObject synonym in term.Synonyms) {
                                                string stemmedSynonym = string.Empty;

                                                // Because of the JSObject issue, we need to parse
                                                // the synonym object
                                                GOTermSynonym temp = GOTermSynonym.Parse(synonym);
                                                int currentSynonym = term.Synonyms.IndexOf(temp);

                                                // If this synonym hasn't already had its parts stemmed
                                                if (stemmedSynonyms[currentTerm][currentSynonym] == null) {

                                                    // Split the synonym name into parts, with the space character
                                                    // as the cut off point
                                                    string[] porterSynonymParts = temp.Name.Split(' ');
                                                    for (int j = 0; j < porterSynonymParts.Length; j++) {
                                                        Stemmer tempStemmer = new Stemmer();
                                                        //foreach (char character in porterSynonymParts[j]) {
                                                        //    tempStemmer.add(character);                                                    
                                                        for (int k = 0; k < porterSynonymParts[j].Length; k++) {
                                                
                                                            // Don't push punctuation to the stemmer
                                                            if (forbiddenCharacters.IndexOf(porterSynonymParts[j][k]) == -1) {
                                                                tempStemmer.add(porterSynonymParts[j][k]);
                                                            }
                                                        }

                                                        // Perform the stemming and then put the stemmed part back in the list
                                                        tempStemmer.stem();
                                                        porterSynonymParts[j] = tempStemmer.ToString();
                                                    }
                                                    //string stemmedSynonym = new string(porterSynonymParts);
                                                    //Console.Log("synonym " + temp.UniqueIdentifier + " - name = " + temp.Name + "; stemmed form = " + stemmedSynonym);

                                                    // Concatenate the stemmed parts
                                                    stemmedSynonym = string.Empty;
                                                    for (int j = 0; j < porterSynonymParts.Length; j++) {
                                                        if (j == 0) {
                                                            stemmedSynonym = stemmedSynonym + porterSynonymParts[j];
                                                        } else {
                                                            stemmedSynonym = stemmedSynonym + " " + porterSynonymParts[j];
                                                        }
                                                    }

                                                // Else, use the existing stemmed synonym
                                                } else {
                                                    stemmedSynonym = stemmedSynonyms[currentTerm][currentSynonym];
                                                }                                                

                                                // Calculate the Levenshtein difference between the stemmed
                                                // function and the stemmed synonym, and store the distance and
                                                // the location of the synonym if it's the current minimum
                                                int currentLevenshteinSynonym = LevenshteinDistance(stemmedGeneFunction, stemmedTerm);
                                                if (currentLevenshteinSynonym < minLevenshtein) {
                                                    minLevenshtein = currentLevenshteinSynonym;
                                                    Console.Log("New minimum: Levenshtein distance between " + stemmedGeneFunction + " and " + stemmedSynonym + " is " + minLevenshtein);
                                                    closestTerm[0] = currentTerm;
                                                    closestTerm[1] = currentSynonym;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                // If we found a closest term or synonym, output the RegPrecise
                                // gene and the term/synonym it matched with to the output list
                                if (closestTerm[0] > -1) {
                                    GOTerm term = actualTerms[closestTerm[0]];
                                    if (closestTerm[1] > -1) {
                                        GOTermSynonym temp = GOTermSynonym.Parse(actualTerms[closestTerm[0]].Synonyms[closestTerm[1]]);
                                        Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ")  was matched closest to synonym '" + temp.Name + "' of GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            term.DatabaseId.ToString(),
                                            term.Name,
                                            term.Namespace,
                                            term.UniqueIdentifier,
                                            term.Obsolete.ToString(),
                                            term.Root.ToString(),
                                            term.Relation.ToString(),
                                            //temp.TermDatabaseId,
                                            temp.Name,
                                            temp.UniqueIdentifier,
                                            temp.TypeId.ToString(),
                                            temp.CategoryId != 0 ? temp.CategoryId.ToString() : null
                                        });
                                    } else {
                                        Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ")  was matched closest to GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            term.DatabaseId.ToString(),
                                            term.Name,
                                            term.Namespace,
                                            term.UniqueIdentifier,
                                            term.Obsolete.ToString(),
                                            term.Root.ToString(),
                                            term.Relation.ToString(),
                                            //String.Empty,
                                            null,
                                            null,
                                            null,
                                            null
                                        });
                                    }
                                }
                            }

			                Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			                ResultDisplay resultDisplay = new ResultDisplay( T, false );
			                contentHolder.AppendChild( resultDisplay.DomElement );

				            StringWriter writer = new StringWriter();

                            csvIO.Write( matches, writer );

				            string doc = writer.ToString();

				            // Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html
				            {
					            string fileName = null;

					            Blob blob = new Blob( new string[] { doc }, new Dictionary<string, string>(
						            "type", "text/csv"
					            ) );

					            // Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition. 
					            Func<Blob, string> getBlobUrl = (Func<Blob, string>) Script.Literal( "(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL" );
					            Action<string> revokeBlobUrl = (Action<string>) Script.Literal( "(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL" );

					            string url = getBlobUrl( blob );
					            string actualFileName = fileName ?? "Untitled.csv";

					            if ( (bool) Script.Literal( "!! window.navigator.msSaveBlob" ) ) {
						            // Internet Explorer...
						            Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
					            }
					            else {
						            // FireFox, Chrome...
						            AnchorElement save = (AnchorElement) Document.CreateElement( "a" );
						            save.Href = url;
						            save.Target = "_blank";
						            save.Download = actualFileName;
						            save.Style.Display = "none";
						            save.InnerHTML = "Click to save!";
						
						            // This is a shim for Firefox.
						            string alternateUrl = String.Format( "{0}:{1}:{2}", "text/csv", actualFileName, url );
						            Script.Literal( "if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }" );
						
						            contentHolder.AppendChild( save );

						            save.Click();
					            }
				            }
                        }
                        catch ( Exception ex ) {
					        Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					        Console.Log( ex.StackTrace );
				        }
                    } );

					Util.AppendResult( T, true );
				} );
            }
			catch ( Exception ex ) {
				Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				Console.Log( ex.StackTrace );
			}
        }
        
        // Uses Porter Stemming to try to match RegPrecise gene functions to
        // Gene Ontology terms/synonyms. Each part of the stemmed functions/
        // terms/synonyms is matched together to find the closest match
        static public void GeneFunctionMatching_PorterStemmingMatching(int maxToCompare) {
            string T = "GeneFunctionMatching_PorterStemmingMatching";
            try {

                // Loads all of the RegPrecise genes from the provided data file
                Util.ReadAllText( "/UnitTests/Resources/Csv/" + "geneData.csv", delegate( string text ) {
                    StringReader reader = new StringReader( text );
				    CsvIO csvIO = new CsvIO( ',', '"' );
				    string[][] records = csvIO.Read( reader );

                    Console.Log("Checking the closest match for gene functions in RegPrecise's list of genes to Gene Ontology terms or synonyms, based on the matching of stemmed parts of the full name.");

                    // Retreives all of the terms, with any synonyms they might
                    // have, from the (local) MySQL database
                    GOservice.GO_ListTermsWithSynonyms( delegate( ServiceResponse<GOTerm[]> response_2 ) {
				        Assert.IsTrue( response_2.Error == null, T + ": " + "response_2.Error == null" );

				        try {
					        GOTerm[] actualTerms = response_2.Content;

                            Assert.IsTrue(actualTerms != null, T + ": " + "actual != null" );

					        actualTerms.Sort<GOTerm>( GOTerm.OrderByDatabaseId );

                            // Creates the first row of the output file, with
                            // the column headers
                            List<string[]> matches = new List<string[]>();
                            matches[0] = new string[] { "RP_genomeId",
                                "RP_taxonomyId",
                                "RP_vimssId",
                                "RP_regulonId",
                                "RP_geneFunction",
                                "RP_geneName",
                                "RP_genomeName",
                                "RP_locusTag",
                                "GO_id",
                                "GO_name",
                                "GO_term_type",
                                "GO_acc",
                                "GO_is_obsolete",
                                "GO_is_root",
                                "GO_is_relation",
                                //"GO_term_id",
                                "GO_term_synonym",
                                "GO_acc_synonym",
                                "GO_synonym_type_id",
                                "GO_synonym_category_id",
                            };

                            // If the parameter is a negative value, check the
                            // whole list of RegPrecise genes
                            if (maxToCompare < 0) {
                                maxToCompare = records.Length;
                            }

                            // If the parameter is 0, make it one
                            if (maxToCompare < 1) {
                                maxToCompare = 1;
                            }

                            // For each gene in the loaded file
                            for (int i = 1; i < Math.Min(maxToCompare, records.Length); i++) {

                                // Get the gene name, function and genome name for the current gene
                                string geneName = records[i][records[0].IndexOf("geneName")];
                                string geneFunction = records[i][records[0].IndexOf("geneFunction")];
                                string genomeName = records[i][records[0].IndexOf("genomeName")];

                                // Split the function name into parts, with the space character
                                // as the cut off point
                                string[] porterFunctionParts = geneFunction.Split(' ');
                                for (int j = 0; j < porterFunctionParts.Length; j++) {

                                    // Don't try to stem forbidden words
                                    if (forbiddenWords.IndexOf(porterFunctionParts[j].ToLowerCase()) == -1) {

                                        // Create a new stemmer object, and push each character one by
                                        // one
                                        Stemmer tempStemmer = new Stemmer();
                                        for (int k = 0; k < porterFunctionParts[j].Length; k++) {

                                            // Don't push punctuation to the stemmer
                                            if (forbiddenCharacters.IndexOf(porterFunctionParts[j][k]) == -1) {
                                                tempStemmer.add(porterFunctionParts[j][k]);
                                            }
                                        }

                                        // Perform the stemming and then put the stemmed part back in
                                        // the list
                                        tempStemmer.stem();
                                        porterFunctionParts[j] = tempStemmer.ToString();
                                    }
                                }
                                

                                // Concatenate the stemmed parts for viewing later
                                string stemmedGeneFunction = string.Empty;
                                for (int j = 0; j < porterFunctionParts.Length; j++) {
                                    if (j == 0) {
                                        stemmedGeneFunction = stemmedGeneFunction + porterFunctionParts[j];
                                    } else {
                                        stemmedGeneFunction = stemmedGeneFunction + " " + porterFunctionParts[j];
                                    }
                                }

                                // Stores the current closest term and the associated number of
                                // matches for this gene function
                                int maxMatches = 0;
                                int[] closestTerm = {-1, -1};

                                // So we don't have to restem each time, put the stemmed terms
                                // and synonyms into a dictionary
                                Dictionary<int, string[]> stemmedTerms = new Dictionary<int,string[]>();
                                Dictionary<int, Dictionary<int, string[]>> stemmedSynonyms = new Dictionary<int,Dictionary<int,string[]>>();

					            foreach (GOTerm term in actualTerms) {
                                    int currentTerm = actualTerms.IndexOf(term);
                                    string[] porterTermParts;

                                    // If this term hasn't already had its parts stemmed...
                                    if (stemmedTerms[currentTerm] == null) {

                                        // Split the term into parts, with the space character as
                                        // the cut off point
                                        porterTermParts = term.Name.Split(' ');
                                        for (int j = 0; j < porterTermParts.Length; j++) {

                                            // Don't try to stem forbidden words
                                            if (forbiddenWords.IndexOf(porterTermParts[j].ToLowerCase()) == -1) {

                                                Stemmer tempStemmer = new Stemmer();
                                                for (int k = 0; k < porterTermParts[j].Length; k++) {

                                                    // Don't push punctuation to the stemmer
                                                    if (forbiddenCharacters.IndexOf(porterTermParts[j][k]) == -1) {
                                                        tempStemmer.add(porterTermParts[j][k]);
                                                    }
                                                }

                                                // Perform the stemming and then put the stemmed part
                                                // back in the list
                                                tempStemmer.stem();
                                                porterTermParts[j] = tempStemmer.ToString();
                                            }
                                        }

                                        stemmedTerms[currentTerm] = porterTermParts;

                                    // Else, use the existing stemmed term
                                    } else {
                                        porterTermParts = stemmedTerms[currentTerm];
                                    }

                                    
                                    // Concatenate the stemmed parts for viewing later
                                    string stemmedTerm = string.Empty;
                                    for (int j = 0; j < porterTermParts.Length; j++) {
                                        if (j == 0) {
                                            stemmedTerm = stemmedTerm + porterTermParts[j];
                                        } else {
                                            stemmedTerm = stemmedTerm + " " + porterTermParts[j];
                                        }
                                    }

                                    // Calculate how many parts of the stemmed function and the
                                    // stemmed term match, and store the distance and the location
                                    // of the term if it's the current maximum
                                    int currentMatchesTerm = 0;
                                    foreach (string functionPart in porterFunctionParts) {

                                        //Don't try to match forbidden words
                                        if (forbiddenWords.IndexOf(functionPart.ToLowerCase()) == -1) {
                                            foreach (string termPart in porterTermParts) {
                                                if (forbiddenWords.IndexOf(termPart.ToLowerCase()) == -1 && functionPart.ToLowerCase() == termPart.ToLowerCase()) {
                                                    currentMatchesTerm = currentMatchesTerm + 1;
                                                }
                                            }
                                        }
                                    }
                                    if (maxMatches < currentMatchesTerm) {
                                        maxMatches = currentMatchesTerm;
                                        Console.Log("New maximum - matches between " + stemmedGeneFunction + " and " + stemmedTerm + " are " + maxMatches);
                                        closestTerm[0] = currentTerm;
                                        closestTerm[1] = -1;
                                    }
                                    
                                    // Not all terms have a list of synonyms
                                    if (term.Synonyms != null) {
                                        if (term.Synonyms.Length > 0) {

                                            // If the term does have a list of synonyms, create a
                                            // subdictionary for it
                                            stemmedSynonyms[currentTerm] = new Dictionary<int,string[]>();
                                            foreach (JSObject synonym in term.Synonyms) {

                                                // Because of the JSObject issue, we need to parse
                                                // the synonym object
                                                GOTermSynonym temp = GOTermSynonym.Parse(synonym);
                                                int currentSynonym = term.Synonyms.IndexOf(temp);
                                                string[] porterSynonymParts;

                                                // If this synonym hasn't already had its parts stemmed
                                                if (stemmedSynonyms[currentTerm][currentSynonym] == null) {

                                                    // Split the synonym name into parts, with the space character
                                                    // as the cut off point
                                                    porterSynonymParts = temp.Name.Split(' ');
                                                    for (int j = 0; j < porterSynonymParts.Length; j++) {

                                                        // Don't try to stem forbidden words
                                                        if (forbiddenWords.IndexOf(porterSynonymParts[j].ToLowerCase()) == -1) {

                                                            Stemmer tempStemmer = new Stemmer();                                   
                                                            for (int k = 0; k < porterSynonymParts[j].Length; k++) {
                                                
                                                                // Don't push punctuation to the stemmer
                                                                if (forbiddenCharacters.IndexOf(porterSynonymParts[j][k]) == -1) {
                                                                    tempStemmer.add(porterSynonymParts[j][k]);
                                                                }
                                                            }

                                                            // Perform the stemming and then put the stemmed part back in the list
                                                            tempStemmer.stem();
                                                            porterSynonymParts[j] = tempStemmer.ToString();
                                                        }
                                                    }

                                                    stemmedSynonyms[currentTerm][currentSynonym] = porterSynonymParts;

                                                // Else, use the existing stemmed synonym
                                                } else {
                                                    porterSynonymParts = stemmedSynonyms[currentTerm][currentSynonym];
                                                }                                            
                                                
                                                // Concatenate the stemmed parts for viewing later
                                                string stemmedSynonym = string.Empty;
                                                for (int j = 0; j < porterSynonymParts.Length; j++) {
                                                    if (j == 0) {
                                                        stemmedSynonym = stemmedSynonym + porterSynonymParts[j];
                                                    } else {
                                                        stemmedSynonym = stemmedSynonym + " " + porterSynonymParts[j];
                                                    }
                                                }

                                                // Calculate how many parts of the stemmed function and the
                                                // stemmed term match, and store the distance and the location
                                                // of the term if it's the current maximum
                                                int currentMatchesSynonym = 0;
                                                foreach (string functionPart in porterFunctionParts) {

                                                    //Don't try to match forbidden words
                                                    if (forbiddenWords.IndexOf(functionPart.ToLowerCase()) == -1) {
                                                        foreach (string synonymPart in porterSynonymParts) {
                                                            if (forbiddenWords.IndexOf(synonymPart.ToLowerCase()) == -1 && functionPart.ToLowerCase() == synonymPart.ToLowerCase()) {
                                                                currentMatchesSynonym = currentMatchesSynonym + 1;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (maxMatches < currentMatchesTerm) {
                                                    maxMatches = currentMatchesTerm;
                                                    Console.Log("New maximum - matches between " + stemmedGeneFunction + " and " + stemmedSynonym + " are " + maxMatches);
                                                    closestTerm[0] = currentTerm;
                                                    closestTerm[1] = currentSynonym;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                // If we found a closest term or synonym, output the RegPrecise
                                // gene and the term/synonym it matched with to the output list
                                if (closestTerm[0] > -1) {
                                    GOTerm term = actualTerms[closestTerm[0]];
                                    if (closestTerm[1] > -1) {
                                        GOTermSynonym temp = GOTermSynonym.Parse(actualTerms[closestTerm[0]].Synonyms[closestTerm[1]]);
                                        Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ") was matched closest to synonym '" + temp.Name + "' of GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            term.DatabaseId.ToString(),
                                            term.Name,
                                            term.Namespace,
                                            term.UniqueIdentifier,
                                            term.Obsolete.ToString(),
                                            term.Root.ToString(),
                                            term.Relation.ToString(),
                                            //temp.TermDatabaseId,
                                            temp.Name,
                                            temp.UniqueIdentifier,
                                            temp.TypeId.ToString(),
                                            temp.CategoryId != 0 ? temp.CategoryId.ToString() : null
                                        });
                                    } else {
                                        Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ")  was matched closest to GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            term.DatabaseId.ToString(),
                                            term.Name,
                                            term.Namespace,
                                            term.UniqueIdentifier,
                                            term.Obsolete.ToString(),
                                            term.Root.ToString(),
                                            term.Relation.ToString(),
                                            //String.Empty,
                                            null,
                                            null,
                                            null,
                                            null
                                        });
                                    }
                                } else {
                                    Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ") was not matched to anything!");
                                    matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            //String.Empty,
                                            null,
                                            null,
                                            null,
                                            null
                                        });
                                }
                            }

			                Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			                ResultDisplay resultDisplay = new ResultDisplay( T, false );
			                contentHolder.AppendChild( resultDisplay.DomElement );

				            StringWriter writer = new StringWriter();

                            csvIO.Write( matches, writer );

				            string doc = writer.ToString();

				            // Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html
				            {
					            string fileName = null;

					            Blob blob = new Blob( new string[] { doc }, new Dictionary<string, string>(
						            "type", "text/csv"
					            ) );

					            // Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition. 
					            Func<Blob, string> getBlobUrl = (Func<Blob, string>) Script.Literal( "(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL" );
					            Action<string> revokeBlobUrl = (Action<string>) Script.Literal( "(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL" );

					            string url = getBlobUrl( blob );
					            string actualFileName = fileName ?? "Untitled.csv";

					            if ( (bool) Script.Literal( "!! window.navigator.msSaveBlob" ) ) {
						            // Internet Explorer...
						            Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
					            }
					            else {
						            // FireFox, Chrome...
						            AnchorElement save = (AnchorElement) Document.CreateElement( "a" );
						            save.Href = url;
						            save.Target = "_blank";
						            save.Download = actualFileName;
						            save.Style.Display = "none";
						            save.InnerHTML = "Click to save!";
						
						            // This is a shim for Firefox.
						            string alternateUrl = String.Format( "{0}:{1}:{2}", "text/csv", actualFileName, url );
						            Script.Literal( "if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }" );
						
						            contentHolder.AppendChild( save );

						            save.Click();
					            }
				            }
                        }
                        catch ( Exception ex ) {
					        Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					        Console.Log( ex.StackTrace );
				        }
                    } );

					Util.AppendResult( T, true );
				} );
            }
			catch ( Exception ex ) {
				Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				Console.Log( ex.StackTrace );
			}
        }
        
        // Uses Porter Stemming and Levenshtein distances to try to match
        // RegPrecise gene functions to Gene Ontology terms/synonyms. Each part
        // of the stemmed functions/terms/synonyms is matched using Levenshtein
        // to find the smallest distance across all of the words
        static public void GeneFunctionMatching_LevenshteinPorterStemming_2(int maxToCompare) {
            string T = "GeneFunctionMatching_LevenshteinPorterStemming_2";
            try {

                // Loads all of the RegPrecise genes from the provided data file
                Util.ReadAllText( "/UnitTests/Resources/Csv/" + "geneData.csv", delegate( string text ) {
                    StringReader reader = new StringReader( text );
				    CsvIO csvIO = new CsvIO( ',', '"' );
				    string[][] records = csvIO.Read( reader );

                    Console.Log("Checking the closest match for gene functions in RegPrecise's list of genes to Gene Ontology terms or synonyms, based on the matching of stemmed parts of the full name by Levenshtein distance.");

                    // Retreives all of the terms, with any synonyms they might
                    // have, from the (local) MySQL database
                    GOservice.GO_ListTermsWithSynonyms( delegate( ServiceResponse<GOTerm[]> response_2 ) {
				        Assert.IsTrue( response_2.Error == null, T + ": " + "response_2.Error == null" );

				        try {
					        GOTerm[] actualTerms = response_2.Content;

                            Assert.IsTrue(actualTerms != null, T + ": " + "actual != null" );

					        actualTerms.Sort<GOTerm>( GOTerm.OrderByDatabaseId );

                            // Creates the first row of the output file, with
                            // the column headers
                            List<string[]> matches = new List<string[]>();
                            matches[0] = new string[] { "RP_genomeId",
                                "RP_taxonomyId",
                                "RP_vimssId",
                                "RP_regulonId",
                                "RP_geneFunction",
                                "RP_geneName",
                                "RP_genomeName",
                                "RP_locusTag",
                                "GO_id",
                                "GO_name",
                                "GO_term_type",
                                "GO_acc",
                                "GO_is_obsolete",
                                "GO_is_root",
                                "GO_is_relation",
                                //"GO_term_id",
                                "GO_term_synonym",
                                "GO_acc_synonym",
                                "GO_synonym_type_id",
                                "GO_synonym_category_id",
                            };

                            // If the parameter is a negative value, check the
                            // whole list of RegPrecise genes
                            if (maxToCompare < 0) {
                                maxToCompare = records.Length;
                            }

                            // If the parameter is 0, make it one
                            if (maxToCompare < 1) {
                                maxToCompare = 1;
                            }

                            // For each gene in the loaded file
                            for (int i = 1; i < Math.Min(maxToCompare, records.Length); i++) {

                                // Get the gene name, function and genome name for the current gene
                                string geneName = records[i][records[0].IndexOf("geneName")];
                                string geneFunction = records[i][records[0].IndexOf("geneFunction")];
                                string genomeName = records[i][records[0].IndexOf("genomeName")];

                                // Split the function name into parts, with the space character
                                // as the cut off point
                                string[] porterFunctionParts = geneFunction.Split(' ');
                                for (int j = 0; j < porterFunctionParts.Length; j++) {

                                    // Don't try to stem forbidden words
                                    if (forbiddenWords.IndexOf(porterFunctionParts[j].ToLowerCase()) == -1) {

                                        // Create a new stemmer object, and push each character one by
                                        // one
                                        Stemmer tempStemmer = new Stemmer();
                                        for (int k = 0; k < porterFunctionParts[j].Length; k++) {

                                            // Don't push punctuation to the stemmer
                                            if (forbiddenCharacters.IndexOf(porterFunctionParts[j][k]) == -1) {
                                                tempStemmer.add(porterFunctionParts[j][k]);
                                            }
                                        }

                                        // Perform the stemming and then put the stemmed part back in
                                        // the list
                                        tempStemmer.stem();
                                        porterFunctionParts[j] = tempStemmer.ToString();
                                    }
                                }
                                

                                // Concatenate the stemmed parts for viewing later
                                string stemmedGeneFunction = string.Empty;
                                for (int j = 0; j < porterFunctionParts.Length; j++) {
                                    if (j == 0) {
                                        stemmedGeneFunction = stemmedGeneFunction + porterFunctionParts[j];
                                    } else {
                                        stemmedGeneFunction = stemmedGeneFunction + " " + porterFunctionParts[j];
                                    }
                                }

                                // Stores the current closest term and the associated number of
                                // matches for this gene function
                                int totalMinLevenshtein = int.MaxValue;
                                int[] closestTerm = {-1, -1};

                                // So we don't have to restem each time, put the stemmed terms
                                // and synonyms into a dictionary
                                Dictionary<int, string[]> stemmedTerms = new Dictionary<int,string[]>();
                                Dictionary<int, Dictionary<int, string[]>> stemmedSynonyms = new Dictionary<int,Dictionary<int,string[]>>();

					            foreach (GOTerm term in actualTerms) {
                                    int currentTerm = actualTerms.IndexOf(term);
                                    string[] porterTermParts;

                                    // If this term hasn't already had its parts stemmed...
                                    if (stemmedTerms[currentTerm] == null) {

                                        // Split the term into parts, with the space character as
                                        // the cut off point
                                        porterTermParts = term.Name.Split(' ');
                                        for (int j = 0; j < porterTermParts.Length; j++) {

                                            // Don't try to stem forbidden words
                                            if (forbiddenWords.IndexOf(porterTermParts[j].ToLowerCase()) == -1) {

                                                Stemmer tempStemmer = new Stemmer();
                                                for (int k = 0; k < porterTermParts[j].Length; k++) {

                                                    // Don't push punctuation to the stemmer
                                                    if (forbiddenCharacters.IndexOf(porterTermParts[j][k]) == -1) {
                                                        tempStemmer.add(porterTermParts[j][k]);
                                                    }
                                                }

                                                // Perform the stemming and then put the stemmed part
                                                // back in the list
                                                tempStemmer.stem();
                                                porterTermParts[j] = tempStemmer.ToString();
                                            }
                                        }

                                        stemmedTerms[currentTerm] = porterTermParts;

                                    // Else, use the existing stemmed term
                                    } else {
                                        porterTermParts = stemmedTerms[currentTerm];
                                    }

                                    
                                    // Concatenate the stemmed parts for viewing later
                                    string stemmedTerm = string.Empty;
                                    for (int j = 0; j < porterTermParts.Length; j++) {
                                        if (j == 0) {
                                            stemmedTerm = stemmedTerm + porterTermParts[j];
                                        } else {
                                            stemmedTerm = stemmedTerm + " " + porterTermParts[j];
                                        }
                                    }

                                    // Calculate the Levenshtein distance between each stemmed part
                                    // of the function and each stemmed part of the term, and add
                                    // up the distances of the closest match for each part of the
                                    // function. If this is the current minimum total, store the
                                    // distance and location of the term
                                    int currentTotalMinLevenshtein = 0;
                                    foreach (string functionPart in porterFunctionParts) {

                                        //Don't try to match forbidden words
                                        if (forbiddenWords.IndexOf(functionPart.ToLowerCase()) == -1) {
                                            int currentMinLevenshtein = int.MaxValue;
                                            foreach (string termPart in porterTermParts) {
                                                if (forbiddenWords.IndexOf(termPart.ToLowerCase()) == -1) {
                                                    int currentLevenshtein = LevenshteinDistance(functionPart.ToLowerCase(), termPart.ToLowerCase());
                                                    if (currentLevenshtein < currentMinLevenshtein) {
                                                        currentMinLevenshtein = currentLevenshtein;
                                                    }
                                                }
                                            }
                                            currentTotalMinLevenshtein = currentTotalMinLevenshtein + currentMinLevenshtein;
                                        }
                                    }
                                    if (totalMinLevenshtein > currentTotalMinLevenshtein) {
                                        totalMinLevenshtein = currentTotalMinLevenshtein;
                                        Console.Log("New minimum - matches between " + stemmedGeneFunction + " and " + stemmedTerm + " create a total of " + totalMinLevenshtein);
                                        closestTerm[0] = currentTerm;
                                        closestTerm[1] = -1;
                                    }
                                    
                                    // Not all terms have a list of synonyms
                                    if (term.Synonyms != null) {
                                        if (term.Synonyms.Length > 0) {

                                            // If the term does have a list of synonyms, create a
                                            // subdictionary for it
                                            stemmedSynonyms[currentTerm] = new Dictionary<int,string[]>();
                                            foreach (JSObject synonym in term.Synonyms) {

                                                // Because of the JSObject issue, we need to parse
                                                // the synonym object
                                                GOTermSynonym temp = GOTermSynonym.Parse(synonym);
                                                int currentSynonym = term.Synonyms.IndexOf(temp);
                                                string[] porterSynonymParts;

                                                // If this synonym hasn't already had its parts stemmed
                                                if (stemmedSynonyms[currentTerm][currentSynonym] == null) {

                                                    // Split the synonym name into parts, with the space character
                                                    // as the cut off point
                                                    porterSynonymParts = temp.Name.Split(' ');
                                                    for (int j = 0; j < porterSynonymParts.Length; j++) {

                                                        // Don't try to stem forbidden words
                                                        if (forbiddenWords.IndexOf(porterSynonymParts[j].ToLowerCase()) == -1) {

                                                            Stemmer tempStemmer = new Stemmer();                                   
                                                            for (int k = 0; k < porterSynonymParts[j].Length; k++) {
                                                
                                                                // Don't push punctuation to the stemmer
                                                                if (forbiddenCharacters.IndexOf(porterSynonymParts[j][k]) == -1) {
                                                                    tempStemmer.add(porterSynonymParts[j][k]);
                                                                }
                                                            }

                                                            // Perform the stemming and then put the stemmed part back in the list
                                                            tempStemmer.stem();
                                                            porterSynonymParts[j] = tempStemmer.ToString();
                                                        }
                                                    }

                                                    stemmedSynonyms[currentTerm][currentSynonym] = porterSynonymParts;

                                                // Else, use the existing stemmed synonym
                                                } else {
                                                    porterSynonymParts = stemmedSynonyms[currentTerm][currentSynonym];
                                                }                                            
                                                
                                                // Concatenate the stemmed parts for viewing later
                                                string stemmedSynonym = string.Empty;
                                                for (int j = 0; j < porterSynonymParts.Length; j++) {
                                                    if (j == 0) {
                                                        stemmedSynonym = stemmedSynonym + porterSynonymParts[j];
                                                    } else {
                                                        stemmedSynonym = stemmedSynonym + " " + porterSynonymParts[j];
                                                    }
                                                }

                                                // Calculate the Levenshtein distance between each stemmed part
                                                // of the function and each stemmed part of the synonym, and add
                                                // up the distances of the closest match for each part of the
                                                // function. If this is the current minimum total, store the
                                                // distance and location of the synonym
                                                currentTotalMinLevenshtein = 0;
                                                foreach (string functionPart in porterFunctionParts) {

                                                    //Don't try to match forbidden words
                                                    if (forbiddenWords.IndexOf(functionPart.ToLowerCase()) == -1) {
                                                        int currentMinLevenshtein = int.MaxValue;
                                                        foreach (string synonymPart in porterSynonymParts) {
                                                            if (forbiddenWords.IndexOf(synonymPart.ToLowerCase()) == -1) {
                                                                int currentLevenshtein = LevenshteinDistance(functionPart.ToLowerCase(), synonymPart.ToLowerCase());
                                                                if (currentLevenshtein < currentMinLevenshtein) {
                                                                    currentMinLevenshtein = currentLevenshtein;
                                                                }
                                                            }
                                                        }
                                                        currentTotalMinLevenshtein = currentTotalMinLevenshtein + currentMinLevenshtein;
                                                    }
                                                }
                                                if (totalMinLevenshtein > currentTotalMinLevenshtein) {
                                                    totalMinLevenshtein = currentTotalMinLevenshtein;
                                                    Console.Log("New minimum - matches between " + stemmedGeneFunction + " and " + stemmedSynonym + " create a total of " + totalMinLevenshtein);
                                                    closestTerm[0] = currentTerm;
                                                    closestTerm[1] = currentSynonym;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                // If we found a closest term or synonym, output the RegPrecise
                                // gene and the term/synonym it matched with to the output list
                                if (closestTerm[0] > -1) {
                                    GOTerm term = actualTerms[closestTerm[0]];
                                    if (closestTerm[1] > -1) {
                                        GOTermSynonym temp = GOTermSynonym.Parse(actualTerms[closestTerm[0]].Synonyms[closestTerm[1]]);
                                        Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ") was matched closest to synonym '" + temp.Name + "' of GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            term.DatabaseId.ToString(),
                                            term.Name,
                                            term.Namespace,
                                            term.UniqueIdentifier,
                                            term.Obsolete.ToString(),
                                            term.Root.ToString(),
                                            term.Relation.ToString(),
                                            //temp.TermDatabaseId,
                                            temp.Name,
                                            temp.UniqueIdentifier,
                                            temp.TypeId.ToString(),
                                            temp.CategoryId != 0 ? temp.CategoryId.ToString() : null
                                        });
                                    } else {
                                        Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ")  was matched closest to GO term '" + term.Name + "' (" + term.UniqueIdentifier + ")");
                                        matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            term.DatabaseId.ToString(),
                                            term.Name,
                                            term.Namespace,
                                            term.UniqueIdentifier,
                                            term.Obsolete.ToString(),
                                            term.Root.ToString(),
                                            term.Relation.ToString(),
                                            //String.Empty,
                                            null,
                                            null,
                                            null,
                                            null
                                        });
                                    }
                                } else {
                                    Console.Log("- Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ") was not matched to anything!");
                                    matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            records[i][records[0].IndexOf("geneFunction")],
                                            records[i][records[0].IndexOf("geneName")],
                                            records[i][records[0].IndexOf("genomeName")],
                                            records[i][records[0].IndexOf("locusTag")],
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            //String.Empty,
                                            null,
                                            null,
                                            null,
                                            null
                                        });
                                }
                            }

			                Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			                ResultDisplay resultDisplay = new ResultDisplay( T, false );
			                contentHolder.AppendChild( resultDisplay.DomElement );

				            StringWriter writer = new StringWriter();

                            csvIO.Write( matches, writer );

				            string doc = writer.ToString();

				            // Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html
				            {
					            string fileName = null;

					            Blob blob = new Blob( new string[] { doc }, new Dictionary<string, string>(
						            "type", "text/csv"
					            ) );

					            // Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition. 
					            Func<Blob, string> getBlobUrl = (Func<Blob, string>) Script.Literal( "(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL" );
					            Action<string> revokeBlobUrl = (Action<string>) Script.Literal( "(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL" );

					            string url = getBlobUrl( blob );
					            string actualFileName = fileName ?? "Untitled.csv";

					            if ( (bool) Script.Literal( "!! window.navigator.msSaveBlob" ) ) {
						            // Internet Explorer...
						            Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
					            }
					            else {
						            // FireFox, Chrome...
						            AnchorElement save = (AnchorElement) Document.CreateElement( "a" );
						            save.Href = url;
						            save.Target = "_blank";
						            save.Download = actualFileName;
						            save.Style.Display = "none";
						            save.InnerHTML = "Click to save!";
						
						            // This is a shim for Firefox.
						            string alternateUrl = String.Format( "{0}:{1}:{2}", "text/csv", actualFileName, url );
						            Script.Literal( "if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }" );
						
						            contentHolder.AppendChild( save );

						            save.Click();
					            }
				            }
                        }
                        catch ( Exception ex ) {
					        Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					        Console.Log( ex.StackTrace );
				        }
                    } );

					Util.AppendResult( T, true );
				} );
            }
			catch ( Exception ex ) {
				Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				Console.Log( ex.StackTrace );
			}
        }
        
        // Uses Porter Stemming and Levenshtein distances to try to match
        // RegPrecise gene functions to Gene Ontology terms/synonyms. Each part
        // of the stemmed functions/terms/synonyms is matched using Levenshtein
        // to find the smallest distance across all of the words
        //
        // This one is limited to terms only and only checks for molecular
        // functions. It then drills up to see the top most category for the
        // term that isn't "molecular function"
        static public void GeneFunctionMatching_LevenshteinPorterStemming_2b(int maxToCompare) {
            string T = "GeneFunctionMatching_LevenshteinPorterStemming_2b";
            try {

                // Loads all of the RegPrecise genes from the provided data file
                //Util.ReadAllText( "/UnitTests/Resources/Csv/" + "geneData.csv", delegate( string text ) {
                Util.ReadAllText( "/UnitTests/Resources/Csv/" + "geneData_unique.csv", delegate( string text ) {
                    StringReader reader = new StringReader( text );
				    CsvIO csvIO = new CsvIO( ',', '"' );
				    string[][] records = csvIO.Read( reader );

                   // Console.Log("Checking the closest match for gene functions in RegPrecise's list of genes to Gene Ontology terms or synonyms, based on the matching of stemmed parts of the full name by Levenshtein distance.");
                   Console.Log("Checking the closest match for gene functions in RegPrecise's list of genes (unique gene functions only) to Gene Ontology terms or synonyms, based on the matching of stemmed parts of the full name by Levenshtein distance.");

                    // Retreives all of the terms, with any synonyms they might
                    // have, from the (local) MySQL database
                    GOservice.GO_ListTermsWithParents( delegate( ServiceResponse<GOTerm[]> response_2 ) {
				        Assert.IsTrue( response_2.Error == null, T + ": " + "response_2.Error == null" );

				        try {
					        GOTerm[] actualTerms = response_2.Content;

                            Assert.IsTrue(actualTerms != null, T + ": " + "actual != null" );

					        actualTerms.Sort<GOTerm>( GOTerm.OrderByDatabaseId );

                            // Creates the first row of the output file, with
                            // the column headers
                            List<string[]> matches = new List<string[]>();
                            matches[0] = new string[] { "RP_genomeId",
                                "RP_taxonomyId",
                                "RP_vimssId",
                                "RP_regulonId",
                                "RP_geneFunction",
                                //"RP_geneName",
                                //"RP_genomeName",
                                "RP_locusTag",
                                "Length_of_RP_gene_function",
                                "GO_id",
                                "GO_name",
                                "GO_term_type",
                                "GO_acc",
                                "GO_is_obsolete",
                                "GO_is_root",
                                "GO_is_relation",
                                "Length_of_GO_term",
                                "Difference_in_length",
                                "Levenshtein_distance",
                                "Top_most_GO_id",
                                "Top_most_GO_name",
                                "Top_most_GO_term_type",
                                "Top_most_GO_acc",
                                "Top_most_GO_is_obsolete",
                                "Top_most_GO_is_root",
                                "Top_most_GO_is_relation",
                                "Number_of_top_most_terms",
                            };

                            // If the parameter is a negative value, check the
                            // whole list of RegPrecise genes
                            if (maxToCompare < 0) {
                                maxToCompare = records.Length;
                            }

                            // If the parameter is 0, make it one
                            if (maxToCompare < 1) {
                                maxToCompare = 1;
                            }

                            // For each gene in the loaded file
                            for (int i = 1; i < Math.Min(maxToCompare, records.Length); i++) {

                                // Get the gene name, function and genome name for the current gene
                                //string geneName = records[i][records[0].IndexOf("geneName")];
                                string geneFunction = records[i][records[0].IndexOf("geneFunction")];
                                //string genomeName = records[i][records[0].IndexOf("genomeName")];

                                // Split the function name into parts, with the space character
                                // as the cut off point
                                string[] porterFunctionParts = geneFunction.Split(' ');
                                for (int j = 0; j < porterFunctionParts.Length; j++) {

                                    // Don't try to stem forbidden words
                                    if (forbiddenWords.IndexOf(porterFunctionParts[j].ToLowerCase()) == -1) {

                                        // Create a new stemmer object, and push each character one by
                                        // one
                                        Stemmer tempStemmer = new Stemmer();
                                        for (int k = 0; k < porterFunctionParts[j].Length; k++) {

                                            // Don't push punctuation to the stemmer
                                            if (forbiddenCharacters.IndexOf(porterFunctionParts[j][k]) == -1) {
                                                tempStemmer.add(porterFunctionParts[j][k]);
                                            }
                                        }

                                        // Perform the stemming and then put the stemmed part back in
                                        // the list
                                        tempStemmer.stem();
                                        porterFunctionParts[j] = tempStemmer.ToString();
                                    }
                                }
                                

                                // Concatenate the stemmed parts for viewing later
                                string stemmedGeneFunction = string.Empty;
                                for (int j = 0; j < porterFunctionParts.Length; j++) {
                                    if (j == 0) {
                                        stemmedGeneFunction = stemmedGeneFunction + porterFunctionParts[j];
                                    } else {
                                        stemmedGeneFunction = stemmedGeneFunction + " " + porterFunctionParts[j];
                                    }
                                }

                                // Stores the current closest term and the associated number of
                                // matches for this gene function
                                int totalMinLevenshtein = int.MaxValue;
                                int closestTerm = -1;

                                // So we don't have to restem each time, put the stemmed terms
                                // and synonyms into a dictionary
                                Dictionary<int, string[]> stemmedTerms = new Dictionary<int,string[]>();
                                Dictionary<int, Dictionary<int, string[]>> stemmedSynonyms = new Dictionary<int,Dictionary<int,string[]>>();

					            foreach (GOTerm term in actualTerms) {

                                    // Only molecular functions, and only if they're not obsolete.
                                    // Have to compare to the name for the moment since it's not
                                    // being imported corectly
                                    if (term.Namespace == "molecular_function" && term.Name.ToLowerCase().IndexOf("obsolete") == -1) {
                                        int currentTerm = actualTerms.IndexOf(term);
                                        string[] porterTermParts;

                                        // If this term hasn't already had its parts stemmed...
                                        if (stemmedTerms[currentTerm] == null) {

                                            // Split the term into parts, with the space character as
                                            // the cut off point
                                            porterTermParts = term.Name.Split(' ');
                                            for (int j = 0; j < porterTermParts.Length; j++) {

                                                // Don't try to stem forbidden words
                                                if (forbiddenWords.IndexOf(porterTermParts[j].ToLowerCase()) == -1) {

                                                    Stemmer tempStemmer = new Stemmer();
                                                    for (int k = 0; k < porterTermParts[j].Length; k++) {

                                                        // Don't push punctuation to the stemmer
                                                        if (forbiddenCharacters.IndexOf(porterTermParts[j][k]) == -1) {
                                                            tempStemmer.add(porterTermParts[j][k]);
                                                        }
                                                    }

                                                    // Perform the stemming and then put the stemmed part
                                                    // back in the list
                                                    tempStemmer.stem();
                                                    porterTermParts[j] = tempStemmer.ToString();
                                                }
                                            }

                                            stemmedTerms[currentTerm] = porterTermParts;

                                        // Else, use the existing stemmed term
                                        } else {
                                            porterTermParts = stemmedTerms[currentTerm];
                                        }

                                    
                                        // Concatenate the stemmed parts for viewing later
                                        string stemmedTerm = string.Empty;
                                        for (int j = 0; j < porterTermParts.Length; j++) {
                                            if (j == 0) {
                                                stemmedTerm = stemmedTerm + porterTermParts[j];
                                            } else {
                                                stemmedTerm = stemmedTerm + " " + porterTermParts[j];
                                            }
                                        }

                                        // Calculate the Levenshtein distance between each stemmed part
                                        // of the function and each stemmed part of the term, and add
                                        // up the distances of the closest match for each part of the
                                        // function. If this is the current minimum total, store the
                                        // distance and location of the term
                                        int currentTotalMinLevenshtein = 0;
                                        foreach (string functionPart in porterFunctionParts) {

                                            //Don't try to match forbidden words
                                            if (forbiddenWords.IndexOf(functionPart.ToLowerCase()) == -1) {
                                                int currentMinLevenshtein = int.MaxValue;
                                                foreach (string termPart in porterTermParts) {
                                                    if (forbiddenWords.IndexOf(termPart.ToLowerCase()) == -1) {
                                                        int currentLevenshtein = LevenshteinDistance(functionPart.ToLowerCase(), termPart.ToLowerCase());
                                                        if (currentLevenshtein < currentMinLevenshtein) {
                                                            currentMinLevenshtein = currentLevenshtein;
                                                        }
                                                    }
                                                }
                                                currentTotalMinLevenshtein = currentTotalMinLevenshtein + currentMinLevenshtein;
                                            }
                                        }
                                        if (totalMinLevenshtein > currentTotalMinLevenshtein) {
                                            totalMinLevenshtein = currentTotalMinLevenshtein;
                                            //Console.Log("New minimum - matches between " + stemmedGeneFunction + " and " + stemmedTerm + " create a total of " + totalMinLevenshtein);
                                            closestTerm = currentTerm;
                                        }
                                    }
                                }
                                
                                // If we found a closest term or synonym, output the RegPrecise
                                // gene and the term/synonym it matched with to the output list
                                if (closestTerm > -1) {
                                    GOTerm term = actualTerms[closestTerm];
                                        //Console.Log(i + " - Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ")  was matched closest to molecular function '" + term.Name + "' (" + term.UniqueIdentifier + "), with a Levenshtein distance total of " + totalMinLevenshtein);
                                        Console.Log(i + " - Gene function '" + geneFunction + "' was matched closest to molecular function '" + term.Name + "' (" + term.UniqueIdentifier + "), with a Levenshtein distance total of " + totalMinLevenshtein);
                                        List<GOTerm> topMostTerms = FindTopMostCategoryTerms(actualTerms, new List<GOTerm>(), term);
                                        if (topMostTerms.Length > 0) {
                                            GOTerm topMostTerm = topMostTerms[0];
                                            Console.Log("      - Top most parent is '" + topMostTerm.Name + "' (" + topMostTerm.UniqueIdentifier + ")" );
                                            matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                                records[i][records[0].IndexOf("taxonomyId")],
                                                records[i][records[0].IndexOf("vimssId")],
                                                records[i][records[0].IndexOf("regulonId")],
                                                geneFunction,
                                                //geneName,
                                                //genomeName,
                                                records[i][records[0].IndexOf("locusTag")],
                                                geneFunction.Length.ToString(),
                                                term.DatabaseId.ToString(),
                                                term.Name,
                                                term.Namespace,
                                                term.UniqueIdentifier,
                                                term.Obsolete.ToString(),
                                                term.Root.ToString(),
                                                term.Relation.ToString(),
                                                term.Name.Length.ToString(),
                                                (term.Name.Length - geneFunction.Length).ToString(),
                                                totalMinLevenshtein.ToString(),
                                                topMostTerm.DatabaseId.ToString(),
                                                topMostTerm.Name,
                                                topMostTerm.Namespace,
                                                topMostTerm.UniqueIdentifier,
                                                topMostTerm.Obsolete.ToString(),
                                                topMostTerm.Root.ToString(),
                                                topMostTerm.Relation.ToString(),
                                                topMostTerms.Length.ToString(),
                                            });
                                        } else {
                                            Console.Log("      - Top most parent couldn't be found." );
                                            matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                                records[i][records[0].IndexOf("taxonomyId")],
                                                records[i][records[0].IndexOf("vimssId")],
                                                records[i][records[0].IndexOf("regulonId")],
                                                geneFunction,
                                                //geneName,
                                                //genomeName,
                                                records[i][records[0].IndexOf("locusTag")],
                                                geneFunction.Length.ToString(),
                                                term.DatabaseId.ToString(),
                                                term.Name,
                                                term.Namespace,
                                                term.UniqueIdentifier,
                                                term.Obsolete.ToString(),
                                                term.Root.ToString(),
                                                term.Relation.ToString(),
                                                term.Name.Length.ToString(),
                                                (term.Name.Length - geneFunction.Length).ToString(),
                                                totalMinLevenshtein.ToString(),
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                            });
                                        }
                                    } else {
                                    //Console.Log(i + " - Gene function '" + geneFunction + "' of gene '" + geneName + "' (from genome " + genomeName + ") was not matched to any molecular function!");
                                    Console.Log(i + " - Gene function '" + geneFunction + "' was not matched to any molecular function!");
                                    matches.Add(new string[] { records[i][records[0].IndexOf("genomeId")],
                                            records[i][records[0].IndexOf("taxonomyId")],
                                            records[i][records[0].IndexOf("vimssId")],
                                            records[i][records[0].IndexOf("regulonId")],
                                            geneFunction,
                                            //geneName,
                                            //genomeName,
                                            records[i][records[0].IndexOf("locusTag")],
                                            geneFunction.Length.ToString(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                        });
                                }
                            }

			                Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, null );
			                ResultDisplay resultDisplay = new ResultDisplay( T, false );
			                contentHolder.AppendChild( resultDisplay.DomElement );

				            StringWriter writer = new StringWriter();

                            csvIO.Write( matches, writer );

				            string doc = writer.ToString();

				            // Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html
				            {
					            string fileName = null;

					            Blob blob = new Blob( new string[] { doc }, new Dictionary<string, string>(
						            "type", "text/csv"
					            ) );

					            // Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition. 
					            Func<Blob, string> getBlobUrl = (Func<Blob, string>) Script.Literal( "(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL" );
					            Action<string> revokeBlobUrl = (Action<string>) Script.Literal( "(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL" );

					            string url = getBlobUrl( blob );
					            string actualFileName = fileName ?? "Untitled.csv";

					            if ( (bool) Script.Literal( "!! window.navigator.msSaveBlob" ) ) {
						            // Internet Explorer...
						            Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
					            }
					            else {
						            // FireFox, Chrome...
						            AnchorElement save = (AnchorElement) Document.CreateElement( "a" );
						            save.Href = url;
						            save.Target = "_blank";
						            save.Download = actualFileName;
						            save.Style.Display = "none";
						            save.InnerHTML = "Click to save!";
						
						            // This is a shim for Firefox.
						            string alternateUrl = String.Format( "{0}:{1}:{2}", "text/csv", actualFileName, url );
						            Script.Literal( "if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }" );
						
						            contentHolder.AppendChild( save );

						            save.Click();
					            }
				            }
                        }
                        catch ( Exception ex ) {
					        Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					        Console.Log( ex.StackTrace );
				        }
                    } );

					Util.AppendResult( T, true );
				} );
            }
			catch ( Exception ex ) {
				Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				Console.Log( ex.StackTrace );
			}
        }        

        // Levenshtein distance calculator
        // https://en.wikipedia.org/wiki/Levenshtein_distance#Iterative_with_two_matrix_rows
        private static int LevenshteinDistance(string s, string t)
        {
            // degenerate cases
            if (s == t) return 0;
            if (s.Length == 0) return t.Length;
            if (t.Length == 0) return s.Length;

            // create two work vectors of integer distances
            int[] v0 = new int[t.Length + 1];
            int[] v1 = new int[t.Length + 1];

            // initialize v0 (the previous row of distances)
            // this row is A[0][i]: edit distance for an empty s
            // the distance is just the number of characters to delete from t
            for (int i = 0; i < v0.Length; i++)
                v0[i] = i;

            for (int i = 0; i < s.Length; i++)
            {
                // calculate v1 (current row distances) from the previous row v0

                // first element of v1 is A[i+1][0]
                //   edit distance is delete (i+1) chars from s to match empty t
                v1[0] = i + 1;

                // use formula to fill in the rest of the row
                for (int j = 0; j < t.Length; j++)
                {
                    int cost = (s[i] == t[j]) ? 0 : 1;
                    v1[j + 1] = Math.Min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost);
                }

                // copy v1 (current row) to v0 (previous row) for next iteration
                for (int j = 0; j < v0.Length; j++)
                    v0[j] = v1[j];
            }

            return v1[t.Length];
        }

        // Finds the top most parents of this term that's not one of the actual
        // ontologies
        private static List<GOTerm> FindTopMostCategoryTerms(GOTerm[] originalTermList, List<GOTerm> parentTermList, GOTerm term)
        {
            if (term.Relationships != null && term.Relationships.Length > 0) {
                foreach (JSObject relationship in term.Relationships) {
                    GOTermRelationship temp = GOTermRelationship.Parse(relationship);
                    if (temp.RelationshipDatabaseId == 1) {
                        if (originalTermList[temp.ParentDatabaseId - 1].Name == originalTermList[temp.ParentDatabaseId - 1].Namespace) {
                            parentTermList.Add(term);
                        } else {
                            parentTermList = FindTopMostCategoryTerms(originalTermList, parentTermList, originalTermList[temp.ParentDatabaseId - 1]);
                        }
                    }
                }
            }
            return parentTermList;
        }
    }

    //Porter stemming, on Jim's suggestion
    //http://tartarus.org/martin/PorterStemmer/csharp.txt
    /**
	    * Stemmer, implementing the Porter Stemming Algorithm
	    *
	    * The Stemmer class transforms a word into its root form.  The input
	    * word can be provided a character at time (by calling add()), or at once
	    * by calling one of the various stem(something) methods.
	    */

	class Stemmer {
		private char[] b;
		private int i;     /* offset into b */
		private int i_end; /* offset to end of stemmed word */
		private int j;
        private int k;
		private static int INC = 50;
		/* unit of size whereby b is increased */
		
		public Stemmer() {
			b = new char[INC];
			i = 0;
			i_end = 0;
		}

		/**
		    * Add a character to the word being stemmed.  When you are finished
		    * adding characters, you can call stem(void) to stem the word.
		    */

		public void add(char ch) {
			if (i == b.Length) {
				char[] new_b = new char[i+INC];
				for (int c = 0; c < i; c++)
					new_b[c] = b[c];
				b = new_b;
			}
			b[i++] = ch;
		}

		/** Adds wLen characters to the word being stemmed contained in a portion
		    * of a char[] array. This is like repeated calls of add(char ch), but
		    * faster.
		    */

		public void add2(char[] w, int wLen) {
			if (i+wLen >= b.Length) {
				char[] new_b = new char[i+wLen+INC];
				for (int c = 0; c < i; c++)
					new_b[c] = b[c];
				b = new_b;
			}
			for (int c = 0; c < wLen; c++)
				b[i++] = w[c];
		}

		/**
		    * After a word has been stemmed, it can be retrieved by toString(),
		    * or a reference to the internal buffer can be retrieved by getResultBuffer
		    * and getResultLength (which is generally more efficient.)
		    */
		public override string ToString() {
			//return new String(b,0,i_end);
            //return new String(b);
            string temp = string.Empty;
            for (int i = 0; i < i_end; i++) {
                temp = temp + b[i];
            }
            return temp;
		}

		/**
		    * Returns the length of the word resulting from the stemming process.
		    */
		public int getResultLength() {
			return i_end;
		}

		/**
		    * Returns a reference to a character buffer containing the results of
		    * the stemming process.  You also need to consult getResultLength()
		    * to determine the length of the result.
		    */
		public char[] getResultBuffer() {
			return b;
		}

		/* cons(i) is true <=> b[i] is a consonant. */
		private bool cons(int i) {
			switch (b[i]) {
				case 'a': case 'e': case 'i': case 'o': case 'u': return false;
				case 'y': return (i==0) ? true : !cons(i-1);
				default: return true;
			}
		}

		/* m() measures the number of consonant sequences between 0 and j. if c is
		    a consonant sequence and v a vowel sequence, and <..> indicates arbitrary
		    presence,

			    <c><v>       gives 0
			    <c>vc<v>     gives 1
			    <c>vcvc<v>   gives 2
			    <c>vcvcvc<v> gives 3
			    ....
		*/
		private int m() {
			int n = 0;
			int i = 0;
			while(true) {
				if (i > j) return n;
				if (! cons(i)) break; i++;
			}
			i++;
			while(true) {
				while(true) {
					if (i > j) return n;
					if (cons(i)) break;
					i++;
				}
				i++;
				n++;
				while(true) {
					if (i > j) return n;
					if (! cons(i)) break;
					i++;
				}
				i++;
			}
		}

		/* vowelinstem() is true <=> 0,...j contains a vowel */
		private bool vowelinstem() {
			int i;
			for (i = 0; i <= j; i++)
				if (! cons(i))
					return true;
			return false;
		}

		/* doublec(j) is true <=> j,(j-1) contain a double consonant. */
		private bool doublec(int j) {
			if (j < 1)
				return false;
			if (b[j] != b[j-1])
				return false;
			return cons(j);
		}

		/* cvc(i) is true <=> i-2,i-1,i has the form consonant - vowel - consonant
		    and also if the second c is not w,x or y. this is used when trying to
		    restore an e at the end of a short word. e.g.

			    cav(e), lov(e), hop(e), crim(e), but
			    snow, box, tray.

		*/
		private bool cvc(int i) {
			if (i < 2 || !cons(i) || cons(i-1) || !cons(i-2))
				return false;
			int ch = b[i];
			if (ch == 'w' || ch == 'x' || ch == 'y')
				return false;
			return true;
		}

		private bool ends(String s) {
			int l = s.Length;
			int o = k-l+1;
			if (o < 0)
				return false;
			//char[] sc = s.ToCharArray();
            char[] sc = new char[l];
            for (int i = 0; i < l; i++) {
                sc[i] = s[i];
            }
			for (int i = 0; i < l; i++)
				if (b[o+i] != sc[i])
					return false;
			j = k-l;
			return true;
		}

		/* setto(s) sets (j+1),...k to the characters in the string s, readjusting
		    k. */
		private void setto(String s) {
			int l = s.Length;
			int o = j+1;
			//char[] sc = s.ToCharArray();            
            char[] sc = new char[l];
            for (int i = 0; i < l; i++) {
                sc[i] = s[i];
            }
			for (int i = 0; i < l; i++)
				b[o+i] = sc[i];
			k = j+l;
		}

		/* r(s) is used further down. */
		private void r(String s) {
			if (m() > 0)
				setto(s);
		}

		/* step1() gets rid of plurals and -ed or -ing. e.g.
			    caresses  ->  caress
			    ponies    ->  poni
			    ties      ->  ti
			    caress    ->  caress
			    cats      ->  cat

			    feed      ->  feed
			    agreed    ->  agree
			    disabled  ->  disable

			    matting   ->  mat
			    mating    ->  mate
			    meeting   ->  meet
			    milling   ->  mill
			    messing   ->  mess

			    meetings  ->  meet

		*/

		private void step1() {
			if (b[k] == 's') {
				if (ends("sses"))
					k -= 2;
				else if (ends("ies"))
					setto("i");
				else if (b[k-1] != 's')
					k--;
			}
			if (ends("eed")) {
				if (m() > 0)
					k--;
			} else if ((ends("ed") || ends("ing")) && vowelinstem()) {
				k = j;
				if (ends("at"))
					setto("ate");
				else if (ends("bl"))
					setto("ble");
				else if (ends("iz"))
					setto("ize");
				else if (doublec(k)) {
					k--;
					int ch = b[k];
					if (ch == 'l' || ch == 's' || ch == 'z')
						k++;
				}
				else if (m() == 1 && cvc(k)) setto("e");
			}
		}

		/* step2() turns terminal y to i when there is another vowel in the stem. */
		private void step2() {
			if (ends("y") && vowelinstem())
				b[k] = 'i';
		}

		/* step3() maps double suffices to single ones. so -ization ( = -ize plus
		    -ation) maps to -ize etc. note that the string before the suffix must give
		    m() > 0. */
		private void step3() {
			if (k == 0)
				return;
			
			/* For Bug 1 */
			switch (b[k-1]) {
				case 'a':
					if (ends("ational")) { r("ate"); break; }
					if (ends("tional")) { r("tion"); break; }
					break;
				case 'c':
					if (ends("enci")) { r("ence"); break; }
					if (ends("anci")) { r("ance"); break; }
					break;
				case 'e':
					if (ends("izer")) { r("ize"); break; }
					break;
				case 'l':
					if (ends("bli")) { r("ble"); break; }
					if (ends("alli")) { r("al"); break; }
					if (ends("entli")) { r("ent"); break; }
					if (ends("eli")) { r("e"); break; }
					if (ends("ousli")) { r("ous"); break; }
					break;
				case 'o':
					if (ends("ization")) { r("ize"); break; }
					if (ends("ation")) { r("ate"); break; }
					if (ends("ator")) { r("ate"); break; }
					break;
				case 's':
					if (ends("alism")) { r("al"); break; }
					if (ends("iveness")) { r("ive"); break; }
					if (ends("fulness")) { r("ful"); break; }
					if (ends("ousness")) { r("ous"); break; }
					break;
				case 't':
					if (ends("aliti")) { r("al"); break; }
					if (ends("iviti")) { r("ive"); break; }
					if (ends("biliti")) { r("ble"); break; }
					break;
				case 'g':
					if (ends("logi")) { r("log"); break; }
					break;
				default :
					break;
			}
		}

		/* step4() deals with -ic-, -full, -ness etc. similar strategy to step3. */
		private void step4() {
			switch (b[k]) {
				case 'e':
					if (ends("icate")) { r("ic"); break; }
					if (ends("ative")) { r(""); break; }
					if (ends("alize")) { r("al"); break; }
					break;
				case 'i':
					if (ends("iciti")) { r("ic"); break; }
					break;
				case 'l':
					if (ends("ical")) { r("ic"); break; }
					if (ends("ful")) { r(""); break; }
					break;
				case 's':
					if (ends("ness")) { r(""); break; }
					break;
			}
		}

		/* step5() takes off -ant, -ence etc., in context <c>vcvc<v>. */
		private void step5() {
			if (k == 0)
 				return; 

			/* for Bug 1 */
			switch ( b[k-1] ) {
				case 'a':
					if (ends("al")) break; return;
				case 'c':
					if (ends("ance")) break;
					if (ends("ence")) break; return;
				case 'e':
					if (ends("er")) break; return;
				case 'i':
					if (ends("ic")) break; return;
				case 'l':
					if (ends("able")) break;
					if (ends("ible")) break; return;
				case 'n':
					if (ends("ant")) break;
					if (ends("ement")) break;
					if (ends("ment")) break;
					/* element etc. not stripped before the m */
					if (ends("ent")) break; return;
				case 'o':
					if (ends("ion") && j >= 0 && (b[j] == 's' || b[j] == 't')) break;
					/* j >= 0 fixes Bug 2 */
					if (ends("ou")) break; return;
					/* takes care of -ous */
				case 's':
					if (ends("ism")) break; return;
				case 't':
					if (ends("ate")) break;
					if (ends("iti")) break; return;
				case 'u':
					if (ends("ous")) break; return;
				case 'v':
					if (ends("ive")) break; return;
				case 'z':
					if (ends("ize")) break; return;
				default:
					return;
			}
			if (m() > 1)
				k = j;
		}

		/* step6() removes a final -e if m() > 1. */
		private void step6() {
			j = k;
			
			if (b[k] == 'e') {
				int a = m();
				if (a > 1 || a == 1 && !cvc(k-1))
					k--;
			}
			if (b[k] == 'l' && doublec(k) && m() > 1)
				k--;
		}

		/** Stem the word placed into the Stemmer buffer through calls to add().
		    * Returns true if the stemming process resulted in a word different
		    * from the input.  You can retrieve the result with
		    * getResultLength()/getResultBuffer() or toString().
		    */
		public void stem() {
			k = i - 1;
			if (k > 1) {
				step1();
				step2();
				step3();
				step4();
				step5();
				step6();
			}
			i_end = k+1;
			i = 0;
		}
    }
}
