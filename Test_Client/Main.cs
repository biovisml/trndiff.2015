// Page.cs
//

using System;
using System.Collections.Generic;

using SystemQut;
using Test.Client;

static class Main {

	public static void Run () {
		//Window.Alert( "here!" );

		Assert.Process += delegate( Exception error ) {
			Util.AppendText( error.StackTrace );
		};

		Action<Action[]> runTests = delegate( Action[] tests ) {
			foreach ( Action test in tests ) {
				try {
					test();
				}
				catch ( Exception ex ) {
					Console.Log( ex.StackTrace + "\n" );
				}
			}
		};

		// TestGenBankParser.RunAllTests();

		// TODO: Problems with TestEColiRpoD and all the regprecise calls need to be looked at.

		//TestFastAParser.RunAllTests();
		//runTests( new TestRegPrecise().Tests );
		//runTests( new TestValidCsvFiles().AllTests );

		// runTests( new TestSvg().Tests );

		//runTests( new TestControls().Tests );
		//runTests( new TestD3().Tests );
		//TestStringReader.RunAllTests();
		//TestXmlReader.RunAllTests();
		//TestBlastParser.RunAllTests();

		//new TestDragDrop().RunAllTests();
        //new TestRegPrecise().Test1001();
        //new TestRegPrecise().TestVector();
        /*TestSvg testSvg = new TestSvg();
        for (int i = 0; i < testSvg.Tests.Length; i++)
        {
            Action test = testSvg.Tests[i];

            try
            {
                test();
            }
            catch (Exception ex)
            {
                Window.Alert("Unhandled error:\n" + ex.Message + "\n" + ex.StackTrace);
            }
        }*/
       // new TestTouch().RunAllTests();
        //new TestGeneData().RunAllTests();
        //TestOBOImport.RunAllTests();

        TestMySQL.RunAllTests();

		/*TestRegPrecise testRegPrecise = new TestRegPrecise();
        for (int i = 0; i < testRegPrecise.Tests.Length; i++)
        {
            Action test = testRegPrecise.Tests[i];

            try
            {
                test();
            }
            catch (Exception ex)
            {
                Window.Alert("Unhandled error:\n" + ex.Message + "\n" + ex.StackTrace);
            }
        }*/

		//runTests( new TestHammer().Tests );

        //TestStringMatching.GeneFunctionMatching_LevenshteinPorterStemming_2(40);
        //TestStringMatching.GeneFunctionMatching_LevenshteinPorterStemming_2b(100);

        //TestRetrieveFromGO.RunTests();
        //TestRetrieveFromEntrez.RunTests();
        //TestRetrieveFromUniProt.RunTests();
    }
}
