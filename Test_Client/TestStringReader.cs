﻿/*
 * TestStringReader.cs
 * 
 * JavaScript unit tests for StringReader class.
 * 
 * Copyright Queensland University of Technology, 2013.
 * 
 */

using System;
using System.Collections.Generic;

using SystemQut.IO;
using System.Html;

namespace Test.Client {
	internal class TestStringReader {
		static public void RunAllTests() {
			Action [] tests = {
				TestReadCharFromEmptyString,
				TestReadLineFromEmptyString,
				TestReadCharFromNonEmptyString,
				TestReadLineFromNonEmptyString,
			};

			for ( int i = 0; i < tests.Length; i++ ) {
				Action test = tests[i];
				test();
			}
		}

		static void TestReadCharFromEmptyString () {
			string source = "";
			StringReader reader = new StringReader( source );
			char expected = TextReader.NUL;
			char actual = reader.ReadChar();
			Util.AppendResult( "TestReadCharFromEmptyString", expected == actual );
		}

		static void TestReadLineFromEmptyString () {
			string source = "";
			StringReader reader = new StringReader( source );
			string expected = null;
			string actual = reader.ReadLine();
			Util.AppendResult( "TestReadLineFromEmptyString", expected == actual );
		}

		static void TestReadCharFromNonEmptyString() {
			string source = "abcdefghijklmnop\r\nqrstuvwxyz\t\n";
			StringReader reader = new StringReader( source );
			bool allOk = true;

			for ( int i = 0; i < source.Length; i++ ) {
				char expected = source[i];
				char actual = reader.ReadChar();
				allOk = allOk && expected == actual;
			}

			Util.AppendResult( "TestReadCharFromNonEmptyString", allOk );
		}

		static void TestReadLineFromNonEmptyString() {
			string [] lines = { "abcdefghijklmnop", "qrstuvwxyz\t", "", "bananas are not strawberries." };
			string source = lines[0] + "\r\n" + lines[1] + "\n" + lines[2] + "\r" + lines[3];
			StringReader reader = new StringReader( source );
			bool allOk = true;

			for ( int i = 0; i < lines.Length; i++ ) {
				string actual = reader.ReadLine();
				string expected = lines[i];
				allOk = allOk && expected == actual;
			}

			Util.AppendResult( "TestReadLineFromNonEmptyString", allOk );
		}
	}
}
