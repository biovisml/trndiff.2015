// TestSvg.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Svg;
using SystemQut;

namespace Test.Client {

	public class TestSvg {
		public readonly Action[] Tests = new Action[] {
				CreateSvg,
				CreateGroup,
				DrawLine,
				DisplayText,
                DrawPath,
			};

		const double VIEWBOX_WIDTH = 1000;
		const double VIEWBOX_HEIGHT = 1000;
		static readonly SvgLength SVG_WIDTH = new SvgLength( 6, SvgLengthType.IN );
		static readonly SvgLength SVG_HEIGHT = new SvgLength( 1.5, SvgLengthType.IN );
		static readonly SvgRect VIEWBOX = new SvgRect( 0, 0, VIEWBOX_WIDTH, VIEWBOX_HEIGHT );

		static void CreateSvg() {
			const string T = "CreateSvg";
			try {
				SvgSvg svg = Svg.CreateOuterSvgElement( HtmlUtil.CreateElement( "div", Document.Body, null ), null );
				svg.Width = SVG_WIDTH;
				svg.Height = SVG_HEIGHT;
				svg.ViewBox = new SvgRect( 0, 0, VIEWBOX_WIDTH, VIEWBOX_HEIGHT );

				SvgRectangle rect = new SvgRectangle( svg, 0, 0, VIEWBOX_WIDTH, VIEWBOX_HEIGHT, null );
				rect.Fill = "yellow";

				Assert.IsTrue( SVG_WIDTH.IsEqualTo( svg.Width ), T + ": " + "svg.Width is incorrect." );
				Assert.IsTrue( SVG_HEIGHT.IsEqualTo( svg.Height ), T + ": " + "svg.Height is incorrect." );
				Assert.IsTrue( VIEWBOX.IsEqualTo( svg.ViewBox ), T + ": " + "svg.ViewBox is incorrect." );

				Util.AppendResult( T, true );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": exception caught -- " + ex.StackTrace, false );
			}
		}

		static void CreateGroup() {
			const string T = "CreateGroup";
			try {
				SvgLength _100pc = new SvgLength( 100, SvgLengthType.Percentage );
				SvgLength _50pc = new SvgLength( 50, SvgLengthType.Percentage );

				SvgSvg svg = Svg.CreateOuterSvgElement(
					HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>( "style", "overflow: hidden;" ) ),
					null );
				svg.Width = _50pc;
				svg.Height = _50pc;
				// svg.ViewBox = VIEWBOX;
				// svg.PreserveAspectRatio = PreserveAspectRatioType.None;
				// svg.Clip = string.Format("rect({0},{1},{2},{3})", 0, VIEWBOX.Width, VIEWBOX.Height, 0);

				double viewPortWidth = svg.DomElement.ClientWidth;
				double viewPortHeight = svg.DomElement.ClientHeight;

				//SvgRectangle background = new SvgRectangle( svg, 0, 0, viewPortWidth, viewPortHeight, null );
				//background.Fill = "salmon";
				//background.Opacity = 0.2;

				/*
					<g id="group1" fill="red">
						<rect x="1cm" y="1cm" width="1cm" height="1cm"/>
						<rect x="3cm" y="1cm" width="1cm" height="1cm"/>
					  </g>
					  <g id="group2" fill="blue">
						<rect x="1cm" y="3cm" width="1cm" height="1cm"/>
						<rect x="3cm" y="3cm" width="1cm" height="1cm"/>
					  </g>
				 */
				double blockSize = 100;
				double vGap = ( viewPortHeight - 3 * blockSize ) / 4;
				double hGap = ( viewPortWidth - 2 * blockSize ) / 3;

				Action<SvgGroup> addContent = delegate( SvgGroup g ) {
					double strokeWidth = 1;
					string stroke = "black";

					SvgLine l = new SvgLine( g, 0, 0, 350, 0, null );
					l.Stroke = stroke;
					l.StrokeWidth = strokeWidth;

					l = new SvgLine( g, 0, 0, 0, 350, null );
					l.Stroke = stroke;
					l.StrokeWidth = strokeWidth;

					new SvgRectangle( g, 200, vGap, blockSize, blockSize, null );

					new SvgRectangle( g, 600, vGap, blockSize, blockSize, null );
				};


				SvgGroup g1 = new SvgGroup( svg, null );
				g1.Fill = "red";
				Assert.StringsEqual( g1.Fill, "red", T + ": g1.Fill" );

				SvgGroup g2 = new SvgGroup( svg, null );
				g2.Fill = "blue";
				Assert.StringsEqual( g2.Fill, "blue", T + ": g1.Fill" );

				SvgGroup g3 = new SvgGroup( svg, null );
				g3.Fill = "yellow";

				addContent( g1 );
				addContent( g2 );
				addContent( g3 );

				g1.TransformProperty = Transform.TranslateXY( 0, 0 );
				g2.TransformProperty = Transform.TranslateXY( 0, vGap + blockSize );
				g3.TransformProperty = Transform.TranslateXY( 0, 2 * ( vGap + blockSize ) ) + Transform.RotateXY( 30, g3[2].BoundingBox.X, g3[2].BoundingBox.Y );

				Util.AppendResult( T, true );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": exception caught -- " + ex.StackTrace, false );
			}
		}

		static void DrawLine() {
			const string T = "DrawLine";

			try {
				SvgSvg svg = Svg.CreateOuterSvgElement( HtmlUtil.CreateElement( "div", Document.Body, null ), null );
				svg.Width = SVG_WIDTH;
				svg.Height = SVG_HEIGHT;
				svg.ViewBox = VIEWBOX;
				svg.PreserveAspectRatio = PreserveAspectRatioType.None;

				new SvgRectangle( svg, 0, 0, VIEWBOX.Width, VIEWBOX.Height, null ).Fill = "yellow";

				const double x1 = 100;
				const double y1 = 175;
				const double x2 = VIEWBOX_WIDTH - 100;
				const double y2 = VIEWBOX_HEIGHT - 67.5;
				const double strokeWidth = 40;
				double[] strokeArray = { strokeWidth * 3, strokeWidth * 2 };

				SvgLine line = new SvgLine( svg, x1, y1, x2, y2, null );
				line.Stroke = "black";
				line.StrokeWidth = strokeWidth;
				line.StrokeLinecap = LineCapType.round;
				line.StrokeDashArray = strokeArray;

				Assert.NumbersEqual( x1, line.X1, T + ": X1 coordinate not correct." );
				Assert.NumbersEqual( y1, line.Y1, T + ": Y1 coordinate not correct." );
				Assert.NumbersEqual( x2, line.X2, T + ": X2 coordinate not correct." );
				Assert.NumbersEqual( y2, line.Y2, T + ": Y2 coordinate not correct." );
				Assert.NumbersEqual( x2 - x1, line.Width, T + ": WIDTH not correct." );
				Assert.NumbersEqual( y2 - y1, line.Height, T + ": HEIGHT not correct." );
				Assert.ArraysEqual( strokeArray, line.StrokeDashArray, T + ": StrokeDashArray not correct." );

				Util.AppendResult( T, true );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": exception caught -- " + ex.StackTrace, false );
			}
		}

		static void DisplayText() {
			const string CodeBehindSubclass = "DisplayText";
			try {
				const string SvgWidth = "15cm";
				const string SvgHeight = "5cm";
				const string VIEWBOX = "0 0 1000 1000";

				SvgSvg svg = Svg.CreateOuterSvgElement( HtmlUtil.CreateElement( "div", Document.Body, null ),
					new Dictionary<string, string>(
						"width", SvgWidth,
						"height", SvgHeight,
						"viewBox", VIEWBOX
					) );

				SystemQut.Svg.SvgText text = new SvgText( svg, new Dictionary<string,string>(
					"font-family", "Serif",
					"font-size", Svg.ConvertLength( 0.5, 5, 1000 ).ToString()
				) );
				text.Text = "Hello World!";

				Util.AppendResult( CodeBehindSubclass, true );
			}
			catch ( Exception ex ) {
				Util.AppendResult( CodeBehindSubclass + ": exception caught -- " + ex.StackTrace, false );
			}
		}

        static void DrawPath() {
			const string T = "DrawPath";

			try {
				SvgSvg svg = Svg.CreateOuterSvgElement( HtmlUtil.CreateElement( "div", Document.Body, null ), null );
				svg.Width = SVG_WIDTH;
				svg.Height = SVG_HEIGHT;
				svg.ViewBox = VIEWBOX;
				svg.PreserveAspectRatio = PreserveAspectRatioType.None;

				new SvgRectangle( svg, 0, 0, VIEWBOX.Width, VIEWBOX.Height, null ).Fill = "yellow";

				//const double x1 = 100;
				//const double y1 = 175;
				//const double x2 = VIEWBOX_WIDTH - 100;
				//const double y2 = VIEWBOX_HEIGHT - 67.5;
				const double strokeWidth = 20;
				double[] strokeArray = { strokeWidth * 5, strokeWidth * 1 };

				SvgPath path = new SvgPath( svg, null );

                path.MoveTo(150, 0, true)
					.LineTo(75, 200, true)
					.LineTo(225, 200, true)
					.EllipticalArcTo( 100, 45, 0, false, false, 275, 300, true );

				path.Stroke = "green";
				path.StrokeWidth = strokeWidth;
				path.StrokeLinecap = LineCapType.round;
				path.StrokeDashArray = strokeArray;

				path.TransformProperty = Transform.Rotate( 45 );

				//Assert.NumbersEqual( x1, path.X1, T + ": X1 coordinate not correct." );
				//Assert.NumbersEqual( y1, path.Y1, T + ": Y1 coordinate not correct." );
				//Assert.NumbersEqual( x2, path.X2, T + ": X2 coordinate not correct." );
				//Assert.NumbersEqual( y2, path.Y2, T + ": Y2 coordinate not correct." );
				//Assert.NumbersEqual( x2 - x1, path.Width, T + ": WIDTH not correct." );
				//Assert.NumbersEqual( y2 - y1, path.Height, T + ": HEIGHT not correct." );
				//Assert.ArraysEqual( strokeArray, path.StrokeDashArray, T + ": StrokeDashArray not correct." );

				Util.AppendResult( T, true );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": exception caught -- " + ex.StackTrace, false );
			}
		}
	}
}
