﻿using System;
using System.Collections.Generic;
using SystemQut.ComponentModel;

namespace Test.Client {
	public class ChemicalElement : INotifyPropertyChanged {
		private bool isSelected;
		private string name;
		private string symbol;
		private double weight;

		public ChemicalElement ( string name, string symbol, double weight ) {
			this.name = name;
			this.symbol = symbol;
			this.weight = weight;
		}

		public static ChemicalElement[] Elements {
			get {
				return new ChemicalElement[] { 
					new ChemicalElement( "Hydrogen", "H", 1.008 ),
					new ChemicalElement( "Helium", "He", 4.0026 ),
					new ChemicalElement( "Lithium", "Li", 6.94 ),
					new ChemicalElement( "Beryllium", "Be", 9.0122 ),
					new ChemicalElement( "Boron", "B", 10.81 ),
					new ChemicalElement( "Carbon", "C", 12.011 ),
					new ChemicalElement( "Nitrogen", "N", 14.007 ),
					new ChemicalElement( "Oxygen", "O", 15.999 ),
					new ChemicalElement( "Fluorine", "F", 18.998 ),
					new ChemicalElement( "Neon", "Ne", 20.180 ),
				};
			}
		}

		/// <summary> PropertyChanged event handler.
		/// </summary>

		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary> Fire PropertyChanged events.
		/// </summary>
		/// <param name="propertyNames">
		///		A comma-separated list of properties that have new values.
		/// </param>

		private void NotifyPropertyChanged ( string propertyNames ) {
			if ( PropertyChanged != null ) {
				PropertyChanged( this, new PropertyChangedEventArgs( propertyNames ) );
			}
		}

		public bool IsSelected {
			get {
				return isSelected;
			}
			set {
				if ( isSelected == value ) return;

				isSelected = value;
				NotifyPropertyChanged( "IsSelected" );
			}
		}

		public string Name {
			get { return name; }
			set {
				if ( value == name ) return;
				name = value;
				NotifyPropertyChanged( "Name" );
			}
		}

		public string Symbol {
			get { return symbol; }
			set { 
				if ( value == symbol ) return;
				symbol = value;
				NotifyPropertyChanged( "Symbol" );
			}
		}

		public double Weight {
			get { return weight; }
			set { 
				if ( value == weight ) return;
				weight = value;
				NotifyPropertyChanged( "Weight" );
			}
		}

		public object Clone () {
			return new ChemicalElement( name, symbol, weight );
		}

		public override string ToString () {
			return string.Format( "[{0},{1},{2},{3}]", isSelected, name, symbol, weight );
		}

		public bool Equals ( ChemicalElement other ) {
			return this.name == other.name && this.symbol == other.symbol && this.weight == other.weight;
		}
	}
}
