// TestSvg.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Svg;
using SystemQut;
using D3JS;
using SystemQut.Css;
using System.Serialization;
using Test.Client.Controls;

namespace Test.Client {

	public class TestD3 {
		public readonly Action[] Tests;

		public TestD3 () {
			Tests = new Action[] {
				Select_01_ThreeDivsInDocument,
				Select_02_ThreeDivsInDocument,
				Select_03_ClassedReturnsTrue,
				Select_04_ClassedReturnsFalse,
				Select_05_ClassedSetClass,
				Select_06_ClassedUnsetClass,
				Select_07_ClassedSetClassViaFunction,
				Select_08_ClassedUnsetClassViaFunction,
				Select_09_GettingStartedExample01a,
				Select_10_GettingStartedExample01b,
				Select_11_GettingStartedExample01c,
				Select_12_GettingStartedExample01d,
			};
		}

		void Select_01_ThreeDivsInDocument () {
			const string T = "Select_01_ThreeDivsInDocument";
			try {
				const int N = 3;

				for ( int i = 0; i < N; i++ ) {
#pragma warning disable 618
					HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
						"class", T
					) );
#pragma warning restore 618
				}

				Selection sel = D3.Current.SelectAll( new ClassQualifier( T ).ToString() );

				Util.AppendResult( T, sel[0].Length == N );
			}
			catch {
				Util.AppendResult( T, false );
			}
		}

		void Select_02_ThreeDivsInDocument () {
			const string T = "Select_02_ThreeDivsInDocument";
			try {
				const int N = 3;
				List<Element> divs = new List<Element>();

				for ( int i = 0; i < N; i++ ) {
#pragma warning disable 618
					divs.Add( HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
						"class", T
					) ) );
#pragma warning restore 618
				}

				Selection sel = D3.Current.SelectAll( new ClassQualifier( T ).ToString() );
				sel.Each<object>( delegate( object d, int i ) {
					( (Element) Script.This ).TextContent = T + ": section " + i;
				} );

				for ( int i = 0; i < N; i++ ) {
					string text = T + ": section " + i;
					if ( divs[i].TextContent != text ) Util.AppendResult( text, false );
				}

				Util.AppendResult( T, true );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_03_ClassedReturnsTrue () {
			const string T = "Select_03_ClassedReturnsTrue";
			try {
				const int N = 1;

				for ( int i = 0; i < N; i++ ) {
#pragma warning disable 618
					HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
						"class", "something " + T + " somethingElse"
					) ).TextContent = T + " test element.";
#pragma warning restore 618
				}

				Selection sel = D3.Current.SelectAll( new ClassQualifier( T ).ToString() );

				Util.AppendResult( T, sel.Size() == 1 );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_04_ClassedReturnsFalse () {
			const string T = "Select_04_ClassedReturnsFalse";
			try {
				const int N = 1;

				for ( int i = 0; i < N; i++ ) {
#pragma warning disable 618
					HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
						"class", "something " + T + " somethingElse"
					) ).TextContent = T + " test element.";
#pragma warning restore 618
				}

				Selection sel = D3.Current.SelectAll( new ClassQualifier( "baboon" ).ToString() );

				Util.AppendResult( T, sel.IsEmpty() );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_05_ClassedSetClass () {
			const string T = "Select_05_ClassedSetClass";
			try {
				const int N = 3;
				const string className = "bamboo";

				for ( int i = 0; i < N; i++ ) {
					string classes = "something " + T + " somethingElse" + ( i == N ? " " + className : "" );
					string innerText = T + " test element in class '" + classes + "'";
#pragma warning disable 618
					HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
						"class", classes
					) ).TextContent = innerText;
#pragma warning restore 618
				}

				Selection sel = D3.Current.SelectAll( new ClassQualifier( T ).ToString() );

				sel.Classed( className, true );

				int correctCount = 0;

				sel.Each( delegate( object datum, int index ) {
					if ( ( (Element) Script.This ).ClassList.Contains( className ) ) correctCount++;
				} );

				Util.AppendResult( T, correctCount == N );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_06_ClassedUnsetClass () {
			const string T = "Select_06_ClassedUnsetClass";
			try {
				const int N = 3;
				const string className = "bamboo";

				for ( int i = 0; i < N; i++ ) {
					string classes = "something " + T + " somethingElse" + ( i == N ? " " + className : "" );
					string innerText = T + " test element in class '" + classes + "'";
#pragma warning disable 618
					HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
						"class", classes
					) ).TextContent = innerText;
#pragma warning restore 618
				}

				Selection sel = D3.Current.SelectAll( new ClassQualifier( T ).ToString() );

				sel.Classed( className, false );

				int incorrectCount = 0;

				sel.Each( delegate( object datum, int index ) {
					if ( ( (Element) Script.This ).ClassList.Contains( className ) ) incorrectCount++;
				} );

				Util.AppendResult( T, incorrectCount == 0 );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_07_ClassedSetClassViaFunction () {
			const string T = "Select_07_ClassedSetClassViaFunction";
			try {
				const int N = 3;
				const string className = "bamboo";

				for ( int i = 0; i < N; i++ ) {
					string classes = "something " + T + " somethingElse";
					string innerText = T + " test element in class '" + classes + "'";
#pragma warning disable 618
					HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
						"class", classes
					) ).TextContent = innerText;
#pragma warning restore 618
				}

				Selection sel = D3.Current.SelectAll( new ClassQualifier( T ).ToString() );

				sel.Classed( className, delegate( object datum, int index ) {
					return index == N - 1;
				} );

				int correctCount = 0;
				int expectedCount = 1;

				sel.Each( delegate( object datum, int index ) {
					if ( ( (Element) Script.This ).ClassList.Contains( className ) ) correctCount++;
				} );

				Util.AppendResult( T, correctCount == expectedCount );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_08_ClassedUnsetClassViaFunction () {
			const string T = "Select_08_ClassedUnsetClassViaFunction";
			try {
				const int N = 3;
				const string className = "bamboo";

				for ( int i = 0; i < N; i++ ) {
					string classes = "something " + T + " somethingElse";
					string innerText = T + " test element in class '" + classes + "'";
#pragma warning disable 618
					HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
						"class", classes
					) ).TextContent = innerText;
#pragma warning restore 618
				}

				Selection sel = D3.Current.SelectAll( new ClassQualifier( T ).ToString() );

				sel.Classed( className, delegate( object datum, int index ) {
					return index != N - 1;
				} );

				int correctCount = 0;
				int expectedCount = N - 1;

				sel.Each( delegate( object datum, int index ) {
					if ( ( (Element) Script.This ).ClassList.Contains( className ) ) correctCount++;
				} );

				Util.AppendResult( T, correctCount == expectedCount );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_09_GettingStartedExample01a () {
			const string T = "Select_09_GettingStartedExample01";

			try {
				const string contentId = T + "_ContentHolder";

#pragma warning disable 618
				Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					"id", contentId
				) );
#pragma warning restore 618

				ChemicalElement[] data = { 
					new ChemicalElement( "Hydrogen", "H", 1.008 ),
					new ChemicalElement( "Helium", "He", 4.0026 ),
					new ChemicalElement( "Lithium", "Li", 6.94 ),
					new ChemicalElement( "Beryllium", "Be", 9.0122 ),
					new ChemicalElement( "Boron", "B", 10.81 ),
					new ChemicalElement( "Carbon", "C", 12.011 ),
					new ChemicalElement( "Nitrogen", "N", 14.007 ),
					new ChemicalElement( "Oxygen", "O", 15.999 ),
					new ChemicalElement( "Fluorine", "F", 18.998 ),
					new ChemicalElement( "Neon", "Ne", 20.180 ),
				};

				D3.Current.Select( new IdentQualifier( contentId ).ToString() )
					.Append( "ul" )
					.SelectAll( "li" )
					.Data( data )
					.Enter()
					.Append( "li" )
					.Text( delegate( ChemicalElement element, int index ) {
					return element.Name + ": " + element.Symbol + "(" + element.Weight + ")";
				} );

				string expected = @"<ul><li>Hydrogen: H(1.008)</li><li>Helium: He(4.0026)</li><li>Lithium: Li(6.94)</li><li>Beryllium: Be(9.0122)</li><li>Boron: B(10.81)</li><li>Carbon: C(12.011)</li><li>Nitrogen: N(14.007)</li><li>Oxygen: O(15.999)</li><li>Fluorine: F(18.998)</li><li>Neon: Ne(20.18)</li></ul>";
				string actual = contentHolder.InnerHTML;

				Util.AppendResult( T, expected == actual );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_10_GettingStartedExample01b () {
			const string T = "Select_10_GettingStartedExample01";

			try {
				const string contentId = T + "_ContentHolder";

#pragma warning disable 618
				Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					"id", contentId
				) );
#pragma warning restore 618

				ChemicalElement[] data = { 
					new ChemicalElement( "Hydrogen", "H", 1.008 ),
					new ChemicalElement( "Helium", "He", 4.0026 ),
					new ChemicalElement( "Lithium", "Li", 6.94 ),
					new ChemicalElement( "Beryllium", "Be", 9.0122 ),
					new ChemicalElement( "Boron", "B", 10.81 ),
					new ChemicalElement( "Carbon", "C", 12.011 ),
					new ChemicalElement( "Nitrogen", "N", 14.007 ),
					new ChemicalElement( "Oxygen", "O", 15.999 ),
					new ChemicalElement( "Fluorine", "F", 18.998 ),
					new ChemicalElement( "Neon", "Ne", 20.180 ),
				};

				Selection listItems = D3.Current.Select( new IdentQualifier( contentId ).ToString() )
					.Append( "ul" )
					.SelectAll( "li" )
					.Data( data )
					.Enter()
					.Append( "li" )
					.Text( delegate( ChemicalElement datum, int index ) {
					return datum.Name + ": " + datum.Symbol + "(" + datum.Weight + ")";
				} );

				listItems.Style( "font-weight", delegate( ChemicalElement datum, int index ) {
					return datum.Name.StartsWith( "H" ) ? "bold" : "normal";
				} );

				string expected = @"<ul><li style=""font-weight: bold;"">Hydrogen: H(1.008)</li><li style=""font-weight: bold;"">Helium: He(4.0026)</li><li style=""font-weight: normal;"">Lithium: Li(6.94)</li><li style=""font-weight: normal;"">Beryllium: Be(9.0122)</li><li style=""font-weight: normal;"">Boron: B(10.81)</li><li style=""font-weight: normal;"">Carbon: C(12.011)</li><li style=""font-weight: normal;"">Nitrogen: N(14.007)</li><li style=""font-weight: normal;"">Oxygen: O(15.999)</li><li style=""font-weight: normal;"">Fluorine: F(18.998)</li><li style=""font-weight: normal;"">Neon: Ne(20.18)</li></ul>";
				string actual = contentHolder.InnerHTML;

				Util.AppendResult( T, expected == actual );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_11_GettingStartedExample01c () {
			const string T = "Select_11_GettingStartedExample01";

			try {
				const string contentId = T + "_ContentHolder";

#pragma warning disable 618
				Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					"id", contentId
				) );
#pragma warning restore 618

				ChemicalElement[] data = { 
					new ChemicalElement( "Hydrogen", "H", 1.008 ),
					new ChemicalElement( "Helium", "He", 4.0026 ),
					new ChemicalElement( "Lithium", "Li", 6.94 ),
					new ChemicalElement( "Beryllium", "Be", 9.0122 ),
					new ChemicalElement( "Boron", "B", 10.81 ),
					new ChemicalElement( "Carbon", "C", 12.011 ),
					new ChemicalElement( "Nitrogen", "N", 14.007 ),
					new ChemicalElement( "Oxygen", "O", 15.999 ),
					new ChemicalElement( "Fluorine", "F", 18.998 ),
					new ChemicalElement( "Neon", "Ne", 20.180 ),
				};

				Selection tables = D3.Current.Select( new IdentQualifier( contentId ).ToString() )
					.Append( "table" );

				tables.SelectAll( "tr" )
					.Data( data )
					.Enter()
					.Append( delegate( ChemicalElement datum, int index ) {
					Element row = HtmlUtil.CreateElement( "tr", null, null );
					HtmlUtil.CreateElement( "td", row, null ).TextContent = datum.Name;
					HtmlUtil.CreateElement( "td", row, null ).TextContent = datum.Symbol;
					HtmlUtil.CreateElement( "td", row, null ).TextContent = datum.Weight.ToString();
					return row;
				} );

				tables.SelectAll( "tr" ).Select( "td" ).Style( "font-weight", delegate( ChemicalElement datum, int index ) {
					return datum.Name.StartsWith( "H" ) ? "bold" : "normal";
				} );

				Element heading = HtmlUtil.CreateElement( "tr", null, null );

#pragma warning disable 618
				HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
					"style", "font-weight: bold;"
				) ).TextContent = "Name";

				HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
					"style", "font-weight: bold;"
				) ).TextContent = "Symbol";

				HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
					"style", "font-weight: bold;"
				) ).TextContent = "Atomic Weight";
#pragma warning restore 618

				TableElement table = (TableElement) tables[0][0];

				table.InsertBefore( heading, tables.Select( "tr" )[0][0] );
				table.SetAttribute( "border", "1" );
				table.SetAttribute( "cellpadding", "3" );
				table.SetAttribute( "cellspacing", "0" );

				string expected = @"<tr><td style=""font-weight: bold;"">Name</td><td style=""font-weight: bold;"">Symbol</td><td style=""font-weight: bold;"">Atomic Weight</td></tr><tr><td style=""font-weight: bold;"">Hydrogen</td><td>H</td><td>1.008</td></tr><tr><td style=""font-weight: bold;"">Helium</td><td>He</td><td>4.0026</td></tr><tr><td style=""font-weight: normal;"">Lithium</td><td>Li</td><td>6.94</td></tr><tr><td style=""font-weight: normal;"">Beryllium</td><td>Be</td><td>9.0122</td></tr><tr><td style=""font-weight: normal;"">Boron</td><td>B</td><td>10.81</td></tr><tr><td style=""font-weight: normal;"">Carbon</td><td>C</td><td>12.011</td></tr><tr><td style=""font-weight: normal;"">Nitrogen</td><td>N</td><td>14.007</td></tr><tr><td style=""font-weight: normal;"">Oxygen</td><td>O</td><td>15.999</td></tr><tr><td style=""font-weight: normal;"">Fluorine</td><td>F</td><td>18.998</td></tr><tr><td style=""font-weight: normal;"">Neon</td><td>Ne</td><td>20.18</td></tr>";
				string actual = table.InnerHTML;

				Util.AppendResult( T, expected == actual );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}

		void Select_12_GettingStartedExample01d () {
			const string T = "Select_12_GettingStartedExample01d";

			try {
				const string contentId = T + "_ContentHolder";

#pragma warning disable 618
				Element contentHolder = HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					"id", contentId
				) );
#pragma warning restore 618

				ChemicalElement[] data = { 
					new ChemicalElement( "Hydrogen", "H", 1.008 ),
					new ChemicalElement( "Helium", "He", 4.0026 ),
					new ChemicalElement( "Lithium", "Li", 6.94 ),
					new ChemicalElement( "Beryllium", "Be", 9.0122 ),
					new ChemicalElement( "Boron", "B", 10.81 ),
					new ChemicalElement( "Carbon", "C", 12.011 ),
					new ChemicalElement( "Nitrogen", "N", 14.007 ),
					new ChemicalElement( "Oxygen", "O", 15.999 ),
					new ChemicalElement( "Fluorine", "F", 18.998 ),
					new ChemicalElement( "Neon", "Ne", 20.180 ),
				};


				Selection tables = D3.Current.Select( new IdentQualifier( contentId ).ToString() )
					.Append( "table" );

				tables.SelectAll( "tr" )
					.Data( data )
					.Enter()
					.Append( delegate( ChemicalElement datum, int index ) {
					Element row = HtmlUtil.CreateElement( "tr", null, null );

					Element firstCol = HtmlUtil.CreateElement( "td", row, null );
#pragma warning disable 618
					HtmlUtil.CreateElement( "input", firstCol, new Dictionary<string, string>(
						"type", "checkbox"
					) );
#pragma warning restore 618

					HtmlUtil.CreateElement( "td", row, null ).TextContent = datum.Name;
					HtmlUtil.CreateElement( "td", row, null ).TextContent = datum.Symbol;
					HtmlUtil.CreateElement( "td", row, null ).TextContent = datum.Weight.ToString();
					return row;
				} );

				tables.SelectAll( "tr" ).Select( "td" ).Style( "font-weight", delegate( ChemicalElement datum, int index ) {
					return datum.Name.StartsWith( "H" ) ? "bold" : "normal";
				} );

				tables.SelectAll( "tr" ).Select( "input" ).Property( "checked", delegate( ChemicalElement datum, int index ) {
					return index % 3 == 0;
				} );

				Element heading = HtmlUtil.CreateElement( "tr", null, null );
				HtmlUtil.CreateElement( "td", heading, null );

#pragma warning disable 618
				HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
					"style", "font-weight: bold;"
				) ).TextContent = "Name";

				HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
					"style", "font-weight: bold;"
				) ).TextContent = "Symbol";

				HtmlUtil.CreateElement( "td", heading, new Dictionary<string, string>(
					"style", "font-weight: bold;"
				) ).TextContent = "Atomic Weight";
#pragma warning restore 618

				TableElement table = (TableElement) tables[0][0];

				table.InsertBefore( heading, tables.Select( "tr" )[0][0] );
				table.SetAttribute( "border", "1" );
				table.SetAttribute( "cellpadding", "3" );
				table.SetAttribute( "cellspacing", "0" );

				string expected = @"<tr><td></td><td style=""font-weight: bold;"">Name</td><td style=""font-weight: bold;"">Symbol</td><td style=""font-weight: bold;"">Atomic Weight</td></tr><tr><td style=""font-weight: bold;""><input type=""checkbox""></td><td>Hydrogen</td><td>H</td><td>1.008</td></tr><tr><td style=""font-weight: bold;""><input type=""checkbox""></td><td>Helium</td><td>He</td><td>4.0026</td></tr><tr><td style=""font-weight: normal;""><input type=""checkbox""></td><td>Lithium</td><td>Li</td><td>6.94</td></tr><tr><td style=""font-weight: normal;""><input type=""checkbox""></td><td>Beryllium</td><td>Be</td><td>9.0122</td></tr><tr><td style=""font-weight: normal;""><input type=""checkbox""></td><td>Boron</td><td>B</td><td>10.81</td></tr><tr><td style=""font-weight: normal;""><input type=""checkbox""></td><td>Carbon</td><td>C</td><td>12.011</td></tr><tr><td style=""font-weight: normal;""><input type=""checkbox""></td><td>Nitrogen</td><td>N</td><td>14.007</td></tr><tr><td style=""font-weight: normal;""><input type=""checkbox""></td><td>Oxygen</td><td>O</td><td>15.999</td></tr><tr><td style=""font-weight: normal;""><input type=""checkbox""></td><td>Fluorine</td><td>F</td><td>18.998</td></tr><tr><td style=""font-weight: normal;""><input type=""checkbox""></td><td>Neon</td><td>Ne</td><td>20.18</td></tr>";
				string actual = table.InnerHTML;

				Util.AppendResult( T, expected == actual );
			}
			catch ( Exception ex ) {
				Util.AppendResult( T + ": " + ex.Message, false );
			}
		}
	}
}
