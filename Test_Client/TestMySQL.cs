﻿using System;
using System.Collections.Generic;
//using MySql.Data.MySqlClient;
using RegPrecise;
using SystemQut;
using System.Serialization;
using SystemQut.ServiceModel;
using SystemQut.IO;
using System.Html.Data.Files;
using System.Html;
using SystemQUT.Csv;
using SystemQut.Linq;
using Test.Client.Controls;

namespace Test.Client
{
    public class TestMySQL {
        /*static void TestMySQLConnection () {
            MySqlConnection connection = null;
            string connectionString = @"server=localhost;userid=gene_ontology_access;password=GeneOntology1;database=gene_ontology";

            try {
                connection = new MySqlConnection(connectionString);
                connection.Open();
                Console.Log("MySQL version: " + connection.ServerVersion);
            } catch (Exception ex) {
                Console.Log("Error: " +  ex.ToString());
            } finally {
                if (connection != null) {
                    connection.Close();
                }
            }
        }*/

		static ILocalMySQLService service = new LocalMySQLService( "/LocalMySQL.svc", false );

        static void TestListTerms() {
			string T = "ListTerms";
			service.GO_ListTerms( delegate( ServiceResponse<GOTerm[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				//ReadRecords( "/UnitTests/Resources/GOTerms.json", delegate( JSObject[] rawData ) {
					try {
                        int expectedNum = 44426;
						GOTerm[] actual = response.Content;

                        Assert.IsTrue(actual != null, T + ": " + "actual != null" );

						actual.Sort<GOTerm>( GOTerm.OrderById );

                        /*Console.Log("Checking terms in the received object.");

						foreach (GOTerm term in actual) {
                            Console.Log("'" + term.Name + "' is present, with the following attributes: id - '" + term.DatabaseId
                                + "', term_type - '" + term.Namespace
                                + "', acc - '" + term.UniqueIdentifier
                                + "', is_obsolete - '" + term.Obsolete
                                + "', is_root - '" + term.Root
                                + "', is_relation - '" + term.Relation
                                + "'");
                        }*/

                        Console.Log(actual.Length + " terms all up.");

                        Assert.NumbersEqual(expectedNum, actual.Length, T + " actual != the expected number of terms, " + expectedNum);

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				//} );
			} );
            Console.Log("Reached the end of " + T);
        }

        static void TestListSynonymsOfTerm () {
			string T = "ListSynonymsOfTerm";
            int termId = 93;
			service.GO_ListSynonymsOfTerm( termId, delegate( ServiceResponse<GOTermSynonym[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

                try {                
                    int expectedNum = 5;
					GOTermSynonym[] actual = response.Content;

                    Assert.IsTrue(actual != null, T + ": " + "actual != null" );

					actual.Sort<GOTermSynonym>( GOTermSynonym.OrderById );

                    /*Console.Log("Checking synonyms in the received object.");

					foreach (GOTermSynonym synonym in actual) {
                        Console.Log("'" + synonym.Name + "' is present, with the following attributes: term_id - '" + synonym.TermDatabaseId
                            + "', acc_synonym - '" + synonym.UniqueIdentifier
                            + "', synonym_type_id - '" + synonym.TypeId
                            + "', synonym_category_id - '" + synonym.CategoryId
                            + "'");
                    }*/
                    Console.Log(actual.Length + " synonyms all up.");

                    Assert.NumbersEqual(expectedNum, actual.Length, T + " actual != the expected number of synonyms, " + expectedNum);

				    Util.AppendResult( T, true );
			    }
			    catch ( Exception ex ) {
				    Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				    Console.Log( ex.StackTrace );
			    }
            });
            Console.Log("Reached the end of " + T);
        }

        static void TestListRelationshipsOfTermAsChild() {
			string T = "ListRelationshipsOfTermAsChild";
            int termId = 93;
			service.GO_ListRelationshipsOfTermAsChild( termId, delegate( ServiceResponse<GOTermRelationship[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

                try {                
                    int expectedNum = 2;
					GOTermRelationship[] actual = response.Content;

                    Assert.IsTrue(actual != null, T + ": " + "actual != null" );

                    /* Console.Log("Checking relationships in the received object.");

					foreach (GOTermRelationship relationship in actual) {
                        Console.Log("'ID " + relationship.DatabaseId + "' is present, with the following attributes: relationship_type_id - '" + relationship.RelationshipDatabaseId
                            + "', term1_id - '" + relationship.ParentDatabaseId
                            + "', term2_id - '" + relationship.ChildDatabaseId
                            + "', complete - '" + relationship.Complete
                            + "'");
                    }*/
                    Console.Log(actual.Length + " relationships all up.");

                    Assert.NumbersEqual(expectedNum, actual.Length, T + " actual != the expected number of relationships, " + expectedNum);

				    Util.AppendResult( T, true );
			    }
			    catch ( Exception ex ) {
				    Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				    Console.Log( ex.StackTrace );
			    }
            });
            Console.Log("Reached the end of " + T);
        }

        static void TestGetTerm() {
			string T = "GetTerm";
            int termId = 93;
			service.GO_GetTerm( termId, delegate( ServiceResponse<GOTerm> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

                try {                
                    GOTerm expected = new GOTerm(93, "protein import into nucleus, docking", "biological_process", "GO:0000059", 0, 0 ,0, null, null);
					GOTerm actual = response.Content;

                    Assert.IsTrue(actual != null, T + ": " + "actual != null" );

                    /*Console.Log("Checking the received object.");

					Console.Log("'" + actual.Name + "' was recieved, with the following attributes: id - '" + actual.DatabaseId
                                + "', term_type - '" + actual.Namespace
                                + "', acc - '" + actual.UniqueIdentifier
                                + "', is_obsolete - '" + actual.Obsolete
                                + "', is_root - '" + actual.Root
                                + "', is_relation - '" + actual.Relation
                                + "'");*/

                    Assert.NumbersEqual(expected.DatabaseId, actual.DatabaseId, T + " actual does not have the expected database ID, " + expected.DatabaseId);
                    Assert.StringsEqual(expected.Name, actual.Name, T + " actual does not have the expected value for name, " + expected.Name);
                    Assert.StringsEqual(expected.Namespace, actual.Namespace, T + " actual does not have the expected value for term_type, " + expected.Namespace);
                    Assert.StringsEqual(expected.UniqueIdentifier, actual.UniqueIdentifier, T + " actual does not have the expected value for acc, " + expected.UniqueIdentifier);
                    Assert.IsTrue(expected.Obsolete == actual.Obsolete, T + " actual does not have the expected value for is_obsolete, " + expected.Obsolete);
                    Assert.IsTrue(expected.Root == actual.Root, T + " actual does not have the expected value for is_root, " + expected.Root);
                    Assert.IsTrue(expected.Relation == actual.Relation, T + " actual does not have the expected value for is_relation, " + expected.Relation);

				    Util.AppendResult( T, true );
			    }
			    catch ( Exception ex ) {
				    Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				    Console.Log( ex.StackTrace );
			    }
            });
            Console.Log("Reached the end of " + T);
        }

        // Original attempt to call ListSynonymsOfTerm for each term. It doesn't work as it seems
        // the code after the call to that service function runs before the foreach loop is complete
        // which of course causes errors
        static void TestGetAllSynonyms() {
			string T = "GetAllSynonyms";
			service.GO_ListTerms( delegate( ServiceResponse<GOTerm[]> response_1 ) {
				Assert.IsTrue( response_1.Error == null, T + ": " + "response_1.Error == null" );

				try {
					GOTerm[] actual_1 = response_1.Content;

                    Assert.IsTrue(actual_1 != null, T + ": " + "actual_1 != null" );

					actual_1.Sort<GOTerm>( GOTerm.OrderById );

                    //I limited it to the first 1000 here to limit the amount of calls
					for (int i = 0; i < 1000; i++) {
                        service.GO_ListSynonymsOfTerm(actual_1[i].DatabaseId, delegate(ServiceResponse<GOTermSynonym[]> response_2 ) {
				            Assert.IsTrue( response_2.Error == null, T + ": " + "response_2.Error == null" );
                            GOTermSynonym[] actual_2 = response_2.Content;
                            if (actual_2 != null) {
                                actual_2.Sort<GOTermSynonym>( GOTermSynonym.OrderById );
                                foreach (GOTermSynonym synonym in actual_2) {                                    
                                    foreach (GOTerm term in actual_1) {
                                        if (term.DatabaseId == synonym.TermDatabaseId) {
                                            Console.Log("Adding " + synonym.Name + " to " + term.UniqueIdentifier + "'s list of synonyms.");
                                            term.Synonyms.Add(synonym);
                                            break;
                                        }
                                    }
                                }
                            }

                        } );

                        /*if (actual_1[i].Synonyms.Length > 0) {
                            Console.Log("'" + actual_1[i].Name + "' has '" + actual_1[i].Synonyms.Length + "' synonyms.");
                        } else {
                            Console.Log("'"  + actual_1[i].Name + "' has no synonyms.");
                        }*/
                    }

					Util.AppendResult( T, true );
				}
				catch ( Exception ex ) {
					Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					Console.Log( ex.StackTrace );
				}
			} );
            Console.Log("Reached the end of " + T);
        }

        // The attempt to do this task with a new service call, one that retrieves all the terms
        // and their synonyms at once. It works, but for some reason the lists of synonyms for
        // each GOTerm object end up as a list of unspecific objects, rather than a GOTermSynonym
        static void TestListTermsWithSynonyms() {
			string T = "ListTermsWithSynonyms";
			service.GO_ListTermsWithSynonyms( delegate( ServiceResponse<GOTerm[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				try {
                    int expectedNumTerms = 44426;
                    int expectedNumSynonyms = 120700;
					GOTerm[] actual = response.Content;
                    int actualNumSynonyms = 0;

                    Assert.IsTrue(actual != null, T + ": " + "actual != null" );

					actual.Sort<GOTerm>( GOTerm.OrderByDatabaseId );

                    Console.Log("Checking terms and synonyms in the received object.");

					foreach (GOTerm term in actual) {
                        /*Console.Log("'" + term.Name + "' (term) is present, with the following attributes: id - '" + term.DatabaseId
                            + "', term_type - '" + term.Namespace
                            + "', acc - '" + term.UniqueIdentifier
                            + "', is_obsolete - '" + term.Obsolete
                            + "', is_root - '" + term.Root
                            + "', is_relation - '" + term.Relation
                            + "'");*/
                        if (term.Synonyms == null) {
                            Console.Log(term.Name + " (" + term.DatabaseId + ") has a null list of synonyms!");
                        }
                        //Original attempts to print the attributes of each term
                        /*else if (term.Synonyms.Length > 0) {
                            //foreach (GOTermSynonym synonym in term.Synonyms) {
                            Console.Log(term.Name + " has the following synonyms:");
                            foreach (JSObject synonym in term.Synonyms) {
                         * 
                                // trying with GOTermSynonym first - for some reason I get a non-specific object
                                // for each synonym in the term object, rather than a GOTermSynonym
                                Console.Log("'" + synonym.Name + "' (synonym of " + term.Name + ") is present, with the following attributes: term_id - '" + synonym.TermDatabaseId
                                    + "', acc_synonym - '" + synonym.UniqueIdentifier
                                    + "', synonym_type_id - '" + synonym.TypeId
                                    + "', synonym_category_id - '" + synonym.CategoryId
                                    + "'");
                         * 
                                // then with treating it like a JSObject - I can't just use these identifiers, actually
                                Console.Log("'" + synonym["term_synonym"] + "' (synonym of " + term.Name + ") is present, with the following attributes: term_id - '" + synonym["term_id"]
                                    + "', acc_synonym - '" + synonym["acc_synonym"]
                                    + "', synonym_type_id - '" + synonym["synonym_type_id"]
                                    + "', synonym_category_id - '" + synonym["synonym_category_id"]
                                    + "'");
                         * 
                                // So I had to do it like this for now - going through the key value pairs in the
                                // synonym object. Don't actually run it like this, since it runs too slow for
                                // obvious reasons
                                Console.Log("(Going through the key value pairs in the synonym object because for some reason it is not considered a GOTermSynonym by JavaScript.)");
                                foreach (KeyValuePair<string, object> pair in synonym) {
                                    if (pair.Value == null) {
                                        Console.Log(pair.Key + " has a null value");
                                    } else {
                                        Console.Log(pair.Key + " has the value " + pair.Value.ToString());
                                    }
                                }
                                actualNumSynonyms++;
                                }*/
                        else {

                            // Fortunately, just counting the number of synonyms works no matter
                            // what type the synonym objects are
                            Console.Log(term.Name + " (" + term.DatabaseId + ") has " + term.Synonyms.Length + " synonyms.");
                            actualNumSynonyms = actualNumSynonyms + term.Synonyms.Length;

                            if (term.Synonyms.Length > 0) {
                                foreach (JSObject synonym in term.Synonyms) {
                                    GOTermSynonym temp = GOTermSynonym.Parse(synonym);
                                    Console.Log("'" + temp.Name + "' (synonym of " + term.Name + ") is present, with the following attributes: term_id - '" + temp.TermDatabaseId
                                    + "', acc_synonym - '" + temp.UniqueIdentifier
                                    + "', synonym_type_id - '" + temp.TypeId
                                    + "', synonym_category_id - '" + temp.CategoryId
                                    + "'");
                                }
                            }
                        }
                    }

                    Console.Log(actual.Length + " terms all up.");
                    Console.Log(actualNumSynonyms + " synonyms all up.");

                    Assert.NumbersEqual(expectedNumTerms, actual.Length, T + " actual != the expected number of terms, " + expectedNumTerms);
                    Assert.NumbersEqual(expectedNumSynonyms, actualNumSynonyms, T + " actual != the expected number of synonyms, " + expectedNumSynonyms);

					Util.AppendResult( T, true );
				}
				catch ( Exception ex ) {
					Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					Console.Log( ex.StackTrace );
				}
			} );
            Console.Log("Reached the end of " + T);
        }

        // Second attempt at a combined ListTerms and ListSynonymsOfTerm implementation of this task
        static void TestListTermsWithSynonyms2() {
			string T = "ListTermsWithSynonyms2";
            GOTerm[] terms = new GOTerm[0];
            Console.Log("Getting all terms");
			service.GO_ListTerms( delegate( ServiceResponse<GOTerm[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				try {
                    int expectedNumTerms = 44426;
					terms = response.Content;

                    Assert.IsTrue(terms != null, T + ": " + "terms != null" );

					terms.Sort<GOTerm>( GOTerm.OrderById );

                    /*Console.Log("Checking terms in the received object.");

					foreach (GOTerm term in terms) {
                        Console.Log("'" + term.Name + "' (term) is present, with the following attributes: id - '" + term.DatabaseId
                            + "', term_type - '" + term.Namespace
                            + "', acc - '" + term.UniqueIdentifier
                            + "', is_obsolete - '" + term.Obsolete
                            + "', is_root - '" + term.Root
                            + "', is_relation - '" + term.Relation
                            + "'");
                    }*/

                    Console.Log(terms.Length + " terms all up.");

                    Assert.NumbersEqual(expectedNumTerms, terms.Length, T + " actual != the expected number of terms, " + expectedNumTerms);

				}
				catch ( Exception ex ) {
					Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					Console.Log( ex.StackTrace );
				}
			} );
            
            int expectedNumSynonyms = 120700;
            int actualNumSynonyms = 0;
            foreach (GOTerm term in terms) {
                service.GO_ListSynonymsOfTerm( term.DatabaseId, delegate( ServiceResponse<GOTermSynonym[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

                try {
					GOTermSynonym[] synonyms = response.Content;
					synonyms.Sort<GOTermSynonym>( GOTermSynonym.OrderById );
                    term.Synonyms = (List<GOTermSynonym>)synonyms;
			    }
			    catch ( Exception ex ) {
				    Util.AppendResult( T + ": error = " + ex.StackTrace, false );
				    Console.Log( ex.StackTrace );
			    }
            });
            }

            // The issue is that JavaScript calls the service, and then goes to the code after this
            // loop while this is executing, causing it to fail as the code is intended to run only
            // after the loop finishes
            foreach (GOTerm term in terms) {
                if (term.Synonyms.Length > 0) {
                    foreach (GOTermSynonym synonym in term.Synonyms) {
                        Console.Log("'" + synonym.Name + "' (synonym of " + term.Name + ") is present, with the following attributes: term_id - '" + synonym.TermDatabaseId
                            + "', acc_synonym - '" + synonym.UniqueIdentifier
                            + "', synonym_type_id - '" + synonym.TypeId
                            + "', synonym_category_id - '" + synonym.CategoryId
                            + "'");
                        actualNumSynonyms++;
                        }
                }
            }

            Console.Log(actualNumSynonyms + " synonyms all up.");
            Assert.NumbersEqual(expectedNumSynonyms, actualNumSynonyms, T + " actual != the expected number of synonyms, " + expectedNumSynonyms);
			Util.AppendResult( T, true );
            Console.Log("Reached the end of " + T);
        }

        // Test on whether we can retrieve all terms with any relationships
        // where they have a parent
        static void TestListTermsWithParents() {
			string T = "ListTermsWithParents";
			service.GO_ListTermsWithParents( delegate( ServiceResponse<GOTerm[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				try {
                    int expectedNumTerms = 44426;
                    int expectedNumRelationships = 90996;
					GOTerm[] actual = response.Content;
                    int actualNumRelationships = 0;

                    Assert.IsTrue(actual != null, T + ": " + "actual != null" );

					actual.Sort<GOTerm>( GOTerm.OrderByDatabaseId );

                    Console.Log("Checking terms and relationships in the received object.");

					foreach (GOTerm term in actual) {
                        /*Console.Log("'" + term.Name + "' (term) is present, with the following attributes: id - '" + term.DatabaseId
                            + "', term_type - '" + term.Namespace
                            + "', acc - '" + term.UniqueIdentifier
                            + "', is_obsolete - '" + term.Obsolete
                            + "', is_root - '" + term.Root
                            + "', is_relation - '" + term.Relation
                            + "'");*/
                        if (term.Relationships == null) {
                            //Console.Log(term.Name + " (" + term.DatabaseId + ") has a null list of relationships!");
                        }

                        else {

                            // Fortunately, just counting the number of synonyms works no matter
                            // what type the synonym objects are
                            //Console.Log(term.Name + " (" + term.DatabaseId + ") has " + term.Relationships.Length + " relationships where it is the child.");
                            actualNumRelationships = actualNumRelationships + term.Relationships.Length;

                            /*if (term.Relationships.Length > 0) {
                                foreach (JSObject relationship in term.Relationships) {
                                    GOTermRelationship temp = GOTermRelationship.Parse(relationship);
                                    Console.Log("Relatipnship ID " + temp.DatabaseId + " (relationship of " + term.Name + ") is present, with the following attributes: relationship_type_id - '" + temp.RelationshipDatabaseId
                                    + "', term1_id - '" + temp.ParentDatabaseId
                                    + "', term2_id - '" + temp.ChildDatabaseId
                                    + "', complete - '" + temp.Complete
                                    + "'");
                                }
                            }*/
                        }
                    }

                    Console.Log(actual.Length + " terms all up.");
                    Console.Log(actualNumRelationships + " relationships all up.");

                    Assert.NumbersEqual(expectedNumTerms, actual.Length, T + " actual != the expected number of terms, " + expectedNumTerms);
                    Assert.NumbersEqual(expectedNumRelationships, actualNumRelationships, T + " actual != the expected number of relationships, " + expectedNumRelationships);

					Util.AppendResult( T, true );
				}
				catch ( Exception ex ) {
					Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					Console.Log( ex.StackTrace );
				}
			} );
            Console.Log("Reached the end of " + T);
        }

        // Test whether we can retrieve all the entries in the new
        // regprecise_matches table (only the parent ID)
        static void TestGetRegPreciseCategoryMatches() {
			string T = "GetRegPreciseCategoryMatches";
			service.GO_GetRegPreciseCategoryMatches( delegate( ServiceResponse<GORegPreciseMatch[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );
				try {
                    int expectedNum = 9140;
					GORegPreciseMatch[] actual = response.Content;

                    Assert.IsTrue(actual != null, T + ": " + "actual != null" );

                    Console.Log("Checking matches in the received object.");

					foreach (GORegPreciseMatch match in actual) {
                        Console.Log("'" + match.GeneFunction + "' is present, matched to " + match.ParentTermName);
                    }

                    Console.Log(actual.Length + " matches all up.");

                    Assert.NumbersEqual(expectedNum, actual.Length, T + " actual != the expected number of matches, " + expectedNum);

					Util.AppendResult( T, true );
				}
				catch ( Exception ex ) {
					Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					Console.Log( ex.StackTrace );
				}
			} );
            Console.Log("Reached the end of " + T);
        }

        static public void RunAllTests() {
			Action [] tests = {
				//TestMySQLConnection,
                TestListTerms,
                TestListSynonymsOfTerm,
                TestListRelationshipsOfTermAsChild,
                TestGetTerm,
                //TestGetAllSynonyms,
                //TestListTermsWithSynonyms,
                //TestListTermsWithSynonyms2,
                TestListTermsWithParents,
                TestGetRegPreciseCategoryMatches,
			};

			for ( int i = 0; i < tests.Length; i++ ) {
				Action test = tests[i];
				test();
			}
		}

		#region Helper methods
        /// <summary> Parses the content of a file from json into an array of JavaScript Objects
		/// </summary>
		/// <param name="json"></param>
		/// <param name="topLevelObject"></param>
		/// <returns></returns>

		static JSObject[] GetRecords ( string json, string topLevelObject ) {
			object result = ( (JSObject) Json.Parse( json ) )[topLevelObject];

			return result is Array ? (JSObject[]) result : new JSObject[] { (JSObject) result };
		}

		/// <summary> Gets the contents of a file as an array of javascript objects.
		/// </summary>
		/// <param name="fileName"> The name of a JSON-formatted text file. </param>
		/// <param name="process"> A delegate that will process the resulting array of JavaScript objects. </param>
		/// <returns></returns>

		static void ReadRecords ( string fileName, Action<JSObject[]> process ) {
			Util.ReadAllText( fileName, delegate( string json ) {
				RegExp pattern = new RegExp( "[\"'](\\w+)[\"']" );
				string topLevelObject = pattern.Exec( json )[1];
				process( GetRecords( json, topLevelObject ) );
			} );
		}

		/// <summary> Returns true iff the supplied argument is really a number.
		/// </summary>
		/// <param name="x"> A (possibly) numeric value. </param>
		/// <returns> Returns true iff the supplied argument is really a number. </returns>

		private bool IsReallyNumber ( Number x ) {
			return x == x + 0;
		}
		#endregion
    }
}
