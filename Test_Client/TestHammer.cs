// TestSvg.cs
//

using System;
using System.Collections.Generic;
using System.Html;
using SystemQut.Svg;
using SystemQut;
using SystemQut.Css;
using System.Serialization;
using Test.Client.Controls;
using Hammer;

namespace Test.Client {

	public class TestHammer {
		public readonly Action[] Tests;

		public TestHammer() {
			Tests = new Action[] {
				Test01, 
                Test02,
				Test03,
                Test04,
                Test05,
			};
		}

        //Basic implementation
		void Test01() {
			const string T = "TestHammer_Test01";
			try {

#pragma warning disable 618
				HtmlUtil.CreateElement( "p", Document.Body, null ).TextContent = "Test01: Do touch gestures on the area below:";
				DivElement div = (DivElement) HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					"class", T,
					"style", "height: 1in; background: silver; overflow: auto;"
				) );
#pragma warning restore 618

				Hammer.Hammer mc = new Hammer.Hammer( div );

				mc.On( "panleft panright tap press", delegate( JSObject ev ) {
					div.TextContent = ev["type"] +" gesture detected.";
				} );
			}
			catch {
				Util.AppendResult( T, false );
			}
		}
        
        //Basic with vertical Pan recognizer
		void Test02() {
			const string T = "TestHammer_Test02";
			try {

#pragma warning disable 618
				HtmlUtil.CreateElement( "p", Document.Body, null ).TextContent = "Test02: Do touch gestures on the area below:";
				DivElement myElement = (DivElement) HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					"class", T,
					"style", "height: 1in; background: silver; overflow: auto;"
				) );
#pragma warning restore 618

                // create a simple instance
                // by default, it only adds horizontal recognizers
				Hammer.Hammer mc = new Hammer.Hammer( myElement );

                //Is there a better way to create this?
                JSObject options = new JSObject();
                options["direction"] = 30; //Hammer.Hammer.DIRECTION_ALL;
                
                // let the pan gesture support all directions.
                // this will block the vertical scrolling on a touch-device while on the element
                mc.Get("pan").Set(options);
                
                // listen to events...
				mc.On( "panleft panright panup pandown tap press", delegate( JSObject ev ) {
					myElement.TextContent = ev["type"] +" gesture detected.";
				} );
			}
			catch {
				Util.AppendResult( T, false );
			}
		}
        
        //RecognizeWith with Pinch and Rotate
		void Test03() {
			const string T = "TestHammer_Test03";
			try {

#pragma warning disable 618
				HtmlUtil.CreateElement( "p", Document.Body, null ).TextContent = "Test03: Do touch gestures on the area below:";
				DivElement myElement = (DivElement) HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					//"id", "myElement",
					"class", T,
					"style", "height: 1in; background: silver; overflow: auto;"
				) );
#pragma warning restore 618

				Manager mc = new Hammer.Manager( myElement );

				Recognizer pinch = new Hammer.Pinch();
				Recognizer rotate = new Hammer.Rotate();

				// we want to detect both the same time
				pinch.RecognizeWith( rotate );

				// add to the Manager
				mc.Add( new Recognizer[] { pinch, rotate } );

				mc.On( "pinch rotate", delegate( JSObject ev ) {
					myElement.TextContent += ev["type"] + " ";
				} );
			}
			catch {
				Util.AppendResult( T, false );
			}
		}

        //RecognizeWith with a Quadrupletap recognizer
        void Test04() {
			const string T = "TestHammer_Test04";
			try {

#pragma warning disable 618
				HtmlUtil.CreateElement( "p", Document.Body, null ).TextContent = "Test04: Do touch gestures on the area below:";
				DivElement myElement = (DivElement) HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					"id", "myElement",
					"class", T,
					"style", "height: 1in; background: silver; overflow: auto;"
				) );
#pragma warning restore 618

                // We create a manager object, which is the same as Hammer(), but without the presetted recognizers.
                Manager mc = new Hammer.Manager(myElement);

                // Default, tap recognizer
                mc.Add( new Hammer.Tap() );

                //Is there a better way to create this?
                /*TapOptions options = new TapOptions();
                options.Event = "quadrupletap";
                options.Taps = 4;*/

                JSObject options = new JSObject();
                options["event"] = "quadrupletap";
                options["taps"] = 4;

                // Tap recognizer with minimal 4 taps
                mc.Add(new Hammer.Tap(options));

                // we want to recognize this simulatenous, so a quadrupletap will be detected even while a tap has been recognized.
                // the tap event will be emitted on every tap
                mc.Get("quadrupletap").RecognizeWith("tap");

				mc.On( "tap quadrupletap", delegate( JSObject ev ) {
					myElement.TextContent += ev["type"] + " ";
				} );
			}
			catch {
				Util.AppendResult( T, false );
			}
        }
        //SingleTap and DoubleTap (with recognizeWith/requireFailure)

        //RecognizeWith with a Quadrupletap recognizer
        void Test05() {
			const string T = "TestHammer_Test05";
			try {

#pragma warning disable 618
				HtmlUtil.CreateElement( "p", Document.Body, null ).TextContent = "Test05: Do touch gestures on the area below:";
				DivElement myElement = (DivElement) HtmlUtil.CreateElement( "div", Document.Body, new Dictionary<string, string>(
					//"id", "myElement",
					"class", T,
					"style", "height: 1in; background: silver; overflow: auto;"
				) );
#pragma warning restore 618

                // We create a manager object, which is the same as Hammer(), but without the presetted recognizers.
                Manager mc = new Hammer.Manager(myElement);

                //Is there a better way to create this?
                /*TapOptions doubleTapOptions = new TapOptions();
                doubleTapOptions.Event = "doubletap";
                doubleTapOptions.Taps = 2;
                TapOptions singleTapOptions = new TapOptions();
                singleTapOptions.Event = "singletap";*/

                JSObject doubleTapOptions = new JSObject();
                doubleTapOptions["event"] = "doubletap";
                doubleTapOptions["taps"] = 2;
                JSObject singleTapOptions = new JSObject();
                singleTapOptions["event"] = "singletap";

                // Tap recognizer with minimal 2 taps
                mc.Add( new Hammer.Tap(doubleTapOptions) );

                // Single tap recognizer
                mc.Add( new Hammer.Tap(singleTapOptions) );

                // we want to recognize this simulatenous, so a quadrupletap will be detected even while a tap has been recognized.
                mc.Get("doubletap").RecognizeWith("singletap");

                // we only want to trigger a tap, when we don't have detected a doubletap
                mc.Get("singletap").RequireFailure("doubletap");
                
				mc.On( "singletap doubletap", delegate( JSObject ev ) {
					myElement.TextContent += ev["type"] + " ";
				} );
			}
			catch {
				Util.AppendResult( T, false );
			}
        }
	}
}
