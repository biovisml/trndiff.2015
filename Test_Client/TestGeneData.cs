﻿using System;
using System.Collections.Generic;
using SystemQut.IO;
using SystemQUT.Csv;

namespace Test.Client
{
    class TestGeneData
    {
        internal void RunAllTests()
        {
            try {
                Util.ReadAllText( "/UnitTests/Resources/Csv/" + "geneData.csv", delegate( string text ) {
                    StringReader reader = new StringReader( text );
				    CsvIO csvIO = new CsvIO( ',', '"' );
				    string[][] records = csvIO.Read( reader );
                    //Console.Log("Loaded " + (records.Length - 1) + " gene records with columns: " + records[0].ToString());

                    //just contains a list of all functions to prevent duplicates
                    //Console.Log("Unique entries ONLY");
                    List<string> functions = new List<string>();

                    Dictionary<string, int> functionTermAmounts = new Dictionary<string, int>();

                    string[] forbiddenTerms = { "for", "of", "and", "not", "\\", "\\(", "\\)", ",", ".", "to", "a", "in" };

                    int firstTotal = 0;
                    int secondTotal = 0;

                    for (int i = 1; i < records.Length; i++)
                    {
                        //if (!functions.Contains(records[i][records[0].IndexOf("geneFunction")].ToLowerCase())) {
                        functions.Add(records[i][records[0].IndexOf("geneFunction")].ToLowerCase());

                            string[] currentGeneTerms = records[i][records[0].IndexOf("geneFunction")].Split(" ");
                            /*foreach (string term in currentGeneTerms)
                            {
                                if (term.ToLowerCase() != records[i][records[0].IndexOf("geneName")].ToLowerCase() && !forbiddenTerms.Contains(term.ToLowerCase()) && Number.IsNaN(Number.Parse(term)))
                                {
                                    if (!functionTermAmounts.ContainsKey(term.ToLowerCase()))
                                    {
                                        functionTermAmounts[term.ToLowerCase()] = 1;
                                    }
                                    else
                                    {
                                        functionTermAmounts[term.ToLowerCase()] = functionTermAmounts[term.ToLowerCase()] + 1;
                                    }
                                }
                            }*/

                            //only the first one
                            /*if (currentGeneTerms[0].ToLowerCase() != records[i][records[0].IndexOf("geneName")].ToLowerCase() && !forbiddenTerms.Contains(currentGeneTerms[0].ToLowerCase()) && Number.IsNaN(Number.Parse(currentGeneTerms[0])))
                            {
                                if (!functionTermAmounts.ContainsKey(currentGeneTerms[0].ToLowerCase()))
                                {
                                    functionTermAmounts[currentGeneTerms[0].ToLowerCase()] = 1;
                                }
                                else
                                {
                                    functionTermAmounts[currentGeneTerms[0].ToLowerCase()] = functionTermAmounts[currentGeneTerms[0].ToLowerCase()] + 1;
                                }
                            }*/

                            //The first pair
                            string currentKey = currentGeneTerms[0].ToLowerCase();
                            if (currentGeneTerms.Length > 1) {
                                currentKey += (" " + currentGeneTerms[1].ToLowerCase());
                            }
                            if (!functionTermAmounts.ContainsKey(currentKey))
                            {
                                functionTermAmounts[currentKey] = 1;
                            }
                            else
                            {
                                functionTermAmounts[currentKey] = functionTermAmounts[currentKey] + 1;
                            }
                            firstTotal++;
                        //}
                    }

                    //Console.Log(functionTermAmounts.Count + " terms in all of the gene functions");
                    //Console.Log(functions.Length + " distinct functions");
                    //Console.Log(functionTermAmounts.Count + " distinct first words in those functions");
                    Console.Log(functions.Length + " genes");
                    //Console.Log(functionTermAmounts.Count + " distinct pairs of words in those functions");
                    Console.Log(functionTermAmounts.Count + " distinct pairs of words in the functions of those genes");

                    List<string> sortedFunctionTermsNames = new List<string>();
                    List<int> sortedFunctionTermsAmounts = new List<int>();

                    foreach (KeyValuePair<string, int> pair in functionTermAmounts)
                    {
                        switch (sortedFunctionTermsAmounts.Length)
                        {
                            case 0:
                                sortedFunctionTermsNames.Add(pair.Key);
                                sortedFunctionTermsAmounts.Add(pair.Value);
                                secondTotal += pair.Value;
                                break;
                            case 1:
                                if (sortedFunctionTermsAmounts[0] > pair.Value)
                                {
                                    sortedFunctionTermsNames.Insert(1, pair.Key);
                                    sortedFunctionTermsAmounts.Insert(1, pair.Value);
                                }
                                else
                                {
                                    sortedFunctionTermsNames.Insert(0, pair.Key);
                                    sortedFunctionTermsAmounts.Insert(0, pair.Value);
                                }
                                secondTotal += pair.Value;
                                break;
                            default:
                                int insertLocation = 0;
                                for (int i = 0; i < sortedFunctionTermsAmounts.Length; i++)
                                {
                                    if (sortedFunctionTermsAmounts[i] >= pair.Value)
                                    {
                                        insertLocation = i + 1;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                sortedFunctionTermsNames.Insert(insertLocation, pair.Key);
                                sortedFunctionTermsAmounts.Insert(insertLocation, pair.Value);
                                secondTotal += pair.Value;
                                break;
                        }
                    }

                    //foreach (KeyValuePair<string, int> pair in functionTermAmounts) {
                    //    Console.Log(pair.Key + " occurs " + pair.Value + " times in this regulog.");
                    //}
                    for (int i = 0; i < sortedFunctionTermsAmounts.Length; i++)
                    {
                        //Console.Log(sortedFunctionTermsNames[i] + " occurs " + sortedFunctionTermsAmounts[i] + " times in the gene list.");
                        //Console.Log("The first word '" + sortedFunctionTermsNames[i] + "' occurs " + sortedFunctionTermsAmounts[i] + " times in the gene list.");
                        Console.Log(sortedFunctionTermsNames[i] + " " + sortedFunctionTermsAmounts[i]);
                    }

                    Console.Log("total 1: " + firstTotal + ", total 2: " + secondTotal);
                });
            } catch (Exception ex) {
                Console.Log("Reading geneData.csv failed for the following reason: " + ex.Message);
            }
        }
    }
}
