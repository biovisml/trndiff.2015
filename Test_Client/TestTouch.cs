﻿using System;
using System.Collections.Generic;
using System.Html;
//using System.Text;
using SystemQut.Svg;

namespace Test.Client
{
    public class TestTouch
    {
        internal void RunAllTests()
        {

            Action[] tests = {
                                 TouchableElementAndADiv,
                             };

            for (int i = 0; i < tests.Length; i++)
            {
                Action test = tests[i];

                try
                {
                    test();
                }
                catch (Exception ex)
                {
                    Window.Alert("Unhandled error:\n" + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        //private SvgText TouchInfoText;
        private Element TouchLabel;

        private void TouchableElementAndADiv()
        {
            DivElement div = (DivElement)Document.CreateElement("div");
            Document.Body.AppendChild(div);

            Element touchSpan = Document.CreateElement("span");
            touchSpan.ID = "Touch span";
            div.AppendChild(touchSpan);

            SvgSvg touchBlock = Svg.CreateOuterSvgElement(touchSpan, null);

            touchBlock.Width = new SvgLength(2.5, SvgLengthType.IN);
            touchBlock.Height = new SvgLength(2.5, SvgLengthType.IN);
            touchBlock.ViewBox = new SvgRect(-100, -100, 200, 200);
            touchBlock.DomElement.Style.BackgroundColor = "white";
            touchBlock.DomElement.Style.Margin = "3px";
            touchBlock.DomElement.Style.Border = "1px dashed black";
            touchBlock.Name = "Touch block";

            SvgText touchText = new SvgText(touchBlock, null);
            touchText.FontSize = Svg.ConvertLength(0.25, 2.5, 200).ToString();
            touchText.Text = "Touch here";
            touchText.TextAnchor = TextAnchorType.middle;
            touchText.Name = "Touch text";
            
            touchSpan.AddEventListener(TouchEventType.touchmove, TouchStartHandler, false);

            //Element infoSpan = Document.CreateElement("span");
            //infoSpan.ID = "Info span";
            //div.AppendChild(infoSpan);

            //SvgSvg infoBlock = Svg.CreateOuterSvgElement(infoSpan, null);

            //infoBlock.Width = new SvgLength(7.5, SvgLengthType.IN);
            //infoBlock.Height = new SvgLength(2.5, SvgLengthType.IN);
            //infoBlock.ViewBox = new SvgRect(-100, -100, 600, 200);
            //infoBlock.DomElement.Style.BackgroundColor = "white";
            //infoBlock.DomElement.Style.Margin = "3px";
            //infoBlock.DomElement.Style.Border = "1px dashed black";

            //TouchInfoText = new SvgText(infoBlock, null);
            //TouchInfoText.FontSize = Svg.ConvertLength(0.2, 2.5, 200).ToString();
            //TouchInfoText.TextAnchor = TextAnchorType.middle;

            TouchLabel = Document.CreateElement("label");
            TouchLabel.ID = "Touch label";
            div.AppendChild(TouchLabel);
            string information = "Touch information goes here";
            //TouchInfoText.Text = information;
            TouchLabel.TextContent = information;
        }
        
        private void TouchStartHandler(ElementEvent e)
        {
            Element svgElement = e.Target;
            string information = string.Empty;

            for (int i = 0; i < (int)Script.Literal("e.changedTouches.length"); i ++) {
                if (i > 0) {
                    information = information + " | ";
                }
                information = information + "Touch " + i + " data (identifier = " + Script.Literal("e.changedTouches[i].identifier") + "): ";
                if (svgElement != null) {
                    if (svgElement.GetAttribute("data-name") != null) {                        
                        information = information + "touch occurred in element " + svgElement.GetAttribute("data-name") + ", ";
                    } else {
                        information = information + "touch occurred in element " + svgElement.ID + ", ";
                    }
                }
                if ((bool)Script.Literal("e.changedTouches[i].pageX != 0")) {
                    information = information + "pageX = " + Script.Literal("Math.round(e.changedTouches[i].pageX)") + ", ";
                }
                if ((bool)Script.Literal("e.changedTouches[i].pageY != 0")) {
                    information = information + "pageY = " + Script.Literal("Math.round(e.changedTouches[i].pageY)") + ", ";
                }
                /*if ((bool)Script.Literal("e.changedTouches[i].target != null")) {
                    Element element = (Element)Script.Literal("e.changedTouches[i].target");
                    information = information + "target = " + element.ID + ", ";
                }*/
                if ((bool)Script.Literal("e.changedTouches[i].force != 0")) {
                    information = information + "force = " + Script.Literal("e.changedTouches[i].force") + ", ";
                }
                if ((bool)Script.Literal("e.changedTouches[i].radiusX != 0")) {
                    information = information + "radiusX = " + Script.Literal("Math.round(e.changedTouches[i].radiusX)") + ", ";
                }
                if ((bool)Script.Literal("e.changedTouches[i].radiusY != 0")) {
                    information = information + "radiusY = " + Script.Literal("Math.round(e.changedTouches[i].radiusY)") + ", ";
                }
                if ((bool)Script.Literal("e.changedTouches[i].rotationAngle != 0")) {
                    information = information + "rotationAngle = " + Script.Literal("Math.round(e.changedTouches[i].rotationAngle)");
                }
                if ((bool)Script.Literal("e.changedTouches[i].rotationAngle != 0")) {
                    information = information + "rotationAngle = " + Script.Literal("Math.round(e.changedTouches[i].rotationAngle)");
                }
            }
            //TouchInfoText.Text = information;
            TouchLabel.TextContent = information;
        }
    }
}
