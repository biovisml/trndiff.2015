// TestRegPrecise.cs
//

using System;
using System.Collections.Generic;
using RegPrecise;
using SystemQut;
using System.Serialization;
using SystemQut.ServiceModel;
using SystemQut.IO;
using System.Html.Data.Files;
using System.Html;
//using RegulonExplorer.ViewModel;
using SystemQUT.Csv;
using SystemQut.Linq;

namespace Test.Client {
	public class TestRegPrecise {
		public Action[] Tests;

		public TestRegPrecise () {
			Tests = new Action[] {
				// TODO: what is Test1001 trying to do? can we fix it?
				// this.Test1001,
				this.GetRegulog,
				this.GetRegulon,
				this.ListGenesInRegulog,
				this.ListGenesInRegulon,
				this.ListGenomeStats,
				this.ListRegulatorsInRegulog,
				this.ListRegulatorsInRegulon,
				this.ListRegulonsInGenome,
				this.ListRegulonsInRegulog,
				this.ListSitesInRegulog,
				this.ListSitesInRegulon,
				// this.SearchExtRegulonsByRegulator,
				this.SearchRegulonsByGene,
				this.SearchRegulonsByRegulator,
				this.ListGenomes,
				this.ListRegulogCollections,
				this.ListRegulogs,
			};
		}

		IRegPreciseService service = new RegPreciseService( "http://127.0.0.1:32207/RegPrecise.svc", false );

        #region Test1001
        public void Test1001 () {
            Util.ReadAllText("/UnitTests/Resources/Yersinia_Fur_processed_May.json", delegate(string json)
            {
				//Console.Log( json );
                object[] result = (JSObject[])Json.Parse(json);
                //GenomeYersinia[] genomeYersinia;
                //genomeYersinia = (GenomeYersinia[])result;

                double[] GeneNumbers = new double[(((JSObject)result[0])["TGs"] as JSObject[]).Length];
                double[] LinkNumbers = new double[(((JSObject)result[0])["TGs"] as JSObject[]).Length];

                double[] GeneNumbersPathogenic = new double[(((JSObject)result[0])["TGs"] as JSObject[]).Length];
                double[] LinkNumbersPathogenic = new double[(((JSObject)result[0])["TGs"] as JSObject[]).Length];

                double[] GeneNumbersNonPathogenic = new double[(((JSObject)result[0])["TGs"] as JSObject[]).Length];
                double[] LinkNumbersNonPathogenic = new double[(((JSObject)result[0])["TGs"] as JSObject[]).Length];

                //Probabilities of whether a pathogenic genome will be
                //generated each time. Non-pathogenic is 1 - this probability
                double[] alpha = { 0.25, 0.5, 0.75 };

                //Initialise these numbers
                for (int i = 0; i < GeneNumbers.Length; i++ )
                {
                    GeneNumbers[i] = 0;
                    LinkNumbers[i] = 0;
                    GeneNumbersPathogenic[i] = 0;
                    LinkNumbersPathogenic[i] = 0;
                    GeneNumbersNonPathogenic[i] = 0;
                    LinkNumbersNonPathogenic[i] = 0;
                }

                int originalTotalGenes = 0;
                int originalTotalLinks = 0;

                int numPathogenicGenomes = 0;

                //Tally up all the genes and links
                for (int i = 1; i < result.Length; i++)
                {
                    bool isPathogenic = false;

                    for (int j = 0; j < (((JSObject)result[i])["TGs"] as JSObject[]).Length; j++)
                    {
                        if (((((JSObject)result[i])["TGs"] as JSObject[])[j]["group"] as string) == "Pathogenic")
                        {
                            isPathogenic = true;
                            numPathogenicGenomes = numPathogenicGenomes + 1;
                            break;
                        }
                    }
                    for (int j = 0; j < (((JSObject)result[i])["TGs"] as JSObject[]).Length; j++)
                    {
                        //Console.Log("Processing " + (string)(((JSObject)result[0])["TGs"] as JSObject[])[j]["TG"] + " for " + (string)((JSObject)result[i])["Genome"]);
                        //Tally up this gene if it exists in the current network
                        if (!String.IsNullOrEmpty((string)(((JSObject)result[i])["TGs"] as JSObject[])[j]["TG"]))
                        {
                            //Console.Log("It is present in this genome.");
                            GeneNumbers[j] = GeneNumbers[j] + 1;
                            originalTotalGenes = originalTotalGenes + 1;

                            //If it's pathogenic or not, put a tally in the correct list
                            //Have to use this boolean now because the format
                            //is odd in Xin-Yi's file
                            if (isPathogenic)
                            {
                                //Console.Log("Adding a tally to the pathogenic list.");
                                GeneNumbersPathogenic[j] = GeneNumbersPathogenic[j] + 1;
                            }
                            else
                            {
                                //Console.Log("Adding a tally to the non-pathogenic list.");
                                GeneNumbersNonPathogenic[j] = GeneNumbersNonPathogenic[j] + 1;
                            }

                            //Tally up a link if it exists in the current network
                            if ((int)(((JSObject)result[i])["TGs"] as JSObject[])[j]["weight"] == 1)
                            {
                                //Console.Log("It is regulated in this genome.");
                                LinkNumbers[j] = LinkNumbers[j] + 1;
                                originalTotalLinks = originalTotalLinks + 1;

                                //If it's pathogenic or not, put a tally in the correct list
                                //Might as well use the boolean here too
                                if (isPathogenic)
                                {
                                    //Console.Log("Adding a tally to the pathogenic list.");
                                    LinkNumbersPathogenic[j] = LinkNumbersPathogenic[j] + 1;
                                }
                                else
                                {
                                    //Console.Log("Adding a tally to the non-pathogenic list.");
                                    LinkNumbersNonPathogenic[j] = LinkNumbersNonPathogenic[j] + 1;
                                }
                            }
                        }
                    }
                }

                //Make them into probabilities.
                Console.Log("Probabilities for each gene.");
                for (int i = 0; i < (((JSObject)result[0])["TGs"] as JSObject[]).Length; i++)
                {

                    Console.Log((string)(((JSObject)result[0])["TGs"] as JSObject[])[i]["TG"]);

                    GeneNumbers[i] = GeneNumbers[i] / (result.Length - 1);
                    Console.Log("Gene total probability: " + GeneNumbers[i]);

                    GeneNumbersPathogenic[i] = GeneNumbersPathogenic[i] / (numPathogenicGenomes);
                    Console.Log("Gene pathogenic probability: " + GeneNumbersPathogenic[i]);

                    GeneNumbersNonPathogenic[i] = GeneNumbersNonPathogenic[i] / (result.Length - numPathogenicGenomes - 1);
                    Console.Log("Gene non-pathogenic probability: " + GeneNumbersNonPathogenic[i]);

                    LinkNumbers[i] = LinkNumbers[i] / (result.Length - 1);
                    Console.Log("Link total probability: " + LinkNumbers[i]);

                    LinkNumbersPathogenic[i] = LinkNumbersPathogenic[i] / (numPathogenicGenomes);
                    Console.Log("Link pathogenic probability: " + LinkNumbersPathogenic[i]);

                    LinkNumbersNonPathogenic[i] = LinkNumbersNonPathogenic[i] / (result.Length - numPathogenicGenomes - 1);
                    Console.Log("Link non-pathogenic probability: " + LinkNumbersNonPathogenic[i]);

                }

                for (int k = 0; k < alpha.Length; k++) {
                    //Create a random set of genomes
                    object[] randomGenomes = new object[20];

                    //For Lawrence's stuff later...
                    int totalGenes = 0;
                    int totalLinks = 0;
                    for (int i = 0; i < randomGenomes.Length; i++)
                    {
                        //Name the genome and the transcription factor
                        randomGenomes[i] = new JSObject();
                        ((JSObject[])randomGenomes)[i]["Genome"] = "E coli. variant " + (i+1);
                        ((JSObject[])randomGenomes)[i]["TF"] = "fur";
                        ((JSObject[])randomGenomes)[i]["TGs"] = new JSObject[(((JSObject)result[0])["TGs"] as JSObject[]).Length];
                        Console.Log("Determining E coli. variant " + (i+1) + "'s genes.");

                        //Determine whether the current genome should be pathogenic
                        bool pathogenic = false;
                        if (Math.Random() < alpha[k])
                        {
                            pathogenic = true;
                            Console.Log("This genome is pathogenic.");
                            ((JSObject[])randomGenomes)[i]["Genome"] = ((JSObject[])randomGenomes)[i]["Genome"] + " - pathogenic";
                        }
                        else
                        {
                            Console.Log("This genome is non-pathogenic.");
                        }

                        //If it is pathogenic
                        if (pathogenic)
                        {

                            //Determine which genes are in this genome
                            for (int j = 0; j < (((JSObject)result[0])["TGs"] as JSObject[]).Length; j++)
                            {
                                JSObject currentGene = (((JSObject)result[0])["TGs"] as JSObject[])[j];
                                //Console.Log("Determining whether " + currentGene["TG"] + " is in E coli. variant " + (i+1) + ".");

                                //If the random number is less than the probabilty
                                //number for this gene, add it
                                if (Math.Random() < GeneNumbersPathogenic[j])
                                {
                                    //Console.Log("This genome will contain this gene.");
                                    totalGenes = totalGenes + 1;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j] = new JSObject();
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["TG"] = currentGene["TG"];
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["weight"] = 0;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["bioFunction"] = currentGene["bioFunction"];
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["operon"] = currentGene["operon"];
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["group"] = "Pathogenic";

                                    //If a gene was added, determine whether there's a
                                    //link - if this random number is less than the
                                    //probability number for this link, set the weight
                                    //to 1 (i.e. it exists) otherwise set it to 0
                                    if (Math.Random() < LinkNumbersPathogenic[j])
                                    {
                                        //Console.Log("The transcription factor will act on this gene.");
                                        totalLinks = totalLinks + 1;
                                        (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["weight"] = 1;
                                    }
                                    else
                                    {
                                        //Console.Log("The transcription factor will not act on this gene.");
                                        (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["weight"] = 0;
                                    }
                                    //Else add a blank gene
                                }
                                else
                                {
                                    //Console.Log("This genome will not contain this gene.");
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j] = new JSObject();
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["TG"] = null;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["weight"] = null;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["bioFunction"] = null;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["operon"] = null;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["group"] = null;
                                }
                            }

                            //If it isn't pathogenic
                        }
                        else
                        {

                            //Determine which genes are in this genome
                            for (int j = 0; j < (((JSObject)result[0])["TGs"] as JSObject[]).Length; j++)
                            {
                                JSObject currentGene = (((JSObject)result[0])["TGs"] as JSObject[])[j];
                                //Console.Log("Determining whether " + currentGene["TG"] + " is in E coli. variant " + (i+1) + ".");


                                //If this random number is less than the probabilty
                                //number for this gene, add it
                                if (Math.Random() < GeneNumbersNonPathogenic[j])
                                {
                                    //Console.Log("This genome will contain this gene.");
                                    totalGenes = totalGenes + 1;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j] = new JSObject();
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["TG"] = currentGene["TG"];
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["weight"] = 0;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["bioFunction"] = currentGene["bioFunction"];
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["operon"] = currentGene["operon"];
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["group"] = "Non-pathogenic";

                                    //If a gene was added, determine whether there's a
                                    //link - if this random number is less than the
                                    //probability number for this link, set the weight
                                    //to 1 (i.e. it exists) otherwise set it to 0
                                    if (Math.Random() < LinkNumbersNonPathogenic[j])
                                    {
                                        //Console.Log("The transcription factor will act on this gene.");
                                        totalLinks = totalLinks + 1;
                                        (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["weight"] = 1;
                                    }
                                    else
                                    {
                                        //Console.Log("The transcription factor will not act on this gene.");
                                        (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["weight"] = 0;
                                    }
                                    //Else add a blank gene
                                }
                                else
                                {
                                    //Console.Log("This genome will not contain this gene.");
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j] = new JSObject();
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["TG"] = null;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["weight"] = null;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["bioFunction"] = null;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["operon"] = null;
                                    (((JSObject)randomGenomes[i])["TGs"] as JSObject[])[j]["group"] = null;
                                }
                            }
                        }
                    }

                    //Add the reference to the start of the new set
                    //randomGenomes[0] = result[0];

                    string doc = ConvertToLawrenceFormat((((JSObject)result[0])["TGs"] as JSObject[]), randomGenomes, totalGenes, totalLinks);

                    Element contentHolder = HtmlUtil.CreateElement("div", Document.Body, null);

                    string exportFilename = "randomLawrenceOutput " + k + ".csv";

                    ExportLawrenceFormat(doc, contentHolder, exportFilename);

                    //HtmlUtil.SaveText("C:\\PhD\\Programs\\randomLawrenceOutput.csv", writer.ToString(), "text/csv");
                }

                //Export the original set as well

                object[] resultWithoutReference = new object[result.Length-1];
                for (int l = 0; l < resultWithoutReference.Length; l++) {
                    resultWithoutReference[l] = result[l+1];
                }

                string originalDoc = ConvertToLawrenceFormat((((JSObject)result[0])["TGs"] as JSObject[]), resultWithoutReference, originalTotalGenes, originalTotalLinks);

                Element originalContentHolder = HtmlUtil.CreateElement("div", Document.Body, null);

                string originalExportFilename = "originalXinYIConverted.csv";

                ExportLawrenceFormat(originalDoc, originalContentHolder, originalExportFilename);

                Console.Log("finished this test");
			} );
		}

        private static string ConvertToLawrenceFormat(JSObject[] reference, object[] genomesToConvert, int totalGenes, int totalLinks)
        {
            //Let's try to put this into Lawrence TRNDiff format
            Regulog[] randomLawrenceRegulogs = new Regulog[1];
            Genome[] randomLawrenceGenomes = new Genome[genomesToConvert.Length];
            Regulon[] randomLawrenceRegulons = new Regulon[genomesToConvert.Length];
            int currentGenePos = 0;
            int currentLinkPos = 0;
            Site[] randomLawrenceSites = new Site[totalLinks];
            Gene[] randomLawrenceGenes = new Gene[totalGenes];
            Regulator[] randomLawrenceRegulators = new Regulator[genomesToConvert.Length];

            randomLawrenceRegulogs[0] = new Regulog("Iron ion, (Fe2+)", RegulationTypes.TranscriptionFactor, "FUR", "fur", 1112, "E coli.", "Iron homeostasis");

            for (int i = 0; i < genomesToConvert.Length; i++)
            {

                //if ((string)(((JSObject)genomesToConvert[i])["Genome"]) == "reference")
                //{
                //    i = i + 1;
                //}
                randomLawrenceGenomes[i] = new Genome(10000 + i, (string)(((JSObject)genomesToConvert[i])["Genome"]), 1);
                randomLawrenceRegulons[i] = new Regulon(10000 + i, randomLawrenceRegulogs[0].RegulogId, randomLawrenceGenomes[i].GenomeId, (string)(((JSObject)genomesToConvert[i])["Genome"]), randomLawrenceRegulogs[0].RegulatorName, randomLawrenceRegulogs[0].RegulatorFamily, randomLawrenceRegulogs[0].RegulationType, randomLawrenceRegulogs[0].Effector, "default pathway");
                for (int j = 0; j < reference.Length; j++)
                {
                    if (!String.IsNullOrEmpty((string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["TG"]))
                    {
                        string locusTag = "VAR_" + (currentGenePos + 9000);
                        if (!String.IsNullOrEmpty((string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["locus"])) {
                            locusTag = (string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["locus"];
                        }
                        //Console.Log("Adding " + (string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["TG"] + " to " + (string)(((JSObject)genomesToConvert[i])["Genome"]));
                        if ((int)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["weight"] == 1)
                        {
                            randomLawrenceSites[currentLinkPos] = new Site(locusTag, currentGenePos, currentGenePos, 10000 + i, 5, "N\\A");
                            currentLinkPos = currentLinkPos + 1;
                        }
                        if ((string)(((JSObject)genomesToConvert[0])["Genome"]) == "reference")
                        {
                            //randomLawrenceGenes[currentGenePos] = new Gene(10000 + i, (string)(((JSObject)genomesToConvert[0])["TGs"] as JSObject[])[j]["TG"], locusTag, currentGenePos, (string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["bioFunction"], false, string.Empty, string.Empty);
                            randomLawrenceGenes[currentGenePos] = new Gene(10000 + i, (string)(((JSObject)genomesToConvert[0])["TGs"] as JSObject[])[j]["TG"], locusTag, currentGenePos, (string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["bioFunction"], string.Empty, string.Empty);
                        }
                        else
                        {
                            //randomLawrenceGenes[currentGenePos] = new Gene(10000 + i, (string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["TG"], locusTag, currentGenePos, (string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["bioFunction"], false, string.Empty, string.Empty);
                            randomLawrenceGenes[currentGenePos] = new Gene(10000 + i, (string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["TG"], locusTag, currentGenePos, (string)(((JSObject)genomesToConvert[i])["TGs"] as JSObject[])[j]["bioFunction"], string.Empty, string.Empty);
                        }
                        //Console.Log("Gene " + currentGenePos + " is " + randomLawrenceGenes[currentGenePos].Name);
                        currentGenePos = currentGenePos + 1;
                    }
                }
                randomLawrenceRegulators[i] = new Regulator(10000 + i, "Fur", "VAR_" + (i + 8000), 20000 + i, randomLawrenceRegulogs[0].RegulatorFamily);
            }

            JSObject[][] collections = { randomLawrenceGenomes, randomLawrenceRegulogs, randomLawrenceRegulons, randomLawrenceGenes, randomLawrenceRegulators, randomLawrenceSites };

            string[] tableNames = { "Genomes", "Regulogs", "Regulons", "Genes", "Regulators", "Sites" };

            StringWriter writer = new StringWriter();

            for (int i = 0; i < tableNames.Length; i++)
            {
                writer.WriteLine(tableNames[i], null);
                writer.WriteLn();
                writer.WriteString(Objects.SerializeTable(collections[i]));
                writer.WriteLn();
            }

            string doc = writer.ToString();
            return doc;
        }

        private static void ExportLawrenceFormat(string doc, Element contentHolder, string exportFilename)
        {
            /* Save document... experimental: code from SaveToDisk at http://muaz-khan.blogspot.com.au/2012/10/save-files-on-disk-using-javascript-or.html */
            {
                string fileName = null;

                Blob blob = new Blob(new string[] { doc }, new Dictionary<string, string>(
                    "type", "text/csv"
                ));

                // Flanagan, David (2011-04-18). JavaScript: The Definitive Guide (Definitive Guides) (p. 696). O'Reilly Media. Kindle Edition.
                Func<Blob, string> getBlobUrl = (Func<Blob, string>)Script.Literal("(window.URL && URL.createObjectURL.bind( URL)) || (window.webkitURL && webkitURL.createObjectURL.bind( webkitURL)) || window.createObjectURL");
                Action<string> revokeBlobUrl = (Action<string>)Script.Literal("(window.URL && URL.revokeObjectURL.bind( URL)) || (window.webkitURL && webkitURL.revokeObjectURL.bind( webkitURL)) || window.revokeObjectURL");

                string url = getBlobUrl(blob);
                string actualFileName = fileName ?? exportFilename;

                if ((bool)Script.Literal("!! window.navigator.msSaveBlob"))
                {
                    // Internet Explorer...
                    Script.Literal("window.navigator.msSaveBlob( blob, actualFileName );");
                }
                else
                {
                    // FireFox, Chrome...
                    AnchorElement save = (AnchorElement)Document.CreateElement("a");
                    save.Href = url;
                    save.Target = "_blank";
                    save.Download = actualFileName;
                    save.Style.Display = "none";
                    save.InnerHTML = "Click to save!";

                    // This is a shim for Firefox.
                    string alternateUrl = String.Format("{0}:{1}:{2}", "text/csv", actualFileName, url);
                    Script.Literal("if ( save.dataset ) { save.dataset.downloadurl = alternateUrl; }");

                    contentHolder.AppendChild(save);

                    // This appears to not work in Firefox.
                    // MutableEvent evt = Document.CreateEvent( "Event" );
                    // evt.InitEvent( "click", true, true );
                    // save.DispatchEvent( evt );

                    save.Click();

                    // This causes grief in Firefox.
                    // revokeBlobUrl( url );
                }
            }
        }
        #endregion

        #region TestVector
		#if false

        /*Action<FileProgressEvent> onError = delegate(FileProgressEvent evt)
        {
            Window.Alert(String.Format("Error: {0}", evt.Detail));
        };*/

        /// <summary> Lookup table mapping genome key to GenomeInfo.
        /// <para>
        ///		This will contain both regPrecise and locally loaded genomes.
        /// </para>
        /// </summary>
        //private static GenomeCollection genomeCollection = new GenomeCollection();

        /// <summary> Constant identifier which defines the default source (RegPrecise).
        /// </summary>

        //public static string LocalFile
        //{
        //    get { return "LocalFile"; }
        //}

        //public void TestVector()
        //{
        //    Util.ReadAllText("/UnitTests/Resources/Csv/regulog1112.csv", delegate(string csv)
        //    {
        //        Console.Log("regulog1112.csv loaded");
        //        RegulogInfo[] regulogs = ParseRegulog(csv);
        //        Console.Log("regulog1112.csv parsed");
        //        foreach (RegulogInfo regulog in regulogs)
        //        {
        //            Console.Log("Creating vectors for regulog with regulator " + regulog.RegulatorName);
        //            int[][] vectors = new int[regulog.Regulons.Length][];
        //            Console.Log("This regulog has " + regulog.Regulons.Length + " genes");
        //            for (int i = 0; i < regulog.Regulons.Length; i++)
        //            {
        //                Console.Log("Now creating vector for regulon " + regulog.Regulons[i].GenomeName);
        //                vectors[i] = new int[regulog.Genes.Length];
        //                for (int j = 0; j < regulog.Genes.Length; j++)
        //                {
        //                    vectors[i][j] = 0;
        //                    GeneInfo currentGene = regulog.Genes[j];
        //                    Console.Log("Checking if the current regulon has " + currentGene.Name);
        //                    foreach (GeneInfo regulonGene in regulog.Regulons[i].Genes)
        //                    {
        //                        if (regulonGene.Name == currentGene.Name) {
        //                            vectors[i][j] = 1;
        //                            Console.Log("Yes it does - setting the entry in the vector to '1'");
        //                            break;
        //                        }
        //                    }
        //                }
        //                Console.Log("Vector for regulon " + regulog.Regulons[i].GenomeName + "created");
        //            }
        //            Console.Log("Vectors for regulog with regulator " + regulog.RegulatorName + "created");
        //        }
        //    });
        //}

        //private static RegulogInfo[] ParseRegulog(
        //    string text
        //)
        //{
        //    #region Parse CSV into records.
        //    CsvIO csv = new CsvIO();
        //    StringReader reader = new StringReader(text);
        //    string[][] records = csv.Read(reader);
        //    #endregion

        //    #region Split records up into a collection of tables.
        //    List<string[]> genomeRecords = new List<string[]>();
        //    List<string[]> regulogRecords = new List<string[]>();
        //    List<string[]> regulonRecords = new List<string[]>();
        //    List<string[]> geneRecords = new List<string[]>();
        //    List<string[]> regulatorRecords = new List<string[]>();
        //    List<string[]> siteRecords = new List<string[]>();

        //    List<string[]>[] tables = { genomeRecords, regulogRecords, regulonRecords, geneRecords, regulatorRecords, siteRecords };
        //    string[] tableNames = { "genomes", "regulogs", "regulons", "genes", "regulators", "sites" };

        //    List<string[]> currentTable = null;

        //    foreach (string[] record in records)
        //    {
        //        if (record.Length == 0 || Enumerable.All(record, delegate(string r) { return string.IsNullOrEmpty(r); })) continue;

        //        int nameIndex = tableNames.IndexOf(record[0].ToLowerCase());

        //        if (nameIndex >= 0)
        //        {
        //            currentTable = tables[nameIndex];
        //        }
        //        else
        //        {
        //            currentTable.Add(record);
        //        }
        //    }
        //    #endregion

        //    #region Parse tables into lists of xyzInfo objects
        //    GenomeInfo[] genomes = Objects.DeserializeRecords(genomeRecords)
        //                                  .Map<JSObject, GenomeInfo>(ParseGenome);
        //    RegulogInfo[] regulogs = Objects.DeserializeRecords(regulogRecords).Map<JSObject, RegulogInfo>(RegulogInfo.Parse);
        //    RegulonInfo[] regulons = Objects.DeserializeRecords(regulonRecords).Map<JSObject, RegulonInfo>(RegulonInfo.Parse);
        //    GeneInfo[] genes = Objects.DeserializeRecords(geneRecords).Map<JSObject, GeneInfo>(GeneInfo.Parse);
        //    RegulatorInfo[] regulators = Objects.DeserializeRecords(regulatorRecords).Map<JSObject, RegulatorInfo>(RegulatorInfo.Parse);
        //    SiteInfo[] sites = Objects.DeserializeRecords(siteRecords).Map<JSObject, SiteInfo>(SiteInfo.Parse);
        //    #endregion

        //    // Wire the records into the regulogs.
        //    Dictionary<int, List<GeneInfo>> regulogGenes = PartitionGenesByRegulog(regulons, genes);
        //    Dictionary<int, List<SiteInfo>> regulogSites = PartitionSitesByRegulog(regulons, sites);
        //    Dictionary<int, List<RegulatorInfo>> regulogRegulators = PartitionRegulatorsByRegulog(regulons, regulators);
        //    Dictionary<int, List<GenomeInfo>> regulogGenomes = PartitionGenomesByRegulog(regulons, genomes);

        //    foreach (RegulogInfo regulog in regulogs)
        //    {
        //        int regulogId = regulog.RegulogId;

        //        regulog.Regulons = regulons.Filter(delegate(RegulonInfo regulon)
        //        {
        //            return regulogId == regulon.RegulogId;
        //        });
        //        regulog.Genes = regulogGenes[regulogId];
        //        regulog.Sites = regulogSites[regulogId];
        //        regulog.Regulators = regulogRegulators[regulogId];
        //    }

        //    // Add the genomes to the data store.
        //    foreach (GenomeInfo genome in genomes)
        //    {
        //        genomeCollection.AddOrReplace(genome);
        //    }

        //    // Connect the Relulons to their associated Genomes.
        //    foreach (RegulonInfo regulon in regulons)
        //    {
        //        GenomeInfo genome = genomeCollection.Get(LocalFile, regulon.GenomeId);

        //        if (genome == null)
        //        {
        //            GenomeStats gStats = new GenomeStats(
        //                regulon.GenomeId,
        //                regulon.GenomeName,
        //                0, 0, 0, 0, 0
        //            );
        //            genome = new GenomeInfo(gStats);
        //        }

        //        genome.AddRegulon(regulon);
        //    }

        //    // Done
        //    return regulogs;
        //}

        //static GenomeInfo ParseGenome(JSObject obj)
        //{
        //    return new GenomeInfo(GenomeStats.Parse(obj), DataLayer.LocalFile);
        //}

        //private static Dictionary<int, List<GeneInfo>> PartitionGenesByRegulog(RegulonInfo[] regulons, GeneInfo[] genes)
        //{
        //    Dictionary<int, List<GeneInfo>> map = new Dictionary<int, List<GeneInfo>>();

        //    foreach (RegulonInfo regulon in regulons)
        //    {
        //        int regulogId = regulon.RegulogId;
        //        int regulonId = regulon.RegulonId;

        //        GeneInfo[] genes_ = genes.Filter(delegate(GeneInfo g) { return g.RegulonId == regulonId; });

        //        if (map.ContainsKey(regulogId))
        //        {
        //            foreach (GeneInfo g in genes_)
        //            {
        //                map[regulogId].Add(g);
        //            }
        //        }
        //        else
        //        {
        //            map[regulogId] = (List<GeneInfo>)genes_;
        //        }
        //    }
        //    return map;
        //}

        //private static Dictionary<int, List<SiteInfo>> PartitionSitesByRegulog(RegulonInfo[] regulons, SiteInfo[] sites)
        //{
        //    Dictionary<int, List<SiteInfo>> map = new Dictionary<int, List<SiteInfo>>();

        //    foreach (RegulonInfo regulon in regulons)
        //    {
        //        int regulogId = regulon.RegulogId;
        //        int regulonId = regulon.RegulonId;

        //        SiteInfo[] sites_ = sites.Filter(delegate(SiteInfo s) { return s.RegulonId == regulonId; });

        //        if (map.ContainsKey(regulogId))
        //        {
        //            foreach (SiteInfo g in sites_)
        //            {
        //                map[regulogId].Add(g);
        //            }
        //        }
        //        else
        //        {
        //            map[regulogId] = (List<SiteInfo>)sites_;
        //        }
        //    }
        //    return map;
        //}

        //private static Dictionary<int, List<RegulatorInfo>> PartitionRegulatorsByRegulog(RegulonInfo[] regulons, RegulatorInfo[] regulators)
        //{
        //    Dictionary<int, List<RegulatorInfo>> map = new Dictionary<int, List<RegulatorInfo>>();

        //    foreach (RegulonInfo regulon in regulons)
        //    {
        //        int regulogId = regulon.RegulogId;
        //        int regulonId = regulon.RegulonId;

        //        RegulatorInfo[] regulators_ = regulators.Filter(delegate(RegulatorInfo s) { return s.RegulonId == regulonId; });

        //        if (map.ContainsKey(regulogId))
        //        {
        //            foreach (RegulatorInfo g in regulators_)
        //            {
        //                map[regulogId].Add(g);
        //            }
        //        }
        //        else
        //        {
        //            map[regulogId] = (List<RegulatorInfo>)regulators_;
        //        }
        //    }
        //    return map;
        //}

        //private static Dictionary<int, List<GenomeInfo>> PartitionGenomesByRegulog(RegulonInfo[] regulons, GenomeInfo[] genomes)
        //{
        //    Dictionary<int, List<GenomeInfo>> map = new Dictionary<int, List<GenomeInfo>>();

        //    foreach (RegulonInfo regulon in regulons)
        //    {
        //        int regulogId = regulon.RegulogId;
        //        int genomeId = regulon.GenomeId;

        //        GenomeInfo[] genomes_ = genomes.Filter(delegate(GenomeInfo s) { return s.GenomeId == genomeId; });

        //        if (map.ContainsKey(regulogId))
        //        {
        //            foreach (GenomeInfo g in genomes_)
        //            {
        //                map[regulogId].Add(g);
        //            }
        //        }
        //        else
        //        {
        //            map[regulogId] = (List<GenomeInfo>)genomes_;
        //        }
        //    }
        //    return map;
        //}
		#endif
        #endregion

        #region Helper methods
        /// <summary> Parses the content of a file from json into an array of JavaScript Objects
		/// </summary>
		/// <param name="json"></param>
		/// <param name="topLevelObject"></param>
		/// <returns></returns>

		static JSObject[] GetRecords ( string json, string topLevelObject ) {
			object result = ( (JSObject) Json.Parse( json ) )[topLevelObject];

			return result is Array ? (JSObject[]) result : new JSObject[] { (JSObject) result };
		}

		/// <summary> Gets the contents of a file as an array of javascript objects.
		/// </summary>
		/// <param name="fileName"> The name of a JSON-formatted text file. </param>
		/// <param name="process"> A delegate that will process the resulting array of JavaScript objects. </param>
		/// <returns></returns>

		static void ReadRecords ( string fileName, Action<JSObject[]> process ) {
			Util.ReadAllText( fileName, delegate( string json ) {
				RegExp pattern = new RegExp( "[\"'](\\w+)[\"']" );
				string topLevelObject = pattern.Exec( json )[1];
				process( GetRecords( json, topLevelObject ) );
			} );
		}

		/// <summary> Returns true iff the supplied argument is really a number.
		/// </summary>
		/// <param name="x"> A (possibly) numeric value. </param>
		/// <returns> Returns true iff the supplied argument is really a number. </returns>

		private bool IsReallyNumber ( Number x ) {
			return x == x + 0;
		}
		#endregion

		#region Genomes
		void ListGenomes () {
			string T = "ListGenomes";
			service.ListGenomes( delegate( ServiceResponse<Genome[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/genomes.json", delegate( JSObject[] rawData ) {
					try {
						Genome[] expected = rawData.Map<JSObject, Genome>( Genome.Parse );
						Genome[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<Genome>( Genome.OrderById );
						actual.Sort<Genome>( Genome.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							// Assert.IsTrue( IsReallyNumber( expected[i].GenomeId ), CodeBehindSubclass + ": valid genome Id" );
							Assert.IsTrue( IsReallyNumber( actual[i].GenomeId ), T + ": valid genome Id" );
							Assert.IsTrue( IsReallyNumber( actual[i].TaxonomyId ), T + ": valid taxonomy Id" );
							Assert.IsTrue( actual[i].Name.Length > 0, T + ": valid name" );
							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": genomes identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				} );
			} );
		}
		#endregion

		#region Regulogs
		void ListRegulogs () {
			string T = "ListRegulogs";
			service.ListRegulogs( RegulogCollectionType.TaxGroup, 1, delegate( ServiceResponse<Regulog[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );
				ReadRecords( "/UnitTests/Resources/regulogs.json", delegate( JSObject[] rawData ) {
					try {
						Regulog[] expected = rawData.Map<JSObject, Regulog>( Regulog.Parse );
						Regulog[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<Regulog>( Regulog.OrderById );
						actual.Sort<Regulog>( Regulog.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							// Assert.IsTrue( IsReallyNumber( expected[i].GenomeId ), CodeBehindSubclass + ": valid genome Id" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulogId ), T + ": valid regulog Id" );
							Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				} );
			} );
		}
		#endregion

		#region RegulogCollections
		void ListRegulogCollections () {
			string T = "ListRegulogCollections";
			service.ListRegulogCollections( RegulogCollectionType.TaxGroup, delegate( ServiceResponse<RegulogCollection[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/regulogCollections.json", delegate( JSObject[] rawData ) {
					try {
						RegulogCollection[] expected = rawData.Map<JSObject, RegulogCollection>( RegulogCollection.Parse );
						RegulogCollection[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<RegulogCollection>( RegulogCollection.OrderById );
						actual.Sort<RegulogCollection>( RegulogCollection.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].CollectionId ), T + ": valid regulog Id" );
							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				} );
			} );
		}
		#endregion

		#region Sites
		void ListSitesInRegulon () {
			string T = "ListSitesInRegulon";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulonId=6423'
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulogId=647'

			service.ListSitesInRegulon( 6423, delegate( ServiceResponse<Site[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/sitesByRegulon.json", delegate( JSObject[] rawData ) {
					try {
						Site[] expected = rawData.Map<JSObject, Site>( Site.Parse );
						Site[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<RegulogCollection>( RegulogCollection.OrderById );
						actual.Sort<RegulogCollection>( RegulogCollection.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].GeneVIMSSId ), T + ": valid GeneVIMSSId" );
							Assert.IsTrue( IsReallyNumber( actual[i].Position ), T + ": valid Position" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
							Assert.IsTrue( IsReallyNumber( actual[i].Score ), T + ": valid Score" );
							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": serviceResponse identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				} );
			} );
		}

		void ListSitesInRegulog () {
			string T = "ListSitesInRegulog";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulonId=6423'
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulogId=647'

			service.ListSitesInRegulog( 647, delegate( ServiceResponse<Site[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );
				ReadRecords( "/UnitTests/Resources/sitesByRegulog.json", delegate( JSObject[] rawData ) {
					try {
						Site[] expected = rawData.Map<JSObject, Site>( Site.Parse );
						Site[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<RegulogCollection>( RegulogCollection.OrderById );
						actual.Sort<RegulogCollection>( RegulogCollection.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].GeneVIMSSId ), T + ": valid GeneVIMSSId" );
							Assert.IsTrue( IsReallyNumber( actual[i].Position ), T + ": valid Position" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
							Assert.IsTrue( IsReallyNumber( actual[i].Score ), T + ": valid Score" );
							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": serviceResponse identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				} );
			} );
		}
		#endregion

		#region GetRegulog
		void GetRegulog () {
			string T = "GetRegulog";
			int regulogId = 621;

			service.GetRegulog( regulogId, delegate( ServiceResponse<Regulog> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Util.ReadAllText( "/UnitTests/Resources/regulog" + regulogId + ".json", delegate( string json ) {
					try {
						Regulog expected = Regulog.Parse( (JSObject) Json.Parse( json ) );
						Regulog actual = response.Content;

						Assert.NumbersEqual( regulogId, actual.RegulogId, T + ": correct regulog Id" );
						Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
						Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulogs identical" );

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				} );
			} );
		}
		#endregion

		#region GetRegulon
		void GetRegulon () {
			string T = "GetRegulon";
			int regulonId = 6423;

			service.GetRegulon( regulonId, delegate( ServiceResponse<Regulon> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				Util.ReadAllText( "/UnitTests/Resources/regulon" + regulonId + ".json", delegate( string json ) {
					try {
						Regulon expected = Regulon.Parse( (JSObject) Json.Parse( json ) );
						Regulon actual = response.Content;

						Assert.NumbersEqual( regulonId, actual.RegulonId, T + ": correct regulon Id" );

						Assert.IsTrue( IsReallyNumber( actual.RegulonId ), T + ": numeric regulon Id" );
						Assert.IsTrue( IsReallyNumber( actual.RegulogId ), T + ": numeric regulog Id" );
						Assert.IsTrue( IsReallyNumber( actual.GenomeId ), T + ": numeric genome Id" );

						Assert.IsTrue( Objects.Identical( expected, actual ), T + ": regulons identical" );

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				} );
			} );
		}
		#endregion

		#region ListRegulons
		void ListRegulonsInGenome () {
			string T = "ListRegulonsInGenome";
			int genomeId = 1;

			service.ListRegulonsInGenome( genomeId, delegate( ServiceResponse<Regulon[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/regulonsInGenome" + genomeId + ".json", delegate( JSObject[] rawData ) {
					try {
						Regulon [] expected = rawData.Map<JSObject, Regulon>( Regulon.Parse );
						Regulon [] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<Regulon>( Regulon.OrderById );
						actual.Sort<Regulon>( Regulon.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.NumbersEqual( genomeId, actual[i].GenomeId, T + ": correct regulon Id" );

							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": numeric regulon Id" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulogId ), T + ": numeric regulog Id" );
							Assert.IsTrue( IsReallyNumber( actual[i].GenomeId ), T + ": numeric genome Id" );

							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulons identical" );
						}
						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
						Console.Log( ex.StackTrace );
					}
				} );
			} );
		}

		void ListRegulonsInRegulog () {
			string T = "ListRegulonsInRegulog";
			int regulogId = 621;

			service.ListRegulonsInRegulog( regulogId, delegate( ServiceResponse<Regulon[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/regulonsInRegulog" + regulogId + ".json", delegate( JSObject[] rawData ) {
					try {
						Regulon [] expected = rawData.Map<JSObject, Regulon>( Regulon.Parse );
						Regulon[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<Regulon>( Regulon.OrderById );
						actual.Sort<Regulon>( Regulon.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.NumbersEqual( regulogId, actual[i].RegulogId, T + ": correct regulon Id" );

							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": numeric regulon Id" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulogId ), T + ": numeric regulog Id" );
							Assert.IsTrue( IsReallyNumber( actual[i].GenomeId ), T + ": numeric genome Id" );

							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulons identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}
				} );
			} );
		}
		#endregion

		#region ListRegulators
		void ListRegulatorsInRegulon () {
			string T = "ListRegulatorsInRegulon";
			int regulonId = 6423;

			service.ListRegulatorsInRegulon( regulonId, delegate( ServiceResponse<Regulator[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error not null" );

				ReadRecords( "/UnitTests/Resources/regulatorsInRegulon" + regulonId + ".json", delegate( JSObject[] rawData ) {
					try {
						Regulator [] expected = rawData.Map<JSObject, Regulator>( Regulator.Parse );
						Regulator[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length not equal" );

						expected.Sort<Regulator>( Regulator.OrderById );
						actual.Sort<Regulator>( Regulator.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.NumbersEqual( regulonId, actual[i].RegulonId, T + ": incorrect regulon Id" );

							Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid numeric Vimssid" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid numeric genome Id" );

							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulators not identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}
				} );
			} );
		}

		void ListRegulatorsInRegulog () {
			string T = "ListRegulatorsInRegulog";
			int regulogId = 647;

			service.ListRegulatorsInRegulog( regulogId, delegate( ServiceResponse<Regulator[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/regulatorsInRegulog" + regulogId + ".json", delegate( JSObject[] rawData ) {
					try {
						Regulator [] expected = rawData.Map<JSObject, Regulator>( Regulator.Parse );
						Regulator[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length not equal" );

						expected.Sort<Regulator>( Regulator.OrderById );
						actual.Sort<Regulator>( Regulator.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid numeric VimssId" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid numeric RegulonId" );

							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": regulators identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}
				} );
			} );
		}
		#endregion

		#region Genes
		void ListGenesInRegulon () {
			string T = "ListGenesInRegulon";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulonId=6423'
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/sites?regulogId=647'

			int regulonId = 6423;
			service.ListGenesInRegulon( regulonId, delegate( ServiceResponse<Gene[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error != null" );

				ReadRecords( "/UnitTests/Resources/genesInRegulon6423.json", delegate( JSObject[] rawData ) {
					try {
						Gene[] expected = rawData.Map<JSObject, Gene>( Gene.Parse );
						Gene[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length unequal" );

						expected.Sort<Gene>( Gene.OrderById );
						actual.Sort<Gene>( Gene.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.NumbersEqual( regulonId, actual[i].RegulonId, T + ": incorrect regulon id" );
							Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid VimssId" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid RegulonId" );
							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": serviceResponse not equal" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}
				} );
			} );
		}

		void ListGenesInRegulog () {
			string T = "ListGenesInRegulog";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/genes?regulonId=6423'
			//	curl -i 'http://regprecise.lbl.gov/Services/rest/genes?regulogId=647'

			service.ListGenesInRegulog( 647, delegate( ServiceResponse<Gene[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error != null" );

				ReadRecords( "/UnitTests/Resources/genesInRegulog647.json", delegate( JSObject[] rawData ) {
					try {
						Gene[] expected = rawData.Map<JSObject, Gene>( Gene.Parse );
						Gene[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length unequal" );

						expected.Sort<Gene>( Gene.OrderById );
						actual.Sort<Gene>( Gene.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].VimssId ), T + ": invalid VimssId" );
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": invalid RegulonId" );
							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": serviceResponse not equal" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}
				} );
			} );
		}
		#endregion

		#region GenomeStats
		void ListGenomeStats () {
			string T = "ListGenomeStats";

			//	curl -i 'http://regprecise.lbl.gov/Services/rest/genomeStats'

			service.ListGenomeStats( delegate( ServiceResponse<GenomeStats[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error != null" );

				ReadRecords( "/UnitTests/Resources/genomeStats.json", delegate( JSObject[] rawData ) {
					try {
						GenomeStats[] expected = rawData.Map<JSObject, GenomeStats>( GenomeStats.Parse );
						GenomeStats[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length unequal" );

						expected.Sort<GenomeStats>( GenomeStats.OrderById );
						actual.Sort<GenomeStats>( GenomeStats.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].GenomeId ), T + ": invalid GenomeId" );
							Assert.IsTrue( IsReallyNumber( actual[i].TaxonomyId ), T + ": invalid TaxonomyId" );
							Assert.IsTrue( IsReallyNumber( actual[i].TfRegulonCount ), T + ": invalid TfRegulonCount" );
							Assert.IsTrue( IsReallyNumber( actual[i].TfSiteCount ), T + ": invalid TfSiteCount" );
							Assert.IsTrue( IsReallyNumber( actual[i].RnaRegulonCount ), T + ": invalid RnaRegulonCount" );
							Assert.IsTrue( IsReallyNumber( actual[i].RnaSiteCount ), T + ": invalid RnaSiteCount" );
							Assert.IsTrue( Objects.Identical( expected[i], actual[i] ), T + ": serviceResponse not equal" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}
				} );
			} );
		}
		#endregion

		#region SearchRegulogs
		void SearchRegulonsByRegulator () {
			string T = "SearchRegulonsByRegulator";
			service.SearchRegulons( ObjectTypes.Regulator, "argR", delegate( ServiceResponse<RegulonReference[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/searchRegulons_Regulator_argR.json", delegate( JSObject[] rawData ) {
					try {
						RegulonReference[] expected = rawData.Map<JSObject, RegulonReference>( RegulonReference.Parse );
						RegulonReference[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<RegulonReference>( RegulonReference.OrderById );
						actual.Sort<RegulonReference>( RegulonReference.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
							Assert.StringsEqual( "argR".ToLowerCase(), actual[i].RegulatorName.ToLowerCase(), T + ": correct RegulatorName" );
							Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}

				} );
			} );
		}

		void SearchRegulonsByGene () {
			string T = "SearchRegulonsByGene";
			service.SearchRegulons( ObjectTypes.Gene, "SO_2706", delegate( ServiceResponse<RegulonReference[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/searchRegulons_Gene_SO_2706.json", delegate( JSObject[] rawData ) {
					try {
						RegulonReference[] expected = rawData.Map<JSObject, RegulonReference>( RegulonReference.Parse );
						RegulonReference[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<RegulonReference>( RegulonReference.OrderById );
						actual.Sort<RegulonReference>( RegulonReference.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
							Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}
				} );
			} );
		}
		#endregion

		#region SearchExtRegulons
		void SearchExtRegulonsByRegulator () {
			string T = "SearchExtRegulonsByRegulator";

			int ncbiTaxonomyId = 882;
			string [] locusTags = { "DVU0043", "DVU0694", "DVU3234", "DVU3233", "DVU0910", "DVU3384", "DVU0863" };

			service.SearchExtRegulons( ncbiTaxonomyId, locusTags, delegate( ServiceResponse<RegulonReference[]> response ) {
				Assert.IsTrue( response.Error == null, T + ": " + "response.Error == null" );

				ReadRecords( "/UnitTests/Resources/searchRegulons_Regulator_argR.json", delegate( JSObject[] rawData ) {
					try {
						RegulonReference[] expected = rawData.Map<JSObject, RegulonReference>( RegulonReference.Parse );
						RegulonReference[] actual = response.Content;

						Assert.NumbersEqual( expected.Length, actual.Length, T + ": " + "array length equal" );

						expected.Sort<RegulonReference>( RegulonReference.OrderById );
						actual.Sort<RegulonReference>( RegulonReference.OrderById );

						for ( int i = 0; i < expected.Length; i++ ) {
							Assert.IsTrue( IsReallyNumber( actual[i].RegulonId ), T + ": valid RegulonId" );
							Assert.StringsEqual( "argR".ToLowerCase(), actual[i].RegulatorName.ToLowerCase(), T + ": correct RegulatorName" );
							Assert.IsTrue( Regulog.Identical( expected[i], actual[i] ), T + ": regulogs identical" );
						}

						Util.AppendResult( T, true );
					}
					catch ( Exception ex ) {
						Util.AppendResult( T + ": error = " + ex.StackTrace, false );
					}
				} );
			} );
		}
		#endregion
	}
}
