﻿using RegPrecise;
using System;
using System.Collections.Generic;
using System.Html;
using System.Net;
using System.Serialization;
using SystemQut.ServiceModel;
using SystemQut;
using SystemQut.Xml;
using SystemQut.IO;

namespace Test.Client
{
    /// <summary>
    /// Runs tests for retriving data from GenBank Entrez, based upon a regulog from RegPrecise
    /// </summary>
    class TestRetrieveFromEntrez
    {

		// RegPrecise service to retrieve regulogs
        private static IRegPreciseService service = new RegPreciseService( "/RegPrecise.svc", false );

        // All the organism/gene combinations and their associated Entrez IDs
        private static Dictionary<string, List<int>> associations = new Dictionary<string, List<int>>();

        // All the loaded Entrez IDs and their associated descriptions
        private static Dictionary<int, string> downloadedDescriptions = new Dictionary<int, string>();

        /// <summary>
        /// Runs the tests
        /// </summary>
        public static void RunTests () {

            // The regulog of FUR containing E coli will be used
			int regulogId = 1112;

            // To store the regulog, regulons and genes retrieved from RegPrecise
            Regulog regulog;
            Dictionary<int, Regulon> regulons = new Dictionary<int, Regulon>();
            Gene[] genes = new Gene[0];

            // Get the regulog first
            service.GetRegulog( regulogId, delegate( ServiceResponse<Regulog> response1 ) {

                // If there was an error connecting to RegPrecise
                if (response1.Error != null)
                {
                    Console.Log("Entrez: Error in retriving regulog " + regulogId + " from RegPreciseService: " + response1.Error);
                    Util.AppendResult("Entrez: Error in retriving regulog " + regulogId + " from RegPreciseService: " + response1.Error, false);
                    return;
                }
				try {

                    // Store the retrieved regulog
                    regulog = response1.Content;

                    Console.Log("Entrez: Loaded regulog " + regulogId + " from RegPreciseService");
                    Util.AppendResult("Entrez: Loaded regulog " + regulogId + " from RegPreciseService" , true);

                    // Get the regulons in the retrieved regulog
                    service.ListRegulonsInRegulog( regulog.RegulogId, delegate( ServiceResponse<Regulon[]> response2 ) {

                        // If there was an error connecting to RegPrecise
					    if ( response2.Error != null ) {
                            Console.Log("Entrez: Error in retriving regulons for regulog " + regulogId + " from RegPreciseService: " + response2.Error);
                            Util.AppendResult("Entrez: Error in retriving regulons for regulog " + regulogId + " from RegPreciseService: " + response2.Error, false);
                            return;
					    }

                        try {

                            // Store the regulons in the dictionary, using their
                            // ID as the key
					        for (int i = 0; i < response2.Content.Length; i++) {
                                Regulon currentRegulon = response2.Content[i];
                                regulons[currentRegulon.RegulonId] = currentRegulon;
                            }

                            Console.Log("Entrez: Loaded regulons for regulog " + regulogId + " from RegPreciseService");
                            Util.AppendResult("Entrez: Loaded regulons for regulog " + regulogId + " from RegPreciseService" , true);

                        } catch (Exception ex) {
                            Console.Log("Entrez: Error in retriving regulons for regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace);
                            Util.AppendResult("Entrez: Error in retriving regulons for regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace, false);
                        }
				    } );

                    // Get the genes in the retrieved regulog
                    service.ListGenesInRegulog( regulog.RegulogId, delegate( ServiceResponse<Gene[]> response3 ) {

                        // If there was an error connecting to RegPrecise
					    if ( response3.Error != null ) {
                            Console.Log("Entrez: Error in retriving genes for regulog " + regulogId + " from RegPreciseService: " + response3.Error);
                            Util.AppendResult("Entrez: Error in retriving genes for regulog " + regulogId + " from RegPreciseService: " + response3.Error, false);
                            return;
					    }

                        try {

                            // Store the retrieved genes
					        genes = response3.Content;

                            Console.Log("Entrez: Loaded genes for regulog " + regulogId + " from RegPreciseService");
                            Util.AppendResult("Entrez: Loaded genes for regulog " + regulogId + " from RegPreciseService" , true);

                        } catch (Exception ex) {
                            Console.Log("Entrez: Error in retriving genes for regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace);
                            Util.AppendResult("Entrez: Error in retriving genes for regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace, false);
                        }
                    } );

                } catch (Exception ex) {
                    Console.Log("Entrez: Error in retriving regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace);
                    Util.AppendResult("Entrez: Error in retriving regulog " + regulogId + " from RegPreciseService: " + ex.StackTrace, false);
                }
            });

            // Wait for five seconds so that the above is complete before
            // attempting to retrieve IDs from Entrez
            Script.SetTimeout((Action) delegate {

                // For each retrieved gene...
                for (int i = 0; i < genes.Length; i++) {
                    Gene currentGene = genes[i];

                    // Get its associated regulon using the regulon ID
                    Regulon currentRegulon = regulons[currentGene.RegulonId];

                    // Retrieve all records for the combination of organism and
                    // gene
                    RetrieveFromEntrezSearch(currentRegulon.GenomeName, currentGene.Name, false);
                }

            }, 5000);

            // Wait for two minutes so that the above is complete. The calls to
            // retrieve Entrez summaries are delayed until all the searches are
            // complete
            //
            Script.SetTimeout((Action) delegate {

                // Go through all of the organism/gene combinations and their
                // associated IDs
                foreach (KeyValuePair<string, List<int>> list in associations) {

                    // If there were IDs given to this combination, list them
                    if (list.Value.Length > 0) {
                        Console.Log("Entrez: " + list.Key + " has the following descriptions associated with it:");
                        Util.AppendResult("Entrez: " + list.Key + " has the following descriptions associated with it:", true);

                        // Go through the IDs and get their description from
                        // the list
                        foreach (int id in list.Value) {
                            Console.Log("- (" + id + ") " + downloadedDescriptions[id]);
                            Util.AppendResult("- (" + id + ") " + downloadedDescriptions[id], true);
                        }

                    // Otherwise say there were no descriptions
                    } else {
                        Console.Log("Entrez: " + list.Key + " had no descriptions retrieved for it");
                        Util.AppendResult("Entrez: " + list.Key + " had no descriptions retrieved for it", true);
                    }
                }
            }, 120000);
        }

        /// <summary>
        /// Queries the Genbank Entrez service for all entries that contain both the specified organism and gene
        /// </summary>
        /// <param name="organism">The organism to search for</param>
        /// <param name="gene">The gene to search for</param>
        /// <param name="show">Whether to add the log to the test page</param>
        private static void RetrieveFromEntrezSearch(string organism, string gene, bool show)
        {
            // Create a new request
            XmlHttpRequest req = new XmlHttpRequest();
            req.OnReadyStateChange = delegate()
            {
                if (req.ReadyState == ReadyState.Loaded)
                {
                    // If an OK status is returned
                    if (req.Status == 200)
                    {
                        Console.Log("Entrez (" + gene + " of " + organism + "): Able to connect with 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gene&term=" + organism.Replace(" ", "+") + "[Organism]+" + gene + "' - Status: " + req.Status);
                        if (show) Util.AppendResult("Entrez (" + gene + " of " + organism + "): Able to connect with 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gene&term=" + organism.Replace(" ", "+") + "[Organism]+" + gene + "' - Status: " + req.Status, true);

                        // Start reading the retrieved XML
                        XmlReader reader = XmlReader.Create(new StringReader(req.ResponseText));

                        // There are multiple count tags in the returned XML.
                        // Knowing when the first one is found allows checking
                        // for things that occur directly after it that are
                        // needed
                        bool foundFirstCount = false;

                        // To count how many IDs were retrieved
                        int count = 1;

                        // Set up the list for this organism/gene combination
                        associations[gene + " of " + organism] = new List<int>();

                        // While there is text to be read...
                        while (reader.Read())
                        {
                            // If the current tag is "Count"...
                            if (reader.Name == "Count" && !String.IsNullOrWhiteSpace(reader.Value)) {
                                Console.Log("Entrez (" + gene + " of " + organism + "): count is " + reader.Value);
                                if (show) Util.AppendResult("Entrez (" + gene + " of " + organism + "): count is " + reader.Value, true);

                                // Mark that we've found the first count. This
                                // while loop should be terminated before
                                // reaching the next one
                                foundFirstCount = true;
                            }

                            // Start checking for ID if the first count has
                            // been found
                            if (foundFirstCount) {

                                // If the current tag is "Id"...
                                if (reader.Name == "Id" && !String.IsNullOrWhiteSpace(reader.Value)) {

                                    // Parse the value as an integer
                                    int id = int.Parse(reader.Value);

                                    // Add the ID to the correct list for this
                                    // organism/gene combination
                                    associations[gene + " of " + organism].Add(id);

                                    // If the description has not already been
                                    // downloaded...
                                    if (string.IsNullOrWhiteSpace(downloadedDescriptions[id])) {

                                        // Store a dummy value, as retrieving a summary
                                        // will be delayed until all of the requests from
                                        // this function have been completed
                                        downloadedDescriptions[id] = "waiting";

                                        // Retrieve the corresponding summary for this ID
                                        RetrieveFromEntrezSummary(id, false);
                                    }
                                    Console.Log("Entrez (" + gene + " of " + organism + "): ID " + count + " is " + id);
                                    if (show) Util.AppendResult("Entrez (" + gene + " of " + organism + "): ID " + count + " is " + id, true);

                                    // Increase the ID count
                                    count = count + 1;

                                // If "TranslationSet" is reached, all the IDs have been
                                // passed and so there is no need to read the rest of the
                                // XML
                                } else if (reader.Name == "TranslationSet") {
                                    break;
                                }
                            }
                        }
                    }

                    // Otherwise if there's an error
                    else
                    {
                        Console.Log("Entrez (" + gene + " of " + organism + "): Error received: " + req.Status);
                        Util.AppendResult("Entrez (" + gene + " of " + organism + "): Error received: " + req.Status, false);
                    }
                }
            };
            // Set the URL to search for
            req.Open(HttpVerb.Post, "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gene&term=" + organism.Replace(" ", "+") + "[Organism]+" + gene);

            // Send the request
            req.Send();
        }

        /// <summary>
        /// Queries the Genbank Entrez service for a summary of the entry that corresponds to the given ID
        /// </summary>
        /// <param name="id">The ID to search for</param>
        /// <param name="show">Whether to add the log to the test page</param>
        private static void RetrieveFromEntrezSummary(int id, bool show) {

            // Create a new request
            XmlHttpRequest req = new XmlHttpRequest();
            req.OnReadyStateChange = delegate()
            {
                if (req.ReadyState == ReadyState.Loaded)
                {
                    // If an OK status is returned
                    if (req.Status == 200)
                    {
                        Console.Log("Entrez (" + id + "): Able to connect with 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gene&id=" + id + "' - Status: " + req.Status);
                        if (show) Util.AppendResult("Entrez (" + id + "): Able to connect with 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gene&id=" + id + "' - Status: " + req.Status, true);

                        // Start reading the retrieved XML
                        XmlReader reader = XmlReader.Create(new StringReader(req.ResponseText));

                        // While there is text to be read...
                        while (reader.Read())
                        {
                            // If the current tag is "Description"...
                            if (reader.Name == "Description" && !String.IsNullOrWhiteSpace(reader.Value)) {
                                Console.Log("Entrez (" + id + "): description is " + reader.Value);
                                if (show) Util.AppendResult("Entrez (" + id + "): description is " + reader.Value, true);

                                // Store the description in the list against its ID
                                downloadedDescriptions[id] = reader.Value;

                                // There is no need to check the XML further, so stop the
                                // loop
                                break;
                            }
                        }
                    }

                    // Otherwise if there's an error
                    else
                    {
                        Console.Log("Entrez (" + id + "): Error received: " + req.Status);
                        if (show) Util.AppendResult("Entrez (" + id + "): Error received: " + req.Status, false);
                    }
                }
            };
            // Set the URL to search for
            req.Open(HttpVerb.Post, "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gene&id=" + id);

            // Send the request
            req.Send();
        }
    }
}
