﻿using System;
using System.Collections.Generic;

using System.Html;
using SystemQut.Svg;
using SystemQut.Controls;
using SystemQut;

namespace Test.Client {

	/// <summary> Unit tests for Drag and drop operations.
	/// </summary>

	public class TestDragDrop {
		internal void RunAllTests () {
			InitDragHandlers();

			Action[] tests = {
				ThreeBoxesInADiv,
                TenBoxesInTwoDivColumns,
			};

			for ( int i = 0; i < tests.Length; i++ ) {
				Action test = tests[i];

				try {
					test();
				}
				catch ( Exception ex ) {
					Window.Alert( "Unhandled error:\n" + ex.Message + "\n" + ex.StackTrace );
				}
			}
		}

		private void ThreeBoxesInADiv () {
			DivElement div = (DivElement) Document.CreateElement( "div" );
			Document.Body.AppendChild( div );

			for ( int i = 0; i < 3; i++ ) {
				Element span = Document.CreateElement( "span" );
				span.ID = "Span " + i;
				div.AppendChild( span );

				SvgSvg block = Svg.CreateOuterSvgElement( span, null );

				block.Width = new SvgLength( 2.5, SvgLengthType.IN );
				block.Height = new SvgLength( 2.5, SvgLengthType.IN );
				block.ViewBox = new SvgRect( -100, -100, 200, 200 );
				block.DomElement.Style.BackgroundColor = "white";
				block.DomElement.Style.Margin = "3px";
				block.DomElement.Style.Border = "1px dashed black";

				SvgText text = new SvgText( block, null );
				text.FontSize = Svg.ConvertLength( 0.25, 2.5, 200 ).ToString();
				text.Text = "Block " + i;
				text.TextAnchor = TextAnchorType.middle;

				span.SetAttribute( "draggable", "true" );

				foreach ( Pair<MouseEventType, ElementEventListener> pair in DragEventListeners ) {
					span.AddEventListener( pair.Key, pair.Value, false );
				}
			}
		}

        //We'll use this to store the names of the columns, so we can use this
        //to identify them later when we're moving stuff between, in and out of
        //the columns
        private string[] ColumnNames = { };

        private int blockWidth = 100;
        private int blockHeight = 100;

        private void TenBoxesInTwoDivColumns()
        {
            InitColumnDragHandlers();
            DivElement mainDiv = (DivElement)Document.CreateElement("mainDiv");
            Document.Body.AppendChild(mainDiv);
            DivElement leftColumn = (DivElement)Document.CreateElement("leftColumn");
            this.ColumnNames[0] = leftColumn.NodeName;
            leftColumn.Style.Width = "25%";
            leftColumn.Style.CssFloat = "left";
            leftColumn.Style.BackgroundColor = "red";
            foreach (Pair<MouseEventType, ElementEventListener> pair in ColumnDragEventListeners)
            {
                leftColumn.AddEventListener(pair.Key, pair.Value, false);
            }
            mainDiv.AppendChild(leftColumn);
            DivElement rightColumn = (DivElement)Document.CreateElement("rightColumn");
            this.ColumnNames[1] = rightColumn.NodeName;
            rightColumn.Style.Width = "25%";
            rightColumn.Style.CssFloat = "left";
            rightColumn.Style.BackgroundColor = "blue";
            foreach (Pair<MouseEventType, ElementEventListener> pair in ColumnDragEventListeners)
            {
                rightColumn.AddEventListener(pair.Key, pair.Value, false);
            }
            mainDiv.AppendChild(rightColumn);

			for ( int i = 0; i < 4; i++ ) {
				Element span = Document.CreateElement( "span" );
				span.ID = "Left Span " + i;

				leftColumn.AppendChild( span );

				SvgSvg block = Svg.CreateOuterSvgElement( span, null );

				block.Width = new SvgLength( 1.25, SvgLengthType.IN );
				block.Height = new SvgLength( 1.25, SvgLengthType.IN );
				block.ViewBox = new SvgRect( -50, -50, blockWidth, blockHeight );
				block.DomElement.Style.Margin = "3px";
                block.DomElement.Style.Border = "1px dashed black";

                SvgRectangle background = new SvgRectangle(block, -50, -50, blockWidth, blockHeight, null);
                background.Fill = "green";

				SvgText text = new SvgText( block, null );
				text.FontSize = Svg.ConvertLength( 0.125, 1.25, blockWidth ).ToString();
				text.Text = "Left Block " + i;
				text.TextAnchor = TextAnchorType.middle;

				span.SetAttribute( "draggable", "true" );

				foreach ( Pair<MouseEventType, ElementEventListener> pair in DragEventListeners ) {
					span.AddEventListener( pair.Key, pair.Value, false );
				}                

                block.DomElement.AddEventListener(MouseEventType.mouseover, MouseOverSvg, false);
                block.DomElement.AddEventListener(MouseEventType.mouseout, MouseOutSvg, false);
			}

			for ( int i = 0; i < 6; i++ ) {
				Element span = Document.CreateElement( "span" );
				span.ID = "Right Span " + i;

				rightColumn.AppendChild( span );

				SvgSvg block = Svg.CreateOuterSvgElement( span, null );

				block.Width = new SvgLength( 1.25, SvgLengthType.IN );
				block.Height = new SvgLength( 1.25, SvgLengthType.IN );
				block.ViewBox = new SvgRect( -50, -50, blockWidth, blockHeight );
				block.DomElement.Style.Margin = "3px";
				block.DomElement.Style.Border = "1px dashed black";

				SvgRectangle background = new SvgRectangle( block, -50, -50, blockWidth, blockHeight, null );
				background.Fill = "green";

				SvgText text = new SvgText( block, null );
				text.FontSize = Svg.ConvertLength( 0.125, 1.25, blockWidth ).ToString();
				text.Text = "Right Block " + i;
                text.TextAnchor = TextAnchorType.middle;

                span.SetAttribute("draggable", "true");

                foreach (Pair<MouseEventType, ElementEventListener> pair in DragEventListeners)
                {
                    span.AddEventListener(pair.Key, pair.Value, false);
                }

                block.DomElement.AddEventListener(MouseEventType.mouseover, MouseOverSvg, false);
                block.DomElement.AddEventListener(MouseEventType.mouseout, MouseOutSvg, false);
            }

            //Make sure the columns are all the same height
            UpdateColumnHeight(mainDiv);
        }

        private void UpdateColumnHeight(Element element)
        {
            //Reset them to auto first so we can find the column which has the
            //largest height - i.e. the column with the most boxes in it
            for (int i = 0; i < element.ChildNodes.Length; i++)
            {
                element.ChildNodes[i].Style.Height = "auto";
            }

            //Find the one with the largest height
            for (int i = 0; i < element.ChildNodes.Length; i++)
            {
                CurrentMaxColumnHeight = (Math.Max(element.ChildNodes[i].ClientHeight, CurrentMaxColumnHeight));

                //If the columns are empty and have disappeared, we need to use
                //a special value to indicate this
                if (CurrentMaxColumnHeight == 0)
                {
                    CurrentMaxColumnHeight = -1;
                }
            }

            //Set the new heights
            for (int i = 0; i < element.ChildNodes.Length; i++)
            {
                if (CurrentMaxColumnHeight == -1) {
                    element.ChildNodes[i].Style.Height = blockHeight + "px";
                }
                else
                {
                    element.ChildNodes[i].Style.Height = CurrentMaxColumnHeight + "px";
                }
            }
        }

		private static void MouseOverSvg ( ElementEvent e ) {	
			Element svgElement = e.Target;

			while ( svgElement != null && svgElement.TagName != "svg" ) {
				svgElement = svgElement.ParentNode;
			}

			if ( svgElement != null ) {
				svgElement.FirstChild.SetAttribute( "fill", "silver" );
			}
		}

		private static void MouseOutSvg ( ElementEvent e ) {
			Element svgElement = e.Target;

			while ( svgElement != null && svgElement.TagName != "svg" ) {
				svgElement = svgElement.ParentNode;
			}

			if ( svgElement != null ) {
				svgElement.FirstChild.SetAttribute( "fill", "green" );
			}
		}

		private Pair<MouseEventType, ElementEventListener>[] DragEventListeners = { };
		private Pair<MouseEventType, ElementEventListener>[] ColumnDragEventListeners = { };

		private void InitDragHandlers () {
			this.DragEventListeners[0] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragstart, DragStart );
			this.DragEventListeners[1] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragenter, DragEnter );
			this.DragEventListeners[2] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragover, DragOver );
			this.DragEventListeners[3] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragend, DragEnd );
			this.DragEventListeners[4] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragleave, DragLeave );
			this.DragEventListeners[5] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.drop, Drop );
		}

		private void InitColumnDragHandlers () {
			this.ColumnDragEventListeners[0] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragstart, ColumnDragStart );
			this.ColumnDragEventListeners[1] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragenter, ColumnDragEnter );
			this.ColumnDragEventListeners[2] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragover, ColumnDragOver );
			this.ColumnDragEventListeners[3] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragend, ColumnDragEnd );
			this.ColumnDragEventListeners[4] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.dragleave, ColumnDragLeave );
			this.ColumnDragEventListeners[5] = new Pair<MouseEventType, ElementEventListener>( MouseEventType.drop, ColumnDrop );
		}

		private static Element FindParentOfType ( Element element, string tagName ) {
			while ( element != null && String.Compare( element.TagName, tagName, true ) != 0 ) {
				element = element.ParentNode;
			}

			return element;
		}

		private Element draggedObject = null;
        private int CurrentMaxColumnHeight = 0;

		private void DragStart ( ElementEvent e ) {
			Element target = FindParentOfType( e.Target, "span" );
			Console.Log( "DragStart: " + target.ID );
			e.DataTransfer.SetData( DataFormat.Text, "Dummy Payload" );
			draggedObject = target;
			e.StopPropagation();
		}

		private void DragEnter ( ElementEvent e ) {
			Element target = FindParentOfType( e.Target, "span" );
			Console.Log( "DragEnter: " + target.ID );
			e.PreventDefault();
			e.StopPropagation();
		}

		private void DragOver ( ElementEvent e ) {
			Element target = FindParentOfType( e.Target, "span" );
			Console.Log( "DragOver: " + target.ID );
			e.PreventDefault();
			e.StopPropagation();
		}

		private void DragLeave ( ElementEvent e ) {
			Element target = FindParentOfType( e.Target, "span" );
			Console.Log( "DragLeave: " + target.ID );
			e.PreventDefault();
			e.StopPropagation();
		}

		private void DragEnd ( ElementEvent e ) {
			Element target = FindParentOfType( e.Target, "span" );
			Console.Log( "DragEnd: " + target.ID );
			e.PreventDefault();
			e.StopPropagation();
		}

		private void Drop ( ElementEvent e ) {
			Element target = FindParentOfType( e.Target, "span" );
			Console.Log( "Drop: " + target.ID );
			e.PreventDefault();
			e.StopPropagation();

            //Since the parent will change after we insert the item somewhere,
            //we need to keep a reference to where it was before it moves
            Element temp = draggedObject.ParentNode;
            target.ParentNode.InsertBefore(draggedObject, target);

            //We'll check using the column name to ensure that the item moved
            //either to or from a column
            if (ColumnNames.Contains(target.ParentNode.NodeName))
            {
                UpdateColumnHeight(target.ParentNode.ParentNode);
            }
            else if (ColumnNames.Contains(temp.NodeName))
            {
                UpdateColumnHeight(temp.ParentNode);
            }
		}

		private void ColumnDragStart ( ElementEvent e ) {
			Console.Log( "DragEnter: " + e.Target.ID );
			e.PreventDefault();
		}

		private void ColumnDragEnter ( ElementEvent e ) {
			Console.Log( "DragEnter: " + e.Target.ID );
			e.PreventDefault();
		}

		private void ColumnDragOver ( ElementEvent e ) {
			Console.Log( "DragOver: " + e.Target.ID );
			e.PreventDefault();
		}

		private void ColumnDragLeave ( ElementEvent e ) {
			Console.Log( "DragLeave: " + e.Target.ID );
			e.PreventDefault();
		}

		private void ColumnDragEnd ( ElementEvent e ) {
			Console.Log( "DragEnd: " + e.Target.ID );
			e.PreventDefault();
		}

		private void ColumnDrop ( ElementEvent e ) {
			Element target = e.Target;
			Console.Log( "Drop: " + target.ID );
			e.PreventDefault();

            //Get the position of the mouse cursor
            int mouseX = e.PageX;
            int mouseY = e.PageY;
            Console.Log("Mouse location is " + mouseX + ", " + mouseY);

            //These variables are to keep track of which is closest
            int closestBlockIndex = -1;
            double closestDistance = double.MaxValue;

            //Since the parent will change after we insert the item somewhere,
            //we need to keep a reference to where it was before it moves
            Element temp = draggedObject.ParentNode;

            //For every block in the container, check its actual distance
            //from the mouse cursor's position
            Console.Log("Determining the closest graph to the drop position");
            for (int i = 0; i < target.ChildNodes.Length; i++) {
                Console.Log(target.ChildNodes[i].NodeName + " OffsetLeft = " + target.ChildNodes[i].OffsetLeft + 
                    ", OffsetTop = " + target.ChildNodes[i].OffsetTop + 
                    ", blockWidth = " + blockWidth + 
                    ", blockHeight = " + blockHeight);
                
                //Calculate the centre of the current network
                int graphX = target.ChildNodes[i].OffsetLeft + (blockWidth)/2;
                int graphY = target.ChildNodes[i].OffsetTop + (blockHeight)/2;
                Console.Log(target.ChildNodes[i].ID + " centre = " + graphX + ", " + graphY);

                //Determine the distance between the current centre and the
                //mouse's position using Pythagoras
                double currentDistance = Math.Sqrt(Math.Pow(mouseX - graphX, 2) + Math.Pow(mouseY - graphY, 2));
                Console.Log(target.ChildNodes[i].ID + " centre is " + currentDistance + " away.");

                //If the calculated distance is less than the current closest,
                //this network is the new closest
                if (currentDistance < closestDistance) {
                    closestDistance = currentDistance;
                    closestBlockIndex = i;
                    Console.Log(target.ChildNodes[i].ID + " is the new closest.");
                }
            }

            //If we found a closest network, place the dragged network in front
            //of the closest one (in essence, putting it in its place)
            if (closestBlockIndex > -1) {
                target.InsertBefore(draggedObject, target.ChildNodes[closestBlockIndex]);
                Console.Log("The dropped object was placed in front of " + target.ChildNodes[closestBlockIndex+1].ID);
            }

            //Else append it to the end of the container - this should only
            //occur if the container is empty
            else {
                target.AppendChild(draggedObject);
                Console.Log("A closest network could not be found, appending the dropped object to the end of the container.");
            }

			//target.AppendChild( draggedObject );
			UpdateColumnHeight( target.ParentNode );
		}
	}
}

            /*//Get the position of the mouse cursor
            int mouseX = e.PageX;
            int mouseY = e.PageY;
            Console.Log("Mouse location is " + mouseX + ", " + mouseY);

            //These variables are to keep track of which is closest
            int closestBlockIndex = -1;
            double closestDistance = double.MaxValue;

            //Since the parent will change after we insert the item somewhere,
            //we need to keep a reference to where it was before it moves
            Element temp = draggedObject.ParentNode;

            //For every block in the container, check its actual distance
            //from the mouse cursor's position
            Console.Log("Determining the closest graph to the drop position");
            for (int i = 0; i < target.ParentNode.ChildNodes.Length; i++) {
                Console.Log(target.ParentNode.ChildNodes[i].NodeName + " OffsetLeft = " + target.ParentNode.ChildNodes[i].OffsetLeft + 
                    ", OffsetTop = " + target.ParentNode.ChildNodes[i].OffsetTop + 
                    ", ClientWidth = " + target.ParentNode.ChildNodes[i].ClientWidth + 
                    ", ClientHeight = " + target.ParentNode.ChildNodes[i].ClientHeight);
                
                //Calculate the centre of the current network
                int graphX = target.ParentNode.ChildNodes[i].OffsetLeft + (target.ParentNode.ChildNodes[i].ClientWidth)/2;
                int graphY = target.ParentNode.ChildNodes[i].OffsetTop + (target.ParentNode.ChildNodes[i].ClientHeight)/2;
                Console.Log(target.ParentNode.ChildNodes[i].NodeName + " centre = " + graphX + ", " + graphY);

                //Determine the distance between the current centre and the
                //mouse's position using Pythagoras
                double currentDistance = Math.Sqrt(Math.Pow(mouseX - graphX, 2) + Math.Pow(mouseY - graphY, 2));
                Console.Log(target.ParentNode.ChildNodes[i].NodeName + " centre is " + currentDistance + " away.");

                //If the calculated distance is less than the current closest,
                //this network is the new closest
                if (currentDistance < closestDistance) {
                    closestDistance = currentDistance;
                    closestBlockIndex = i;
                    Console.Log(target.ParentNode.ChildNodes[i].NodeName + " is the new closest.");
                }
            }

            //If we found a closest network, place the dragged network in front
            //of the closest one (in essence, putting it in its place)
            if (closestBlockIndex > -1) {
                target.InsertBefore(draggedObject, target.ParentNode.ChildNodes[closestBlockIndex]);
                Console.Log("The dropped object was placed in front of " + target.ParentNode.ChildNodes[closestBlockIndex].NodeName);
            }

            //Else append it to the end of the container - this should only
            //occur if the container is empty
            else {
                target.AppendChild(draggedObject);
                Console.Log("A closest graph could not be found, appending the dropped object to the end of the container.");
            }*/