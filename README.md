# TRNDiff Demo Installation #
# The live version of the application can be found [at this location](http://203.101.224.105/) #
#### Note that the application connects to the [RegPrecise database](http://regprecise.lbl.gov/RegPrecise). From time to time these services are offline and the application may display a 503 Service Temporarily Unavailable error in an alert when the page is loaded. ####

# TRNDiff - Introduction #

![Figure_01.png](https://bitbucket.org/repo/gM6G76/images/40289716-Figure_01.png)

The advent of next generation sequencing technologies has seen explosive growth in genomic data, and dense coverage of related organisms, supporting study of subtle, strain-specific variations as a determinant of function. The challenge thus lies in comparing models of complex relationships, were once restricted to a single genome, across hundreds of sequences. Transcriptional Regulatory Network (TRN) structures document the influence of regulatory proteins called Transcription Factors (TFs) on associated Target Genes (TGs). TRNs are routinely inferred from model systems or iterative search, and analysis requires simultaneous displays of multiple networks well beyond those of existing network visualisation tools (Gehlenborg et al., 2010). TRNDiff supports the comparative analysis and visualization of TRNs (and similarly structured data) from many genomes, allowing rapid identification of functional variations across strains.

## Reconstructing TRNs based on the Regulog approach ##

A widely used assumption to infer regulatory interactions follows: given a known interaction between a TF and TG in a model system, if both the orthologous TF and TG are found in a target genome, then it is assumed the orthologous TF regulates the orthologous TG (citation). This is referred to as the regulog assumption. Traditional regulatory network structures based on this approach display an edge to represent the regulatory relationship. However, this approach does not take into account the identification of transcription factor binding sites (TFBSs), which is becoming the standard approach. We include TFBSs in our construction when visualising such regulatory networks.

## Browsing the RegPrecise database ##

RegPrecise [[http://regprecise.lbl.gov/RegPrecise/](http://regprecise.lbl.gov/RegPrecise/)] is a repository of high-quality reference data which captures the regulatory relationships between genes in 390 prokaryote genomes. The database contains manually curated details of genes, transcription factors and transcription factor binding sites which have been obtained by careful comparative genomic analysis, experimentation and literature review as outlined in the paper:

Pavel S. Novichkov, Olga N. Laikova, Elena S. Novichkova, Mikhail S. Gelfand, Adam P. Arkin, Inna Dubchak, and Dmitry A. Rodionov (2010)  RegPrecise: a database of curated genomic inferences of transcriptional regulatory interactions in prokaryotes. Nucleic Acids Res. 2010 January; 38(Database issue): D111–D118. 

TRNDiff provides you with an easy way to browse the RegPrecise database, locate a regulog of interest, and view the regulons belonging to the regulog. You can save any regulog to your desktop in CSV format, edit the saved file, and reload the edited file to view the results. 

Future versions of this application will provide further integration of TRNDiff and RegPrecise with standard tools such as NCBI BLAST to allow you to execute more powerful workflows. 

## Downloads ##

[Node+TrnDiff](https://bitbucket.org/biovisml/trndiff.2015/downloads/TRNDiff.zip) is a zip file containing a recent snapshot of the TRNDiff system. Unpack and run the server batch file for Windows or Linux. Assumes that you are not already running something on port 80. 

[Source](https://bitbucket.org/biovisml/trndiff.2015/get/11d3c698e431.zip) is the latest commit. 

## Cite Us ##

If you use TRNDiff in your work, please cite the following [paper](http://www.sciencedirect.com/science/article/pii/S1877050915009977): 

Xin-Yi Chua, Lawrence Buckingham, James M. Hogan and Pavel Novichkov (2015). Large Scale Comparative Visualisation of Regulatory Networks with TRNDiff. Proceedings of the 2015 International Conference on Computational Science, Reykjavik, June 1-3 2015. Procedia Computer Science Volume 51, 2015, Pages 713–724. 


## Contact us ##

Please direct any enquiries to the project leader, James Hogan [[j.hogan@qut.edu.au](mailto:j.hogan@qut.edu.au)].